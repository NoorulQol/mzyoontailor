// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GetAddressAdapter$Holder_ViewBinding implements Unbinder {
  private GetAddressAdapter.Holder target;

  @UiThread
  public GetAddressAdapter$Holder_ViewBinding(GetAddressAdapter.Holder target, View source) {
    this.target = target;

    target.mEditLay = Utils.findRequiredViewAsType(source, R.id.get_address_edit_lay, "field 'mEditLay'", LinearLayout.class);
    target.mDeleteLay = Utils.findRequiredViewAsType(source, R.id.get_address_delete_lay, "field 'mDeleteLay'", LinearLayout.class);
    target.mGetAddressUserNameTxt = Utils.findRequiredViewAsType(source, R.id.get_address_user_name_txt, "field 'mGetAddressUserNameTxt'", TextView.class);
    target.mGetAddressTxt = Utils.findRequiredViewAsType(source, R.id.get_address_txt, "field 'mGetAddressTxt'", TextView.class);
    target.mGetAddressPhoneNumTxt = Utils.findRequiredViewAsType(source, R.id.get_address_phone_num, "field 'mGetAddressPhoneNumTxt'", TextView.class);
    target.mGetAddressMakeAsDefaultTxt = Utils.findRequiredViewAsType(source, R.id.address_default_address_lay, "field 'mGetAddressMakeAsDefaultTxt'", RelativeLayout.class);
    target.mGetAddressLocationTxt = Utils.findRequiredViewAsType(source, R.id.get_address_location_txt, "field 'mGetAddressLocationTxt'", TextView.class);
    target.mAddressAdapParLay = Utils.findRequiredViewAsType(source, R.id.address_adap_par_lay, "field 'mAddressAdapParLay'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GetAddressAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mEditLay = null;
    target.mDeleteLay = null;
    target.mGetAddressUserNameTxt = null;
    target.mGetAddressTxt = null;
    target.mGetAddressPhoneNumTxt = null;
    target.mGetAddressMakeAsDefaultTxt = null;
    target.mGetAddressLocationTxt = null;
    target.mAddressAdapParLay = null;
  }
}
