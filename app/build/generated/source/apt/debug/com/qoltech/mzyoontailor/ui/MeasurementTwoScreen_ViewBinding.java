// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeasurementTwoScreen_ViewBinding implements Unbinder {
  private MeasurementTwoScreen target;

  private View view2131296934;

  private View view2131297340;

  private View view2131297349;

  private View view2131297337;

  private View view2131297342;

  private View view2131297344;

  @UiThread
  public MeasurementTwoScreen_ViewBinding(MeasurementTwoScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MeasurementTwoScreen_ViewBinding(final MeasurementTwoScreen target, View source) {
    this.target = target;

    View view;
    target.mMeasurementTwoParLay = Utils.findRequiredViewAsType(source, R.id.measurement_two_par_lay, "field 'mMeasurementTwoParLay'", RelativeLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mViewPager = Utils.findRequiredViewAsType(source, R.id.view_pager, "field 'mViewPager'", ViewPager.class);
    target.mMeasurementTwoSliderOne = Utils.findRequiredViewAsType(source, R.id.measurement_two_slider_one, "field 'mMeasurementTwoSliderOne'", RelativeLayout.class);
    target.mMeasurementTwoSliderTwo = Utils.findRequiredViewAsType(source, R.id.measurement_two_slider_two, "field 'mMeasurementTwoSliderTwo'", RelativeLayout.class);
    target.mMeasurementTwoSliderThree = Utils.findRequiredViewAsType(source, R.id.measurement_two_slider_three, "field 'mMeasurementTwoSliderThree'", RelativeLayout.class);
    target.mMeasurementTwoSliderFour = Utils.findRequiredViewAsType(source, R.id.measurement_two_slider_four, "field 'mMeasurementTwoSliderFour'", RelativeLayout.class);
    target.mMeasurementTwoSliderFive = Utils.findRequiredViewAsType(source, R.id.measurement_two_slider_five, "field 'mMeasurementTwoSliderFive'", RelativeLayout.class);
    target.mMeasurementTwoPartRecyView = Utils.findRequiredViewAsType(source, R.id.measurement_two_part_recy_view, "field 'mMeasurementTwoPartRecyView'", RecyclerView.class);
    target.mViewPageLay = Utils.findRequiredViewAsType(source, R.id.view_pager_lay, "field 'mViewPageLay'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.measurement_two_img_lay, "field 'mMeasurementTwoImgLay' and method 'onClick'");
    target.mMeasurementTwoImgLay = Utils.castView(view, R.id.measurement_two_img_lay, "field 'mMeasurementTwoImgLay'", RelativeLayout.class);
    view2131297340 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.measurement_two_parts_lay, "field 'mMeasurementTwoPartsLay' and method 'onClick'");
    target.mMeasurementTwoPartsLay = Utils.castView(view, R.id.measurement_two_parts_lay, "field 'mMeasurementTwoPartsLay'", RelativeLayout.class);
    view2131297349 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMeasurementTwoImgTxt = Utils.findRequiredViewAsType(source, R.id.measurement_two_img_txt, "field 'mMeasurementTwoImgTxt'", TextView.class);
    target.mMeasurementTwoPartTxt = Utils.findRequiredViewAsType(source, R.id.measurement_two_part_txt, "field 'mMeasurementTwoPartTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.measurement_two_cm_lay, "field 'mMeasurementTwoCmLay' and method 'onClick'");
    target.mMeasurementTwoCmLay = Utils.castView(view, R.id.measurement_two_cm_lay, "field 'mMeasurementTwoCmLay'", RelativeLayout.class);
    view2131297337 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.measurement_two_in_lay, "field 'mMeasurementTwoInLay' and method 'onClick'");
    target.mMeasurementTwoInLay = Utils.castView(view, R.id.measurement_two_in_lay, "field 'mMeasurementTwoInLay'", RelativeLayout.class);
    view2131297342 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMeasurementTwoCmTxt = Utils.findRequiredViewAsType(source, R.id.measurement_two_cm_txt, "field 'mMeasurementTwoCmTxt'", TextView.class);
    target.mMeasurementTwoInTxt = Utils.findRequiredViewAsType(source, R.id.measurement_two_in_txt, "field 'mMeasurementTwoInTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.measurement_two_nxt_lay, "field 'mMeasurementTwoNxtLay' and method 'onClick'");
    target.mMeasurementTwoNxtLay = Utils.castView(view, R.id.measurement_two_nxt_lay, "field 'mMeasurementTwoNxtLay'", RelativeLayout.class);
    view2131297344 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MeasurementTwoScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMeasurementTwoParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mViewPager = null;
    target.mMeasurementTwoSliderOne = null;
    target.mMeasurementTwoSliderTwo = null;
    target.mMeasurementTwoSliderThree = null;
    target.mMeasurementTwoSliderFour = null;
    target.mMeasurementTwoSliderFive = null;
    target.mMeasurementTwoPartRecyView = null;
    target.mViewPageLay = null;
    target.mMeasurementTwoImgLay = null;
    target.mMeasurementTwoPartsLay = null;
    target.mMeasurementTwoImgTxt = null;
    target.mMeasurementTwoPartTxt = null;
    target.mMeasurementTwoCmLay = null;
    target.mMeasurementTwoInLay = null;
    target.mMeasurementTwoCmTxt = null;
    target.mMeasurementTwoInTxt = null;
    target.mMeasurementTwoNxtLay = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297340.setOnClickListener(null);
    view2131297340 = null;
    view2131297349.setOnClickListener(null);
    view2131297349 = null;
    view2131297337.setOnClickListener(null);
    view2131297337 = null;
    view2131297342.setOnClickListener(null);
    view2131297342 = null;
    view2131297344.setOnClickListener(null);
    view2131297344 = null;
  }
}
