// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LanguageSettingScreen_ViewBinding implements Unbinder {
  private LanguageSettingScreen target;

  private View view2131296934;

  private View view2131297000;

  private View view2131296997;

  private View view2131297766;

  @UiThread
  public LanguageSettingScreen_ViewBinding(LanguageSettingScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LanguageSettingScreen_ViewBinding(final LanguageSettingScreen target, View source) {
    this.target = target;

    View view;
    target.mLanguageSettingScreenParLay = Utils.findRequiredViewAsType(source, R.id.languauge_setting_par_lay, "field 'mLanguageSettingScreenParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mLanguageArabicImg = Utils.findRequiredViewAsType(source, R.id.language_arabic_img, "field 'mLanguageArabicImg'", ImageView.class);
    target.mLanguageEnglishImg = Utils.findRequiredViewAsType(source, R.id.language_english_img, "field 'mLanguageEnglishImg'", ImageView.class);
    target.mLanguageEnglishTxt = Utils.findRequiredViewAsType(source, R.id.language_english_txt, "field 'mLanguageEnglishTxt'", TextView.class);
    target.mLanguageArabicTxt = Utils.findRequiredViewAsType(source, R.id.language_arabic_txt, "field 'mLanguageArabicTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.language_english_par_lay, "method 'onClick'");
    view2131297000 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.language_arabic_par_lay, "method 'onClick'");
    view2131296997 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.select_lang_btn, "method 'onClick'");
    view2131297766 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LanguageSettingScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mLanguageSettingScreenParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mLanguageArabicImg = null;
    target.mLanguageEnglishImg = null;
    target.mLanguageEnglishTxt = null;
    target.mLanguageArabicTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297000.setOnClickListener(null);
    view2131297000 = null;
    view2131296997.setOnClickListener(null);
    view2131296997 = null;
    view2131297766.setOnClickListener(null);
    view2131297766 = null;
  }
}
