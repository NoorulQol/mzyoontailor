// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeasurementEditTextScreen_ViewBinding implements Unbinder {
  private MeasurementEditTextScreen target;

  private View view2131296934;

  private View view2131297716;

  private View view2131297715;

  @UiThread
  public MeasurementEditTextScreen_ViewBinding(MeasurementEditTextScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MeasurementEditTextScreen_ViewBinding(final MeasurementEditTextScreen target,
      View source) {
    this.target = target;

    View view;
    target.mMeasurementEdtTxtParLay = Utils.findRequiredViewAsType(source, R.id.measurement_edt_txt_par_lay, "field 'mMeasurementEdtTxtParLay'", RelativeLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mScaleCmEdtTxt = Utils.findRequiredViewAsType(source, R.id.scale_cm_edt_txt, "field 'mScaleCmEdtTxt'", EditText.class);
    target.mMeasurementScalseBodyImg = Utils.findRequiredViewAsType(source, R.id.measurement_scale_body_img, "field 'mMeasurementScalseBodyImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.scale_measurement_ok_btn, "field 'mScaleOkBtn' and method 'onClick'");
    target.mScaleOkBtn = Utils.castView(view, R.id.scale_measurement_ok_btn, "field 'mScaleOkBtn'", Button.class);
    view2131297716 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.scale_measurement_cancel_btn, "field 'mScaleCancelBtn' and method 'onClick'");
    target.mScaleCancelBtn = Utils.castView(view, R.id.scale_measurement_cancel_btn, "field 'mScaleCancelBtn'", Button.class);
    view2131297715 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MeasurementEditTextScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMeasurementEdtTxtParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mScaleCmEdtTxt = null;
    target.mMeasurementScalseBodyImg = null;
    target.mScaleOkBtn = null;
    target.mScaleCancelBtn = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297716.setOnClickListener(null);
    view2131297716 = null;
    view2131297715.setOnClickListener(null);
    view2131297715 = null;
  }
}
