// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterTypeScreen_ViewBinding implements Unbinder {
  private FilterTypeScreen target;

  private View view2131296934;

  private View view2131296807;

  @UiThread
  public FilterTypeScreen_ViewBinding(FilterTypeScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public FilterTypeScreen_ViewBinding(final FilterTypeScreen target, View source) {
    this.target = target;

    View view;
    target.mFilterRecyclerView = Utils.findRequiredViewAsType(source, R.id.filter_type_recycler_view, "field 'mFilterRecyclerView'", RecyclerView.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.filter_type_apply_btn, "method 'onClick'");
    view2131296807 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterTypeScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mFilterRecyclerView = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296807.setOnClickListener(null);
    view2131296807 = null;
  }
}
