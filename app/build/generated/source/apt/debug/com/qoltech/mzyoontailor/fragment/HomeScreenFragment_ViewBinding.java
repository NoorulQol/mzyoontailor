// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeScreenFragment_ViewBinding implements Unbinder {
  private HomeScreenFragment target;

  private View view2131297473;

  private View view2131296509;

  @UiThread
  public HomeScreenFragment_ViewBinding(final HomeScreenFragment target, View source) {
    this.target = target;

    View view;
    target.mHomeScreenParLay = Utils.findRequiredViewAsType(source, R.id.home_screen_frag_par_lay, "field 'mHomeScreenParLay'", LinearLayout.class);
    target.mHomeCircleImg = Utils.findRequiredViewAsType(source, R.id.profile_image_circle_img, "field 'mHomeCircleImg'", CircleImageView.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.new_order_inner_lay, "method 'onClick'");
    view2131297473 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.book_an_appointment_inner_lay, "method 'onClick'");
    view2131296509 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    HomeScreenFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHomeScreenParLay = null;
    target.mHomeCircleImg = null;
    target.mHeaderTxt = null;

    view2131297473.setOnClickListener(null);
    view2131297473 = null;
    view2131296509.setOnClickListener(null);
    view2131296509 = null;
  }
}
