// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginScreen_ViewBinding implements Unbinder {
  private LoginScreen target;

  private View view2131297043;

  private View view2131296660;

  @UiThread
  public LoginScreen_ViewBinding(LoginScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginScreen_ViewBinding(final LoginScreen target, View source) {
    this.target = target;

    View view;
    target.mFlagImg = Utils.findRequiredViewAsType(source, R.id.flag_img, "field 'mFlagImg'", ImageView.class);
    target.mCountryCodeTxt = Utils.findRequiredViewAsType(source, R.id.country_code_txt, "field 'mCountryCodeTxt'", TextView.class);
    target.mMobileNumEdtTxt = Utils.findRequiredViewAsType(source, R.id.mobile_num_edt_txt, "field 'mMobileNumEdtTxt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.login_continue_btn, "method 'onClick'");
    view2131297043 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.country_code_lay, "method 'onClick'");
    view2131296660 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mFlagImg = null;
    target.mCountryCodeTxt = null;
    target.mMobileNumEdtTxt = null;

    view2131297043.setOnClickListener(null);
    view2131297043 = null;
    view2131296660.setOnClickListener(null);
    view2131296660 = null;
  }
}
