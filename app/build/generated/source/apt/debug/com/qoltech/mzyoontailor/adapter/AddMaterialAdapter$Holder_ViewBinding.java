// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddMaterialAdapter$Holder_ViewBinding implements Unbinder {
  private AddMaterialAdapter.Holder target;

  @UiThread
  public AddMaterialAdapter$Holder_ViewBinding(AddMaterialAdapter.Holder target, View source) {
    this.target = target;

    target.mGridViewLay = Utils.findRequiredViewAsType(source, R.id.grid_view_only_img_lay, "field 'mGridViewLay'", LinearLayout.class);
    target.mGridViewImgLay = Utils.findRequiredViewAsType(source, R.id.grid_view_only_img, "field 'mGridViewImgLay'", ImageView.class);
    target.mGridViewOnlyDeleteImg = Utils.findRequiredViewAsType(source, R.id.grid_view_only_img_close_img, "field 'mGridViewOnlyDeleteImg'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddMaterialAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mGridViewLay = null;
    target.mGridViewImgLay = null;
    target.mGridViewOnlyDeleteImg = null;
  }
}
