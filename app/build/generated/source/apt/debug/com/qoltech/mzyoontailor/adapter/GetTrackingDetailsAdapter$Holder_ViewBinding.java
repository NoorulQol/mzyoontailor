// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GetTrackingDetailsAdapter$Holder_ViewBinding implements Unbinder {
  private GetTrackingDetailsAdapter.Holder target;

  @UiThread
  public GetTrackingDetailsAdapter$Holder_ViewBinding(GetTrackingDetailsAdapter.Holder target,
      View source) {
    this.target = target;

    target.mAdapterGetTrackingDatePlaecdTxt = Utils.findRequiredViewAsType(source, R.id.adapter_get_tracking_date_placed_txt, "field 'mAdapterGetTrackingDatePlaecdTxt'", TextView.class);
    target.mAdapterGetTrackingTimePlacedTxt = Utils.findRequiredViewAsType(source, R.id.adapter_get_tracking_time_placed_txt, "field 'mAdapterGetTrackingTimePlacedTxt'", TextView.class);
    target.mAdapterTrackingDetailPlacedTxt = Utils.findRequiredViewAsType(source, R.id.adapter_tracking_details_placing_txt, "field 'mAdapterTrackingDetailPlacedTxt'", TextView.class);
    target.mAdapterTrackingDetailsLineLay = Utils.findRequiredView(source, R.id.adapter_tracking_details_line_lay, "field 'mAdapterTrackingDetailsLineLay'");
  }

  @Override
  @CallSuper
  public void unbind() {
    GetTrackingDetailsAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mAdapterGetTrackingDatePlaecdTxt = null;
    target.mAdapterGetTrackingTimePlacedTxt = null;
    target.mAdapterTrackingDetailPlacedTxt = null;
    target.mAdapterTrackingDetailsLineLay = null;
  }
}
