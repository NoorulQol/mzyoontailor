// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TailorListAdapter$Holder_ViewBinding implements Unbinder {
  private TailorListAdapter.Holder target;

  @UiThread
  public TailorListAdapter$Holder_ViewBinding(TailorListAdapter.Holder target, View source) {
    this.target = target;

    target.mAdapterTailorParLay = Utils.findRequiredViewAsType(source, R.id.adapter_tailor_par_lay, "field 'mAdapterTailorParLay'", RelativeLayout.class);
    target.mTailorNameTxt = Utils.findRequiredViewAsType(source, R.id.tailor_user_name_txt, "field 'mTailorNameTxt'", TextView.class);
    target.mTailorActShopTxt = Utils.findRequiredViewAsType(source, R.id.tailor_act_shop_name_txt, "field 'mTailorActShopTxt'", TextView.class);
    target.mTailorActOrderNoTxt = Utils.findRequiredViewAsType(source, R.id.tailor_order_count_txt, "field 'mTailorActOrderNoTxt'", TextView.class);
    target.mTailorUserImg = Utils.findRequiredViewAsType(source, R.id.tailor_user_img, "field 'mTailorUserImg'", ImageView.class);
    target.mTailorListRatingBar = Utils.findRequiredViewAsType(source, R.id.tailor_list_rating_bar, "field 'mTailorListRatingBar'", RatingBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TailorListAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mAdapterTailorParLay = null;
    target.mTailorNameTxt = null;
    target.mTailorActShopTxt = null;
    target.mTailorActOrderNoTxt = null;
    target.mTailorUserImg = null;
    target.mTailorListRatingBar = null;
  }
}
