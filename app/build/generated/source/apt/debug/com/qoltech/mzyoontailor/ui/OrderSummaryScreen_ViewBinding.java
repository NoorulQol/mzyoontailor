// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderSummaryScreen_ViewBinding implements Unbinder {
  private OrderSummaryScreen target;

  private View view2131296934;

  private View view2131297534;

  @UiThread
  public OrderSummaryScreen_ViewBinding(OrderSummaryScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderSummaryScreen_ViewBinding(final OrderSummaryScreen target, View source) {
    this.target = target;

    View view;
    target.mOrderSummaryParLay = Utils.findRequiredViewAsType(source, R.id.order_summary_par_lay, "field 'mOrderSummaryParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mOrderApprovalGenderTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_gender_txt, "field 'mOrderApprovalGenderTxt'", TextView.class);
    target.mOrderApprovalDressTypeTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_dress_type_txt, "field 'mOrderApprovalDressTypeTxt'", TextView.class);
    target.mOrderApprovalSubTypeTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_sub_type_txt, "field 'mOrderApprovalSubTypeTxt'", TextView.class);
    target.mOrderApprovalSeasonalTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_seasonal_txt, "field 'mOrderApprovalSeasonalTxt'", TextView.class);
    target.mOrderApprovalPlaceTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_place_txt, "field 'mOrderApprovalPlaceTxt'", TextView.class);
    target.mOrderApprovalBrandsTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_brands_txt, "field 'mOrderApprovalBrandsTxt'", TextView.class);
    target.mOrderApprovalColorTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_color_txt, "field 'mOrderApprovalColorTxt'", TextView.class);
    target.mOrderApprovalMaterialTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_material_txt, "field 'mOrderApprovalMaterialTxt'", TextView.class);
    target.mOrderApprovalPatternTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_pattern_txt, "field 'mOrderApprovalPatternTxt'", TextView.class);
    target.mOrderApprovalCustomizationRecView = Utils.findRequiredViewAsType(source, R.id.order_approval_customization_rec_view, "field 'mOrderApprovalCustomizationRecView'", RecyclerView.class);
    target.mOrderApprovalTailorRecView = Utils.findRequiredViewAsType(source, R.id.order_approval_tailor_rec_view, "field 'mOrderApprovalTailorRecView'", RecyclerView.class);
    target.mOrderApprovalTailorListCount = Utils.findRequiredViewAsType(source, R.id.order_approval_tailor_list_count, "field 'mOrderApprovalTailorListCount'", TextView.class);
    target.mCustomizationOneTwoTxt = Utils.findRequiredViewAsType(source, R.id.customization_one_two_txt, "field 'mCustomizationOneTwoTxt'", TextView.class);
    target.mCustomizationOneTwoParLay = Utils.findRequiredViewAsType(source, R.id.customization_one_two_par_lay, "field 'mCustomizationOneTwoParLay'", LinearLayout.class);
    target.mSummaryMeasurementTxt = Utils.findRequiredViewAsType(source, R.id.summary_measurement_txt, "field 'mSummaryMeasurementTxt'", TextView.class);
    target.mSummaryServiceTypeTxt = Utils.findRequiredViewAsType(source, R.id.summary_service_type_txt, "field 'mSummaryServiceTypeTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.order_summary_submit, "method 'onClick'");
    view2131297534 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderSummaryScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mOrderSummaryParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mOrderApprovalGenderTxt = null;
    target.mOrderApprovalDressTypeTxt = null;
    target.mOrderApprovalSubTypeTxt = null;
    target.mOrderApprovalSeasonalTxt = null;
    target.mOrderApprovalPlaceTxt = null;
    target.mOrderApprovalBrandsTxt = null;
    target.mOrderApprovalColorTxt = null;
    target.mOrderApprovalMaterialTxt = null;
    target.mOrderApprovalPatternTxt = null;
    target.mOrderApprovalCustomizationRecView = null;
    target.mOrderApprovalTailorRecView = null;
    target.mOrderApprovalTailorListCount = null;
    target.mCustomizationOneTwoTxt = null;
    target.mCustomizationOneTwoParLay = null;
    target.mSummaryMeasurementTxt = null;
    target.mSummaryServiceTypeTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297534.setOnClickListener(null);
    view2131297534 = null;
  }
}
