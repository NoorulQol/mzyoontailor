// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderApprovalPriceAdapter$Holder_ViewBinding implements Unbinder {
  private OrderApprovalPriceAdapter.Holder target;

  @UiThread
  public OrderApprovalPriceAdapter$Holder_ViewBinding(OrderApprovalPriceAdapter.Holder target,
      View source) {
    this.target = target;

    target.mOrderApprovalChargeNameTxt = Utils.findRequiredViewAsType(source, R.id.adapter_order_approval_charges_name, "field 'mOrderApprovalChargeNameTxt'", TextView.class);
    target.mOrderApprovalChargeTxt = Utils.findRequiredViewAsType(source, R.id.adapter_order_price_details_measurement_price, "field 'mOrderApprovalChargeTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderApprovalPriceAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mOrderApprovalChargeNameTxt = null;
    target.mOrderApprovalChargeTxt = null;
  }
}
