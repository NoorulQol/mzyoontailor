// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeasurementOneScreen_ViewBinding implements Unbinder {
  private MeasurementOneScreen target;

  private View view2131296934;

  private View view2131297328;

  private View view2131297336;

  private View view2131297217;

  @UiThread
  public MeasurementOneScreen_ViewBinding(MeasurementOneScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MeasurementOneScreen_ViewBinding(final MeasurementOneScreen target, View source) {
    this.target = target;

    View view;
    target.mMeasurementOneParLay = Utils.findRequiredViewAsType(source, R.id.measurement_one_par_lay, "field 'mMeasurementOneParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMeasurementManuallyImg = Utils.findRequiredViewAsType(source, R.id.measurement_manully_img, "field 'mMeasurementManuallyImg'", ImageView.class);
    target.mMeasurementTailorImg = Utils.findRequiredViewAsType(source, R.id.measurement_tailor_img, "field 'mMeasurementTailorImg'", ImageView.class);
    target.mMeasurementTailorComeImg = Utils.findRequiredViewAsType(source, R.id.measurement_tailor_come_img, "field 'mMeasurementTailorComeImg'", ImageView.class);
    target.mMeasurementManulllyTxt = Utils.findRequiredViewAsType(source, R.id.measurement_manully_txt, "field 'mMeasurementManulllyTxt'", TextView.class);
    target.mMeasurementTailorTxt = Utils.findRequiredViewAsType(source, R.id.measurement_tailor_txt, "field 'mMeasurementTailorTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.measurement_tailor_come_to_place_lay, "field 'mMeasurementTailorComeToPlaceLay' and method 'onClick'");
    target.mMeasurementTailorComeToPlaceLay = Utils.castView(view, R.id.measurement_tailor_come_to_place_lay, "field 'mMeasurementTailorComeToPlaceLay'", LinearLayout.class);
    view2131297328 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.measurement_to_tailor_lay, "field 'mMeasurementToTailorLay' and method 'onClick'");
    target.mMeasurementToTailorLay = Utils.castView(view, R.id.measurement_to_tailor_lay, "field 'mMeasurementToTailorLay'", LinearLayout.class);
    view2131297336 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMeasurementTailorComeTxt = Utils.findRequiredViewAsType(source, R.id.measurement_tailor_come_txt, "field 'mMeasurementTailorComeTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.measurement_manully_lay, "method 'onClick'");
    view2131297217 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MeasurementOneScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMeasurementOneParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mMeasurementManuallyImg = null;
    target.mMeasurementTailorImg = null;
    target.mMeasurementTailorComeImg = null;
    target.mMeasurementManulllyTxt = null;
    target.mMeasurementTailorTxt = null;
    target.mMeasurementTailorComeToPlaceLay = null;
    target.mMeasurementToTailorLay = null;
    target.mMeasurementTailorComeTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297328.setOnClickListener(null);
    view2131297328 = null;
    view2131297336.setOnClickListener(null);
    view2131297336 = null;
    view2131297217.setOnClickListener(null);
    view2131297217 = null;
  }
}
