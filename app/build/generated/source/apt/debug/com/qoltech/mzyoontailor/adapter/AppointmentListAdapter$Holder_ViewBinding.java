// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentListAdapter$Holder_ViewBinding implements Unbinder {
  private AppointmentListAdapter.Holder target;

  @UiThread
  public AppointmentListAdapter$Holder_ViewBinding(AppointmentListAdapter.Holder target,
      View source) {
    this.target = target;

    target.mAppointmentNameImg = Utils.findRequiredViewAsType(source, R.id.appointment_list_img, "field 'mAppointmentNameImg'", ImageView.class);
    target.mAppointmentListDateTxt = Utils.findRequiredViewAsType(source, R.id.appointment_list_date_txt, "field 'mAppointmentListDateTxt'", TextView.class);
    target.mAppointmentIdTxt = Utils.findRequiredViewAsType(source, R.id.appointment_id_txt, "field 'mAppointmentIdTxt'", TextView.class);
    target.mAppointmentTailorNameTxt = Utils.findRequiredViewAsType(source, R.id.appointment_tailor_name_txt, "field 'mAppointmentTailorNameTxt'", TextView.class);
    target.mAppointmentShopNameTxt = Utils.findRequiredViewAsType(source, R.id.appointment_shop_name_txt, "field 'mAppointmentShopNameTxt'", TextView.class);
    target.mAppointmentProductNameTxt = Utils.findRequiredViewAsType(source, R.id.appointment_product_name_txt, "field 'mAppointmentProductNameTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentListAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mAppointmentNameImg = null;
    target.mAppointmentListDateTxt = null;
    target.mAppointmentIdTxt = null;
    target.mAppointmentTailorNameTxt = null;
    target.mAppointmentShopNameTxt = null;
    target.mAppointmentProductNameTxt = null;
  }
}
