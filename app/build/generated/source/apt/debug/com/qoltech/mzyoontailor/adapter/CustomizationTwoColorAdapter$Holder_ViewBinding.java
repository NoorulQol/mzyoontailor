// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizationTwoColorAdapter$Holder_ViewBinding implements Unbinder {
  private CustomizationTwoColorAdapter.Holder target;

  @UiThread
  public CustomizationTwoColorAdapter$Holder_ViewBinding(CustomizationTwoColorAdapter.Holder target,
      View source) {
    this.target = target;

    target.mGridViewLay = Utils.findRequiredViewAsType(source, R.id.grid_custimize_view_lay, "field 'mGridViewLay'", LinearLayout.class);
    target.mGridViewImgLay = Utils.findRequiredViewAsType(source, R.id.grid_custimize_view_img_lay, "field 'mGridViewImgLay'", ImageView.class);
    target.mGridViewTxt = Utils.findRequiredViewAsType(source, R.id.grid_custimize_view_txt, "field 'mGridViewTxt'", TextView.class);
    target.mGridCustimizeImg = Utils.findRequiredViewAsType(source, R.id.grid_custimize_img, "field 'mGridCustimizeImg'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizationTwoColorAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mGridViewLay = null;
    target.mGridViewImgLay = null;
    target.mGridViewTxt = null;
    target.mGridCustimizeImg = null;
  }
}
