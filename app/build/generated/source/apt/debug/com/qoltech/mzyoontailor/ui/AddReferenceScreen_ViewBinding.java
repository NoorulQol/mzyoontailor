// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddReferenceScreen_ViewBinding implements Unbinder {
  private AddReferenceScreen target;

  private View view2131296934;

  private View view2131297680;

  private View view2131296399;

  @UiThread
  public AddReferenceScreen_ViewBinding(AddReferenceScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddReferenceScreen_ViewBinding(final AddReferenceScreen target, View source) {
    this.target = target;

    View view;
    target.mAddReferencePayLay = Utils.findRequiredViewAsType(source, R.id.add_reference_par_lay, "field 'mAddReferencePayLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mReferenceRecyclerView = Utils.findRequiredViewAsType(source, R.id.add_refenece_recycler_view, "field 'mReferenceRecyclerView'", RecyclerView.class);
    target.mAddReferenceImg = Utils.findRequiredViewAsType(source, R.id.add_reference_img, "field 'mAddReferenceImg'", ImageView.class);
    target.mAddImg = Utils.findRequiredViewAsType(source, R.id.add_reference_add_img, "field 'mAddImg'", ImageView.class);
    target.mNoImagesTxt = Utils.findRequiredViewAsType(source, R.id.no_imges_shown, "field 'mNoImagesTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.reference_next_btn, "method 'onClick'");
    view2131297680 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_reference_lay, "method 'onClick'");
    view2131296399 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddReferenceScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mAddReferencePayLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mReferenceRecyclerView = null;
    target.mAddReferenceImg = null;
    target.mAddImg = null;
    target.mNoImagesTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297680.setOnClickListener(null);
    view2131297680 = null;
    view2131296399.setOnClickListener(null);
    view2131296399 = null;
  }
}
