// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentListScreen_ViewBinding implements Unbinder {
  private AppointmentListScreen target;

  private View view2131296934;

  @UiThread
  public AppointmentListScreen_ViewBinding(AppointmentListScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AppointmentListScreen_ViewBinding(final AppointmentListScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mAppointmentListRecyclerView = Utils.findRequiredViewAsType(source, R.id.appointment_list_recycler_view, "field 'mAppointmentListRecyclerView'", RecyclerView.class);
    target.mAppointmentListParLay = Utils.findRequiredViewAsType(source, R.id.appointment_list_par_lay, "field 'mAppointmentListParLay'", LinearLayout.class);
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentListScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mAppointmentListRecyclerView = null;
    target.mAppointmentListParLay = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
  }
}
