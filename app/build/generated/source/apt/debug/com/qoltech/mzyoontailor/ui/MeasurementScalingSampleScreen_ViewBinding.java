// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeasurementScalingSampleScreen_ViewBinding implements Unbinder {
  private MeasurementScalingSampleScreen target;

  private View view2131296934;

  private View view2131297716;

  @UiThread
  public MeasurementScalingSampleScreen_ViewBinding(MeasurementScalingSampleScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MeasurementScalingSampleScreen_ViewBinding(final MeasurementScalingSampleScreen target,
      View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mNumberPicker = Utils.findRequiredViewAsType(source, R.id.number_picker, "field 'mNumberPicker'", NumberPicker.class);
    target.mScaleCmTxt = Utils.findRequiredViewAsType(source, R.id.scale_cm_txt, "field 'mScaleCmTxt'", TextView.class);
    target.mMeasurementScalseBodyImg = Utils.findRequiredViewAsType(source, R.id.measurement_scale_body_img, "field 'mMeasurementScalseBodyImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.scale_measurement_ok_btn, "method 'onClick'");
    view2131297716 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    MeasurementScalingSampleScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mNumberPicker = null;
    target.mScaleCmTxt = null;
    target.mMeasurementScalseBodyImg = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297716.setOnClickListener(null);
    view2131297716 = null;
  }
}
