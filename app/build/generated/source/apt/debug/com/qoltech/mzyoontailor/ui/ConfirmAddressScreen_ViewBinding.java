// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ConfirmAddressScreen_ViewBinding implements Unbinder {
  private ConfirmAddressScreen target;

  @UiThread
  public ConfirmAddressScreen_ViewBinding(ConfirmAddressScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ConfirmAddressScreen_ViewBinding(ConfirmAddressScreen target, View source) {
    this.target = target;

    target.mConfirmAddressParLay = Utils.findRequiredViewAsType(source, R.id.confirm_address_par_lay, "field 'mConfirmAddressParLay'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ConfirmAddressScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mConfirmAddressParLay = null;
  }
}
