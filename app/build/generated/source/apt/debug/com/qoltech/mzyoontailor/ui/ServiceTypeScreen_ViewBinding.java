// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ServiceTypeScreen_ViewBinding implements Unbinder {
  private ServiceTypeScreen target;

  private View view2131296934;

  private View view2131297790;

  private View view2131297784;

  private View view2131297777;

  @UiThread
  public ServiceTypeScreen_ViewBinding(ServiceTypeScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ServiceTypeScreen_ViewBinding(final ServiceTypeScreen target, View source) {
    this.target = target;

    View view;
    target.mServiceTypeParLay = Utils.findRequiredViewAsType(source, R.id.service_type_par_lay, "field 'mServiceTypeParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mServiceTypeUrgentImg = Utils.findRequiredViewAsType(source, R.id.service_type_urgent_img, "field 'mServiceTypeUrgentImg'", ImageView.class);
    target.mServiceTypeUrgentTxt = Utils.findRequiredViewAsType(source, R.id.service_type_urgent_txt, "field 'mServiceTypeUrgentTxt'", TextView.class);
    target.mServiceTypeUrgentBodyImg = Utils.findRequiredViewAsType(source, R.id.service_type_urgent_bodyimg, "field 'mServiceTypeUrgentBodyImg'", ImageView.class);
    target.mServiceTypeNormalImg = Utils.findRequiredViewAsType(source, R.id.service_type_normal_img, "field 'mServiceTypeNormalImg'", ImageView.class);
    target.mServiceTypeNormalTxt = Utils.findRequiredViewAsType(source, R.id.service_type_normal_txt, "field 'mServiceTypeNormalTxt'", TextView.class);
    target.mServiceTypeNormalBodyImg = Utils.findRequiredViewAsType(source, R.id.service_type_normal_body_img, "field 'mServiceTypeNormalBodyImg'", ImageView.class);
    target.mServiceTypeAppointmentHeadImg = Utils.findRequiredViewAsType(source, R.id.service_type_appointement_img, "field 'mServiceTypeAppointmentHeadImg'", ImageView.class);
    target.mServiceTypeAppointmentBodyImg = Utils.findRequiredViewAsType(source, R.id.service_type_appointement_body_img, "field 'mServiceTypeAppointmentBodyImg'", ImageView.class);
    target.mServiceTypeAppointmentTxt = Utils.findRequiredViewAsType(source, R.id.service_type_appointement_txt, "field 'mServiceTypeAppointmentTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.service_type_urgent_lay, "field 'mServiceTypeUrgentLay' and method 'onClick'");
    target.mServiceTypeUrgentLay = Utils.castView(view, R.id.service_type_urgent_lay, "field 'mServiceTypeUrgentLay'", LinearLayout.class);
    view2131297790 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.service_type_normal_lay, "field 'mServiceTypeNormalLay' and method 'onClick'");
    target.mServiceTypeNormalLay = Utils.castView(view, R.id.service_type_normal_lay, "field 'mServiceTypeNormalLay'", LinearLayout.class);
    view2131297784 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.service_type_appointement_lay, "field 'mServiceTypeAppointmentLay' and method 'onClick'");
    target.mServiceTypeAppointmentLay = Utils.castView(view, R.id.service_type_appointement_lay, "field 'mServiceTypeAppointmentLay'", LinearLayout.class);
    view2131297777 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mServiceTypeUrgentEmptyTxt = Utils.findRequiredViewAsType(source, R.id.service_type_urgent_empty_lay, "field 'mServiceTypeUrgentEmptyTxt'", TextView.class);
    target.mServiceTypeNormalEmtptyTxt = Utils.findRequiredViewAsType(source, R.id.service_type_normal_empty_lay, "field 'mServiceTypeNormalEmtptyTxt'", TextView.class);
    target.mServiceTypeAppointmentEmptyTxt = Utils.findRequiredViewAsType(source, R.id.service_type_appointement_empty_lay, "field 'mServiceTypeAppointmentEmptyTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ServiceTypeScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mServiceTypeParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mServiceTypeUrgentImg = null;
    target.mServiceTypeUrgentTxt = null;
    target.mServiceTypeUrgentBodyImg = null;
    target.mServiceTypeNormalImg = null;
    target.mServiceTypeNormalTxt = null;
    target.mServiceTypeNormalBodyImg = null;
    target.mServiceTypeAppointmentHeadImg = null;
    target.mServiceTypeAppointmentBodyImg = null;
    target.mServiceTypeAppointmentTxt = null;
    target.mServiceTypeUrgentLay = null;
    target.mServiceTypeNormalLay = null;
    target.mServiceTypeAppointmentLay = null;
    target.mServiceTypeUrgentEmptyTxt = null;
    target.mServiceTypeNormalEmtptyTxt = null;
    target.mServiceTypeAppointmentEmptyTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297790.setOnClickListener(null);
    view2131297790 = null;
    view2131297784.setOnClickListener(null);
    view2131297784 = null;
    view2131297777.setOnClickListener(null);
    view2131297777 = null;
  }
}
