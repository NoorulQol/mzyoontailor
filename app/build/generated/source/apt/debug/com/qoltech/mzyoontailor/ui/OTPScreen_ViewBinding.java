// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OTPScreen_ViewBinding implements Unbinder {
  private OTPScreen target;

  private View view2131296934;

  private View view2131297552;

  private View view2131297557;

  @UiThread
  public OTPScreen_ViewBinding(OTPScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OTPScreen_ViewBinding(final OTPScreen target, View source) {
    this.target = target;

    View view;
    target.mOtpParLay = Utils.findRequiredViewAsType(source, R.id.otp_par_lay, "field 'mOtpParLay'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackImg' and method 'onClick'");
    target.mHeaderLeftBackImg = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackImg'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mOtpFirstEdtTxt = Utils.findRequiredViewAsType(source, R.id.otp_first_edt_txt, "field 'mOtpFirstEdtTxt'", EditText.class);
    target.mOtpSecondEdtTxt = Utils.findRequiredViewAsType(source, R.id.otp_second_edt_txt, "field 'mOtpSecondEdtTxt'", EditText.class);
    target.mOtpThirdEdtTxt = Utils.findRequiredViewAsType(source, R.id.otp_third_edt_txt, "field 'mOtpThirdEdtTxt'", EditText.class);
    target.mOtpFourthEdtTxt = Utils.findRequiredViewAsType(source, R.id.otp_fourth_edt_txt, "field 'mOtpFourthEdtTxt'", EditText.class);
    target.mOtpFivthEdtTxt = Utils.findRequiredViewAsType(source, R.id.otp_fivth_edt_txt, "field 'mOtpFivthEdtTxt'", EditText.class);
    target.mOtpSixthEdtTxt = Utils.findRequiredViewAsType(source, R.id.otp_sixth_edt_txt, "field 'mOtpSixthEdtTxt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.otp_change_num_btn, "method 'onClick'");
    view2131297552 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.otp_resent_btn, "method 'onClick'");
    view2131297557 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OTPScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mOtpParLay = null;
    target.mHeaderLeftBackImg = null;
    target.mOtpFirstEdtTxt = null;
    target.mOtpSecondEdtTxt = null;
    target.mOtpThirdEdtTxt = null;
    target.mOtpFourthEdtTxt = null;
    target.mOtpFivthEdtTxt = null;
    target.mOtpSixthEdtTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297552.setOnClickListener(null);
    view2131297552 = null;
    view2131297557.setOnClickListener(null);
    view2131297557 = null;
  }
}
