// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GenderAdapter$Holder_ViewBinding implements Unbinder {
  private GenderAdapter.Holder target;

  @UiThread
  public GenderAdapter$Holder_ViewBinding(GenderAdapter.Holder target, View source) {
    this.target = target;

    target.mGridViewImgLay = Utils.findRequiredViewAsType(source, R.id.adapter_gender_img, "field 'mGridViewImgLay'", ImageView.class);
    target.mGridLeftViewTxt = Utils.findRequiredViewAsType(source, R.id.adapter_left_gender_txt, "field 'mGridLeftViewTxt'", TextView.class);
    target.mGridRightViewTxt = Utils.findRequiredViewAsType(source, R.id.adapter_right_gender_txt, "field 'mGridRightViewTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GenderAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mGridViewImgLay = null;
    target.mGridLeftViewTxt = null;
    target.mGridRightViewTxt = null;
  }
}
