// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ScheduletDetailScreen_ViewBinding implements Unbinder {
  private ScheduletDetailScreen target;

  private View view2131296934;

  private View view2131297732;

  private View view2131296443;

  private View view2131296444;

  private View view2131297743;

  private View view2131297736;

  private View view2131297739;

  @UiThread
  public ScheduletDetailScreen_ViewBinding(ScheduletDetailScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ScheduletDetailScreen_ViewBinding(final ScheduletDetailScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mScheduleParLay = Utils.findRequiredViewAsType(source, R.id.schedule_par_lay, "field 'mScheduleParLay'", LinearLayout.class);
    target.mScheduleInnerParLay = Utils.findRequiredViewAsType(source, R.id.schedule_inner_par_lay, "field 'mScheduleInnerParLay'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.schedule_status_txt, "field 'mScheduleStatusTxt' and method 'onClick'");
    target.mScheduleStatusTxt = Utils.castView(view, R.id.schedule_status_txt, "field 'mScheduleStatusTxt'", TextView.class);
    view2131297732 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mAppointmentScheduleTypeHeaderImg = Utils.findRequiredViewAsType(source, R.id.appointment_schedule_type_header_img, "field 'mAppointmentScheduleTypeHeaderImg'", ImageView.class);
    target.mAppointmentScheduleTypeHeaderTxt = Utils.findRequiredViewAsType(source, R.id.appointment_schedule_type_header_txt, "field 'mAppointmentScheduleTypeHeaderTxt'", TextView.class);
    target.mAppointmentScheduleBodyImg = Utils.findRequiredViewAsType(source, R.id.appointment_schedule_type_body_img, "field 'mAppointmentScheduleBodyImg'", ImageView.class);
    target.mScheduleTypeTimeSlotTxt = Utils.findRequiredViewAsType(source, R.id.schedule_type_time_slot_txt, "field 'mScheduleTypeTimeSlotTxt'", TextView.class);
    target.mScheduleTypeFromDateTxt = Utils.findRequiredViewAsType(source, R.id.schedule_type_fromt_date_txt, "field 'mScheduleTypeFromDateTxt'", TextView.class);
    target.mScheduleTypeToDateTxt = Utils.findRequiredViewAsType(source, R.id.appointment_schedule_type_to_date_txt, "field 'mScheduleTypeToDateTxt'", TextView.class);
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.appointment_material_approve_lay, "method 'onClick'");
    view2131296443 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.appointment_material_reject_lay, "method 'onClick'");
    view2131296444 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.schedule_type_time_slot_par_lay, "method 'onClick'");
    view2131297743 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.schedule_type_calender_from_par_lay, "method 'onClick'");
    view2131297736 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.schedule_type_calender_to_par_lay, "method 'onClick'");
    view2131297739 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ScheduletDetailScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mScheduleParLay = null;
    target.mScheduleInnerParLay = null;
    target.mScheduleStatusTxt = null;
    target.mAppointmentScheduleTypeHeaderImg = null;
    target.mAppointmentScheduleTypeHeaderTxt = null;
    target.mAppointmentScheduleBodyImg = null;
    target.mScheduleTypeTimeSlotTxt = null;
    target.mScheduleTypeFromDateTxt = null;
    target.mScheduleTypeToDateTxt = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297732.setOnClickListener(null);
    view2131297732 = null;
    view2131296443.setOnClickListener(null);
    view2131296443 = null;
    view2131296444.setOnClickListener(null);
    view2131296444 = null;
    view2131297743.setOnClickListener(null);
    view2131297743 = null;
    view2131297736.setOnClickListener(null);
    view2131297736 = null;
    view2131297739.setOnClickListener(null);
    view2131297739 = null;
  }
}
