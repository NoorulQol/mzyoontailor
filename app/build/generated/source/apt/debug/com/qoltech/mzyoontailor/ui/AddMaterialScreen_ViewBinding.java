// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddMaterialScreen_ViewBinding implements Unbinder {
  private AddMaterialScreen target;

  private View view2131296934;

  private View view2131296382;

  private View view2131296381;

  @UiThread
  public AddMaterialScreen_ViewBinding(AddMaterialScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddMaterialScreen_ViewBinding(final AddMaterialScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMaterialRecyclerView = Utils.findRequiredViewAsType(source, R.id.add_material_recycler_view, "field 'mMaterialRecyclerView'", RecyclerView.class);
    target.mAddMaterialImg = Utils.findRequiredViewAsType(source, R.id.add_material_img, "field 'mAddMaterialImg'", ImageView.class);
    target.mAddImg = Utils.findRequiredViewAsType(source, R.id.add_material_add_img, "field 'mAddImg'", ImageView.class);
    target.mNoImagesShown = Utils.findRequiredViewAsType(source, R.id.no_imges_shown, "field 'mNoImagesShown'", TextView.class);
    target.mAddMaterialParLay = Utils.findRequiredViewAsType(source, R.id.add_material_par_lay, "field 'mAddMaterialParLay'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.add_material_next_btn, "method 'onClick'");
    view2131296382 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_material_lay, "method 'onClick'");
    view2131296381 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddMaterialScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mMaterialRecyclerView = null;
    target.mAddMaterialImg = null;
    target.mAddImg = null;
    target.mNoImagesShown = null;
    target.mAddMaterialParLay = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296382.setOnClickListener(null);
    view2131296382 = null;
    view2131296381.setOnClickListener(null);
    view2131296381 = null;
  }
}
