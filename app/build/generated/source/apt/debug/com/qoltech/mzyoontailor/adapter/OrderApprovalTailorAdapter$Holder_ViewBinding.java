// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderApprovalTailorAdapter$Holder_ViewBinding implements Unbinder {
  private OrderApprovalTailorAdapter.Holder target;

  @UiThread
  public OrderApprovalTailorAdapter$Holder_ViewBinding(OrderApprovalTailorAdapter.Holder target,
      View source) {
    this.target = target;

    target.mAdapterOrderApprovalImg = Utils.findRequiredViewAsType(source, R.id.adapter_order_approval_img, "field 'mAdapterOrderApprovalImg'", ImageView.class);
    target.mOrderApprovalHeadingTxt = Utils.findRequiredViewAsType(source, R.id.order_approval_heading_txt, "field 'mOrderApprovalHeadingTxt'", TextView.class);
    target.mOrderApprovalTxt = Utils.findRequiredViewAsType(source, R.id.adapter_order_approval_txt, "field 'mOrderApprovalTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderApprovalTailorAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mAdapterOrderApprovalImg = null;
    target.mOrderApprovalHeadingTxt = null;
    target.mOrderApprovalTxt = null;
  }
}
