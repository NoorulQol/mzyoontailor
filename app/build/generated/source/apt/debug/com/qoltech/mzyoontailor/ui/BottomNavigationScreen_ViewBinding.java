// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BottomNavigationScreen_ViewBinding implements Unbinder {
  private BottomNavigationScreen target;

  private View view2131297820;

  private View view2131297822;

  @UiThread
  public BottomNavigationScreen_ViewBinding(BottomNavigationScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public BottomNavigationScreen_ViewBinding(final BottomNavigationScreen target, View source) {
    this.target = target;

    View view;
    target.mHandle = Utils.findRequiredViewAsType(source, R.id.handle, "field 'mHandle'", RelativeLayout.class);
    target.mSliderDrawerImg = Utils.findRequiredViewAsType(source, R.id.slider_drawer_icon_img, "field 'mSliderDrawerImg'", ImageView.class);
    target.simpleSlidingDrawer = Utils.findRequiredViewAsType(source, R.id.simpleSlidingDrawer, "field 'simpleSlidingDrawer'", SlidingDrawer.class);
    view = Utils.findRequiredView(source, R.id.slider_account_lay, "method 'onClick'");
    view2131297820 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.slider_address_lay, "method 'onClick'");
    view2131297822 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    BottomNavigationScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHandle = null;
    target.mSliderDrawerImg = null;
    target.simpleSlidingDrawer = null;

    view2131297820.setOnClickListener(null);
    view2131297820 = null;
    view2131297822.setOnClickListener(null);
    view2131297822 = null;
  }
}
