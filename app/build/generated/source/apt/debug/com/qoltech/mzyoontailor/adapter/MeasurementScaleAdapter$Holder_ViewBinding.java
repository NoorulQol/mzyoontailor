// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeasurementScaleAdapter$Holder_ViewBinding implements Unbinder {
  private MeasurementScaleAdapter.Holder target;

  @UiThread
  public MeasurementScaleAdapter$Holder_ViewBinding(MeasurementScaleAdapter.Holder target,
      View source) {
    this.target = target;

    target.mMeasurementTxt = Utils.findRequiredViewAsType(source, R.id.measurement_txt, "field 'mMeasurementTxt'", TextView.class);
    target.mMeasurementView = Utils.findRequiredView(source, R.id.measurement_view, "field 'mMeasurementView'");
  }

  @Override
  @CallSuper
  public void unbind() {
    MeasurementScaleAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMeasurementTxt = null;
    target.mMeasurementView = null;
  }
}
