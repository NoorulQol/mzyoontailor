// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderTypeScreen_ViewBinding implements Unbinder {
  private OrderTypeScreen target;

  private View view2131296934;

  private View view2131297572;

  private View view2131297571;

  private View view2131296632;

  @UiThread
  public OrderTypeScreen_ViewBinding(OrderTypeScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderTypeScreen_ViewBinding(final OrderTypeScreen target, View source) {
    this.target = target;

    View view;
    target.mOrderTypeParLay = Utils.findRequiredViewAsType(source, R.id.order_type_par_lay, "field 'mOrderTypeParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mOwnMaterialDirectTxt = Utils.findRequiredViewAsType(source, R.id.own_material_direct_txt, "field 'mOwnMaterialDirectTxt'", TextView.class);
    target.mOwnMaterialCourierTxt = Utils.findRequiredViewAsType(source, R.id.own_courier_txt, "field 'mOwnMaterialCourierTxt'", TextView.class);
    target.mCompaniesMaterialTxt = Utils.findRequiredViewAsType(source, R.id.companies_material_txt, "field 'mCompaniesMaterialTxt'", TextView.class);
    target.mOwnDeliveryBodyImg = Utils.findRequiredViewAsType(source, R.id.own_delivery_bodyimg, "field 'mOwnDeliveryBodyImg'", ImageView.class);
    target.mOwnCourierBodyImg = Utils.findRequiredViewAsType(source, R.id.own_courier_body_img, "field 'mOwnCourierBodyImg'", ImageView.class);
    target.mCompaniesMaterialBodyImg = Utils.findRequiredViewAsType(source, R.id.companies_material_body_img, "field 'mCompaniesMaterialBodyImg'", ImageView.class);
    target.mOwnDeliveryImg = Utils.findRequiredViewAsType(source, R.id.own_delviery_img, "field 'mOwnDeliveryImg'", ImageView.class);
    target.mOwnCourierImg = Utils.findRequiredViewAsType(source, R.id.own_courier_img, "field 'mOwnCourierImg'", ImageView.class);
    target.mCompanyImg = Utils.findRequiredViewAsType(source, R.id.company_img, "field 'mCompanyImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.own_material_direct_delivery_lay, "field 'mOwnMaterialDirectLay' and method 'onClick'");
    target.mOwnMaterialDirectLay = Utils.castView(view, R.id.own_material_direct_delivery_lay, "field 'mOwnMaterialDirectLay'", LinearLayout.class);
    view2131297572 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.own_material_courier_lay, "field 'mOwnMaterialCourierLay' and method 'onClick'");
    target.mOwnMaterialCourierLay = Utils.castView(view, R.id.own_material_courier_lay, "field 'mOwnMaterialCourierLay'", LinearLayout.class);
    view2131297571 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.companies_material_lay, "field 'mCompaniesMaterialLay' and method 'onClick'");
    target.mCompaniesMaterialLay = Utils.castView(view, R.id.companies_material_lay, "field 'mCompaniesMaterialLay'", LinearLayout.class);
    view2131296632 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mOwnDeliveryBodyEmptyLay = Utils.findRequiredViewAsType(source, R.id.own_delivery_body_empty_lay, "field 'mOwnDeliveryBodyEmptyLay'", TextView.class);
    target.mOwnCourierBodyEmptyLay = Utils.findRequiredViewAsType(source, R.id.own_courier_body_empty_lay, "field 'mOwnCourierBodyEmptyLay'", TextView.class);
    target.mCompaniesMaterialBodyEmptyLay = Utils.findRequiredViewAsType(source, R.id.companies_material_body_empty_lay, "field 'mCompaniesMaterialBodyEmptyLay'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderTypeScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mOrderTypeParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mOwnMaterialDirectTxt = null;
    target.mOwnMaterialCourierTxt = null;
    target.mCompaniesMaterialTxt = null;
    target.mOwnDeliveryBodyImg = null;
    target.mOwnCourierBodyImg = null;
    target.mCompaniesMaterialBodyImg = null;
    target.mOwnDeliveryImg = null;
    target.mOwnCourierImg = null;
    target.mCompanyImg = null;
    target.mOwnMaterialDirectLay = null;
    target.mOwnMaterialCourierLay = null;
    target.mCompaniesMaterialLay = null;
    target.mOwnDeliveryBodyEmptyLay = null;
    target.mOwnCourierBodyEmptyLay = null;
    target.mCompaniesMaterialBodyEmptyLay = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297572.setOnClickListener(null);
    view2131297572 = null;
    view2131297571.setOnClickListener(null);
    view2131297571 = null;
    view2131296632.setOnClickListener(null);
    view2131296632 = null;
  }
}
