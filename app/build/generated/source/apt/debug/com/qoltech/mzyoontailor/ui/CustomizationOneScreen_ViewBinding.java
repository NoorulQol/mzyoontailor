// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizationOneScreen_ViewBinding implements Unbinder {
  private CustomizationOneScreen target;

  private View view2131296934;

  private View view2131296710;

  @UiThread
  public CustomizationOneScreen_ViewBinding(CustomizationOneScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CustomizationOneScreen_ViewBinding(final CustomizationOneScreen target, View source) {
    this.target = target;

    View view;
    target.mCustomizationOneParLay = Utils.findRequiredViewAsType(source, R.id.customization_one_par_lay, "field 'mCustomizationOneParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mSeasonalRecyclerView = Utils.findRequiredViewAsType(source, R.id.customize_seaonal_recycler_view, "field 'mSeasonalRecyclerView'", RecyclerView.class);
    target.mIndustryRecyclerView = Utils.findRequiredViewAsType(source, R.id.customize_industry_recycler_view, "field 'mIndustryRecyclerView'", RecyclerView.class);
    target.mBrandRecyclerView = Utils.findRequiredViewAsType(source, R.id.customize_brands_recycler_view, "field 'mBrandRecyclerView'", RecyclerView.class);
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.customize_one_nxt_lay, "method 'onClick'");
    view2131296710 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizationOneScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mCustomizationOneParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mSeasonalRecyclerView = null;
    target.mIndustryRecyclerView = null;
    target.mBrandRecyclerView = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296710.setOnClickListener(null);
    view2131296710 = null;
  }
}
