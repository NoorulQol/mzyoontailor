// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingScreen_ViewBinding implements Unbinder {
  private SettingScreen target;

  private View view2131296934;

  private View view2131297002;

  private View view2131296940;

  @UiThread
  public SettingScreen_ViewBinding(SettingScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SettingScreen_ViewBinding(final SettingScreen target, View source) {
    this.target = target;

    View view;
    target.mSettingScreenParLay = Utils.findRequiredViewAsType(source, R.id.setting_par_lay, "field 'mSettingScreenParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mLanguageSettingTxt = Utils.findRequiredViewAsType(source, R.id.language_setting_txt, "field 'mLanguageSettingTxt'", TextView.class);
    target.mHintOnOffTxt = Utils.findRequiredViewAsType(source, R.id.hint_txt, "field 'mHintOnOffTxt'", TextView.class);
    target.mHintOnOffImg = Utils.findRequiredViewAsType(source, R.id.hint_img, "field 'mHintOnOffImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.language_setting_lay, "method 'onClick'");
    view2131297002 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.hint_lay, "method 'onClick'");
    view2131296940 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SettingScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSettingScreenParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mLanguageSettingTxt = null;
    target.mHintOnOffTxt = null;
    target.mHintOnOffImg = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297002.setOnClickListener(null);
    view2131297002 = null;
    view2131296940.setOnClickListener(null);
    view2131296940 = null;
  }
}
