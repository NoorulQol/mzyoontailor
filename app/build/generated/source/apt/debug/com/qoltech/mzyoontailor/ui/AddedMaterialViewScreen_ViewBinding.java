// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import com.rd.PageIndicatorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddedMaterialViewScreen_ViewBinding implements Unbinder {
  private AddedMaterialViewScreen target;

  private View view2131296934;

  @UiThread
  public AddedMaterialViewScreen_ViewBinding(AddedMaterialViewScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddedMaterialViewScreen_ViewBinding(final AddedMaterialViewScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mViewDetailsViewpager = Utils.findRequiredViewAsType(source, R.id.view_details_view_pager, "field 'mViewDetailsViewpager'", ViewPager.class);
    target.mPageIndicator = Utils.findRequiredViewAsType(source, R.id.pageIndicatorView, "field 'mPageIndicator'", PageIndicatorView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddedMaterialViewScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mViewDetailsViewpager = null;
    target.mPageIndicator = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
  }
}
