// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import com.rd.PageIndicatorView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ViewDetailsScreen_ViewBinding implements Unbinder {
  private ViewDetailsScreen target;

  private View view2131296934;

  private View view2131296704;

  @UiThread
  public ViewDetailsScreen_ViewBinding(ViewDetailsScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ViewDetailsScreen_ViewBinding(final ViewDetailsScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mViewDetailsParLay = Utils.findRequiredViewAsType(source, R.id.view_details_par_lay, "field 'mViewDetailsParLay'", LinearLayout.class);
    target.mViewDetailsPatternNameTxt = Utils.findRequiredViewAsType(source, R.id.view_details_pattern_name_txt, "field 'mViewDetailsPatternNameTxt'", TextView.class);
    target.mViewDetailsSeasonalNameTxt = Utils.findRequiredViewAsType(source, R.id.view_details_seasonal_name_txt, "field 'mViewDetailsSeasonalNameTxt'", TextView.class);
    target.mViewDetailsPlaceIndustryNameTxt = Utils.findRequiredViewAsType(source, R.id.view_details_place_industry_name_txt, "field 'mViewDetailsPlaceIndustryNameTxt'", TextView.class);
    target.mViewDetailsBrandsNameTxt = Utils.findRequiredViewAsType(source, R.id.view_details_brands_name_txt, "field 'mViewDetailsBrandsNameTxt'", TextView.class);
    target.mViewDetailsMaterialTypeNameTxt = Utils.findRequiredViewAsType(source, R.id.view_details_material_type_name_txt, "field 'mViewDetailsMaterialTypeNameTxt'", TextView.class);
    target.mViewDetailsColorNameTxt = Utils.findRequiredViewAsType(source, R.id.view_details_color_name_txt, "field 'mViewDetailsColorNameTxt'", TextView.class);
    target.mViewDetailsViewpager = Utils.findRequiredViewAsType(source, R.id.view_details_view_pager, "field 'mViewDetailsViewpager'", ViewPager.class);
    target.mPageIndicator = Utils.findRequiredViewAsType(source, R.id.pageIndicatorView, "field 'mPageIndicator'", PageIndicatorView.class);
    view = Utils.findRequiredView(source, R.id.customization_three_view_details_btn, "method 'onClick'");
    view2131296704 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ViewDetailsScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mViewDetailsParLay = null;
    target.mViewDetailsPatternNameTxt = null;
    target.mViewDetailsSeasonalNameTxt = null;
    target.mViewDetailsPlaceIndustryNameTxt = null;
    target.mViewDetailsBrandsNameTxt = null;
    target.mViewDetailsMaterialTypeNameTxt = null;
    target.mViewDetailsColorNameTxt = null;
    target.mViewDetailsViewpager = null;
    target.mPageIndicator = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296704.setOnClickListener(null);
    view2131296704 = null;
  }
}
