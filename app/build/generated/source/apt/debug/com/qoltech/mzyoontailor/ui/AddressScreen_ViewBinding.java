// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddressScreen_ViewBinding implements Unbinder {
  private AddressScreen target;

  private View view2131296934;

  private View view2131296405;

  private View view2131297677;

  @UiThread
  public AddressScreen_ViewBinding(AddressScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddressScreen_ViewBinding(final AddressScreen target, View source) {
    this.target = target;

    View view;
    target.mAddressPayLay = Utils.findRequiredViewAsType(source, R.id.address_par_lay, "field 'mAddressPayLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mGetAddressRecyclerView = Utils.findRequiredViewAsType(source, R.id.get_address_recycler_view, "field 'mGetAddressRecyclerView'", RecyclerView.class);
    target.mGetAddressEmptyImgLay = Utils.findRequiredViewAsType(source, R.id.get_address_empty_img_lay, "field 'mGetAddressEmptyImgLay'", LinearLayout.class);
    target.mRecyclerViewAddAddressBtn = Utils.findRequiredViewAsType(source, R.id.recycler_view_add_address_btn, "field 'mRecyclerViewAddAddressBtn'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.address_add_btn, "field 'mAddressAddBtn' and method 'onClick'");
    target.mAddressAddBtn = Utils.castView(view, R.id.address_add_btn, "field 'mAddressAddBtn'", Button.class);
    view2131296405 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMeasurementAddressWizardLay = Utils.findRequiredViewAsType(source, R.id.measurement_address_wizard_lay, "field 'mMeasurementAddressWizardLay'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.recyclerview_address_add_btn, "method 'onClick'");
    view2131297677 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddressScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mAddressPayLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mGetAddressRecyclerView = null;
    target.mGetAddressEmptyImgLay = null;
    target.mRecyclerViewAddAddressBtn = null;
    target.mAddressAddBtn = null;
    target.mMeasurementAddressWizardLay = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296405.setOnClickListener(null);
    view2131296405 = null;
    view2131297677.setOnClickListener(null);
    view2131297677 = null;
  }
}
