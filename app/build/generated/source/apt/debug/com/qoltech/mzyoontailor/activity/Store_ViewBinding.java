// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.webkit.WebView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Store_ViewBinding implements Unbinder {
  private Store target;

  @UiThread
  public Store_ViewBinding(Store target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Store_ViewBinding(Store target, View source) {
    this.target = target;

    target.htmlWebView = Utils.findRequiredViewAsType(source, R.id.webView, "field 'htmlWebView'", WebView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Store target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.htmlWebView = null;
  }
}
