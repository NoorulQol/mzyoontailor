// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddAddressScreen_ViewBinding implements Unbinder {
  private AddAddressScreen target;

  private View view2131296934;

  private View view2131297046;

  private View view2131296338;

  private View view2131296343;

  private View view2131296364;

  private View view2131296345;

  private View view2131296375;

  private View view2131296360;

  private View view2131296337;

  @UiThread
  public AddAddressScreen_ViewBinding(AddAddressScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AddAddressScreen_ViewBinding(final AddAddressScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mChooseLocationAddress = Utils.findRequiredViewAsType(source, R.id.choosed_location_address, "field 'mChooseLocationAddress'", TextView.class);
    target.mAddAddressParLay = Utils.findRequiredViewAsType(source, R.id.add_address_par_lay, "field 'mAddAddressParLay'", RelativeLayout.class);
    target.mAddAddressCountyrNameEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_country_edt_txt, "field 'mAddAddressCountyrNameEdtTxt'", EditText.class);
    target.mAddAddressFlagImg = Utils.findRequiredViewAsType(source, R.id.add_address_flag_img, "field 'mAddAddressFlagImg'", ImageView.class);
    target.mAddAddressCountryCodeTxt = Utils.findRequiredViewAsType(source, R.id.add_address_country_code_txt, "field 'mAddAddressCountryCodeTxt'", TextView.class);
    target.mAddAddressStateEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_state_edt_txt, "field 'mAddAddressStateEdtTxt'", EditText.class);
    target.mAddAddressFrsNameEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_frt_name_edt_txt, "field 'mAddAddressFrsNameEdtTxt'", EditText.class);
    target.mAddressSecNameEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_sec_name_edt_txt, "field 'mAddressSecNameEdtTxt'", EditText.class);
    target.mAddressFloorEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_floor_edt_txt, "field 'mAddressFloorEdtTxt'", EditText.class);
    target.mAddressAreaEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_area_edt_txt, "field 'mAddressAreaEdtTxt'", EditText.class);
    target.mAddressLandmarkEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_landmark_edt_txt, "field 'mAddressLandmarkEdtTxt'", EditText.class);
    target.mAddressShippingEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_shipping_edt_txt, "field 'mAddressShippingEdtTxt'", EditText.class);
    target.mAddressLocationTypeEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_loc_type_edt_txt, "field 'mAddressLocationTypeEdtTxt'", EditText.class);
    target.mAddressMobNumEdtTxt = Utils.findRequiredViewAsType(source, R.id.add_address_mobile_num_edt_txt, "field 'mAddressMobNumEdtTxt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.make_as_default_toggle_btn, "field 'mMakeAsDefaultImg' and method 'onClick'");
    target.mMakeAsDefaultImg = Utils.castView(view, R.id.make_as_default_toggle_btn, "field 'mMakeAsDefaultImg'", ImageView.class);
    view2131297046 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_country_code_lay, "method 'onClick'");
    view2131296338 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_country_lay, "method 'onClick'");
    view2131296343 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_save_btn, "method 'onClick'");
    view2131296364 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_edit_btn, "method 'onClick'");
    view2131296345 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_state_lay, "method 'onClick'");
    view2131296375 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_location_type_lay, "method 'onClick'");
    view2131296360 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.add_address_area_lay, "method 'onClick'");
    view2131296337 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AddAddressScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mChooseLocationAddress = null;
    target.mAddAddressParLay = null;
    target.mAddAddressCountyrNameEdtTxt = null;
    target.mAddAddressFlagImg = null;
    target.mAddAddressCountryCodeTxt = null;
    target.mAddAddressStateEdtTxt = null;
    target.mAddAddressFrsNameEdtTxt = null;
    target.mAddressSecNameEdtTxt = null;
    target.mAddressFloorEdtTxt = null;
    target.mAddressAreaEdtTxt = null;
    target.mAddressLandmarkEdtTxt = null;
    target.mAddressShippingEdtTxt = null;
    target.mAddressLocationTypeEdtTxt = null;
    target.mAddressMobNumEdtTxt = null;
    target.mMakeAsDefaultImg = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297046.setOnClickListener(null);
    view2131297046 = null;
    view2131296338.setOnClickListener(null);
    view2131296338 = null;
    view2131296343.setOnClickListener(null);
    view2131296343 = null;
    view2131296364.setOnClickListener(null);
    view2131296364 = null;
    view2131296345.setOnClickListener(null);
    view2131296345 = null;
    view2131296375.setOnClickListener(null);
    view2131296375 = null;
    view2131296360.setOnClickListener(null);
    view2131296360 = null;
    view2131296337.setOnClickListener(null);
    view2131296337 = null;
  }
}
