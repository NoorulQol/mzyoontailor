// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AddMeasurementListAdapter$Holder_ViewBinding implements Unbinder {
  private AddMeasurementListAdapter.Holder target;

  @UiThread
  public AddMeasurementListAdapter$Holder_ViewBinding(AddMeasurementListAdapter.Holder target,
      View source) {
    this.target = target;

    target.mMeasurementImg = Utils.findRequiredViewAsType(source, R.id.profile_image, "field 'mMeasurementImg'", CircleImageView.class);
    target.mAddMeasurementNameTxt = Utils.findRequiredViewAsType(source, R.id.add_measurement_list_measurement_name_txt, "field 'mAddMeasurementNameTxt'", TextView.class);
    target.mAddMeasuremenDressTypeTxt = Utils.findRequiredViewAsType(source, R.id.add_measurement_list_dress_type_txt, "field 'mAddMeasuremenDressTypeTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AddMeasurementListAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMeasurementImg = null;
    target.mAddMeasurementNameTxt = null;
    target.mAddMeasuremenDressTypeTxt = null;
  }
}
