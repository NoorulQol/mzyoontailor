// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileIntroScreen_ViewBinding implements Unbinder {
  private ProfileIntroScreen target;

  private View view2131296934;

  private View view2131297653;

  private View view2131297652;

  @UiThread
  public ProfileIntroScreen_ViewBinding(ProfileIntroScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileIntroScreen_ViewBinding(final ProfileIntroScreen target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mProfileNameEdtTxt = Utils.findRequiredViewAsType(source, R.id.profile_intro_edt_txt, "field 'mProfileNameEdtTxt'", EditText.class);
    target.name_arabic = Utils.findRequiredViewAsType(source, R.id.name_arabic, "field 'name_arabic'", EditText.class);
    target.email_edittext = Utils.findRequiredViewAsType(source, R.id.email_edittext, "field 'email_edittext'", EditText.class);
    target.mProfileIntroImg = Utils.findRequiredViewAsType(source, R.id.profile_intro_image, "field 'mProfileIntroImg'", CircleImageView.class);
    view = Utils.findRequiredView(source, R.id.profile_intro_nxt_btn, "method 'onClick'");
    view2131297653 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.profile_intro_img_lay, "method 'onClick'");
    view2131297652 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileIntroScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mProfileNameEdtTxt = null;
    target.name_arabic = null;
    target.email_edittext = null;
    target.mProfileIntroImg = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297653.setOnClickListener(null);
    view2131297653 = null;
    view2131297652.setOnClickListener(null);
    view2131297652 = null;
  }
}
