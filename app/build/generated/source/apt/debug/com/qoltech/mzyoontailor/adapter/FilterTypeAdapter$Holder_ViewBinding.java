// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FilterTypeAdapter$Holder_ViewBinding implements Unbinder {
  private FilterTypeAdapter.Holder target;

  @UiThread
  public FilterTypeAdapter$Holder_ViewBinding(FilterTypeAdapter.Holder target, View source) {
    this.target = target;

    target.mFilterViewImgLay = Utils.findRequiredViewAsType(source, R.id.filter_type_recycler_view_img, "field 'mFilterViewImgLay'", ImageView.class);
    target.mFilterViewTxt = Utils.findRequiredViewAsType(source, R.id.filter_type_recycler_view_txt, "field 'mFilterViewTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FilterTypeAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mFilterViewImgLay = null;
    target.mFilterViewTxt = null;
  }
}
