// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeasurementTwoPartsAdapter$Holder_ViewBinding implements Unbinder {
  private MeasurementTwoPartsAdapter.Holder target;

  @UiThread
  public MeasurementTwoPartsAdapter$Holder_ViewBinding(MeasurementTwoPartsAdapter.Holder target,
      View source) {
    this.target = target;

    target.mMeasurementPartsImg = Utils.findRequiredViewAsType(source, R.id.adapter_parts_img, "field 'mMeasurementPartsImg'", ImageView.class);
    target.mMeasurementPartsTxt = Utils.findRequiredViewAsType(source, R.id.adapter_parts_txt, "field 'mMeasurementPartsTxt'", TextView.class);
    target.mMeasuremntPartsCmTxt = Utils.findRequiredViewAsType(source, R.id.adpater_parts_cm_txt, "field 'mMeasuremntPartsCmTxt'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MeasurementTwoPartsAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mMeasurementPartsImg = null;
    target.mMeasurementPartsTxt = null;
    target.mMeasuremntPartsCmTxt = null;
  }
}
