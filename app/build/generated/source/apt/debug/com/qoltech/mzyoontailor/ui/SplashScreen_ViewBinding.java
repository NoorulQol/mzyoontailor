// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SplashScreen_ViewBinding implements Unbinder {
  private SplashScreen target;

  @UiThread
  public SplashScreen_ViewBinding(SplashScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SplashScreen_ViewBinding(SplashScreen target, View source) {
    this.target = target;

    target.mSplashViewGroup = Utils.findRequiredViewAsType(source, R.id.splash_par_lay, "field 'mSplashViewGroup'", ViewGroup.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SplashScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSplashViewGroup = null;
  }
}
