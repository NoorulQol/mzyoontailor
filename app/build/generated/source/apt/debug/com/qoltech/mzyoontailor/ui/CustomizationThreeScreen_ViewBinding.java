// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizationThreeScreen_ViewBinding implements Unbinder {
  private CustomizationThreeScreen target;

  private View view2131296934;

  private View view2131296692;

  private View view2131296690;

  private View view2131296696;

  private View view2131296700;

  @UiThread
  public CustomizationThreeScreen_ViewBinding(CustomizationThreeScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CustomizationThreeScreen_ViewBinding(final CustomizationThreeScreen target, View source) {
    this.target = target;

    View view;
    target.mCustomizationThreeParLay = Utils.findRequiredViewAsType(source, R.id.customization_three_par_lay, "field 'mCustomizationThreeParLay'", RelativeLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mCustomizatinThreeRecyclerView = Utils.findRequiredViewAsType(source, R.id.customize_three_recycler_view, "field 'mCustomizatinThreeRecyclerView'", RecyclerView.class);
    target.mCustomizationThreeSpinnerTxt = Utils.findRequiredViewAsType(source, R.id.customization_three_spinner_txt, "field 'mCustomizationThreeSpinnerTxt'", TextView.class);
    target.mCustomizationThreeFrontImg = Utils.findRequiredViewAsType(source, R.id.customization_three_front_img, "field 'mCustomizationThreeFrontImg'", ImageView.class);
    target.mCustomizationThreeBackImg = Utils.findRequiredViewAsType(source, R.id.customization_three_back_img, "field 'mCustomizationThreeBackImg'", ImageView.class);
    target.mCustomizationThreeFullImg = Utils.findRequiredViewAsType(source, R.id.customization_three_full_img, "field 'mCustomizationThreeFullImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.customization_three_front_img_lay, "field 'mCustomizationThreeFrontImgLay' and method 'onClick'");
    target.mCustomizationThreeFrontImgLay = Utils.castView(view, R.id.customization_three_front_img_lay, "field 'mCustomizationThreeFrontImgLay'", RelativeLayout.class);
    view2131296692 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.customization_three_back_img_lay, "field 'mCustomizationThreeBackImgLay' and method 'onClick'");
    target.mCustomizationThreeBackImgLay = Utils.castView(view, R.id.customization_three_back_img_lay, "field 'mCustomizationThreeBackImgLay'", RelativeLayout.class);
    view2131296690 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.customization_three_next_btn, "field 'mCustomizationNextBtn' and method 'onClick'");
    target.mCustomizationNextBtn = Utils.castView(view, R.id.customization_three_next_btn, "field 'mCustomizationNextBtn'", RelativeLayout.class);
    view2131296696 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.customization_three_spinner_lay, "method 'onClick'");
    view2131296700 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizationThreeScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mCustomizationThreeParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mCustomizatinThreeRecyclerView = null;
    target.mCustomizationThreeSpinnerTxt = null;
    target.mCustomizationThreeFrontImg = null;
    target.mCustomizationThreeBackImg = null;
    target.mCustomizationThreeFullImg = null;
    target.mCustomizationThreeFrontImgLay = null;
    target.mCustomizationThreeBackImgLay = null;
    target.mCustomizationNextBtn = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296692.setOnClickListener(null);
    view2131296692 = null;
    view2131296690.setOnClickListener(null);
    view2131296690 = null;
    view2131296696.setOnClickListener(null);
    view2131296696 = null;
    view2131296700.setOnClickListener(null);
    view2131296700 = null;
  }
}
