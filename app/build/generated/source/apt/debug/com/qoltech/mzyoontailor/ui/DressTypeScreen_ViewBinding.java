// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DressTypeScreen_ViewBinding implements Unbinder {
  private DressTypeScreen target;

  private View view2131296934;

  private View view2131296768;

  private View view2131296775;

  private View view2131296751;

  private View view2131296808;

  private View view2131296809;

  private View view2131296813;

  private View view2131296774;

  private View view2131296769;

  @UiThread
  public DressTypeScreen_ViewBinding(DressTypeScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DressTypeScreen_ViewBinding(final DressTypeScreen target, View source) {
    this.target = target;

    View view;
    target.mDressTypeParLay = Utils.findRequiredViewAsType(source, R.id.dress_type_par_lay, "field 'mDressTypeParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mDressTypeRecyclerView = Utils.findRequiredViewAsType(source, R.id.dress_type_recycler_view, "field 'mDressTypeRecyclerView'", RecyclerView.class);
    target.mDressSortParLay = Utils.findRequiredViewAsType(source, R.id.dress_sort_par_lay, "field 'mDressSortParLay'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.dress_type_filter_lay, "field 'mDressTypeFilterLay' and method 'onClick'");
    target.mDressTypeFilterLay = Utils.castView(view, R.id.dress_type_filter_lay, "field 'mDressTypeFilterLay'", RelativeLayout.class);
    view2131296768 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.dress_type_sort_lay, "field 'mDressSortLay' and method 'onClick'");
    target.mDressSortLay = Utils.castView(view, R.id.dress_type_sort_lay, "field 'mDressSortLay'", RelativeLayout.class);
    view2131296775 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mDressFilterTxt = Utils.findRequiredViewAsType(source, R.id.dress_filter_txt, "field 'mDressFilterTxt'", TextView.class);
    target.mDressSortTxt = Utils.findRequiredViewAsType(source, R.id.dress_sort_txt, "field 'mDressSortTxt'", TextView.class);
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
    target.mSortRecyclerView = Utils.findRequiredViewAsType(source, R.id.sort_recycler_view, "field 'mSortRecyclerView'", RecyclerView.class);
    target.mDressFilterScrollView = Utils.findRequiredViewAsType(source, R.id.dress_filter_scroll_view, "field 'mDressFilterScrollView'", ScrollView.class);
    target.mFilterGenderTxt = Utils.findRequiredViewAsType(source, R.id.filter_gender_txt, "field 'mFilterGenderTxt'", TextView.class);
    target.mFilterOccasionTxt = Utils.findRequiredViewAsType(source, R.id.filter_occasion_txt, "field 'mFilterOccasionTxt'", TextView.class);
    target.mFilterRegionTxt = Utils.findRequiredViewAsType(source, R.id.filter_region_txt, "field 'mFilterRegionTxt'", TextView.class);
    target.mDressTypeEdtTxt = Utils.findRequiredViewAsType(source, R.id.dress_type_edt_txt, "field 'mDressTypeEdtTxt'", EditText.class);
    view = Utils.findRequiredView(source, R.id.dres_type_apply_btn, "method 'onClick'");
    view2131296751 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.filter_type_gender_lay, "method 'onClick'");
    view2131296808 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.filter_type_occasion_lay, "method 'onClick'");
    view2131296809 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.filter_type_region_lay, "method 'onClick'");
    view2131296813 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.dress_type_search_lay, "method 'onClick'");
    view2131296774 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.dress_type_filter_reset_btn, "method 'onClick'");
    view2131296769 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DressTypeScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDressTypeParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mDressTypeRecyclerView = null;
    target.mDressSortParLay = null;
    target.mDressTypeFilterLay = null;
    target.mDressSortLay = null;
    target.mDressFilterTxt = null;
    target.mDressSortTxt = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;
    target.mSortRecyclerView = null;
    target.mDressFilterScrollView = null;
    target.mFilterGenderTxt = null;
    target.mFilterOccasionTxt = null;
    target.mFilterRegionTxt = null;
    target.mDressTypeEdtTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131296768.setOnClickListener(null);
    view2131296768 = null;
    view2131296775.setOnClickListener(null);
    view2131296775 = null;
    view2131296751.setOnClickListener(null);
    view2131296751 = null;
    view2131296808.setOnClickListener(null);
    view2131296808 = null;
    view2131296809.setOnClickListener(null);
    view2131296809 = null;
    view2131296813.setOnClickListener(null);
    view2131296813 = null;
    view2131296774.setOnClickListener(null);
    view2131296774 = null;
    view2131296769.setOnClickListener(null);
    view2131296769 = null;
  }
}
