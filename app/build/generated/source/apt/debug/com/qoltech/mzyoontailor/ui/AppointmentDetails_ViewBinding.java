// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AppointmentDetails_ViewBinding implements Unbinder {
  private AppointmentDetails target;

  private View view2131296934;

  private View view2131297551;

  private View view2131297365;

  private View view2131296443;

  private View view2131296444;

  private View view2131296445;

  private View view2131296452;

  private View view2131296453;

  private View view2131296454;

  private View view2131297102;

  private View view2131297361;

  private View view2131297095;

  private View view2131297098;

  private View view2131297159;

  private View view2131297160;

  @UiThread
  public AppointmentDetails_ViewBinding(AppointmentDetails target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AppointmentDetails_ViewBinding(final AppointmentDetails target, View source) {
    this.target = target;

    View view;
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mAppointmentMaterialStatusTxt = Utils.findRequiredViewAsType(source, R.id.appointment_material_status_txt, "field 'mAppointmentMaterialStatusTxt'", TextView.class);
    target.mAppointmentMaterialTypeHeaderImg = Utils.findRequiredViewAsType(source, R.id.appointment_material_type_header_img, "field 'mAppointmentMaterialTypeHeaderImg'", ImageView.class);
    target.mAppointmentMaterialTypeHeaderTxt = Utils.findRequiredViewAsType(source, R.id.appointment_material_type_header_txt, "field 'mAppointmentMaterialTypeHeaderTxt'", TextView.class);
    target.mAppointmentMaterialBodyImg = Utils.findRequiredViewAsType(source, R.id.appointment_material_type_body_img, "field 'mAppointmentMaterialBodyImg'", ImageView.class);
    target.mMaterialParLay = Utils.findRequiredViewAsType(source, R.id.material_par_lay, "field 'mMaterialParLay'", LinearLayout.class);
    target.mAppointmentMeasurementStatusTxt = Utils.findRequiredViewAsType(source, R.id.appointment_measurement_status_txt, "field 'mAppointmentMeasurementStatusTxt'", TextView.class);
    target.mAppointmentMeasurementTypeImg = Utils.findRequiredViewAsType(source, R.id.appointment_measurement_type_img, "field 'mAppointmentMeasurementTypeImg'", ImageView.class);
    target.mAppointmentMeasurementTypeHeaderTxt = Utils.findRequiredViewAsType(source, R.id.appointment_measurment_type_header_txt, "field 'mAppointmentMeasurementTypeHeaderTxt'", TextView.class);
    target.mAppointmentMeasurementBodyImg = Utils.findRequiredViewAsType(source, R.id.appointment_measurement_type_body_img, "field 'mAppointmentMeasurementBodyImg'", ImageView.class);
    target.mMeasurementParLay = Utils.findRequiredViewAsType(source, R.id.measurement_par_lay, "field 'mMeasurementParLay'", LinearLayout.class);
    target.mAppointmentDetailsPayLay = Utils.findRequiredViewAsType(source, R.id.appointment_details_par_lay, "field 'mAppointmentDetailsPayLay'", LinearLayout.class);
    target.mMaterialApproveRejectLay = Utils.findRequiredViewAsType(source, R.id.material_approve_reject_lay, "field 'mMaterialApproveRejectLay'", LinearLayout.class);
    target.mMeasurementApproveRejectLay = Utils.findRequiredViewAsType(source, R.id.measurement_approve_reject_lay, "field 'mMeasurementApproveRejectLay'", LinearLayout.class);
    target.mMaterialSaveBtnLay = Utils.findRequiredViewAsType(source, R.id.material_save_btn_lay, "field 'mMaterialSaveBtnLay'", RelativeLayout.class);
    target.mMeasurementSaveBtnLay = Utils.findRequiredViewAsType(source, R.id.measurement_save_btn_lay, "field 'mMeasurementSaveBtnLay'", RelativeLayout.class);
    target.mMaterialTypeTimeSlotTxt = Utils.findRequiredViewAsType(source, R.id.material_type_time_slot_txt, "field 'mMaterialTypeTimeSlotTxt'", TextView.class);
    target.mMeasurementTypeTimeSlotTxt = Utils.findRequiredViewAsType(source, R.id.measurement_type_time_slot_txt, "field 'mMeasurementTypeTimeSlotTxt'", TextView.class);
    target.mMaterialTypeFromDateTxt = Utils.findRequiredViewAsType(source, R.id.appointment_type_fromt_date_txt, "field 'mMaterialTypeFromDateTxt'", TextView.class);
    target.mMaterialTypeToDateTxt = Utils.findRequiredViewAsType(source, R.id.appointment_material_type_to_date_txt, "field 'mMaterialTypeToDateTxt'", TextView.class);
    target.mMeasurementTypeFromDateTxt = Utils.findRequiredViewAsType(source, R.id.measurement_type_from_date_txt, "field 'mMeasurementTypeFromDateTxt'", TextView.class);
    target.mMeasurementTypeToDateTxt = Utils.findRequiredViewAsType(source, R.id.measurement_type_to_date_txt, "field 'mMeasurementTypeToDateTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.order_type_txt_lay, "field 'mOrderTypeTxtLay' and method 'onClick'");
    target.mOrderTypeTxtLay = Utils.castView(view, R.id.order_type_txt_lay, "field 'mOrderTypeTxtLay'", RelativeLayout.class);
    view2131297551 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mOrderTypeTxt = Utils.findRequiredViewAsType(source, R.id.order_type_txt, "field 'mOrderTypeTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.measurement_type_txt__lay, "field 'mMeasurementTypeTxtLay' and method 'onClick'");
    target.mMeasurementTypeTxtLay = Utils.castView(view, R.id.measurement_type_txt__lay, "field 'mMeasurementTypeTxtLay'", RelativeLayout.class);
    view2131297365 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mMeasurementTypeTxt = Utils.findRequiredViewAsType(source, R.id.measurement_type_txt, "field 'mMeasurementTypeTxt'", TextView.class);
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.appointment_material_approve_lay, "method 'onClick'");
    view2131296443 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.appointment_material_reject_lay, "method 'onClick'");
    view2131296444 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.appointment_material_save_btn, "method 'onClick'");
    view2131296445 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.appointment_measurement_approve_lay, "method 'onClick'");
    view2131296452 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.appointment_measurement_reject_lay, "method 'onClick'");
    view2131296453 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.appointment_measurement_save_btn, "method 'onClick'");
    view2131296454 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.material_type_time_slot_par_lay, "method 'onClick'");
    view2131297102 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.measurement_type_time_slot_lay, "method 'onClick'");
    view2131297361 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.material_type_calender_from_par_lay, "method 'onClick'");
    view2131297095 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.material_type_calender_to_par_lay, "method 'onClick'");
    view2131297098 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.measurement_calender_from_date_par_lay, "method 'onClick'");
    view2131297159 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.measurement_calender_to_date_par_lay, "method 'onClick'");
    view2131297160 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    AppointmentDetails target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mAppointmentMaterialStatusTxt = null;
    target.mAppointmentMaterialTypeHeaderImg = null;
    target.mAppointmentMaterialTypeHeaderTxt = null;
    target.mAppointmentMaterialBodyImg = null;
    target.mMaterialParLay = null;
    target.mAppointmentMeasurementStatusTxt = null;
    target.mAppointmentMeasurementTypeImg = null;
    target.mAppointmentMeasurementTypeHeaderTxt = null;
    target.mAppointmentMeasurementBodyImg = null;
    target.mMeasurementParLay = null;
    target.mAppointmentDetailsPayLay = null;
    target.mMaterialApproveRejectLay = null;
    target.mMeasurementApproveRejectLay = null;
    target.mMaterialSaveBtnLay = null;
    target.mMeasurementSaveBtnLay = null;
    target.mMaterialTypeTimeSlotTxt = null;
    target.mMeasurementTypeTimeSlotTxt = null;
    target.mMaterialTypeFromDateTxt = null;
    target.mMaterialTypeToDateTxt = null;
    target.mMeasurementTypeFromDateTxt = null;
    target.mMeasurementTypeToDateTxt = null;
    target.mOrderTypeTxtLay = null;
    target.mOrderTypeTxt = null;
    target.mMeasurementTypeTxtLay = null;
    target.mMeasurementTypeTxt = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297551.setOnClickListener(null);
    view2131297551 = null;
    view2131297365.setOnClickListener(null);
    view2131297365 = null;
    view2131296443.setOnClickListener(null);
    view2131296443 = null;
    view2131296444.setOnClickListener(null);
    view2131296444 = null;
    view2131296445.setOnClickListener(null);
    view2131296445 = null;
    view2131296452.setOnClickListener(null);
    view2131296452 = null;
    view2131296453.setOnClickListener(null);
    view2131296453 = null;
    view2131296454.setOnClickListener(null);
    view2131296454 = null;
    view2131297102.setOnClickListener(null);
    view2131297102 = null;
    view2131297361.setOnClickListener(null);
    view2131297361 = null;
    view2131297095.setOnClickListener(null);
    view2131297095 = null;
    view2131297098.setOnClickListener(null);
    view2131297098 = null;
    view2131297159.setOnClickListener(null);
    view2131297159 = null;
    view2131297160.setOnClickListener(null);
    view2131297160 = null;
  }
}
