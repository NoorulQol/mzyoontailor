// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubTypeScreen_ViewBinding implements Unbinder {
  private SubTypeScreen target;

  private View view2131296934;

  private View view2131297874;

  @UiThread
  public SubTypeScreen_ViewBinding(SubTypeScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SubTypeScreen_ViewBinding(final SubTypeScreen target, View source) {
    this.target = target;

    View view;
    target.mDressSubTypeParLay = Utils.findRequiredViewAsType(source, R.id.dress_sub_type_par_lay, "field 'mDressSubTypeParLay'", LinearLayout.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mEmptyListLay = Utils.findRequiredViewAsType(source, R.id.empty_list_lay, "field 'mEmptyListLay'", RelativeLayout.class);
    target.mEmptyListTxt = Utils.findRequiredViewAsType(source, R.id.empty_list_txt, "field 'mEmptyListTxt'", TextView.class);
    target.mSubTypeEdtTxt = Utils.findRequiredViewAsType(source, R.id.sub_type_edt_txt, "field 'mSubTypeEdtTxt'", EditText.class);
    target.mSubTypeRecyclerView = Utils.findRequiredViewAsType(source, R.id.sub_type_recycler_view, "field 'mSubTypeRecyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.sub_type_search_lay, "method 'onClick'");
    view2131297874 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SubTypeScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mDressSubTypeParLay = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mEmptyListLay = null;
    target.mEmptyListTxt = null;
    target.mSubTypeEdtTxt = null;
    target.mSubTypeRecyclerView = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297874.setOnClickListener(null);
    view2131297874 = null;
  }
}
