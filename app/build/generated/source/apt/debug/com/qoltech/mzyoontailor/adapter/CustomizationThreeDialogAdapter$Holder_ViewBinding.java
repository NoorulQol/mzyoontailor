// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizationThreeDialogAdapter$Holder_ViewBinding implements Unbinder {
  private CustomizationThreeDialogAdapter.Holder target;

  @UiThread
  public CustomizationThreeDialogAdapter$Holder_ViewBinding(CustomizationThreeDialogAdapter.Holder target,
      View source) {
    this.target = target;

    target.mCustomizationThreeTxtViewTxt = Utils.findRequiredViewAsType(source, R.id.customization_three_txt, "field 'mCustomizationThreeTxtViewTxt'", TextView.class);
    target.mCustimzationThreegImg = Utils.findRequiredViewAsType(source, R.id.customization_three_img, "field 'mCustimzationThreegImg'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizationThreeDialogAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mCustomizationThreeTxtViewTxt = null;
    target.mCustimzationThreegImg = null;
  }
}
