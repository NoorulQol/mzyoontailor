// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ManuallyAdapter$Holder_ViewBinding implements Unbinder {
  private ManuallyAdapter.Holder target;

  @UiThread
  public ManuallyAdapter$Holder_ViewBinding(ManuallyAdapter.Holder target, View source) {
    this.target = target;

    target.mManualTxt = Utils.findRequiredViewAsType(source, R.id.get_country_recycler_view_txt, "field 'mManualTxt'", TextView.class);
    target.mManualImg = Utils.findRequiredViewAsType(source, R.id.get_country_flag_img, "field 'mManualImg'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ManuallyAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mManualTxt = null;
    target.mManualImg = null;
  }
}
