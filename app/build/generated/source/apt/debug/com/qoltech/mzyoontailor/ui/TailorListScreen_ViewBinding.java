// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TailorListScreen_ViewBinding implements Unbinder {
  private TailorListScreen target;

  private View view2131296934;

  private View view2131297924;

  private View view2131297930;

  private View view2131297944;

  @UiThread
  public TailorListScreen_ViewBinding(TailorListScreen target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public TailorListScreen_ViewBinding(final TailorListScreen target, View source) {
    this.target = target;

    View view;
    target.mTailorRecyclerView = Utils.findRequiredViewAsType(source, R.id.tailor_recycler_view, "field 'mTailorRecyclerView'", RecyclerView.class);
    target.mHeaderTxt = Utils.findRequiredViewAsType(source, R.id.header_txt, "field 'mHeaderTxt'", TextView.class);
    target.mRightSideImg = Utils.findRequiredViewAsType(source, R.id.header_right_side_img, "field 'mRightSideImg'", ImageView.class);
    view = Utils.findRequiredView(source, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn' and method 'onClick'");
    target.mHeaderLeftBackBtn = Utils.castView(view, R.id.header_left_side_img, "field 'mHeaderLeftBackBtn'", ImageView.class);
    view2131296934 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mTailorSelectedTxt = Utils.findRequiredViewAsType(source, R.id.tailor_selected_txt, "field 'mTailorSelectedTxt'", TextView.class);
    target.mTailorListViewLay = Utils.findRequiredViewAsType(source, R.id.tailor_list_view_lay, "field 'mTailorListViewLay'", RelativeLayout.class);
    target.mTailorMapViewLay = Utils.findRequiredViewAsType(source, R.id.tailor_map_view_lay, "field 'mTailorMapViewLay'", RelativeLayout.class);
    view = Utils.findRequiredView(source, R.id.tailor_list_lay, "field 'mTailorListlay' and method 'onClick'");
    target.mTailorListlay = Utils.castView(view, R.id.tailor_list_lay, "field 'mTailorListlay'", RelativeLayout.class);
    view2131297924 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tailor_map_lay, "field 'mTailorMapLay' and method 'onClick'");
    target.mTailorMapLay = Utils.castView(view, R.id.tailor_map_lay, "field 'mTailorMapLay'", RelativeLayout.class);
    view2131297930 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.mTailorListTxt = Utils.findRequiredViewAsType(source, R.id.tailor_list_txt, "field 'mTailorListTxt'", TextView.class);
    target.mTailorMapTxt = Utils.findRequiredViewAsType(source, R.id.tailor_map_txt, "field 'mTailorMapTxt'", TextView.class);
    target.mTailorListTotalListTxt = Utils.findRequiredViewAsType(source, R.id.tailor_list_total_list_txt, "field 'mTailorListTotalListTxt'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tailor_send_req_btn, "field 'mTailorSendReqBtn' and method 'onClick'");
    target.mTailorSendReqBtn = Utils.castView(view, R.id.tailor_send_req_btn, "field 'mTailorSendReqBtn'", Button.class);
    view2131297944 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    TailorListScreen target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTailorRecyclerView = null;
    target.mHeaderTxt = null;
    target.mRightSideImg = null;
    target.mHeaderLeftBackBtn = null;
    target.mTailorSelectedTxt = null;
    target.mTailorListViewLay = null;
    target.mTailorMapViewLay = null;
    target.mTailorListlay = null;
    target.mTailorMapLay = null;
    target.mTailorListTxt = null;
    target.mTailorMapTxt = null;
    target.mTailorListTotalListTxt = null;
    target.mTailorSendReqBtn = null;

    view2131296934.setOnClickListener(null);
    view2131296934 = null;
    view2131297924.setOnClickListener(null);
    view2131297924 = null;
    view2131297930.setOnClickListener(null);
    view2131297930 = null;
    view2131297944.setOnClickListener(null);
    view2131297944 = null;
  }
}
