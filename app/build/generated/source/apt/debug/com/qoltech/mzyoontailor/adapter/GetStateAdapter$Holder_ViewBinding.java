// Generated code from Butter Knife. Do not modify!
package com.qoltech.mzyoontailor.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.qoltech.mzyoontailor.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GetStateAdapter$Holder_ViewBinding implements Unbinder {
  private GetStateAdapter.Holder target;

  @UiThread
  public GetStateAdapter$Holder_ViewBinding(GetStateAdapter.Holder target, View source) {
    this.target = target;

    target.mGetCountryTxtViewTxt = Utils.findRequiredViewAsType(source, R.id.get_country_recycler_view_txt, "field 'mGetCountryTxtViewTxt'", TextView.class);
    target.mCountryFlagImg = Utils.findRequiredViewAsType(source, R.id.get_country_flag_img, "field 'mCountryFlagImg'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GetStateAdapter.Holder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mGetCountryTxtViewTxt = null;
    target.mCountryFlagImg = null;
  }
}
