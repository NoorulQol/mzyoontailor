package com.qoltech.mzyoontailor.ui;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GenderAdapter;
import com.qoltech.mzyoontailor.entity.GenderEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GenderResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GenderScreen extends BaseActivity {

    private GenderAdapter mGenderAdapter;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.gender_par_lay)
    LinearLayout mGenderParLay;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private ArrayList<GenderEntity> mGenderList;

    @BindView(R.id.gender_recycler_view)
    RecyclerView mGenderRecyclerView;
    @BindView(R.id.gender_wizard)
    RelativeLayout gender_wizard_layout;
    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_gender_screen);

        initView();

    }

    /*InitViews*/
    @SuppressLint("ResourceAsColor")
    private void initView() {

        ButterKnife.bind(this);

        setupUI(mGenderParLay);

        setHeader();

        getGenderApiCall();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(GenderScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (AppConstants.GENDER_SCREEN_SKILLUPDATE.equalsIgnoreCase("")) {
            gender_wizard_layout.setVisibility(View.VISIBLE);


        } else {
            gender_wizard_layout.setVisibility(View.INVISIBLE);
        }

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.gender);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    public void getGenderApiCall() {

        if (NetworkUtil.isNetworkAvailable(GenderScreen.this)) {
            APIRequestHandler.getInstance().getGenderAPICall(GenderScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(GenderScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getGenderApiCall();
                }
            });
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<GenderEntity> genderList) {

        if (mGenderAdapter == null) {

            mGenderAdapter = new GenderAdapter(this, genderList);
            mGenderRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                mGenderRecyclerView.setScaleX(-1);
            }
            else {
                mGenderRecyclerView.setScaleX(1);

            }
            mGenderRecyclerView.setAdapter(mGenderAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGenderAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GenderResponse) {
            GenderResponse mResponse = (GenderResponse) resObj;

            mGenderList = new ArrayList<>();
            mGenderList.addAll(mResponse.getResult());

            setAdapter(mGenderList);
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.gender_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.gender_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AppConstants.GENDER_SCREEN_SKILLUPDATE = "";
    }
}
