package com.qoltech.mzyoontailor.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.DressTypeAdapter;
import com.qoltech.mzyoontailor.adapter.FilterTypeAdapter;
import com.qoltech.mzyoontailor.entity.DressTypeEntity;
import com.qoltech.mzyoontailor.entity.FilterTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.DressTypeResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DressTypeScreen extends BaseActivity {

    private DressTypeAdapter mDressTypeAdapter;

    @BindView(R.id.dress_type_par_lay)
    LinearLayout mDressTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private ArrayList<DressTypeEntity> mDressTypeList;

    @BindView(R.id.dress_type_recycler_view)
    RecyclerView mDressTypeRecyclerView;

    @BindView(R.id.dress_sort_par_lay)
    RelativeLayout mDressSortParLay;

    @BindView(R.id.dress_type_filter_lay)
    RelativeLayout mDressTypeFilterLay;

    @BindView(R.id.dress_type_sort_lay)
    RelativeLayout mDressSortLay;

    @BindView(R.id.dress_filter_txt)
    TextView mDressFilterTxt;

    @BindView(R.id.dress_sort_txt)
    TextView mDressSortTxt;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.sort_recycler_view)
    RecyclerView mSortRecyclerView;

    @BindView(R.id.dress_filter_scroll_view)
    ScrollView mDressFilterScrollView;

    ArrayList<FilterTypeEntity> mFilterEntity;

    private FilterTypeAdapter mFilterTypeAdapter;

    @BindView(R.id.filter_gender_txt)
    TextView mFilterGenderTxt;

    @BindView(R.id.filter_occasion_txt)
    TextView mFilterOccasionTxt;

    @BindView(R.id.filter_region_txt)
    TextView mFilterRegionTxt;

    @BindView(R.id.dress_type_edt_txt)
    EditText mDressTypeEdtTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_dress_type_screen);

        initView();

    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setupUI(mDressTypeParLay);

        setHeader();

        getDressTypeApiCall();

        mDressTypeList = new ArrayList<>();

        mFilterEntity = new ArrayList<>();


        mDressTypeEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(DressTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<DressTypeEntity> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (DressTypeEntity s : mDressTypeList) {
            //if the existing elements contains the search input
            if (s.getDressTypeInEnglish().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        mDressTypeAdapter.filterList(filterdNames);
        mDressTypeRecyclerView.setVisibility(filterdNames.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(filterdNames.size() > 0 ? View.GONE : View.VISIBLE);

    }
    @OnClick({R.id.dress_type_filter_lay, R.id.dress_type_sort_lay,R.id.header_left_side_img,R.id.dres_type_apply_btn,R.id.filter_type_gender_lay,R.id.filter_type_occasion_lay,R.id.filter_type_region_lay,R.id.dress_type_search_lay,R.id.dress_type_filter_reset_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dress_type_filter_lay:

                mDressTypeFilterLay.setBackgroundColor(this.getResources().getColor(R.color.slider_blue));
                mDressSortLay.setBackgroundColor(this.getResources().getColor(R.color.grey_clr));

                mDressFilterTxt.setTextColor(this.getResources().getColor(R.color.white));
                mDressSortTxt.setTextColor(this.getResources().getColor(R.color.black));

                mDressSortParLay.setVisibility(View.VISIBLE);
                mDressTypeRecyclerView.setVisibility(View.GONE);
                mDressFilterScrollView.setVisibility(View.VISIBLE);
                mSortRecyclerView.setVisibility(View.GONE);
                break;
            case R.id.dress_type_sort_lay:

                setDressSortData();
                AppConstants.FILTER_TYPE = "SORT";
                mDressSortLay.setBackgroundColor(this.getResources().getColor(R.color.slider_blue));
                mDressTypeFilterLay.setBackgroundColor(this.getResources().getColor(R.color.grey_clr));

                mDressFilterTxt.setTextColor(this.getResources().getColor(R.color.black));
                mDressSortTxt.setTextColor(this.getResources().getColor(R.color.white));

//                mDressTypeRecyclerView.setVisibility(mDressTypeList.size()>0 ? View.VISIBLE : View.GONE);
//                mEmptyListLay.setVisibility(mDressTypeList.size() > 0 ? View.GONE : View.VISIBLE);
                mDressSortParLay.setVisibility(View.VISIBLE);
                mDressTypeRecyclerView.setVisibility(View.GONE);
                mDressFilterScrollView.setVisibility(View.GONE);
                mSortRecyclerView.setVisibility(View.VISIBLE);


                break;
            case R.id.dres_type_apply_btn:
                mDressTypeFilterLay.setBackgroundColor(this.getResources().getColor(R.color.slider_blue));
                mDressSortLay.setBackgroundColor(this.getResources().getColor(R.color.grey_clr));

                mDressFilterTxt.setTextColor(this.getResources().getColor(R.color.white));
                mDressSortTxt.setTextColor(this.getResources().getColor(R.color.black));

                mDressTypeRecyclerView.setVisibility(mDressTypeList.size()>0 ? View.VISIBLE : View.GONE);
                mEmptyListLay.setVisibility(mDressTypeList.size() > 0 ? View.GONE : View.VISIBLE);
                mDressSortParLay.setVisibility(View.GONE);
                if (!AppConstants.FILTER_FLAG.equalsIgnoreCase("")){
                    compareAscendingtoDesending();
                }
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.filter_type_gender_lay:
                AppConstants.FILTER_TYPE = "GENDER";
                nextScreen(FilterTypeScreen.class,true);
                break;
            case R.id.filter_type_occasion_lay:
                AppConstants.FILTER_TYPE = "OCCASION";
                nextScreen(FilterTypeScreen.class,true);
                break;
        /*    case R.id.filter_type_price_lay:
                AppConstants.FILTER_TYPE = "PRICE";
                nextScreen(FilterTypeScreen.class,true);
                break;*/
            case R.id.filter_type_region_lay:
                AppConstants.FILTER_TYPE = "REGION";
                nextScreen(FilterTypeScreen.class,true);
                break;
            case R.id.dress_type_search_lay:
                mDressTypeEdtTxt.setEnabled(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mDressTypeEdtTxt, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.dress_type_filter_reset_btn:
                AppConstants.NEW_FILTER_GENDER = "";
                AppConstants.OLD_FILTER_OCCASION = "";
                AppConstants.OLD_FILTER_REGION = "";
                AppConstants.OLD_FILTER_REGION_LIST = new ArrayList<>();
                AppConstants.OLD_FILTER_OCCASION_LIST = new ArrayList<>();
                setHeader();
                break;
        }

    }

    public void getDressTypeApiCall(){
        if (NetworkUtil.isNetworkAvailable(DressTypeScreen.this)){
            APIRequestHandler.getInstance().getDressTypeAPICall(DressTypeScreen.this,AppConstants.GENDER_ID);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(DressTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getDressTypeApiCall();
                }
            });
        }
    }
    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<DressTypeEntity> mDressTypeList) {

//        if (mDressTypeAdapter == null) {

        mDressTypeRecyclerView.setVisibility(mDressTypeList.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(mDressTypeList.size() > 0 ? View.GONE : View.VISIBLE);

        mDressTypeAdapter = new DressTypeAdapter(this,mDressTypeList);
        mDressTypeRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        mDressTypeRecyclerView.setAdapter(mDressTypeAdapter);
//        }
//        else {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mDressTypeAdapter.notifyDataSetChanged();
//                }
//            });
//        }

    }

    /*Set Adapter for the Recycler view*/
    public void setDressSortAdapter(ArrayList<FilterTypeEntity> mFilterList) {

        if (mFilterTypeAdapter == null) {

            mFilterTypeAdapter = new FilterTypeAdapter(this,mFilterList);
            mSortRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mSortRecyclerView.setAdapter(mFilterTypeAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mFilterTypeAdapter.notifyDataSetChanged();
                }
            });
        }

    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.dress_type).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

        if (AppConstants.OLD_FILTER_OCCASION_LIST.size()>0){
            for (int i=0; i<AppConstants.OLD_FILTER_OCCASION_LIST.size(); i++){
                if (AppConstants.OLD_FILTER_OCCASION_LIST.get(i).getChecked()){
                    AppConstants.OLD_FILTER_OCCASION =AppConstants.OLD_FILTER_OCCASION + AppConstants.OLD_FILTER_OCCASION_LIST.get(i).getfilterTxt()+",";
                }
            }
        }

        if (AppConstants.OLD_FILTER_REGION_LIST.size()>0){
            for (int i=0; i<AppConstants.OLD_FILTER_REGION_LIST.size(); i++){
                if (AppConstants.OLD_FILTER_REGION_LIST.get(i).getChecked()){
                    AppConstants.OLD_FILTER_REGION =AppConstants.OLD_FILTER_REGION + AppConstants.OLD_FILTER_REGION_LIST.get(i).getfilterTxt()+",";
                }
            }
        }

        mFilterGenderTxt.setText(AppConstants.NEW_FILTER_GENDER.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_GENDER : AppConstants.NEW_FILTER_GENDER);
        mFilterOccasionTxt.setText(AppConstants.NEW_FILTER_OCCASION.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_OCCASION.equalsIgnoreCase("") ? "NONE" : AppConstants.OLD_FILTER_OCCASION : AppConstants.NEW_FILTER_OCCASION);
        mFilterRegionTxt.setText(AppConstants.NEW_FILTER_REGION.equalsIgnoreCase("") ? AppConstants.OLD_FILTER_REGION.equalsIgnoreCase("") ? "NONE" : AppConstants.OLD_FILTER_REGION : AppConstants.NEW_FILTER_REGION);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof DressTypeResponse){
            DressTypeResponse mResponse = (DressTypeResponse) resObj;

            mDressTypeList.addAll(mResponse.getResult());
            setAdapter(mDressTypeList);
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    private void setDressSortData() {
        mFilterEntity = new ArrayList<>();

        FilterTypeEntity filterList = new FilterTypeEntity("Name - Z to A", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Name - A to Z", false);
        mFilterEntity.add(filterList);

        setDressSortAdapter(mFilterEntity);

    }

    public void compareAscendingtoDesending(){
        if (AppConstants.FILTER_FLAG.equalsIgnoreCase("ATOZ")) {
            Collections.sort(mDressTypeList, new Comparator<DressTypeEntity>() {
                @Override
                public int compare(DressTypeEntity AgentNameCompare1, DressTypeEntity AgentNameCompare2) {

                    return AgentNameCompare1.getDressTypeInEnglish().compareTo(AgentNameCompare2.getDressTypeInEnglish());

                }
            });

        }else {
            Collections.sort(mDressTypeList, new Comparator<DressTypeEntity>() {
                @Override
                public int compare(DressTypeEntity AgentNameCompare1, DressTypeEntity AgentNameCompare2) {
                    return AgentNameCompare2.getDressTypeInEnglish().compareTo(AgentNameCompare1.getDressTypeInEnglish());
                }
            });

        }
        setAdapter(mDressTypeList);
        AppConstants.FILTER_FLAG = "";
    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }
}
