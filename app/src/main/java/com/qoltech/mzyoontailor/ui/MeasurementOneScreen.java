package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ManuallyAdapter;
import com.qoltech.mzyoontailor.entity.ManuallyEntity;
import com.qoltech.mzyoontailor.entity.MeasurementOneEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.ManuallyResponse;
import com.qoltech.mzyoontailor.modal.MeasurementOneResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeasurementOneScreen extends BaseActivity {

    @BindView(R.id.measurement_one_par_lay)
    LinearLayout mMeasurementOneParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.measurement_manully_img)
    ImageView mMeasurementManuallyImg;

    @BindView(R.id.measurement_tailor_img)
    ImageView mMeasurementTailorImg;

    @BindView(R.id.measurement_tailor_come_img)
    ImageView mMeasurementTailorComeImg;

    @BindView(R.id.measurement_manully_txt)
    TextView mMeasurementManulllyTxt;

    @BindView(R.id.measurement_tailor_txt)
    TextView mMeasurementTailorTxt;

    @BindView(R.id.measurement_tailor_come_to_place_lay)
    LinearLayout mMeasurementTailorComeToPlaceLay;

    @BindView(R.id.measurement_to_tailor_lay)
    LinearLayout mMeasurementToTailorLay;

    @BindView(R.id.measurement_tailor_come_txt)
    TextView mMeasurementTailorComeTxt;

    ApiService restService;

    private Dialog mManuallyDialog, mEditTxtDialog;

    private ManuallyAdapter mManuallyAdapter;

    private ArrayList<ManuallyEntity> mManualEntity;

    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;

    ArrayList<MeasurementOneEntity> mMeasurementList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_one_screen);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mMeasurementOneParLay);

        setHeader();
        restService = ((MainApplication) getApplication()).getClient();

        mManualEntity = new ArrayList<>();
        mMeasurementList = new ArrayList<>();

        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getMeasuementApiCall();
        getManuallyApiCall();

        if (AppConstants.TAILORMEASUREMENT.equalsIgnoreCase("TAILORMEASUREMENT")) {
            mMeasurementTailorComeToPlaceLay.setVisibility(View.INVISIBLE);
            mMeasurementToTailorLay.setVisibility(View.INVISIBLE);
        }

        else {

        }

        getLanguage();

    }

    @OnClick({R.id.measurement_manully_lay, R.id.measurement_to_tailor_lay, R.id.measurement_tailor_come_to_place_lay, R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.measurement_manully_lay:
                AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(0).getId());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
                    AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInArabic());

                } else {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
                    AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInEnglish());

                }
                startActivity(new Intent(getApplicationContext(), AddMeasurementScreen.class));
//                alertDismiss(mManuallyDialog);
//                mManuallyDialog = getDialog(MeasurementOneScreen.this, R.layout.pop_up_manually);
//
//                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
//                Window window = mManuallyDialog.getWindow();
//
//                if (window != null) {
//                    LayoutParams.copyFrom(window.getAttributes());
//                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
//                    LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                    window.setAttributes(LayoutParams);
//                    window.setGravity(Gravity.CENTER);
//                }
//
//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//                    AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInArabic());
//
//                } else {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//                    AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInEnglish());
//
//                }
//                TextView cancelTxt, addTxt, cancelEmptyTxt, addEmptyTxt;
//
//                RecyclerView countryRecyclerView;
//
//                LinearLayout emptyLay, addLay;
//
//
//                /*Init view*/
//                emptyLay = mManuallyDialog.findViewById(R.id.manually_empty_cancel_add_lay);
//                addLay = mManuallyDialog.findViewById(R.id.manually_cancel_add_lay);
//                cancelTxt = mManuallyDialog.findViewById(R.id.pop_up_cancel_txt);
//                addTxt = mManuallyDialog.findViewById(R.id.pop_up_new_txt);
//                cancelEmptyTxt = mManuallyDialog.findViewById(R.id.pop_up_cancel_bottom_txt);
//                addEmptyTxt = mManuallyDialog.findViewById(R.id.pop_up_new_bottom_txt);
//
//                emptyLay.setVisibility(mManualEntity.size() > 0 ? View.GONE : View.VISIBLE);
//                addLay.setVisibility(mManualEntity.size() > 0 ? View.VISIBLE : View.GONE);
//
//                countryRecyclerView = mManuallyDialog.findViewById(R.id.pop_up_manually_recycler_view);
//                mManuallyAdapter = new ManuallyAdapter(MeasurementOneScreen.this, mManualEntity, mManuallyDialog);
//
//                countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//                countryRecyclerView.setAdapter(mManuallyAdapter);
//
//                /*Set data*/
//                cancelTxt.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mManuallyDialog.dismiss();
//                    }
//                });
//                cancelEmptyTxt.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mManuallyDialog.dismiss();
//
//                    }
//                });
//                addEmptyTxt.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mManuallyDialog.dismiss();
//                        EditTextDialog();
//                    }
//                });
//                addTxt.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mManuallyDialog.dismiss();
//                        EditTextDialog();
//                    }
//                });
//
//                alertShowing(mManuallyDialog);
                break;
            case R.id.measurement_to_tailor_lay:
                AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(1).getId());
                nextScreen(AddReferenceScreen.class, true);
                break;
            case R.id.measurement_tailor_come_to_place_lay:
                AppConstants.MEASUREMENT_TYPE = String.valueOf(mMeasurementList.get(2).getId());
                nextScreen(AddReferenceScreen.class, true);
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void updateMeasurementByTailor(String orderId, String measurementId) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", orderId);
        map.put("MeasurementId", measurementId);

        restService.updateMeasurementByTailor(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                AppConstants.MEASUREMENT_ID = "";
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void getMeasuementApiCall() {

        if (NetworkUtil.isNetworkAvailable(MeasurementOneScreen.this)) {
            APIRequestHandler.getInstance().getMeasurementOneAPICall(MeasurementOneScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMeasuementApiCall();
                }
            });
        }

    }

    public void getManuallyApiCall() {
        if (NetworkUtil.isNetworkAvailable(MeasurementOneScreen.this)) {
            APIRequestHandler.getInstance().manuallyApiCall(AppConstants.SUB_DRESS_TYPE_ID, AppConstants.DIRECT_USERS_ID, MeasurementOneScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getManuallyApiCall();
                }
            });
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement_1));
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof MeasurementOneResponse) {
            MeasurementOneResponse mResponse = (MeasurementOneResponse) resObj;

            setImagesAndTitle(mResponse.getResult());
        }
        if (resObj instanceof ManuallyResponse) {
            ManuallyResponse mResponse = (ManuallyResponse) resObj;
            mManualEntity = mResponse.getResult();
        }
    }

    public void setImagesAndTitle(ArrayList<MeasurementOneEntity> MeasurementList) {

        if (MeasurementList.size() > 0) {
            mMeasurementList = MeasurementList;

            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                mMeasurementManulllyTxt.setText(MeasurementList.get(0).getMeasurementInArabic());
                mMeasurementTailorTxt.setText(MeasurementList.get(1).getMeasurementInArabic());
                mMeasurementTailorComeTxt.setText(MeasurementList.get(2).getMeasurementInArabic());
            } else {
                mMeasurementManulllyTxt.setText(MeasurementList.get(0).getMeasurementInEnglish());
                mMeasurementTailorTxt.setText(MeasurementList.get(1).getMeasurementInEnglish());
                mMeasurementTailorComeTxt.setText(MeasurementList.get(2).getMeasurementInEnglish());
            }


            try {
                Glide.with(MeasurementOneScreen.this)
                        .load(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + MeasurementList.get(0).getBodyImage())
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(mMeasurementManuallyImg);

            } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
                Log.e(AppConstants.TAG, ex.getMessage());
            }
            try {
                Glide.with(MeasurementOneScreen.this)
                        .load(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + MeasurementList.get(1).getBodyImage())
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(mMeasurementTailorImg);

            } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
                Log.e(AppConstants.TAG, ex.getMessage());
            }
            try {
                Glide.with(MeasurementOneScreen.this)
                        .load(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + MeasurementList.get(2).getBodyImage())
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(mMeasurementTailorComeImg);

            } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
                Log.e(AppConstants.TAG, ex.getMessage());
            }

        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void EditTextDialog() {
        alertDismiss(mEditTxtDialog);
        mEditTxtDialog = getDialog(MeasurementOneScreen.this, R.layout.pop_up_edit_txt);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mEditTxtDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        TextView cancelTxt, okTxt;
        final EditText manuallyEdtTxt;

        /*Init view*/

        cancelTxt = mEditTxtDialog.findViewById(R.id.pop_up_cancel_txt);
        okTxt = mEditTxtDialog.findViewById(R.id.pop_up_ok_txt);
        manuallyEdtTxt = mEditTxtDialog.findViewById(R.id.measurement_manually_edt_txt);


        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTxtDialog.dismiss();
            }
        });
        okTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (manuallyEdtTxt.getText().toString().isEmpty()) {
                    manuallyEdtTxt.setError("Enter Measurement Name");
                } else {

                    mEditTxtDialog.dismiss();
                    AppConstants.MEASUREMENT_MANUALLY = manuallyEdtTxt.getText().toString().trim();
//                    AppConstants.MEASUREMENT_ID = "-1";
                    nextScreen(MeasurementTwoScreen.class, true);
                }

            }
        });

        alertShowing(mEditTxtDialog);
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_one_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_one_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

}
