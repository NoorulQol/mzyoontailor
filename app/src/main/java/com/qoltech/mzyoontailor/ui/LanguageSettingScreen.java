package com.qoltech.mzyoontailor.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LanguageSettingScreen extends BaseActivity {

    @BindView(R.id.languauge_setting_par_lay)
    LinearLayout mLanguageSettingScreenParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.language_arabic_img)
    ImageView mLanguageArabicImg;

    @BindView(R.id.language_english_img)
    ImageView mLanguageEnglishImg;

    @BindView(R.id.language_english_txt)
    TextView mLanguageEnglishTxt;

    @BindView(R.id.language_arabic_txt)
    TextView mLanguageArabicTxt;

    private UserDetailsEntity mUserDetailsEntity;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_language_setting_screen);
        initView();
    }

    public void initView() {
        ButterKnife.bind(this);

        setupUI(mLanguageSettingScreenParLay);

        setHeader();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(LanguageSettingScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getLanguageDone();

        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.choose_your_language));
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    @OnClick({R.id.header_left_side_img, R.id.language_english_par_lay, R.id.language_arabic_par_lay, R.id.select_lang_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.language_english_par_lay:

                mUserDetailsEntity.setLanguage("English");

                mLanguageEnglishImg.setImageDrawable(getResources().getDrawable(R.drawable.checked_icon));
                mLanguageArabicImg.setImageDrawable(getResources().getDrawable(R.drawable.unchecked_icon));

                break;
            case R.id.language_arabic_par_lay:

                mUserDetailsEntity.setLanguage("Arabic");

                mLanguageEnglishImg.setImageDrawable(getResources().getDrawable(R.drawable.unchecked_icon));
                mLanguageArabicImg.setImageDrawable(getResources().getDrawable(R.drawable.checked_icon));

                break;
            case R.id.select_lang_btn:
                PreferenceUtil.storeUserDetails(LanguageSettingScreen.this, mUserDetailsEntity);
                InsertLanguageApiCall("Tailor");
                nextScreen(HomeActivity.class, true);
                getLanguageDone();
                break;

        }

    }

    public void getLanguageDone() {

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
            mLanguageEnglishImg.setImageDrawable(getResources().getDrawable(R.drawable.unchecked_icon));
            mLanguageArabicImg.setImageDrawable(getResources().getDrawable(R.drawable.checked_icon));

            updateResources(LanguageSettingScreen.this, "ar");

        } else {
            mLanguageEnglishImg.setImageDrawable(getResources().getDrawable(R.drawable.checked_icon));
            mLanguageArabicImg.setImageDrawable(getResources().getDrawable(R.drawable.unchecked_icon));

            updateResources(LanguageSettingScreen.this, "en");
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
            mLanguageEnglishImg.setImageDrawable(getResources().getDrawable(R.drawable.unchecked_icon));
            mLanguageArabicImg.setImageDrawable(getResources().getDrawable(R.drawable.checked_icon));

            ViewCompat.setLayoutDirection(findViewById(R.id.languauge_setting_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            updateResources(LanguageSettingScreen.this, "ar");

        } else {
            mLanguageEnglishImg.setImageDrawable(getResources().getDrawable(R.drawable.checked_icon));
            mLanguageArabicImg.setImageDrawable(getResources().getDrawable(R.drawable.unchecked_icon));

            ViewCompat.setLayoutDirection(findViewById(R.id.languauge_setting_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(LanguageSettingScreen.this, "en");
        }

    }

    public void InsertLanguageApiCall(String tailor_or_buyer) {
        if (NetworkUtil.isNetworkAvailable(LanguageSettingScreen.this)) {
            APIRequestHandler.getInstance().insertLanguageApiCall(sharedPreferences.getString("TailorId", ""),
                    mUserDetailsEntity.getLanguage(),tailor_or_buyer, LanguageSettingScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(LanguageSettingScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    InsertLanguageApiCall("Tailor");
                }
            });
        }
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }


}
