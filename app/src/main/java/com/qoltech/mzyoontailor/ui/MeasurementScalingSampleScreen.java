package com.qoltech.mzyoontailor.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.utils.AppConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeasurementScalingSampleScreen  extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.number_picker)
    NumberPicker mNumberPicker;

    @BindView(R.id.scale_cm_txt)
    TextView mScaleCmTxt;

    @BindView(R.id.measurement_scale_body_img)
    ImageView mMeasurementScalseBodyImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_scalling_screen);

        initView();
    }
    public void initView() {

        ButterKnife.bind(this);

        setHeader();


        final String[] values= {"29","28", "27", "26", "25","24","23","22","21","20","19","18","17","16","15","14","13","12","11","10","09","08","07","06","05","04","03","02","01"};

        mNumberPicker.setMinValue(0); //from array first value
        //Specify the maximum value/number of NumberPicker
        mNumberPicker.setMaxValue(values.length-1); //to array last value

        //Specify the NumberPicker data source as array elements
        mNumberPicker.setDisplayedValues(values);

        //Gets whether the selector wheel wraps when reaching the min/max value.
        mNumberPicker.setWrapSelectorWheel(true);

        mNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected value from picker
//                tv.setText("Selected value : " + values[newVal]);
//                AppConstants.SelectedNum = values[newVal];
                mScaleCmTxt.setText(values[newVal] +  " CM");
            }
        });

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img,R.id.scale_measurement_ok_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.scale_measurement_ok_btn:
                onBackPressed();

                break;


        }

    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText("Measurement");
        mRightSideImg.setVisibility(View.VISIBLE);

        try {
            Glide.with(MeasurementScalingSampleScreen.this)
                    .load(AppConstants.BASE_URL+"images/Measurement2/"+AppConstants.SelectedNum)
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(mMeasurementScalseBodyImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
    }

}