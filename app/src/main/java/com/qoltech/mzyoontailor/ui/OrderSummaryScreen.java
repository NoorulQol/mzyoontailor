package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.OrderConfirmationActivity;
import com.qoltech.mzyoontailor.adapter.OrderApprovalTailorAdapter;
import com.qoltech.mzyoontailor.adapter.OrderCustomizationAdapter;
import com.qoltech.mzyoontailor.entity.ApicallidEntity;
import com.qoltech.mzyoontailor.entity.InsertMeasurmentValueEntity;
import com.qoltech.mzyoontailor.entity.OrderCustomizationIdValueEntity;
import com.qoltech.mzyoontailor.entity.OrderSummaryMaterialImageEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.entity.UserMeasurementValueEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueResponse;
import com.qoltech.mzyoontailor.modal.OrderSummaryApiCallModal;
import com.qoltech.mzyoontailor.modal.OrderSummaryResponse;
import com.qoltech.mzyoontailor.modal.ViewDetailsNewFlowResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderSummaryScreen extends BaseActivity {

    @BindView(R.id.order_summary_par_lay)
    LinearLayout mOrderSummaryParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.order_approval_gender_txt)
    TextView mOrderApprovalGenderTxt;

    @BindView(R.id.order_approval_dress_type_txt)
    TextView mOrderApprovalDressTypeTxt;

    @BindView(R.id.order_approval_sub_type_txt)
    TextView mOrderApprovalSubTypeTxt;

    @BindView(R.id.order_approval_seasonal_txt)
    TextView mOrderApprovalSeasonalTxt;

    @BindView(R.id.order_approval_place_txt)
    TextView mOrderApprovalPlaceTxt;

    @BindView(R.id.order_approval_brands_txt)
    TextView mOrderApprovalBrandsTxt;

    @BindView(R.id.order_approval_color_txt)
    TextView mOrderApprovalColorTxt;

    @BindView(R.id.order_approval_material_txt)
    TextView mOrderApprovalMaterialTxt;

    @BindView(R.id.order_approval_pattern_txt)
    TextView mOrderApprovalPatternTxt;

    @BindView(R.id.order_approval_customization_rec_view)
    RecyclerView mOrderApprovalCustomizationRecView;

    @BindView(R.id.order_approval_tailor_rec_view)
    RecyclerView mOrderApprovalTailorRecView;

    @BindView(R.id.order_approval_tailor_list_count)
    TextView mOrderApprovalTailorListCount;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.customization_one_two_txt)
    TextView mCustomizationOneTwoTxt;

    @BindView(R.id.customization_one_two_par_lay)
    LinearLayout mCustomizationOneTwoParLay;

    private OrderApprovalTailorAdapter mCustomizationTailorAdapter;
    //    private OrderApprovalCustomizationAdapter mCustomizationAdapter;
    private OrderCustomizationAdapter mCustomizationAdapter;

    SharedPreferences sharedPreferences;


    @BindView(R.id.summary_measurement_txt)
    TextView mSummaryMeasurementTxt;

    @BindView(R.id.summary_service_type_txt)
    TextView mSummaryServiceTypeTxt;
    ApiService restService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_summary);
        restService = ((MainApplication) getApplication()).getClient();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mOrderSummaryParLay);


        mUserDetailsEntityRes = new UserDetailsEntity();


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

    }

    @OnClick({R.id.header_left_side_img, R.id.order_summary_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.order_summary_submit:
                getOrderSummaryOrders();
//                if (AppConstants.MEASUREMENT_MAP.size()>0){
////                    insertMeasaurementValue();
//                }
                break;
        }

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mRightSideImg.setVisibility(View.VISIBLE);

        mOrderApprovalGenderTxt.setText(AppConstants.GENDER_NAME);
        mOrderApprovalDressTypeTxt.setText(AppConstants.DRESS_TYPE_NAME);
        mOrderApprovalSubTypeTxt.setText(AppConstants.DRESS_SUB_TYPE_NAME);
        getNewFlowViewDetailApiCall();
//        mOrderApprovalSeasonalTxt.setText(AppConstants.SEASONAL_NAME);
//        mOrderApprovalPlaceTxt.setText(AppConstants.PLACE_OF_INDUSTRY_NAME);
//        mOrderApprovalBrandsTxt.setText(AppConstants.BRANDS_NAME);
//        mOrderApprovalColorTxt.setText(AppConstants.COLOUR_NAME);
//        mOrderApprovalMaterialTxt.setText(AppConstants.MATERIAL_TYPE_NAME);
//        mOrderApprovalPatternTxt.setText(AppConstants.PATTERN_NAME);


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            mSummaryMeasurementTxt.setText("يدويا");

        } else {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            mSummaryMeasurementTxt.setText("Manually");

        }


        mSummaryServiceTypeTxt.setText(AppConstants.SERVICE_TYPE_NAME);
//        mOrderApprovalTailorListCount.setText(getResources().getString(R.string.total_num_of_tailor).toUpperCase()+
//                "("+String.valueOf(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size())+")");

        if (AppConstants.PATTERN_ID.equalsIgnoreCase("0")) {
            mCustomizationOneTwoTxt.setVisibility(View.GONE);
            mCustomizationOneTwoParLay.setVisibility(View.GONE);
        }
        else {
            mCustomizationOneTwoTxt.setVisibility(View.VISIBLE);
            mCustomizationOneTwoParLay.setVisibility(View.VISIBLE);
        }

        setCustomizationAdapter();
        setTailorAdapter();
    }

    /*Set Adapter for the Recycler view*/
    public void setCustomizationAdapter() {

        if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.size() > 0) {

            mCustomizationAdapter = new OrderCustomizationAdapter(this, AppConstants.CUSTOMIZATION_CLICK_ARRAY);
            mOrderApprovalCustomizationRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mOrderApprovalCustomizationRecView.setAdapter(mCustomizationAdapter);

        }
    }

    /*Set Adapter for the Recycler view*/
    public void setTailorAdapter() {

        if (AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size() > 0) {

            mCustomizationTailorAdapter = new OrderApprovalTailorAdapter(this, AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST);
            mOrderApprovalTailorRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mOrderApprovalTailorRecView.setAdapter(mCustomizationTailorAdapter);

        }


    }

    public void getOrderSummaryOrders() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {

            AppConstants.USER_ID_STR = mUserDetailsEntityRes.getUSER_ID();
            ArrayList<OrderSummaryMaterialImageEntity> mOrderSummaMaterialEntity = new ArrayList<>();
            ArrayList<OrderSummaryMaterialImageEntity> mOrderSummaReferenceEntity = new ArrayList<>();

            for (int i = 0; i < AppConstants.MATERIAL_IMAGES.size(); i++) {
                OrderSummaryMaterialImageEntity orderSummaryMaterialImageEntity = new OrderSummaryMaterialImageEntity();

                orderSummaryMaterialImageEntity.setImage(AppConstants.MATERIAL_IMAGES.get(i));
                mOrderSummaMaterialEntity.add(orderSummaryMaterialImageEntity);

            }

            for (int i = 0; i < AppConstants.REFERENCE_IMAGES.size(); i++) {
                OrderSummaryMaterialImageEntity orderSummaryReferenceImageEntity = new OrderSummaryMaterialImageEntity();

                orderSummaryReferenceImageEntity.setImage(AppConstants.REFERENCE_IMAGES.get(i));
                mOrderSummaReferenceEntity.add(orderSummaryReferenceImageEntity);

            }


            ArrayList<OrderCustomizationIdValueEntity> orderCustomizationIdValueEntities = new ArrayList<>();

            for (int i = 0; i < AppConstants.CUSTOMIZATION_CLICK_ARRAY.size(); i++) {
                OrderCustomizationIdValueEntity orderCustomizationIdValueEntity = new OrderCustomizationIdValueEntity();

                orderCustomizationIdValueEntity.setAttributeImageId(AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getId());
                orderCustomizationIdValueEntity.setCustomizationAttributeId(AppConstants.CUSTOMIZATION_CLICK_ARRAY.get(i).getCustomizationName());
                orderCustomizationIdValueEntities.add(orderCustomizationIdValueEntity);

            }


            ArrayList<UserMeasurementValueEntity> userMeasurementValueEntities = new ArrayList<>();

            Set keys = AppConstants.MEASUREMENT_MAP.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while (itr.hasNext()) {
                UserMeasurementValueEntity userMeasurementValueEntity = new UserMeasurementValueEntity();

                key = (String) itr.next();
                value = (String) AppConstants.MEASUREMENT_MAP.get(key);
                System.out.println(key + " - " + value);
                userMeasurementValueEntity.setUserMeasurementId(String.valueOf(key));
                userMeasurementValueEntity.setValue(value);
                userMeasurementValueEntities.add(userMeasurementValueEntity);
            }


            ArrayList<ApicallidEntity> apicallidEntities = new ArrayList<>();

            for (int i = 0; i < 1; i++) {
                ApicallidEntity apicallidEntity = new ApicallidEntity();

                apicallidEntity.setId(sharedPreferences.getString("TailorId", ""));
                apicallidEntities.add(apicallidEntity);

            }


            OrderSummaryApiCallModal orderSummaryApiCallModal = new OrderSummaryApiCallModal();
            orderSummaryApiCallModal.setDressType(AppConstants.SUB_DRESS_TYPE_ID);
            orderSummaryApiCallModal.setCustomerId(AppConstants.DIRECT_USERS_ID);
            orderSummaryApiCallModal.setAddressId(AppConstants.GET_ADDRESS_ID);
            orderSummaryApiCallModal.setPatternId(AppConstants.PATTERN_ID);
            orderSummaryApiCallModal.setOrdertype(AppConstants.ORDER_TYPE_ID);
            orderSummaryApiCallModal.setType("Direct");
            orderSummaryApiCallModal.setMeasurementId(AppConstants.MEASUREMENT_ID.
                    equalsIgnoreCase("") ? "-1" :
                    AppConstants.MEASUREMENT_ID);
            orderSummaryApiCallModal.setMaterialImage(mOrderSummaMaterialEntity);
            orderSummaryApiCallModal.setReferenceImage(mOrderSummaReferenceEntity);
            orderSummaryApiCallModal.setOrderCustomization(orderCustomizationIdValueEntities);
            orderSummaryApiCallModal.setUserMeasurementValue(userMeasurementValueEntities);
            orderSummaryApiCallModal.setMeasurementBy("Tailor");
            orderSummaryApiCallModal.setCreatedBy(AppConstants.DIRECT_USERS_ID);
            orderSummaryApiCallModal.setMeasurementName(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);
            orderSummaryApiCallModal.setTailorId(apicallidEntities);
            orderSummaryApiCallModal.setDeliveryTypeId(AppConstants.DELIVERY_TYPE_ID);
//            orderSummaryApiCallModal.setDeliveryTypeId("1");
            orderSummaryApiCallModal.setMeasurmentType(AppConstants.MEASUREMENT_TYPE);
            orderSummaryApiCallModal.setUnits(AppConstants.UNITS);


            APIRequestHandler.getInstance().orderSummary(orderSummaryApiCallModal, OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getOrderSummaryOrders();
                }
            });
        }
    }

    public void getNewFlowViewDetailApiCall() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            APIRequestHandler.getInstance().getNewFlowViewDetailsApiCall(sharedPreferences.getString("TailorId", ""), AppConstants.PATTERN_ID, OrderSummaryScreen.this);

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowViewDetailApiCall();
                }
            });
        }
    }

    public void insertMeasaurementValue() {
        if (NetworkUtil.isNetworkAvailable(OrderSummaryScreen.this)) {
            AppConstants.USER_ID_STR = mUserDetailsEntityRes.getUSER_ID();

            ArrayList<InsertMeasurmentValueEntity> mResponse = new ArrayList<>();

            Set keys = AppConstants.MEASUREMENT_MAP.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while (itr.hasNext()) {
                InsertMeasurmentValueEntity measurmentValueEntity = new InsertMeasurmentValueEntity();

                key = (String) itr.next();
                value = (String) AppConstants.MEASUREMENT_MAP.get(key);
                System.out.println(key + " - " + value);
                measurmentValueEntity.setMeasurementId(String.valueOf(key));
                measurmentValueEntity.setValue(value);
                mResponse.add(measurmentValueEntity);
            }


            InsertMeasurementValueModal measurementValueModal = new InsertMeasurementValueModal();
            measurementValueModal.setUserId(mUserDetailsEntityRes.getUSER_ID());
            measurementValueModal.setDressTypeId(AppConstants.SUB_DRESS_TYPE_ID);
            measurementValueModal.setMeasurementValue(mResponse);
            measurementValueModal.setUnits(AppConstants.UNITS);

            measurementValueModal.setMeasurementBy(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);
            measurementValueModal.setCreatedBy("Customer");
            measurementValueModal.setName(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);

            APIRequestHandler.getInstance().insertMeasurementValue(measurementValueModal, OrderSummaryScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertMeasaurementValue();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderSummaryResponse) {
            OrderSummaryResponse mResponse = (OrderSummaryResponse) resObj;

            Toast.makeText(OrderSummaryScreen.this, "Your order Id is : " + mResponse.getResult(), Toast.LENGTH_SHORT).show();
//         <--Start-->  For New Flow
            AppConstants.ORDER_ID = mResponse.getResult();
            //         <--End-->  For New Flow

            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {

                DialogManager.getInstance().showSuccessPopup(OrderSummaryScreen.this, new InterfaceBtnCallBack() {
                    @Override
                    public void onPositiveClick() {


                        AppConstants.HEAD = "";
                        AppConstants.NECK = "";
                        AppConstants.CHEST = "";
                        AppConstants.WAIST = "";
                        AppConstants.THIGH = "";
                        AppConstants.ANKLE = "";
                        AppConstants.KNEE = "";

                        AppConstants.OVER_ALL_HEIGHT = "";
                        AppConstants.SHORTS = "";
                        AppConstants.OUTSEAM = "";
                        AppConstants.INSEAM = "";

                        AppConstants.SHOULDER = "";
                        AppConstants.HALF_SLEEVE = "";
                        AppConstants.BICEP = "";
                        AppConstants.HIP = "";
                        AppConstants.BOTTOM = "";

                        AppConstants.HEIGHT = "";
                        AppConstants.SLEEVE = "";
                        AppConstants.WRIST = "";

                        AppConstants.WOMEN_OVER_BUST = "";
                        AppConstants.WOMEN_UNDER_BUST = "";
                        AppConstants.WOMEN_HIP_BONE = "";
                        AppConstants.WOMEN_THIGH = "";
                        AppConstants.WOMEN_KNEE = "";
                        AppConstants.WOMEN_CALF = "";
                        AppConstants.WOMEN_ANKLE = "";

                        AppConstants.WOMEN_HEAD = "";
                        AppConstants.WOMEN_NECK = "";
                        AppConstants.WOMEN_BUST = "";
                        AppConstants.WOMEN_WAIST = "";
                        AppConstants.WOMEN_FULL_HIP = "";

                        AppConstants.WOMEN_HEIGHT = "";
                        AppConstants.WOMEN_STW = "";
                        AppConstants.WOMEN_NLTC = "";
                        AppConstants.WOMENT_NLTB = "";
                        AppConstants.WOMEN_STHB = "";
                        AppConstants.WOMEN_WTHB = "";
                        AppConstants.WOMEN_HTH = "";
                        AppConstants.WOMEN_INSEAM = "";
                        AppConstants.WOMEN_OUTSEAM = "";

                        AppConstants.WOMEN_SHOULDER = "";
                        AppConstants.WOMEN_BICEP = "";
                        AppConstants.WOMEN_WRIST = "";

                        AppConstants.WOMEN_SLEEVE = "";

                        AppConstants.BOY_HEAD = "";
                        AppConstants.BOY_NECK = "";
                        AppConstants.BOY_CHEST = "";
                        AppConstants.BOY_WAIST = "";
                        AppConstants.BOY_THIGH = "";
                        AppConstants.BOY_KNEE = "";
                        AppConstants.BOY_ANKLE = "";

                        AppConstants.BOY_OVER_ALL_HEIGHT = "";
                        AppConstants.BOY_SHORTS = "";
                        AppConstants.BOY_OUTSEAM = "";
                        AppConstants.BOY_INSEAM = "";

                        AppConstants.BOY_SHOULDER = "";
                        AppConstants.BOY_HALFSLEEVE = "";
                        AppConstants.BOY_HIP = "";
                        AppConstants.BOY_BICEP = "";
                        AppConstants.BOY_BOTTOM = "";

                        AppConstants.BOY_HEIGHT = "";
                        AppConstants.BOY_SLEEVE = "";
                        AppConstants.BOY_WRIST = "";

                        AppConstants.GIRL_OVER_BURST = "";
                        AppConstants.GIRL_UNDER_BURST = "";
                        AppConstants.GIRL_HIP_BONE = "";
                        AppConstants.GIRL_THIGH = "";
                        AppConstants.GIRL_KNEE = "";
                        AppConstants.GIRL_CALF = "";
                        AppConstants.GIRL_ANKLE = "";
                        AppConstants.GIRL_HEAD = "";
                        AppConstants.GIRL_NECK = "";
                        AppConstants.GIRL_BUST = "";
                        AppConstants.GIRL_WAIST = "";
                        AppConstants.GIRL_FULL_HIP = "";
                        AppConstants.GIRL_HEIGHT = "";
                        AppConstants.GIRL_STW = "";
                        AppConstants.GIRL_NLTC = "";
                        AppConstants.GIRL_NLTB = "";
                        AppConstants.GIRL_STHB = "";
                        AppConstants.GIRL_WTHB = "";
                        AppConstants.GIRL_HTH = "";
                        AppConstants.GIRL_INSEAM = "";
                        AppConstants.GIRL_OUTSEAM = "";
                        AppConstants.GIRL_SHOULDER = "";
                        AppConstants.GIRL_BICEP = "";
                        AppConstants.GIRL_WRIST = "";
                        AppConstants.GIRL_SLEEVE = "";
                        AppConstants.MEASUREMENT_VALUES = new ArrayList<>();
                        AppConstants.MEASUREMENT_ID = "";
                        AppConstants.MEASUREMENT_MANUALLY = "";
                        AppConstants.MEASUREMENT_MAP = new HashMap<>();
//                        AppConstants.SUB_DRESS_TYPE_ID = "";
                        AppConstants.GET_ADDRESS_ID = "";

//                        AppConstants.PATTERN_ID = "";

                        AppConstants.ORDER_TYPE_ID = "";

                        AppConstants.MEASUREMENT_ID = "";

                        AppConstants.MATERIAL_IMAGES = new ArrayList<>();

                        AppConstants.REFERENCE_IMAGES = new ArrayList<>();

                        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

                        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

                        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                        AppConstants.DELIVERY_TYPE_ID = "";

                        AppConstants.MEASUREMENT_TYPE = "";

                        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
//                        AppConstants.TAILOR_DIRECT_ORDER = "";

//                        nextScreen(HomeActivity.class, true);
                        isApproveOrder();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                nextScreen(OrderConfirmationActivity.class, true);

                            }
                        }, 1000);

                    }
                });

            }

        }

        if (resObj instanceof ViewDetailsNewFlowResponse){
            ViewDetailsNewFlowResponse mResponse = (ViewDetailsNewFlowResponse)resObj;

            mOrderApprovalSeasonalTxt.setText(getResources().getString(R.string.none));

            if (mResponse.getResult().getGetpattternById().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPatternInArabic());
                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                }else {
                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish());
                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                }
            }

            if(mResponse.getResult().getGetColorById().size()>0){
                String ColorNameArabic = "";
                String ColorNameEnglish = "";

                for (int i=0; i<mResponse.getResult().getGetColorById().size(); i++){
                    if (i>0){
                        ColorNameArabic = ColorNameArabic + ", " + mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = ColorNameEnglish + ", " +  mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
                    }else {
                        ColorNameArabic = mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = mResponse.getResult().getGetColorById().get(i).getColorInEnglish();

                    }
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalColorTxt.setText(ColorNameArabic.equalsIgnoreCase("") ? getResources().getString(R.string.none) : ColorNameArabic);
                }else {
                    mOrderApprovalColorTxt.setText(ColorNameEnglish.equalsIgnoreCase("") ? getResources().getString(R.string.none) : ColorNameEnglish);

                }


            }

            if (mResponse.getResult().getGetMaterialName().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic());
                }else {
                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish());
                }

            }

        }


        if (resObj instanceof InsertMeasurementValueResponse) {
            InsertMeasurementValueResponse mResponse = (InsertMeasurementValueResponse) resObj;
        }
//        if (resObj instanceof ViewDetailsNewFlowResponse) {
//            ViewDetailsNewFlowResponse mResponse = (ViewDetailsNewFlowResponse) resObj;
//
//            mOrderApprovalSeasonalTxt.setText(getResources().getString(R.string.none));
//
//            if (mResponse.getResult().getGetpattternById().size() > 0) {
//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPatternInArabic());
//                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
//                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
//                } else {
//                    mOrderApprovalPatternTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish());
//                    mOrderApprovalPlaceTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
//                    mOrderApprovalBrandsTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
//                }
//            }
//
//            if (mResponse.getResult().getGetColorById().size() > 0) {
//                String ColorNameArabic = "";
//                String ColorNameEnglish = "";
//
//                for (int i = 0; i < mResponse.getResult().getGetColorById().size(); i++) {
//                    if (i > 0) {
//                        ColorNameArabic = ColorNameArabic + ", " + mResponse.getResult().getGetColorById().get(i).getColorInArabic();
//                        ColorNameEnglish = ColorNameEnglish + ", " + mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
//                    } else {
//                        ColorNameArabic = mResponse.getResult().getGetColorById().get(i).getColorInArabic();
//                        ColorNameEnglish = mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
//
//                    }
//                }
//
//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    mOrderApprovalColorTxt.setText(ColorNameArabic.equalsIgnoreCase("") ? getResources().getString(R.string.none) : ColorNameArabic);
//                } else {
//                    mOrderApprovalColorTxt.setText(ColorNameEnglish.equalsIgnoreCase("") ? getResources().getString(R.string.none) : ColorNameEnglish);
//
//                }
//
//
//            }
//
//            if (mResponse.getResult().getGetMaterialName().size() > 0) {
//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic());
//                } else {
//                    mOrderApprovalMaterialTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish().equalsIgnoreCase("") ? getResources().getString(R.string.none) : mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish());
//                }
//
//            }
//
//        }
    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_summary_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            mHeaderTxt.setText("ملخص الطلبية");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_summary_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            mHeaderTxt.setText("order_summary");

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    public void isApproveOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("IsApproved", "1");
        map.put("Type", "Tailor");

        restService.approveOrder(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

}
