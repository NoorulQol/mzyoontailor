package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoontailor.entity.GetPatternImageByMaterialId;
import com.qoltech.mzyoontailor.entity.ReferenceImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.BuyerOrderDetailResponse;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ZoomOutPageTransformer;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddedReferenceViewScreen extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;

    TextView material_txt, season_text,
            place_of_industry_txt, brand_txt, material_type_txt, color_txt;

    List<GetPatternImageByMaterialId> getPatternImageByMaterialIds;
    List<ReferenceImagesSkillUpdateGetEntity> materialImagesSkillUpdateGetEntities;
    AppCompatCheckBox check_box_material_screen;
    Button save;
    @BindView(R.id.view_details_view_pager)
    ViewPager mViewDetailsViewpager;

    @BindView(R.id.pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;


    ArrayList<String> mImageList;

    ViewDetailPagerAdapter mViewDetailAdapter;
    SharedPreferences sharedPreferences;
    public UserDetailsEntity mUserDetailsEntityRes;
    TransparentProgressDialog transparentProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_added_material_view);
//        material_txt = (TextView) findViewById(R.id.material_txt);
//        season_text = (TextView) findViewById(R.id.season_text);
//        place_of_industry_txt = (TextView) findViewById(R.id.place_of_industry_txt);
//        brand_txt = (TextView) findViewById(R.id.brand_txt);
//        material_type_txt = (TextView) findViewById(R.id.material_type_txt);
//        color_txt = (TextView) findViewById(R.id.color_txt);
//        check_box_material_screen = findViewById(R.id.check_box_material_screen);
//        save = findViewById(R.id.save_material_screen);
//        friendly_name_english = (EditText) findViewById(R.id.friendly_name_english);
//        friendly_name_arabic = (EditText) findViewById(R.id.friendly_name_arabic);
//        friendly_name_english_layout = (RelativeLayout) findViewById(R.id.friendly_name_english_layout);
//        friendly_name_arabic_layout = (RelativeLayout) findViewById(R.id.friendly_name_arabic_layout);
        transparentProgressDialog = new TransparentProgressDialog(AddedReferenceViewScreen.this);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        initView();
//
        restService = ((MainApplication) getApplication()).getClient();


        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
            getOrderDetailsForTailor("Tailor");
        } else {
//            getOrderDetails();
//            getOrderdetailsForBuyer();

            getOrderDetailsForTailor("Buyer");
        }

    }

    public void initView() {

        ButterKnife.bind(this);
        setHeader();
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddedReferenceViewScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        mImageList = new ArrayList<>();


    }



    public void viewPagerGet(ArrayList<String> image) {
        mViewDetailAdapter = new ViewDetailPagerAdapter(this, image);
        mViewDetailsViewpager.setAdapter(mViewDetailAdapter);
        mViewDetailsViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mViewDetailsViewpager);
        //page change tracker
        mViewDetailsViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("ViewDetail");
    }



    ;


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.material_details);
        mRightSideImg.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.GONE);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void getOrderDetailsForTailor(String orderTypes) {
        restService.getOrderDetailsResponseForTailor(AppConstants.ORDER_ID,AppConstants.ORDER_TYPE,orderTypes).enqueue(new Callback<GetOrderDetailsResponseCheck>() {

            @Override
            public void onResponse(Call<GetOrderDetailsResponseCheck> call, Response<GetOrderDetailsResponseCheck> response) {

          try {
              materialImagesSkillUpdateGetEntities=response.body().getResult().getReferenceImage();
//              referenceImagesSkillUpdateGetEntities=response.body().getResult().getReferenceImage();
              for (int i = 0; i < materialImagesSkillUpdateGetEntities.size(); i++) {
                  mImageList.add(GlobalData.SERVER_URL + "Images/ReferenceImages/" + materialImagesSkillUpdateGetEntities.get(i).getImage());

                  viewPagerGet(mImageList);

              }
          }

          catch (Exception e){}
            }

            @Override
            public void onFailure(Call<GetOrderDetailsResponseCheck> call, Throwable t) {

            }
        });
    }

    public void getOrderdetailsForBuyer() {

        restService.getOrderDetailsResponseBuyer(AppConstants.ORDER_ID).enqueue(new Callback<BuyerOrderDetailResponse>() {
            @Override
            public void onResponse(Call<BuyerOrderDetailResponse> call, Response<BuyerOrderDetailResponse> response) {

                    try {
                        materialImagesSkillUpdateGetEntities=response.body().getResult().getReferenceImage();
                        for (int i = 0; i < materialImagesSkillUpdateGetEntities.size(); i++) {
                            mImageList.add(GlobalData.SERVER_URL + "Images/ReferenceImages/" + materialImagesSkillUpdateGetEntities.get(i).getImage());

                            viewPagerGet(mImageList);

                        }

                    } catch (Exception e) {


                }

            }

            @Override
            public void onFailure(Call<BuyerOrderDetailResponse> call, Throwable t) {

            }
        });
    }


}
