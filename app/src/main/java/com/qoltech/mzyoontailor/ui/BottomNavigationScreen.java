package com.qoltech.mzyoontailor.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.fragment.ContactUsFragment;
import com.qoltech.mzyoontailor.fragment.HomeScreenFragment;
import com.qoltech.mzyoontailor.main.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BottomNavigationScreen extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener{


    @BindView(R.id.handle)
    RelativeLayout mHandle;

    @BindView(R.id.slider_drawer_icon_img)
    ImageView mSliderDrawerImg;

    @BindView(R.id.simpleSlidingDrawer)
    SlidingDrawer simpleSlidingDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_bottom_navigation);

        initView();

        //loading the default fragment
        loadFragment(new HomeScreenFragment());

        BottomNavigationView navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(this);

        simpleSlidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
                Bitmap myImg = BitmapFactory.decodeResource(getResources(), R.drawable.side_menu_icon);
                Matrix matrix = new Matrix();
                matrix.postRotate(180);
                Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(),
                        matrix, true);
                mSliderDrawerImg.setImageBitmap(rotated);

            }
        });
        // implement setOnDrawerCloseListener event
        simpleSlidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
                // change the handle button text
                Bitmap myImg = BitmapFactory.decodeResource(getResources(), R.drawable.side_menu_icon);
                Matrix matrix = new Matrix();
                matrix.postRotate(0);
                Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(),
                        matrix, true);
                mSliderDrawerImg.setImageBitmap(rotated);

            }
        });
    }


    public void initView(){
        ButterKnife.bind(this);
//        Toast.makeText(BottomNavigationScreen.this,PreferenceUtil.getStringValue(BottomNavigationScreen.this,AppConstants.USER_IS_STR),Toast.LENGTH_SHORT).show();
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.slider_account_lay,R.id.slider_address_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.slider_account_lay:
                simpleSlidingDrawer.animateClose();
                nextScreen(ProfileScreen.class,true);
                break;
            case R.id.slider_address_lay:
                simpleSlidingDrawer.animateClose();
                nextScreen(AddressScreen.class,true);
                break;

        }

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                fragment = new HomeScreenFragment();
                break;

            case R.id.navigation_order:
                fragment = new ContactUsFragment();
                break;

            case R.id.navigation_cart:
                fragment = new ContactUsFragment();
                break;


            case R.id.navigation_contact:
                fragment = new ContactUsFragment();
                break;
        }

        return loadFragment(fragment);
    }

    @Override
    public void onBackPressed() {
        exitFromApp();
    }

    /*App exit popup*/
    private void exitFromApp() {
//        DialogManager.getInstance().showOptionPopup(this, getString(R.string.exit_msg),
//                getString(R.string.yes), getString(R.string.no), new InterfaceTwoBtnCallBack() {
//                    @Override
//                    public void onPositiveClick() {
//                        ActivityCompat.finishAffinity(BottomNavigationScreen.this);
//                    }
//
//                    @Override
//                    public void onNegativeClick() {
//
//                    }
//                });

    }
}
