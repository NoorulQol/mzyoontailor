package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.ApproveScheduleRespone;
import com.qoltech.mzyoontailor.modal.GetAppointmentDateForScheduleResponse;
import com.qoltech.mzyoontailor.modal.GetAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScheduletDetailScreen extends BaseActivity {


    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.schedule_par_lay)
    LinearLayout mScheduleParLay;

    @BindView(R.id.schedule_inner_par_lay)
    LinearLayout mScheduleInnerParLay;

    @BindView(R.id.schedule_status_txt)
    TextView mScheduleStatusTxt;

    @BindView(R.id.appointment_schedule_type_header_img)
    ImageView mAppointmentScheduleTypeHeaderImg;

    @BindView(R.id.appointment_schedule_type_header_txt)
    TextView mAppointmentScheduleTypeHeaderTxt;

    @BindView(R.id.appointment_schedule_type_body_img)
    ImageView mAppointmentScheduleBodyImg;

    @BindView(R.id.schedule_type_time_slot_txt)
    public TextView mScheduleTypeTimeSlotTxt;

    @BindView(R.id.schedule_type_fromt_date_txt)
    public TextView mScheduleTypeFromDateTxt;

    @BindView(R.id.appointment_schedule_type_to_date_txt)
    public TextView mScheduleTypeToDateTxt;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mAppointmentReschedule, mAppointmentReject;


    String mScheduleStatusStr = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_schedule_detail_screen);
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mScheduleParLay);

        setHeader();


        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ScheduletDetailScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);


        getAppointmentForScheduleTypeApiCall();
        getAppointmentDateForScheduleApiCall();

        getLanguage();

        AppConstants.DATE_FOR_RESTRICT_SCHEDULE = "";
    }

    @OnClick({R.id.header_left_side_img,R.id.appointment_material_approve_lay,R.id.appointment_material_reject_lay,R.id.schedule_type_time_slot_par_lay,R.id.schedule_type_calender_from_par_lay,R.id.schedule_type_calender_to_par_lay,R.id.schedule_status_txt})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                    onBackPressed();
                }
                break;
            case R.id.appointment_material_approve_lay:
                if (mScheduleTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")||mScheduleTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")||mScheduleTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                   DialogManager.getInstance().showAlertPopup(ScheduletDetailScreen.this, "Appointment is not created to approve", new InterfaceBtnCallBack() {
                       @Override
                       public void onPositiveClick() {

                       }
                   });
                }else {
                    approveScheduleApiCall(AppConstants.APPOINTMENT_SCHEDULE_ID, "1", "Approve");

                }

                break;
            case R.id.appointment_material_reject_lay:
                if (mScheduleTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")||mScheduleTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")||mScheduleTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    DialogManager.getInstance().showAlertPopup(ScheduletDetailScreen.this, "Appointment is not created to reject", new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    alertDismiss(mAppointmentReject);
                    mAppointmentReject = getDialog(ScheduletDetailScreen.this, R.layout.pop_up_appointment);
                    WindowManager.LayoutParams LayoutParam = new WindowManager.LayoutParams();
                    Window windows = mAppointmentReject.getWindow();
                    if (windows != null) {
                        LayoutParam.copyFrom(windows.getAttributes());
                        LayoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        windows.setAttributes(LayoutParam);
                        windows.setGravity(Gravity.CENTER);
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }
                    RelativeLayout cancelLays, saveLay;
                    final EditText mEditText;
                    cancelLays = mAppointmentReject.findViewById(R.id.appointment_popup_cancel_lay);
                    saveLay = mAppointmentReject.findViewById(R.id.appointment_popup_save_lay);
                    mEditText = mAppointmentReject.findViewById(R.id.appointment_popup_edt_txt);
                    cancelLays.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReject.dismiss();
                        }
                    });

                    saveLay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mEditText.getText().toString().equalsIgnoreCase("")) {
                                mEditText.clearAnimation();
                                mEditText.setAnimation(ShakeErrorUtils.shakeError());
                                mEditText.setError("Please give reason");
                            } else {
                                approveScheduleApiCall(AppConstants.APPOINTMENT_SCHEDULE_ID, "2", mEditText.getText().toString());
                            }

                            mAppointmentReject.dismiss();
                        }
                    });

                    alertShowing(mAppointmentReject);
                }
                break;
            case R.id.schedule_status_txt:
                if (mScheduleStatusStr.equalsIgnoreCase("Rejected")){
                    alertDismiss(mAppointmentReschedule);
                    mAppointmentReschedule = getDialog(ScheduletDetailScreen.this, R.layout.pop_up_appointment_reschedule);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAppointmentReschedule.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    RelativeLayout mOkRal,mResRal;

                    /*Init view*/
                    mOkRal = mAppointmentReschedule.findViewById(R.id.appointment_reschedule_ok_lay);
                    mResRal = mAppointmentReschedule.findViewById(R.id.pop_up_appointment_reschedule_par_lay);

                    mOkRal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();
                        }
                    });
                    mResRal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReschedule.dismiss();

                        }
                    });

                    alertShowing(mAppointmentReschedule);
                }
                break;
        }
    }

    public void approveScheduleApiCall(final String AppointmentId, final String IsApproved, final String Reason) {
        if (NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)) {
            APIRequestHandler.getInstance().approveScheduleApiCall(AppointmentId, IsApproved, Reason, ScheduletDetailScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    approveScheduleApiCall(AppointmentId, IsApproved, Reason);
                }
            });
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.appointment).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

        mScheduleInnerParLay.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            mHeaderLeftBackBtn.setVisibility(View.INVISIBLE);
        }

    }

    public void getAppointmentForScheduleTypeApiCall(){
        if (NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getAppointmentForScheduleTypeApiCall(AppConstants.APPOINTMENT_LIST_ID,ScheduletDetailScreen.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentForScheduleTypeApiCall(AppConstants.REQUEST_LIST_ID,ScheduletDetailScreen.this);

            }

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentForScheduleTypeApiCall();
                }
            });
        }
    }

    public void getAppointmentDateForScheduleApiCall(){
        if (NetworkUtil.isNetworkAvailable(ScheduletDetailScreen.this)){

            if ( AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")){
                APIRequestHandler.getInstance().getAppointmentDateForScheduleTypeApiCall
                        (AppConstants.APPOINTMENT_LIST_ID,AppConstants.DIRECT_USERS_ID,"Buyer",ScheduletDetailScreen.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentDateForScheduleTypeApiCall
                        (AppConstants.REQUEST_LIST_ID,AppConstants.DIRECT_USERS_ID,"Buyer",ScheduletDetailScreen.this);

            }
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ScheduletDetailScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentDateForScheduleApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetAppointmentForScheduleTypeResponse){
            GetAppointmentForScheduleTypeResponse mResponse = (GetAppointmentForScheduleTypeResponse)resObj;
            if (mResponse.getResult().size() >0){
                AppConstants.APPOINTMENT_SCHEDULE_ID = String.valueOf(mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentId());

                try {
                    Glide.with(ScheduletDetailScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/ServiceType/"+mResponse.getResult().get(0).getHeaderImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mAppointmentScheduleTypeHeaderImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                try {
                    Glide.with(ScheduletDetailScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/ServiceType/"+mResponse.getResult().get(0).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mAppointmentScheduleBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                mScheduleStatusStr = mResponse.getResult().get(0).getStatus();
                mScheduleStatusTxt.setText(mResponse.getResult().get(0).getStatus());

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

                    mAppointmentScheduleTypeHeaderTxt.setText(mResponse.getResult().get(0).getHeaderInArabic());


                }else {
                    mAppointmentScheduleTypeHeaderTxt.setText(mResponse.getResult().get(0).getHeaderInEnglish());

                }

            }
        }
        if (resObj instanceof GetAppointmentDateForScheduleResponse){
            GetAppointmentDateForScheduleResponse mResponse = (GetAppointmentDateForScheduleResponse)resObj;
            if (mResponse.getResult().size() >0){
                mScheduleTypeFromDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt());
                mScheduleTypeToDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getToDt());
                mScheduleTypeTimeSlotTxt.setText(mResponse.getResult().get(mResponse.getResult().size()-1).getAppointmentTime());

            }
        }

        if (resObj instanceof ApproveScheduleRespone) {
            ApproveScheduleRespone mResponse = (ApproveScheduleRespone) resObj;
            Toast.makeText(ScheduletDetailScreen.this, "Appointment Created", Toast.LENGTH_SHORT).show();

            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {

                nextScreen(HomeActivity.class, true);

            }
        }
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }
    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.schedule_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.schedule_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    @Override
    public void onBackPressed() {
        AppConstants.DATE_FOR_RESTRICT_SCHEDULE = "";

        if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("DIRECT_ORDER")||!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
            super.onBackPressed();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }

    }
}
