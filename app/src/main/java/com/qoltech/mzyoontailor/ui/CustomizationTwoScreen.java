package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.CustomizationTwoColorAdapter;
import com.qoltech.mzyoontailor.adapter.CustomizationTwoMaterialAdapter;
import com.qoltech.mzyoontailor.adapter.CustomizationTwoPatternAdapter;
import com.qoltech.mzyoontailor.entity.ApicallidEntity;
import com.qoltech.mzyoontailor.entity.CustomizationColorsEntity;
import com.qoltech.mzyoontailor.entity.CustomizationMaterialsEntity;
import com.qoltech.mzyoontailor.entity.CustomizationPatternsEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.CustomizationTwoApiCallModal;
import com.qoltech.mzyoontailor.modal.GetCustomizationTwoResponse;
import com.qoltech.mzyoontailor.modal.NewFlowCustomizationTwoApiCall;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomizationTwoScreen extends  BaseActivity {


    private CustomizationTwoMaterialAdapter mMaterialAdapter;
    private CustomizationTwoColorAdapter mColorAdapter;
    private CustomizationTwoPatternAdapter mPattrnAdapter;

    private ArrayList<CustomizationMaterialsEntity> mMateriallList;
    private ArrayList<CustomizationColorsEntity> mColorList;
    private ArrayList<CustomizationPatternsEntity> mPatternList;

    @BindView(R.id.customization_two_par_lay)
    LinearLayout mCustomizationTwoParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.customize_material_recycler_view)
    RecyclerView mMaterialRecyclerView;

    @BindView(R.id.customize_color_recycler_view)
    RecyclerView mColorRecyclerView;

    @BindView(R.id.customize_pattern_recycler_view)
    RecyclerView mPatternRecyclerView;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

//    @BindView(R.id.new_flow_material_wiz_lay)
//    RelativeLayout mNewFlowMaterialWizLay;
//
//    @BindView(R.id.material_wiz_lay)
//    RelativeLayout mMaterialWizLay;

//    @BindView(R.id.customization_one_check_img)
//    ImageView mCustomizationOneCheckImg;

    SharedPreferences sharedPreferences;
    String mCheckBool = "false";

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_customization_two_screen);
        sharedPreferences=getSharedPreferences("TailorId",MODE_PRIVATE);

        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mCustomizationTwoParLay);

        setHeader();

        mMateriallList = new ArrayList<>();
        mColorList = new ArrayList<>();
        mPatternList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationTwoScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
        getNewFlowCustomizationTwoApiCall();
//        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//            getNewFlowCustomizationTwoApiCall();
//
//        }else {
//            getCustomizationTwoApiCall();
//
//        }
//        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
//            mMaterialWizLay.setVisibility(View.GONE);
//            mNewFlowMaterialWizLay.setVisibility(View.VISIBLE);
//        }else {
//            mMaterialWizLay.setVisibility(View.VISIBLE);
//            mNewFlowMaterialWizLay.setVisibility(View.GONE);
//        }

    }


    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                AppConstants.COLOR_ID = "";
                AppConstants.MATERIAL_ID = "";
                AppConstants.PATTERN_ID = "";
                onBackPressed();
                break;
        }

    }

    public void  getCustomizationTwoApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationTwoScreen.this)){

            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();
            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();

            CustomizationTwoApiCallModal mModal = new CustomizationTwoApiCallModal();


            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("1");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }

            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("1");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }

            }
            if (AppConstants.COLOR_ID.equalsIgnoreCase("")){
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("1");

                mColorId.add(mColorEntityId);

            }else {
                List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(items.get(i));

                    mColorId.add(mColorEntityId);
                }

            }



            mModal.setBrandId(mBrandId);
            mModal.setMaterialTypeId(mMaterialId);
            mModal.setColorId(mColorId);

            APIRequestHandler.getInstance().getCustomizationTwoApi(mModal,CustomizationTwoScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationTwoApiCall();
                }
            });
        }

    }

    public void  getNewFlowCustomizationTwoApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationTwoScreen.this)){

            ArrayList<ApicallidEntity> mBrandId = new ArrayList<>();
            ArrayList<ApicallidEntity> mMaterialId = new ArrayList<>();
            ArrayList<ApicallidEntity> mColorId = new ArrayList<>();

            NewFlowCustomizationTwoApiCall mModal = new NewFlowCustomizationTwoApiCall();


            if (AppConstants.BRANDS_ID.equalsIgnoreCase("")){
                ApicallidEntity mBrandEntityId = new ApicallidEntity();

                mBrandEntityId.setId("1");

                mBrandId.add(mBrandEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.BRANDS_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mBrandEntityId = new ApicallidEntity();

                    mBrandEntityId.setId(items.get(i));

                    mBrandId.add(mBrandEntityId);
                }

            }
            if (AppConstants.MATERIAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                mMaterialEntityId.setId("1");

                mMaterialId.add(mMaterialEntityId);


            }else {
                List<String> items = Arrays.asList(AppConstants.MATERIAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mMaterialEntityId = new ApicallidEntity();

                    mMaterialEntityId.setId(items.get(i));

                    mMaterialId.add(mMaterialEntityId);

                }

            }
            if (AppConstants.COLOR_ID.equalsIgnoreCase("")){
                ApicallidEntity mColorEntityId = new ApicallidEntity();

                mColorEntityId.setId("1");

                mColorId.add(mColorEntityId);

            }else {
                List<String> items = Arrays.asList(AppConstants.COLOR_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mColorEntityId = new ApicallidEntity();

                    mColorEntityId.setId(items.get(i));

                    mColorId.add(mColorEntityId);
                }

            }


            mModal.setTailorId(Integer.parseInt(sharedPreferences.getString("TailorId","")));
            mModal.setBrandId(mBrandId);
            mModal.setMaterialTypeId(mMaterialId);
            mModal.setColorId(mColorId);

            APIRequestHandler.getInstance().getNewFlowCustomizationTwoApi(mModal,CustomizationTwoScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationTwoApiCall();
                }
            });
        }

    }


    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.material_selection).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));


    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetCustomizationTwoResponse){
            GetCustomizationTwoResponse mResponse = (GetCustomizationTwoResponse)resObj;

            setMaterialAdapter(mResponse.getResult().getMaterials());
            setColorAdapter(mResponse.getResult().getColors());
            setPatternAdapter(mResponse.getResult().getPatterns());
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    /*Set Adapter for the Recycler view*/
    public void setMaterialAdapter(ArrayList<CustomizationMaterialsEntity> mMaterialList) {

        if (mMaterialAdapter == null) {


            mMaterialAdapter = new CustomizationTwoMaterialAdapter(this,mMaterialList);
            mMaterialRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mMaterialRecyclerView.setAdapter(mMaterialAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMaterialAdapter.notifyDataSetChanged();
                }
            });
        }

    }
    /*Set Adapter for the Recycler view*/
    public void setColorAdapter(ArrayList<CustomizationColorsEntity> mColorList) {

        if (mColorAdapter == null) {


            mColorAdapter = new CustomizationTwoColorAdapter(this,mColorList);
            mColorRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mColorRecyclerView.setAdapter(mColorAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mColorAdapter.notifyDataSetChanged();
                }
            });
        }

    }
    /*Set Adapter for the Recycler view*/
    public void setPatternAdapter(ArrayList<CustomizationPatternsEntity> mPatternList) {

//        if (mPattrnAdapter == null) {

        mPatternRecyclerView.setVisibility(mPatternList.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(mPatternList.size() > 0 ? View.GONE : View.VISIBLE);

        mPattrnAdapter = new CustomizationTwoPatternAdapter(this,mPatternList);
        mPatternRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mPatternRecyclerView.setAdapter(mPattrnAdapter);
//        } else {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mPattrnAdapter.notifyDataSetChanged();
//                }
//            });
//        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AppConstants.MATERIAL_ID = "";
        AppConstants.MATERIAL_TYPE_NAME = "";

        AppConstants.COLOR_ID = "";
        AppConstants.COLOUR_NAME = "";

        AppConstants.PATTERN_ID = "";
        AppConstants.PATTERN_NAME = "";

        AppConstants.SEASONAL_NAME = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";
        AppConstants.BRANDS_NAME = "";
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_two_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_two_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}
