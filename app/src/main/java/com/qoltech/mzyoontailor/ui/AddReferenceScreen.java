package com.qoltech.mzyoontailor.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AddMaterialAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.FileUploadResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddReferenceScreen extends BaseActivity {

    @BindView(R.id.add_reference_par_lay)
    LinearLayout mAddReferencePayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;

    String IMAGE_PATH = "";
    private static final String IMAGE_DIRECTORY = "/MZYOON";
    private ArrayList<String> mAddImageList;

    private AddMaterialAdapter mAddReferenceAdapter;

    @BindView(R.id.add_refenece_recycler_view)
    RecyclerView mReferenceRecyclerView;

    @BindView(R.id.add_reference_img)
    public ImageView mAddReferenceImg;

    @BindView(R.id.add_reference_add_img)
    ImageView mAddImg;

//    @BindView(R.id.new_flow_measurement_wizard_lay)
//    RelativeLayout mNewFlowMeasurementWizLay;

//    @BindView(R.id.measurement_wizard_lay)
//    RelativeLayout mMeasurementWizLay;

    @BindView(R.id.no_imges_shown)
    public TextView mNoImagesTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mHintDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_add_reference_screen);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAddReferencePayLay);

        setHeader();
        mAddImageList = new ArrayList<>();

        mAddImageList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddReferenceScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
            getHintDialog();

        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<String> addMaterialList) {

        mAddReferenceAdapter = new AddMaterialAdapter(this, addMaterialList);
        mReferenceRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mReferenceRecyclerView.setAdapter(mAddReferenceAdapter);
        mReferenceRecyclerView.scrollToPosition(addMaterialList.size() - 1);

    }

    @OnClick({R.id.reference_next_btn, R.id.header_left_side_img, R.id.add_reference_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reference_next_btn:
                AppConstants.ADD_MATERIAL = "ADD_REFERENCE";
                AppConstants.EDIT_ADDRESS = "";
                AppConstants.SERVICE_TYPE = "SERVICE_TYPE";
                uploadProfileImageApiCall();
                nextScreen(AddressScreen.class, true);
                break;

            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.add_reference_lay:
                if (mAddImageList.size() >= 10) {
                    mAddImg.clearAnimation();
                    mAddImg.setAnimation(ShakeErrorUtils.shakeError());
                    Toast.makeText(AddReferenceScreen.this, getResources().getString(R.string.maximum_img_for_add_reference), Toast.LENGTH_SHORT).show();
                } else {
                    if (checkPermission()) {
                        uploadImage();
                    }
                }
                break;
        }

    }

    public void getHintDialog() {
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(AddReferenceScreen.this, R.layout.popup_add_material_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.add_material_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        } else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.add_material_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button hintSkipBtn;
        TextView mAddMaterialTxt,mAddMaterialHeaderTxt;

        hintSkipBtn = mHintDialog.findViewById(R.id.next_hint_btn);
        mAddMaterialTxt = mHintDialog.findViewById(R.id.material_hint_txt);
        mAddMaterialHeaderTxt = mHintDialog.findViewById(R.id.add_material_pop_up_header_txt);

        /*Set data*/
        mAddMaterialHeaderTxt.setText(getResources().getString(R.string.reference));
        mAddMaterialTxt.setText(getResources().getString(R.string.add_ref_hint));
        hintSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });

        alertShowing(mHintDialog);
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.add_reference));
        mRightSideImg.setVisibility(View.VISIBLE);

//        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
//            mMeasurementWizLay.setVisibility(View.GONE);
//            mNewFlowMeasurementWizLay.setVisibility(View.VISIBLE);
//        }else {
//            mMeasurementWizLay.setVisibility(View.VISIBLE);
//            mNewFlowMeasurementWizLay.setVisibility(View.GONE);
//        }
    }

    private void uploadImage() {
        AppConstants.ADD_MATERIAL = "ADD_REFERENCE";

        DialogManager.getInstance().showImageUploadPopup(this, getResources().getString(R.string.select_phot_type), getResources().getString(R.string.take_camera), getResources().getString(R.string.open_gallery), new InterfaceTwoBtnCallBack() {
            @Override
            public void onNegativeClick() {
                captureImage();
            }

            @Override
            public void onPositiveClick() {
                galleryImage();
            }
        });
    }

    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    String path = saveImage(thumbnail);
//                    IMAGE_PATH = path;
//                    IMAGE_PATH = path;
//                    saveImage(thumbnail);

                    mAddImageList.add(compressImage(String.valueOf(getImageUri(getApplicationContext(), thumbnail))));

                    setAdapter(mAddImageList);

                    mAddReferenceAdapter.notifyDataSetChanged();

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    // successfully captured the image
                    // display it in image view

                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), contentURI);
//                        String path = saveImage(bitmap);
//                        IMAGE_PATH = path;

                        mAddImageList.add(compressImage(String.valueOf(contentURI)));

                        setAdapter(mAddImageList);

                        mAddReferenceAdapter.notifyDataSetChanged();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;

        }
    }
//    public String saveImage(Bitmap myBitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//        File wallpaperDirectory = new File(
//                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
//        // have the object build the directory structure, if needed.
//        if (!wallpaperDirectory.exists()) {
//            if (checkPermission() == true) {
//                wallpaperDirectory.mkdirs();
//
//            }
//        }
//
//        try {
//            File f = new File(wallpaperDirectory, Calendar.getInstance()
//                    .getTimeInMillis() + ".jpg");
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//            MediaScannerConnection.scanFile(getApplicationContext(),
//                    new String[]{f.getPath()},
//                    new String[]{"image/jpeg"}, null);
//            fo.close();
//            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());
//
//            return f.getAbsolutePath();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        return "";
//    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;

        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }


                @Override
                public void onPositiveClick() {
                    uploadImage();
                }

            });
        }

        return addPermission;

    }

    public void uploadProfileImageApiCall() {
        if (NetworkUtil.isNetworkAvailable(AddReferenceScreen.this)) {
            if (mAddImageList.size() > 0) {

                APIRequestHandler.getInstance().uploadMultiFile(mAddImageList, AddReferenceScreen.this, "ReferenceImages");

            }
        } else {
            uploadProfileImageApiCall();
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof FileUploadResponse) {
            FileUploadResponse mResponse = (FileUploadResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success") && mResponse.getResult().size() > 0) {

                for (int i = 0; i < mResponse.getResult().size(); i++) {
                    String[] parts = mResponse.getResult().get(i).split("\\\\");
                    String part1 = parts[0]; // 004
                    String part2 = parts[1];
                    String lastOne = parts[parts.length - 1];

                    AppConstants.REFERENCE_IMAGES.add(lastOne);

                }
            }
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_reference_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            updateResources(AddReferenceScreen.this,"ar");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_reference_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(AddReferenceScreen.this,"en");

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}
