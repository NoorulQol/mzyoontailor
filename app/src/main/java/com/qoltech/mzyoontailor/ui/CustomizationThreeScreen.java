package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.CustomizationThreeAdapter;
import com.qoltech.mzyoontailor.adapter.CustomizationThreeDialogAdapter;
import com.qoltech.mzyoontailor.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoontailor.entity.GetCustomizationThreeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.CustomizationThreeGetResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationThreeResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomizationThreeScreen extends BaseActivity {

    private CustomizationThreeAdapter mCustomizeAdapter;

    private CustomizationThreeDialogAdapter mCustomizationThreeDialogAdapter;

    private ArrayList<CustomizationThreeScreen> mCustomizationList;

    private ArrayList<CustomizationAttributesEntity> mCustomizationDialogList;

    private Dialog mDialog;

    @BindView(R.id.customization_three_par_lay)
    RelativeLayout mCustomizationThreeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.customize_three_recycler_view)
    RecyclerView mCustomizatinThreeRecyclerView;

    @BindView(R.id.customization_three_spinner_txt)
    public TextView mCustomizationThreeSpinnerTxt;

    @BindView(R.id.customization_three_front_img)
    ImageView mCustomizationThreeFrontImg;

    @BindView(R.id.customization_three_back_img)
    ImageView mCustomizationThreeBackImg;

    @BindView(R.id.customization_three_full_img)
    public ImageView mCustomizationThreeFullImg;

    @BindView(R.id.customization_three_front_img_lay)
    RelativeLayout mCustomizationThreeFrontImgLay;

    @BindView(R.id.customization_three_back_img_lay)
    RelativeLayout mCustomizationThreeBackImgLay;

    @BindView(R.id.customization_three_next_btn)
    RelativeLayout mCustomizationNextBtn;

    String mCustomizationThreeBackStr, mCustomizationThreeFrontStr, mCustomizationHintLapelTxt = "", mListOneTxtStr = "", mListOneImgStr = "", mListTwoTxtStr = "", mListTwoImgStr = "";
    int mCountInt;

    private UserDetailsEntity mUserDetailsEntityRes;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    private Dialog mCustomizationHintDialog;

    int mCountHintInt = 1, showingPopUp = 1;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_customization_three_screen);
        sharedPreferences=getSharedPreferences("TailorId",MODE_PRIVATE);
        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mCustomizationThreeParLay);

        setHeader();

        mCustomizationList = new ArrayList<>();

        mCustomizationDialogList = new ArrayList<>();
        customizationThreeNewFlowApiCall();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationThreeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
        mCustomizationThreeFrontImgLay.setVisibility(View.INVISIBLE);

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
            getHintDialog();

        }

    }

    public void customizationThreeApiCall() {
        if (NetworkUtil.isNetworkAvailable(CustomizationThreeScreen.this)) {
            APIRequestHandler.getInstance().customizationThreeApiCall(AppConstants.SUB_DRESS_TYPE_ID,
                    CustomizationThreeScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationThreeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    customizationThreeApiCall();
                }
            });
        }
    }

    public void getCustomizationThreeApiCall() {
        if (NetworkUtil.isNetworkAvailable(CustomizationThreeScreen.this)) {
            APIRequestHandler.getInstance().getCustomizationThreeAPICall(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID,
                    CustomizationThreeScreen.this);
        } else {

            DialogManager.getInstance().showNetworkErrorPopup(CustomizationThreeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationThreeApiCall();
                }
            });
        }
    }

    public void customizationThreeNewFlowApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationThreeScreen.this)){
            APIRequestHandler.getInstance().customizationNewFlowThreeApiCall
                    (String.valueOf(sharedPreferences.getString("TailorId","")),
                            AppConstants.SUB_DRESS_TYPE_ID,CustomizationThreeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationThreeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    customizationThreeApiCall();
                }
            });
        }
    }

    public void getCustomizationThreeNewFlowApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationThreeScreen.this)){
            APIRequestHandler.getInstance().getCustomizationThreeNewFlowAPICall(AppConstants.CUSTOMIZATION_DRESS_TYPE_ID,
                    sharedPreferences.getString("TailorId",""),
                    AppConstants.SUB_DRESS_TYPE_ID,CustomizationThreeScreen.this);
        }else {

            DialogManager.getInstance().showNetworkErrorPopup(CustomizationThreeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationThreeApiCall();
                }
            });
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetCustomizationThreeResponse) {
            GetCustomizationThreeResponse mResponse = (GetCustomizationThreeResponse) resObj;

            setAdapter(mResponse.getResult());

        }
        if (resObj instanceof CustomizationThreeGetResponse) {
            CustomizationThreeGetResponse mResponse = (CustomizationThreeGetResponse) resObj;

            if (mResponse.getResult().getCustomizationAttributes().size() > 0 || mResponse.getResult().getCustomizationImages().size() > 0 || mResponse.getResult().getAttributeImages().size() > 0) {
                mEmptyListLay.setVisibility(mResponse.getResult().getAttributeImages().size() > 0 ? View.GONE : View.VISIBLE);
                mCustomizatinThreeRecyclerView.setVisibility(mResponse.getResult().getAttributeImages().size() > 0 ? View.VISIBLE : View.GONE);
                mCustomizationThreeSpinnerTxt.setText(getResources().getString(R.string.no_result_found));

                for (int i = 0; i < mResponse.getResult().getCustomizationAttributes().size(); i++) {
                    if (i == 0) {
                        AppConstants.CUSTOMIZATION_NAME = mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameInEnglish();
                        AppConstants.CUSTOMIZATION_DRESS_TYPE_ID = String.valueOf(mResponse.getResult().getCustomizationAttributes().get(0).getId());
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            mCustomizationThreeSpinnerTxt.setText(mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameinArabic());
                            mCustomizationHintLapelTxt = mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameinArabic();

                        } else {
                            mCustomizationThreeSpinnerTxt.setText(mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameInEnglish());
                            mCustomizationHintLapelTxt = mResponse.getResult().getCustomizationAttributes().get(0).getAttributeNameInEnglish();

                        }

                    }

                }

                mCustomizationDialogList = mResponse.getResult().getCustomizationAttributes();
                AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = mCustomizationDialogList;

                mCountInt = mResponse.getResult().getCustomizationAttributes().size();

                setAdapter(mResponse.getResult().getAttributeImages());

                for (int i = 0; i < mResponse.getResult().getCustomizationImages().size(); i++) {
                    if (i == 0) {
                        mCustomizationThreeBackStr = mResponse.getResult().getCustomizationImages().get(0).getImage();
                        mCustomizationThreeBackImgLay.setBackgroundResource(R.color.app_color);

                    }
                    if (i == 1) {
                        mCustomizationThreeFrontStr = mResponse.getResult().getCustomizationImages().get(1).getImage();
                        mCustomizationThreeFrontImgLay.setVisibility(View.VISIBLE);

                    }

                }

                try {
                    Glide.with(CustomizationThreeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mCustomizationThreeFrontStr)
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCustomizationThreeFullImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
                try {
                    Glide.with(CustomizationThreeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mCustomizationThreeFrontStr)
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCustomizationThreeFrontImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
                try {
                    Glide.with(CustomizationThreeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mCustomizationThreeBackStr)
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCustomizationThreeBackImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            } else {
                mCustomizationNextBtn.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void setAdapter(ArrayList<GetCustomizationThreeEntity> mCustomizationList) {

        mEmptyListLay.setVisibility(mCustomizationList.size() > 0 ? View.GONE : View.VISIBLE);
        mCustomizatinThreeRecyclerView.setVisibility(mCustomizationList.size() > 0 ? View.VISIBLE : View.GONE);

        if (showingPopUp == 1) {
            showingPopUp = ++showingPopUp;

            for (int i = 0; i < mCustomizationList.size(); i++) {
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    if (i == 0) {
                        mListOneTxtStr = mCustomizationList.get(0).getAttributeNameInArabic();
                        mListOneImgStr = mCustomizationList.get(0).getImages();
                    } else if (i == 1) {
                        mListTwoTxtStr = mCustomizationList.get(1).getAttributeNameInArabic();
                        mListTwoImgStr = mCustomizationList.get(1).getImages();
                    }

                } else {
                    if (i == 0) {
                        mListOneTxtStr = mCustomizationList.get(0).getAttributeNameInEnglish();
                        mListOneImgStr = mCustomizationList.get(0).getImages();
                    } else if (i == 1) {
                        mListTwoTxtStr = mCustomizationList.get(1).getAttributeNameInEnglish();
                        mListTwoImgStr = mCustomizationList.get(1).getImages();
                    }

                }
            }


            if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
                getHintDialog();

            }
        }


        mCustomizeAdapter = new CustomizationThreeAdapter(this, mCustomizationList);
        mCustomizatinThreeRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mCustomizatinThreeRecyclerView.setAdapter(mCustomizeAdapter);

    }

    @OnClick({R.id.customization_three_next_btn, R.id.header_left_side_img, R.id.customization_three_spinner_lay, R.id.customization_three_front_img_lay, R.id.customization_three_back_img_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customization_three_next_btn:
                if (AppConstants.CUSTOMIZATION_CLICK_ARRAY.size() == mCountInt) {

                    AppConstants.MEASUREMENT_TYPE = "1";
                    nextScreen(AddMeasurementScreen.class, true);
                } else {
                    Toast.makeText(CustomizationThreeScreen.this, getResources().getString(R.string.please_select_all_the_customization), Toast.LENGTH_SHORT).show();
                    mCustomizationNextBtn.clearAnimation();
                    mCustomizationNextBtn.setAnimation(ShakeErrorUtils.shakeError());

                    if (mCustomizationDialogList.size() > 0) {
                        alertDismiss(mDialog);
                        mDialog = getDialog(CustomizationThreeScreen.this, R.layout.popup_country_alert);
                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mDialog.getWindow();
                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }
                        TextView cancelTxt, headerTxt;
                        RecyclerView countryRecyclerView;
                        cancelTxt = mDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mDialog.findViewById(R.id.country_popup_recycler_view);
                        mCustomizationThreeDialogAdapter = new CustomizationThreeDialogAdapter(CustomizationThreeScreen.this, mCustomizationDialogList, mDialog);

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(CustomizationThreeScreen.this));
                        countryRecyclerView.setAdapter(mCustomizationThreeDialogAdapter);

                        headerTxt.setText(getResources().getString(R.string.customize_your_own));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });

                        alertShowing(mDialog);
                    } else {
                        Toast.makeText(CustomizationThreeScreen.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.customization_three_spinner_lay:
                if (mCustomizationDialogList.size() > 0) {
                    alertDismiss(mDialog);
                    mDialog = getDialog(CustomizationThreeScreen.this, R.layout.popup_country_alert);
                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mDialog.getWindow();
                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }
                    TextView cancelTxt, headerTxt;
                    RecyclerView countryRecyclerView;
                    cancelTxt = mDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mDialog.findViewById(R.id.country_popup_recycler_view);
                    mCustomizationThreeDialogAdapter = new CustomizationThreeDialogAdapter(CustomizationThreeScreen.this, mCustomizationDialogList, mDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(CustomizationThreeScreen.this));
                    countryRecyclerView.setAdapter(mCustomizationThreeDialogAdapter);

                    headerTxt.setText(getResources().getString(R.string.customize_your_own));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });

                    alertShowing(mDialog);
                } else {
                    Toast.makeText(CustomizationThreeScreen.this,
                            getResources().getString(R.string.sorry_no_result_found),
                            Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.customization_three_front_img_lay:
                try {
                    Glide.with(CustomizationThreeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mCustomizationThreeFrontStr)
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCustomizationThreeFullImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
                mCustomizationThreeFrontImgLay.setBackgroundResource(R.color.app_color);
                mCustomizationThreeBackImgLay.setBackgroundResource(R.color.grey_clr);
                break;
            case R.id.customization_three_back_img_lay:

                try {
                    Glide.with(CustomizationThreeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mCustomizationThreeBackStr)
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCustomizationThreeFullImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                mCustomizationThreeFrontImgLay.setBackgroundResource(R.color.grey_clr);
                mCustomizationThreeBackImgLay.setBackgroundResource(R.color.app_color);
                break;


        }

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.customization));
        mRightSideImg.setVisibility(View.VISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));


    }

    public void getHintDialog() {
        alertDismiss(mCustomizationHintDialog);
        mCustomizationHintDialog = getDialog(CustomizationThreeScreen.this, R.layout.pop_up_customization_three_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mCustomizationHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mCustomizationHintDialog.findViewById(R.id.customization_three_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        } else {
            ViewCompat.setLayoutDirection(mCustomizationHintDialog.findViewById(R.id.customization_three_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        final Button hintBackBtn, hintSkipBtn, hintNextBtn;
        final TextView mHintLapelTxt, mSpinnerHintTxt, mListHintTxt, mListOneTxt, mListTwoTxt;
        final RelativeLayout mSpinnerParLay;
        final LinearLayout mListParLay;
        ImageView mListOneImg, mListTwoImg;
        /*Init view*/

        hintBackBtn = mCustomizationHintDialog.findViewById(R.id.back_hint_btn);
        hintSkipBtn = mCustomizationHintDialog.findViewById(R.id.skip_hint_btn);
        hintNextBtn = mCustomizationHintDialog.findViewById(R.id.next_hint_btn);
        mHintLapelTxt = mCustomizationHintDialog.findViewById(R.id.customization_three_spinner_hint_txt);
        mSpinnerParLay = mCustomizationHintDialog.findViewById(R.id.customization_three_spinner_par_lay);
        mSpinnerHintTxt = mCustomizationHintDialog.findViewById(R.id.customization_spinner_hint_txt);
        mListHintTxt = mCustomizationHintDialog.findViewById(R.id.customization_list_hint_txt);
        mListParLay = mCustomizationHintDialog.findViewById(R.id.customization_list_par_lay);
        mListOneTxt = mCustomizationHintDialog.findViewById(R.id.customization_list_txt_one);
        mListTwoTxt = mCustomizationHintDialog.findViewById(R.id.customization_list_txt_two);
        mListOneImg = mCustomizationHintDialog.findViewById(R.id.customization_list_img_one);
        mListTwoImg = mCustomizationHintDialog.findViewById(R.id.customization_list_img_two);

        try {

            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mListOneImgStr)
                    .apply(new RequestOptions().placeholder(R.color.grey_clr).error(R.color.grey_clr))
                    .into(mListOneImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        try {

            Glide.with(this)
                    .load(AppConstants.IMAGE_BASE_URL + "images/Customazation3/" + mListTwoImgStr)
                    .apply(new RequestOptions().placeholder(R.color.grey_clr).error(R.color.grey_clr))
                    .into(mListTwoImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        mHintLapelTxt.setText(getResources().getString(R.string.no_result_found));
        mHintLapelTxt.setText(mCustomizationHintLapelTxt);
        mListOneTxt.setText(mListOneTxtStr);
        mListTwoTxt.setText(mListTwoTxtStr);


        if (mCountHintInt == 1) {
            hintBackBtn.setVisibility(View.INVISIBLE);
            mListHintTxt.setVisibility(View.INVISIBLE);
            mListParLay.setVisibility(View.INVISIBLE);
        }

        /*Set data*/
        hintBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountHintInt = --mCountHintInt;
                if (mCountHintInt == 1) {
                    hintBackBtn.setVisibility(View.INVISIBLE);
                    hintNextBtn.setText(getResources().getString(R.string.next));
                    mSpinnerParLay.setVisibility(View.VISIBLE);
                    mSpinnerHintTxt.setVisibility(View.VISIBLE);
                    mListHintTxt.setVisibility(View.INVISIBLE);
                    mListParLay.setVisibility(View.INVISIBLE);

                } else if (mCountHintInt == 2) {
                    hintBackBtn.setVisibility(View.VISIBLE);
                    hintNextBtn.setText(getResources().getString(R.string.got_it));
                    mSpinnerParLay.setVisibility(View.INVISIBLE);
                    mSpinnerHintTxt.setVisibility(View.INVISIBLE);
                    mListHintTxt.setVisibility(View.VISIBLE);
                    mListParLay.setVisibility(View.VISIBLE);

                }
            }
        });

        hintNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountHintInt = ++mCountHintInt;
                if (mCountHintInt == 1) {
                    hintBackBtn.setVisibility(View.INVISIBLE);
                    hintNextBtn.setText(getResources().getString(R.string.next));
                    mSpinnerParLay.setVisibility(View.VISIBLE);
                    mSpinnerHintTxt.setVisibility(View.VISIBLE);
                    mListHintTxt.setVisibility(View.INVISIBLE);
                    mListParLay.setVisibility(View.INVISIBLE);


                } else if (mCountHintInt == 2) {
                    hintBackBtn.setVisibility(View.VISIBLE);
                    hintNextBtn.setText(getResources().getString(R.string.got_it));
                    mSpinnerParLay.setVisibility(View.INVISIBLE);
                    mSpinnerHintTxt.setVisibility(View.INVISIBLE);
                    mListHintTxt.setVisibility(View.VISIBLE);
                    mListParLay.setVisibility(View.VISIBLE);
                } else if (mCountHintInt >= 3) {
                    mCustomizationHintDialog.dismiss();
                }
            }
        });

        hintSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCustomizationHintDialog.dismiss();
            }
        });

        alertShowing(mCustomizationHintDialog);
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_three_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_three_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();
        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
}
