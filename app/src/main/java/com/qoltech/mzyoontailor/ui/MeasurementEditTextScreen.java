package com.qoltech.mzyoontailor.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MeasurementEditTextScreen  extends BaseActivity {

    @BindView(R.id.measurement_edt_txt_par_lay)
    RelativeLayout mMeasurementEdtTxtParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.scale_cm_edt_txt)
    EditText mScaleCmEdtTxt;

    @BindView(R.id.measurement_scale_body_img)
    ImageView mMeasurementScalseBodyImg;

    @BindView(R.id.scale_measurement_ok_btn)
    Button mScaleOkBtn;

    @BindView(R.id.scale_measurement_cancel_btn)
    Button mScaleCancelBtn;

    private UserDetailsEntity mUserDetailsEntityRes;

    String mBackHandleStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_edit_txt_screen);

        initView();
    }
    public void initView() {

        ButterKnife.bind(this);

        setupUI(mMeasurementEdtTxtParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MeasurementEditTextScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

    }


    @OnClick({R.id.header_left_side_img,R.id.scale_measurement_ok_btn,R.id.scale_measurement_cancel_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
//                mBackHandleStr = "true";
//                DialogManager.getInstance().showOptionPopup(MeasurementEditTextScreen.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                    @Override
//                    public void onNegativeClick() {
//                        mBackHandleStr = "false";
//
//                    }
//                    @Override
//                    public void onPositiveClick() {
                onBackPressed();
//                    }
//                });
                break;
            case R.id.scale_measurement_ok_btn:
                if (isEmailValid(mScaleCmEdtTxt.getText().toString().trim())){
                    DecimalFormat dtime = new DecimalFormat("###.#");
                    Float values = Float.parseFloat(mScaleCmEdtTxt.getText().toString().trim());
                    Float multiplyValue = Float.parseFloat("2.5");
                    if (AppConstants.SelectedPartsId.equalsIgnoreCase("1")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.HEAD = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.HEAD = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.HEAD_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("2")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.NECK = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.NECK = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.NECK_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("3")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.CHEST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.CHEST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.CHEST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.CHEST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("4")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WAIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WAIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WAIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("5")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.THIGH = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.THIGH = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.THIGH_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("7")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.ANKLE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.ANKLE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.ANKLE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("6")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.KNEE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.KNEE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.KNEE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("8")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.OVER_ALL_HEIGHT = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.OVER_ALL_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.OVER_ALL_HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.OVER_ALL_HEIGHT_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("9")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.SHORTS = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.SHORTS_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.SHORTS = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.SHORTS_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("10")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.OUTSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.OUTSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.OUTSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("11")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.INSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.INSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.INSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("12")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.SHOULDER = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.SHOULDER = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.SHOULDER_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("13")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.HALF_SLEEVE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.HALF_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.HALF_SLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.HALF_SLEEVE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("14")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BICEP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BICEP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BICEP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("15")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.HIP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.HIP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.HIP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("16")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOTTOM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOTTOM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOTTOM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOTTOM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("17")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.HEIGHT = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.HEIGHT_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("18")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.SLEEVE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.SLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.SLEEVE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("19")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WRIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WRIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WRIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("21")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_OVER_BUST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_OVER_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_OVER_BUST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_OVER_BUST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("22")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_UNDER_BUST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_UNDER_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_UNDER_BUST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_UNDER_BUST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("23")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_HIP_BONE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_HIP_BONE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_HIP_BONE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_HIP_BONE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("24")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_THIGH = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_THIGH = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_THIGH_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("25")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_KNEE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_KNEE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_KNEE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("26")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_CALF = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_CALF_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_CALF = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_CALF_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("27")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_ANKLE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_ANKLE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_ANKLE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("20")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_HEAD = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_HEAD = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_HEAD_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("28")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_NECK = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_NECK = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_NECK_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("29")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_BUST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_BUST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_BUST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("30")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_WAIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_WAIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_WAIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("31")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_FULL_HIP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_FULL_HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_FULL_HIP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_FULL_HIP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("32")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_HEIGHT = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_HEIGHT_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("33")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_STW = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_STW_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_STW = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_STW_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("34")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_NLTC = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_NLTC_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_NLTC = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_NLTC_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("35")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMENT_NLTB = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMENT_NLTB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMENT_NLTB = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMENT_NLTB_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("36")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_STHB = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_STHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_STHB = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_STHB_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("37")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_WTHB = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_WTHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_WTHB = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_WTHB_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("38")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_HTH = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_HTH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_HTH = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_HTH_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("39")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_INSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_INSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_INSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("40")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_OUTSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_OUTSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_OUTSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("41")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_SHOULDER = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_SHOULDER = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_SHOULDER_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("42")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_BICEP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_BICEP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_BICEP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("43")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_WRIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_WRIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_WRIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("44")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.WOMEN_SLEEVE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.WOMEN_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.WOMEN_SLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.WOMEN_SLEEVE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("45")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_HEAD = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_HEAD = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_HEAD_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("46")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_NECK = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_NECK = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_NECK_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("47")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_CHEST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_CHEST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_CHEST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_CHEST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("48")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_WAIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_WAIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_WAIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("49")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_THIGH = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_THIGH = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_THIGH_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("50")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_KNEE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_KNEE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_KNEE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("51")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_ANKLE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_ANKLE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_ANKLE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("52")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_OVER_ALL_HEIGHT = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_OVER_ALL_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_OVER_ALL_HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_OVER_ALL_HEIGHT_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("53")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_SHORTS = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_SHORTS_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_SHORTS = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_SHORTS_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("54")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_OUTSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_OUTSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_OUTSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("55")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_INSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_INSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_INSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("56")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_SHOULDER = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_SHOULDER = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_SHOULDER_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("57")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_HALFSLEEVE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_HALFSLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_HALFSLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_HALFSLEEVE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("58")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_BICEP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_BICEP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_BICEP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("59")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_HIP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_HIP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_HIP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("60")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_BOTTOM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_BOTTOM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_BOTTOM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_BOTTOM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("61")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_HEIGHT = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_HEIGHT= String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_HEIGHT_INCHES =  mScaleCmEdtTxt.getText().toString().trim();
                        }

                    }
                    else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("62")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_SLEEVE = AppConstants.MEASUREMENT_LENGTH;
                            AppConstants.BOY_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_SLEEVE= String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_SLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                        }

                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("63")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.BOY_WRIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.BOY_WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.BOY_WRIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.BOY_WRIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("65")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_OVER_BURST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_OVER_BURST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_OVER_BURST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_OVER_BURST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("66")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_UNDER_BURST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_UNDER_BURST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_UNDER_BURST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_UNDER_BURST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("67")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_HIP_BONE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_HIP_BONE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_HIP_BONE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_HIP_BONE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("68")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_THIGH = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_THIGH = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_THIGH_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("69")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_KNEE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_KNEE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_KNEE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("70")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_CALF = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_CALF_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_CALF = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_CALF_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("71")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_ANKLE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_ANKLE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_ANKLE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("64")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_HEAD = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_HEAD = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_HEAD_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("72")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_NECK = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_NECK = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_NECK_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("73")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_BUST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_BUST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_BUST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("74")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_WAIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_WAIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_WAIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("75")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_FULL_HIP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_FULL_HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_FULL_HIP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_FULL_HIP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("76")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_HEIGHT = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_HEIGHT_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("77")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_STW = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_STW_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_STW = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_STW_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("78")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_NLTC = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_NLTC_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_NLTC = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_NLTC_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("79")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_NLTB = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_NLTB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_NLTB = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_NLTB_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("80")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_STHB = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_STHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_STHB = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_STHB_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("81")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_WTHB = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_WTHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_WTHB = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_WTHB_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("83")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_INSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_INSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_INSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("84")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_OUTSEAM = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_OUTSEAM = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_OUTSEAM_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("85")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_SHOULDER = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_SHOULDER = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_SHOULDER_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("86")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_BICEP = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_BICEP = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_BICEP_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("87")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_WRIST = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_WRIST = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_WRIST_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    else if (AppConstants.SelectedPartsId.equalsIgnoreCase("88")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                            AppConstants.GIRL_SLEEVE = mScaleCmEdtTxt.getText().toString().trim();
                            AppConstants.GIRL_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                        }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                            AppConstants.GIRL_SLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                            AppConstants.GIRL_SLEEVE_INCHES = mScaleCmEdtTxt.getText().toString().trim();
                        }
                    }
                    AppConstants.SelectedPartsEditValue = "";
                    mBackHandleStr = "true";
                    onBackPressed();
                }else {
                    mScaleCmEdtTxt.clearAnimation();
                    mScaleCmEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        mScaleCmEdtTxt.setError("يرجى إعطاء سم مناسب أو IN (EX: 00.0)");
                    }else {
                        mScaleCmEdtTxt.setError(getResources().getString(R.string.please_give_proper_cm_or_in));
                    }
                }

                break;
            case R.id.scale_measurement_cancel_btn:
//                mBackHandleStr = "true";
//
//                DialogManager.getInstance().showOptionPopup(MeasurementEditTextScreen.this, getResources().getString(R.string.sure_want_to_cancel), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                    @Override
//                    public void onNegativeClick() {
//                        mBackHandleStr = "false";
//
//                    }
//
//                    @Override
//                    public void onPositiveClick() {
                onBackPressed();
//                    }
//                });
                break;
        }

    }

    public static boolean isEmailValid(String measurement) {
        String expression = "([0-9]{1,3}+\\.)+[0-9]{1}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(measurement);
        return matcher.matches();
    }


    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText( AppConstants.SelectedPartsName);
        mRightSideImg.setVisibility(View.VISIBLE);

        mBackHandleStr = "false";

        mScaleCmEdtTxt.setText( AppConstants.SelectedPartsEditValue);
        try {
            Glide.with(MeasurementEditTextScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+AppConstants.SelectedPartsImage)
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(mMeasurementScalseBodyImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mScaleOkBtn.setText("حفظ");
            mScaleCancelBtn.setText("إلغاء");
        }
    }

    @Override
    public void onBackPressed() {
//        if (mBackHandleStr.equalsIgnoreCase("true")){
//            super.onBackPressed();
//            AppConstants.SelectedPartsEditValue = "";
//            this.overridePendingTransition(R.anim.animation_f_enter,
//                    R.anim.animation_f_leave);
//        }else {
//            DialogManager.getInstance().showOptionPopup(MeasurementEditTextScreen.this, getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                @Override
//                public void onNegativeClick() {
//
//                }
//
//                @Override
//                public void onPositiveClick() {
//                    mBackHandleStr = "true";
//                    onBackPressed();
//                }
//            });
//        }
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
        AppConstants.SelectedPartsEditValue = "";
//        onBackPressed();


    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }


}