package com.qoltech.mzyoontailor.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.TailorListAdapter;
import com.qoltech.mzyoontailor.entity.TailorListEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.TailorListResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TailorListScreen extends  BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private ArrayList<TailorListEntity> mTailorList;

    @BindView(R.id.tailor_recycler_view)
    RecyclerView mTailorRecyclerView;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.tailor_selected_txt)
    public TextView mTailorSelectedTxt;

    @BindView(R.id.tailor_list_view_lay)
    RelativeLayout mTailorListViewLay;

    @BindView(R.id.tailor_map_view_lay)
    RelativeLayout mTailorMapViewLay;

    @BindView(R.id.tailor_list_lay)
    RelativeLayout mTailorListlay;

    @BindView(R.id.tailor_map_lay)
    RelativeLayout mTailorMapLay;

    @BindView(R.id.tailor_list_txt)
    TextView mTailorListTxt;

    @BindView(R.id.tailor_map_txt)
    TextView mTailorMapTxt;

    @BindView(R.id.tailor_list_total_list_txt)
    TextView mTailorListTotalListTxt;

    @BindView(R.id.tailor_send_req_btn)
    Button mTailorSendReqBtn;

    private TailorListAdapter mTailorAdapter;

    private GoogleMap mGoogleMap;

    Location mLastLocation;
    Marker mCurrLocationMarker;
    SupportMapFragment mapFrag;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_tailor_list_screen);

        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setHeader();

        mapFrag = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.tailor_map);
        mapFrag.getMapAsync(this);

        mTailorList = new ArrayList<>();
        AppConstants.ADD_MATERIAL = "";


        getTailorList();
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText("TAILOR LIST");
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    public void getTailorList(){

        if (NetworkUtil.isNetworkAvailable(TailorListScreen.this)){
            APIRequestHandler.getInstance().getTailorListApiCall(TailorListScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(TailorListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getTailorList();
                }
            });
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof TailorListResponse){
            TailorListResponse mResponse = (TailorListResponse)resObj;

            setAdapter(mResponse.getResult());
            mTailorListTotalListTxt.setText(String.valueOf(mResponse.getResult().size()));
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img, R.id.tailor_send_req_btn, R.id.tailor_list_lay, R.id.tailor_map_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.tailor_send_req_btn:
                if (AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size()>0){
                    nextScreen(OrderSummaryScreen.class, true);
                }else {
                    Toast.makeText(TailorListScreen.this,"Please select atleast one tailor",Toast.LENGTH_SHORT).show();
                    mTailorSendReqBtn.clearAnimation();
                    mTailorSendReqBtn.setAnimation(ShakeErrorUtils.shakeError());
                }
                break;
            case R.id.tailor_list_lay:
                mTailorListViewLay.setVisibility(View.VISIBLE);
                mTailorMapViewLay.setVisibility(View.GONE);
                mTailorListlay.setBackgroundResource(R.color.slider_blue);
                mTailorListTxt.setTextColor(getResources().getColor(R.color.white));
                mTailorMapLay.setBackgroundResource(R.color.grey_clr);
                mTailorMapTxt.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.tailor_map_lay:
                mTailorListViewLay.setVisibility(View.GONE);
                mTailorMapViewLay.setVisibility(View.VISIBLE);
                mTailorListlay.setBackgroundResource(R.color.grey_clr);
                mTailorListTxt.setTextColor(getResources().getColor(R.color.black));
                mTailorMapLay.setBackgroundResource(R.color.slider_blue);
                mTailorMapTxt.setTextColor(getResources().getColor(R.color.white));
                break;
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<TailorListEntity> mTailorList) {

        if (mTailorAdapter == null) {


            mTailorAdapter = new TailorListAdapter(this, mTailorList);
            mTailorRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mTailorRecyclerView.setAdapter(mTailorAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTailorAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

        //move map camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        }
        else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(TailorListScreen.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();
    }
}