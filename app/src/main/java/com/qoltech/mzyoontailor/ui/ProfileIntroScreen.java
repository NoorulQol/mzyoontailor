package com.qoltech.mzyoontailor.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;
import com.qoltech.mzyoontailor.wrappers.ImageResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileIntroScreen extends BaseActivity {


    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.profile_intro_edt_txt)
    EditText mProfileNameEdtTxt;

    @BindView(R.id.name_arabic)
    EditText name_arabic;


    @BindView(R.id.email_edittext)
    EditText email_edittext;


    @BindView(R.id.profile_intro_image)
    de.hdodenhof.circleimageview.CircleImageView mProfileIntroImg;

    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;
    private Uri mPictureFileUri;

    private String mUserProfileImagePath = "";
    private File mUserImageFile = null;
    TransparentProgressDialog progressDialog;
    private UserDetailsEntity mUserDetailsEntity;
    ApiService restService;
    SignUpResponse signUp = new SignUpResponse();
    SharedPreferences sharedPreferences, sharedPreferences1;

    int i = 1;
    Uri selectedImagePath;
    String IMAGE_PATH = "";
    private static final String IMAGE_DIRECTORY = "/MZYOON";
    SharedPreferences sharedPreferencesImageName;
    SharedPreferences.Editor editor, editorName;
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    FrameLayout country_code_lay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_profile_intro_screen);
        initView();


        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        sharedPreferences = getSharedPreferences("profileIntroId", MODE_PRIVATE);
        sharedPreferences1 = getSharedPreferences("TailorId", MODE_PRIVATE);
        sharedPreferencesImageName = getSharedPreferences("ImageName", MODE_PRIVATE);
        editor = sharedPreferencesImageName.edit();
        editorName = sharedPreferences1.edit();
        getLanguage();
    }

    public void initView() {
        ButterKnife.bind(this);

        mUserDetailsEntity = new UserDetailsEntity();
        setHeader();
        restService = ((MainApplication) getApplication()).getClient();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img, R.id.profile_intro_nxt_btn, R.id.profile_intro_img_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.profile_intro_nxt_btn:

                if (mProfileNameEdtTxt.getText().toString().trim().isEmpty() &&
                        name_arabic.getText().toString().trim().isEmpty()
                        && email_edittext.getText().toString().trim().isEmpty() &&
                        IMAGE_PATH.equalsIgnoreCase("")) {
                    mProfileNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    name_arabic.setAnimation(ShakeErrorUtils.shakeError());
                    email_edittext.setAnimation(ShakeErrorUtils.shakeError());
//                    mProfileIntroImg.setAnimation(ShakeErrorUtils.shakeError());
                } else if (mProfileNameEdtTxt.getText().toString().trim().isEmpty()) {

                    mProfileNameEdtTxt.clearAnimation();
                    mProfileNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mProfileNameEdtTxt.setError("Please fill your name");

                } else if (name_arabic.getText().toString().trim().isEmpty()) {
                    name_arabic.clearAnimation();
                    name_arabic.setAnimation(ShakeErrorUtils.shakeError());
                    name_arabic.setError("Please fill your name");

                } else if (email_edittext.getText().toString().trim().isEmpty() ||
                        (!isEmailValid(email_edittext.getText().toString().trim()))) {
                    email_edittext.clearAnimation();
                    email_edittext.setAnimation(ShakeErrorUtils.shakeError());
                    email_edittext.setError("Please Enter Valid email");
                } else if (IMAGE_PATH.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Please Select A Image", Toast.LENGTH_LONG).show();
                } else {


                    if (sharedPreferences1.getString("TailorId", "").isEmpty()) {

                        Toast.makeText(getApplicationContext(), "Something Went Wrong Try Again", Toast.LENGTH_LONG).show();
                        finishAffinity();

                    } else {
                        updateProfile1();
                    }
                }
                break;
            case R.id.profile_intro_img_lay:
                if (checkPermission()) {
                    uploadImages();

                }
                break;
        }

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.INVISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.profile);
        mRightSideImg.setVisibility(View.INVISIBLE);

    }


    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }


                @Override
                public void onPositiveClick() {
                    uploadImages();
                }


            });
        }

        return addPermission;

    }

    private void uploadImages() {
        DialogManager.getInstance().showImageUploadPopup(this, "Select Photo Type", "Take Camera", "Open Gallery", new InterfaceTwoBtnCallBack() {
            @Override
            public void onNegativeClick() {
                captureImage();
            }

            @Override
            public void onPositiveClick() {
                galleryImage();
            }
        });
    }

    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    String path = saveImage(thumbnail);
                    IMAGE_PATH = compressImage(String.valueOf(getImageUri(getApplicationContext(), thumbnail)));
//                    IMAGE_PATH = path;
//                    saveImage(thumbnail);

                    Glide.with(this)
                            .load(IMAGE_PATH)
                            .into(mProfileIntroImg);

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }


                }

                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    // successfully captured the image
                    // display it in image view
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), contentURI);
//                        String path = saveImage(bitmap);
                        IMAGE_PATH = compressImage(String.valueOf(contentURI));

                        Glide.with(this)
                                .load(compressImage(String.valueOf(contentURI)))
                                .into(mProfileIntroImg);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;

        }
    }

//    public String saveImage(Bitmap myBitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//        File wallpaperDirectory = new File(
//                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
//        // have the object build the directory structure, if needed.
//        if (!wallpaperDirectory.exists()) {
////            if (is() == true) {
//            wallpaperDirectory.mkdirs();
//
////            }
//        }
//
//        try {
//            File f = new File(wallpaperDirectory, Calendar.getInstance()
//                    .getTimeInMillis() + ".jpg");
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//            MediaScannerConnection.scanFile(getApplicationContext(),
//                    new String[]{f.getPath()},
//                    new String[]{"image/jpeg"}, null);
//            fo.close();
//            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());
//
//            return f.getAbsolutePath();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        return "";
//    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;

        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public void updateProfile1() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {


            try {
                mUserImageFile = new File(IMAGE_PATH);

                // create RequestBody instance from file
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("TailorImage", mUserImageFile.getName(), requestFile);
                restService.imageUpload2(body).enqueue(new Callback<ImageResponse>() {

                    @Override
                    public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                        if (response.isSuccessful()) {


                            String[] parts = response.body().getResult().get(0).split("\\\\");
                            String part1 = parts[0]; // 004
                            String part2 = parts[1];
                            String lastOne = parts[parts.length - 1];
                            editor.putString("ImageName", lastOne);
                            editor.commit();
                            try {
                                HashMap<String, String> map = new HashMap<>();
                                map.put("TailorId", sharedPreferences1.getString("TailorId", ""));
                                map.put("TailorNameInEnglish", mProfileNameEdtTxt.getText().toString());
                                map.put("TailorNameInArabic", name_arabic.getText().toString());
                                map.put("Email", email_edittext.getText().toString());
                                map.put("ShopOwnerImageURL", lastOne);

                                restService.profileIntroScreen(map).enqueue(new Callback<SignUpResponse>() {

                                    @Override
                                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                                        signUp = response.body();
                                        if (progressDialog != null && progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                        if (response.isSuccessful()) {
                                            if (signUp != null && signUp
                                                    .getResponseMsg().trim().equalsIgnoreCase("Success")) {

                                                editorName.putString("TailorName", mProfileNameEdtTxt.getText().toString().trim());
                                                editorName.commit();
                                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                                finish();


                                            } else if (signUp != null) {
                                                Toast.makeText(getBaseContext(), signUp.getResponseMsg(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                                        if (progressDialog != null && progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }

                                        Toast.makeText(getBaseContext(), "Connection Failure, try again!" + t.toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<ImageResponse> call, Throwable t) {


                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void updateProfileWithoutImage() {

        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences1.getString("TailorId", "1"));
            map.put("TailorNameInEnglish", mProfileNameEdtTxt.getText().toString());
            map.put("TailorNameInArabic", mProfileNameEdtTxt.getText().toString());

            if (IMAGE_PATH.equalsIgnoreCase("") && sharedPreferencesImageName.getString("ImageName", "") != null) {
                map.put("ShopOwnerImageURL", "null");

            } else if (IMAGE_PATH.equalsIgnoreCase("") && sharedPreferencesImageName.getString("ImageName", "") == null) {
                map.put("ShopOwnerImageURL", sharedPreferencesImageName.getString("ImageName", ""));

            }
            restService.profileIntroScreen(map).enqueue(new Callback<SignUpResponse>() {

                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                    signUp = response.body();
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    if (response.isSuccessful()) {
                        if (signUp != null && signUp
                                .getResponseMsg().trim().equalsIgnoreCase("Success")) {

                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            finish();


                        } else if (signUp != null) {
                            Toast.makeText(getBaseContext(), signUp.getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    Toast.makeText(getBaseContext(), "Connection Failure, try again!" + t.toString(), Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.total_edittext_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            country_code_lay.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
//            phone_number.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);
//            updateResources(getApplicationContext(), "ar");
//            mobile_num_txt.setHint(getResources().getString(R.string.mobile_num));
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.total_edittext_layout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(getApplicationContext(), "en");


        }


//        submitButton.setText(getResources().getString(R.string.contin));
//        login_choose_language_txt.setText(getResources().getString(R.string.choose_location));
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        editorName.remove("TailorName");
        editorName.clear();
        editorName.commit();
        super.onBackPressed();


    }

//    @Override
//    protected void onStop() {
//        editorName.clear();
////        editorName.remove("TailorId");
//        editorName.commit();
//        super.onStop();
//    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//
//        super.onDestroy();
//
//    }


//    PreferenceManager.OnActivityStopListener onActivityStopListener=new PreferenceManager.OnActivityStopListener() {
//        @Override
//        public void onActivityStop() {
//            editorName.clear();
//////        editorName.remove("TailorId");
//            editorName.commit();
//        }
//    };


    @Override
    protected void onStop() {

//        if (isFinishing()){
        editorName.clear();
////        editorName.remove("TailorId");
        editorName.commit();
//        }
//        onDestroy();

        finishAffinity();
        super.onStop();
    }
}
