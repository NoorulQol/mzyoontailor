package com.qoltech.mzyoontailor.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.FilterTypeAdapter;
import com.qoltech.mzyoontailor.entity.FilterTypeEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterTypeScreen extends BaseActivity {

    ArrayList<FilterTypeEntity> mFilterEntity;

    @BindView(R.id.filter_type_recycler_view)
    RecyclerView mFilterRecyclerView;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private FilterTypeAdapter mFilterTypeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_filter_selection);

        initView();



    }

    public void initView(){

        ButterKnife.bind(this);

        setHeader();

        if (AppConstants.FILTER_TYPE.equalsIgnoreCase("GENDER")){
            GenderData();
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
            OccasionData();
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("PRICE")){
            PriceData();
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
            RegionData();
        }
    }

    private void GenderData() {
        mFilterEntity = new ArrayList<>();

        FilterTypeEntity filterList = new FilterTypeEntity("Men", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Women", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Boy", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Girl", false);
        mFilterEntity.add(filterList);

        setAdapter(mFilterEntity);

        }
    private void OccasionData() {
        mFilterEntity = new ArrayList<>();

        FilterTypeEntity filterList = new FilterTypeEntity("Ceremonial Dress", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("WaistCoat", false);
        mFilterEntity.add(filterList);

        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Religion Clothing", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Office Parties", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Business Lunch Meeting", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Engagement parties", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Social Event", false);
        mFilterEntity.add(filterList);

        setAdapter(mFilterEntity);

    }
    private void PriceData() {
        mFilterEntity = new ArrayList<>();

        FilterTypeEntity filterList = new FilterTypeEntity("Below AED 199", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 200-499 ", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 500-799", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 900-1199", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 1200-1499", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 1500-1799", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 1800-1999", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 2000-4999", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 5000-7999", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("AED 8000-10000", false);
        mFilterEntity.add(filterList);

        setAdapter(mFilterEntity);

    }
    private void RegionData() {
        mFilterEntity = new ArrayList<>();

        FilterTypeEntity filterList = new FilterTypeEntity("India", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Afghanistan", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Belgium", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("China", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Iraq", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Kuwait", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Saudi Arabia", false);
        mFilterEntity.add(filterList);

        filterList = new FilterTypeEntity("Malaysia", false);
        mFilterEntity.add(filterList);

        setAdapter(mFilterEntity);

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);

        if (AppConstants.FILTER_TYPE.equalsIgnoreCase("GENDER")){
            mHeaderTxt.setText("Gender");
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
            mHeaderTxt.setText("Occasion");
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("PRICE")){
            mHeaderTxt.setText("Price");
        }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
            mHeaderTxt.setText("Region");
        }
        mRightSideImg.setVisibility(View.VISIBLE);

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img,R.id.filter_type_apply_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.filter_type_apply_btn:
                onBackPressed();
                break;

        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<FilterTypeEntity> mTailorList) {

        if (mFilterTypeAdapter == null) {


            mFilterTypeAdapter = new FilterTypeAdapter(this,mTailorList);
            mFilterRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mFilterRecyclerView.setAdapter(mFilterTypeAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mFilterTypeAdapter.notifyDataSetChanged();
                }
            });
        }

    }
}
