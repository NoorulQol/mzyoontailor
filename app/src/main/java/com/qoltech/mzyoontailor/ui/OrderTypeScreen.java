package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.OrderTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.OrderTypeResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderTypeScreen extends BaseActivity {

    @BindView(R.id.order_type_par_lay)
    LinearLayout mOrderTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.own_material_direct_txt)
    TextView mOwnMaterialDirectTxt;

    @BindView(R.id.own_courier_txt)
    TextView mOwnMaterialCourierTxt;

    @BindView(R.id.companies_material_txt)
    TextView mCompaniesMaterialTxt;

    @BindView(R.id.own_delivery_bodyimg)
    ImageView mOwnDeliveryBodyImg;

    @BindView(R.id.own_courier_body_img)
    ImageView mOwnCourierBodyImg;

    @BindView(R.id.companies_material_body_img)
    ImageView mCompaniesMaterialBodyImg;

    @BindView(R.id.own_delviery_img)
    ImageView mOwnDeliveryImg;

    @BindView(R.id.own_courier_img)
    ImageView mOwnCourierImg;

    @BindView(R.id.company_img)
    ImageView mCompanyImg;


    @BindView(R.id.own_material_direct_delivery_lay)
    LinearLayout mOwnMaterialDirectLay;

    @BindView(R.id.own_material_courier_lay)
    LinearLayout mOwnMaterialCourierLay;

    @BindView(R.id.companies_material_lay)
    LinearLayout mCompaniesMaterialLay;


    @BindView(R.id.own_delivery_body_empty_lay)
    TextView mOwnDeliveryBodyEmptyLay;

    @BindView(R.id.own_courier_body_empty_lay)
    TextView mOwnCourierBodyEmptyLay;

    @BindView(R.id.companies_material_body_empty_lay)
    TextView mCompaniesMaterialBodyEmptyLay;
    ArrayList<OrderTypeEntity> mOrderTypeList;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mHintDialog;

    int mCountInt = 1;
SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_type_screen);
        sharedPreferences=getSharedPreferences("TailorId",MODE_PRIVATE);
        initView();


    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setupUI(mOrderTypeParLay);

        setHeader();

        mOrderTypeList = new ArrayList<>();
        NewFlowOrderTypeApiCall();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();


        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
            getHintDialog();

        }
    }

    @OnClick({R.id.own_material_direct_delivery_lay, R.id.own_material_courier_lay, R.id.companies_material_lay, R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.own_material_direct_delivery_lay:

//                if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {
//                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(2).getId());
//                    AppConstants.DIRECT_ORDER_TYPE = "Own Material-Direct Delivery";
//                    nextScreen(AddMaterialScreen.class, true);
//                } else {
//                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(0).getId());
//                    AppConstants.DIRECT_ORDER_TYPE = "Own Material-Direct Delivery";
//                    nextScreen(AddMaterialScreen.class, true);
//                }



                if (mOrderTypeList.get(0).isStatus()){
                    AppConstants.ORDER_TYPE_ID =String.valueOf(mOrderTypeList.get(0).getId());
                    AppConstants.DIRECT_ORDER_TYPE = mOrderTypeList.get(0).getHeaderInEnglish();
                    if (mOrderTypeList.get(0).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")){
                        nextScreen(AddMaterialScreen.class,true);

                    }
                    else {
                        AppConstants.PATTERN_ID = "0";
                        nextScreen(AddMaterialScreen.class,true);
                    }
                }

                break;
            case R.id.own_material_courier_lay:
//                if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {
//                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(2).getId());
//                    AppConstants.DIRECT_ORDER_TYPE = "Own Material-Courier the Material";
//                    nextScreen(AddMaterialScreen.class, true);
//                } else {
//                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(1).getId());
//                    AppConstants.DIRECT_ORDER_TYPE = "Own Material-Courier the Material";
//                    nextScreen(AddMaterialScreen.class, true);
//                }
                if (mOrderTypeList.get(1).isStatus()) {
                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(1).getId());
                    AppConstants.DIRECT_ORDER_TYPE = mOrderTypeList.get(1).getHeaderInEnglish();
                    if (mOrderTypeList.get(1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")) {
                        nextScreen(AddMaterialScreen.class, true);

                    }
                    else {
                        AppConstants.PATTERN_ID = "0";
                        nextScreen(AddMaterialScreen.class, true);
                    }
                }

                break;
            case R.id.companies_material_lay:


//                if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {
//                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(2).getId());
//                    AppConstants.DIRECT_ORDER_TYPE = "Companies-Material";
//                    nextScreen(CustomizationOneScreen.class, true);
//                } else {
//                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(2).getId());
//                    AppConstants.DIRECT_ORDER_TYPE = "Companies-Material";
//                    nextScreen(CustomizationOneScreen.class, true);
//                }

                if (mOrderTypeList.get(2).isStatus()) {
                    AppConstants.ORDER_TYPE_ID = String.valueOf(mOrderTypeList.get(2).getId());
                    AppConstants.DIRECT_ORDER_TYPE = mOrderTypeList.get(2).getHeaderInEnglish();
                    if (mOrderTypeList.get(2).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")) {
                        nextScreen(CustomizationOneScreen.class, true);

                    }
                    else {
                        AppConstants.PATTERN_ID = "0";
                        nextScreen(CustomizationOneScreen.class, true);
                    }
                }


                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void OrderTypeApiCall() {

        if (NetworkUtil.isNetworkAvailable(OrderTypeScreen.this)) {
            APIRequestHandler.getInstance().getOrderTypeAPICall(OrderTypeScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    OrderTypeApiCall();
                }
            });
        }

    }



    public void getNewFlowOrderTypeApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderTypeScreen.this)) {
            APIRequestHandler.getInstance().getNewFlowOrderTypeApiCall(sharedPreferences.getString("TailorId",""),OrderTypeScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowOrderTypeApiCall();
                }
            });
        }
    }

    public void NewFlowOrderTypeApiCall(){

        if (NetworkUtil.isNetworkAvailable(OrderTypeScreen.this)){
            APIRequestHandler.getInstance().
                    getNewFlowOrderTypeApiCall(sharedPreferences.getString("TailorId",""),
                    OrderTypeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    NewFlowOrderTypeApiCall();
                }
            });
        }

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.order_type).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof OrderTypeResponse) {
            OrderTypeResponse mResponse = (OrderTypeResponse) resObj;
            mOrderTypeList = mResponse.getResult();
            setOrderTypeImgTitle(mOrderTypeList);
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

//    public void setOrderTypeImgTitle(ArrayList<OrderTypeEntity> OrderTypeList) {
//
//        mOrderTypeList = OrderTypeList;
//
//        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//            mOwnMaterialDirectTxt.setText(OrderTypeList.get(0).getHeaderInArabic());
//            mOwnMaterialCourierTxt.setText(OrderTypeList.get(1).getHeaderInArabic());
//            mCompaniesMaterialTxt.setText(OrderTypeList.get(2).getHeaderInArabic());
//
//        } else {
//            mOwnMaterialDirectTxt.setText(OrderTypeList.get(0).getHeaderInEnglish());
//            mOwnMaterialCourierTxt.setText(OrderTypeList.get(1).getHeaderInEnglish());
//            mCompaniesMaterialTxt.setText(OrderTypeList.get(2).getHeaderInEnglish());
//
//        }
//
//
//        try {
//            Glide.with(OrderTypeScreen.this)
//                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + OrderTypeList.get(0).getHeaderImage())
//                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                    .into(mOwnDeliveryImg);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
//        try {
//            Glide.with(OrderTypeScreen.this)
//                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + OrderTypeList.get(1).getHeaderImage())
//                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                    .into(mOwnCourierImg);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
//
//        try {
//            Glide.with(OrderTypeScreen.this)
//                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + OrderTypeList.get(2).getHeaderImage())
//                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                    .into(mCompanyImg);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
//
//        try {
//            Glide.with(OrderTypeScreen.this)
//                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + OrderTypeList.get(0).getBodyImage())
//                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                    .into(mOwnDeliveryBodyImg);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
//        try {
//            Glide.with(OrderTypeScreen.this)
//                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + OrderTypeList.get(1).getBodyImage())
//                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                    .into(mOwnCourierBodyImg);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
//        try {
//            Glide.with(OrderTypeScreen.this)
//                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + OrderTypeList.get(2).getBodyImage())
//                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                    .into(mCompaniesMaterialBodyImg);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
//
//        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
//            getHintDialog();
//
//        }
//
//    }

    public void setOrderTypeImgTitle(ArrayList<OrderTypeEntity> OrderTypeList){

        mOwnMaterialDirectLay.setVisibility(View.VISIBLE);
        mOwnMaterialCourierLay.setVisibility(View.GONE);
        mCompaniesMaterialLay.setVisibility(View.VISIBLE);
        mOwnDeliveryBodyImg.setVisibility(View.VISIBLE);
        mOwnCourierBodyImg.setVisibility(View.GONE);
        mCompaniesMaterialBodyImg.setVisibility(View.VISIBLE);

        for (int i=0; i<OrderTypeList.size(); i++){
            if (!OrderTypeList.get(i).isStatus()){
                if (i == 0){
                    mOwnDeliveryBodyEmptyLay.setVisibility(View.VISIBLE);
                    mOwnDeliveryBodyImg.setAlpha(30);

                }else if (i == 1){
                    mOwnCourierBodyEmptyLay.setVisibility(View.INVISIBLE);
                    mOwnCourierBodyImg.setAlpha(30);

                }else if (i==2){
                    mCompaniesMaterialBodyEmptyLay.setVisibility(View.VISIBLE);
                    mCompaniesMaterialBodyImg.setAlpha(30);

                }
            }else {
                if (i == 0){
                    mOwnDeliveryBodyEmptyLay.setVisibility(View.GONE);
//                        mOwnDeliveryBodyImg.setVisibility(View.VISIBLE);
                }else if (i == 1){
                    mOwnCourierBodyEmptyLay.setVisibility(View.GONE);
//                        mOwnCourierBodyImg.setVisibility(View.VISIBLE);
                }else if (i==2){
                    mCompaniesMaterialBodyEmptyLay.setVisibility(View.GONE);
//                        mCompaniesMaterialBodyImg.setVisibility(View.VISIBLE);
                }
            }


        }



//        if (OrderTypeList.size()>0){
//            mOwnMaterialDirectLay.setVisibility(View.VISIBLE);
//        }
//        if (OrderTypeList.size()>1){
//            mOwnMaterialCourierLay.setVisibility(View.VISIBLE);
//        }
//        if (OrderTypeList.size()>2){
//            mCompaniesMaterialLay.setVisibility(View.VISIBLE);
//        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            for (int i=0; i < OrderTypeList.size(); i++ ){
                if (i==0){
                    mOwnMaterialDirectTxt.setText(OrderTypeList.get(0).getHeaderInArabic());
                }else if (i==1){
                    mOwnMaterialCourierTxt.setText(OrderTypeList.get(1).getHeaderInArabic());
                }else if (i==2){
                    mCompaniesMaterialTxt.setText(OrderTypeList.get(2).getHeaderInArabic());
                }

            }
        }else {
            for (int i=0; i < OrderTypeList.size(); i++ ){
                if (i == 0){
                    mOwnMaterialDirectTxt.setText(OrderTypeList.get(0).getHeaderInEnglish());
                }else if (i==1){
                    mOwnMaterialCourierTxt.setText(OrderTypeList.get(1).getHeaderInEnglish());
                }else if (i==2){
                    mCompaniesMaterialTxt.setText(OrderTypeList.get(2).getHeaderInEnglish());
                }
            }
        }

        for (int i=0; i < OrderTypeList.size(); i++ ) {

            if (i==0){
                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(0).getHeaderImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOwnDeliveryImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(0).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOwnDeliveryBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }else if (i==1){
                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(1).getHeaderImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOwnCourierImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }


                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mOwnCourierBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }
            }else if (i==2){
                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(2).getHeaderImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCompanyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                try {
                    Glide.with(OrderTypeScreen.this)
                            .load(AppConstants.IMAGE_BASE_URL+"Images/OrderType/"+OrderTypeList.get(2).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mCompaniesMaterialBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }
        }

        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
            getHintDialog();
        }
    }


    public void getHintDialog() {
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(OrderTypeScreen.this, R.layout.pop_up_order_type_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.order_type_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        } else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.order_type_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        final Button mSkipBtn, mBackBtn, mNextBtn;
        ImageView mOrderTypeOneImg, mOrderTypeTwoImg, mOrderTypeThreeImg;
        final RelativeLayout mOrderOneLay, mOrderTwoLay, mOrderThreeLay;
//        LinearLayout mArrowBgLay,mCmToInBgLay,mImagePartsBgLay;
//
//
        /*Init view*/
        mSkipBtn = mHintDialog.findViewById(R.id.skip_hint_btn);
        mBackBtn = mHintDialog.findViewById(R.id.back_hint_btn);
        mNextBtn = mHintDialog.findViewById(R.id.next_hint_btn);
        mOrderTypeOneImg = mHintDialog.findViewById(R.id.order_type_one_hint_img);
        mOrderTypeTwoImg = mHintDialog.findViewById(R.id.order_type_two_hint_img);
        mOrderTypeThreeImg = mHintDialog.findViewById(R.id.order_type_three_hint_img);

        mOrderOneLay = mHintDialog.findViewById(R.id.order_type_one__par_lay);
        mOrderTwoLay = mHintDialog.findViewById(R.id.order_type_two__par_lay);
        mOrderThreeLay = mHintDialog.findViewById(R.id.order_type_three__par_lay);

        try {
            Glide.with(OrderTypeScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + mOrderTypeList.get(0).getBodyImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(mOrderTypeOneImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        try {
            Glide.with(OrderTypeScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + mOrderTypeList.get(1).getBodyImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(mOrderTypeTwoImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        try {
            Glide.with(OrderTypeScreen.this)
                    .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + mOrderTypeList.get(2).getBodyImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(mOrderTypeThreeImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (mCountInt == 1) {
            mBackBtn.setVisibility(View.INVISIBLE);
            mOrderTwoLay.setVisibility(View.INVISIBLE);
            mOrderThreeLay.setVisibility(View.INVISIBLE);
        }

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = --mCountInt;
                if (mCountInt == 1) {
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mOrderOneLay.setVisibility(View.VISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                } else if (mCountInt == 2) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.VISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                } else if (mCountInt == 3) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                }
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = ++mCountInt;
                if (mCountInt == 1) {
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mOrderOneLay.setVisibility(View.VISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                } else if (mCountInt == 2) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.VISIBLE);
                    mOrderThreeLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                } else if (mCountInt == 3) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mOrderOneLay.setVisibility(View.INVISIBLE);
                    mOrderTwoLay.setVisibility(View.INVISIBLE);
                    mOrderThreeLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                } else if (mCountInt >= 4) {
                    mHintDialog.dismiss();
                }
            }
        });

        /*Set data*/
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });


        alertShowing(mHintDialog);
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
