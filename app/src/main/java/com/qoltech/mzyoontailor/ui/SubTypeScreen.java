package com.qoltech.mzyoontailor.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.SubTypeAdapter;
import com.qoltech.mzyoontailor.entity.SubTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.SubTypeResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubTypeScreen extends BaseActivity {

    @BindView(R.id.dress_sub_type_par_lay)
    LinearLayout mDressSubTypeParLay;

    private SubTypeAdapter mSubTypeAdapter;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    ArrayList<SubTypeEntity> mSubTypeList;

    @BindView(R.id.sub_type_edt_txt)
    EditText mSubTypeEdtTxt;


    @BindView(R.id.sub_type_recycler_view)
    RecyclerView mSubTypeRecyclerView;

    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_sub_type_screen);

        initView();
    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mDressSubTypeParLay);

        mSubTypeList = new ArrayList<>();

        getSubTypeApiCall();

        setHeader();

        mSubTypeEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(String.valueOf(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(SubTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<SubTypeEntity> filterdNames = new ArrayList<>();

        //looping through existing elements
        for (SubTypeEntity s : mSubTypeList) {
            //if the existing elements contains the search input
            if (s.getNameInEnglish().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }

        //calling a method of the adapter class and passing the filtered list
        mSubTypeAdapter.filterList(filterdNames);
        mSubTypeRecyclerView.setVisibility(filterdNames.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(filterdNames.size() > 0 ? View.GONE : View.VISIBLE);

    }
    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(AppConstants.DRESS_TYPE_NAME);
        mRightSideImg.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

    }


    public void getSubTypeApiCall(){

        if (NetworkUtil.isNetworkAvailable(SubTypeScreen.this)){
            APIRequestHandler.getInstance().getSubTypeCall(AppConstants.DRESS_TYPE_ID,SubTypeScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(SubTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getSubTypeApiCall();
                }
            });
        }

    }

    @OnClick({R.id.header_left_side_img,R.id.sub_type_search_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.sub_type_search_lay:
                mSubTypeEdtTxt.setEnabled(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mSubTypeEdtTxt, InputMethodManager.SHOW_IMPLICIT);
                break;
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof SubTypeResponse){
            SubTypeResponse mResponse = (SubTypeResponse)resObj;

            mSubTypeList.addAll(mResponse.getResult());

            setAdapter(mResponse.getResult());
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void setAdapter(ArrayList<SubTypeEntity> mSubTypeList) {

        if (mSubTypeAdapter == null) {

            mSubTypeRecyclerView.setVisibility(mSubTypeList.size()>0 ? View.VISIBLE : View.GONE);
            mEmptyListLay.setVisibility(mSubTypeList.size() > 0 ? View.GONE : View.VISIBLE);

            mSubTypeAdapter = new SubTypeAdapter(this,mSubTypeList);
            mSubTypeRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            mSubTypeRecyclerView.setAdapter(mSubTypeAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSubTypeAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_sub_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_sub_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}
