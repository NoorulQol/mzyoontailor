package com.qoltech.mzyoontailor.ui;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.ValidateOTPResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OTPScreen extends BaseActivity {

@BindView(R.id.otp_par_lay)
LinearLayout mOtpParLay;

@BindView(R.id.header_left_side_img)
ImageView mHeaderLeftBackImg;

@BindView(R.id.otp_first_edt_txt)
EditText mOtpFirstEdtTxt;

@BindView(R.id.otp_second_edt_txt)
EditText mOtpSecondEdtTxt;

@BindView(R.id.otp_third_edt_txt)
EditText mOtpThirdEdtTxt;

@BindView(R.id.otp_fourth_edt_txt)
EditText mOtpFourthEdtTxt;

@BindView(R.id.otp_fivth_edt_txt)
EditText mOtpFivthEdtTxt;

@BindView(R.id.otp_sixth_edt_txt)
EditText mOtpSixthEdtTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_otpscreen);

        initView();

    }
    public void initView(){
        ButterKnife.bind(this);

        setupUI(mOtpParLay);

        setHeader();

        textNextFocusOTP();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.otp_change_num_btn,R.id.header_left_side_img,R.id.otp_resent_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.otp_change_num_btn:
                onBackPressed();
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.otp_resent_btn:
                resendOTPGeneraterApiCall();
                break;
        }

    }


    public void textNextFocusOTP(){

        mOtpFirstEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1){
                    mOtpFirstEdtTxt.clearFocus();
                    mOtpSecondEdtTxt.requestFocus();
                    mOtpSecondEdtTxt.setCursorVisible(true);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        mOtpSecondEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1){
                    mOtpSecondEdtTxt.clearFocus();
                    mOtpThirdEdtTxt.requestFocus();
                    mOtpThirdEdtTxt.setCursorVisible(true);
                }else {
                    mOtpSecondEdtTxt.clearFocus();
                    mOtpFirstEdtTxt.requestFocus();
                    mOtpFirstEdtTxt.setCursorVisible(true);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mOtpThirdEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1){
                    mOtpThirdEdtTxt.clearFocus();
                    mOtpFourthEdtTxt.requestFocus();
                    mOtpFourthEdtTxt.setCursorVisible(true);
                }else {
                    mOtpThirdEdtTxt.clearFocus();
                    mOtpSecondEdtTxt.requestFocus();
                    mOtpSecondEdtTxt.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpFourthEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1){
                    mOtpFourthEdtTxt.clearFocus();
                    mOtpFivthEdtTxt.requestFocus();
                    mOtpFivthEdtTxt.setCursorVisible(true);
                }else {
                    mOtpFourthEdtTxt.clearFocus();
                    mOtpThirdEdtTxt.requestFocus();
                    mOtpThirdEdtTxt.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpFivthEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 1){
                    mOtpFivthEdtTxt.clearFocus();
                    mOtpSixthEdtTxt.requestFocus();
                    mOtpSixthEdtTxt.setCursorVisible(true);
                }else {
                    mOtpFivthEdtTxt.clearFocus();
                    mOtpFourthEdtTxt.requestFocus();
                    mOtpFourthEdtTxt.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mOtpSixthEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count >= 1) {
                    validateOTPApiCall();
                }else {
                    mOtpSixthEdtTxt.clearFocus();
                    mOtpFivthEdtTxt.requestFocus();
                    mOtpFivthEdtTxt.setCursorVisible(true);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void resendOTPGeneraterApiCall(){
        if (NetworkUtil.isNetworkAvailable(OTPScreen.this)){
            APIRequestHandler.getInstance().resendOTPGenerateAPICall(AppConstants.COUNTRY_CODE,AppConstants.MOBILE_NUM,AppConstants.DEVICE_ID,OTPScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OTPScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    resendOTPGeneraterApiCall();
                }
            });
        }
    }

    public void validateOTPApiCall(){
        if (NetworkUtil.isNetworkAvailable(OTPScreen.this)){
            String OTP = mOtpFirstEdtTxt.getText().toString()+mOtpSecondEdtTxt.getText().toString()+mOtpThirdEdtTxt.getText().toString()+mOtpFourthEdtTxt.getText().toString()+mOtpFivthEdtTxt.getText().toString()+mOtpSixthEdtTxt.getText().toString();
            APIRequestHandler.getInstance().validateOTPAPICall(AppConstants.COUNTRY_CODE,AppConstants.MOBILE_NUM,AppConstants.DEVICE_ID,Integer.parseInt(OTP),OTPScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OTPScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    validateOTPApiCall();
                }
            });
        }
    }

    public void setHeader(){
        mHeaderLeftBackImg.setVisibility(View.VISIBLE);

    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ValidateOTPResponse){
            ValidateOTPResponse mResponse = (ValidateOTPResponse)resObj;
            if (!mResponse.getResult().equalsIgnoreCase("-1")){
                AppConstants.USER_ID = Integer.parseInt(mResponse.getResult());
                nextScreen(ProfileIntroScreen.class,true);
//

            }
        }

    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }
}
