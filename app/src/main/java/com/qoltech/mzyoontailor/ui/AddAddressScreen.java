package com.qoltech.mzyoontailor.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.CountryPickerAdapter;
import com.qoltech.mzyoontailor.adapter.GetAreaAdapter;
import com.qoltech.mzyoontailor.adapter.GetCountryAdapter;
import com.qoltech.mzyoontailor.adapter.GetStateAdapter;
import com.qoltech.mzyoontailor.adapter.LocationTypeAdapter;
import com.qoltech.mzyoontailor.entity.GetAddressEntity;
import com.qoltech.mzyoontailor.entity.GetAreaEntity;
import com.qoltech.mzyoontailor.entity.GetCountryEntity;
import com.qoltech.mzyoontailor.entity.GetStateEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.CountryCodeResponse;
import com.qoltech.mzyoontailor.modal.GetAddressResponse;
import com.qoltech.mzyoontailor.modal.GetAreaResponse;
import com.qoltech.mzyoontailor.modal.GetStateResponse;
import com.qoltech.mzyoontailor.modal.InsertAddressResponse;
import com.qoltech.mzyoontailor.modal.UpdateAddressResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoontailor.main.MZYOONApplication.getContext;

public class AddAddressScreen extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.choosed_location_address)
    TextView mChooseLocationAddress;

    @BindView(R.id.add_address_par_lay)
    RelativeLayout mAddAddressParLay;

    private GetCountryAdapter mAddAddressGetCountryAdapter;

    private CountryPickerAdapter mAddAddressGetCountryNameAdapter;

    private LocationTypeAdapter mShippingLocationAdapter;

    private ArrayList<GetCountryEntity> mAddAddressCountryList;

    private ArrayList<GetStateEntity> mAddAddressGetStateList;

    private ArrayList<GetAreaEntity> mAddAddressGetAreaList;

    private GetStateAdapter mAddAddressGetStateAdapter;

    private GetAreaAdapter mAddAddressGetAreaAdapter;

    private GetAddressEntity mGetAddressEntity;

    private Dialog mCountryDialog, mAddressDialog, mAddressStateDialog, mLocationShippingDialog, mAddressAreaDialog;

    @BindView(R.id.add_address_country_edt_txt)
    public EditText mAddAddressCountyrNameEdtTxt;

    @BindView(R.id.add_address_flag_img)
    public ImageView mAddAddressFlagImg;

    @BindView(R.id.add_address_country_code_txt)
    public TextView mAddAddressCountryCodeTxt;

    @BindView(R.id.add_address_state_edt_txt)
    public EditText mAddAddressStateEdtTxt;

    @BindView(R.id.add_address_frt_name_edt_txt)
    EditText mAddAddressFrsNameEdtTxt;

    @BindView(R.id.add_address_sec_name_edt_txt)
    EditText mAddressSecNameEdtTxt;

    @BindView(R.id.add_address_floor_edt_txt)
    EditText mAddressFloorEdtTxt;

    @BindView(R.id.add_address_area_edt_txt)
    public EditText mAddressAreaEdtTxt;

    @BindView(R.id.add_address_landmark_edt_txt)
    EditText mAddressLandmarkEdtTxt;

    @BindView(R.id.add_address_shipping_edt_txt)
    EditText mAddressShippingEdtTxt;

    @BindView(R.id.add_address_loc_type_edt_txt)
    public EditText mAddressLocationTypeEdtTxt;

    @BindView(R.id.add_address_mobile_num_edt_txt)
    EditText mAddressMobNumEdtTxt;

    @BindView(R.id.make_as_default_toggle_btn)
    ImageView mMakeAsDefaultImg;

    private ArrayList<String> mLocationList;

    private UserDetailsEntity mUserDetailsEntityRes;

    protected static final String TAG = "LocationOnOff";


    private LocationManager locationManager;

    private boolean GpsStatus, mDefaultApiBool = false;


    private String mEdit = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_add_address_screen);

        initView();

    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAddAddressParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddAddressScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        mAddAddressCountryList = new ArrayList<>();
        mAddAddressGetStateList = new ArrayList<>();
        mAddAddressGetAreaList = new ArrayList<>();
        mGetAddressEntity = new GetAddressEntity();

        setHeader();

//        if (AppConstants.EDIT_ADDRESS.equalsIgnoreCase("EDIT_ADDRESS")){
//            setEditText();
//        }

        if (!AppConstants.GET_ADDRESS_ID.equalsIgnoreCase("")
                && AppConstants.EDIT_ADDRESS.equalsIgnoreCase("EDIT_ADDRESS")) {
            getBuyerAddressById();
        } else {
            mAddAddressCountryCodeTxt.setText("971");

        }

        getCountryApiCall();

        mLocationList = new ArrayList<>();
        mLocationList.add(getResources().getString(R.string.home));
        mLocationList.add(getResources().getString(R.string.work));
        mLocationList.add(getResources().getString(R.string.others));

        getLanguage();
    }


    public void setEditText(ArrayList<GetAddressEntity> addressDetails) {

        mGetAddressEntity = new GetAddressEntity();
        mEdit = "mEdit";
        if (addressDetails.size() > 0) {
            mGetAddressEntity = addressDetails.get(0);
        }
        mAddAddressFrsNameEdtTxt.setText(mGetAddressEntity.getFirstName());
        mAddressSecNameEdtTxt.setText(mGetAddressEntity.getLastName());
        mAddressFloorEdtTxt.setText(mGetAddressEntity.getFloor());
        mAddressLandmarkEdtTxt.setText(mGetAddressEntity.getLandMark());
        mAddressLocationTypeEdtTxt.setText(mGetAddressEntity.getLocationType());
        mAddAddressCountryCodeTxt.setText(mGetAddressEntity.getCountryCode());
        mAddressMobNumEdtTxt.setText(mGetAddressEntity.getPhoneNo());
        mAddressShippingEdtTxt.setText(mGetAddressEntity.getShippingNotes());
        mDefaultApiBool = mGetAddressEntity.isDefault();

        if (mDefaultApiBool == true) {
            mMakeAsDefaultImg.setBackground(getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
        } else {
            mMakeAsDefaultImg.setBackground(getResources().getDrawable(R.drawable.toggle_orange_clr_off));

        }

        getCountryApiCall();

        AppConstants.COUNTRY_CODE_ID = String.valueOf(mGetAddressEntity.getCountryId());
        AppConstants.COUNTRY_STATE_ID = String.valueOf(mGetAddressEntity.getStateId());
        AppConstants.COUNTRY_AREA_ID = String.valueOf(mGetAddressEntity.getAreaId());

        AppConstants.SELECTED_LAT_STR = String.valueOf(mGetAddressEntity.getLattitude());
        AppConstants.SELECTED_LONG_STR = String.valueOf(mGetAddressEntity.getLongitude());

        AppConstants.LAT_STR = String.valueOf(mGetAddressEntity.getLattitude());
        AppConstants.LONG_STR = String.valueOf(mGetAddressEntity.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(
                    mGetAddressEntity.getLattitude(), mGetAddressEntity.getLongitude(), 1);
            Location mLocation = new Location("");
            mLocation.setLatitude(mGetAddressEntity.getLattitude());
            mLocation.setLongitude(mGetAddressEntity.getLongitude());
            if (addresses.size() > 0) {
                mChooseLocationAddress.setText(addresses.get(0).getAddressLine(0) + "," + addresses.get(0).getAdminArea());
            }

        } catch (Exception e) {
        }

    }

    @OnClick({R.id.header_left_side_img, R.id.add_address_country_code_lay, R.id.add_address_country_lay, R.id.add_address_save_btn, R.id.add_address_edit_btn, R.id.add_address_state_lay, R.id.add_address_location_type_lay, R.id.add_address_area_lay, R.id.make_as_default_toggle_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.add_address_country_code_lay:

                AppConstants.CountryCode = "ADD_ADDRESS_PHONE";
                if (mAddAddressCountryList.size() > 0) {
                    alertDismiss(mCountryDialog);
                    mCountryDialog = getDialog(AddAddressScreen.this, R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mCountryDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mCountryDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mCountryDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mCountryDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mCountryDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mCountryDialog.findViewById(R.id.country_popup_recycler_view);
                    mAddAddressGetCountryAdapter = new GetCountryAdapter(AddAddressScreen.this, mAddAddressCountryList, mCountryDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mAddAddressGetCountryAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_country_code));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCountryDialog.dismiss();
                        }
                    });

                    alertShowing(mCountryDialog);
                } else {
                    Toast.makeText(AddAddressScreen.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_address_country_lay:
                AppConstants.CountryCode = "ADD_ADDRESS";

                if (mAddAddressCountryList.size() > 0) {
                    mEdit = "";
                    alertDismiss(mAddressDialog);
                    mAddressDialog = getDialog(AddAddressScreen.this, R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAddressDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAddressDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


                    } else {
                        ViewCompat.setLayoutDirection(mAddressDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mAddressDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mAddressDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mAddressDialog.findViewById(R.id.country_popup_recycler_view);
                    mAddAddressGetCountryNameAdapter = new CountryPickerAdapter(AddAddressScreen.this, mAddAddressCountryList, mAddressDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mAddAddressGetCountryNameAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_country));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAddressDialog.dismiss();
                        }
                    });

                    alertShowing(mAddressDialog);
                } else {
                    Toast.makeText(AddAddressScreen.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_address_save_btn:

                if (mAddAddressFrsNameEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddAddressFrsNameEdtTxt.clearAnimation();
                    mAddAddressFrsNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddAddressFrsNameEdtTxt.setError(getResources().getString(R.string.please_give_your_first_name));
                } else if (mAddressSecNameEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddressSecNameEdtTxt.clearAnimation();
                    mAddressSecNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddressSecNameEdtTxt.setError(getResources().getString(R.string.please_give_your_second_name));
                } else if (mAddAddressCountyrNameEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddAddressCountyrNameEdtTxt.clearAnimation();
                    mAddAddressCountyrNameEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddAddressCountyrNameEdtTxt.setError(getResources().getString(R.string.please_select_country));
                } else if (mAddAddressStateEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddAddressStateEdtTxt.clearAnimation();
                    mAddAddressStateEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddAddressStateEdtTxt.setError(getResources().getString(R.string.please_select_state));
                } else if (mAddressAreaEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddressAreaEdtTxt.clearAnimation();
                    mAddressAreaEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddressAreaEdtTxt.setError(getResources().getString(R.string.please_give_area));
                }
//                else if (mAddressFloorEdtTxt.getText().toString().trim().isEmpty()){
//                    mAddressFloorEdtTxt.clearAnimation();
//                    mAddressFloorEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                    mAddressFloorEdtTxt.setError(getResources().getString(R.string.please_give_floor));
//                }
                else if (mAddressLandmarkEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddressLandmarkEdtTxt.clearAnimation();
                    mAddressLandmarkEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddressLandmarkEdtTxt.setError(getResources().getString(R.string.please_give_landmark));
                } else if (mAddressLocationTypeEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddressLocationTypeEdtTxt.clearAnimation();
                    mAddressLocationTypeEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddressLocationTypeEdtTxt.setError(getResources().getString(R.string.please_select_location_type));
                } else if (mAddressMobNumEdtTxt.getText().toString().trim().isEmpty()) {
                    mAddressMobNumEdtTxt.clearAnimation();
                    mAddressMobNumEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddressMobNumEdtTxt.setError(getResources().getString(R.string.please_give_mobile_num));
                } else if (mAddressMobNumEdtTxt.getText().toString().trim().length() < 6) {
                    mAddressMobNumEdtTxt.clearAnimation();
                    mAddressMobNumEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mAddressMobNumEdtTxt.setError(getResources().getString(R.string.please_give_valid_mobile_num));
                }
//                else if (mAddressShippingEdtTxt.getText().toString().trim().isEmpty()){
//                    mAddressShippingEdtTxt.clearAnimation();
//                    mAddressShippingEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
//                    mAddressShippingEdtTxt.setError(getResources().getString(R.string.please_give_shipping_details));
//                }
                else {
                    if (AppConstants.EDIT_ADDRESS.equalsIgnoreCase("EDIT_ADDRESS")) {
                        updateAddressApiCall();
                    } else {
                        insertAddressApiCall();

                    }
                }
                break;
            case R.id.add_address_edit_btn:
                if (checkPermission()) {
                    CheckGpsStatus();
                    if (GpsStatus) {
                        AppConstants.CHECK_ADD_ADDRESS = "ADD_ADDRESS_EDIT";
                        nextScreen(ConfirmAddressScreen.class, true);
                    } else {
                        EnableGPSAutoMatically();

                    }
                }
                break;
            case R.id.add_address_state_lay:
                if (mAddAddressGetStateList.size() > 0) {
                    mEdit = "";
                    alertDismiss(mAddressStateDialog);
                    mAddressStateDialog = getDialog(AddAddressScreen.this, R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAddressStateDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAddressStateDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


                    } else {
                        ViewCompat.setLayoutDirection(mAddressStateDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mAddressStateDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mAddressStateDialog.findViewById(R.id.country_header_text_cancel);


                    countryRecyclerView = mAddressStateDialog.findViewById(R.id.country_popup_recycler_view);
                    mAddAddressGetStateAdapter = new GetStateAdapter(AddAddressScreen.this, mAddAddressGetStateList, mAddressStateDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mAddAddressGetStateAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_state));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAddressStateDialog.dismiss();
                        }
                    });

                    alertShowing(mAddressStateDialog);
                } else {
                    Toast.makeText(AddAddressScreen.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_address_location_type_lay:
                if (mLocationList.size() > 0) {
                    alertDismiss(mLocationShippingDialog);
                    mLocationShippingDialog = getDialog(AddAddressScreen.this, R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mLocationShippingDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mLocationShippingDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


                    } else {
                        ViewCompat.setLayoutDirection(mLocationShippingDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mLocationShippingDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mLocationShippingDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mLocationShippingDialog.findViewById(R.id.country_popup_recycler_view);
                    mShippingLocationAdapter = new LocationTypeAdapter(AddAddressScreen.this, mLocationList, mLocationShippingDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mShippingLocationAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_location_type));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mLocationShippingDialog.dismiss();
                        }
                    });

                    alertShowing(mLocationShippingDialog);
                } else {
                    Toast.makeText(AddAddressScreen.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_address_area_lay:
                if (mAddAddressGetAreaList.size() > 0) {
                    mEdit = "";
                    alertDismiss(mAddressAreaDialog);
                    mAddressAreaDialog = getDialog(AddAddressScreen.this, R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mAddressAreaDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAddressAreaDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


                    } else {
                        ViewCompat.setLayoutDirection(mAddressAreaDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mAddressAreaDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mAddressAreaDialog.findViewById(R.id.country_header_text_cancel);


                    countryRecyclerView = mAddressAreaDialog.findViewById(R.id.country_popup_recycler_view);
                    mAddAddressGetAreaAdapter = new GetAreaAdapter(AddAddressScreen.this, mAddAddressGetAreaList, mAddressAreaDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mAddAddressGetAreaAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_area));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAddressAreaDialog.dismiss();
                        }
                    });

                    alertShowing(mAddressAreaDialog);
                } else {
                    Toast.makeText(AddAddressScreen.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                }
                break;


            case R.id.make_as_default_toggle_btn:
                if (mDefaultApiBool == true) {
                    mDefaultApiBool = false;
                    mMakeAsDefaultImg.setBackground(getResources().getDrawable(R.drawable.toggle_orange_clr_off));
                } else {
                    mDefaultApiBool = true;
                    mMakeAsDefaultImg.setBackground(getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
                }
        }

    }

    public void CheckGpsStatus() {

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

    }

    private void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setNeedBle(true);
            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
//                            toast("Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            toast("GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(AddAddressScreen.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            toast("Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.address).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

        mChooseLocationAddress.setText(AppConstants.ADRRESS_STR);


        mAddAddressFrsNameEdtTxt.setText(mUserDetailsEntityRes.getUSER_NAME());
        mAddressMobNumEdtTxt.setText(mUserDetailsEntityRes.getMOBILE_NUM());
        mAddAddressCountryCodeTxt.setText(mUserDetailsEntityRes.getCOUNTRY_CODE());

    }

    public void getBuyerAddressById() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            APIRequestHandler.getInstance().getAddressApiCall(AppConstants.GET_ADDRESS_ID, AddAddressScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getBuyerAddressById();
                }
            });
        }
    }

    public void getCountryApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            APIRequestHandler.getInstance().getAllCountryAPICall(AddAddressScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCountryApiCall();
                }
            });
        }

    }

    public void getStateApiCall(final String stateId) {
        if (NetworkUtil.isNetworkAvailable(AddAddressScreen.this)) {
            APIRequestHandler.getInstance().getStateApiCall(stateId, AddAddressScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getStateApiCall(stateId);
                }
            });
        }
    }

    public void getAreaApiCall(final String areaId) {
        if (NetworkUtil.isNetworkAvailable(AddAddressScreen.this)) {
            APIRequestHandler.getInstance().getAreaApiCall(areaId, AddAddressScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {

                }
            });
        }
    }

    public void insertAddressApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            APIRequestHandler.getInstance().
                    insertAddressApiCall(Integer.parseInt(AppConstants.DIRECT_USERS_ID),
                            mAddAddressFrsNameEdtTxt.getText().toString().trim(), mAddressSecNameEdtTxt.getText().toString().trim(), Integer.parseInt(AppConstants.COUNTRY_CODE_ID), Integer.parseInt(AppConstants.COUNTRY_STATE_ID), Integer.parseInt(AppConstants.COUNTRY_AREA_ID), mAddressFloorEdtTxt.getText().toString().trim(), mAddressLandmarkEdtTxt.getText().toString().trim(), mAddressLocationTypeEdtTxt.getText().toString().trim(), mAddressShippingEdtTxt.getText().toString().trim(),
                            mDefaultApiBool, mAddAddressCountryCodeTxt.getText().toString().trim(), mAddressMobNumEdtTxt.getText().toString().trim(), Double.parseDouble(AppConstants.LONG_STR), Double.parseDouble(AppConstants.LAT_STR), AddAddressScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertAddressApiCall();
                }
            });
        }
    }


//    public void updateAddressApiCall(){
//        if (NetworkUtil.isNetworkAvailable(AddAddressScreen.this)){
//            APIRequestHandler.getInstance().
//                    updateAddressApiCall(String.valueOf(mGetAddressEntity.getId()),
//                            String.valueOf(mGetAddressEntity.getBuyerId()),
//                            mAddAddressFrsNameEdtTxt.getText().toString(),
//                            mAddressSecNameEdtTxt.getText().toString(),
//                            Integer.parseInt(AppConstants.COUNTRY_CODE_ID),
//                            Integer.parseInt(AppConstants.COUNTRY_STATE_ID),
//                            Integer.parseInt(AppConstants.COUNTRY_AREA_ID),mAddressFloorEdtTxt.getText().toString(),
//                            mAddressLandmarkEdtTxt.getText().toString(),
//                            mAddressLocationTypeEdtTxt.getText().toString()
//                            ,mAddressShippingEdtTxt.getText().toString(),
//                            mDefaultApiBool,
//                            mAddAddressCountryCodeTxt.getText().toString().trim(),
//                            mAddressMobNumEdtTxt.getText().toString(),
//                            Double.parseDouble(AppConstants.LONG_STR),
//                            Double.parseDouble(AppConstants.LAT_STR),AddAddressScreen.this);
//        }else {
//            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
//                @Override
//                public void onPositiveClick() {
//                    updateAddressApiCall();
//                }
//            });
//        }
//    }


    public void updateAddressApiCall() {
        if (NetworkUtil.isNetworkAvailable(AddAddressScreen.this)) {
            APIRequestHandler.getInstance().
                    updateAddressApiCall(String.valueOf(mGetAddressEntity.getId()),
                            String.valueOf(mGetAddressEntity.getBuyerId()),
                            mAddAddressFrsNameEdtTxt.getText().toString(),
                            mAddressSecNameEdtTxt.getText().toString(),
                            Integer.parseInt(AppConstants.COUNTRY_CODE_ID),
                            Integer.parseInt(AppConstants.COUNTRY_STATE_ID),
                            Integer.parseInt(AppConstants.COUNTRY_AREA_ID),
                            mAddressFloorEdtTxt.getText().toString(),
                            mAddressLandmarkEdtTxt.getText().toString(),
                            mAddressLocationTypeEdtTxt.getText().toString(),
                            mAddressShippingEdtTxt.getText().toString(),
                            mDefaultApiBool,
                            mAddAddressCountryCodeTxt.getText().toString().trim(),
                            mAddressMobNumEdtTxt.getText().toString(),
                            Double.parseDouble(AppConstants.LONG_STR),
                            Double.parseDouble(AppConstants.LAT_STR), AddAddressScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddAddressScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    updateAddressApiCall();
                }
            });
        }
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof CountryCodeResponse) {
            CountryCodeResponse mResponse = (CountryCodeResponse) resObj;
            mAddAddressCountryList = new ArrayList<>();
            mAddAddressCountryList.addAll(mResponse.getResult());

            if (AppConstants.EDIT_ADDRESS.equalsIgnoreCase("EDIT_ADDRESS")) {
                for (int i = 0; i < mAddAddressCountryList.size(); i++) {
                    if (mGetAddressEntity.getCountryCode().equalsIgnoreCase(String.valueOf(mAddAddressCountryList.get(i).getPhoneCode()))) {
                        try {
                            Glide.with(AddAddressScreen.this)
                                    .load(AppConstants.IMAGE_BASE_URL + "images/flags/" + mAddAddressCountryList.get(i).getFlag())
                                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                    .into(mAddAddressFlagImg);

                        } catch (Exception ex) {
                            Log.e(AppConstants.TAG, ex.getMessage());
                        }
                    }
                }
            }


            if (!mEdit.equalsIgnoreCase("")) {
                for (int i = 0; i < mAddAddressCountryList.size(); i++) {
                    if (String.valueOf(mAddAddressCountryList.get(i).getId()).equalsIgnoreCase(AppConstants.COUNTRY_CODE_ID)) {
                        String strings = mAddAddressCountryList.get(i).getCountryName();
                        String[] part = strings.split("\\(");
                        String parts1 = part[0];
                        mAddAddressCountyrNameEdtTxt.setText(parts1);

                    }
                }
                getStateApiCall(AppConstants.COUNTRY_CODE_ID);
            }

        }
        if (resObj instanceof InsertAddressResponse) {
            InsertAddressResponse mResponse = (InsertAddressResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                AppConstants.EDIT_ADDRESS = "";

                onBackPressed();

            }
        }
        if (resObj instanceof GetStateResponse) {
            GetStateResponse mResponse = (GetStateResponse) resObj;
            mAddAddressGetStateList = new ArrayList<>();
            mAddAddressGetStateList.addAll(mResponse.getResult());

            if (!mEdit.equalsIgnoreCase("")) {
                for (int i = 0; i < mAddAddressGetStateList.size(); i++) {
                    if (String.valueOf(mAddAddressGetStateList.get(i).getId()).equalsIgnoreCase(AppConstants.COUNTRY_STATE_ID)) {
                        mAddAddressStateEdtTxt.setText(mAddAddressGetStateList.get(i).getStateName());

                    }
                }

                getAreaApiCall(AppConstants.COUNTRY_STATE_ID);
            }

        }
        if (resObj instanceof GetAreaResponse) {
            GetAreaResponse mResponse = (GetAreaResponse) resObj;
            mAddAddressGetAreaList = new ArrayList<>();
            mAddAddressGetAreaList.addAll(mResponse.getResult());
            if (!mEdit.equalsIgnoreCase("")) {
                for (int i = 0; i < mAddAddressGetAreaList.size(); i++) {
                    if (String.valueOf(mAddAddressGetAreaList.get(i).getId()).equalsIgnoreCase(AppConstants.COUNTRY_AREA_ID)) {
                        mAddressAreaEdtTxt.setText(mAddAddressGetAreaList.get(i).getArea());

                    }
                }

            }

        }
        if (resObj instanceof UpdateAddressResponse) {
            UpdateAddressResponse mResponse = (UpdateAddressResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                AppConstants.EDIT_ADDRESS = "";
                onBackPressed();
            }
        }
        if (resObj instanceof GetAddressResponse) {
            GetAddressResponse mResponse = (GetAddressResponse) resObj;

            getCountryApiCall();

            setEditText(mResponse.getResult());

        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mChooseLocationAddress.setText(AppConstants.ADRRESS_STR);
    }

    /* Ask for permission on Camera access*/
    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int fineLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int coarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

            if (fineLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            } else if (coarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }


                @Override
                public void onPositiveClick() {
                    nextScreen(ConfirmAddressScreen.class, true);
                }

            });
        }

        return addPermission;

    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_address_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_address_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.GET_ADDRESS_ID = "";
        AppConstants.EDIT_ADDRESS = "";
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }





}
