package com.qoltech.mzyoontailor.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GetTrackingDetailsAdapter;
import com.qoltech.mzyoontailor.entity.GetTrackingEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GetTrackingResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderTrackingDetailsScreen extends BaseActivity {

    @BindView(R.id.order_tracking_details_pay_lay)
    LinearLayout mOrderTrackingDetailsPayLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    private UserDetailsEntity mUserDetailsEntityRes;

    private GetTrackingDetailsAdapter mGetTrackingAdapter;

    @BindView(R.id.get_tracking_details_rec_lay)
    RecyclerView mGetTrackingDetailsRecLay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_order_tracking_details_screen);
        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mOrderTrackingDetailsPayLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderTrackingDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
        getTreackingDetailsApiCall();
        setHeader();

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.tracking_details).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    public void getTreackingDetailsApiCall(){
        if (NetworkUtil.isNetworkAvailable(OrderTrackingDetailsScreen.this)){
            APIRequestHandler.getInstance().getTrackingDetailsApiCall(AppConstants.ORDER_ID,OrderTrackingDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(OrderTrackingDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getTreackingDetailsApiCall();
                }
            });
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.order_tracking_details_pay_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_tracking_details_pay_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<GetTrackingEntity> getTrackingList) {

        if (mGetTrackingAdapter == null) {

            mGetTrackingAdapter = new GetTrackingDetailsAdapter(this,getTrackingList);
            mGetTrackingDetailsRecLay.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mGetTrackingDetailsRecLay.setAdapter(mGetTrackingAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mGetTrackingAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetTrackingResponse){
            GetTrackingResponse mResponse = (GetTrackingResponse)resObj;

            setAdapter(mResponse.getResult());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
