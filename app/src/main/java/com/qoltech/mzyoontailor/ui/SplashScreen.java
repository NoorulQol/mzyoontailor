package com.qoltech.mzyoontailor.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.activity.SignUpActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreen extends BaseActivity {

    @BindView(R.id.splash_par_lay)
    ViewGroup mSplashViewGroup;

    private Handler mHandler;
    private Runnable mRunnable;
    SharedPreferences sharedPreferences;
    public UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.ui_splash_screen);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        initView();
        AppConstants.DEVICE_ID_NEW = Settings.Secure.getString(getApplicationContext()
                .getContentResolver(), Settings.Secure.ANDROID_ID);

    }


    /*View initialization*/
    private void initView() {
        /*ButterKnife for variable initialization*/
        ButterKnife.bind(this);

        /*Keypad to be hidden when a Click/touch made outside the edit text*/
        setupUI(mSplashViewGroup);


        if (askPermissions()) {
            /****** Create Thread that will sleep for 5 seconds****/
            Thread background = new Thread() {
                public void run() {
                    try {
                        // Thread will sleep for 5 seconds
                        sleep(3 * 1000);

                        // After 5 seconds redirect to another intent
                        if (sharedPreferences.getString("TailorId", "").isEmpty()) {

                            startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                        } else {
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            mUserDetailsEntityRes = new UserDetailsEntity();
                            Gson gson = new Gson();
                            String json = PreferenceUtil.getStringValue(SplashScreen.this, AppConstants.USER_DETAILS);
                            mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

                            getLanguage();
                        }

                        //Remove activity
                        finish();
                    } catch (Exception e) {
                    }
                }
            };
            // start thread
            background.start();
        }


    }


    public void nextScreen() {
        /****** Create Thread that will sleep for 5 seconds****/
        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(1 * 1000);

                    // After 5 seconds redirect to another intent
                    if (sharedPreferences.getString("TailorId", "").isEmpty()) {

                        startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                    } else {
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));

                    }

                    //Remove activity
                    finish();
                } catch (Exception e) {
                }
            }
        };
        // start thread
        background.start();

    }


    /*To get permission for access image camera and storage*/
    private boolean askPermissions() {
        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            int permissionCoarseLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);


            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

        }
        if (!listPermissionsNeeded.isEmpty()) {
            addPermission = askAccessPermission(listPermissionsNeeded, 1, new InterfaceTwoBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    nextScreen();
                }

                public void onNegativeClick() {
                    nextScreen();

                }
            });
        }

        return addPermission;
    }


    private static void updateResources(Context context,
                                        String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            updateResources(SplashScreen.this, "ar");

        } else {

            updateResources(SplashScreen.this, "en");

        }
    }


}
