package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoontailor.entity.GetColorByIdEntity;
import com.qoltech.mzyoontailor.entity.GetMaterialFriendlyNameEntity;
import com.qoltech.mzyoontailor.entity.GetPatternByMaterialIdEntity;
import com.qoltech.mzyoontailor.entity.GetPatternImageByMaterialId;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.MaterialByIdGetRespone;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ZoomOutPageTransformer;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialDetailsScreenOrderDetails extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;

    TextView material_txt, season_text,
            place_of_industry_txt, brand_txt, material_type_txt, color_txt;

    List<GetPatternImageByMaterialId> getPatternImageByMaterialIds;
    List<GetPatternByMaterialIdEntity> getPatternByMaterialIdEntities;
    List<GetColorByIdEntity> getColorByIdEntities;
    List<GetMaterialFriendlyNameEntity> getMaterialFriendlyNameEntities;
    AppCompatCheckBox check_box_material_screen;
    Button save;
    @BindView(R.id.view_details_view_pager)
    ViewPager mViewDetailsViewpager;

    @BindView(R.id.pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;


    ArrayList<String> mImageList;

    ViewDetailPagerAdapter mViewDetailAdapter;
    SharedPreferences sharedPreferences;
    EditText friendly_name_english, friendly_name_arabic;
    RelativeLayout friendly_name_english_layout, friendly_name_arabic_layout;
    public UserDetailsEntity mUserDetailsEntityRes;
    TransparentProgressDialog transparentProgressDialog;
    ImageView image_color;
    //
    @BindView(R.id.view_page_layout)
    RelativeLayout view_page_layout;
    LinearLayout friendly_layout;
//    @BindView(R.id.view_details_pattern_name_txt)
//    TextView mViewDetailsPatternNameTxt;
//
//    @BindView(R.id.view_details_seasonal_name_txt)
//    TextView mViewDetailsSeasonalNameTxt;
//
//    @BindView(R.id.view_details_place_industry_name_txt)
//    TextView mViewDetailsPlaceIndustryNameTxt;
//
//    @BindView(R.id.view_details_brands_name_txt)
//    TextView mViewDetailsBrandsNameTxt;
//
//    @BindView(R.id.view_details_material_type_name_txt)
//    TextView mViewDetailsMaterialTypeNameTxt;
//
//    @BindView(R.id.view_details_color_name_txt)
//    TextView mViewDetailsColorNameTxt;
//
//    @BindView(R.id.view_details_view_pager)
//    ViewPager mViewDetailsViewpager;
//
//    @BindView(R.id.pageIndicatorView)
//    com.rd.PageIndicatorView mPageIndicator;
//
//    ArrayList<String> mImageList;
//
//    ViewDetailPagerAdapter mViewDetailAdapter;
//
//    private UserDetailsEntity mUserDetailsEntityRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_material_details_screen);
        material_txt = (TextView) findViewById(R.id.material_txt);
        season_text = (TextView) findViewById(R.id.season_text);
        place_of_industry_txt = (TextView) findViewById(R.id.place_of_industry_txt);
        brand_txt = (TextView) findViewById(R.id.brand_txt);
        material_type_txt = (TextView) findViewById(R.id.material_type_txt);
        color_txt = (TextView) findViewById(R.id.color_txt);
        check_box_material_screen = findViewById(R.id.check_box_material_screen);
        save = findViewById(R.id.save_material_screen);
        image_color = findViewById(R.id.image_color);
        friendly_name_english = (EditText) findViewById(R.id.friendly_name_english);
        friendly_name_arabic = (EditText) findViewById(R.id.friendly_name_arabic);
        friendly_name_english_layout = (RelativeLayout) findViewById(R.id.friendly_name_english_layout);
        friendly_name_arabic_layout = (RelativeLayout) findViewById(R.id.friendly_name_arabic_layout);
        friendly_layout = (LinearLayout) findViewById(R.id.friendly_layout);
        transparentProgressDialog = new TransparentProgressDialog(MaterialDetailsScreenOrderDetails.this);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        initView();
//
        restService = ((MainApplication) getApplication()).getClient();

        getMaterialDetailsByPattern(sharedPreferences.getString("TailorId", ""), AppConstants.PATTERN_ID);
        save.setVisibility(View.GONE);
        friendly_layout.setVisibility(View.GONE);

//        if (check_box_material_screen.isChecked() == false) {
//            save.setVisibility(View.INVISIBLE);
//        }
//
//
//        check_box_material_screen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//
//
//                    friendly_name_english_layout.setVisibility(View.VISIBLE);
//                    friendly_name_arabic_layout.setVisibility(View.VISIBLE);
//                    save.setVisibility(View.VISIBLE);
////                    cutomization_text_label.setVisibility(View.VISIBLE);
////                    custumization_list_layout.setVisibility(View.VISIBLE);
////                    stitching_charges_layout.setVisibility(View.VISIBLE);
////                    material_charges_layout.setVisibility(View.VISIBLE);
//                } else {
//                    friendly_name_english_layout.setVisibility(View.GONE);
//                    friendly_name_arabic_layout.setVisibility(View.GONE);
//                    save.setVisibility(View.GONE);
//                    if (getMaterialFriendlyNameEntities.size() > 0) {
//
//
//                        new AlertDialog.Builder(MaterialDetailsScreenOrderDetails.this)
//                                .setMessage("Are You Sure Want To Remove Material")
//                                .setPositiveButton(R.string.yes,
//                                        new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(
//                                                    DialogInterface dialog,
//                                                    int which) {
//
//                                                deleteMaterialMap();
////
//
//
//                                                final Handler handler = new Handler();
//                                                handler.postDelayed(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        //Do something after 100ms
//                                                        finish();
////                                                        startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
//
//                                                        Toast.makeText(getApplicationContext(), "Deleted Sucessfully", Toast.LENGTH_LONG).show();
//
//                                                    }
//                                                }, 100);
//                                            }
//                                        }).setNegativeButton(R.string.no, null).show();
//                        check_box_material_screen.setChecked(true);
//                        friendly_name_english_layout.setVisibility(View.VISIBLE);
//                        friendly_name_arabic_layout.setVisibility(View.VISIBLE);
//                        save.setVisibility(View.VISIBLE);
//                    }
//
//                }
//            }
//        });

//
//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if (friendly_name_english.getText().toString().isEmpty()) {
//                    friendly_name_english.setText(AppConstants.MATERIAL_NAME_ENGLISH);
//                }
//
//                if (friendly_name_arabic.getText().toString().isEmpty()) {
//                    friendly_name_arabic.setText(AppConstants.MATERIAL_NAME_ARABIC);
//                }
//
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        //Do something after 100ms
//                        transparentProgressDialog.show();
//                        InsertMaterialMapping();
//                    }
//                }, 1000);
//
////                else {
////
////
////
////
////                }
//
//            }
//        });

//        friendly_name_arabic.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

    }

    public void initView() {

        ButterKnife.bind(this);
        setHeader();
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MaterialDetailsScreenOrderDetails.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        mImageList = new ArrayList<>();


    }

    public void getMaterialDetailsByPattern(String tailorId, String materialId) {

        restService.getMaterialDetails(tailorId, materialId).enqueue(new Callback<MaterialByIdGetRespone>() {
            @Override
            public void onResponse(Call<MaterialByIdGetRespone> call, Response<MaterialByIdGetRespone> response) {

                try {
                    getPatternByMaterialIdEntities = response.body().getResult().getGetPattern();
                    getColorByIdEntities = response.body().getResult().getGetColors();
                    getPatternImageByMaterialIds = response.body().getResult().getPatternImg();
                    getMaterialFriendlyNameEntities = response.body().getResult().getGetMaterialName();


                    if (getMaterialFriendlyNameEntities.size() > 0) {
                        check_box_material_screen.setChecked(true);
                        friendly_name_english.setText(getMaterialFriendlyNameEntities.get(0).
                                getFriendlyNameInEnglish());
                        friendly_name_arabic.setText(getMaterialFriendlyNameEntities.get(0).
                                getFriendlyNameInArabic());
                        friendly_name_english_layout.setVisibility(View.VISIBLE);
                        friendly_name_arabic_layout.setVisibility(View.VISIBLE);

                    }

                    if (getPatternImageByMaterialIds.size() > 0) {


                        for (int i = 0; i < getPatternImageByMaterialIds.size(); i++) {
                            mImageList.add(GlobalData.SERVER_URL + "Images/Pattern/" +
                                    getPatternImageByMaterialIds.get(i).getImageName());

                            viewPagerGet(mImageList);

                        }
                    } else {
                        view_page_layout.setVisibility(View.INVISIBLE);
                    }


                 if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                     material_txt.setText(getMaterialFriendlyNameEntities.get(0).getFriendlyNameInArabic());
                     place_of_industry_txt.setText(getPatternByMaterialIdEntities.get(0).getPlaceInArabic());
                     brand_txt.setText(getPatternByMaterialIdEntities.get(0).getBrandInArabic());
                     material_type_txt.setText(getPatternByMaterialIdEntities.get(0).getMaterialInArabic());
                     color_txt.setText(getColorByIdEntities.get(0).getColorInArabic());

                 }
                 else {
                     material_txt.setText(getMaterialFriendlyNameEntities.get(0).getFriendlyNameInEnglish());
                     place_of_industry_txt.setText(getPatternByMaterialIdEntities.get(0).getPlaceInEnglish());
                     brand_txt.setText(getPatternByMaterialIdEntities.get(0).getBrandInEnglish());
                     material_type_txt.setText(getPatternByMaterialIdEntities.get(0).getMaterialInEnglish());
                     color_txt.setText(getColorByIdEntities.get(0).getColorInEnglish());

                 }

                    try {

                        Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "images/Color/" + getColorByIdEntities.get(0).getColorImage())
                                .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.placeholder))
                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.placeholder)).into(image_color);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(Call<MaterialByIdGetRespone> call, Throwable t) {

            }
        });

    }


    public void viewPagerGet(ArrayList<String> image) {
        mViewDetailAdapter = new ViewDetailPagerAdapter(this, image);
        mViewDetailsViewpager.setAdapter(mViewDetailAdapter);
        mViewDetailsViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mViewDetailsViewpager);
        //page change tracker
        mViewDetailsViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("ViewDetail");
    }

    public void InsertMaterialMapping() {
        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("FriendlyNameInEnglish", friendly_name_english.getText().toString());
        map.put("FriendlyNameInArabic", friendly_name_arabic.getText().toString());
        map.put("MaterialId", String.valueOf(AppConstants.PATTERN_ID));
        restService.InsertMaterialMapping(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms

                        transparentProgressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), R.string.material_updated_sucessfully, Toast.LENGTH_LONG).show();
                        finish();
                    }
                }, 2000);


            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });

    }


    ;


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.material_details);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void deleteMaterialMap() {


        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("MaterialId", AppConstants.PATTERN_ID);

        restService.deleteMaterialMap(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
