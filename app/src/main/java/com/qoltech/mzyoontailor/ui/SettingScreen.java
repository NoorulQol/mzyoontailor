package com.qoltech.mzyoontailor.ui;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingScreen extends BaseActivity {

    @BindView(R.id.setting_par_lay)
    LinearLayout mSettingScreenParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.language_setting_txt)
    TextView mLanguageSettingTxt;

    @BindView(R.id.hint_txt)
    TextView mHintOnOffTxt;

    @BindView(R.id.hint_img)
    ImageView mHintOnOffImg;

    private UserDetailsEntity mUserDetailsEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_setting_screen);
        initView();
    }

    public void initView(){
        ButterKnife.bind(this);

        setupUI(mSettingScreenParLay);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(SettingScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntity = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        setHeader();

    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.setting).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);
        String mLanguage = mUserDetailsEntity.getLanguage().
                equalsIgnoreCase("Arabic") ? getResources().getString(R.string.arabic) :
                getResources().getString(R.string.english);
        mLanguageSettingTxt.setText(getResources().getString(R.string.language_setting)+" - "+mLanguage);
        mHintOnOffImg.setImageDrawable(mUserDetailsEntity.getHINT_ON_OFF().
                equalsIgnoreCase("") ? getResources().getDrawable(R.drawable.toggle_oragne_clr_on):
                mUserDetailsEntity.getHINT_ON_OFF().equalsIgnoreCase("ON") ? getResources().getDrawable(R.drawable.toggle_oragne_clr_on) : getResources().
                        getDrawable(R.drawable.toggle_orange_clr_off));
        mHintOnOffTxt.setText(mUserDetailsEntity.getHINT_ON_OFF().equalsIgnoreCase("")? getResources()
                .getString(R.string.hint_on): mUserDetailsEntity.getHINT_ON_OFF().equalsIgnoreCase("ON") ? getResources().getString(R.string.hint_on) : getResources().getString(R.string.hint_off));
    }
    @OnClick({R.id.header_left_side_img,R.id.language_setting_lay,R.id.hint_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.language_setting_lay:
                nextScreen(LanguageSettingScreen.class,true);
                break;
            case R.id.hint_lay:
                if (mUserDetailsEntity.getHINT_ON_OFF().equalsIgnoreCase("")){
                    mUserDetailsEntity.setHINT_ON_OFF("ON");
                    mHintOnOffImg.setImageDrawable(getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
                    mHintOnOffTxt.setText("Hint - ON");
                }
                else if (mUserDetailsEntity.getHINT_ON_OFF().equalsIgnoreCase("ON")){
                    mUserDetailsEntity.setHINT_ON_OFF("OFF");
                    mHintOnOffImg.setImageDrawable(getResources().getDrawable(R.drawable.toggle_orange_clr_off));
                    mHintOnOffTxt.setText("Hint - OFF");

                }else if (mUserDetailsEntity.getHINT_ON_OFF().equalsIgnoreCase("OFF")){
                    mUserDetailsEntity.setHINT_ON_OFF("ON");
                    mHintOnOffImg.setImageDrawable(getResources().getDrawable(R.drawable.toggle_oragne_clr_on));
                    mHintOnOffTxt.setText("Hint - ON");

                }
                PreferenceUtil.storeUserDetails(SettingScreen.this, mUserDetailsEntity);
                break;
        }

    }

    public void getLanguage(){

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.setting_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.setting_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
