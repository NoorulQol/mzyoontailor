package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AppointmentListAdapter;
import com.qoltech.mzyoontailor.entity.AppointmentListEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.AppointmentListResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppointmentListScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.appointment_list_recycler_view)
    RecyclerView mAppointmentListRecyclerView;

    private AppointmentListAdapter mAppointmentAdapter;

    @BindView(R.id.appointment_list_par_lay)
    LinearLayout mAppointmentListParLay;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;
    SharedPreferences sharedPreferences;
    private UserDetailsEntity mUserDetailsEntityRes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_appointment_list_screen);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        initView();

    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAppointmentListParLay);

        setHeader();


        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AppointmentListScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getApponitmentListApiCall();

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AppointmentListResponse) {
            AppointmentListResponse mResponse = (AppointmentListResponse) resObj;
            setAdapter(mResponse.getResult());
        }
    }

    public void getApponitmentListApiCall() {
        if (NetworkUtil.isNetworkAvailable(AppointmentListScreen.this)) {
            APIRequestHandler.getInstance().getAppointmentListApi(sharedPreferences.getString("TailorId", ""), AppointmentListScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentListScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getApponitmentListApiCall();
                }
            });
        }
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.appointment_list));
        mRightSideImg.setVisibility(View.VISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

    }

    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<AppointmentListEntity> appointmentListEntities) {

        if (mAppointmentAdapter == null) {

            mAppointmentListRecyclerView.setVisibility(appointmentListEntities.size() > 0 ? View.VISIBLE : View.GONE);
            mEmptyListLay.setVisibility(appointmentListEntities.size() > 0 ? View.GONE : View.VISIBLE);

            mAppointmentAdapter = new AppointmentListAdapter(this, appointmentListEntities);
            mAppointmentListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAppointmentListRecyclerView.setAdapter(mAppointmentAdapter);


        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAppointmentAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }
}

