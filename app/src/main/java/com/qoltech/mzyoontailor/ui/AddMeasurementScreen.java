package com.qoltech.mzyoontailor.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AddMeasurementListAdapter;
import com.qoltech.mzyoontailor.entity.AddMeasurementEntity;
import com.qoltech.mzyoontailor.entity.GetAppoinmentLeftMaterialEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.AddMeasurementResponse;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMeasurementScreen extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.add_measurement_par_lay)
    LinearLayout mAddMeasurementParLay;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    @BindView(R.id.add_new_measurement_recycler_view)
    RecyclerView mAddNewMeasurementRecyclerView;

    private AddMeasurementListAdapter mMeasurementListAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;
    ApiService restService;
    List<GetAppoinmentLeftMaterialEntity> getAppoinmentLeftMaterialEntities;
    TextView measurement_appointment_type, measurement_appointment_date,
            measurement_appointment_left, measurement_appointment_time, measurement_appointment_status;
    private Dialog mManuallyDialog, mEditTxtDialog;
    LinearLayout app_txt_layout, appointment_layout, measurement_appointment_status_layout;
    SharedPreferences sharedPreferences;
    boolean isThere = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_add_measurement_screen);
        restService = ((MainApplication) getApplication()).getClient();
        measurement_appointment_type = (TextView) findViewById(R.id.measurement_appointment_type);
        measurement_appointment_date = (TextView) findViewById(R.id.measurement_appointment_date);
        measurement_appointment_left = (TextView) findViewById(R.id.measurement_appointment_left);
        measurement_appointment_time = (TextView) findViewById(R.id.measurement_appointment_time);
        measurement_appointment_status = (TextView) findViewById(R.id.measurement_appointment_status);
        app_txt_layout = (LinearLayout) findViewById(R.id.app_txt_layout);
        measurement_appointment_status_layout = findViewById(R.id.measurement_appointment_status_layout);
        appointment_layout = (LinearLayout) findViewById(R.id.appointment_layout);
        AppConstants.MEASUREMENT_LIST_CHECK = new HashSet<>();
//        getOrderDetailsForTailor();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        initView();


        if (AppConstants.TAILORMEASUREMENT.equalsIgnoreCase("TAILORMEASUREMENT")) {
//

            if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
                getOrderDetailsForTailor("Tailor");
            } else {

                getOrderDetailsForTailor("Buyer");

            }
            app_txt_layout.setVisibility(View.VISIBLE);
            appointment_layout.setVisibility(View.VISIBLE);
        } else {

            app_txt_layout.setVisibility(View.GONE);
            appointment_layout.setVisibility(View.GONE);

        }
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAddMeasurementParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AddMeasurementScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        getLanguage();

        getAddMeasurementListApiCall();
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement_list).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_for_measurement_list));

    }

    public void getAddMeasurementListApiCall() {
        if (NetworkUtil.isNetworkAvailable(AddMeasurementScreen.this)) {
            APIRequestHandler.getInstance().getAddMeasurementApiCall(AppConstants.SUB_DRESS_TYPE_ID,
                    AppConstants.DIRECT_USERS_ID, sharedPreferences.getString("TailorId", ""), AddMeasurementScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AddMeasurementScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAddMeasurementListApiCall();
                }
            });
        }
    }

    @OnClick({R.id.header_left_side_img, R.id.add_new_measurement_par_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.add_new_measurement_par_lay:


                alertDismiss(mManuallyDialog);
                mManuallyDialog = getDialog(AddMeasurementScreen.this, R.layout.pop_up_manually);

                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                Window window = mManuallyDialog.getWindow();

                if (window != null) {
                    LayoutParams.copyFrom(window.getAttributes());
                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(LayoutParams);
                    window.setGravity(Gravity.CENTER);
                }

//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//                    AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInArabic());
//
//                } else {
//                    ViewCompat.setLayoutDirection(mManuallyDialog.findViewById(R.id.pop_up_manaully_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//                    AppConstants.MEASUREMENT_ONE = String.valueOf(mMeasurementList.get(0).getMeasurementInEnglish());
//
//                }
                TextView cancelTxt, addTxt, cancelEmptyTxt, addEmptyTxt;

                RecyclerView countryRecyclerView;

                LinearLayout emptyLay, addLay;


                /*Init view*/
                emptyLay = mManuallyDialog.findViewById(R.id.manually_empty_cancel_add_lay);
                addLay = mManuallyDialog.findViewById(R.id.manually_cancel_add_lay);
                cancelTxt = mManuallyDialog.findViewById(R.id.pop_up_cancel_txt);
                addTxt = mManuallyDialog.findViewById(R.id.pop_up_new_txt);
                cancelEmptyTxt = mManuallyDialog.findViewById(R.id.pop_up_cancel_bottom_txt);
                addEmptyTxt = mManuallyDialog.findViewById(R.id.pop_up_new_bottom_txt);

//                emptyLay.setVisibility(mManualEntity.size() > 0 ? View.GONE : View.VISIBLE);
//                addLay.setVisibility(mManualEntity.size() > 0 ? View.VISIBLE : View.GONE);

//                countryRecyclerView = mManuallyDialog.findViewById(R.id.pop_up_manually_recycler_view);
//                mManuallyAdapter = new ManuallyAdapter(MeasurementOneScreen.this, mManualEntity, mManuallyDialog);

//                countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//                countryRecyclerView.setAdapter(mManuallyAdapter);

                /*Set data*/
                cancelTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mManuallyDialog.dismiss();
                    }
                });
                cancelEmptyTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mManuallyDialog.dismiss();

                    }
                });
                addEmptyTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mManuallyDialog.dismiss();
                        EditTextDialog();
                    }
                });
                addTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mManuallyDialog.dismiss();
                        EditTextDialog();
                    }
                });

                alertShowing(mManuallyDialog);
                AppConstants.ADD_NEW_MEASUREMENT = "ADD_NEW_MEASUREMENT";
//                nextScreen(MeasurementTwoScreen.class, true);
                break;
        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AddMeasurementResponse) {
            AddMeasurementResponse mResponse = (AddMeasurementResponse) resObj;

            setAdapter(mResponse.getResult());
            for (int i = 0; i < mResponse.getResult().size(); i++) {
                AppConstants.MEASUREMENT_LIST.add(mResponse.getResult().get(i).getName());

//                AppConstants.MEASUREMENT_LIST_CHECK.add(mResponse.getResult().get(i).getName());


//                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
////                                    AppConstants.MEASUREMENT_LIST.add(mResponse.getResult().get(i).getNameInArabic());
//                    AppConstants.MEASUREMENT_LIST.add(mResponse.getResult().get(i).getName());
//
//                }
//                else {
//
//                }
            }
        }
    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<AddMeasurementEntity> measurementListEntities) {

        if (mMeasurementListAdapter == null) {

            mAddNewMeasurementRecyclerView.setVisibility(measurementListEntities.size() > 0 ? View.VISIBLE : View.GONE);
            mEmptyListLay.setVisibility(measurementListEntities.size() > 0 ? View.GONE : View.VISIBLE);

            mMeasurementListAdapter = new AddMeasurementListAdapter(this, measurementListEntities);
            mAddNewMeasurementRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mAddNewMeasurementRecyclerView.setAdapter(mMeasurementListAdapter);


        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMeasurementListAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_measurement_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_measurement_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        previousScreen(BottomNavigationScreen.class,true);
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    public void getOrderDetailsForTailor(String orderTypes) {

//        restService.getOrderDetailsResponseForTailor("212").enqueue(new Callback<GetOrderDetailsResponseCheck>() {
        restService.getOrderDetailsResponseForTailor(AppConstants.ORDER_ID, AppConstants.ORDER_TYPE, orderTypes).enqueue(new Callback<GetOrderDetailsResponseCheck>() {

            @Override
            public void onResponse(Call<GetOrderDetailsResponseCheck> call, Response<GetOrderDetailsResponseCheck> response) {

                if (response.isSuccessful()) {
                    try {

//                        orderDetails = response.body().getResult().getOrderDetail();
//                        buyerAddresses = response.body().getResult().getBuyerAddress();
////                        productPrices = response.body().getResult().getProductPrice();
////                        shippingCharges = response.body().getResult().getShippingCharges();
//                        statuses = response.body().getResult().getStatus();
//                        materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();
//                        referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();
//                        balances = response.body().getResult().getBalances();
//                        getAppoinmentLeftMeasurementEntities = response.body().getResult().getGetAppoinmentLeftMeasurementEntities();

                        getAppoinmentLeftMaterialEntities = response.body().getResult().getGetAppoinmentLeftMaterialEntities();

                        measurement_appointment_type.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInEnglish());
                        measurement_appointment_date.setText(getAppoinmentLeftMaterialEntities.get(0).getOrderDt());
                        measurement_appointment_left.setText("" + getAppoinmentLeftMaterialEntities.get(0).getAppointmentLeftMeterialCount());

                        measurement_appointment_time.setText(getAppoinmentLeftMaterialEntities.get(0).getAppointmentTime());
                        measurement_appointment_status.setText(getAppoinmentLeftMaterialEntities.get(0).getStatus());
//                        measurement_appointment_type = (TextView) findViewById(R.id.measurement_appointment_type);
//                        measurement_appointment_date = (TextView) findViewById(R.id.measurement_appointment_date);
//                        measurement_appointment_left = (TextView) findViewById(R.id.measurement_appointment_left);
                        if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Approved")) {
                            measurement_appointment_status_layout.setVisibility(View.GONE);
                        } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Buyer Approved")) {

                            measurement_appointment_status_layout.setVisibility(View.GONE);

                        }
//                        if (orderDetails == null) {
//                            orderDetails = new ArrayList<>();
//                        }
//                        if (buyerAddresses == null) {
//                            buyerAddresses = new ArrayList<>();
//                        }
//                        if (statuses == null) {
//                            statuses = new ArrayList<>();
//                        }
//                        if (materialImagesSkillUpdateGetEntities.size()<=0) {
//                            materialImagesSkillUpdateGetEntities = new ArrayList<>();
//
//                            add_material_lable_layout.setVisibility(View.GONE);
//                            add_material_image_layout.setVisibility(View.GONE);
//
//                        }
//                        if (referenceImagesSkillUpdateGetEntities.size()<=0) {
//                            referenceImagesSkillUpdateGetEntities = new ArrayList<>();
//                            add_refernce_lable_layout.setVisibility(View.GONE);
//                            add_reference_img_layout.setVisibility(View.GONE);
//
//                        }
//
//                        if (referenceImagesSkillUpdateGetEntities.size() > 0) {
//                            add_refernce_lable_layout.setVisibility(View.VISIBLE);
//                            add_reference_img_layout.setVisibility(View.VISIBLE);
//
//                        }
//
//                        if (materialImagesSkillUpdateGetEntities.size() > 0) {
//
//                            add_material_lable_layout.setVisibility(View.VISIBLE);
//                            add_material_image_layout.setVisibility(View.VISIBLE);
//
//                        }


//                        setImageList();
//                        order_id.setText("#" + String.valueOf(orderDetails.get(0).getOrderId()));
//                        product_name.setText(orderDetails.get(0).getProductName());
////                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
////                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
////                        shipping_handling.setText("0");
//
//
//                        stitching_charges.setText(String.valueOf(response.body().getResult().getStichingAndMaterialCharge()));
//                        urgent_charges.setText(String.valueOf(response.body().getResult().getUrgentStichingCharges()));
//                        measurement_charges.setText(String.valueOf(response.body().getResult().getMeasurementCharges()));
////                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialDeliveryCharges()));
//                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialCharges()));
//                        ftotal = response.body().getResult().getStichingAndMaterialCharge() +
//                                response.body().getResult().getUrgentStichingCharges() +
//                                response.body().getResult().getMeasurementCharges() +
//                                response.body().getResult().getMaterialCharges();
//                        total.setText(String.valueOf(ftotal));
//                        price.setText(String.valueOf(ftotal));
//                        order_date.setText(orderDetails.get(0).getOrderDt());
//                        dress_type.setText(orderDetails.get(0).getDressTypeName());
//
//                        try {
//                            Glide.with(getApplicationContext())
//                                    .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
//                                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
//                                    .into(order_details_image);
//
//                        } catch (Exception ex) {
//                            Log.e(AppConstants.TAG, ex.getMessage());
//                        }
//                        tracking_current_status.setText("Tracking Current Status: " + statuses.get(0).getStatus());
//                        payment_status.setText((orderDetails.get(0).getPaymentStatus()));
//                        qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));
//
//                        order_type.setText(orderDetails.get(0).getServiceType());
//
//                        material_type.setText(orderDetails.get(0).getServiceType());
//                        name.setText(buyerAddresses.get(0).getFirstName());
//                        address.setText("  " + buyerAddresses.get(0).getFloor() + "\n" + "  " + buyerAddresses.get(0).getArea() + "\n" +
//                                "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
//                        number.setText(buyerAddresses.get(0).getPhoneNo());
//
//
//                        AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
//                        AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
//                        genderId = String.valueOf(orderDetails.get(0).getGender());
//
////                        material_appointment_type = (TextView) findViewById(R.id.material_appointment_type);
////                        material_appointment_date = (TextView) findViewById(R.id.material_appointment_date);
////                        material_appointment_left = (TextView) findViewById(R.id.material_appointment_left);
////                        measurement_appointment_type = (TextView) findViewById(R.id.measurement_appointment_type);
////                        measurement_appointment_date = (TextView) findViewById(R.id.measurement_appointment_date);
////                        measurement_appointment_left = (TextView) findViewById(R.id.measurement_appointment_left);
//
//
//                        measurement_appointment_type.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInEnglish());
//                        measurement_appointment_left.setText("" + getAppoinmentLeftMaterialEntities.get(0).
//                                getAppointmentLeftMaterialCount());
//
//
//                        measurement_appointment_date.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInEnglish());
//
//                        material_appointment_type.setText(getAppoinmentLeftMeasurementEntities.get(0).getHeaderInEnglish());
//                        material_appointment_left.setText("" + getAppoinmentLeftMeasurementEntities.get(0).
//                                getAppointmentLeftMeasurementCount());
//
//
//                        material_appointment_date.setText(getAppoinmentLeftMeasurementEntities.get(0).getHeaderInEnglish());
//                        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
//
//                            AppConstants.BALANCE = balances.get(0).getBalance().toString();
//                            bal = ftotal - Float.parseFloat(AppConstants.BALANCE);
//                            balance.setText(balances.get(0).getBalance().toString());
//                            amount_paid.setText(String.valueOf(bal));
////                            Toast.makeText(getApplicationContext(), AppConstants.BALANCE, Toast.LENGTH_LONG).show();
//
//
//                            if (Float.parseFloat(AppConstants.BALANCE) > 0) {
//
//                                paid_layout.setVisibility(View.VISIBLE);
//                                balance_layout.setVisibility(View.VISIBLE);
//                                pay_balance_layout.setVisibility(View.VISIBLE);
//
//                            }
//
//                            if (Float.parseFloat(AppConstants.BALANCE) == 0) {
//                                pay_balance_layout.setVisibility(View.GONE);
//                            }
//
//                        }
//                        float bal = Float.parseFloat(AppConstants.BALANCE);
//                        float remaining = ftotal - bal;
//                        tax.setText(productPrices.get(0).getTax());
//
//
//                        appointment_charges.setText(String.valueOf(productPrices.get(0).getAppoinment()));
//                        total.setText(String.valueOf(productPrices.get(0).getTotal()));


                    } catch (Exception e) {

                    }

                }

            }

            @Override
            public void onFailure(Call<GetOrderDetailsResponseCheck> call, Throwable t) {

            }
        });
    }

    public void EditTextDialog() {
        alertDismiss(mEditTxtDialog);
        mEditTxtDialog = getDialog(AddMeasurementScreen.this, R.layout.pop_up_edit_txt);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mEditTxtDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(mEditTxtDialog.findViewById(R.id.pop_up_measurement_manually_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        TextView cancelTxt, okTxt;
        final EditText manuallyEdtTxt;

        /*Init view*/

        cancelTxt = mEditTxtDialog.findViewById(R.id.pop_up_cancel_txt);
        okTxt = mEditTxtDialog.findViewById(R.id.pop_up_ok_txt);
        manuallyEdtTxt = mEditTxtDialog.findViewById(R.id.measurement_manually_edt_txt);


        /*Set data*/
        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTxtDialog.dismiss();
            }
        });

        okTxt.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
//                if (AppConstants.MEASUREMENT_LIST.get(0).equalsIgnoreCase(manuallyEdtTxt.getText().toString())) {
//                    manuallyEdtTxt.setError("Name Already Present In The List");
//
//                }
                if (manuallyEdtTxt.getText().toString().isEmpty()) {
                    manuallyEdtTxt.setError("Enter Measurement Name");
                }


//                if (AppConstants.MEASUREMENT_LIST.stream().anyMatch(s -> s.equals("banana"))) {
//                    System.out.println("Found");
//                } else {
//                    throw new SkipException("could not be found");
//                }


//                if (AppConstants.MEASUREMENT_LIST.get(0).equalsIgnoreCase("Noorul")) {
//                    manuallyEdtTxt.setError("Name Already Present In The List");
//
//                }
//                if (!AppConstants.MEASUREMENT_LIST_CHECK.contains(manuallyEdtTxt.getText().toString())) {
////                else if (AppConstants.MEASUREMENT_LIST_CHECK.toArray().toString().equalsIgnoreCase(manuallyEdtTxt.getText().toString())) {
//                    manuallyEdtTxt.setError("Name Already Present In The List");
////                }
//                }
//                else {
//                    isThere =Arrays.asList(AppConstants.MEASUREMENT_LIST).contains(manuallyEdtTxt.getText().toString());
//
//                    if (isThere==true) {
//                        manuallyEdtTxt.setError("Name Already Present In The List");
//
//                    }
                else {


                    for (int i = 0; i < AppConstants.MEASUREMENT_LIST.size(); i++) {
                        String[] parts = AppConstants.MEASUREMENT_LIST.get(i).split("-");
                        String part1 = parts[0]; // 004
                        String part2 = parts[1].trim().toLowerCase();
                        String lastOne = parts[parts.length - 1];

                        AppConstants.MEASUREMENT_LIST_CHECK.add(part2);


                    }


                    if (AppConstants.MEASUREMENT_LIST_CHECK.contains(manuallyEdtTxt.getText().toString().trim().toLowerCase())) {
                        manuallyEdtTxt.setError(getResources().getString(R.string.measurement_name_already_exist));

                    } else {

                        mEditTxtDialog.dismiss();
                        AppConstants.MEASUREMENT_MANUALLY = manuallyEdtTxt.getText().toString().trim();
//                    AppConstants.MEASUREMENT_ID = "-1";

                        AppConstants.DIRECT_MEASUREMENT_TYPE = "Manually";

                        nextScreen(MeasurementTwoScreen.class, true);
                    }
                }
//                }

            }
        });

        alertShowing(mEditTxtDialog);
    }

}
