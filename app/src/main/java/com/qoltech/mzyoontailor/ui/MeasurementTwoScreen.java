package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.activity.OrderDetailsActivity;
import com.qoltech.mzyoontailor.adapter.MeasurementTwoPartsAdapter;
import com.qoltech.mzyoontailor.adapter.SliderImgaeAdapter;
import com.qoltech.mzyoontailor.entity.GetMeasurementImageEntity;
import com.qoltech.mzyoontailor.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoontailor.entity.InsertMeasurmentValueEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GetMeasurementTwoResponse;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeasurementTwoScreen extends BaseActivity {

    @BindView(R.id.measurement_two_par_lay)
    RelativeLayout mMeasurementTwoParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.measurement_two_slider_one)
    RelativeLayout mMeasurementTwoSliderOne;

    @BindView(R.id.measurement_two_slider_two)
    RelativeLayout mMeasurementTwoSliderTwo;

    @BindView(R.id.measurement_two_slider_three)
    RelativeLayout mMeasurementTwoSliderThree;

    @BindView(R.id.measurement_two_slider_four)
    RelativeLayout mMeasurementTwoSliderFour;

    @BindView(R.id.measurement_two_slider_five)
    RelativeLayout mMeasurementTwoSliderFive;

    @BindView(R.id.measurement_two_part_recy_view)
    RecyclerView mMeasurementTwoPartRecyView;

    @BindView(R.id.view_pager_lay)
    LinearLayout mViewPageLay;

    @BindView(R.id.measurement_two_img_lay)
    RelativeLayout mMeasurementTwoImgLay;

    @BindView(R.id.measurement_two_parts_lay)
    RelativeLayout mMeasurementTwoPartsLay;

    @BindView(R.id.measurement_two_img_txt)
    TextView mMeasurementTwoImgTxt;

    @BindView(R.id.measurement_two_part_txt)
    TextView mMeasurementTwoPartTxt;

    @BindView(R.id.measurement_two_cm_lay)
    RelativeLayout mMeasurementTwoCmLay;

    @BindView(R.id.measurement_two_in_lay)
    RelativeLayout mMeasurementTwoInLay;

    @BindView(R.id.measurement_two_cm_txt)
    TextView mMeasurementTwoCmTxt;

    @BindView(R.id.measurement_two_in_txt)
    TextView mMeasurementTwoInTxt;

    @BindView(R.id.measurement_two_nxt_lay)
    RelativeLayout mMeasurementTwoNxtLay;

//    private SectionedRecyclerViewAdapter sectionAdapter;

    private MeasurementTwoPartsAdapter mMeasurementPartsAdapter;

    private SliderImgaeAdapter mSliderAdapter;

    private ArrayList<GetMeasurementPartEntity> mGetMeasurementPartEntity;

    private ArrayList<GetMeasurementImageEntity> mGetMeasurementImgeEntity;

    private UserDetailsEntity mUserDetailsEntityRes;

    HashMap<String, String> parts = new HashMap<>();

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;
    ApiService restService;

    Button mSkipBtn, mBackBtn, mNextBtn;
    RelativeLayout mImageBgLay, mPartsBgLay;
    LinearLayout mArrowBgLay, mCmToInBgLay, mImagePartsBgLay;
    Dialog mHintDialog;
    int mCountInt = 1;
    String mBackHandleString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_measurement_two_screen);
        initView();
        if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female") ||
                AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")) {
            mMeasurementTwoSliderFive.setVisibility(View.VISIBLE);
        } else {
            mMeasurementTwoSliderFive.setVisibility(View.GONE);
        }

    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mMeasurementTwoParLay);

        setHeader();

        restService = ((MainApplication) getApplication()).getClient();

        mGetMeasurementPartEntity = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MeasurementTwoScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getMeasurementTwoApiCall();
        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
            getHintDialog();

        }


        if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male") || AppConstants.GENDER_NAME.equalsIgnoreCase("Female")
                || AppConstants.GENDER_NAME.equalsIgnoreCase("Boy") ||
                AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")) {
            mMeasurementTwoSliderFive.setVisibility(View.VISIBLE);
        } else {
            mMeasurementTwoSliderFive.setVisibility(View.GONE);
        }

    }


    public void getMeasurementTwoApiCall() {
        if (NetworkUtil.isNetworkAvailable(MeasurementTwoScreen.this)) {
            APIRequestHandler.getInstance().getMeasurementTwoApiCall(AppConstants.SUB_DRESS_TYPE_ID, MeasurementTwoScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getMeasurementTwoApiCall();
                }
            });
        }
    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.measurement_2));
        mRightSideImg.setVisibility(View.VISIBLE);
        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));
        mBackHandleString = "false";

    }

    @OnClick({R.id.header_left_side_img, R.id.measurement_two_nxt_lay, R.id.measurement_two_img_lay, R.id.measurement_two_parts_lay, R.id.measurement_two_cm_lay, R.id.measurement_two_in_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                getLanguage();
                onBackPressed();
                break;
            case R.id.measurement_two_nxt_lay:
//                getLanguage();
//                parts = AppConstants.MEASUREMENT_MAP;
//                if (parts.isEmpty()) {
//                    Toast.makeText(getApplicationContext(), "Insert Measurement values", Toast.LENGTH_LONG).show();
//                } else {
//
//                    if (AppConstants.TAILORMEASUREMENT.equalsIgnoreCase("TAILORMEASUREMENT")) {
//                        insertMeasaurementValue();
//                    } else {
//                        nextScreen(AddReferenceScreen.class, true);
//                    }
//                }


                parts = AppConstants.MEASUREMENT_MAP;

                String MapCount = "0";

                Set keys = AppConstants.MEASUREMENT_MAP.keySet();
                Iterator itr = keys.iterator();

                String key;
                String value;
                while (itr.hasNext()) {
                    key = (String) itr.next();
                    value = (String) AppConstants.MEASUREMENT_MAP.get(key);
                    if (value.equalsIgnoreCase("")) {
                        MapCount = String.valueOf(Integer.parseInt(MapCount) - 1);
                    } else {
                        MapCount = String.valueOf(Integer.parseInt(MapCount) + 1);
                    }
                }

                if (MapCount.equalsIgnoreCase(String.valueOf(mGetMeasurementPartEntity.size()))) {
                    if (AppConstants.TAILORMEASUREMENT.equalsIgnoreCase("TAILORMEASUREMENT")) {
                        insertMeasaurementValue();
                    } else {
                        nextScreen(AddReferenceScreen.class, true);
                    }
                } else {
                    mMeasurementTwoNxtLay.clearAnimation();
                    mMeasurementTwoNxtLay.setAnimation(ShakeErrorUtils.shakeError());

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        Toast.makeText(MeasurementTwoScreen.this, "يرجى اختيار جميع القياس", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MeasurementTwoScreen.this, R.string.please_select_all_the_measurement, Toast.LENGTH_LONG).show();

                    }
                }


                break;
            case R.id.measurement_two_img_lay:
                mMeasurementTwoImgLay.setBackgroundResource(R.color.app_color);
                mMeasurementTwoImgTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTwoPartsLay.setBackgroundResource(R.color.grey_clr);
                mMeasurementTwoPartTxt.setTextColor(getResources().getColor(R.color.black));
                mMeasurementTwoPartRecyView.setVisibility(View.GONE);
                mEmptyListLay.setVisibility(View.GONE);
                if (mSliderAdapter != null) {
                    mSliderAdapter = new SliderImgaeAdapter(this, mGetMeasurementImgeEntity, mGetMeasurementPartEntity);
                    mViewPager.setAdapter(mSliderAdapter);
                }
                mViewPageLay.setVisibility(View.VISIBLE);
                break;
            case R.id.measurement_two_parts_lay:
                mMeasurementTwoImgLay.setBackgroundResource(R.color.grey_clr);
                mMeasurementTwoImgTxt.setTextColor(getResources().getColor(R.color.black));
                mMeasurementTwoPartsLay.setBackgroundResource(R.color.app_color);
                mMeasurementTwoPartTxt.setTextColor(getResources().getColor(R.color.white));
                mViewPageLay.setVisibility(View.GONE);
//                mMeasurementTwoPartRecyView.setVisibility(View.VISIBLE);
                mMeasurementTwoPartRecyView.setVisibility(mGetMeasurementPartEntity.size() > 0 ? View.VISIBLE : View.GONE);
                mEmptyListLay.setVisibility(mGetMeasurementPartEntity.size() > 0 ? View.GONE : View.VISIBLE);
//                if (sectionAdapter != null){
//                    sectionAdapter.notifyDataSetChanged();
//                }
                if (mMeasurementPartsAdapter != null) {
                    mMeasurementPartsAdapter.notifyDataSetChanged();
                }
                mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.orange_clr_circle);
                mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);
                break;
            case R.id.measurement_two_cm_lay:
                AppConstants.UNITS = "CM";
                mMeasurementTwoCmLay.setBackgroundResource(R.drawable.orange_clr_with_corner);
                mMeasurementTwoInLay.setBackgroundResource(R.drawable.white_right_top_bottom_corner);

//                if (sectionAdapter != null){
//                    sectionAdapter.notifyDataSetChanged();
//                }
                if (mMeasurementPartsAdapter != null) {
                    mMeasurementPartsAdapter.notifyDataSetChanged();
                }
                if (mSliderAdapter != null) {
                    mSliderAdapter = new SliderImgaeAdapter(this, mGetMeasurementImgeEntity, mGetMeasurementPartEntity);
                    mViewPager.setAdapter(mSliderAdapter);
                }
                mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.orange_clr_circle);
                mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);

                mMeasurementTwoCmTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTwoInTxt.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.measurement_two_in_lay:
                AppConstants.UNITS = "IN";

                mMeasurementTwoCmLay.setBackgroundResource(R.drawable.white_left_top_bottom_corner);
                mMeasurementTwoInLay.setBackgroundResource(R.drawable.orange_clr_with_corner);

//                if (sectionAdapter != null){
//                    sectionAdapter.notifyDataSetChanged();
//                }
                if (mMeasurementPartsAdapter != null) {
                    mMeasurementPartsAdapter.notifyDataSetChanged();
                }
                if (mSliderAdapter != null) {
                    mSliderAdapter = new SliderImgaeAdapter(this, mGetMeasurementImgeEntity, mGetMeasurementPartEntity);
                    mViewPager.setAdapter(mSliderAdapter);
                }
                mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.orange_clr_circle);
                mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);

                mMeasurementTwoCmTxt.setTextColor(getResources().getColor(R.color.black));
                mMeasurementTwoInTxt.setTextColor(getResources().getColor(R.color.white));
                break;
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);

        if (resObj instanceof GetMeasurementTwoResponse) {
            GetMeasurementTwoResponse mResponse = (GetMeasurementTwoResponse) resObj;
            mGetMeasurementPartEntity = mResponse.getResult().getMeasurements();
            mGetMeasurementImgeEntity = mResponse.getResult().getImage();

            setPartsAdapter(mResponse.getResult().getMeasurements());

            viewPagerGet(mResponse.getResult().getImage(), mResponse.getResult().getMeasurements());

        }
        if (resObj instanceof InsertMeasurementValueResponse) {
            InsertMeasurementValueResponse mResponse = (InsertMeasurementValueResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                Toast.makeText(getApplicationContext(), mResponse.getResponseMsg(), Toast.LENGTH_SHORT).show();

                if (AppConstants.MEASUREMENT_EXISTING.equalsIgnoreCase("EXISTING")) {
                    updateMeasurementByTailor(AppConstants.ORDER_ID, AppConstants.MEASUREMENT_ID.equalsIgnoreCase("") ? "-1" : AppConstants.MEASUREMENT_ID);

                } else {
                    updateMeasurementByTailor(AppConstants.ORDER_ID, String.valueOf(mResponse.getResult()));
                }

                if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")){
                    nextScreen(HomeActivity.class, true);

                }
                else {
                    finish();
                    nextScreen(OrderDetailsActivity.class, true);


                }
            }
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    public void viewPagerGet(ArrayList<GetMeasurementImageEntity> image, ArrayList<GetMeasurementPartEntity> parts) {
        mSliderAdapter = new SliderImgaeAdapter(this, image, parts);
        mViewPager.setAdapter(mSliderAdapter);

        //page change tracker
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.orange_clr_circle);
                    mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);
                } else if (position == 1) {
                    mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.orange_clr_circle);
                    mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);
                } else if (position == 2) {
                    mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.orange_clr_circle);
                    mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);
                } else if (position == 3) {
                    mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.orange_clr_circle);
                    mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.app_clr_circle);
                } else if (position == 4) {
                    mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
                    mMeasurementTwoSliderFive.setBackgroundResource(R.drawable.orange_clr_circle);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("MeasurementTwo");

    }

    @Override
    public void onBackPressed() {

        if (mBackHandleString.equalsIgnoreCase("true")) {
            super.onBackPressed();
            AppConstants.HEAD = "";
            AppConstants.NECK = "";
            AppConstants.CHEST = "";
            AppConstants.WAIST = "";
            AppConstants.THIGH = "";
            AppConstants.ANKLE = "";
            AppConstants.KNEE = "";

            AppConstants.OVER_ALL_HEIGHT = "";
            AppConstants.SHORTS = "";
            AppConstants.OUTSEAM = "";
            AppConstants.INSEAM = "";

            AppConstants.SHOULDER = "";
            AppConstants.HALF_SLEEVE = "";
            AppConstants.BICEP = "";
            AppConstants.HIP = "";
            AppConstants.BOTTOM = "";

            AppConstants.HEIGHT = "";
            AppConstants.SLEEVE = "";
            AppConstants.WRIST = "";

            AppConstants.WOMEN_OVER_BUST = "";
            AppConstants.WOMEN_UNDER_BUST = "";
            AppConstants.WOMEN_HIP_BONE = "";
            AppConstants.WOMEN_THIGH = "";
            AppConstants.WOMEN_KNEE = "";
            AppConstants.WOMEN_CALF = "";
            AppConstants.WOMEN_ANKLE = "";

            AppConstants.WOMEN_HEIGHT = "";
            AppConstants.WOMEN_STW = "";
            AppConstants.WOMENT_NLTB = "";
            AppConstants.WOMEN_NLTC = "";
            AppConstants.WOMEN_STHB = "";
            AppConstants.WOMEN_WTHB = "";
            AppConstants.WOMEN_STHB = "";
            AppConstants.WOMEN_INSEAM = "";
            AppConstants.WOMEN_OUTSEAM = "";

            AppConstants.WOMEN_SHOULDER = "";
            AppConstants.WOMEN_BICEP = "";
            AppConstants.WOMEN_WRIST = "";

            AppConstants.WOMEN_SLEEVE = "";

            AppConstants.BOY_HEAD = "";
            AppConstants.BOY_NECK = "";
            AppConstants.BOY_CHEST = "";
            AppConstants.BOY_WAIST = "";
            AppConstants.BOY_THIGH = "";
            AppConstants.BOY_KNEE = "";
            AppConstants.BOY_ANKLE = "";

            AppConstants.BOY_OVER_ALL_HEIGHT = "";
            AppConstants.BOY_SHORTS = "";
            AppConstants.BOY_OUTSEAM = "";
            AppConstants.BOY_INSEAM = "";

            AppConstants.BOY_SHOULDER = "";
            AppConstants.BOY_HALFSLEEVE = "";
            AppConstants.BOY_HIP = "";
            AppConstants.BOY_BICEP = "";
            AppConstants.BOY_BOTTOM = "";

            AppConstants.BOY_HEIGHT = "";
            AppConstants.BOY_SLEEVE = "";
            AppConstants.BOY_WRIST = "";

            AppConstants.GIRL_OVER_BURST = "";
            AppConstants.GIRL_UNDER_BURST = "";
            AppConstants.GIRL_HIP_BONE = "";
            AppConstants.GIRL_THIGH = "";
            AppConstants.GIRL_KNEE = "";
            AppConstants.GIRL_CALF = "";
            AppConstants.GIRL_ANKLE = "";

            AppConstants.GIRL_HEAD = "";
            AppConstants.GIRL_NECK = "";
            AppConstants.GIRL_BUST = "";
            AppConstants.GIRL_WAIST = "";
            AppConstants.GIRL_FULL_HIP = "";

            AppConstants.GIRL_HEIGHT = "";
            AppConstants.GIRL_STW = "";
            AppConstants.GIRL_NLTC = "";
            AppConstants.GIRL_NLTB = "";
            AppConstants.GIRL_STHB = "";
            AppConstants.GIRL_WTHB = "";
            AppConstants.GIRL_HTH = "";
            AppConstants.GIRL_INSEAM = "";
            AppConstants.GIRL_OUTSEAM = "";

            AppConstants.GIRL_SHOULDER = "";
            AppConstants.GIRL_BICEP = "";
            AppConstants.GIRL_WRIST = "";

            AppConstants.GIRL_SLEEVE = "";

            AppConstants.HEAD_INCHES = "";
            AppConstants.NECK_INCHES = "";
            AppConstants.CHEST_INCHES = "";
            AppConstants.WAIST_INCHES = "";
            AppConstants.THIGH_INCHES = "";
            AppConstants.ANKLE_INCHES = "";
            AppConstants.KNEE_INCHES = "";

            AppConstants.OVER_ALL_HEIGHT_INCHES = "";
            AppConstants.SHORTS_INCHES = "";
            AppConstants.OUTSEAM_INCHES = "";
            AppConstants.INSEAM_INCHES = "";

            AppConstants.SHOULDER_INCHES = "";
            AppConstants.HALF_SLEEVE_INCHES = "";
            AppConstants.BICEP_INCHES = "";
            AppConstants.HIP_INCHES = "";
            AppConstants.BOTTOM_INCHES = "";

            AppConstants.HEIGHT_INCHES = "";
            AppConstants.SLEEVE_INCHES = "";
            AppConstants.WRIST_INCHES = "";

            AppConstants.WOMEN_OVER_BUST_INCHES = "";
            AppConstants.WOMEN_UNDER_BUST_INCHES = "";
            AppConstants.WOMEN_HIP_BONE_INCHES = "";
            AppConstants.WOMEN_THIGH_INCHES = "";
            AppConstants.WOMEN_KNEE_INCHES = "";
            AppConstants.WOMEN_CALF_INCHES = "";
            AppConstants.WOMEN_ANKLE_INCHES = "";

            AppConstants.WOMEN_HEIGHT_INCHES = "";
            AppConstants.WOMEN_STW_INCHES = "";
            AppConstants.WOMENT_NLTB_INCHES = "";
            AppConstants.WOMEN_NLTC_INCHES = "";
            AppConstants.WOMEN_STHB_INCHES = "";
            AppConstants.WOMEN_WTHB_INCHES = "";
            AppConstants.WOMEN_STHB_INCHES = "";
            AppConstants.WOMEN_INSEAM_INCHES = "";
            AppConstants.WOMEN_OUTSEAM_INCHES = "";

            AppConstants.WOMEN_SHOULDER_INCHES = "";
            AppConstants.WOMEN_BICEP_INCHES = "";
            AppConstants.WOMEN_WRIST_INCHES = "";

            AppConstants.WOMEN_SLEEVE_INCHES = "";

            AppConstants.BOY_HEAD_INCHES = "";
            AppConstants.BOY_NECK_INCHES = "";
            AppConstants.BOY_CHEST_INCHES = "";
            AppConstants.BOY_WAIST_INCHES = "";
            AppConstants.BOY_THIGH_INCHES = "";
            AppConstants.BOY_KNEE_INCHES = "";
            AppConstants.BOY_ANKLE_INCHES = "";

            AppConstants.BOY_OVER_ALL_HEIGHT_INCHES = "";
            AppConstants.BOY_SHORTS_INCHES = "";
            AppConstants.BOY_OUTSEAM_INCHES = "";
            AppConstants.BOY_INSEAM_INCHES = "";

            AppConstants.BOY_SHOULDER_INCHES = "";
            AppConstants.BOY_HALFSLEEVE_INCHES = "";
            AppConstants.BOY_HIP_INCHES = "";
            AppConstants.BOY_BICEP_INCHES = "";
            AppConstants.BOY_BOTTOM_INCHES = "";

            AppConstants.BOY_HEIGHT_INCHES = "";
            AppConstants.BOY_SLEEVE_INCHES = "";
            AppConstants.BOY_WRIST_INCHES = "";

            AppConstants.GIRL_OVER_BURST_INCHES = "";
            AppConstants.GIRL_UNDER_BURST_INCHES = "";
            AppConstants.GIRL_HIP_BONE_INCHES = "";
            AppConstants.GIRL_THIGH_INCHES = "";
            AppConstants.GIRL_KNEE_INCHES = "";
            AppConstants.GIRL_CALF_INCHES = "";
            AppConstants.GIRL_ANKLE_INCHES = "";

            AppConstants.GIRL_HEAD_INCHES = "";
            AppConstants.GIRL_NECK_INCHES = "";
            AppConstants.GIRL_BUST_INCHES = "";
            AppConstants.GIRL_WAIST_INCHES = "";
            AppConstants.GIRL_FULL_HIP_INCHES = "";

            AppConstants.GIRL_HEIGHT_INCHES = "";
            AppConstants.GIRL_STW_INCHES = "";
            AppConstants.GIRL_NLTC_INCHES = "";
            AppConstants.GIRL_NLTB_INCHES = "";
            AppConstants.GIRL_STHB_INCHES = "";
            AppConstants.GIRL_WTHB_INCHES = "";
            AppConstants.GIRL_HTH_INCHES = "";
            AppConstants.GIRL_INSEAM_INCHES = "";
            AppConstants.GIRL_OUTSEAM_INCHES = "";

            AppConstants.GIRL_SHOULDER_INCHES = "";
            AppConstants.GIRL_BICEP_INCHES = "";
            AppConstants.GIRL_WRIST_INCHES = "";

            AppConstants.GIRL_SLEEVE_INCHES = "";

            AppConstants.MEASUREMENT_MAP = new HashMap<>();

            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        } else {

            DialogManager.getInstance().showOptionPopup(MeasurementTwoScreen.this,
                    getResources().getString(R.string.sure_want_to_go_back), getResources().getString(R.string.yes), getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                @Override
                public void onNegativeClick() {

                }

                @Override
                public void onPositiveClick() {
                    mBackHandleString = "true";

                    getLanguage();
                    onBackPressed();
                }
            });
        }

    }

    /*Set Adapter for the Recycler view*/
    public void setPartsAdapter(ArrayList<GetMeasurementPartEntity> getMeasurementPartsList) {
//        mMeasurementTwoPartRecyView.setVisibility(getMeasurementPartsList.size()>0 ? View.VISIBLE : View.GONE);
//        mEmptyListLay.setVisibility(getMeasurementPartsList.size() > 0 ? View.GONE : View.VISIBLE);

        if (mMeasurementPartsAdapter == null) {

            mMeasurementPartsAdapter = new MeasurementTwoPartsAdapter(this, getMeasurementPartsList);
            mMeasurementTwoPartRecyView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mMeasurementTwoPartRecyView.setAdapter(mMeasurementPartsAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMeasurementPartsAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (sectionAdapter != null){
//            sectionAdapter.notifyDataSetChanged();
//        }
        if (mMeasurementPartsAdapter != null) {
            mMeasurementPartsAdapter.notifyDataSetChanged();
        }
        if (mSliderAdapter != null) {
            mSliderAdapter = new SliderImgaeAdapter(this, mGetMeasurementImgeEntity, mGetMeasurementPartEntity);
            mViewPager.setAdapter(mSliderAdapter);
            mMeasurementTwoSliderOne.setBackgroundResource(R.drawable.orange_clr_circle);
            mMeasurementTwoSliderTwo.setBackgroundResource(R.drawable.app_clr_circle);
            mMeasurementTwoSliderThree.setBackgroundResource(R.drawable.app_clr_circle);
            mMeasurementTwoSliderFour.setBackgroundResource(R.drawable.app_clr_circle);
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_two_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_two_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(MeasurementTwoScreen.this, "ar");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_two_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(MeasurementTwoScreen.this, "en");

        }
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }


    public void insertMeasaurementValue() {
        if (NetworkUtil.isNetworkAvailable(MeasurementTwoScreen.this)) {
            AppConstants.USER_ID_STR = mUserDetailsEntityRes.getUSER_ID();

            ArrayList<InsertMeasurmentValueEntity> mResponse = new ArrayList<>();

            Set keys = AppConstants.MEASUREMENT_MAP.keySet();
            Iterator itr = keys.iterator();

            String key;
            String value;
            while (itr.hasNext()) {
                InsertMeasurmentValueEntity measurmentValueEntity = new InsertMeasurmentValueEntity();

                key = (String) itr.next();
                value = (String) AppConstants.MEASUREMENT_MAP.get(key);
                System.out.println(key + " - " + value);
                measurmentValueEntity.setMeasurementId(String.valueOf(key));
                measurmentValueEntity.setValue(value);
                mResponse.add(measurmentValueEntity);
            }


            InsertMeasurementValueModal measurementValueModal = new InsertMeasurementValueModal();
            measurementValueModal.setUserId(AppConstants.DIRECT_USERS_ID);
            measurementValueModal.setDressTypeId(AppConstants.SUB_DRESS_TYPE_ID);
            measurementValueModal.setMeasurementValue(mResponse);
            measurementValueModal.setUnits(AppConstants.UNITS);

            measurementValueModal.setMeasurementBy(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);
            measurementValueModal.setCreatedBy("Tailor");
            measurementValueModal.setName(AppConstants.MEASUREMENT_MANUALLY.equalsIgnoreCase("") ? "" : AppConstants.MEASUREMENT_MANUALLY);

            APIRequestHandler.getInstance().insertMeasurementValue(measurementValueModal, MeasurementTwoScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(MeasurementTwoScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertMeasaurementValue();
                }
            });
        }
    }


    public void updateMeasurementByTailor(String orderId, String measurementId) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", orderId);
        map.put("MeasurementId", measurementId);

        restService.updateMeasurementByTailor(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                AppConstants.MEASUREMENT_ID = "";
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


    public void getHintDialog() {
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(MeasurementTwoScreen.this, R.layout.pop_up_measurement_two_hint);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.measurement_two_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);


        } else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.measurement_two_hint_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        final Button mSkipBtn, mBackBtn, mNextBtn;
        final RelativeLayout mImageBgLay, mPartsBgLay;
        final LinearLayout mArrowBgLay, mCmToInBgLay, mImagePartsBgLay;
        final TextView mMeasurementTwoPartImgHintTxt;


        /*Init view*/
        mSkipBtn = mHintDialog.findViewById(R.id.measurement_skip_hint_btn);
        mBackBtn = mHintDialog.findViewById(R.id.measurement_back_hint_btn);
        mNextBtn = mHintDialog.findViewById(R.id.measurement_next_hint_btn);

        mImageBgLay = mHintDialog.findViewById(R.id.measurement_img_par_lay);
        mPartsBgLay = mHintDialog.findViewById(R.id.measurement_part_par_lay);

        mArrowBgLay = mHintDialog.findViewById(R.id.measurement_arrow_par_lay);
        mCmToInBgLay = mHintDialog.findViewById(R.id.measurement_cm_to_in_par_lay);

        mImagePartsBgLay = mHintDialog.findViewById(R.id.measeurement_img_part_par_lay);

        mMeasurementTwoPartImgHintTxt = mHintDialog.findViewById(R.id.measurement_two_part_img_hint);

        if (mCountInt == 1) {
            mBackBtn.setVisibility(View.INVISIBLE);
            mPartsBgLay.setVisibility(View.INVISIBLE);
            mArrowBgLay.setVisibility(View.INVISIBLE);
            mCmToInBgLay.setVisibility(View.INVISIBLE);
            mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_image_tab_hint));
        }

        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = --mCountInt;
                if (mCountInt == 1) {
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mImageBgLay.setVisibility(View.VISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
//                    mNextBtn.setText(getResources().getString(R.string.next));
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        Toast.makeText(MeasurementTwoScreen.this, "لتالى", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MeasurementTwoScreen.this, R.string.next, Toast.LENGTH_LONG).show();

                    }


                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_image_tab_hint));

                } else if (mCountInt == 2) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.VISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        Toast.makeText(MeasurementTwoScreen.this, "لتالى", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MeasurementTwoScreen.this, R.string.next, Toast.LENGTH_LONG).show();

                    }
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_parts_tab_hint));

                } else if (mCountInt == 3) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.VISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        Toast.makeText(MeasurementTwoScreen.this, "لتالى", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MeasurementTwoScreen.this, R.string.next, Toast.LENGTH_LONG).show();

                    }
                } else if (mCountInt == 4) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.VISIBLE);
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        Toast.makeText(MeasurementTwoScreen.this, "صلت هذا", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(MeasurementTwoScreen.this, R.string.got_it, Toast.LENGTH_LONG).show();

                    }
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);

                }
            }
        });

        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountInt = ++mCountInt;
                if (mCountInt == 1) {
                    mBackBtn.setVisibility(View.INVISIBLE);
                    mImageBgLay.setVisibility(View.VISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_image_tab_hint));

                } else if (mCountInt == 2) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.VISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mImagePartsBgLay.setVisibility(View.VISIBLE);
                    mMeasurementTwoPartImgHintTxt.setText(getResources().getString(R.string.measurement_parts_tab_hint));

                } else if (mCountInt == 3) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.VISIBLE);
                    mCmToInBgLay.setVisibility(View.INVISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.next));
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);
                } else if (mCountInt == 4) {
                    mBackBtn.setVisibility(View.VISIBLE);
                    mImageBgLay.setVisibility(View.INVISIBLE);
                    mPartsBgLay.setVisibility(View.INVISIBLE);
                    mArrowBgLay.setVisibility(View.INVISIBLE);
                    mCmToInBgLay.setVisibility(View.VISIBLE);
                    mNextBtn.setText(getResources().getString(R.string.got_it));
                    mImagePartsBgLay.setVisibility(View.INVISIBLE);
                } else if (mCountInt >= 5) {
                    mHintDialog.dismiss();
                }
            }
        });

        /*Set data*/
        mSkipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });


        alertShowing(mHintDialog);
    }
}

