package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.ServiceTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.ServiceTypeResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceTypeScreen extends BaseActivity {

    @BindView(R.id.service_type_par_lay)
    LinearLayout mServiceTypeParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

//    @BindView(R.id.service_type_appointement_img)
//    ImageView mServiceTypeAppointmentImg;
//
//    @BindView(R.id.service_type_appointement_txt)
//    TextView mServicetypeAppointmentTxt;
//
//    @BindView(R.id.service_type_appointement_body_img)
//    ImageView mServiceTypeAppointmentBodyImg;

    @BindView(R.id.service_type_urgent_img)
    ImageView mServiceTypeUrgentImg;

    @BindView(R.id.service_type_urgent_txt)
    TextView mServiceTypeUrgentTxt;

    @BindView(R.id.service_type_urgent_bodyimg)
    ImageView mServiceTypeUrgentBodyImg;

    @BindView(R.id.service_type_normal_img)
    ImageView mServiceTypeNormalImg;

    @BindView(R.id.service_type_normal_txt)
    TextView mServiceTypeNormalTxt;

    @BindView(R.id.service_type_normal_body_img)
    ImageView mServiceTypeNormalBodyImg;


    @BindView(R.id.service_type_appointement_img)
    ImageView mServiceTypeAppointmentHeadImg;

    @BindView(R.id.service_type_appointement_body_img)
    ImageView mServiceTypeAppointmentBodyImg;


    @BindView(R.id.service_type_appointement_txt)
    TextView mServiceTypeAppointmentTxt;

    @BindView(R.id.service_type_urgent_lay)
    LinearLayout mServiceTypeUrgentLay;

    @BindView(R.id.service_type_normal_lay)
    LinearLayout mServiceTypeNormalLay;

    @BindView(R.id.service_type_appointement_lay)
    LinearLayout mServiceTypeAppointmentLay;



    @BindView(R.id.service_type_urgent_empty_lay)
    TextView mServiceTypeUrgentEmptyTxt;

    @BindView(R.id.service_type_normal_empty_lay)
    TextView mServiceTypeNormalEmtptyTxt;

    @BindView(R.id.service_type_appointement_empty_lay)
    TextView mServiceTypeAppointmentEmptyTxt;


    ArrayList<ServiceTypeEntity> serviceTypeResponses;

    private UserDetailsEntity mUserDetailsEntityRes;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_service_type_screen);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        initView();

    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mServiceTypeParLay);

        setHeader();

        serviceTypeResponses = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ServiceTypeScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        getNewFlowServiceTypeApicall();
    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.service_type));
        mRightSideImg.setVisibility(View.VISIBLE);

    }

//    @OnClick({R.id.header_left_side_img,R.id.service_type_urgent_lay,R.id.service_type_normal_lay})
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.header_left_side_img:
//                onBackPressed();
//                break;
////            case R.id.service_type_appointement_lay:
////                if (serviceTypeResponses.size()>0){
////                    AppConstants.DELIVERY_TYPE_ID =String.valueOf(serviceTypeResponses.get(0).getId());
////                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
////                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInArabic());
////                    }else {
////                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInEnglish());
////                    }
////                    nextScreen(TailorListScreen.class, true);
////                }
////                break;
//            case R.id.service_type_urgent_lay:
//                if (serviceTypeResponses.size()>0) {
//                    AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(0).getId());
//                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInArabic());
//                    }else {
//                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInEnglish());
//                    }
//                    nextScreen(OrderSummaryScreen.class, true);
//                }
//                break;
//
//            case R.id.service_type_normal_lay:
//                if (serviceTypeResponses.size()>0) {
//                    AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(1).getId());
//                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
//                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(1).getDeliveryTypeInArabic());
//                    }else {
//                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(1).getDeliveryTypeInEnglish());
//                    }
//                    nextScreen(OrderSummaryScreen.class, true);
//                }
//                break;
//        }
//    }
//
//    public void getServiceTypeApicall(){
//        if (NetworkUtil.isNetworkAvailable(ServiceTypeScreen.this)){
//            APIRequestHandler.getInstance().getServiceTypeApiCall(ServiceTypeScreen.this);
//        }else {
//            DialogManager.getInstance().showNetworkErrorPopup(ServiceTypeScreen.this, new InterfaceBtnCallBack() {
//                @Override
//                public void onPositiveClick() {
//                    getServiceTypeApicall();
//                }
//            });
//        }
//    }


    @OnClick({R.id.header_left_side_img, R.id.service_type_urgent_lay, R.id.service_type_normal_lay, R.id.service_type_appointement_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.service_type_urgent_lay:
                if (serviceTypeResponses.get(0).isStatus()) {
                    AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(0).getId());
                    AppConstants.SERVICE_TYPE_CHARGES=serviceTypeResponses.get(0).getId();
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInArabic());
                    } else {
                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(0).getDeliveryTypeInEnglish());
                    }
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                        nextScreen(OrderSummaryScreen.class, true);

                    } else {
                        nextScreen(OrderSummaryScreen.class, true);

                    }
                }
                break;

            case R.id.service_type_normal_lay:
                if (serviceTypeResponses.get(1).isStatus()) {
                    AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(1).getId());
                    AppConstants.SERVICE_TYPE_CHARGES=serviceTypeResponses.get(1).getId();

                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(1).getDeliveryTypeInArabic());
                    } else {
                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(1).getDeliveryTypeInEnglish());
                    }
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                        nextScreen(OrderSummaryScreen.class, true);

                    } else {
                        nextScreen(OrderSummaryScreen.class, true);

                    }
                }
                break;
            case R.id.service_type_appointement_lay:
                if (serviceTypeResponses.get(2).isStatus()) {
                    AppConstants.DELIVERY_TYPE_ID = String.valueOf(serviceTypeResponses.get(2).getId());
                    AppConstants.SERVICE_TYPE_CHARGES=serviceTypeResponses.get(2).getId();

                    AppConstants.SERVICE_TYPE_NAME_ENG = String.valueOf(serviceTypeResponses.get(2).getDeliveryTypeInEnglish());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(2).getDeliveryTypeInArabic());
                    } else {
                        AppConstants.SERVICE_TYPE_NAME = String.valueOf(serviceTypeResponses.get(2).getDeliveryTypeInEnglish());
                    }
                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
                        nextScreen(OrderSummaryScreen.class, true);

                    } else {
                        nextScreen(OrderSummaryScreen.class, true);

                    }
                }
                break;
        }
    }

    public void getNewFlowServiceTypeApicall() {
        if (NetworkUtil.isNetworkAvailable(ServiceTypeScreen.this)) {
            APIRequestHandler.getInstance().
                    getNewFlowServiceTypeApiCall(sharedPreferences.getString("TailorId", ""),
                            ServiceTypeScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(ServiceTypeScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowServiceTypeApicall();
                }
            });
        }
    }

//    @Override
//    public void onRequestSuccess(Object resObj) {
//        super.onRequestSuccess(resObj);
//        if (resObj instanceof ServiceTypeResponse) {
//            ServiceTypeResponse mResponse = (ServiceTypeResponse) resObj;
//
//            serviceTypeResponses = mResponse.getResult();
////            mServicetypeAppointmentTxt.setText(mResponse.getResult().get(0).getDeliveryTypeInEnglish());
////
////            try {
////                Glide.with(ServiceTypeScreen.this)
////                        .load(AppConstants.IMAGE_BASE_URL+"Images/ServiceType/"+mResponse.getResult().get(0).getHeaderImage())
////                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
////                        .into(mServiceTypeAppointmentImg);
////
////            } catch (Exception ex) {
////                Log.e(AppConstants.TAG, ex.getMessage());
////            }
////
////            try {
////                Glide.with(ServiceTypeScreen.this)
////                        .load(AppConstants.IMAGE_BASE_URL+"Images/ServiceType/"+mResponse.getResult().get(0).getDeliveryImage())
////                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
////                        .into(mServiceTypeAppointmentBodyImg);
////
////            } catch (Exception ex) {
////                Log.e(AppConstants.TAG, ex.getMessage());
////            }
//
//
//            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                mServiceTypeUrgentTxt.setText(mResponse.getResult().get(0).getDeliveryTypeInArabic());
//
//            } else {
//                mServiceTypeUrgentTxt.setText(mResponse.getResult().get(0).getDeliveryTypeInEnglish());
//
//            }
//
//
//            try {
//                Glide.with(ServiceTypeScreen.this)
//                        .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + mResponse.getResult().get(0).getHeaderImage())
//                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                        .into(mServiceTypeUrgentImg);
//
//            } catch (Exception ex) {
//                Log.e(AppConstants.TAG, ex.getMessage());
//            }
//
//            try {
//                Glide.with(ServiceTypeScreen.this)
//                        .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + mResponse.getResult().get(0).getDeliveryImage())
//                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                        .into(mServiceTypeUrgentBodyImg);
//
//            } catch (Exception ex) {
//                Log.e(AppConstants.TAG, ex.getMessage());
//            }
//
//            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                mServiceTypeNormalTxt.setText(mResponse.getResult().get(1).getDeliveryTypeInArabic());
//
//            } else {
//                mServiceTypeNormalTxt.setText(mResponse.getResult().get(1).getDeliveryTypeInEnglish());
//
//            }
//
//
//            try {
//                Glide.with(ServiceTypeScreen.this)
//                        .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + mResponse.getResult().get(1).getHeaderImage())
//                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                        .into(mServiceTypeNormalImg);
//
//            } catch (Exception ex) {
//                Log.e(AppConstants.TAG, ex.getMessage());
//            }
//
//            try {
//                Glide.with(ServiceTypeScreen.this)
//                        .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + mResponse.getResult().get(1).getDeliveryImage())
//                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                        .into(mServiceTypeNormalBodyImg);
//
//            } catch (Exception ex) {
//                Log.e(AppConstants.TAG, ex.getMessage());
//            }
//
//        }
//    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ServiceTypeResponse) {
            ServiceTypeResponse mResponse = (ServiceTypeResponse) resObj;

            serviceTypeResponses = mResponse.getResult();

//            if (!AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                for (int i = 0; i < serviceTypeResponses.size(); i++) {
//                    serviceTypeResponses.get(i).setStatus(true);
//                }
//            }
//            if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("Companies-Material")&&AppConstants.DIRECT_MEASUREMENT_TYPE.equalsIgnoreCase("Manually")) {
//
//            } else {
//                for (int i = 0; i < serviceTypeResponses.size(); i++) {
//                    if (serviceTypeResponses.get(i).getDeliveryTypeInEnglish().equalsIgnoreCase("Schedule")) {
//                        serviceTypeResponses.get(i).setStatus(false);
//                    }
//                }
//            }

//            if (serviceTypeResponses.size()>0){
//                mServiceTypeUrgentLay.setVisibility(View.VISIBLE);
//            }
//
//            if (serviceTypeResponses.size()>1){
//                mServiceTypeNormalLay.setVisibility(View.VISIBLE);
//            }
//            if (serviceTypeResponses.size()>2){
//                mServiceTypeAppointmentLay.setVisibility(View.VISIBLE);
//            }
            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                for (int i = 0; i < serviceTypeResponses.size(); i++) {
                    if (i == 0) {
                        mServiceTypeUrgentTxt.setText(serviceTypeResponses.get(0).getDeliveryTypeInArabic());

                    } else if (i == 1) {
                        mServiceTypeNormalTxt.setText(serviceTypeResponses.get(1).getDeliveryTypeInArabic());

                    } else if (i == 2) {
                        mServiceTypeAppointmentTxt.setText(serviceTypeResponses.get(2).getDeliveryTypeInArabic());
                    }
                }

            } else {
                for (int i = 0; i < serviceTypeResponses.size(); i++) {
                    if (i == 0) {
                        mServiceTypeUrgentTxt.setText(serviceTypeResponses.get(0).getDeliveryTypeInEnglish());

                    } else if (i == 1) {
                        mServiceTypeNormalTxt.setText(serviceTypeResponses.get(1).getDeliveryTypeInEnglish());

                    } else if (i == 2) {
                        mServiceTypeAppointmentTxt.setText(serviceTypeResponses.get(2).getDeliveryTypeInEnglish());

                    }
                }

            }

            for (int i = 0; i < serviceTypeResponses.size(); i++) {
                if (i == 0) {
                    try {
                        Glide.with(ServiceTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(0).getHeaderImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(mServiceTypeUrgentImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                    try {
                        Glide.with(ServiceTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(0).getDeliveryImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(mServiceTypeUrgentBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                } else if (i == 1) {
                    try {
                        Glide.with(ServiceTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(1).getHeaderImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(mServiceTypeNormalImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                    try {
                        Glide.with(ServiceTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(1).getDeliveryImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(mServiceTypeNormalBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                } else if (i == 2) {
                    try {
                        Glide.with(ServiceTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(2).getHeaderImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(mServiceTypeAppointmentHeadImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                    try {
                        Glide.with(ServiceTypeScreen.this)
                                .load(AppConstants.IMAGE_BASE_URL + "Images/ServiceType/" + serviceTypeResponses.get(2).getDeliveryImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(mServiceTypeAppointmentBodyImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }
            }

            mServiceTypeUrgentLay.setVisibility(View.VISIBLE);
            mServiceTypeNormalLay.setVisibility(View.VISIBLE);
            mServiceTypeAppointmentLay.setVisibility(View.VISIBLE);
            mServiceTypeUrgentBodyImg.setVisibility(View.VISIBLE);
            mServiceTypeNormalBodyImg.setVisibility(View.VISIBLE);
            mServiceTypeAppointmentBodyImg.setVisibility(View.VISIBLE);

            if (AppConstants.DIRECT_ORDER_TYPE.equalsIgnoreCase("Companies-Material")&&AppConstants.DIRECT_MEASUREMENT_TYPE.equalsIgnoreCase("Manually")) {

            } else {
                for (int i = 0; i < serviceTypeResponses.size(); i++) {
                    if (serviceTypeResponses.get(i).getDeliveryTypeInEnglish().equalsIgnoreCase("Schedule")) {
                        serviceTypeResponses.get(i).setStatus(false);
                    }
                }

                mServiceTypeAppointmentLay.setVisibility(View.INVISIBLE);
            }


            for (int i = 0; i < serviceTypeResponses.size(); i++) {
                if (!serviceTypeResponses.get(i).isStatus()) {
                    if (i == 0) {
                        mServiceTypeUrgentEmptyTxt.setVisibility(View.VISIBLE);
                        mServiceTypeUrgentBodyImg.setAlpha(30);

                    } else if (i == 1) {
                        mServiceTypeNormalEmtptyTxt.setVisibility(View.VISIBLE);
                        mServiceTypeNormalBodyImg.setAlpha(30);

                    } else if (i == 2) {
                        mServiceTypeAppointmentEmptyTxt.setVisibility(View.VISIBLE);
                        mServiceTypeAppointmentBodyImg.setAlpha(30);

                    }
                } else {
                    if (i == 0) {
                        mServiceTypeUrgentEmptyTxt.setVisibility(View.GONE);
                    } else if (i == 1) {
                        mServiceTypeNormalEmtptyTxt.setVisibility(View.GONE);
                    } else if (i == 2) {
                        mServiceTypeAppointmentEmptyTxt.setVisibility(View.GONE);
                    }
                }
            }

        }
    }
    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.service_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.service_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }
}
