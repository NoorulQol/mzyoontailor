package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.CustomizationOneBrandAdapter;
import com.qoltech.mzyoontailor.adapter.CustomizationOnePlaceAdapter;
import com.qoltech.mzyoontailor.adapter.CustomizationOneSessionAdapter;
import com.qoltech.mzyoontailor.entity.ApicallidEntity;
import com.qoltech.mzyoontailor.entity.CustomizationBrandEntity;
import com.qoltech.mzyoontailor.entity.CustomizationPlaceEntity;
import com.qoltech.mzyoontailor.entity.CustomizationSessionEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.CustomizationOneApiCallModal;
import com.qoltech.mzyoontailor.modal.GetCustomizationOneResponse;
import com.qoltech.mzyoontailor.modal.NewFlowCustomizationOneApiCall;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomizationOneScreen extends BaseActivity {

    private CustomizationOneSessionAdapter mSeasonalAdapter;
    private CustomizationOnePlaceAdapter mIndustryAdapter;
    private CustomizationOneBrandAdapter mBrandAdapter;

    private ArrayList<CustomizationSessionEntity> mSeasonalList;
    private ArrayList<CustomizationPlaceEntity> mIndustryList;
    private ArrayList<CustomizationBrandEntity> mBrandList;

    @BindView(R.id.customization_one_par_lay)
    LinearLayout mCustomizationOneParLay;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;


    @BindView(R.id.customize_seaonal_recycler_view)
    RecyclerView mSeasonalRecyclerView;

    @BindView(R.id.customize_industry_recycler_view)
    RecyclerView mIndustryRecyclerView;

    @BindView(R.id.customize_brands_recycler_view)
    RecyclerView mBrandRecyclerView;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    private UserDetailsEntity mUserDetailsEntityRes;
SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_customization_one_screen);
        sharedPreferences=getSharedPreferences("TailorId",MODE_PRIVATE);

        initView();
    }
    public void initView(){

        ButterKnife.bind(this);

        setupUI(mCustomizationOneParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationOneScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        setHeader();

        mSeasonalList = new ArrayList<>();
        mIndustryList = new ArrayList<>();
        mBrandList = new ArrayList<>();

        getNewFlowCustomizationApiCall();

        getLanguage();
    }

    @OnClick({R.id.customize_one_nxt_lay,R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customize_one_nxt_lay:

                nextScreen(CustomizationTwoScreen.class,true);
                break;
            case R.id.header_left_side_img:
                AppConstants.SEASONAL_ID = "";
                AppConstants.PLACE_INDUSTRY_ID = "";
                AppConstants.BRANDS_ID = "";
                onBackPressed();
                break;
        }

    }

    public void getCustomizationApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){

            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mSeasonalId = new ArrayList<>();
            CustomizationOneApiCallModal mModal = new CustomizationOneApiCallModal();


            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("1");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }

            }
            if (AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                mSeasonEntityId.setId("1");

                mSeasonalId.add(mSeasonEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                    mSeasonEntityId.setId(items.get(i));
                    mSeasonalId.add(mSeasonEntityId);
                }

            }




            mModal.setPlaceofOrginId(mOrginId);
            mModal.setSeasonId(mSeasonalId);

            APIRequestHandler.getInstance().getCustomizationOneApi(mModal,CustomizationOneScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationApiCall();
                }
            });
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.customization_1));
        mRightSideImg.setVisibility(View.VISIBLE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));


    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof GetCustomizationOneResponse){
            GetCustomizationOneResponse mResponse = (GetCustomizationOneResponse)resObj;

            setSeasonalAdapter(mResponse.getResult().getSeasons());
            setIndustryAdapter(mResponse.getResult().getPlaceofIndustrys());
            setBrandAdapter(mResponse.getResult().getMaterialBrand());

        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    /*Set Adapter for the Recycler view*/
    public void setSeasonalAdapter(ArrayList<CustomizationSessionEntity> mSeasonalList) {

        if (mSeasonalAdapter == null) {


            mSeasonalAdapter = new CustomizationOneSessionAdapter(this,mSeasonalList);
            mSeasonalRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mSeasonalRecyclerView.setAdapter(mSeasonalAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSeasonalAdapter.notifyDataSetChanged();
                }
            });
        }

    }
    /*Set Adapter for the Recycler view*/
    public void setIndustryAdapter(ArrayList<CustomizationPlaceEntity> mIndustryList) {

        if (mIndustryAdapter == null) {


            mIndustryAdapter = new CustomizationOnePlaceAdapter(this,mIndustryList);
            mIndustryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mIndustryRecyclerView.setAdapter(mIndustryAdapter);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mIndustryAdapter.notifyDataSetChanged();
                }
            });
        }

    }
    /*Set Adapter for the Recycler view*/
    public void setBrandAdapter(ArrayList<CustomizationBrandEntity> mBrandList) {


        mBrandRecyclerView.setVisibility(mBrandList.size()>0 ? View.VISIBLE : View.GONE);
        mEmptyListLay.setVisibility(mBrandList.size() > 0 ? View.GONE : View.VISIBLE);

        mBrandAdapter = new CustomizationOneBrandAdapter(this,mBrandList);
        mBrandRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mBrandRecyclerView.setAdapter(mBrandAdapter);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.SEASONAL_ID = "";
        AppConstants.SEASONAL_NAME = "";

        AppConstants.PLACE_INDUSTRY_ID = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";

        AppConstants.BRANDS_ID = "";
        AppConstants.BRANDS_NAME = "";
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_one_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.customization_one_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }


    public void getNewFlowCustomizationApiCall(){
        if (NetworkUtil.isNetworkAvailable(CustomizationOneScreen.this)){

            ArrayList<ApicallidEntity> mOrginId = new ArrayList<>();
            ArrayList<ApicallidEntity> mSeasonalId = new ArrayList<>();
            NewFlowCustomizationOneApiCall mModal = new NewFlowCustomizationOneApiCall();


            if (AppConstants.PLACE_INDUSTRY_ID.equalsIgnoreCase("")){
                ApicallidEntity mOrginEntityId = new ApicallidEntity();

                mOrginEntityId.setId("1");

                mOrginId.add(mOrginEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.PLACE_INDUSTRY_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mOrginEntityId = new ApicallidEntity();

                    mOrginEntityId.setId(items.get(i));
                    mOrginId.add(mOrginEntityId);
                }

            }
            if (AppConstants.SEASONAL_ID.equalsIgnoreCase("")){
                ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                mSeasonEntityId.setId("1");

                mSeasonalId.add(mSeasonEntityId);
            }else {
                List<String> items = Arrays.asList(AppConstants.SEASONAL_ID.split("\\s*,\\s*"));

                for (int i=0 ;i<items.size(); i++){
                    ApicallidEntity mSeasonEntityId = new ApicallidEntity();

                    mSeasonEntityId.setId(items.get(i));
                    mSeasonalId.add(mSeasonEntityId);
                }

            }


            mModal.setTailorId(Integer.parseInt(sharedPreferences.getString("TailorId","")));
            mModal.setPlaceofOrginId(mOrginId);
            mModal.setSeasonId(mSeasonalId);

            APIRequestHandler.getInstance().getNewFlowCustomizationOneApi(mModal,CustomizationOneScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(CustomizationOneScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCustomizationApiCall();
                }
            });
        }
    }
}
