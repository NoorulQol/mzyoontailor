package com.qoltech.mzyoontailor.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewDetailPagerAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.ViewDetailsNewFlowResponse;
import com.qoltech.mzyoontailor.modal.ViewDetailsResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ZoomOutPageTransformer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ViewDetailsScreen extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.view_details_par_lay)
    LinearLayout mViewDetailsParLay;

    @BindView(R.id.view_details_pattern_name_txt)
    TextView mViewDetailsPatternNameTxt;

    @BindView(R.id.view_details_seasonal_name_txt)
    TextView mViewDetailsSeasonalNameTxt;

    @BindView(R.id.view_details_place_industry_name_txt)
    TextView mViewDetailsPlaceIndustryNameTxt;

    @BindView(R.id.view_details_brands_name_txt)
    TextView mViewDetailsBrandsNameTxt;

    @BindView(R.id.view_details_material_type_name_txt)
    TextView mViewDetailsMaterialTypeNameTxt;

    @BindView(R.id.view_details_color_name_txt)
    TextView mViewDetailsColorNameTxt;

    @BindView(R.id.view_details_view_pager)
    ViewPager mViewDetailsViewpager;

    @BindView(R.id.pageIndicatorView)
    com.rd.PageIndicatorView mPageIndicator;

    ArrayList<String> mImageList;

    ViewDetailPagerAdapter mViewDetailAdapter;

    private UserDetailsEntity mUserDetailsEntityRes;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_view_detauils_screen);
        sharedPreferences=getSharedPreferences("TailorId",MODE_PRIVATE);
        initView();

    }

    public void initView(){

        ButterKnife.bind(this);

        setupUI(mViewDetailsParLay);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ViewDetailsScreen.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

        mImageList = new ArrayList<>();
        getNewFlowViewDetailApiCall();

//        if (!AppConstants.APPROVED_TAILOR_ID.equalsIgnoreCase("0")&&AppConstants.DIRECT_ORDER.equalsIgnoreCase("Direct")){
//            getNewFlowViewDetailApiCall();
//        }else
//        {
//            if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
//
//            }else {
//                getViewDetailApiCall();
//            }
//
//        }


    }

    @OnClick({R.id.customization_three_view_details_btn,R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.customization_three_view_details_btn:
                nextScreen(CustomizationThreeScreen.class,true);
                break;
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }
    }

    public void setHeader(){
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.material_details).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    public void getViewDetailApiCall(){
        if (NetworkUtil.isNetworkAvailable(ViewDetailsScreen.this)){
            APIRequestHandler.getInstance().getViewDetailsApiCall(AppConstants.PATTERN_ID,ViewDetailsScreen.this);
        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ViewDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getViewDetailApiCall();
                }
            });
        }
    }

    public void getNewFlowViewDetailApiCall(){
        if (NetworkUtil.isNetworkAvailable(ViewDetailsScreen.this)){
//            if (AppConstants.APPROVED_TAILOR_ID.equalsIgnoreCase("0")){
//            }else {
//                APIRequestHandler.getInstance().getNewFlowViewDetailsApiCall(AppConstants.APPROVED_TAILOR_ID,AppConstants.PATTERN_ID,ViewDetailsScreen.this);
//
//            }
            APIRequestHandler.getInstance().getNewFlowViewDetailsApiCall(String.valueOf(sharedPreferences.getString("TailorId","")),AppConstants.PATTERN_ID,ViewDetailsScreen.this);

        }else {
            DialogManager.getInstance().showNetworkErrorPopup(ViewDetailsScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getNewFlowViewDetailApiCall();
                }
            });
        }
    }

    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.view_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.view_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof ViewDetailsResponse){
            ViewDetailsResponse mResponse = (ViewDetailsResponse)resObj;

            if (mResponse.getResult().getPatternImg().size() >0){
                for(int i=0; i<mResponse.getResult().getPatternImg().size(); i++){
                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getPatternImg().get(i).getImageName());
                }
                viewPagerGet(mImageList);

            }

            if (mResponse.getResult().getGetpattternById().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                        mViewDetailsPatternNameTxt.setText(AppConstants.PATTERN_NAME.toUpperCase());
                    mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInArabic());
//                    mViewDetailsSeasonalNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).);
                    mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                    mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                    mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic());
                }else {
//                        mViewDetailsPatternNameTxt.setText(AppConstants.PATTERN_NAME.toUpperCase());
                    mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPatternInEnglish());
//                    mViewDetailsSeasonalNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).);
                    mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                    mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                    mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish());
                }
//                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getGetpattternById().get(0).getImage());
//                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Brands/"+mResponse.getResult().getGetpattternById().get(0).getBrandImage());
//                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/PlaceOfIndustry/"+mResponse.getResult().getGetpattternById().get(0).getIndustryImage());
//                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Material/"+mResponse.getResult().getGetpattternById().get(0).getMaterialImage());


            }

            if(mResponse.getResult().getGetColorById().size()>0){
                String ColorNameArabic = "";
                String ColorNameEnglish = "";

                for (int i=0; i<mResponse.getResult().getGetColorById().size(); i++){
                    if (i>0){
                        ColorNameArabic = ColorNameArabic + ", " + mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = ColorNameEnglish + ", " +  mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
                    }else {
                        ColorNameArabic = mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = mResponse.getResult().getGetColorById().get(i).getColorInEnglish();

                    }
//                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Color/"+mResponse.getResult().getGetColorById().get(i).getColorImage());
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mViewDetailsColorNameTxt.setText(ColorNameArabic);
                }else {
                    mViewDetailsColorNameTxt.setText(ColorNameEnglish);

                }


            }

        }

        if (resObj instanceof ViewDetailsNewFlowResponse){
            ViewDetailsNewFlowResponse mResponse = (ViewDetailsNewFlowResponse)resObj;

            if (mResponse.getResult().getPatternImg().size() >0){
                for(int i=0; i<mResponse.getResult().getPatternImg().size(); i++){
                    mImageList.add(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+mResponse.getResult().getPatternImg().get(i).getImageName());
                }
                viewPagerGet(mImageList);

            }

            if (mResponse.getResult().getGetpattternById().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInArabic());
                    mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInArabic());
                    mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInArabic());
                }else {
                    mViewDetailsPlaceIndustryNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getPlaceInEnglish());
                    mViewDetailsBrandsNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getBrandInEnglish());
                    mViewDetailsMaterialTypeNameTxt.setText(mResponse.getResult().getGetpattternById().get(0).getMaterialInEnglish());
                }
            }

            if(mResponse.getResult().getGetColorById().size()>0){
                String ColorNameArabic = "";
                String ColorNameEnglish = "";

                for (int i=0; i<mResponse.getResult().getGetColorById().size(); i++){
                    if (i>0){
                        ColorNameArabic = ColorNameArabic + ", " + mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = ColorNameEnglish + ", " +  mResponse.getResult().getGetColorById().get(i).getColorInEnglish();
                    }else {
                        ColorNameArabic = mResponse.getResult().getGetColorById().get(i).getColorInArabic();
                        ColorNameEnglish = mResponse.getResult().getGetColorById().get(i).getColorInEnglish();

                    }
                }

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mViewDetailsColorNameTxt.setText(ColorNameArabic);
                }else {
                    mViewDetailsColorNameTxt.setText(ColorNameEnglish);

                }


            }

            if (mResponse.getResult().getGetMaterialName().size()>0){
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInArabic());

                }else {
                    mViewDetailsPatternNameTxt.setText(mResponse.getResult().getGetMaterialName().get(0).getFriendlyNameInEnglish());

                }

            }

        }
    }

    public void viewPagerGet(ArrayList<String> image){
        mViewDetailAdapter = new ViewDetailPagerAdapter(this,image);
        mViewDetailsViewpager.setAdapter(mViewDetailAdapter);
        mViewDetailsViewpager.setPageTransformer(true, new ZoomOutPageTransformer());
        mPageIndicator.setViewPager(mViewDetailsViewpager);
        //page change tracker
        mViewDetailsViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trackScreenName("ViewDetail");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);

    }
}
