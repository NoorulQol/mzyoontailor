package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GetCountryAdapter;
import com.qoltech.mzyoontailor.entity.GetCountryEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.CountryCodeResponse;
import com.qoltech.mzyoontailor.modal.LoginOTPResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoontailor.main.MZYOONApplication.getContext;

public class LoginScreen extends BaseActivity {

    private GetCountryAdapter mGetCountryAdapter;

    private ArrayList<GetCountryEntity> mCountryList;

    private Dialog mDialog;

    @BindView(R.id.flag_img)
    public ImageView mFlagImg;

    @BindView(R.id.country_code_txt)
    public TextView mCountryCodeTxt;

    @BindView(R.id.mobile_num_edt_txt)
    EditText mMobileNumEdtTxt;

    private String android_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_login_screen);

        initView();

    }

    public void initView() {
        ButterKnife.bind(this);

//    android_id = Settings.Secure.getString(getContext().getContentResolver(),
//            Settings.Secure.ANDROID_ID);
//    AppConstants.DEVICE_ID = android_id;

        getCountryApiCall();

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.login_continue_btn, R.id.country_code_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_continue_btn:
                if (mCountryCodeTxt.getText().toString().trim().isEmpty()) {
                    mCountryCodeTxt.clearAnimation();
                    mCountryCodeTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mCountryCodeTxt.setError("Please select your country code");
                } else if (mMobileNumEdtTxt.getText().toString().trim().isEmpty()) {
                    mMobileNumEdtTxt.clearAnimation();
                    mMobileNumEdtTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMobileNumEdtTxt.setError("Please give your mobile number");
                } else {
                    loginApiCall();
                }
                break;
            case R.id.country_code_lay:

                alertDismiss(mDialog);
                mDialog = getDialog(LoginScreen.this, R.layout.popup_country_alert);

                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                Window window = mDialog.getWindow();

                if (window != null) {
                    LayoutParams.copyFrom(window.getAttributes());
                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(LayoutParams);
                    window.setGravity(Gravity.CENTER);
                }

                TextView cancelTxt;

                RecyclerView countryRecyclerView;

                /*Init view*/
                cancelTxt = mDialog.findViewById(R.id.country_text_cancel);

                countryRecyclerView = mDialog.findViewById(R.id.country_popup_recycler_view);
                mGetCountryAdapter = new GetCountryAdapter(LoginScreen.this, mCountryList, mDialog);

                countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                countryRecyclerView.setAdapter(mGetCountryAdapter);

                /*Set data*/
                cancelTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });

                alertShowing(mDialog);
                break;
        }

    }

    public void loginApiCall() {

        if (NetworkUtil.isNetworkAvailable(LoginScreen.this)) {
            AppConstants.MOBILE_NUM = mMobileNumEdtTxt.getText().toString().trim();
            AppConstants.COUNTRY_CODE = mCountryCodeTxt.getText().toString().trim();
            APIRequestHandler.getInstance().loginOTPGenerateAPICall(mCountryCodeTxt.getText().toString().trim(), mMobileNumEdtTxt.getText().toString().trim(), AppConstants.DEVICE_ID, LoginScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(LoginScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    loginApiCall();
                }
            });
        }

    }


    public void getCountryApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            APIRequestHandler.getInstance().getAllCountryAPICall(LoginScreen.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(LoginScreen.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCountryApiCall();
                }
            });
        }

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof CountryCodeResponse) {
            CountryCodeResponse mResponse = (CountryCodeResponse) resObj;
            mCountryList = new ArrayList<>();
            mCountryList.addAll(mResponse.getResult());
        }
        if (resObj instanceof LoginOTPResponse) {
            LoginOTPResponse mResponse = (LoginOTPResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                nextScreen(OTPScreen.class, true);
            }
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }


}
