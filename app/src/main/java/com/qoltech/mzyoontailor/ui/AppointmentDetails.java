package com.qoltech.mzyoontailor.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.adapter.GetAppointmentTimeSlotAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.AppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.ApproveMaterialResponse;
import com.qoltech.mzyoontailor.modal.ApproveMeasurementResponse;
import com.qoltech.mzyoontailor.modal.GetMaterialFromDateResponse;
import com.qoltech.mzyoontailor.modal.GetMeasurementFromDateResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.SelectDateFragment;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.utils.ShakeErrorUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.qoltech.mzyoontailor.main.MZYOONApplication.getContext;

public class AppointmentDetails extends BaseActivity {

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    @BindView(R.id.appointment_material_status_txt)
    TextView mAppointmentMaterialStatusTxt;

    @BindView(R.id.appointment_material_type_header_img)
    ImageView mAppointmentMaterialTypeHeaderImg;

    @BindView(R.id.appointment_material_type_header_txt)
    TextView mAppointmentMaterialTypeHeaderTxt;

    @BindView(R.id.appointment_material_type_body_img)
    ImageView mAppointmentMaterialBodyImg;

    @BindView(R.id.material_par_lay)
    LinearLayout mMaterialParLay;

    @BindView(R.id.appointment_measurement_status_txt)
    TextView mAppointmentMeasurementStatusTxt;

    @BindView(R.id.appointment_measurement_type_img)
    ImageView mAppointmentMeasurementTypeImg;

    @BindView(R.id.appointment_measurment_type_header_txt)
    TextView mAppointmentMeasurementTypeHeaderTxt;

    @BindView(R.id.appointment_measurement_type_body_img)
    ImageView mAppointmentMeasurementBodyImg;

    @BindView(R.id.measurement_par_lay)
    LinearLayout mMeasurementParLay;

    @BindView(R.id.appointment_details_par_lay)
    LinearLayout mAppointmentDetailsPayLay;

    @BindView(R.id.material_approve_reject_lay)
    LinearLayout mMaterialApproveRejectLay;

    @BindView(R.id.measurement_approve_reject_lay)
    LinearLayout mMeasurementApproveRejectLay;

    @BindView(R.id.material_save_btn_lay)
    RelativeLayout mMaterialSaveBtnLay;

    @BindView(R.id.measurement_save_btn_lay)
    RelativeLayout mMeasurementSaveBtnLay;

    @BindView(R.id.material_type_time_slot_txt)
    public TextView mMaterialTypeTimeSlotTxt;

    @BindView(R.id.measurement_type_time_slot_txt)
    public TextView mMeasurementTypeTimeSlotTxt;

    @BindView(R.id.appointment_type_fromt_date_txt)
    public TextView mMaterialTypeFromDateTxt;

    @BindView(R.id.appointment_material_type_to_date_txt)
    public TextView mMaterialTypeToDateTxt;

    @BindView(R.id.measurement_type_from_date_txt)
    public TextView mMeasurementTypeFromDateTxt;

    @BindView(R.id.measurement_type_to_date_txt)
    public TextView mMeasurementTypeToDateTxt;

    @BindView(R.id.order_type_txt_lay)
    RelativeLayout mOrderTypeTxtLay;

    @BindView(R.id.order_type_txt)
    TextView mOrderTypeTxt;

    @BindView(R.id.measurement_type_txt__lay)
    RelativeLayout mMeasurementTypeTxtLay;

    @BindView(R.id.measurement_type_txt)
    TextView mMeasurementTypeTxt;

    private UserDetailsEntity mUserDetailsEntityRes;

    Dialog mAppointmentApprove, mAppointmentReject, mAppointmentTimeDialog;

    private GetAppointmentTimeSlotAdapter mGetAppointmentTimeSlotAdater;

    private ArrayList<String> mAppointmentTimeSlotList;

    @BindView(R.id.empty_list_lay)
    RelativeLayout mEmptyListLay;

    @BindView(R.id.empty_list_txt)
    TextView mEmptyListTxt;

    private String mOrderTypeBoolStr = "", mMeasurementTypeBoolStr = "", mMaterialEdit = "",
            mMeasurementEdit = "", mOrderTypeViewBool = "",
            mMeasurementTypeViewBool = "",
            mOrderStr = "",mOrderApprovedStr="",
            mMeasurementStr = ""
            ,mMeasurementApprovedStr = "",mOrderStatusStr = "",
            mMeasurementStatusStr= "",mOrderTypeFromDateStr = "",
            mOrderTypeToDateStr = "",mOrderTimeStr="",mMeasurementFromDateStr = ""
            ,mMeasurementToDateStr="",mMeasurementTimeStr = "";
    SharedPreferences sharedPreferencesTailorName,sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_appointment_details);
        sharedPreferencesTailorName = getSharedPreferences("TailorName", MODE_PRIVATE);
        sharedPreferences= getSharedPreferences("TailorId", MODE_PRIVATE);
        initView();
    }

    public void initView() {

        ButterKnife.bind(this);

        setupUI(mAppointmentDetailsPayLay);

        setHeader();


        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(AppointmentDetails.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        addTimeSlotList();
        getAppointmentMaterialApiCall();

        getAppointmentMeasurementApiCall();

        getLanguage();

        AppConstants.DATE_FOR_RESTRICT_MATERIAL = "";
        AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = "";
    }

    public void addTimeSlotList() {
        mAppointmentTimeSlotList = new ArrayList<>();
        mAppointmentTimeSlotList.add("01:00 AM TO 02:00 AM");
        mAppointmentTimeSlotList.add("02:00 AM TO 03:00 AM");
        mAppointmentTimeSlotList.add("03:00 AM TO 04:00 AM");
        mAppointmentTimeSlotList.add("04:00 AM TO 05:00 AM");
        mAppointmentTimeSlotList.add("05:00 AM TO 06:00 AM");
        mAppointmentTimeSlotList.add("06:00 AM TO 07:00 AM");
        mAppointmentTimeSlotList.add("07:00 AM TO 08:00 AM");
        mAppointmentTimeSlotList.add("08:00 AM TO 09:00 AM");
        mAppointmentTimeSlotList.add("09:00 AM TO 10:00 AM");
        mAppointmentTimeSlotList.add("10:00 AM TO 11:00 AM");
        mAppointmentTimeSlotList.add("11:00 AM TO 12:00 AM");
        mAppointmentTimeSlotList.add("12:00 AM TO 01:00 PM");
        mAppointmentTimeSlotList.add("01:00 PM TO 02:00 PM");
        mAppointmentTimeSlotList.add("02:00 PM TO 03:00 PM");
        mAppointmentTimeSlotList.add("03:00 PM TO 04:00 PM");
        mAppointmentTimeSlotList.add("04:00 PM TO 05:00 PM");
        mAppointmentTimeSlotList.add("05:00 PM TO 06:00 PM");
        mAppointmentTimeSlotList.add("06:00 PM TO 07:00 PM");
        mAppointmentTimeSlotList.add("07:00 PM TO 08:00 PM");
        mAppointmentTimeSlotList.add("08:00 PM TO 09:00 PM");
        mAppointmentTimeSlotList.add("09:00 PM TO 10:00 PM");
        mAppointmentTimeSlotList.add("10:00 PM TO 11:00 PM");
        mAppointmentTimeSlotList.add("11:00 PM TO 12:00 PM");

    }

    @OnClick({R.id.header_left_side_img, R.id.appointment_material_approve_lay, R.id.appointment_material_reject_lay, R.id.appointment_material_save_btn, R.id.appointment_measurement_approve_lay, R.id.appointment_measurement_reject_lay, R.id.appointment_measurement_save_btn, R.id.material_type_time_slot_par_lay, R.id.measurement_type_time_slot_lay, R.id.material_type_calender_from_par_lay, R.id.material_type_calender_to_par_lay, R.id.measurement_calender_from_date_par_lay, R.id.measurement_calender_to_date_par_lay, R.id.order_type_txt_lay, R.id.measurement_type_txt__lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
            case R.id.appointment_material_approve_lay:
                if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")||mMaterialTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")||mMaterialTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, "Appointment not created to approve", new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    approveMaterialApiCall(AppConstants.APPOINTMENT_MATERIAL_ID, "1", "Approve");

                }

                break;
            case R.id.appointment_material_reject_lay:
                if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")||mMaterialTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")||mMaterialTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, "Appointment not created to reject", new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    alertDismiss(mAppointmentReject);
                    mAppointmentReject = getDialog(AppointmentDetails.this, R.layout.pop_up_appointment);
                    WindowManager.LayoutParams LayoutParam = new WindowManager.LayoutParams();
                    Window windows = mAppointmentReject.getWindow();
                    if (windows != null) {
                        LayoutParam.copyFrom(windows.getAttributes());
                        LayoutParam.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParam.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        windows.setAttributes(LayoutParam);
                        windows.setGravity(Gravity.CENTER);
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }
                    RelativeLayout cancelLays, saveLay;
                    final EditText mEditText;
                    cancelLays = mAppointmentReject.findViewById(R.id.appointment_popup_cancel_lay);
                    saveLay = mAppointmentReject.findViewById(R.id.appointment_popup_save_lay);
                    mEditText = mAppointmentReject.findViewById(R.id.appointment_popup_edt_txt);
                    cancelLays.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReject.dismiss();
                        }
                    });

                    saveLay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mEditText.getText().toString().equalsIgnoreCase("")) {
                                mEditText.clearAnimation();
                                mEditText.setAnimation(ShakeErrorUtils.shakeError());
                                mEditText.setError("Please give reason");
                            } else {
                                approveMaterialApiCall(AppConstants.APPOINTMENT_MATERIAL_ID, "2", mEditText.getText().toString());
                            }

                            mAppointmentReject.dismiss();
                        }
                    });

                    alertShowing(mAppointmentReject);
                }
                break;
            case R.id.appointment_material_save_btn:
                if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")){
                    mMaterialTypeFromDateTxt.clearAnimation();
                    mMaterialTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMaterialTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                }else if (mMaterialTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")){
                    mMaterialTypeToDateTxt.clearAnimation();
                    mMaterialTypeToDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMaterialTypeToDateTxt.setError(getResources().getString(R.string.please_give_date));
                }else if (mMaterialTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    mMaterialTypeTimeSlotTxt.clearAnimation();
                    mMaterialTypeTimeSlotTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMaterialTypeTimeSlotTxt.setError(getResources().getString(R.string.please_give_time));
                }
                else {
                    if (mOrderStatusStr.equalsIgnoreCase("Rejected")){
                        if (mOrderTypeFromDateStr.equalsIgnoreCase(mMaterialTypeFromDateTxt.getText().toString().replace("/"," "))||
                                mOrderTypeToDateStr.equalsIgnoreCase(mMaterialTypeToDateTxt.getText().toString().replace("/"," "))||
                                mOrderTimeStr.equalsIgnoreCase(mMaterialTypeTimeSlotTxt.getText().toString())){
                            DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.appointment_date_and_time_already_rejected), new InterfaceBtnCallBack() {
                                @Override
                                public void onPositiveClick() {

                                }
                            });
                        }else {
                            insertAppointmentMaterialApiCall();
                        }
                    }else {
                        insertAppointmentMaterialApiCall();
                    }

                }
                break;
            case R.id.appointment_measurement_approve_lay:
                if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")||mMeasurementTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")||mMeasurementTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, "Appointment not created to approve", new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    approveMeasurementApiCall(AppConstants.APPOINTMENT_MEASUREMENT_ID, "1", "Approve");
                }
                break;

            case R.id.appointment_measurement_reject_lay:
                if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")||mMeasurementTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")||mMeasurementTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, "Appointment not created to reject", new InterfaceBtnCallBack() {
                        @Override
                        public void onPositiveClick() {

                        }
                    });
                }else {
                    alertDismiss(mAppointmentReject);
                    mAppointmentReject = getDialog(AppointmentDetails.this, R.layout.pop_up_appointment);
                    WindowManager.LayoutParams LayoutParamRe = new WindowManager.LayoutParams();
                    Window windowsRe = mAppointmentReject.getWindow();
                    if (windowsRe != null) {
                        LayoutParamRe.copyFrom(windowsRe.getAttributes());
                        LayoutParamRe.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParamRe.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        windowsRe.setAttributes(LayoutParamRe);
                        windowsRe.setGravity(Gravity.CENTER);
                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                    } else {
                        ViewCompat.setLayoutDirection(mAppointmentReject.findViewById(R.id.pop_up_appointment_edt_txt_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }
                    RelativeLayout cancelLaysRe, saveLayRe;
                    final EditText mEditTextRe;
                    cancelLaysRe = mAppointmentReject.findViewById(R.id.appointment_popup_cancel_lay);
                    saveLayRe = mAppointmentReject.findViewById(R.id.appointment_popup_save_lay);
                    mEditTextRe = mAppointmentReject.findViewById(R.id.appointment_popup_edt_txt);

                    cancelLaysRe.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mAppointmentReject.dismiss();
                        }
                    });

                    saveLayRe.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mEditTextRe.getText().toString().equalsIgnoreCase("")) {
                                mEditTextRe.clearAnimation();
                                mEditTextRe.setAnimation(ShakeErrorUtils.shakeError());
                                mEditTextRe.setError("Please give reason");
                            } else {
                                approveMeasurementApiCall(AppConstants.APPOINTMENT_MEASUREMENT_ID, "2", mEditTextRe.getText().toString());
                            }
                            mAppointmentReject.dismiss();
                        }
                    });

                    alertShowing(mAppointmentReject);
                }
                break;
            case R.id.appointment_measurement_save_btn:
                if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")){
                    mMeasurementTypeFromDateTxt.clearAnimation();
                    mMeasurementTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMeasurementTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                }else if (mMeasurementTypeToDateTxt.getText().toString().equalsIgnoreCase("Date")){
                    mMeasurementTypeToDateTxt.clearAnimation();
                    mMeasurementTypeToDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMeasurementTypeToDateTxt.setError(getResources().getString(R.string.please_give_date));
                }else if (mMeasurementTypeTimeSlotTxt.getText().toString().equalsIgnoreCase("Time")){
                    mMeasurementTypeTimeSlotTxt.clearAnimation();
                    mMeasurementTypeTimeSlotTxt.setAnimation(ShakeErrorUtils.shakeError());
                    mMeasurementTypeTimeSlotTxt.setError(getResources().getString(R.string.please_give_time));
                }
                else {
                    if (mMeasurementStatusStr.equalsIgnoreCase("Rejected")){
                        if (mMeasurementFromDateStr.equalsIgnoreCase(mMeasurementTypeFromDateTxt.getText().toString().replace("/"," "))||mMeasurementToDateStr.equalsIgnoreCase(mMeasurementTypeToDateTxt.getText().toString().replace("/"," ")) || mMeasurementTimeStr.equalsIgnoreCase(mMeasurementTypeTimeSlotTxt.getText().toString())){
                            DialogManager.getInstance().showAlertPopup(AppointmentDetails.this, getResources().getString(R.string.appointment_date_and_time_already_rejected), new InterfaceBtnCallBack() {
                                @Override
                                public void onPositiveClick() {

                                }
                            });
                        }else {
                            insertAppointmentMeasurementApiCall();
                        }
                    }else {
                        insertAppointmentMeasurementApiCall();
                    }
                }
                break;
            case R.id.material_type_time_slot_par_lay:
                if (!mMaterialEdit.equalsIgnoreCase("")) {
                    if (mAppointmentTimeSlotList.size() > 0) {
                        alertDismiss(mAppointmentTimeDialog);
                        mAppointmentTimeDialog = getDialog(AppointmentDetails.this, R.layout.popup_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mAppointmentTimeDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                        } else {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                        }

                        TextView cancelTxt, headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mAppointmentTimeDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mAppointmentTimeDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mAppointmentTimeDialog.findViewById(R.id.country_popup_recycler_view);
                        mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(AppointmentDetails.this, mAppointmentTimeSlotList, mAppointmentTimeDialog, "ORDER_TYPE");

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                        /*Set data*/
                        headerTxt.setText(getResources().getString(R.string.choose_your_time_slot));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAppointmentTimeDialog.dismiss();
                            }
                        });

                        alertShowing(mAppointmentTimeDialog);
                    } else {
                        Toast.makeText(AppointmentDetails.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.measurement_type_time_slot_lay:
                if (!mMeasurementEdit.equalsIgnoreCase("")) {
                    if (mAppointmentTimeSlotList.size() > 0) {
                        alertDismiss(mAppointmentTimeDialog);
                        mAppointmentTimeDialog = getDialog(AppointmentDetails.this, R.layout.popup_country_alert);

                        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                        Window window = mAppointmentTimeDialog.getWindow();

                        if (window != null) {
                            LayoutParams.copyFrom(window.getAttributes());
                            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(LayoutParams);
                            window.setGravity(Gravity.CENTER);
                        }

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

                        } else {
                            ViewCompat.setLayoutDirection(mAppointmentTimeDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                        }

                        TextView cancelTxt, headerTxt;

                        RecyclerView countryRecyclerView;

                        /*Init view*/
                        cancelTxt = mAppointmentTimeDialog.findViewById(R.id.country_text_cancel);
                        headerTxt = mAppointmentTimeDialog.findViewById(R.id.country_header_text_cancel);

                        countryRecyclerView = mAppointmentTimeDialog.findViewById(R.id.country_popup_recycler_view);
                        mGetAppointmentTimeSlotAdater = new GetAppointmentTimeSlotAdapter(AppointmentDetails.this, mAppointmentTimeSlotList, mAppointmentTimeDialog, "MEASUREMENT_TYPE");

                        countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        countryRecyclerView.setAdapter(mGetAppointmentTimeSlotAdater);

                        /*Set data*/
                        headerTxt.setText(getResources().getString(R.string.choose_your_time_slot));
                        cancelTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mAppointmentTimeDialog.dismiss();
                            }
                        });

                        alertShowing(mAppointmentTimeDialog);
                    } else {
                        Toast.makeText(AppointmentDetails.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.material_type_calender_from_par_lay:
                if(!mMaterialEdit.equalsIgnoreCase("")) {
                    AppConstants.DATE_PICKER_COND = "APPOINTMENT_MATERIAL_FROM";
                    mMaterialTypeToDateTxt.setText("Date");
                    DialogFragment newFragment = new SelectDateFragment();
                    newFragment.show(getSupportFragmentManager(), "DatePicker");
                }
                break;
            case R.id.material_type_calender_to_par_lay:
                if(!mMaterialEdit.equalsIgnoreCase("")) {
                    if (mMaterialTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")) {
                        mMaterialTypeFromDateTxt.clearAnimation();
                        mMaterialTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                        mMaterialTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    } else {
                        AppConstants.DATE_PICKER_COND = "APPOINTMENT_MATERIAL_TO";
                        AppConstants.DATE_FOR_RESTRICT_MATERIAL = mMaterialTypeFromDateTxt.getText().toString();
                        DialogFragment newFragments = new SelectDateFragment();
                        newFragments.show(getSupportFragmentManager(), "DatePicker");
                    }
                }
                break;
            case R.id.measurement_calender_from_date_par_lay:
                if(!mMeasurementEdit.equalsIgnoreCase("")) {
                    AppConstants.DATE_PICKER_COND = "APPOINTMENT_MEASUREMENT_FROM";
                    mMeasurementTypeToDateTxt.setText("Date");
                    DialogFragment newFragmentm = new SelectDateFragment();
                    newFragmentm.show(getSupportFragmentManager(), "DatePicker");
                }
                break;
            case R.id.measurement_calender_to_date_par_lay:
                if(!mMeasurementEdit.equalsIgnoreCase("")) {
                    if (mMeasurementTypeFromDateTxt.getText().toString().equalsIgnoreCase("Date")){
                        mMeasurementTypeFromDateTxt.clearAnimation();
                        mMeasurementTypeFromDateTxt.setError(getResources().getString(R.string.please_give_date));
                        mMeasurementTypeFromDateTxt.setAnimation(ShakeErrorUtils.shakeError());
                    }else {
                        AppConstants.DATE_PICKER_COND = "APPOINTMENT_MEASUREMENT_TO";
                        AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = mMeasurementTypeFromDateTxt.getText().toString();
                        DialogFragment newFragmentms = new SelectDateFragment();
                        newFragmentms.show(getSupportFragmentManager(), "DatePicker");
                    }

                }
                break;
            case R.id.order_type_txt_lay:
                mOrderTypeTxtLay.setBackgroundResource(R.color.slider_blue);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMeasurementTypeTxtLay.setBackgroundResource(R.color.grey_clr);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.black));
                if (mOrderTypeViewBool.equalsIgnoreCase("true")) {
                    mMaterialParLay.setVisibility(View.VISIBLE);
                    mEmptyListLay.setVisibility(View.GONE);
                } else {
                    mMaterialParLay.setVisibility(View.GONE);
                    mEmptyListLay.setVisibility(View.VISIBLE);

                }
                mMeasurementParLay.setVisibility(View.GONE);
                break;

            case R.id.measurement_type_txt__lay:
                mOrderTypeTxtLay.setBackgroundResource(R.color.grey_clr);
                mOrderTypeTxt.setTextColor(getResources().getColor(R.color.black));
                mMeasurementTypeTxtLay.setBackgroundResource(R.color.slider_blue);
                mMeasurementTypeTxt.setTextColor(getResources().getColor(R.color.white));
                mMaterialParLay.setVisibility(View.GONE);
                if (mMeasurementTypeViewBool.equalsIgnoreCase("true")) {
                    mMeasurementParLay.setVisibility(View.VISIBLE);
                    mEmptyListLay.setVisibility(View.GONE);
                } else {
                    mMeasurementParLay.setVisibility(View.GONE);
                    mEmptyListLay.setVisibility(View.VISIBLE);

                }
                break;
        }

    }

    public void approveMaterialApiCall(final String AppointmentId, final String IsApproved, final String Reason) {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {
            APIRequestHandler.getInstance().approveMaterialApiCall(AppointmentId, IsApproved, Reason, AppointmentDetails.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    approveMaterialApiCall(AppointmentId, IsApproved, Reason);
                }
            });
        }
    }

    public void approveMeasurementApiCall(final String AppointmentId, final String IsApproved, final String Reason) {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {
            APIRequestHandler.getInstance().approveMeasurementApiCall(AppointmentId, IsApproved, Reason, AppointmentDetails.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    approveMeasurementApiCall(AppointmentId, IsApproved, Reason);
                }
            });
        }
    }

    public void setHeader() {
        if (!AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")){
            mHeaderLeftBackBtn.setVisibility(View.VISIBLE);

        }else {
            mHeaderLeftBackBtn.setVisibility(View.INVISIBLE);

        }

        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.appointment).toUpperCase());
        mRightSideImg.setVisibility(View.VISIBLE);

        mMaterialParLay.setVisibility(View.GONE);
        mMeasurementParLay.setVisibility(View.GONE);

        mEmptyListTxt.setText(getResources().getString(R.string.no_result_found));

    }

    public void insertAppointmentMaterialApiCall() {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {

            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().insertAppointmentMaterialApi(AppConstants.APPOINTMENT_LIST_ID, "1",
                        mMaterialTypeTimeSlotTxt.getText().toString(),
                        mMaterialTypeFromDateTxt.getText().toString(),
                        mMaterialTypeToDateTxt.getText().toString(),sharedPreferences.getString("TailorId","") ,"Tailor",
                        sharedPreferencesTailorName.getString("TailorName", ""), AppointmentDetails.this);
            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().insertAppointmentMaterialApi(AppConstants.REQUEST_LIST_ID, "1",
                        mMaterialTypeTimeSlotTxt.getText().toString(),
                        mMaterialTypeFromDateTxt.getText().toString(),
                        mMaterialTypeToDateTxt.getText().toString(),sharedPreferences.getString("TailorId","") ,"Tailor",
                        sharedPreferencesTailorName.getString("TailorName", ""), AppointmentDetails.this);
            }

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertAppointmentMaterialApiCall();
                }
            });
        }
    }

    public void insertAppointmentMeasurementApiCall() {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {

            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {

                APIRequestHandler.getInstance().insertAppointmentMeasurementApi(AppConstants.APPOINTMENT_LIST_ID, "2",
                        mMeasurementTypeTimeSlotTxt.getText().toString(),
                        mMeasurementTypeFromDateTxt.getText().toString(),
                        mMeasurementTypeToDateTxt.getText().toString(),sharedPreferences.getString("TailorId","") ,"Tailor",
                        sharedPreferencesTailorName.getString("TailorName", ""), AppointmentDetails.this);
            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().insertAppointmentMeasurementApi(AppConstants.REQUEST_LIST_ID, "2",
                        mMeasurementTypeTimeSlotTxt.getText().toString(),
                        mMeasurementTypeFromDateTxt.getText().toString(),
                        mMeasurementTypeToDateTxt.getText().toString(),sharedPreferences.getString("TailorId","") ,"Tailor",
                        sharedPreferencesTailorName.getString("TailorName", ""), AppointmentDetails.this);

            }


        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    insertAppointmentMeasurementApiCall();
                }
            });
        }
    }

    public void getAppointmentMaterialApiCall() {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().getAppointmentMaterialApi(AppConstants.APPOINTMENT_LIST_ID, AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentMaterialApi(AppConstants.REQUEST_LIST_ID, AppointmentDetails.this);

            }

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMaterialApiCall();
                }
            });
        }
    }

    public void getAppointmentMeasurementApiCall() {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().getAppointmentMeasurementApi(AppConstants.APPOINTMENT_LIST_ID, AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getAppointmentMeasurementApi(AppConstants.REQUEST_LIST_ID, AppointmentDetails.this);

            }

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMeasurementApiCall();
                }
            });
        }
    }

    public void getAppointmentMaterialDateApiCall(final String TypeId, final String Type) {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().getMaterialDateApiCall(AppConstants.APPOINTMENT_LIST_ID,TypeId,Type ,AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getMaterialDateApiCall(AppConstants.REQUEST_LIST_ID,TypeId,Type, AppointmentDetails.this);

            }

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMaterialDateApiCall(TypeId ,Type);
                }
            });
        }
    }

    public void getAppointmentMeasurementDateApiCall(final String TypeId, final String Type) {
        if (NetworkUtil.isNetworkAvailable(AppointmentDetails.this)) {
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")) {
                APIRequestHandler.getInstance().getMeasurementDateApiCall(AppConstants.APPOINTMENT_LIST_ID,TypeId,Type ,AppointmentDetails.this);

            }
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                APIRequestHandler.getInstance().getMeasurementDateApiCall(AppConstants.REQUEST_LIST_ID, TypeId,Type,AppointmentDetails.this);

            }

        } else {
            DialogManager.getInstance().showNetworkErrorPopup(AppointmentDetails.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMeasurementDateApiCall(TypeId,Type);
                }
            });
        }
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AppointmentMaterialResponse) {
            AppointmentMaterialResponse mResponse = (AppointmentMaterialResponse) resObj;
            if (mResponse.getResult().size() > 0) {
//                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentId() != 0){
                AppConstants.APPOINTMENT_MATERIAL_ID = String.valueOf(mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentId());
                mOrderStr = mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish();
                mOrderStatusStr = mResponse.getResult().get(mResponse.getResult().size()-1).getStatus();

                mOrderApprovedStr =  mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus();
//                }
                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")) {
                    getAppointmentMaterialDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                        mMaterialParLay.setVisibility(View.GONE);
                        mOrderTypeViewBool = "false";
                        mOrderTypeBoolStr = "true";
                    } else {
                        mMaterialParLay.setVisibility(View.VISIBLE);
                        mMaterialApproveRejectLay.setVisibility(View.VISIBLE);
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";
                        mOrderTypeViewBool = "true";

                        if (mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus().equalsIgnoreCase("Approved")) {
                            mMaterialApproveRejectLay.setVisibility(View.GONE);
                        }
                    }

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")) {
                    mMaterialParLay.setVisibility(View.GONE);
                    mOrderTypeBoolStr = "true";
                    mOrderTypeViewBool = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")) {
                    mMaterialParLay.setVisibility(View.VISIBLE);
                    mMaterialApproveRejectLay.setVisibility(View.GONE);
                    mMaterialSaveBtnLay.setVisibility(View.VISIBLE);
                    mMaterialEdit = "Edit";
                    mOrderTypeViewBool = "true";
                    getAppointmentMaterialDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")
                            && mResponse.getResult().get(mResponse.getResult().size() - 1)
                            .getStatus().equalsIgnoreCase("Approved")) {
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";

                    }
                } else if (mResponse.getResult().
                        get(mResponse.getResult().size() - 1).getHeaderInEnglish().
                        equalsIgnoreCase("Tailor Come To Your Place")) {
                    getAppointmentMaterialDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                        mMaterialParLay.setVisibility(View.GONE);
                        mOrderTypeViewBool = "false";
                        mOrderTypeBoolStr = "true";

                    } else {
                        mMaterialParLay.setVisibility(View.VISIBLE);
                        mOrderTypeViewBool = "true";
                        mMaterialApproveRejectLay.setVisibility(View.VISIBLE);
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";

                        if (mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus().equalsIgnoreCase("Approved")) {
                            mMaterialApproveRejectLay.setVisibility(View.GONE);
                        }
                    }


                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).
                        getHeaderInEnglish().equalsIgnoreCase("Manually")) {
                    mMaterialParLay.setVisibility(View.GONE);
                    mOrderTypeViewBool = "false";
                    mOrderTypeBoolStr = "true";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish()
                        .equalsIgnoreCase("Go to Tailor Shop")) {
                    mMaterialParLay.setVisibility(View.VISIBLE);
                    mOrderTypeViewBool = "true";
                    mMaterialApproveRejectLay.setVisibility(View.GONE);
                    mMaterialSaveBtnLay.setVisibility(View.VISIBLE);
                    mMaterialEdit = "Edit";
                    getAppointmentMaterialDateApiCall(sharedPreferences.getString("",""),"Tailor");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")
                            && mResponse.getResult().get(mResponse.getResult().size() - 1)
                            .getStatus().equalsIgnoreCase("Approved")) {
                        mMaterialSaveBtnLay.setVisibility(View.GONE);
                        mMaterialEdit = "";

                    }

                }

                mAppointmentMaterialStatusTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus());

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mAppointmentMaterialTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInArabic());
                } else {
                    mAppointmentMaterialTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish());
                }
                try {
                    Glide.with(AppointmentDetails.this)
                            .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mAppointmentMaterialTypeHeaderImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

                try {
                    Glide.with(AppointmentDetails.this)
                            .load(AppConstants.IMAGE_BASE_URL + "Images/OrderType/" + mResponse.getResult().get(mResponse.getResult().size() - 1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mAppointmentMaterialBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }

        }
        if (resObj instanceof AppointmentMeasurementResponse) {
            AppointmentMeasurementResponse mResponse = (AppointmentMeasurementResponse) resObj;
            if (mResponse.getResult().size() > 0) {
//                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentId() != 0){
                AppConstants.APPOINTMENT_MEASUREMENT_ID = String.valueOf(mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentId());
                mMeasurementStr = mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish();
                mMeasurementApprovedStr =  mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus();
                mMeasurementStatusStr = mResponse.getResult().get(mResponse.getResult().size()-1).getStatus();

//                }
                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")) {
                    getAppointmentMeasurementDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                        mMeasurementParLay.setVisibility(View.GONE);
                        mMeasurementTypeViewBool = "false";
                        mMeasurementTypeBoolStr = "true";
                    } else {
                        mMeasurementParLay.setVisibility(View.VISIBLE);
                        mMeasurementTypeViewBool = "true";
                        mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);
                        mMeasurementSaveBtnLay.setVisibility(View.GONE);
                        mMeasurementEdit = "";
                        if (mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus().equalsIgnoreCase("Approved")) {
                            mMeasurementApproveRejectLay.setVisibility(View.GONE);
                        }
                    }
                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Manually")) {
                    mMeasurementParLay.setVisibility(View.GONE);
                    mMeasurementTypeViewBool = "false";
                    mMeasurementTypeBoolStr = "true";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")) {
                    mMeasurementParLay.setVisibility(View.VISIBLE);
                    mMeasurementTypeViewBool = "true";
                    mMeasurementSaveBtnLay.setVisibility(View.VISIBLE);
                    mMeasurementApproveRejectLay.setVisibility(View.GONE);
                    mMeasurementEdit = "Edit";
                    getAppointmentMeasurementDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment") && mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus().equalsIgnoreCase("Approved")) {
                        mMeasurementSaveBtnLay.setVisibility(View.GONE);
                        mMeasurementEdit = "";

                    }

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Own Material-Courier the Material")) {
                    getAppointmentMeasurementDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                        mMeasurementParLay.setVisibility(View.GONE);
                        mMeasurementTypeViewBool = "false";
                        mMeasurementTypeBoolStr = "true";

                    } else {
                        mMeasurementParLay.setVisibility(View.VISIBLE);
                        mMeasurementTypeViewBool = "true";
                        mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);
                        mMeasurementSaveBtnLay.setVisibility(View.GONE);
                        mMeasurementEdit = "";
                        if (mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus().equalsIgnoreCase("Approved")) {
                            mMeasurementApproveRejectLay.setVisibility(View.GONE);
                        }
                    }

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Companies-Material")) {
                    mMeasurementParLay.setVisibility(View.GONE);
                    mMeasurementTypeViewBool = "false";
                    mMeasurementTypeBoolStr = "true";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")) {
                    mMeasurementParLay.setVisibility(View.VISIBLE);
                    mMeasurementTypeViewBool = "true";
                    mMeasurementSaveBtnLay.setVisibility(View.VISIBLE);
                    mMeasurementApproveRejectLay.setVisibility(View.GONE);
                    mMeasurementEdit = "Edit";
                    getAppointmentMeasurementDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
                    if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment") && mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus().equalsIgnoreCase("Approved")) {
                        mMeasurementSaveBtnLay.setVisibility(View.GONE);
                        mMeasurementEdit = "";

                    }
                }

                mAppointmentMeasurementStatusTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getStatus());


                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    mAppointmentMeasurementTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInArabic());
                } else {
                    mAppointmentMeasurementTypeHeaderTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish());
                }
//                try {
//                    Glide.with(AppointmentDetails.this)
//                            .load(AppConstants.IMAGE_BASE_URL+"Images/Measurement1/"+mResponse.getResult().get(mResponse.getResult().size()-1).getHeaderImage())
//                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                            .into(mAppointmentMaterialTypeHeaderImg);
//
//                } catch (Exception ex) {
//                    Log.e(AppConstants.TAG, ex.getMessage());
//                }


                try {
                    Glide.with(AppointmentDetails.this)
                            .load(AppConstants.IMAGE_BASE_URL + "Images/Measurement1/" + mResponse.getResult().get(mResponse.getResult().size() - 1).getBodyImage())
                            .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                            .into(mAppointmentMeasurementBodyImg);

                } catch (Exception ex) {
                    Log.e(AppConstants.TAG, ex.getMessage());
                }

            }
        }
        if (resObj instanceof InsertAppointmentMaterialResponse) {
            InsertAppointmentMaterialResponse mResponse = (InsertAppointmentMaterialResponse) resObj;
//            Toast.makeText(AppointmentDetails.this, mResponse.getResult(), Toast.LENGTH_SHORT).show();
            Toast.makeText(AppointmentDetails.this, "Appointment Created", Toast.LENGTH_SHORT).show();

            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                mOrderTypeBoolStr = "true";
                mMaterialSaveBtnLay.setVisibility(View.GONE);

//                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                if (mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                    nextScreen(HomeActivity.class, true);
                }
//                }
            }
        }

        if (resObj instanceof InsertAppointmentMeasurementResponse) {
            InsertAppointmentMeasurementResponse mResponse = (InsertAppointmentMeasurementResponse) resObj;
//            Toast.makeText(AppointmentDetails.this, mResponse.getResult(), Toast.LENGTH_SHORT).show();
            Toast.makeText(AppointmentDetails.this, "Appointment Created", Toast.LENGTH_SHORT).show();

//            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")){

//
//            }
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                mMeasurementTypeBoolStr = "true";
                mMeasurementSaveBtnLay.setVisibility(View.GONE);

//                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                if (mOrderTypeBoolStr.equalsIgnoreCase("true")) {
                    nextScreen(HomeActivity.class, true);
//                    }
                }
            }
        }
        if (resObj instanceof ApproveMeasurementResponse) {
            ApproveMeasurementResponse mResponse = (ApproveMeasurementResponse) resObj;
//            Toast.makeText(AppointmentDetails.this, mResponse.getResult(), Toast.LENGTH_SHORT).show();
            Toast.makeText(AppointmentDetails.this, "Appointment Created", Toast.LENGTH_SHORT).show();

            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                mMeasurementTypeBoolStr = "true";
                mMeasurementApproveRejectLay.setVisibility(View.GONE);

//                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                if (mOrderTypeBoolStr.equalsIgnoreCase("true")) {
                    nextScreen(HomeActivity.class, true);
                }
//                }
            }
        }
        if (resObj instanceof ApproveMaterialResponse) {
            ApproveMaterialResponse mResponse = (ApproveMaterialResponse) resObj;
//            Toast.makeText(AppointmentDetails.this, mResponse.getResult(), Toast.LENGTH_SHORT).show();
            Toast.makeText(AppointmentDetails.this, "Appointment Created", Toast.LENGTH_SHORT).show();

            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                mOrderTypeBoolStr = "true";
                mMaterialApproveRejectLay.setVisibility(View.GONE);

//                if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                if (mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                    nextScreen(HomeActivity.class, true);
                }
//                }
            }
        }
        if (resObj instanceof GetMaterialFromDateResponse) {
            GetMaterialFromDateResponse mResponse = (GetMaterialFromDateResponse) resObj;
            if (mResponse.getResult().size() > 0) {
                mMaterialTypeFromDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getFromDt().replace("T00:00:00", ""));
                AppConstants.DATE_FOR_RESTRICT_MATERIAL = mResponse.getResult().get(mResponse.getResult().size() - 1).getFromDt();
                mOrderTypeFromDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mOrderTypeToDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getToDt();

                mMaterialTypeToDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getToDt().replace("T00:00:00", ""));
                mMaterialTypeTimeSlotTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentTime());
//
//
//                getMaterialView();
            }
        }
        if (resObj instanceof GetMeasurementFromDateResponse) {
            GetMeasurementFromDateResponse mResponse = (GetMeasurementFromDateResponse) resObj;
            if (mResponse.getResult().size() > 0) {
                mMeasurementTypeFromDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getFromDt().replace("T00:00:00", ""));
                mMeasurementFromDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getFromDt();
                mMeasurementToDateStr = mResponse.getResult().get(mResponse.getResult().size()-1).getToDt();

                AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = mResponse.getResult().get(mResponse.getResult().size() - 1).getFromDt();
                mMeasurementTypeToDateTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getToDt().replace("T00:00:00", ""));
                mMeasurementTypeTimeSlotTxt.setText(mResponse.getResult().get(mResponse.getResult().size() - 1).getAppointmentTime());
//                getMeasurementView();
            }
        }
        if (mOrderTypeViewBool.equalsIgnoreCase("true")) {
            mMaterialParLay.setVisibility(View.VISIBLE);
            mEmptyListLay.setVisibility(View.GONE);
        } else {
            mMaterialParLay.setVisibility(View.GONE);
            mEmptyListLay.setVisibility(View.VISIBLE);

        }
        mMeasurementParLay.setVisibility(View.GONE);

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_details_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_details_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void getMaterialView(){
        if (mOrderStr.equalsIgnoreCase("Own Material-Courier the Material")) {
            getAppointmentMaterialDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                mMaterialParLay.setVisibility(View.GONE);
                mOrderTypeViewBool = "false";

            } else {
                mMaterialParLay.setVisibility(View.VISIBLE);
                mMaterialApproveRejectLay.setVisibility(View.VISIBLE);
                mMaterialSaveBtnLay.setVisibility(View.GONE);
                mMaterialEdit = "";
                mOrderTypeViewBool = "true";

                if (mOrderApprovedStr.equalsIgnoreCase("Approved")) {
                    mMaterialApproveRejectLay.setVisibility(View.GONE);
                }
            }

        } else if (mOrderStr.equalsIgnoreCase("Companies-Material")) {
            mMaterialParLay.setVisibility(View.GONE);
            mOrderTypeBoolStr = "true";
            mOrderTypeViewBool = "false";

        } else if (mOrderStr.equalsIgnoreCase("Own Material-Direct Delivery")) {
            mMaterialParLay.setVisibility(View.VISIBLE);
            mMaterialApproveRejectLay.setVisibility(View.GONE);
            mMaterialSaveBtnLay.setVisibility(View.VISIBLE);
            mMaterialEdit = "Edit";
            mOrderTypeViewBool = "true";
            getAppointmentMaterialDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")
                    && mOrderApprovedStr.equalsIgnoreCase("Approved")) {
                mMaterialSaveBtnLay.setVisibility(View.GONE);
                mMaterialEdit = "";

            }
        } else if (mOrderStr.
                equalsIgnoreCase("Tailor Come To Your Place")) {
            getAppointmentMaterialDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                mMaterialParLay.setVisibility(View.GONE);
                mOrderTypeViewBool = "false";


            } else {
                mMaterialParLay.setVisibility(View.VISIBLE);
                mOrderTypeViewBool = "true";
                mMaterialApproveRejectLay.setVisibility(View.VISIBLE);
                mMaterialSaveBtnLay.setVisibility(View.GONE);
                mMaterialEdit = "";

                if (mOrderApprovedStr.equalsIgnoreCase("Approved")) {
                    mMaterialApproveRejectLay.setVisibility(View.GONE);
                }
            }


        } else if (mOrderStr.equalsIgnoreCase("Manually")) {
            mMaterialParLay.setVisibility(View.GONE);
            mOrderTypeViewBool = "false";
            mOrderTypeBoolStr = "true";

        } else if (mOrderStr
                .equalsIgnoreCase("Go to Tailor Shop")) {
            mMaterialParLay.setVisibility(View.VISIBLE);
            mOrderTypeViewBool = "true";
            mMaterialApproveRejectLay.setVisibility(View.GONE);
            mMaterialSaveBtnLay.setVisibility(View.VISIBLE);
            mMaterialEdit = "Edit";
            getAppointmentMaterialDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment")
                    &&mOrderApprovedStr.equalsIgnoreCase("Approved")) {
                mMaterialSaveBtnLay.setVisibility(View.GONE);
                mMaterialEdit = "";

            }

        }
    }

    public void getMeasurementView(){
        if (mMeasurementStr.equalsIgnoreCase("Tailor Come To Your Place")) {
            getAppointmentMeasurementDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                mMeasurementParLay.setVisibility(View.GONE);
                mMeasurementTypeViewBool = "false";

            } else {
                mMeasurementParLay.setVisibility(View.VISIBLE);
                mMeasurementTypeViewBool = "true";
                mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);
                mMeasurementSaveBtnLay.setVisibility(View.GONE);
                mMeasurementEdit = "";
                if (mMeasurementApprovedStr.equalsIgnoreCase("Approved")) {
                    mMeasurementApproveRejectLay.setVisibility(View.GONE);
                }
            }
        } else if (mMeasurementStr.equalsIgnoreCase("Manually")) {
            mMeasurementParLay.setVisibility(View.GONE);
            mMeasurementTypeViewBool = "false";
            mMeasurementTypeBoolStr = "true";

        } else if (mMeasurementStr.equalsIgnoreCase("Go to Tailor Shop")) {
            mMeasurementParLay.setVisibility(View.VISIBLE);
            mMeasurementTypeViewBool = "true";
            mMeasurementSaveBtnLay.setVisibility(View.VISIBLE);
            mMeasurementApproveRejectLay.setVisibility(View.GONE);
            mMeasurementEdit = "Edit";
            getAppointmentMeasurementDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment") && mMeasurementApprovedStr.equalsIgnoreCase("Approved")) {
                mMeasurementSaveBtnLay.setVisibility(View.GONE);
                mMeasurementEdit = "";

            }

        } else if (mMeasurementStr.equalsIgnoreCase("Own Material-Courier the Material")) {
            getAppointmentMeasurementDateApiCall(AppConstants.DIRECT_USERS_ID,"Buyer");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")) {
                mMeasurementParLay.setVisibility(View.GONE);
                mMeasurementTypeViewBool = "false";

            } else {
                mMeasurementParLay.setVisibility(View.VISIBLE);
                mMeasurementTypeViewBool = "true";
                mMeasurementApproveRejectLay.setVisibility(View.VISIBLE);
                mMeasurementSaveBtnLay.setVisibility(View.GONE);
                mMeasurementEdit = "";
                if (mMeasurementApprovedStr.equalsIgnoreCase("Approved")) {
                    mMeasurementApproveRejectLay.setVisibility(View.GONE);
                }
            }

        } else if (mMeasurementStr.equalsIgnoreCase("Companies-Material")) {
            mMeasurementParLay.setVisibility(View.GONE);
            mMeasurementTypeViewBool = "false";
            mMeasurementTypeBoolStr = "true";

        } else if (mMeasurementStr.equalsIgnoreCase("Own Material-Direct Delivery")) {
            mMeasurementParLay.setVisibility(View.VISIBLE);
            mMeasurementTypeViewBool = "true";
            mMeasurementSaveBtnLay.setVisibility(View.VISIBLE);
            mMeasurementApproveRejectLay.setVisibility(View.GONE);
            mMeasurementEdit = "Edit";
            getAppointmentMeasurementDateApiCall(sharedPreferences.getString("TailorId",""),"Tailor");
            if (AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("BookAnAppointment") &&mMeasurementApprovedStr.equalsIgnoreCase("Approved")) {
                mMeasurementSaveBtnLay.setVisibility(View.GONE);
                mMeasurementEdit = "";

            }
        }
    }

    @Override
    public void onBackPressed() {
       if (!AppConstants.CHECK_BOOK_APPOINTMENT.equalsIgnoreCase("RequestList")){
           AppConstants.DATE_FOR_RESTRICT_MATERIAL = "";
           AppConstants.DATE_FOR_RESTRICT_MEASUREMENT = "";

           super.onBackPressed();
           this.overridePendingTransition(R.anim.animation_f_enter,
                   R.anim.animation_f_leave);
        }
    }
}
