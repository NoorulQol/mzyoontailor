package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class GetLanguageEntity implements Serializable {
    public String CountryName;
    public String Flag;
    public String Id;
    public String Language;

    public String getId() {
        return Id == null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getLanguage() {
        return Language == null ? "" : Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }
    public String getCountryName() {
        return CountryName == null ? "" : CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String  getFlag() {
        return Flag == null ? "" : Flag;
    }

    public void setFlag(String  flag) {
        Flag = flag;
    }

}
