package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetCustomizationThreeEntity implements Serializable {
    public int Id;
    public String AttributeNameInEnglish;
    public String AttributeNameInArabic;
    public String Images;
    private boolean isChecked;
    public String NameInEnglish;
    public String AttributeImage;

    public String getAttributeImage() {
        return AttributeImage;
    }

    public void setAttributeImage(String attributeImage) {
        AttributeImage = attributeImage;
    }


    public String getNameInEnglish() {
        return NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public Boolean get_switch() {
        return _switch;
    }

    public void set_switch(Boolean _switch) {
        this._switch = _switch;
    }

    @SerializedName("Switch")
    @Expose

    private Boolean _switch;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getAttributeNameInEnglish() {
        return AttributeNameInEnglish == null ? "" : AttributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        AttributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameInArabic() {
        return AttributeNameInArabic == null ? "" : AttributeNameInArabic;
    }

    public void setAttributeNameInArabic(String attributeNameInArabic) {
        AttributeNameInArabic = attributeNameInArabic;
    }

    public String getImages() {
        return Images == null ? "" : Images;
    }

    public void setImages(String images) {
        Images = images;
    }

}
