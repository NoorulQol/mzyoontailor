package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductPrice {

    @SerializedName("Price")
    @Expose
    private Float price;
    @SerializedName("Appoinment")
    @Expose
    private Float appoinment;
    @SerializedName("Tax")
    @Expose
    private String tax;
    @SerializedName("Total")
    @Expose
    private Float total;

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getAppoinment() {
        return appoinment;
    }

    public void setAppoinment(Float appoinment) {
        this.appoinment = appoinment;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

}
