package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetColorByMateriald {
    @SerializedName("ColorInEnglish")
    @Expose
    private String colorInEnglish;
    @SerializedName("ColorInArabic")
    @Expose
    private String colorInArabic;

    public String getColorInEnglish() {
        return colorInEnglish;
    }

    public void setColorInEnglish(String colorInEnglish) {
        this.colorInEnglish = colorInEnglish;
    }

    public String getColorInArabic() {
        return colorInArabic;
    }

    public void setColorInArabic(String colorInArabic) {
        this.colorInArabic = colorInArabic;
    }

}
