package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetStichingChargeListEntity {



    @SerializedName("StichingCharges")
    @Expose
    private Float stichingCharges;
private Integer NoOfDays;

    public Integer getNoOfDays() {
        return NoOfDays;
    }

    public void setNoOfDays(Integer noOfDays) {
        NoOfDays = noOfDays;
    }

    public Float getStichingCharges() {
        return stichingCharges;
    }

    public void setStichingCharges(Float stichingCharges) {
        this.stichingCharges = stichingCharges;
    }

}

