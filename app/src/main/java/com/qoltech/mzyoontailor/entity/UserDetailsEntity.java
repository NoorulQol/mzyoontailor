package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class UserDetailsEntity  implements Serializable {
    public String USER_ID;
    public String USER_IMAGE;
    public String USER_NAME;
    public String MOBILE_NUM;
    public String EMAIL;
    public String DOB;
    public String GENDER;
    public String Language;
    public String COUNTRY_CODE;
    public String HINT_ON_OFF;

    public String getHINT_ON_OFF() {
        return HINT_ON_OFF == null ? "" : HINT_ON_OFF;
    }

    public void setHINT_ON_OFF(String HINT_ON_OFF) {
        this.HINT_ON_OFF = HINT_ON_OFF;
    }


    public String getCOUNTRY_CODE() {
        return COUNTRY_CODE == null ? "" : COUNTRY_CODE;
    }

    public void setCOUNTRY_CODE(String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }


    public String getLanguage() {
        return Language == null ? "" : Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getUSER_ID() {
        return USER_ID == null ? "" : USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSER_IMAGE() {
        return USER_IMAGE == null ? "" : USER_IMAGE;
    }

    public void setUSER_IMAGE(String USER_IMAGE) {
        this.USER_IMAGE = USER_IMAGE;
    }

    public String getUSER_NAME() {
        return USER_NAME == null ? "" : USER_NAME;
    }

    public void setUSER_NAME(String USER_NAME) {
        this.USER_NAME = USER_NAME;
    }

    public String getMOBILE_NUM() {
        return MOBILE_NUM == null ? "" : MOBILE_NUM;
    }

    public void setMOBILE_NUM(String MOBILE_NUM) {
        this.MOBILE_NUM = MOBILE_NUM;
    }

    public String getEMAIL() {
        return EMAIL == null ?  "" : EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getDOB() {
        return DOB == null ? "" : DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGENDER() {
        return GENDER == null ? "" : GENDER;
    }

    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

}
