package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaterialChargesEntity {
    @SerializedName("MaterialCharges")
    @Expose
    private Float materialCharges;

    public Float getMaterialCharges() {
        return materialCharges;
    }

    public void setMaterialCharges(Float materialCharges) {
        this.materialCharges = materialCharges;
    }

}
