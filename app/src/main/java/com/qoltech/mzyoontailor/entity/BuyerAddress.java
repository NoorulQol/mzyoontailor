package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyerAddress {

    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("Floor")
    @Expose
    private String floor;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("Country_Name")
    @Expose
    private String countryName;
    @SerializedName("StateName")
    @Expose
    private String stateName;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

}