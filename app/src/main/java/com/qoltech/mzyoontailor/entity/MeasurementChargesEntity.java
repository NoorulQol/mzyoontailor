package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MeasurementChargesEntity {

    @SerializedName("MeasurementCharges")
    @Expose
    private Float measurementCharges;

    public Float getMeasurementCharges() {
        return measurementCharges;
    }

    public void setMeasurementCharges(Float measurementCharges) {
        this.measurementCharges = measurementCharges;
    }

}
