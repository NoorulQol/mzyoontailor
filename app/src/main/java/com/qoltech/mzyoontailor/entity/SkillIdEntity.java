package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class SkillIdEntity implements Serializable {
    public String getId() {
        return Id ==  null ? "" : Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String Id;
}
