package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CheckGetOrderDetailsResult {


    @SerializedName("Status")
    @Expose
    private List<Status> status = null;
    @SerializedName("OrderDetail")
    @Expose
    private List<OrderDetail> orderDetail = null;
    @SerializedName("Shipping_Charges")
    @Expose
    private List<ShippingCharge> shippingCharges = null;
    @SerializedName("ProductPrice")
    @Expose
    private List<ProductPrice> productPrice = null;
    @SerializedName("BuyerAddress")
    @Expose
    private List<BuyerAddress> buyerAddress = null;

    @SerializedName("GetAppoinmentLeftMeasurement")
    @Expose
    private List<GetAppoinmentLeftMaterialEntity> getAppoinmentLeftMaterialEntities = null;


    public List<GetOrderDetailScheduleTypeEntity> getGetOrderDetailScheduleTypeEntities() {
        return getOrderDetailScheduleTypeEntities;
    }

    public void setGetOrderDetailScheduleTypeEntities(List<GetOrderDetailScheduleTypeEntity>
                                                              getOrderDetailScheduleTypeEntities) {
        this.getOrderDetailScheduleTypeEntities = getOrderDetailScheduleTypeEntities;
    }

    @SerializedName("GetOrderDetailScheduleType")
    @Expose
    private List<GetOrderDetailScheduleTypeEntity> getOrderDetailScheduleTypeEntities = null;

    public List<MaterialChargesEntity> getMaterialChargesEntities() {
        return materialChargesEntities;
    }

    public void setMaterialChargesEntities(List<MaterialChargesEntity> materialChargesEntities) {
        this.materialChargesEntities = materialChargesEntities;
    }

    @SerializedName("MaterialCharges")
    @Expose
    private List<MaterialChargesEntity> materialChargesEntities = null;



    @SerializedName("StichingAndMaterialCharge")
    @Expose
    private List<StitchingChargesEntity> stitchingChargesEntities = null;


    public List<AdditionalMaterialImageEntity> getAdditionalMaterialImageEntities() {
        return additionalMaterialImageEntities;
    }

    public void setAdditionalMaterialImageEntities(List<AdditionalMaterialImageEntity> additionalMaterialImageEntities) {
        this.additionalMaterialImageEntities = additionalMaterialImageEntities;
    }

    @SerializedName("AdditionalMaterialImage")
    @Expose
    private List<AdditionalMaterialImageEntity> additionalMaterialImageEntities = null;

    public List<StitchingChargesEntity> getStitchingChargesEntities() {
        return stitchingChargesEntities;
    }

    public void setStitchingChargesEntities(List<StitchingChargesEntity> stitchingChargesEntities) {
        this.stitchingChargesEntities = stitchingChargesEntities;
    }

    public List<UrgentStichingChargeEntity> getUrgentStichingChargeEntities() {
        return urgentStichingChargeEntities;
    }

    public void setUrgentStichingChargeEntities(List<UrgentStichingChargeEntity> urgentStichingChargeEntities) {
        this.urgentStichingChargeEntities = urgentStichingChargeEntities;
    }

    @SerializedName("UrgentStichingCharges")
    @Expose
    private List<UrgentStichingChargeEntity> urgentStichingChargeEntities = null;


    public List<MeasurementChargesEntity> getMeasurementChargesEntities() {
        return measurementChargesEntities;
    }

    public void setMeasurementChargesEntities(List<MeasurementChargesEntity> measurementChargesEntities) {
        this.measurementChargesEntities = measurementChargesEntities;
    }

    @SerializedName("MeasurementCharges")
    @Expose
    private List<MeasurementChargesEntity> measurementChargesEntities = null;
    public List<GetAppoinmentLeftMaterialEntity> getGetAppoinmentLeftMaterialEntities() {
        return getAppoinmentLeftMaterialEntities;
    }

    public void setGetAppoinmentLeftMaterialEntities(List<GetAppoinmentLeftMaterialEntity> getAppoinmentLeftMaterialEntities) {
        this.getAppoinmentLeftMaterialEntities = getAppoinmentLeftMaterialEntities;
    }

    public List<GetAppoinmentLeftMeasurementEntity> getGetAppoinmentLeftMeasurementEntities() {
        return getAppoinmentLeftMeasurementEntities;
    }

    public void setGetAppoinmentLeftMeasurementEntities(List<GetAppoinmentLeftMeasurementEntity> getAppoinmentLeftMeasurementEntities) {
        this.getAppoinmentLeftMeasurementEntities = getAppoinmentLeftMeasurementEntities;
    }

    @SerializedName("GetAppoinmentLeftMateril")
    @Expose
    private List<GetAppoinmentLeftMeasurementEntity> getAppoinmentLeftMeasurementEntities = null;


    public List<Balances> getBalances() {
        return balances;
    }

    public void setBalances(List<Balances> balances) {
        this.balances = balances;
    }

    @SerializedName("Balances")
    @Expose
    private List<Balances> balances;

    public List<ReferenceImagesSkillUpdateGetEntity> getReferenceImage() {
        return referenceImage == null ? new ArrayList<ReferenceImagesSkillUpdateGetEntity>() : referenceImage;

//        return seasons == null ? new ArrayList<CustomizationSessionEntity>() : seasons;
    }

    public void setReferenceImage(List<ReferenceImagesSkillUpdateGetEntity> referenceImage) {
        this.referenceImage = referenceImage;
    }

    public List<MaterialImagesSkillUpdateGetEntity> getMaterialImage() {
        return materialImage == null ? new ArrayList<MaterialImagesSkillUpdateGetEntity>() : materialImage;
    }

    public void setMaterialImage(List<MaterialImagesSkillUpdateGetEntity> materialImage) {
        this.materialImage = materialImage;
    }

    @SerializedName("ReferenceImage")
    @Expose
    private List<ReferenceImagesSkillUpdateGetEntity> referenceImage = null;
    @SerializedName("MaterialImage")
    @Expose
    private List<MaterialImagesSkillUpdateGetEntity> materialImage = null;

    public List<Status> getStatus() {
        return status == null ? new ArrayList<Status>() : status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }

    public List<OrderDetail> getOrderDetail() {
        return orderDetail == null ? new ArrayList<OrderDetail>() : orderDetail;
    }

    public void setOrderDetail(List<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public List<ShippingCharge> getShippingCharges() {
        return shippingCharges == null ? new ArrayList<ShippingCharge>() : shippingCharges;
    }

    public void setShippingCharges(List<ShippingCharge> shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public List<ProductPrice> getProductPrice() {
        return productPrice == null ? new ArrayList<ProductPrice>() : productPrice;
    }

    public void setProductPrice(List<ProductPrice> productPrice) {
        this.productPrice = productPrice;
    }

    public List<BuyerAddress> getBuyerAddress() {
        return buyerAddress == null ? new ArrayList<BuyerAddress>() : buyerAddress;
    }

    public void setBuyerAddress(List<BuyerAddress> buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public List<GetTailorPaymentStatusEntity> getGetTailorPaymentStatusEntities() {
        return getTailorPaymentStatusEntities;
    }

    public void setGetTailorPaymentStatusEntities(List<GetTailorPaymentStatusEntity> getTailorPaymentStatusEntities) {
        this.getTailorPaymentStatusEntities = getTailorPaymentStatusEntities;
    }

    @SerializedName("GetTailorPaymentStatus")
    @Expose
    private List<GetTailorPaymentStatusEntity> getTailorPaymentStatusEntities = null;

    public List<CustomerRatingEntity> getCustomerRatingEntities() {
        return customerRatingEntities;
    }

    public void setCustomerRatingEntities(List<CustomerRatingEntity> customerRatingEntities) {
        this.customerRatingEntities = customerRatingEntities;
    }

    @SerializedName("CustomerRating")
    @Expose
    private List<CustomerRatingEntity> customerRatingEntities = null;

    public List<MaterialReviewEntity> getMaterialReview() {
        return materialReview;
    }

    public void setMaterialReview(List<MaterialReviewEntity> materialReview) {
        this.materialReview = materialReview;
    }

    @SerializedName("MaterialReview")
    @Expose
    private List<MaterialReviewEntity> materialReview = null;


    public List<MaterialRatingEntity> getMaterialRating() {
        return materialRating;
    }

    public void setMaterialRating(List<MaterialRatingEntity> materialRating) {
        this.materialRating = materialRating;
    }

    @SerializedName("MaterialRating")
    @Expose
    private List<MaterialRatingEntity> materialRating = null;


}
