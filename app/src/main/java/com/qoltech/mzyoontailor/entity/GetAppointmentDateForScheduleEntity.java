package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class GetAppointmentDateForScheduleEntity implements Serializable {
    public String FromDt;
    public String ToDt;
    public String AppointmentTime;

    public String getFromDt() {
        return FromDt == null ? "" : FromDt;
    }

    public void setFromDt(String fromDt) {
        FromDt = fromDt;
    }

    public String getToDt() {
        return ToDt == null ? "" : ToDt;
    }

    public void setToDt(String toDt) {
        ToDt = toDt;
    }

    public String getAppointmentTime() {
        return AppointmentTime == null ? "" : AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

}
