package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class ShopImagesEntity implements Serializable {
    public String ShopProfileId;
    public String Image;

    public String getShopProfileId() {
        return ShopProfileId;
    }

    public void setShopProfileId(String shopProfileId) {
        ShopProfileId = shopProfileId;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
