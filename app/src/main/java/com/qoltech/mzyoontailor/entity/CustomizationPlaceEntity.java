package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class CustomizationPlaceEntity implements Serializable {

    public int Id;
    public String PlaceInEnglish;
    public String PlaceInArabic;
    public String Image;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getPlaceInEnglish() {
        return PlaceInEnglish == null ? "" : PlaceInEnglish;
    }

    public void setPlaceInEnglish(String placeInEnglish) {
        PlaceInEnglish = placeInEnglish;
    }

    public String getPlaceInArabic() {
        return PlaceInArabic == null ? "" : PlaceInArabic;
    }

    public void setPlaceInArabic(String placeInArabic) {
        PlaceInArabic = placeInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }


}
