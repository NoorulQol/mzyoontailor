package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class TailorListEntity implements Serializable {

    public int Id;
    public String TailorNameInEnglish;
    public String TailorNameInArabic;
    public String AddressInEnglish;
    public String AddressinArabic;
    public String Latitude;
    public String Longitude;
    public String ShopNameInEnglish;
    public String ShopNameInArabic;
    public String ShopOwnerImageURL;
    public int Rating;
    public int OrderCount;
    public boolean isChecked;

    public String getAddressInEnglish() {
        return AddressInEnglish == null ? "" : AddressInEnglish;
    }

    public void setAddressInEnglish(String addressInEnglish) {
        AddressInEnglish = addressInEnglish;
    }

    public String getAddressinArabic() {
        return AddressinArabic == null ? "" : AddressinArabic;
    }

    public void setAddressinArabic(String addressinArabic) {
        AddressinArabic = addressinArabic;
    }

    public String getLatitude() {
        return Latitude == null ? "" : Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude == null ? "" : Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish == null ? "" : ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic == null ? "" : ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic;
    }

    public String getShopOwnerImageURL() {
        return ShopOwnerImageURL == null ? "" : ShopOwnerImageURL;
    }

    public void setShopOwnerImageURL(String shopOwnerImageURL) {
        ShopOwnerImageURL = shopOwnerImageURL;
    }

    public int getRating() {
        return Rating;
    }

    public void setRating(int rating) {
        Rating = rating;
    }

    public int getOrderCount() {
        return OrderCount;
    }

    public void setOrderCount(int orderCount) {
        OrderCount = orderCount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int count;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTailorNameInEnglish() {
        return TailorNameInEnglish == null ? "" : TailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        TailorNameInEnglish = tailorNameInEnglish;
    }

    public String getTailorNameInArabic() {
        return TailorNameInArabic == null ? "" : TailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        TailorNameInArabic = tailorNameInArabic;
    }
}
