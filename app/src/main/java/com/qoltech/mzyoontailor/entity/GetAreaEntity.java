package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetAreaEntity implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;

    public Boolean get_switch() {
        return _switch;
    }

    public void set_switch(Boolean _switch) {
        this._switch = _switch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

}