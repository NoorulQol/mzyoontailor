package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyerOrderDetailsResult {

    @SerializedName("Status")
    @Expose
    private List<Status> status = null;
    @SerializedName("OrderDetail")
    @Expose
    private List<OrderDetail> orderDetail = null;
    @SerializedName("Shipping_Charges")
    @Expose
    private List<ShippingCharge> shippingCharges = null;
    @SerializedName("ProductPrice")
    @Expose
    private List<ProductPrice> productPrice = null;
    @SerializedName("BuyerAddress")
    @Expose
    private List<BuyerAddress> buyerAddress = null;
    @SerializedName("ReferenceImage")
    @Expose
    private List<ReferenceImagesSkillUpdateGetEntity> referenceImage = null;
    @SerializedName("MaterialImage")
    @Expose
    private List<MaterialImagesSkillUpdateGetEntity> materialImage = null;

    public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }

    public List<OrderDetail> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<OrderDetail> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public List<ShippingCharge> getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(List<ShippingCharge> shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public List<ProductPrice> getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(List<ProductPrice> productPrice) {
        this.productPrice = productPrice;
    }

    public List<BuyerAddress> getBuyerAddress() {
        return buyerAddress;
    }

    public void setBuyerAddress(List<BuyerAddress> buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public List<ReferenceImagesSkillUpdateGetEntity> getReferenceImage() {
        return referenceImage;
    }

    public void setReferenceImage(List<ReferenceImagesSkillUpdateGetEntity> referenceImage) {
        this.referenceImage = referenceImage;
    }

    public List<MaterialImagesSkillUpdateGetEntity> getMaterialImage() {
        return materialImage;
    }

    public void setMaterialImage(List<MaterialImagesSkillUpdateGetEntity> materialImage) {
        this.materialImage = materialImage;
    }

}
