package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCustomizationsByTailorIdResult {

    @SerializedName("GetTailorCustomization")
    @Expose
    private List<GetTailorCustomizationSkillUpdate> getTailorCustomization = null;
    @SerializedName("GetMaterial")
    @Expose
    private List<GetMaterialForUpdatingSkillUpdateChargesEntity> getMaterial = null;

    @SerializedName("StichingCharges")
    @Expose
    List<GetStichingChargeListEntity> StichingCharges ;
    public List<GetStichingChargeListEntity> getStichingCharge() {
        return StichingCharges;
    }

    public void setStichingCharge(List<GetStichingChargeListEntity> stichingCharge) {
        StichingCharges = stichingCharge;
    }



    public List<GetTailorCustomizationSkillUpdate> getGetTailorCustomization() {
        return getTailorCustomization;
    }

    public void setGetTailorCustomization(List<GetTailorCustomizationSkillUpdate> getTailorCustomization) {
        this.getTailorCustomization = getTailorCustomization;
    }

    public List<GetMaterialForUpdatingSkillUpdateChargesEntity> getGetMaterial() {
        return getMaterial;
    }

    public void setGetMaterial(List<GetMaterialForUpdatingSkillUpdateChargesEntity> getMaterial) {
        this.getMaterial = getMaterial;
    }

}
