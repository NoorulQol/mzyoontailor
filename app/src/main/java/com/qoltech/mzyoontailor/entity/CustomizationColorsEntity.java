package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class CustomizationColorsEntity implements Serializable {

    public int Id;
    public String ColorInEnglish;
    public String ColorInArabic;
    public String Image;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getColorInEnglish() {
        return ColorInEnglish == null ? "" : ColorInEnglish;
    }

    public void setColorInEnglish(String colorInEnglish) {
        ColorInEnglish = colorInEnglish;
    }

    public String getColorInArabic() {
        return ColorInArabic == null ? "" : ColorInArabic;
    }

    public void setColorInArabic(String colorInArabic) {
        ColorInArabic = colorInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }


}
