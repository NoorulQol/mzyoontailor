package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTailorPaymentStatusEntity {
    @SerializedName("TransactionMode")
    @Expose
    private String transactionMode;
    @SerializedName("ReferenceNo")
    @Expose
    private Integer referenceNo;
    @SerializedName("status")
    @Expose
    private String status;

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public Integer getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(Integer referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
