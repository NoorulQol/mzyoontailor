package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class AppointmentMeasurementEntity implements Serializable {
    public String Status;
    public int OrderId;
    public int BookAppointId;
    public String MeasurementInEnglish;
    public String MeasurementInArabic;
    public String BodyImage;
    public int AppointmentId;
    public String Payment;

    public String getPayment() {
        return Payment == null ? "" : Payment;
    }

    public void setPayment(String payment) {
        Payment = payment;
    }

    public int getAppointmentId() {
        return AppointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        AppointmentId = appointmentId;
    }


    public String getStatus() {
        return Status == null ? "" : Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public int getBookAppointId() {
        return BookAppointId;
    }

    public void setBookAppointId(int bookAppointId) {
        BookAppointId = bookAppointId;
    }

    public String getMeasurementInEnglish() {
        return MeasurementInEnglish == null ? "" : MeasurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        MeasurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return MeasurementInArabic == null ? "" : MeasurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        MeasurementInArabic = measurementInArabic;
    }

    public String getBodyImage() {
        return BodyImage == null ? "" : BodyImage ;
    }

    public void setBodyImage(String bodyImage) {
        BodyImage = bodyImage;
    }

}
