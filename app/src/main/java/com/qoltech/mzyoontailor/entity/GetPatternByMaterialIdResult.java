package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPatternByMaterialIdResult {
    @SerializedName("PatternImg")
    @Expose
    private List<GetPatternImageByMaterialId> patternImg = null;
    @SerializedName("GetPattern")
    @Expose
    private List<GetPatternByMaterialIdEntity> getPattern = null;
    @SerializedName("GetColors")
    @Expose
    private List<GetColorByIdEntity> getColors = null;

private List<GetMaterialFriendlyNameEntity> GetMaterialName=null;

    public List<GetMaterialFriendlyNameEntity> getGetMaterialName() {
        return GetMaterialName;
    }

    public void setGetMaterialName(List<GetMaterialFriendlyNameEntity> getMaterialName) {
        GetMaterialName = getMaterialName;
    }

    public List<GetPatternImageByMaterialId> getPatternImg() {
        return patternImg;
    }

    public void setPatternImg(List<GetPatternImageByMaterialId> patternImg) {
        this.patternImg = patternImg;
    }

    public List<GetPatternByMaterialIdEntity> getGetPattern() {
        return getPattern;
    }

    public void setGetPattern(List<GetPatternByMaterialIdEntity> getPattern) {
        this.getPattern = getPattern;
    }

    public List<GetColorByIdEntity> getGetColors() {
        return getColors;
    }

    public void setGetColors(List<GetColorByIdEntity> getColors) {
        this.getColors = getColors;
    }

}
