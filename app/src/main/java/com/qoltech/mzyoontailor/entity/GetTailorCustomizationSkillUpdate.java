package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTailorCustomizationSkillUpdate {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("AttributeNameInEnglish")
    @Expose
    private String attributeNameInEnglish;
    @SerializedName("AttributeNameinArabic")
    @Expose
    private String attributeNameinArabic;
    @SerializedName("AttributeImage")
    @Expose
    private String attributeImage;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttributeNameInEnglish() {
        return attributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        this.attributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameinArabic() {
        return attributeNameinArabic;
    }

    public void setAttributeNameinArabic(String attributeNameinArabic) {
        this.attributeNameinArabic = attributeNameinArabic;
    }

    public String getAttributeImage() {
        return attributeImage;
    }

    public void setAttributeImage(String attributeImage) {
        this.attributeImage = attributeImage;
    }

}
