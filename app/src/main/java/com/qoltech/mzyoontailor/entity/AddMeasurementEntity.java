package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class AddMeasurementEntity implements Serializable {
    public String Name;
    public String Image;
    public String NameInEnglish;
    public String NameInArabic;
    public String Gender;
    public String GenderInArabic;
    public Integer Id;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getGenderInArabic() {
        return GenderInArabic == null ? "" : GenderInArabic;
    }

    public void setGenderInArabic(String genderInArabic) {
        GenderInArabic = genderInArabic;
    }

}
