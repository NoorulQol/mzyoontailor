package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MeasurementOneEntity implements Serializable {

    private int Id;
    private String MeasurementInEnglish;
    private String MeasurementInArabic;
    private String BodyImage;
    public Boolean get_switch() {
        return _switch;
    }

    public void set_switch(Boolean _switch) {
        this._switch = _switch;
    }

    @SerializedName("Switch")
    @Expose

    private Boolean _switch;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getMeasurementInEnglish() {
        return MeasurementInEnglish == null ? "" : MeasurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        MeasurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return MeasurementInArabic == null ? "" : MeasurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        MeasurementInArabic = measurementInArabic;
    }

    public String getBodyImage() {
        return BodyImage == null ? "" : BodyImage;
    }

    public void setBodyImage(String bodyImage) {
        BodyImage = bodyImage;
    }

}
