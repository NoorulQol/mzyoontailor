package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPatternByMaterialIdEntity {

    @SerializedName("PatternInEnglish")
    @Expose
    private String patternInEnglish;
    @SerializedName("PatternInArabic")
    @Expose
    private String patternInArabic;
    @SerializedName("PlaceInEnglish")
    @Expose
    private String placeInEnglish;
    @SerializedName("PlaceInArabic")
    @Expose
    private String placeInArabic;
    @SerializedName("BrandInEnglish")
    @Expose
    private String brandInEnglish;
    @SerializedName("BrandInArabic")
    @Expose
    private String brandInArabic;
    @SerializedName("MaterialInEnglish")
    @Expose
    private String materialInEnglish;
    @SerializedName("MaterialInArabic")
    @Expose
    private String materialInArabic;

    public String getPatternInEnglish() {
        return patternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        this.patternInEnglish = patternInEnglish;
    }

    public String getPatternInArabic() {
        return patternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        this.patternInArabic = patternInArabic;
    }

    public String getPlaceInEnglish() {
        return placeInEnglish;
    }

    public void setPlaceInEnglish(String placeInEnglish) {
        this.placeInEnglish = placeInEnglish;
    }

    public String getPlaceInArabic() {
        return placeInArabic;
    }

    public void setPlaceInArabic(String placeInArabic) {
        this.placeInArabic = placeInArabic;
    }

    public String getBrandInEnglish() {
        return brandInEnglish;
    }

    public void setBrandInEnglish(String brandInEnglish) {
        this.brandInEnglish = brandInEnglish;
    }

    public String getBrandInArabic() {
        return brandInArabic;
    }

    public void setBrandInArabic(String brandInArabic) {
        this.brandInArabic = brandInArabic;
    }

    public String getMaterialInEnglish() {
        return materialInEnglish;
    }

    public void setMaterialInEnglish(String materialInEnglish) {
        this.materialInEnglish = materialInEnglish;
    }

    public String getMaterialInArabic() {
        return materialInArabic;
    }

    public void setMaterialInArabic(String materialInArabic) {
        this.materialInArabic = materialInArabic;
    }

}
