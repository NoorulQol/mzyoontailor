package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetCustomizationOneEntity  implements Serializable {


    public ArrayList<CustomizationSessionEntity> seasons;
    public ArrayList<CustomizationPlaceEntity> placeofIndustrys;
    public ArrayList<CustomizationBrandEntity> materialBrand;


    public ArrayList<CustomizationSessionEntity> getSeasons() {
        return seasons == null ? new ArrayList<CustomizationSessionEntity>() : seasons;
    }

    public void setSeasons(ArrayList<CustomizationSessionEntity> seasons) {
        this.seasons = seasons;
    }

    public ArrayList<CustomizationPlaceEntity> getPlaceofIndustrys() {
        return placeofIndustrys == null ? new ArrayList<CustomizationPlaceEntity>() : placeofIndustrys;
    }

    public void setPlaceofIndustrys(ArrayList<CustomizationPlaceEntity> placeofIndustrys) {
        this.placeofIndustrys = placeofIndustrys;
    }

    public ArrayList<CustomizationBrandEntity> getMaterialBrand() {
        return materialBrand == null ? new ArrayList<CustomizationBrandEntity>() : materialBrand;
    }

    public void setMaterialBrand(ArrayList<CustomizationBrandEntity> materialBrand) {
        this.materialBrand = materialBrand;
    }


}
