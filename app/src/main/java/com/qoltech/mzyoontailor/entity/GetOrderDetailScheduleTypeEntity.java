package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderDetailScheduleTypeEntity {


    @SerializedName("DeliveryTypeInEnglish")
    @Expose
    private String deliveryTypeInEnglish;
    @SerializedName("DeliveryTypeInArabic")
    @Expose
    private String deliveryTypeInArabic;
    @SerializedName("OrderDt")
    @Expose
    private String orderDt;
    @SerializedName("AppointmentLeftScheduleCount")
    @Expose
    private Integer appointmentLeftScheduleCount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("ScheduleAppoinmentStatus")
    @Expose
    private Boolean scheduleAppoinmentStatus;


    private String AppointmentTime;

    public String getAppointmentTime() {
        return AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

    public String getDeliveryTypeInEnglish() {
        return deliveryTypeInEnglish;
    }

    public void setDeliveryTypeInEnglish(String deliveryTypeInEnglish) {
        this.deliveryTypeInEnglish = deliveryTypeInEnglish;
    }

    public String getDeliveryTypeInArabic() {
        return deliveryTypeInArabic;
    }

    public void setDeliveryTypeInArabic(String deliveryTypeInArabic) {
        this.deliveryTypeInArabic = deliveryTypeInArabic;
    }

    public String getOrderDt() {
        return orderDt;
    }

    public void setOrderDt(String orderDt) {
        this.orderDt = orderDt;
    }

    public Integer getAppointmentLeftScheduleCount() {
        return appointmentLeftScheduleCount;
    }

    public void setAppointmentLeftScheduleCount(Integer appointmentLeftScheduleCount) {
        this.appointmentLeftScheduleCount = appointmentLeftScheduleCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getScheduleAppoinmentStatus() {
        return scheduleAppoinmentStatus;
    }

    public void setScheduleAppoinmentStatus(Boolean scheduleAppoinmentStatus) {
        this.scheduleAppoinmentStatus = scheduleAppoinmentStatus;
    }

}
