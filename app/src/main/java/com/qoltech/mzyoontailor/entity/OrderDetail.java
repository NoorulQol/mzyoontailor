package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetail {
    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("OrderDt")
    @Expose
    private String orderDt;
    @SerializedName("Product_Name")
    @Expose
    private String productName;
    @SerializedName("NameInArabic")
    @Expose
    private String nameInArabic;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("Amount")
    @Expose
    private Float amount;
    @SerializedName("ServiceType")
    @Expose
    private String serviceType;
    @SerializedName("ServiceTypeInArabic")
    @Expose
    private String serviceTypeInArabic;
    @SerializedName("MeasurementType")
    @Expose
    private Integer measurementType;
    @SerializedName("MeasurementInEnglish")
    @Expose
    private String measurementInEnglish;
    @SerializedName("MeasurementInArabic")
    @Expose
    private String measurementInArabic;
    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("DressTypeName")
    @Expose
    private String dressTypeName;
    @SerializedName("DressType")
    @Expose
    private Integer dressType;
    @SerializedName("Gender")
    @Expose
    private Integer gender;
    @SerializedName("Balance")
    @Expose
    private Float balance;
    @SerializedName("PaymentStatus")
    @Expose
    private String paymentStatus;

    @SerializedName("MaterialType")
    @Expose
    private String materialType;
    @SerializedName("MaterialTypeInArabic")
    @Expose
    private String materialTypeInArabic;

    private  Integer PatternId;

    public Integer getPatternId() {
        return PatternId;
    }

    public void setPatternId(Integer patternId) {
        PatternId = patternId;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    @SerializedName("NoOfDays")
    @Expose
    private String noOfDays;
    public String getApproximateDeliveryDate() {
        return approximateDeliveryDate;
    }

    public void setApproximateDeliveryDate(String approximateDeliveryDate) {
        this.approximateDeliveryDate = approximateDeliveryDate;
    }

    @SerializedName("ApproximateDeliveryDate")
    @Expose
    private String approximateDeliveryDate;

    public String getMaterialTypeId() {
        return materialTypeId;
    }

    public void setMaterialTypeId(String materialTypeId) {
        this.materialTypeId = materialTypeId;
    }

    @SerializedName("MaterialTypeId")
    @Expose
    private String materialTypeId;


    public String getPatternInEnglish() {
        return patternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        this.patternInEnglish = patternInEnglish;
    }

    @SerializedName("PatternInEnglish")
    @Expose
    private String patternInEnglish;

    public String getPatternInArabic() {
        return patternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        this.patternInArabic = patternInArabic;
    }

    @SerializedName("PatternInArabic")
    @Expose
    private String patternInArabic;


    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getMaterialTypeInArabic() {
        return materialTypeInArabic;
    }

    public void setMaterialTypeInArabic(String materialTypeInArabic) {
        this.materialTypeInArabic = materialTypeInArabic;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderDt() {
        return orderDt;
    }

    public void setOrderDt(String orderDt) {
        this.orderDt = orderDt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNameInArabic() {
        return nameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        this.nameInArabic = nameInArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceTypeInArabic() {
        return serviceTypeInArabic;
    }

    public void setServiceTypeInArabic(String serviceTypeInArabic) {
        this.serviceTypeInArabic = serviceTypeInArabic;
    }

    public Integer getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(Integer measurementType) {
        this.measurementType = measurementType;
    }

    public String getMeasurementInEnglish() {
        return measurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        this.measurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return measurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        this.measurementInArabic = measurementInArabic;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getDressTypeName() {
        return dressTypeName;
    }

    public void setDressTypeName(String dressTypeName) {
        this.dressTypeName = dressTypeName;
    }

    public Integer getDressType() {
        return dressType;
    }

    public void setDressType(Integer dressType) {
        this.dressType = dressType;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

}