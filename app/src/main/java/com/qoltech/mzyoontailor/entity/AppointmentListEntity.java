package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class AppointmentListEntity implements Serializable {
    public int OrderId;
    public String OrderDate;
    public String NameInEnglish;
    public String NameInArabic;
    public String Image;
    public String Name;
    public int DressType;
    public int DeliveryTypeId;
    public String Customerid;

    public String getCustomerid() {
        return Customerid;
    }

    public void setCustomerid(String customerid) {
        Customerid = customerid;
    }

    public int getDressType() {
        return DressType;
    }

    public void setDressType(int dressType) {
        DressType = dressType;
    }

    public int getDeliveryTypeId() {
        return DeliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        DeliveryTypeId = deliveryTypeId;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(String orderDate) {
        OrderDate = orderDate;
    }

    public String getNameInEnglish() {
        return NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
