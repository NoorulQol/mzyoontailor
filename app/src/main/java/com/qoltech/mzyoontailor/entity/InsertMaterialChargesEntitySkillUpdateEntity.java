package com.qoltech.mzyoontailor.entity;

public class InsertMaterialChargesEntitySkillUpdateEntity {
    String MaterialId;
    String MaterialCharges;

    public String getMaterialId() {
        return MaterialId;
    }

    public void setMaterialId(String materialId) {
        MaterialId = materialId;
    }

    public String getMaterialCharges() {
        return MaterialCharges;
    }

    public void setMaterialCharges(String materialCharges) {
        MaterialCharges = materialCharges;
    }
}
