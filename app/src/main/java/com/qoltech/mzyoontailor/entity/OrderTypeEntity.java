package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderTypeEntity implements Serializable {

    private int id;
    private String HeaderInEnglish;
    private String HeaderInArabic;
    private String HeaderImage;
    @SerializedName("Switch")
    @Expose
    private Boolean Switch;

    private String UnSelected;
    public boolean Status;
    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }


    public String getUnSelected() {
        return UnSelected == null ? "" : UnSelected;
    }

    public void setUnSelected(String unSelected) {
        UnSelected = unSelected;
    }


    public Boolean get_switch() {
        return Switch;
    }

    public void set_switch(Boolean _switch) {
        this.Switch = _switch;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeaderInEnglish() {
        return HeaderInEnglish == null ? "" : HeaderInEnglish;
    }

    public void setHeaderInEnglish(String headerInEnglish) {
        HeaderInEnglish = headerInEnglish;
    }

    public String getHeaderInArabic() {
        return HeaderInArabic == null ? "" : HeaderInArabic;
    }

    public void setHeaderInArabic(String headerInArabic) {
        HeaderInArabic = headerInArabic;
    }

    public String getHeaderImage() {
        return HeaderImage == null ? "" : HeaderImage;
    }

    public void setHeaderImage(String headerImage) {
        HeaderImage = headerImage;
    }

    public String getBodyImage() {
        return BodyImage == null ? "" : BodyImage;
    }

    public void setBodyImage(String bodyImage) {
        BodyImage = bodyImage;
    }

    private String BodyImage;
}
