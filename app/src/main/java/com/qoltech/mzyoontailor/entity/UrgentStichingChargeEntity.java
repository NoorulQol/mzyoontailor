package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrgentStichingChargeEntity {

    @SerializedName("UrgentStichingCharges")
    @Expose
    private Float urgentStichingCharges;

    public Float getUrgentStichingCharges() {
        return urgentStichingCharges;
    }

    public void setUrgentStichingCharges(Float urgentStichingCharges) {
        this.urgentStichingCharges = urgentStichingCharges;
    }

}
