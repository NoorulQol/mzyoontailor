package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetQuotaionPendingEntity {


    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("ProductNameInEnglish")
    @Expose
    private String productNameInEnglish;
    @SerializedName("ProductNameInArabic")
    @Expose
    private String productNameInArabic;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("MeasurementType")
    @Expose
    private Integer measurementType;
    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("DressType")
    @Expose
    private Integer dressType;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("OrderType")
    @Expose
    private String orderType;


    public String getQuotationDt() {
        return quotationDt;
    }

    public void setQuotationDt(String quotationDt) {
        this.quotationDt = quotationDt;
    }

    @SerializedName("QuotationDt")
    @Expose
    private String quotationDt;
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getProductNameInEnglish() {
        return productNameInEnglish;
    }

    public void setProductNameInEnglish(String productNameInEnglish) {
        this.productNameInEnglish = productNameInEnglish;
    }

    public String getProductNameInArabic() {
        return productNameInArabic;
    }

    public void setProductNameInArabic(String productNameInArabic) {
        this.productNameInArabic = productNameInArabic;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(Integer measurementType) {
        this.measurementType = measurementType;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getDressType() {
        return dressType;
    }

    public void setDressType(Integer dressType) {
        this.dressType = dressType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

}
