package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StitchingChargesEntity {
    @SerializedName("StichingAndMaterialCharge")
    @Expose
    private Float stichingAndMaterialCharge;

    public Float getStichingAndMaterialCharge() {
        return stichingAndMaterialCharge;
    }

    public void setStichingAndMaterialCharge(Float stichingAndMaterialCharge) {
        this.stichingAndMaterialCharge = stichingAndMaterialCharge;
    }

}




