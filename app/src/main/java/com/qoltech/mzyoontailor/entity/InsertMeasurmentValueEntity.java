package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class InsertMeasurmentValueEntity implements Serializable {
    public String MeasurementId;
    public String Value;

    public String getMeasurementId() {
        return MeasurementId == null ? "" : MeasurementId;
    }

    public void setMeasurementId(String measurementId) {
        MeasurementId = measurementId;
    }

    public String getValue() {
        return Value == null ? "" : Value;
    }

    public void setValue(String value) {
        Value = value;
    }


}
