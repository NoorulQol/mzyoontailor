package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class OrderApprovalPriceSubTypeEntity implements Serializable {
    public String Image;
    public String NameInEnglish;

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

}
