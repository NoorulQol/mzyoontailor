package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class CustomizationAttributesEntity implements Serializable {
    public int Id;
    public String AttributeNameInEnglish;
    private boolean isChecked;
    public String Images;
    public String AttributeImage;
    public String AttributeNameinArabic;

    public String getImages() {
        return Images;
    }

    public void setImages(String images) {
        Images = images;
    }

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getAttributeImage() {
        return AttributeImage == null ? "" : AttributeImage;
    }

    public void setAttributeImage(String attributeImage) {
        AttributeImage = attributeImage;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getAttributeNameInEnglish() {
        return AttributeNameInEnglish == null ? "" : AttributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        AttributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameinArabic() {
        return AttributeNameinArabic == null ? "" : AttributeNameinArabic;
    }

    public void setAttributeNameinArabic(String attributeNameinArabic) {
        AttributeNameinArabic = attributeNameinArabic;
    }

}
