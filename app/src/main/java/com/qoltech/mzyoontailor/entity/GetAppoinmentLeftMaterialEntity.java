package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAppoinmentLeftMaterialEntity {

    @SerializedName("MeasurementInEnglish")
    @Expose
    private String measurementInEnglish;
    @SerializedName("MeasurementInArabic")
    @Expose
    private String measurementInArabic;
    @SerializedName("OrderDt")
    @Expose
    private String orderDt;
    @SerializedName("AppointmentLeftMeasurementCount")
    @Expose
    private Integer appointmentLeftMeterialCount;
    @SerializedName("status")
    @Expose
    private String status;
    private String AppointmentTime;

    public String getAppointmentTime() {
        return AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

    public boolean isMaasuremntAppoinmentStatus() {
        return maasuremntAppoinmentStatus;
    }

    public void setMaasuremntAppoinmentStatus(boolean maasuremntAppoinmentStatus) {
        this.maasuremntAppoinmentStatus = maasuremntAppoinmentStatus;
    }

    @SerializedName("MaasuremntAppoinmentStatus")
    @Expose
    private boolean maasuremntAppoinmentStatus;



    public String getMeasurementInEnglish() {
        return measurementInEnglish;
    }

    public void setMeasurementInEnglish(String measurementInEnglish) {
        this.measurementInEnglish = measurementInEnglish;
    }

    public String getMeasurementInArabic() {
        return measurementInArabic;
    }

    public void setMeasurementInArabic(String measurementInArabic) {
        this.measurementInArabic = measurementInArabic;
    }

    public String getOrderDt() {
        return orderDt;
    }

    public void setOrderDt(String orderDt) {
        this.orderDt = orderDt;
    }

    public Integer getAppointmentLeftMeterialCount() {
        return appointmentLeftMeterialCount;
    }

    public void setAppointmentLeftMeterialCount(Integer appointmentLeftMeterialCount) {
        this.appointmentLeftMeterialCount = appointmentLeftMeterialCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}