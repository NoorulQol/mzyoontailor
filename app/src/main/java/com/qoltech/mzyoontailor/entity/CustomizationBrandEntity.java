package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class CustomizationBrandEntity implements Serializable {

    public int Id;
    public String BrandInEnglish;
    public String BrandInArabic;
    public String Image;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getBrandInEnglish() {
        return BrandInEnglish == null ? "" : BrandInEnglish;
    }

    public void setBrandInEnglish(String brandInEnglish) {
        BrandInEnglish = brandInEnglish;
    }

    public String getBrandInArabic() {
        return BrandInArabic == null ? "" : BrandInArabic;
    }

    public void setBrandInArabic(String brandInArabic) {
        BrandInArabic = brandInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

}
