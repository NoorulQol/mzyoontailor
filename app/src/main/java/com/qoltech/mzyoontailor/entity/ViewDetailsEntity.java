package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class ViewDetailsEntity implements Serializable {
public ArrayList<GetpattternByIdEntity> GetpattternById;
    public ArrayList<GetColorByIdEntity> GetColorById;
    public ArrayList<PatternImgEntity> PatternImg;

    public ArrayList<PatternImgEntity> getPatternImg() {
        return PatternImg;
    }

    public void setPatternImg(ArrayList<PatternImgEntity> patternImg) {
        PatternImg = patternImg;
    }

    public ArrayList<GetpattternByIdEntity> getGetpattternById() {
        return GetpattternById ;
    }

    public void setGetpattternById(ArrayList<GetpattternByIdEntity> getpattternById) {
        GetpattternById = getpattternById;
    }

    public ArrayList<GetColorByIdEntity> getGetColorById() {
        return GetColorById ;
    }

    public void setGetColorById(ArrayList<GetColorByIdEntity> getColorById) {
        GetColorById = getColorById;
    }

}
