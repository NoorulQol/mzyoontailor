package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class ViewDetailsNewFlowEntity implements Serializable {
    public ArrayList<GetpattternByIdEntity> GetPattern;
    public ArrayList<GetColorByIdEntity> GetColors;
    public ArrayList<PatternImgEntity> PatternImg;
    public ArrayList<GetMaterialNameEntity> GetMaterialName;

    public ArrayList<GetMaterialNameEntity> getGetMaterialName() {
        return GetMaterialName ;
    }

    public void setGetMaterialName(ArrayList<GetMaterialNameEntity> getMaterialName) {
        GetMaterialName = getMaterialName;
    }


    public ArrayList<PatternImgEntity> getPatternImg() {
        return PatternImg ;
    }

    public void setPatternImg(ArrayList<PatternImgEntity> patternImg) {
        PatternImg = patternImg;
    }

    public ArrayList<GetpattternByIdEntity> getGetpattternById() {
        return GetPattern ;
    }

    public void setGetpattternById(ArrayList<GetpattternByIdEntity> getpattternById) {
        GetPattern = getpattternById;
    }

    public ArrayList<GetColorByIdEntity> getGetColorById() {
        return GetColors;
    }

    public void setGetColorById(ArrayList<GetColorByIdEntity> getColorById) {
        GetColors = getColorById;
    }
}
