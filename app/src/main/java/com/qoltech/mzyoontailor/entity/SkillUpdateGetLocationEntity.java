package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SkillUpdateGetLocationEntity {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Area")
    @Expose
    private Object area;

    public Boolean get_switch() {
        return _switch;
    }

    public void set_switch(Boolean _switch) {
        this._switch = _switch;
    }

    @SerializedName("Switch")
    @Expose
    private Boolean _switch;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getArea() {
        return area;
    }

    public void setArea(Object area) {
        this.area = area;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

}

