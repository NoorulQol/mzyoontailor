package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class ManuallyEntity implements Serializable {
    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String Name;
}
