package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMaterialChargesRateList {


    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("PatternInEnglish")
    @Expose
    private String patternInEnglish;
    @SerializedName("PatternInArabic")
    @Expose
    private String patternInArabic;
    @SerializedName("MaterialCharges")
    @Expose
    private Float materialCharges;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatternInEnglish() {
        return patternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        this.patternInEnglish = patternInEnglish;
    }

    public String getPatternInArabic() {
        return patternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        this.patternInArabic = patternInArabic;
    }

    public Float getMaterialCharges() {
        return materialCharges;
    }

    public void setMaterialCharges(Float materialCharges) {
        this.materialCharges = materialCharges;
    }

}
