package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAttributesIdToSaveLocalEntity {

    @SerializedName("CustomizationAttributeId")
    @Expose
    private Integer customizationAttributeId;
    @SerializedName("CustomizationId")
    @Expose
    private Integer customizationId;

    public Integer getCustomizationAttributeId() {
        return customizationAttributeId;
    }

    public void setCustomizationAttributeId(Integer customizationAttributeId) {
        this.customizationAttributeId = customizationAttributeId;
    }

    public Integer getCustomizationId() {
        return customizationId;
    }

    public void setCustomizationId(Integer customizationId) {
        this.customizationId = customizationId;
    }

}
