package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetCustomizationTwoEntity implements Serializable {

    public ArrayList<CustomizationMaterialsEntity> Materials;
    public ArrayList<CustomizationColorsEntity> Colors;
    public ArrayList<CustomizationPatternsEntity> Patterns;


    public ArrayList<CustomizationMaterialsEntity> getMaterials() {
        return Materials == null ? new ArrayList<CustomizationMaterialsEntity>() : Materials;
    }

    public void setMaterials(ArrayList<CustomizationMaterialsEntity> materials) {
        Materials = materials;
    }

    public ArrayList<CustomizationColorsEntity> getColors() {
        return Colors == null ? new ArrayList<CustomizationColorsEntity>() : Colors;
    }

    public void setColors(ArrayList<CustomizationColorsEntity> colors) {
        Colors = colors;
    }

    public ArrayList<CustomizationPatternsEntity> getPatterns() {
        return Patterns == null ? new ArrayList<CustomizationPatternsEntity>() : Patterns;
    }

    public void setPatterns(ArrayList<CustomizationPatternsEntity> patterns) {
        Patterns = patterns;
    }





}
