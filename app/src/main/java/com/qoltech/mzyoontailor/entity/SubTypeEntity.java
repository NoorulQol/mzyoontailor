package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubTypeEntity implements Serializable {

    public int Id;
    public int DressId;
    public String NameInEnglish;
    public String NameInArabic;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getDressId() {
        return DressId;
    }

    public void setDressId(int dressId) {
        DressId = dressId;
    }

    public String getNameInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        NameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        NameInArabic = nameInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String Image;


    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

}
