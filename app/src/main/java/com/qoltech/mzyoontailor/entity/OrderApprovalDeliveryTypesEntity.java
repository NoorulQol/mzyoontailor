package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class OrderApprovalDeliveryTypesEntity implements Serializable {
    public String getDeliveryType() {
        return DeliveryType == null ? "" : DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }

    public String DeliveryType;
}
