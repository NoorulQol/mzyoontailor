package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMaterialFriendlyNameEntity {

    @SerializedName("FriendlyNameInEnglish")
    @Expose
    private String friendlyNameInEnglish;
    @SerializedName("FriendlyNameInArabic")
    @Expose
    private String friendlyNameInArabic;

    public String getFriendlyNameInEnglish() {
        return friendlyNameInEnglish;
    }

    public void setFriendlyNameInEnglish(String friendlyNameInEnglish) {
        this.friendlyNameInEnglish = friendlyNameInEnglish;
    }

    public String getFriendlyNameInArabic() {
        return friendlyNameInArabic;
    }

    public void setFriendlyNameInArabic(String friendlyNameInArabic) {
        this.friendlyNameInArabic = friendlyNameInArabic;
    }

}