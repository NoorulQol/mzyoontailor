package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class OrderRequestListEntity implements Serializable {
    public int OrderId;
    public String ProductImage;
    public String Product_NameInEnglish;
    public String Product_NameInArabic;
    public String RequestDt;
    public int NoOfTailors;

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public String getProductImage() {
        return ProductImage == null ? "" : ProductImage;
    }

    public void setProductImage(String productImage) {
        ProductImage = productImage;
    }

    public String getProduct_NameInEnglish() {
        return Product_NameInEnglish == null ? "" : Product_NameInEnglish;
    }

    public void setProduct_NameInEnglish(String product_NameInEnglish) {
        Product_NameInEnglish = product_NameInEnglish;
    }

    public String getProduct_NameInArabic() {
        return Product_NameInArabic == null ? "" : Product_NameInArabic;
    }

    public void setProduct_NameInArabic(String product_NameInArabic) {
        Product_NameInArabic = product_NameInArabic;
    }

    public String getRequestDt() {
        return RequestDt == null ? "" : RequestDt;
    }

    public void setRequestDt(String requestDt) {
        RequestDt = requestDt;
    }

    public int getNoOfTailors() {
        return NoOfTailors;
    }

    public void setNoOfTailors(int noOfTailors) {
        NoOfTailors = noOfTailors;
    }


}
