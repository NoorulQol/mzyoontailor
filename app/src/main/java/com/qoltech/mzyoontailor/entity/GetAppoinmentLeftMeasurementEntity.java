package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAppoinmentLeftMeasurementEntity {
    @SerializedName("HeaderInEnglish")
    @Expose
    private String headerInEnglish;
    @SerializedName("HeaderInArabic")
    @Expose
    private String headerInArabic;
    @SerializedName("OrderDt")
    @Expose
    private String orderDt;
    @SerializedName("AppointmentLeftMeterialCount")
    @Expose
    private Integer appointmentLeftMeasurementCount;
    @SerializedName("status")
    @Expose
    private String status;

    private String AppointmentTime;

    public String getAppointmentTime() {
        return AppointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        AppointmentTime = appointmentTime;
    }

    public boolean isMaterialAppoinmentStatus() {
        return MaterialAppoinmentStatus;
    }

    public void setMaterialAppoinmentStatus(boolean materialAppoinmentStatus) {
        MaterialAppoinmentStatus = materialAppoinmentStatus;
    }

    @SerializedName("MaterialAppoinmentStatus")
    @Expose
    private boolean MaterialAppoinmentStatus;


    public String getHeaderInEnglish() {
        return headerInEnglish;
    }

    public void setHeaderInEnglish(String headerInEnglish) {
        this.headerInEnglish = headerInEnglish;
    }

    public String getHeaderInArabic() {
        return headerInArabic;
    }

    public void setHeaderInArabic(String headerInArabic) {
        this.headerInArabic = headerInArabic;
    }

    public String getOrderDt() {
        return orderDt;
    }

    public void setOrderDt(String orderDt) {
        this.orderDt = orderDt;
    }

    public Integer getAppointmentLeftMeasurementCount() {
        return appointmentLeftMeasurementCount;
    }

    public void setAppointmentLeftMeasurementCount(Integer appointmentLeftMeasurementCount) {
        this.appointmentLeftMeasurementCount = appointmentLeftMeasurementCount;
    }

    public String getStatus() {
        return status==null?"":status;
//        return Image == null ? "" : Image;

    }

    public void setStatus(String status) {
        this.status = status;
    }

}