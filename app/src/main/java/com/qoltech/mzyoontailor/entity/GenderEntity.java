package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GenderEntity implements Serializable {

    private int Id;
    private String gender;
    private String ImageURL;
    private String GenderInArabic;

    public String getBoolCheck() {
        return boolCheck == null ? "" : boolCheck;
    }

    public void setBoolCheck(String boolCheck) {
        this.boolCheck = boolCheck;
    }

    private String boolCheck;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getGender() {
        return gender == null ? "" : gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageURL() {
        return ImageURL == null ? "" : ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getGenderInArabic() {
        return GenderInArabic == null ? "" : GenderInArabic;
    }

    public void setGenderInArabic(String genderInArabic) {
        GenderInArabic = genderInArabic;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

}
