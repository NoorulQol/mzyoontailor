package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class GetMaterialNameEntity implements Serializable {

    public String FriendlyNameInEnglish;
    public String FriendlyNameInArabic;

    public String getFriendlyNameInEnglish() {
        return FriendlyNameInEnglish == null ? "" : FriendlyNameInEnglish;
    }

    public void setFriendlyNameInEnglish(String friendlyNameInEnglish) {
        FriendlyNameInEnglish = friendlyNameInEnglish;
    }

    public String getFriendlyNameInArabic() {
        return FriendlyNameInArabic == null ? "" : FriendlyNameInArabic;
    }

    public void setFriendlyNameInArabic(String friendlyNameInArabic) {
        FriendlyNameInArabic = friendlyNameInArabic;
    }

}
