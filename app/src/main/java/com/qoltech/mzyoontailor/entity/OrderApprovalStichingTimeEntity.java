package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class OrderApprovalStichingTimeEntity implements Serializable {
    public String StichingTimes;
    public String getStichingTime() {
        return StichingTimes == null ? "" : StichingTimes;
    }

    public void setStichingTime(String stichingTime) {
        StichingTimes = stichingTime;
    }

}
