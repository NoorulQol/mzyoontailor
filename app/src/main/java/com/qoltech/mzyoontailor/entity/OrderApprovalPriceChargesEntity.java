package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class OrderApprovalPriceChargesEntity implements Serializable {
    public String DescInEnglish;
    public int Amount;

    public String getDescInEnglish() {
        return DescInEnglish == null ? "" : DescInEnglish;
    }

    public void setDescInEnglish(String descInEnglish) {
        DescInEnglish = descInEnglish;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

}
