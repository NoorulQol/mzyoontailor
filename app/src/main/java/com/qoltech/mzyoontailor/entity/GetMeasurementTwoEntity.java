package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMeasurementTwoEntity implements Serializable {
    public ArrayList<GetMeasurementImageEntity> Image;

    public ArrayList<GetMeasurementImageEntity> getImage() {
        return Image == null ? new ArrayList<GetMeasurementImageEntity>() : Image;
    }

    public void setImage(ArrayList<GetMeasurementImageEntity> image) {
        Image = image;
    }

    public ArrayList<GetMeasurementPartEntity> getMeasurements() {
        return Measurements == null ? new ArrayList<GetMeasurementPartEntity>() : Measurements;
    }

    public void setMeasurements(ArrayList<GetMeasurementPartEntity> measurements) {
        Measurements = measurements;
    }

    public ArrayList<GetMeasurementPartEntity> Measurements;
}
