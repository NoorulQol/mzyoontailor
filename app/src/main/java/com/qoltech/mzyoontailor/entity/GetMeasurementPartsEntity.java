package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class GetMeasurementPartsEntity implements Serializable {
    public int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return Image ==  null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String Image;
}
