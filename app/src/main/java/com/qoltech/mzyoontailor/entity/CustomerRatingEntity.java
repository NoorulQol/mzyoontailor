package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerRatingEntity {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ProfilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("Review")
    @Expose
    private String review;
    @SerializedName("Rating")
    @Expose
    private Float rating;
    @SerializedName("CreatedDt")
    @Expose
    private String createdDt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(String createdDt) {
        this.createdDt = createdDt;
    }

}

