package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class DressSubIdEntity implements Serializable {
    public String DressSubTypeId;

    public String getDressSubTypeId() {
        return DressSubTypeId == null ? "" : DressSubTypeId;
    }

    public void setDressSubTypeId(String dressSubTypeId) {
        DressSubTypeId = dressSubTypeId;
    }

    public String getCustomizationId() {
        return CustomizationId == null ? "" : CustomizationId;
    }

    public void setCustomizationId(String customizationId) {
        CustomizationId = customizationId;
    }

    public String CustomizationId;
}
