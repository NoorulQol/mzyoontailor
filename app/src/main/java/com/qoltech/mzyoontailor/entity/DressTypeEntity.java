package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DressTypeEntity  implements Serializable {

    private int Id;
    private String NameInEnglish;
    private String NameInArabic;
    private String ImageURL;
    private String Gender;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDressTypeInEnglish() {
        return NameInEnglish == null ? "" : NameInEnglish;
    }

    public void setDressTypeInEnglish(String dressTypeInEnglish) {
        NameInEnglish = dressTypeInEnglish;
    }

    public String getDressTypeInArabic() {
        return NameInArabic == null ? "" : NameInArabic;
    }

    public void setDressTypeInArabic(String dressTypeInArabic) {
        NameInArabic = dressTypeInArabic;
    }

    public String getImageURL() {
        return ImageURL == null ? "" : ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getGender() {
        return Gender == null ? "" : Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }
    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

}
