package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class CustomizationImagesEntity implements Serializable {
    public int Id;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String Image;

}
