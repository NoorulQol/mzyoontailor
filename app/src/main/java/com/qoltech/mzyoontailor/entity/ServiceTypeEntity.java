package com.qoltech.mzyoontailor.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceTypeEntity implements Serializable {
    public int Id;
    public String DeliveryTypeInEnglish;
    public String DeliveryTypeInArabic;
    public String HeaderImage;
    public String DeliveryImage;
    public boolean Status;
    public boolean isStatus() {
        return Status;
    }

    public void setStatus(boolean status) {
        Status = status;
    }

    public Boolean get_switch() {
        return _switch;
    }

    public void set_switch(Boolean _switch) {
        this._switch = _switch;
    }

    @SerializedName("Switch")
    @Expose
    private Boolean _switch;
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getDeliveryTypeInEnglish() {
        return DeliveryTypeInEnglish == null ? "" : DeliveryTypeInEnglish;
    }

    public void setDeliveryTypeInEnglish(String deliveryTypeInEnglish) {
        DeliveryTypeInEnglish = deliveryTypeInEnglish;
    }

    public String getDeliveryTypeInArabic() {
        return DeliveryTypeInArabic == null ? "" : DeliveryTypeInArabic;
    }

    public void setDeliveryTypeInArabic(String deliveryTypeInArabic) {
        DeliveryTypeInArabic = deliveryTypeInArabic;
    }

    public String getHeaderImage() {
        return HeaderImage == null ? "" : HeaderImage;
    }

    public void setHeaderImage(String headerImage) {
        HeaderImage = headerImage;
    }

    public String getDeliveryImage() {
        return DeliveryImage == null ? "" : DeliveryImage;
    }

    public void setDeliveryImage(String deliveryImage) {
        DeliveryImage = deliveryImage;
    }
    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }
}
