package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class GetTrackingEntity implements Serializable {
    public int Id;
    public String Status;
    public String Date;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getStatus() {
        return Status == null ? "" : Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return Date == null ? "" : Date;
    }

    public void setDate(String date) {
        Date = date;
    }

}
