package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class InsertAttributeApiCallEntity implements Serializable {
    public String AttributeId;
    public String AttributeTypeId;
    public String Quantity;

    public String getAttributeId() {
        return AttributeId == null ? "" : AttributeId;
    }

    public void setAttributeId(String attributeId) {
        AttributeId = attributeId;
    }

    public String getAttributeTypeId() {
        return AttributeTypeId == null ? "" : AttributeTypeId;
    }

    public void setAttributeTypeId(String attributeTypeId) {
        AttributeTypeId = attributeTypeId;
    }

    public String getQuantity() {
        return Quantity == null ? "" : Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

}
