package com.qoltech.mzyoontailor.entity;

import java.io.Serializable;

public class CustomizationSessionEntity implements Serializable {

    public int Id;
    public String SeasonInEnglish;
    public String SeasonInArabic;
    public String Image;
    private boolean isChecked;

    public boolean getChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getSeasonInEnglish() {
        return SeasonInEnglish == null ? "" : SeasonInEnglish;
    }

    public void setSeasonInEnglish(String seasonInEnglish) {
        SeasonInEnglish = seasonInEnglish;
    }

    public String getSeasonInArabic() {
        return SeasonInArabic == null ? "" : SeasonInArabic;
    }

    public void setSeasonInArabic(String seasonInArabic) {
        SeasonInArabic = seasonInArabic;
    }

    public String getImage() {
        return Image == null ? "" : Image;
    }

    public void setImage(String image) {
        Image = image;
    }


}
