package com.qoltech.mzyoontailor.services;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NetworkUpdateReceiver extends BroadcastReceiver {

    Context context;

    @SuppressLint({"UnsafeProtectedBroadcastReceiver", "StaticFieldLeak"})
    public void onReceive(final Context context, Intent intent) {
        this.context = context;
    }
}