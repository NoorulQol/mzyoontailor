package com.qoltech.mzyoontailor.cropview.callback;

public interface LoadCallback extends Callback {
  void onSuccess();
}
