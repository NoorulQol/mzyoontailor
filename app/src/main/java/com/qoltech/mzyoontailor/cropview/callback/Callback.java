package com.qoltech.mzyoontailor.cropview.callback;

public interface Callback {
  void onError(Throwable e);
}
