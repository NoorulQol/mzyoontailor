package com.qoltech.mzyoontailor.util;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.qoltech.mzyoontailor.fragments.MaterialUsedFragment;
import com.qoltech.mzyoontailor.fragments.PersonalInfoFragment;
import com.qoltech.mzyoontailor.ui.AppointmentDetails;
import com.qoltech.mzyoontailor.ui.ScheduletDetailScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    SimpleDateFormat dateFormat;
    public static Calendar calendar;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, yy, mm, dd);
//        return new DatePickerDialog(getActivity(), this, yy, mm, dd);

        datePickerDialog.getListView();
        if (AppConstants.CALENDER == "personal_info") {
//        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        }
        else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_FROM")){

            if ( AppConstants.DATE_FOR_RESTRICT_MATERIAL.equalsIgnoreCase("")){
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }else {
                String dateString = AppConstants.DATE_FOR_RESTRICT_MATERIAL.replace(" ","/");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        } else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_TO")){

            String dateString = AppConstants.DATE_FOR_RESTRICT_MATERIAL.replace(" ","/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
            Date date = null;
            try {
                date = sdf.parse(dateString);
                long startDate = date.getTime();
                datePickerDialog.getDatePicker().setMinDate(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_FROM")){
            if (AppConstants.DATE_FOR_RESTRICT_MEASUREMENT.equalsIgnoreCase("")){
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }else {
                String dateString = AppConstants.DATE_FOR_RESTRICT_MEASUREMENT.replace(" ","/");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_TO")){

            String dateString = AppConstants.DATE_FOR_RESTRICT_MEASUREMENT.replace(" ","/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
            Date date = null;
            try {
                date = sdf.parse(dateString);
                long startDate = date.getTime();
                datePickerDialog.getDatePicker().setMinDate(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_FROM")){
            if (AppConstants.DATE_FOR_RESTRICT_SCHEDULE.equalsIgnoreCase("")){

                Date currentDate = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(currentDate);
                c.add(Calendar.DATE, Integer.parseInt(AppConstants.NO_OF_DAYS_FOR_SCHEDULE));

                Date currentDatePlusOne = c.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
                Date date = null;
                try {
                    date = sdf.parse(sdf.format(currentDatePlusOne));
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

//                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            }else {
                String dateString = AppConstants.DATE_FOR_RESTRICT_SCHEDULE.replace(" ","/");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                    long startDate = date.getTime();
                    datePickerDialog.getDatePicker().setMinDate(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_TO")){


            String dateString = AppConstants.DATE_FOR_RESTRICT_SCHEDULE.replace(" ","/");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
            Date date = null;
            try {
                date = sdf.parse(dateString);
                long startDate = date.getTime();
                datePickerDialog.getDatePicker().setMinDate(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
        return datePickerDialog;
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
//        populateSetDate(yy, mm + 1, dd);
        Calendar calendar = Calendar.getInstance();
        calendar.set(yy, mm, dd);

        dateFormat = new SimpleDateFormat("dd/MMM/yyyy");

        String dateString = dateFormat.format(calendar.getTime());

//        TextView textview = (TextView)getActivity().findViewById(R.id.textView1);
//        textview.setText(dateString);


        if (AppConstants.CALENDER == "material_used") {
            MaterialUsedFragment.calender_text.setText(dateString);
        } else if (AppConstants.CALENDER == "personal_info") {
            PersonalInfoFragment.dob.setText(dateString);
        } else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_FROM")){
            ((AppointmentDetails)getActivity()).mMaterialTypeFromDateTxt.setText(dateString);
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MATERIAL_TO")){
            ((AppointmentDetails)getActivity()).mMaterialTypeToDateTxt.setText(dateString);
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_FROM")){
            ((AppointmentDetails)getActivity()).mMeasurementTypeFromDateTxt.setText(dateString);
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_MEASUREMENT_TO")){
            ((AppointmentDetails)getActivity()).mMeasurementTypeToDateTxt.setText(dateString);
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_FROM")){
            ((ScheduletDetailScreen)getActivity()).mScheduleTypeFromDateTxt.setText(dateString);
        }else if (AppConstants.DATE_PICKER_COND.equalsIgnoreCase("APPOINTMENT_SCHEDULE_TO")){
            ((ScheduletDetailScreen)getActivity()).mScheduleTypeToDateTxt.setText(dateString);
        }
    }

//    public void populateSetDate(int year, int month, int day) {
////        PersonalInfoFragment.dob.setText(day + "/" + month + "/" + year);
//
//        if (month == 1) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Jan" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Jan" + "/" + year);
//            }
//
//        } else if (month == 2) {
//
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Feb" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Feb" + "/" + year);
//            }
//
//        } else if (month == 3) {
//
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Mar" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Mar" + "/" + year);
//            }
//
//        } else if (month == 4) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Apr" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Apr" + "/" + year);
//            }
//
//
//        } else if (month == 5) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "May" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "May" + "/" + year);
//            }
//
//
//        } else if (month == 6) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Jun" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Jun" + "/" + year);
//            }
//
//
//        } else if (month == 7) {
//
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Jul" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Jul" + "/" + year);
//            }
//
//
//        } else if (month == 8) {
//
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Aug" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Aug" + "/" + year);
//            }
//
//        } else if (month == 9) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Sep" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Sep" + "/" + year);
//            }
//
//        } else if (month == 10) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Oct" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Oct" + "/" + year);
//            }
//
//        } else if (month == 11) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Nov" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Nov" + "/" + year);
//            }
//
//        } else if (month == 12) {
//            if (AppConstants.CALENDER == "material_used") {
//                MaterialUsedFragment.calender_text.setText(day + "/" + "Dec" + "/" + year);
//            } else if (AppConstants.CALENDER == "personal_info") {
//                PersonalInfoFragment.dob.setText(day + "/" + "Dec" + "/" + year);
//            }
//
//        }
//
//    }

}