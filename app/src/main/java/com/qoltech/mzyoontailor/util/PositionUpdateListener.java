package com.qoltech.mzyoontailor.util;


import com.qoltech.mzyoontailor.wrappers.Country;

import java.io.Serializable;

public interface PositionUpdateListener extends Serializable {
    void onSelectCountry(Country country);
    // void onPositionUpdate(View v,int position);
    // public void onClick(String item,int position);

    void onPositionUpdate(int position);

}