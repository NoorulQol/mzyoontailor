package com.qoltech.mzyoontailor.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class GlobalData {
    //    http://appsapi.mzyoon.com/api/Order/GetTailorlist
//    http://192.168.0.29/TailorAPI/images/BuyerImagesCustomer_1.jpg
    public static String SERVER_URL = "http://appsapi.mzyoon.com/";
    public static Toast toast;
    public static boolean canLogout = false;

    @SuppressLint("ShowToast")
    public static Toast showToast(Context context, String content, int length) {

        try {
            toast.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }

        toast = Toast.makeText(context, content, length);
        toast.setGravity(Gravity.CENTER, 0, 0);
        return toast;
    }
}