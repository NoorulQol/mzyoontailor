package com.qoltech.mzyoontailor.util;

import android.support.multidex.MultiDexApplication;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainApplication extends MultiDexApplication {

    Retrofit restClient;

    @Override
    public void onCreate() {
        super.onCreate();


        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();

        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        Fabric.with(this, new Crashlytics());
        restClient = new Retrofit.Builder()
                .baseUrl(GlobalData.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .readTimeout(1000, TimeUnit.SECONDS)
                        .connectTimeout(1000, TimeUnit.SECONDS)
                        .writeTimeout(1000, TimeUnit.SECONDS)
                        .addInterceptor(httpLoggingInterceptor)
                        .addInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request request = chain.request();
                                HttpUrl url = request.url().newBuilder().build();
                                request = request.newBuilder().url(url).build();
                                return chain.proceed(request);
                            }
                        })
                        .build())
                .build();
    }

    public void addAccessToken(final String accessToken) {
        restClient = new Retrofit.Builder()
                .baseUrl(GlobalData.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .readTimeout(6000, TimeUnit.SECONDS)
                        .connectTimeout(6000, TimeUnit.SECONDS)
                        .writeTimeout(6000, TimeUnit.SECONDS)
                        .addInterceptor(new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request.Builder ongoing = chain.request().newBuilder();
                                ongoing.header("Authorization", accessToken);
                                ongoing.header("Accept", "application/json");
                                ongoing.method(chain.request().method(), chain.request().body());
                                return chain.proceed(ongoing.build());
                            }
                        })
                        .build())
                .build();
    }

    public ApiService getClient() {
        return restClient.create(ApiService.class);

    }



}
