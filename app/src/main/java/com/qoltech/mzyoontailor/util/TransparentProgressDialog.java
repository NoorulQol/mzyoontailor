package com.qoltech.mzyoontailor.util;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.qoltech.mzyoontailor.R;


public class TransparentProgressDialog extends Dialog {

    private RotateLoading rotateLoading;

    public TransparentProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);

        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);

        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        rotateLoading = new RotateLoading(context);
        layout.addView(
                rotateLoading,
                new LinearLayout.LayoutParams((int) context.getResources()
                        .getDimension(R.dimen.buttonHeightLarge), (int) context
                        .getResources().getDimension(R.dimen.buttonHeightLarge)));
        addContentView(layout, params);
    }

    @Override
    public void show() {
        super.show();

        if (rotateLoading.isStart()) {
            rotateLoading.stop();
        } else {
            rotateLoading.start();
        }
    }

}