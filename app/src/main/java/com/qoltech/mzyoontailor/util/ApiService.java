package com.qoltech.mzyoontailor.util;

import com.qoltech.mzyoontailor.modal.BuyerOrderDetailResponse;
import com.qoltech.mzyoontailor.modal.DeleteShopImageResponse;
import com.qoltech.mzyoontailor.modal.DressTypeResponse;
import com.qoltech.mzyoontailor.modal.GenderResponse;
import com.qoltech.mzyoontailor.modal.GetAreaResponse;
import com.qoltech.mzyoontailor.modal.GetAttributeIdToSaveLocalModal;
import com.qoltech.mzyoontailor.modal.GetCustomizationThreeResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationsByTailorIdSkillUpdateResponse;
import com.qoltech.mzyoontailor.modal.GetLanguageResponse;
import com.qoltech.mzyoontailor.modal.GetMaterialChargesByTailorResponse;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.modal.GetOrderQuotationPendingModal;
import com.qoltech.mzyoontailor.modal.GetTailorDefaultChargesResponse;
import com.qoltech.mzyoontailor.modal.InsertAttributeQuantityApicallModal;
import com.qoltech.mzyoontailor.modal.InsertCustumizationSkillUpdateModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementSkillUpdateApiModal;
import com.qoltech.mzyoontailor.modal.InsertOrderTypeSkillUpdateModal;
import com.qoltech.mzyoontailor.modal.InsertServiceLocationSkillUpdateModal;
import com.qoltech.mzyoontailor.modal.InsertServiceTypeSkillUpdateModal;
import com.qoltech.mzyoontailor.modal.InsertUpdateShopResponse;
import com.qoltech.mzyoontailor.modal.MaterialByIdGetRespone;
import com.qoltech.mzyoontailor.modal.MeasurementOneResponse;
import com.qoltech.mzyoontailor.modal.OrderSummaryResponse;
import com.qoltech.mzyoontailor.modal.OrderTypeResponse;
import com.qoltech.mzyoontailor.modal.ServiceTypeResponse;
import com.qoltech.mzyoontailor.modal.SkillUpdateGetLocationModal;
import com.qoltech.mzyoontailor.modal.SkillUpdateResponse;
import com.qoltech.mzyoontailor.modal.SubTypeResponse;
import com.qoltech.mzyoontailor.wrappers.BuyerRequestApprovedResponse;
import com.qoltech.mzyoontailor.wrappers.BuyerRequestPendingResponse;
import com.qoltech.mzyoontailor.wrappers.CountryResponse;
import com.qoltech.mzyoontailor.wrappers.CustomerTypeResponse;
import com.qoltech.mzyoontailor.wrappers.CustumizationSkillUpdateGetAttributeResponse;
import com.qoltech.mzyoontailor.wrappers.DashBoardHomeResponse;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResponse;
import com.qoltech.mzyoontailor.wrappers.GetMeasurementDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.GetOrderDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.GetOrderNotDeliverdResponse;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsResponse;
import com.qoltech.mzyoontailor.wrappers.GetShopProfileResponse;
import com.qoltech.mzyoontailor.wrappers.GetTailorChargesResponse;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.ImageResponse;
import com.qoltech.mzyoontailor.wrappers.MaterialMappingGetResposne;
import com.qoltech.mzyoontailor.wrappers.OldCustomerRespone;
import com.qoltech.mzyoontailor.wrappers.ProfileImageResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.StateResponse;
import com.qoltech.mzyoontailor.wrappers.TailorProfileGetResponse;
import com.qoltech.mzyoontailor.wrappers.ValidationResponse;
import com.qoltech.mzyoontailor.wrappers.WelcomeMessageNewUserResponse;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {
    @GET("api/login/getallcountries")
    Call<CountryResponse> getCountry();


    @POST("api/login/GenerateOTP")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> sentOtp(@FieldMap HashMap<String, String> object);

    @POST("api/login/Validateotp")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<ValidationResponse> OtpValidation(@FieldMap HashMap<String, String> object);

    @GET("Api/Dashboard/GetDashboardValuesForTailor?")
    Call<DashBoardHomeResponse> getDashboardData(@Query("TailorId") String tailorId);

    //    http://192.168.0.21/TailorAPI/Api/Shop/UploadFile     http://192.168.0.21/TailorAPI/Api/Shop/UploadFile
    @Multipart
    @POST("Api/FileUpload/UploadFile")
    Call<ProfileImageResponse> imageUpload(@Part MultipartBody.Part image);

    @POST("Api/Shop/InsertUpdateShopProfile")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<ProfileImageResponse> shopdetailsUpload(@FieldMap HashMap<String, String> object);

    @Multipart
    @POST("Api/FileUpload/UploadFile")
    @Headers("Accept: */*")
    Call<SignUpResponse> imageUpload1(@Part MultipartBody.Part image);


    @Multipart
    @POST("Api/FileUpload/UploadFile")
    @Headers("Accept: */*")
    Call<ImageResponse> imageUpload2(@Part MultipartBody.Part image);

    //    Api/Shop/InsertUpdateTailorProfile
    @POST("API/Shop/InsertUpdateShopProfile")
    Call<SignUpResponse> shopdetailsUpload1(@Body InsertUpdateShopResponse insertUpdateShopResponse);


//    http://192.168.0.21/tailorapi/Api/Shop/DisplayStatebyCountry


    @POST("Api/Shop/DisplayStatebyCountry")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<StateResponse> stateList(@FieldMap HashMap<String, String> object);


    @POST("Api/Shop/InsertUpdateTailorProfile")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> tailorProfileUpload(@FieldMap HashMap<String, String> object);


//    @GET("api/order/getgenders")
//    Call<GenderResponse> getGender();
//
//    @GET("api/order/getdresstypebygender?")
//    Call<DressTypeResponse> getDressTypes(@Query("genderId") String genderId);

//
//    @GET("Api/Order/DisplayMeasurement1")
//    Call<MeasurementOneResponse> getMeasurementOne();
//
//    @GET("Api/Order/DisplayOrderType")
//    Call<OrderTypeResponse> getOrderType();

    @Multipart
    @POST("Api/FileUpload/UploadFile")
//    Call<SignUpResponse> uploadSurvey(@Part MultipartBody.Part[] surveyImage, @Part MultipartBody.Part propertyImage, @Part("DRA") RequestBody dra);
    Call<SignUpResponse> upImageMany(@Part MultipartBody.Part[] multipartTypedOutput);


    @Multipart
    @POST("Api/FileUpload/UploadFile")
    @Headers("Accept: */*")
//    Call<SignUpResponse> uploadSurvey(@Part MultipartBody.Part[] surveyImage, @Part MultipartBody.Part propertyImage, @Part("DRA") RequestBody dra);
    Call<ImageResponse> upImageManys(@Part MultipartBody.Part[] multipartTypedOutput);

    @GET()
    Call<SignUpResponse> validateCustomer(@Url String queryUrl);

    @GET("Api/Shop/GetTailorProfileByTailorId?")
    Call<TailorProfileGetResponse> getTailorProfile(@Query("TailorId") String genderId);

    @GET("Api/Shop/GetApprovedCustomer?")
    Call<BuyerRequestApprovedResponse> getBuyerRequestApproved(@Query("TailorId") String tailorId);

    //    http://192.168.0.21/TailorAPI/Api/Shop/GetPendingCustomer?TailorId=4
    @GET("Api/Shop/GetPendingCustomer?")
    Call<BuyerRequestPendingResponse> getBuyerRequestPending(@Query("TailorId") String tailorId);

//    Api/Shop/IsNewCustomer?PhoneNumber=52134685

    @GET("Api/Shop/IsNewCustomer?")
    Call<CustomerTypeResponse> getCustomerType(@Query("CountryCode") String countryCode, @Query("PhoneNumber") String phoneNumber);


    @GET("Api/Shop/GetExistingUserInTailor?")
    Call<OldCustomerRespone> getOldCustomer(@Query("CountryCode") String countryCode, @Query("PhoneNumber") String phoneNumber);


    @GET("Api/Shop/GetOrderDetails?")
    Call<GetBuyerRequestResponse> getCustomizationType(@Query("OrderId") String orderId);


    @GET("Api/Shop/GetRequestMeasurementParts?")
    Call<GetRequestMeasurementPartsResponse> getRequestMeasurementParts(@Query("OrderId") String orderId);

    @GET("Api/Shop/GetMeasurementDetails?")
    Call<GetMeasurementDetailsResponse> getMeasurementDetails(@Query("OrderId") String orderId);


    @POST("Api/Shop/InsertTailorResponseAndCharges")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> orderQuotation(@FieldMap HashMap<String, String> object);
//    http://appsapi.mzyoon.com/Api/Shop/InsertTailorResponseAndCharges
//    InsertTailorResponseAndChargesTailorOrder
    @POST("Api/Shop/InsertTailorResponseAndChargesTailorOrder")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> orderQuotationTailorOrder(@FieldMap HashMap<String, String> object);


    @GET("Api/Shop/GetShopProfileByTailorId?")
    Call<GetShopProfileResponse> getShopProfile(@Query("TailorId") String orderId);

    @GET("Api/Shop/DeleteShopImages?")
    Call<DeleteShopImageResponse> deleteShopProfileImg(@Query("Id") String Id);

    @POST("api/Login/InsertUpdateDeviceDetails")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> deviceDetails(@FieldMap HashMap<String, String> object);

//    http://appsapi.mzyoon.com/api/Shop/UpdateTailorImages

    @POST("api/Shop/UpdateTailorImages")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> profileIntroScreen(@FieldMap HashMap<String, String> object);

    //    http://192.168.0.21/TailorAPI/Api/Login/IsProfileCreated?UserType=Tailor&id=102
    @GET("Api/Login/IsProfileCreated?UserType=Tailor&?")
    Call<SignUpResponse> IsprofileCreated(@Query("id") String orderId);
//    http://appsapi.mzyoon.com/api/Order/InsertOrder


    @POST("api/Order/InsertOrder")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> orderSummary(@FieldMap HashMap<String, String> object);


    //    http://appsapi.mzyoon.com/api/Shop/WelcomeMessageToNewCustomer?UserName=saleem&PhoneNo=919965614920
//    http://192.168.0.21/TailorAPI/Api/Shop/WelcomeMessageToNewCustomer

    @POST("Api/Shop/WelcomeMessageToNewCustomer")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<WelcomeMessageNewUserResponse> WelcomeMessageToNewCustomer(@FieldMap HashMap<String, String> object);

    //    http://192.168.0.21/TailorAPI/Api/Login/IsProfileCreated?UserType=Tailor&id=102
    @GET("Api/Order/GetOrderDetails?")
    Call<GetOrderDetailsResponse> getOrderDetailsResponse(@Query("OrderId") String orderId);

//    Order Details For Tailor

    @GET("Api/Order/GetOrderDetailsByTailor?")
    Call<GetOrderDetailsResponseCheck> getOrderDetailsResponseForTailor(@Query("OrderId")
                                                                                String orderId, @Query("Type") String s,
                                                                        @Query("OrderType") String o);


    //    http://192.168.0.21/TailorAPI/ API/shop/GetTrackingDetails
    @GET("API/shop/GetTrackingDetails?")
    Call<GetTrackingDetailsResponse> getTrackingDetailsResponse(@Query("OrderId") String orderId);

    //    http://192.168.0.21/TailorAPI/API/Order/GetGenders
    @GET("API/Order/GetGenders")
    Call<GenderResponse> getGenders();

    //    http://192.168.0.21/TailorAPI/API/Order/GetGenders
    @GET("api/Order/GetDressTypeByGender?")
    Call<DressTypeResponse> getDressType(@Query("genderId") String orderId);


    //   http://192.168.0.21/TailorAPI/API/order/DisplayDressSubType?Id=1
    @GET("API/order/DisplayDressSubType?")
    Call<SubTypeResponse> getDressSubType(@Query("Id") String orderId);


    //   http://192.168.0.21/TailorAPI/API/Order/DisplayOrderType
    @GET("API/Order/DisplayOrderType")
    Call<OrderTypeResponse> getOrderType();

    @GET("Api/Order/DisplayMeasurement1")
    Call<MeasurementOneResponse> getMeasurementOne();


    @GET("api/Shop/GetServiceType")
    Call<ServiceTypeResponse> getServiceType();
//

//    http://192.168.0.21/TailorAPi/Api/Shop/InsertAttributeQuantity


    @POST("api/Shop/InsertAttributeQuantity")
//    @Headers("Accept: application/json")
//    @FormUrlEncoded
    Call<SignUpResponse> insertAttributeQuantity(@Body InsertAttributeQuantityApicallModal attributeQuantityApicallModal);

    //    http://appsapi.mzyoon.com/Api/Login/GetAllLanguages
    @GET("Api/Login/GetAllLanguages")
    Call<GetLanguageResponse> getLanguagesApi();


    @POST("Api/shop/TailorOrderApproved")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> approveOrder(@FieldMap HashMap<String, String> object);


//    http://192.168.0.21/TailorAPI/API/shop/InsertTrackingStatus

    @POST("API/shop/InsertTrackingStatus")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateTracking(@FieldMap HashMap<String, String> object);


//    http://192.168.0.26/TailorAPi/Api/Shop/GetOrderNotDeliverd?TailorId=198


    @GET("Api/Shop/GetOrderNotDeliverd?")
    Call<GetOrderNotDeliverdResponse> getOrderNotDelivered(@Query("TailorId") String orderId);

//    http://192.168.0.26/TailorAPi/Api/Shop/GetOrderDeliverd?TailorId=198

    @GET("Api/Shop/GetOrderDeliverd?")
    Call<GetOrderNotDeliverdResponse> getOrderDelivered(@Query("TailorId") String orderId);

//    http://192.168.0.26/TailorAPI/Api/Shop/UpdateDeliveryStatus

    @POST("API/shop/UpdateDeliveryStatus")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> UpdateDeliveryStatus(@Field("OrderId") String OrderId, @Field("IsComplited") boolean IsComplited);


    @POST("api/Shop/InsertAttributeQuantity")
//    @Headers("Accept: application/json")
//    @FormUrlEncoded
    Call<SignUpResponse> getDressTypeByMultipleGender(@Body InsertAttributeQuantityApicallModal attributeQuantityApicallModal);


    @GET("Api/Login/GetAllLanguages")
    Call<GetLanguageResponse> getLanguage();


    //    http://192.168.0.26/TailorAPI/Api/shop/InsertUserByTailor
    @POST("Api/shop/InsertUserByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateNewUserDetails(@FieldMap HashMap<String, String> object);


    //    http://192.168.0.26/TailorAPi/Api/shop/GenderByTailorId?TailorId=1
    @GET("Api/shop/GenderByTailorId?")
    Call<GenderResponse> getGenderSkillUpdate(@Query("TailorId") String phoneNumber);

    //    http://192.168.0.26/TailorAPi/Api/shop/GetDressTypeByTailorId?TailorId=1
    @GET("Api/shop/GetDressTypeByTailorId?")
    Call<DressTypeResponse> getDressTypeSkillUpdate(@Query("TailorId") String orderId);

    //  http://192.168.0.26/TailorAPi/Api/shop/GetDressSubTypeByTailorId?TailorId=1
    @GET("API/shop/GetDressSubTypeByTailorId?")
    Call<SubTypeResponse> getDressSubTypeSkillUpdate(@Query("TailorId") String orderId); //  http://192.168.0.26/TailorAPi/Api/shop/GetDressSubTypeByTailorId?TailorId=1

    @GET("API/shop/GetOrderTypeByTailorId?")
    Call<OrderTypeResponse> getOrderTypeSkillUpdate(@Query("TailorId") String orderId); //  http://192.168.0.26/TailorAPi/Api/shop/GetDressSubTypeByTailorId?TailorId=1

    @GET("API/shop/GetMeasurementsByTailorId?")
    Call<MeasurementOneResponse> getMeasurementTypeSkillUpdate(@Query("TailorId") String orderId); //  http://192.168.0.26/TailorAPi/Api/shop/GetDressSubTypeByTailorId?TailorId=1

    @GET("API/shop/GetAppoinmentTypeByTailorId?")
    Call<ServiceTypeResponse> getAppointmentTypeSkillUpdate(@Query("TailorId") String orderId);

    //    http://192.168.0.26/TailorAPi/api/shop/UpdateTrackingStatusSwitch
    @POST("api/shop/UpdateTrackingStatusSwitch")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateTrackingRemember(@FieldMap HashMap<String, String> object);

//    http://192.168.0.26/TailorAPI/Api/shop/InsertBuyerAddress

    @POST("Api/shop/InsertBuyerAddressByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateDirectCustomerAddress(@FieldMap HashMap<String, String> object);


    @POST("API/Shop/GetAreaByState")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<GetAreaResponse> getAreaApi(@FieldMap HashMap<String, String> object);


    @GET("API/shop/getServiceLocationByTailorId?")
    @Headers("Accept: application/json")
    Call<GetAreaResponse> getAreaApiSkillUpdate(@Query("TailorId") String orderId, @Query("StateId") String stateID);


//    @GET("API/shop/GetAppoinmentTypeByTailorId?")
//    Call<ServiceTypeResponse> getAppointmentTypeSkillUpdate(@Query("TailorId") String orderId);


//    http://192.168.0.26/TailorAPI/Api/shop/InsertTailorAmount

    @POST("API/shop/InsertTailorAmount")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> insertDirectCustomerAmout(@FieldMap HashMap<String, String> object);

//    http://192.168.0.26/TailorAPi/api/shop/UpdateTrackingStatusSwitch

    @POST("API/Order/UpatePaymentStatus")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updatePaymentStatus(@FieldMap HashMap<String, String> object);
//    http://appsapi.mzyoon.com/Api/Login/IsProfileCreated?UserType=Tailor&?&id=314

    @POST("API/Order/BuyerOrderApproval")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> buyerOrderApproval(@FieldMap HashMap<String, String> object);

//    http://192.168.0.21/TailorAPI/Api/Order/InsertUserMeasurementValues

    @POST("API/Order/InsertUserMeasurementValues")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> insertUserMeasurementValues(@FieldMap HashMap<String, String> object);


    @POST("API/Shop/InsertUpdateShopProfile")
    Call<SignUpResponse> insertGotoTailorShopMeasurement(@Body InsertUpdateShopResponse insertUpdateShopResponse);


    //http://192.168.0.26/TailorAPI/Api/Order/GetOrderDetailsByTailor?&OrderId=84
//    http://192.168.0.26/TailorAPi/Api/shop/GetCustomizationsByTailorId?TailorId=1
    @GET("API/shop/GetCustomizationsByTailorId?")
    Call<GetCustomizationThreeResponse> getCustomizationByTailorId();

    @GET("Api/shop/GetServiceLocationByTailorId?")
    Call<SkillUpdateGetLocationModal> getLocationSkillUpdate(@Query("TailorId") String phoneNumber);

    /*Insert Order Summary*/
    @POST("Api/Shop/InsertTailorSkills")
    Call<OrderSummaryResponse> insertSkillUpdate(@Body SkillUpdateResponse skillUpdateResponse);

//    http://192.168.0.26/TailorAPI/Api/shop/UpdateMeasurementByTailor

    @POST("API/shop/UpdateMeasurementByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateMeasurementByTailor(@FieldMap HashMap<String, String> object);

//    http://192.168.0.26/TailorApi/Api/shop/UpdateGenderSwitch

    @POST("API/shop/InsertGenderSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> insertGenderSkillsByTailor(@FieldMap HashMap<String, String> object);


    @POST("API/shop/UpdateGenderSwitch")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateGenderSwitch(@FieldMap HashMap<String, String> object);


    @POST("API/shop/InsertDressTypeSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertDressTypeSkillsByTailor(@FieldMap HashMap<String, String> object);


    @POST("API/shop/InsertDressSubTypeSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertDressSubTypeSkillsByTailor(@FieldMap HashMap<String, String> object);


    @POST("API/shop/InsertOrderTypeSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertOrderTypeSkillsByTailor(@FieldMap HashMap<String, String> object);


    @POST("API/shop/InsertMeasurementSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertMeasurementSkillsByTailor(@FieldMap HashMap<String, String> object);


    @POST("API/shop/InsertAppoinmentSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertAppoinmentSkillsByTailor(@FieldMap HashMap<String, String> object);

//


    @POST("API/shop/InsertServiceSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertServiceSkillsByTailor(@FieldMap HashMap<String, String> object);


//    http://192.168.0.26/TailorAPI/API/shop/DeleteTailorSkillsByGender?TailorId=1


    @GET("Api/shop/DeleteTailorSkillsByGender?")
    Call<SkillUpdateGetLocationModal> deleteSkillsGender(@Query("TailorId") String phoneNumber);

//    http://192.168.0.26/TailorAPI/API/shop/GetAttributeImages?CustomizationAttributeId=1


    @GET("Api/shop/GetAttributeImages?")
    Call<SkillUpdateGetLocationModal> getCustumizationAttribute(@Query("CustomizationAttributeId") String phoneNumber);

    //    http://192.198.0.29/TailorAPI/API/shop/DeleteTailorSkillsByDressType?TailorId=1
    @GET("Api/shop/DeleteTailorSkillsByDressType?")
    Call<SkillUpdateGetLocationModal> deleteSkillsdressType(@Query("TailorId") String phoneNumber);


    //    http://192.198.0.29/TailorAPI/API/shop/DeleteTailorSkillsByDressSubtype?TailorId=1
    @GET("Api/shop/DeleteTailorSkillsByDressSubtype?")
    Call<SkillUpdateGetLocationModal> deleteSkillsSubdressType(@Query("TailorId") String phoneNumber);


    //    http://192.198.0.29/TailorAPI/API/shop/DeleteTailorSkillsByOrderType?TailorId=1
    @GET("Api/shop/DeleteTailorSkillsByOrderType?")
    Call<SkillUpdateGetLocationModal> deleteSkillsOrderType(@Query("TailorId") String phoneNumber);

//    http://192.168.0.29/TailorAPi/Api/shop/GetAttributesByTailorId?AttributeId=1&TailorId=10


//    http://192.198.0.29/TailorAPI/API/shop/InsertCustomizationSkillsByTailor

    @POST("API/shop/InsertCustomizationSkillsByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertCustomizationSkillsByTailor(@FieldMap HashMap<String, String> object);


    //    http://192.168.0.29/TailorAPI/API/shop/GetTailorChargesStatus?OrderId=1
    @GET("Api/shop/GetTailorChargesStatus?")
    Call<GetTailorChargesResponse> getTailorChargesStatus(@Query("OrderId") String phoneNumber);
//api/Order/GetDressTypeByGender?genderId=

    @GET("Api/Order/GetDressTypeByGender?")
    Call<DressTypeResponse> getDressTypeByGenderId(@Query("genderId") String phoneNumber);

    //api/order/DisplayDressSubType?Id=
    @GET("Api/Order/DisplayDressSubType?")
    Call<SubTypeResponse> getSubDressTypeByGenderId(@Query("Id") String phoneNumber);

    //api/order/DisplayDressSubType?Id=
    @GET("Api/shop/GetPatterns?")
    Call<MaterialMappingGetResposne> getMaterialMappingList();


    //    http://192.168.0.29/TailorAPI/Api/shop/GetMaterialDetails?MaterialId=72
    @GET("Api/shop/GetMaterialDetails?")
    Call<MaterialByIdGetRespone> getMaterialDetails(@Query("TailorId") String tailorId, @Query("MaterialId") String phoneNumber);

//    http://192.168.0.29/TailorAPI/Api/shop/InsertMaterialMapping

    @POST("API/shop/InsertMaterialMapping")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> InsertMaterialMapping(@FieldMap HashMap<String, String> object);


//    @POST("API/shop/InsertMaterialMapping")
//    @Headers("Accept: application/json")
//    @FormUrlEncoded
//    Call<SignUpResponse> InsertMaterialMapping(@Field("TailorId") String id,@Field("FriendlyNameInEnglish")
//            String nameEng,@Field("FriendlyNameInArabic") String nameAra, @FieldMap HashMap<String, I> object);


//

    //    http://192.168.0.29/TailorAPI/Api/shop/GetCustomizationsByTailorId?TailorId=1&DressSubTypeId=1
    @GET("Api/shop/GetCustomizationsByTailorId?")
    Call<GetCustomizationsByTailorIdSkillUpdateResponse>
    getCustumizationByTailorId(@Query("TailorId") String tailorId, @Query("DressSubTypeId") String dressSubTypeId);


//    http://192.168.0.29/TailorAPi/Api/Order/GetAttributesByAttributeId?AttributeId=2


//    http://192.168.0.29/TailorAPi/Api/shop/GetAttributesByTailorId?AttributeId=1&TailorId=10

    @GET("Api/shop/GetAttributesByTailorId?")
    Call<CustumizationSkillUpdateGetAttributeResponse>
    getSkillUpdateCustumizationAttribute(@Query("AttributeId") String attributeId,
                                         @Query("TailorId") String phoneNumber, @Query("DressSubTypeId") String s);


//    http://192.198.0.29/TailorApi/Api/shop/InsertMeasurementSkillsByTailor

    @POST("api/Shop/InsertMeasurementSkillsByTailor")
//    @Headers("Accept: application/json")
//    @FormUrlEncoded
    Call<SignUpResponse> insertMeasurementSkillUpdate(@Body InsertMeasurementSkillUpdateApiModal insertMeasurementSkillUpdateApiModal);

//    http://192.198.0.29/TailorApi/Api/shop/InsertOrderTypeSkillsByTailor

    @POST("api/Shop/InsertOrderTypeSkillsByTailor")
//    @Headers("Accept: application/json")
//    @FormUrlEncoded
    Call<SignUpResponse> insertOrderTypeSkillUpdate(@Body InsertOrderTypeSkillUpdateModal insertOrderTypeSkillUpdateModal);


//    http://192.198.0.29/TailorApi/Api/shop/InsertAppoinmentSkillsByTailor

    @POST("api/Shop/InsertAppoinmentSkillsByTailor")
    Call<SignUpResponse> insertServiceTypeSkillUpdate(@Body InsertServiceTypeSkillUpdateModal insertServiceTypeSkillUpdateModal);

    @POST("api/Shop/InsertServiceSkillsByTailor")
    Call<SignUpResponse> insertServiceLocationSkillUpdate(@Body InsertServiceLocationSkillUpdateModal insertServiceLocationSkillUpdateModal);

//    http://192.168.0.29/TailorApi/Api/shop/InsertDressSubTypeSkillsByTailor

    @POST("api/Shop/InsertDressSubTypeSkillsByTailor")
    Call<SignUpResponse> insertCustumizationSkillUpdateModal(@Body InsertCustumizationSkillUpdateModal insertCustumizationSkillUpdateModal);

    //
//    http://192.168.0.21/TailorAPI/Api/Login/IsProfileCreated?UserType=Tailor&id=102
    @GET("Api/Order/GetOrderDetails?")
    Call<BuyerOrderDetailResponse> getOrderDetailsResponseBuyer(@Query("OrderId") String orderId);


    //    http://appsapi.mzyoon.com/api/shop/GetCustomizationList?TailorId=9&DressSubTypeId=2
    @GET("Api/shop/GetCustomizationList?")
    Call<GetAttributeIdToSaveLocalModal>
    getAttributesIDForLocalApi(@Query("TailorId") String orderId, @Query("DressSubTypeId") String s);

    //    http://192.168.0.29/TailorAPI/Api/shop/GetTailorDefaultCharges
    @GET("Api/shop/GetTailorDefaultCharges?")
    Call<GetTailorDefaultChargesResponse> getDefaultCharges();


//    http://appsapi.mzyoon.com/Api/shop/GetMaterialChargesByTailor?TailorId=230&DressSubTypeId=2&MaterialId=1

    @GET("Api/shop/GetMaterialChargesByTailor?")
    Call<GetMaterialChargesByTailorResponse>
    getMaterialChargesByTailor(@Query("TailorId") String orderId, @Query("DressSubTypeId") String s, @Query("MaterialId") String t);

    //    http://localhost:55410/Api/shop/GetStichingChargesbyTailor?TailorId=9&DressSubTypeId=2
    @GET("Api/shop/GetStichingChargesbyTailor?")
    Call<GetMaterialChargesByTailorResponse>
    getStichingChargesbyTailor(@Query("TailorId") String orderId, @Query("DressSubTypeId") String s);

//    http://192.198.0.35/TailorAPI/Api/Shop/InsertTailorResponseAndChargesTailorOrder


    @POST("API/shop/InsertTailorResponseAndChargesTailorOrder")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> insertTailorChargesForTailorOrder(@FieldMap HashMap<String, String> object);

    //    http://appsapi.mzyoon.com/API/shop/CountMaterialByTailorId?&TailorId=230
    @GET("Api/shop/CountMaterialByTailorId?")
    Call<SignUpResponse>
    getMaterialCount(@Query("TailorId") String orderId);


//    @GET("Api/shop/DeleteMaterialByTailorMapping?")
//    Call<SignUpResponse>
//    deleteMaterialMap(@Query("TailorId") String orderId,@Query("MaterialId") String s);

    @POST("API/shop/DeleteMaterialByTailorMapping")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> deleteMaterialMap(@FieldMap HashMap<String, String> object);

//    UpdateBalaceAmountByTailor

    @POST("API/shop/UpdateBalaceAmountByTailor")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> UpdateBalaceAmountByTailor(@FieldMap HashMap<String, String> object);


    @GET("Api/Shop/GetMeasurementpartsByMeasurementId?")
    Call<GetRequestMeasurementPartsResponse> getExistingMeasurement(@Query("MeasurementId") String orderId);

//    http://appsapi.mzyoon.com/Api/Order/UPdateQtyInOrderApproval

    @POST("API/Order/UPdateQtyInOrderApproval")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> updateQuantity(@FieldMap HashMap<String, String> object);


    @GET("Api/shop/GetQuotaionPending?")
    Call<GetOrderQuotationPendingModal> getOrderQuotationPending(@Query("TailorId") String orderId);

    @GET("Api/shop/GetQuotaionReject?")
    Call<GetOrderQuotationPendingModal> getOrderQuotationReject(@Query("TailorId") String orderId);


    @GET("Api/Order/GetQuotationOrderDetailsByTailor?")
    Call<GetOrderDetailsResponseCheck> getOrderDetailsForOrderQuotation(@Query("OrderId") String orderId, @Query("Type") String s, @Query("OrderType") String o);


    //    http://appsapi.mzyoon.com/Api/Login/InsertError
    @POST("API/Login/InsertError")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> insertError(@FieldMap HashMap<String, String> object);

    //    http://appsapi.mzyoon.com/Api/Login/InsertLanguage
    @POST("API/Login/InsertLanguage")
    @Headers("Accept: application/json")
    @FormUrlEncoded
    Call<SignUpResponse> insertLanguage(@FieldMap HashMap<String, String> object);

}

