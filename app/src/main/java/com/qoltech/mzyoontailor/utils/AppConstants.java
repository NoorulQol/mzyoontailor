package com.qoltech.mzyoontailor.utils;

import com.qoltech.mzyoontailor.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoontailor.entity.CustomizationThreeClickEntity;
import com.qoltech.mzyoontailor.entity.FilterTypeEntity;
import com.qoltech.mzyoontailor.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoontailor.entity.TailorListEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class AppConstants {


    /*PREVIOUS_SCREEN*/
    public static ArrayList<String> PREVIOUS_SCREEN = new ArrayList<>();

    public static final String HOME_ACTIVITY = "HOME_ACTIVITY";

    public static String TAG = "TAG";

    static final String SHARE_PREFERENCE = "SHARE_PREFERENCE";

    public static String LOGIN_STATUS = "LOGIN_STATUS";

    //    /*OLD BASE URL*/ http://192.168.0.26/TailorAPI/
    public static final String OLD_BASE_URL = "http://192.168.0.29/TailorAPI/";

    //    /*BASE URL*/http://192.168.0.26/TailorAPI/
    public static final String BASE_URL = "http://appsapi.mzyoon.com/";

    /*Device Id*/
    public static String DEVICE_ID = "DEVICE_ID";

    /*Mobile Num*/
    public static String MOBILE_NUM = "MOBILE_NUM";

    /*Country Code*/
    public static String COUNTRY_CODE = "CountryCode";

    /*User Id*/
    public static int USER_ID = 0;

    /*StringUserId*/
    public static String USER_ID_STR = "USER_ID_STR";

    /*ProfileImg*/
    public static String USER_IMAGE = "USER_IMAGE";

    /*Gender Id*/
    public static String GENDER_ID = "GENDER_ID";

    /*Dress Type*/
    public static String DRESS_TYPE_ID = "DRESS_TYPE_ID";

    /*Seasonal Id*/
    public static String SEASONAL_ID = "";

    /*Place of Industry Id*/
    public static String PLACE_INDUSTRY_ID = "";

    /*Brands Id*/
    public static String BRANDS_ID = "";

    /*Material Id*/
    public static String MATERIAL_ID = "";

    /*Color Id*/
    public static String COLOR_ID = "";

    /*Pattern Id*/
    public static String PATTERN_ID = "";

    /*FilterType*/
    public static String FILTER_TYPE = "FILTER_TYPE";

    /*CustomizationTheeDressTypeId*/
    public static String CUSTOMIZATION_DRESS_TYPE_ID = "";

    /*AddressForAddressScree*/
    public static String ADRRESS_STR = "ADRRESS_STR";

    /*Lat*/
    public static String LAT_STR = "LAT_STR";

    /*Long*/
    public static String LONG_STR = "LONG_STR";

    /*CountryCode*/
    public static String CountryCode = "CountryCode";

    /*CountryCodeImg*/
    public static String COUNTRY_CODE_ID = "COUNTRY_CODE_ID";

    /*CheckAddAddressScree*/
    public static String CHECK_ADD_ADDRESS = "CHECK_ADD_ADDRESS";

    /*Add Referene and Mateial*/
    public static String ADD_MATERIAL = "";

    /*USER_DETAILS*/
    public static String USER_DETAILS = "USER_DETAILS";
    public static String SelectedNum = "SelectedNum";


    /*Filter flag*/
    public static String FILTER_FLAG = "";


    /*CALENDER*/
    public static String CALENDER = "";

    /*COUNRTY_CODE_ADAPTER*/
    public static String COUNRTY_CODE_ADAPTER = "";

    /*COUNRTY_ID_TO_STATE*/
    public static String COUNTRY_ID = "";

    /*OrderSummary Details*/
    public static String GENDER_NAME = "";
    public static String DRESS_TYPE_NAME = "";
    public static String DRESS_SUB_TYPE_NAME = "";
    public static String SEASONAL_NAME = "None";
    public static String PLACE_OF_INDUSTRY_NAME = "None";
    public static String BRANDS_NAME = "None";
    public static String MATERIAL_TYPE_NAME = "None";
    public static String COLOUR_NAME = "None";
    public static String PATTERN_NAME = "None";
    public static String MEASUREMENT_MANUALLY = "";

    /*Customization three click*/
    public static String CUSTOMIZATION_NAME = "CUSTOMIZATION_NAME";

    /*Customization click arraylist*/
    public static ArrayList<CustomizationThreeClickEntity> CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();
    /*Measurement values*/
    public static String HEAD = "";
    public static String NECK = "";
    public static String CHEST = "";
    public static String WAIST = "";
    public static String THIGH = "";
    public static String BOUNCE = "";
    public static String KNEE = "";

    public static String OVER_ALL_HEIGHT = "";
    public static String KNEE_HEIGHT = "";
    public static String BOTTOM_HEIGHT = "";
    public static String HIP_HEIGHT = "";

    public static String SHOULDER = "";
    public static String HALF_SLEEVE = "";
    public static String BICEP = "";
    public static String HIP = "";
    public static String BACK = "";

    public static String HEIGHT = "";
    public static String SLEEVE_HEIGHT = "";
    public static String HAND_CUF = "";


    /*Measurement Scale*/
    public static String MEASUREMENT_SCALE = "";

    /*Measurement Scale Meter*/
    public static String MEASUREMENT_LENGTH = "";

    /*Selected Image*/
    public static String SelectedPartsImage = "SelectedPartsImage";

    /*Selected name*/
    public static String SelectedPartsName = "SelectedPartsName";

    /*Order Approval clicked Tailor list*/
    public static ArrayList<TailorListEntity> ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();
    /*Filter Gender Old*/
    public static String OLD_FILTER_GENDER = "";

    /*Filter Occasion Old*/
    public static String OLD_FILTER_OCCASION = "";

    /*Filter Region Old*/
    public static String OLD_FILTER_REGION = "";

    /*Filter Gender New*/
    public static String NEW_FILTER_GENDER = "";

    /*Filter Occasion New*/
    public static String NEW_FILTER_OCCASION = "";

    /*Filter Region New*/
    public static String NEW_FILTER_REGION = "";
    /*Filter Occasion List Bool*/
    public static String OCCASTION_LIST_BOOL = "";

    /*Filter Region List Bool*/
    public static String REGION_LIST_BOOL = "";
    /*Filter Region New List*/
    public static ArrayList<FilterTypeEntity> NEW_FILTER_REGION_LIST = new ArrayList<>();

    /*Filter Occasion Old List*/
    public static ArrayList<FilterTypeEntity> OLD_FILTER_OCCASION_LIST = new ArrayList<>();

    /*Filter Occasion New List*/
    public static ArrayList<FilterTypeEntity> NEW_FILTER_OCCASION_LIST = new ArrayList<>();

    /*Filter Region Old List*/
    public static ArrayList<FilterTypeEntity> OLD_FILTER_REGION_LIST = new ArrayList<>();

    /*Dress Type*/
    public static String SUB_DRESS_TYPE_ID = "SUB_DRESS_TYPE_ID";

    /*Dress Type*/
    public static String COUNTRY_FLAG = "UnitedArabEmirates.png";


    /*Dress Type*/
    public static String COUNTRY_FLAG_CLICK = "";


    /*Selected Edit Value*/
    public static String SelectedPartsEditValue = "";


    /*Selected Id*/
    public static String SelectedPartsId = "SelectedPartsId";


    /*Measurement Inches*/
    public static String HEAD_INCHES = "";
    public static String NECK_INCHES = "";
    public static String CHEST_INCHES = "";
    public static String WAIST_INCHES = "";
    public static String THIGH_INCHES = "";
    public static String BOUNCE_INCHES = "";
    public static String KNEE_INCHES = "";
    public static String ANKLE_INCHES = "";
    public static String ANKLE = "";
    public static String OVER_ALL_HEIGHT_INCHES = "";
    public static String KNEE_HEIGHT_INCHES = "";
    public static String BOTTOM_HEIGHT_INCHES = "";
    public static String HIP_HEIGHT_INCHES = "";

    public static String SHOULDER_INCHES = "";
    public static String HALF_SLEEVE_INCHES = "";
    public static String BICEP_INCHES = "";
    public static String HIP_INCHES = "";
    public static String BACK_INCHES = "";

    public static String HEIGHT_INCHES = "";
    public static String SLEEVE_HEIGHT_INCHES = "";
    public static String HAND_CUF_INCHES = "";

    public static String UNITS = "CM";

    public static String MEASUREMENT_ID = "";

    //    /*IMAGE BASE_URL*/ http://192.168.0.29/TailorAPI/
//    http://appsapi.mzyoon.com/
    public static final String IMAGE_BASE_URL = "http://appsapi.mzyoon.com/";

    public static String ORDER_ID = "";

    public static float latitude = 0;
    public static float longitude = 0;
    public static ArrayList<String> MATERIAL_IMAGES = new ArrayList<>();
    public static ArrayList<String> REFERENCE_IMAGES = new ArrayList<>();

    /*Edit Address*/
    public static String EDIT_ADDRESS = "";

    public static String SELECTED_LAT_STR = "";
    public static String SELECTED_LONG_STR = "";

    public static ArrayList<String> CUSTOM_ID = new ArrayList<>();

    /*Check Service */
    public static String SERVICE_TYPE = "";

    public static HashMap<String, String> MATERIAL_MAP = new HashMap<>();


    public static HashMap<Integer, String> MATERIAL_MAP_NEW = new HashMap<>();


    /*appointment list id*/
    public static String APPOINTMENT_LIST_ID = "";

    /*appointment id*/
    public static String APPOINTMENT_MATERIAL_ID = "";

    /*appointment  id*/
    public static String APPOINTMENT_MEASUREMENT_ID = "";

    /*schedule id*/
    public static String APPOINTMENT_SCHEDULE_ID = "";

    /*Date Picker*/
    public static String DATE_PICKER_COND = "";

    /*DATE_FOR_RESTRICT_MATERIAL*/
    public static String DATE_FOR_RESTRICT_MATERIAL = "";

    /*DATE_FOR_RESTRICT_MEASUREMENT*/
    public static String DATE_FOR_RESTRICT_MEASUREMENT = "";

    /*order request list Id*/
    public static String REQUEST_LIST_ID = "";

    /*Check book appointment*/
    public static String CHECK_BOOK_APPOINTMENT = "";


    /*GenderSkill Update List*/
    public static ArrayList<String> SKILL_GENDER_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_DRESS_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_SUB_DRESS_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_ORDER_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_CUSTOMIZATION_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_MEASUREMENT_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_APPOINTMENT_ID = new ArrayList<>();

    public static ArrayList<String> SKILL_LOCATION_ID = new ArrayList<>();

    public static String TAILORMEASUREMENT = "";

    /*Measurement Parts*/
    public static HashMap<String, String> MEASUREMENT_MAP = new HashMap<>();

    public static String ORDER_TYPE_ID = "";
    public static String MEASUREMENT_TYPE = "";
    /*Customization three imgaes*/
    public static ArrayList<CustomizationAttributesEntity> CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();

    public static String SHORTS = "";
    public static String SHORTS_INCHES = "";
    public static String OUTSEAM_INCHES = "";
    public static String INSEAM_INCHES = "";
    public static String OUTSEAM = "";
    public static String INSEAM = "";
    public static String BOTTOM = "";
    public static String BOTTOM_INCHES = "";
    public static String SLEEVE = "";
    public static String SLEEVE_INCHES = "";
    public static String WRIST = "";
    public static String WRIST_INCHES = "";
    public static String WOMEN_OVER_BUST = "";
    public static String WOMEN_OVER_BUST_INCHES = "";
    public static String WOMEN_UNDER_BUST = "";
    public static String WOMEN_UNDER_BUST_INCHES = "";
    public static String WOMEN_HIP_BONE = "";
    public static String WOMEN_HIP_BONE_INCHES = "";
    public static String WOMEN_THIGH = "";
    public static String WOMEN_THIGH_INCHES = "";
    public static String WOMEN_KNEE = "";
    public static String WOMEN_KNEE_INCHES = "";
    public static String WOMEN_CALF = "";
    public static String WOMEN_CALF_INCHES = "";
    public static String WOMEN_ANKLE = "";
    public static String WOMEN_ANKLE_INCHES = "";
    public static String WOMEN_HEAD = "";
    public static String WOMEN_HEAD_INCHES = "";
    public static String WOMEN_NECK = "";
    public static String WOMEN_NECK_INCHES = "";
    public static String WOMEN_BUST = "";
    public static String WOMEN_BUST_INCHES = "";
    public static String WOMEN_WAIST = "";
    public static String WOMEN_WAIST_INCHES = "";
    public static String WOMEN_FULL_HIP = "";
    public static String WOMEN_HEIGHT = "";
    public static String WOMEN_FULL_HIP_INCHES = "";
    public static String WOMEN_HEIGHT_INCHES = "";
    public static String WOMEN_STW = "";
    public static String WOMEN_STW_INCHES = "";
    public static String WOMEN_NLTC = "";
    public static String WOMEN_NLTC_INCHES = "";
    public static String WOMENT_NLTB = "";
    public static String WOMENT_NLTB_INCHES = "";
    public static String WOMEN_STHB = "";
    public static String WOMEN_STHB_INCHES = "";
    public static String WOMEN_WTHB = "";
    public static String WOMEN_WTHB_INCHES = "";
    public static String WOMEN_HTH = "";
    public static String WOMEN_HTH_INCHES = "";
    public static String WOMEN_INSEAM = "";
    public static String WOMEN_INSEAM_INCHES = "";
    public static String WOMEN_OUTSEAM = "";
    public static String WOMEN_OUTSEAM_INCHES = "";

    public static String WOMEN_SHOULDER_INCHES = "";
    public static String WOMEN_BICEP_INCHES = "";
    public static String WOMEN_WRIST_INCHES = "";

    public static String WOMEN_SLEEVE_INCHES = "";

    public static String BOY_HEAD_INCHES = "";
    public static String BOY_NECK_INCHES = "";
    public static String BOY_CHEST_INCHES = "";
    public static String BOY_WAIST_INCHES = "";
    public static String BOY_THIGH_INCHES = "";
    public static String BOY_KNEE_INCHES = "";
    public static String BOY_ANKLE_INCHES = "";

    public static String BOY_OVER_ALL_HEIGHT_INCHES = "";
    public static String BOY_SHORTS_INCHES = "";
    public static String BOY_OUTSEAM_INCHES = "";
    public static String BOY_INSEAM_INCHES = "";

    public static String BOY_SHOULDER_INCHES = "";
    public static String BOY_HALFSLEEVE_INCHES = "";
    public static String BOY_BICEP_INCHES = "";
    public static String BOY_HIP_INCHES = "";
    public static String BOY_BOTTOM_INCHES = "";

    public static String BOY_HEIGHT_INCHES = "";
    public static String BOY_SLEEVE_INCHES = "";
    public static String BOY_WRIST_INCHES = "";

    public static String GIRL_OVER_BURST_INCHES = "";
    public static String GIRL_UNDER_BURST_INCHES = "";
    public static String GIRL_HIP_BONE_INCHES = "";
    public static String GIRL_THIGH_INCHES = "";
    public static String GIRL_KNEE_INCHES = "";
    public static String GIRL_CALF_INCHES = "";
    public static String GIRL_ANKLE_INCHES = "";

    public static String GIRL_HEAD_INCHES = "";
    public static String GIRL_NECK_INCHES = "";
    public static String GIRL_BUST_INCHES = "";
    public static String GIRL_WAIST_INCHES = "";
    public static String GIRL_FULL_HIP_INCHES = "";

    public static String GIRL_HEIGHT_INCHES = "";
    public static String GIRL_STW_INCHES = "";
    public static String GIRL_NLTC_INCHES = "";
    public static String GIRL_NLTB_INCHES = "";
    public static String GIRL_STHB_INCHES = "";
    public static String GIRL_WTHB_INCHES = "";
    public static String GIRL_HTH_INCHES = "";
    public static String GIRL_INSEAM_INCHES = "";
    public static String GIRL_OUTSEAM_INCHES = "";

    public static String GIRL_SHOULDER_INCHES = "";
    public static String GIRL_BICEP_INCHES = "";
    public static String GIRL_WRIST_INCHES = "";

    public static String GIRL_SLEEVE_INCHES = "";

    public static String BOY_HEAD = "";
    public static String BOY_NECK = "";
    public static String BOY_CHEST = "";
    public static String BOY_WAIST = "";
    public static String BOY_THIGH = "";
    public static String BOY_KNEE = "";
    public static String BOY_ANKLE = "";

    public static String BOY_OVER_ALL_HEIGHT = "";
    public static String BOY_SHORTS = "";
    public static String BOY_OUTSEAM = "";
    public static String BOY_INSEAM = "";

    public static String BOY_SHOULDER = "";
    public static String BOY_HALFSLEEVE = "";
    public static String BOY_BICEP = "";
    public static String BOY_HIP = "";
    public static String BOY_BOTTOM = "";

    public static String BOY_HEIGHT = "";
    public static String BOY_SLEEVE = "";
    public static String BOY_WRIST = "";

    public static String GIRL_OVER_BURST = "";
    public static String GIRL_UNDER_BURST = "";
    public static String GIRL_HIP_BONE = "";
    public static String GIRL_THIGH = "";
    public static String GIRL_KNEE = "";
    public static String GIRL_CALF = "";
    public static String GIRL_ANKLE = "";

    public static String GIRL_HEAD = "";
    public static String GIRL_NECK = "";
    public static String GIRL_BUST = "";
    public static String GIRL_WAIST = "";
    public static String GIRL_FULL_HIP = "";

    public static String GIRL_HEIGHT = "";
    public static String GIRL_STW = "";
    public static String GIRL_NLTC = "";
    public static String GIRL_NLTB = "";
    public static String GIRL_STHB = "";
    public static String GIRL_WTHB = "";
    public static String GIRL_HTH = "";
    public static String GIRL_INSEAM = "";
    public static String GIRL_OUTSEAM = "";

    public static String GIRL_SHOULDER = "";
    public static String GIRL_BICEP = "";
    public static String GIRL_WRIST = "";

    public static String GIRL_SLEEVE = "";


    public static String WOMEN_SHOULDER = "";
    public static String WOMEN_BICEP = "";
    public static String WOMEN_WRIST = "";

    public static String WOMEN_SLEEVE = "";
    /*GET_ADDRESS_ENTITY*/
    public static String GET_ADDRESS_ID = "";

    public static String DELIVERY_TYPE_ID = "1";

    /*MeasurementParts*/
    public static ArrayList<GetMeasurementPartEntity> MEASUREMENT_VALUES = new ArrayList<>();

    public static String DIRECT_USERS_ID = "";
    public static String STATE_ID = "";
    public static String AREA_ID = "";
    public static String INITIATED_BY = "";
    public static int MEASUREMENT_ID_MANUALLY = 0;

    public static String MEASUREMENT_EXISTING = "";
    public static String TYPE_TAILOR_BUYER = "";

    /*Customization three imgaes*/

    /*Service type*/
    public static String SERVICE_TYPE_NAME = "";

    /*MeasurementType type*/
    public static String MEASUREMENT_ONE = "";
    //    Measurement Charges Id
    public static int MEASUREMENT_TYPE_CHARGES=0;
    //Order Charges Id
    public static int ORDER_TYPE_CHARGES = 0;
    //    Service Charges Id
    public static int SERVICE_TYPE_CHARGES = 0;

    public static String STATE_ID_SHOP = "";

    /*Pending Delivery Click*/
    public static String PENDING_DELIVERY_CLICK = "";


    /*Area Code Id*/
    public static String COUNTRY_AREA_ID = "";


    /*State Code Id*/
    public static String COUNTRY_STATE_ID = "";


    /*State Code Id*/
    public static String GENDER_SCREEN_SKILLUPDATE = "";

    /*Customization Selected*/
    public static ArrayList<String> CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

    /*Order Type*/
    public static String DIRECT_ORDER_TYPE = "";


    /*Attribute Id*/
    public static String ATTRIBUTE_ID = "";

//

    /*TOTAL_RUPEES*/
    public static String TOTAL_RUPEES = "";

    /*AttributeId List*/
    public static ArrayList<String> ATTRIBUTE_ID_LIST = new ArrayList<>();


    //*AttributeId List*/
    public static ArrayList<String> CUSTOMIZATION_ID_SAVE = new ArrayList<>();


    public static ArrayList<String> GETCUSTUMIZATION_ID = new ArrayList<>();

    public static ArrayList<String> GETCUSTUMIZATION_ID_CLICK = new ArrayList<>();


    /*AttributeId List*/
    public static ArrayList<String> LOCAL_ATTRIBUTE_ID_LIST = new ArrayList<>();


    /*Attribute Id*/
    public static String TAILOR_DIRECT_ORDER = "";


    public static String MATERIAL_NAME_ENGLISH = "";
    public static String MATERIAL_NAME_ARABIC = "";

    public static int MATERIAL_MAP_SIZE = 0;
    public static String ORDER_TYPE = "";

    public static String BALANCE = "";
    public static String NEW_CUSTOMER_ACTIVITY = "";

    /*Add New Measurement List*/
    public static String ADD_NEW_MEASUREMENT = "NEW_ORDER";


    public static String SET_MATERIAL_IMAGE = "";

    public static String SET_REFERENCE_IMAGE = "";
    public static String CLASS_NAME = "";

    /*Date and Time Restrict*/
    public static String DATE_FOR_RESTRICT_SCHEDULE = "";
    public static String NO_OF_DAYS_FOR_SCHEDULE = "0";
    /*Direct Order*/
    public static String DIRECT_ORDER = "";

    /*Approved TailorId*/
    public static String APPROVED_TAILOR_ID = "";


    /*Approved TailorId*/
    public static String EXISTING_MEASUREMENT_ID = "";


    /*Approved TailorId*/
    public static String PATTERN_ID_ORDER_DETAILS = "";

    public static String ORDER_DETAILS = "";

    public static String MEASUREMENT_APPOINTMENT_STATUS = "";

    public static String QUOTATION_ADAPTER = "";

    public static String QUOTATION_DATE = "";

    public static String TOKEN = "";

    public static String CHANNEL_ID = "";
    public static String DEVICE_ID_NEW = "";
    public static String SERVICE_TYPE_NAME_ENG = "";
    public static String DIRECT_MEASUREMENT_TYPE = "";
    public static String PAYMENT_TYPE = "";
    public static String PAYMENT_MODE = "";
    public static String TRANSACTION_NUMBER = "0";

    public static ArrayList<String> MEASUREMENT_LIST = new ArrayList<>();
    public static HashSet<String> MEASUREMENT_LIST_CHECK=new HashSet<>();

}




