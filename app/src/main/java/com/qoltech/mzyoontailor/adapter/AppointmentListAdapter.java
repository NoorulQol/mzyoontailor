package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.AppointmentListEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.AppointmentDetails;
import com.qoltech.mzyoontailor.ui.ScheduletDetailScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

//import com.qoltech.mzyoonbuyer.R;
//import com.qoltech.mzyoonbuyer.entity.AppointmentListEntity;
//import com.qoltech.mzyoonbuyer.entity.UserDetailsEntity;
//import com.qoltech.mzyoonbuyer.main.BaseActivity;
//import com.qoltech.mzyoonbuyer.ui.AppointmentDetails;
//import com.qoltech.mzyoonbuyer.utils.AppConstants;
//import com.qoltech.mzyoonbuyer.utils.PreferenceUtil;

public class AppointmentListAdapter extends RecyclerView.Adapter<AppointmentListAdapter.Holder> {

    private Context mContext;
    private ArrayList<AppointmentListEntity> mAppointmentList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public AppointmentListAdapter(Context activity, ArrayList<AppointmentListEntity> appointmentList) {
        mContext = activity;
        mAppointmentList = appointmentList;
    }

    @NonNull
    @Override
    public AppointmentListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_appointment_list, parent, false);
        return new AppointmentListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AppointmentListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        AppointmentListEntity mList = mAppointmentList.get(position);

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/DressSubType/"+mList.getImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mAppointmentNameImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mAppointmentTailorNameTxt.setText(mList.getName());
//            holder.mAppointmentShopNameTxt.setText(mList.getShopNameInArabic());
            holder.mAppointmentProductNameTxt.setText(mList.getNameInArabic());
        }else {
            holder.mAppointmentTailorNameTxt.setText(mList.getName());
//            holder.mAppointmentShopNameTxt.setText(mList.getShopNameInEnglish());
            holder.mAppointmentProductNameTxt.setText(mList.getNameInEnglish());
        }
        holder.mAppointmentListDateTxt.setText(mList.getOrderDate().replace("T00:00:00",""));
        holder.mAppointmentIdTxt.setText(String.valueOf(mList.getOrderId()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CHECK_BOOK_APPOINTMENT = "BookAnAppointment";
                AppConstants.APPOINTMENT_LIST_ID =String.valueOf(mAppointmentList.get(position).getOrderId());
                AppConstants.DIRECT_USERS_ID =String.valueOf(mAppointmentList.get(position).getCustomerid());

                if (mAppointmentList.get(position).getDeliveryTypeId() == 3){
                    ((BaseActivity)mContext).nextScreen(ScheduletDetailScreen.class,true);

                }else {
                    ((BaseActivity)mContext).nextScreen(AppointmentDetails.class,true);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mAppointmentList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.appointment_list_img)
        ImageView mAppointmentNameImg;

        @BindView(R.id.appointment_list_date_txt)
        TextView mAppointmentListDateTxt;

        @BindView(R.id.appointment_id_txt)
        TextView mAppointmentIdTxt;

        @BindView(R.id.appointment_tailor_name_txt)
        TextView mAppointmentTailorNameTxt;

        @BindView(R.id.appointment_shop_name_txt)
        TextView mAppointmentShopNameTxt;

        @BindView(R.id.appointment_product_name_txt)
        TextView mAppointmentProductNameTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

