package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationSessionEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.CustomizationOneScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationOneSessionAdapter extends  RecyclerView.Adapter<CustomizationOneSessionAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationSessionEntity> mCustomizeSessionList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationOneSessionAdapter(Context activity, ArrayList<CustomizationSessionEntity> customizeSessionList) {
        mContext = activity;
        mCustomizeSessionList = customizeSessionList;
    }

    @NonNull
    @Override
    public CustomizationOneSessionAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
        return new CustomizationOneSessionAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationOneSessionAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final  CustomizationSessionEntity sessionEntity = mCustomizeSessionList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(sessionEntity.getSeasonInArabic());

        }else {
            holder.mGridViewTxt.setText(sessionEntity.getSeasonInEnglish());

        }


        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/seasons/"+sessionEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCustomizeSessionList.get(position).getSeasonInEnglish().equalsIgnoreCase("All Season")){

                    if (mCustomizeSessionList.get(position).getChecked()){
                        for (int i=0; i<mCustomizeSessionList.size(); i++) {
                            mCustomizeSessionList.get(i).setChecked(false);
                        }
                        AppConstants.SEASONAL_ID = "";
                        AppConstants.SEASONAL_NAME = "";
                    }else {
                        AppConstants.SEASONAL_ID = "";
                        AppConstants.SEASONAL_NAME = "";
                        for (int i=0; i<mCustomizeSessionList.size(); i++) {
                            mCustomizeSessionList.get(i).setChecked(true);
                            AppConstants.SEASONAL_ID = String.valueOf(mCustomizeSessionList.get(position).getId()) ;

                            AppConstants.SEASONAL_NAME = String.valueOf(mCustomizeSessionList.get(position).getSeasonInEnglish()) ;

                        }
                    }
                    ((CustomizationOneScreen)mContext).getNewFlowCustomizationApiCall();


//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationOneScreen)mContext).getNewFlowCustomizationApiCall();
//                    }else {
//                        ((CustomizationOneScreen)mContext).getCustomizationApiCall();
//
//                    }
                    notifyDataSetChanged();

                }else {
                    if (mCustomizeSessionList.get(position).getChecked()){
                        mCustomizeSessionList.get(position).setChecked(false);
                        for (int i=0; i<mCustomizeSessionList.size() ; i++){
                            if (mCustomizeSessionList.get(i).getSeasonInEnglish().equalsIgnoreCase("All Season")){
                                mCustomizeSessionList.get(i).setChecked(false);
                            }
                        }
                    }else {
                        mTotalInt = 1;
                        for (int i=0; i<mCustomizeSessionList.size();i++){
                            if (mCustomizeSessionList.get(i).getChecked()){
                                mTotalInt = mTotalInt + 1;
                            }
                            if (String.valueOf(mTotalInt).equalsIgnoreCase(String.valueOf(mCustomizeSessionList.size()-1))){
                                for (int j=0; j<mCustomizeSessionList.size(); j++){
                                    if (mCustomizeSessionList.get(j).getSeasonInEnglish().equalsIgnoreCase("All Season")){
                                        mCustomizeSessionList.get(j).setChecked(true);
                                    }
                                }

                            }
                        }

                        mCustomizeSessionList.get(position).setChecked(true);

                    }
                    for (int i=0; i<mCustomizeSessionList.size(); i++){
                        if (mCustomizeSessionList.get(i).getChecked()){
                            AppConstants.SEASONAL_ID = AppConstants.SEASONAL_ID.equalsIgnoreCase("") ? mCustomizeSessionList.get(i).getId()+"" : AppConstants.SEASONAL_ID  + ","+ mCustomizeSessionList.get(i).getId();

                            AppConstants.SEASONAL_NAME =  AppConstants.SEASONAL_NAME.equalsIgnoreCase("") ? mCustomizeSessionList.get(i).getSeasonInEnglish() : AppConstants.SEASONAL_NAME + ","+ mCustomizeSessionList.get(i).getSeasonInEnglish();
                        }
                    }
                    ((CustomizationOneScreen)mContext).getNewFlowCustomizationApiCall();

//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//
//                    }else {
//                        ((CustomizationOneScreen)mContext).getCustomizationApiCall();
//
//                    }

                    notifyDataSetChanged();


                }

            }
        });


        holder.mGridCustimizeImg.setVisibility(sessionEntity.getChecked() ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return mCustomizeSessionList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

