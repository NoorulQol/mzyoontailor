package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qoltech.mzyoontailor.R;

import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.Holder> {

    private Context mContext;

    public OrderListAdapter(Context activity ) {
        mContext = activity;
    }

    @NonNull
    @Override
    public OrderListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_list, parent, false);
        return new OrderListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderListAdapter.Holder holder, final int position) {

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((BaseActivity)mContext).nextScreen(OrderDetailsScreen.class,true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return 15;
    }

    public class Holder extends RecyclerView.ViewHolder {


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
