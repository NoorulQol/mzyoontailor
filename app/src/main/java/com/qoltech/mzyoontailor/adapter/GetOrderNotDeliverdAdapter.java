package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.OrderDetailsActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetOrderNotDeliverdResult;

import java.util.List;


public class GetOrderNotDeliverdAdapter extends RecyclerView.Adapter<GetOrderNotDeliverdAdapter.ViewHolder> {

    List<GetOrderNotDeliverdResult> items;
    private Context context;
    private PositionUpdateListener listener;
    private UserDetailsEntity mUserDetailsEntityRes;
    public GetOrderNotDeliverdAdapter(Context context, List<GetOrderNotDeliverdResult> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_appointment_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final GetOrderNotDeliverdResult item = items.get(position);
        holder.view.setTag(item);

        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

            holder.appointment_list_date_txt.setText(item.getOrderDate().replace("T00:00:00",""));
            holder.appointment_id_txt.setText(String.valueOf(item.getOrderId()));
            holder.appointment_tailor_name_txt.setText(item.getFirstName());
            holder.appointment_product_name_txt.setText(item.getProductNameInArabic());

        }else {
            holder.appointment_list_date_txt.setText(item.getOrderDate().replace("T00:00:00",""));
            holder.appointment_id_txt.setText(String.valueOf(item.getOrderId()));
            holder.appointment_tailor_name_txt.setText(item.getFirstName());
            holder.appointment_product_name_txt.setText(item.getProductNameInEnglish());

        }
        try {

//            Glide.with(context).load("http://192.168.0.26/TailorAPI/images/DressSubType/Maroon_Solid_Skater.png")
////            Glide.with(context).load(GlobalData.SERVER_URL + "images/DressSubType/" + item.getImage())
//                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
//                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .placeholder(R.drawable.placeholder)).into(holder.appointment_list_img);


            Glide.with(context)
                    .load(GlobalData.SERVER_URL + "images/DressSubType/" + item.getImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.appointment_list_img);


        } catch (Exception e) {
        }
        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                GetOrderNotDeliverdResult gender = (BuyerRequestPendingModal) v.getTag();
//                if (listener != null) {
//                    listener.onPositionUpdate(items.indexOf(gender));
//                }
//                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
                AppConstants.ORDER_ID = String.valueOf(item.getOrderId());
//                AppConstants.REQUEST_LIST_ID = String.valueOf(item.getOrderId());
//                context.startActivity(new Intent(context, RequestViewActivity.class));


                AppConstants.ORDER_ID = String.valueOf(item.getOrderId());
                AppConstants.MEASUREMENT_ID_MANUALLY = item.getMeasurementType();
                AppConstants.TYPE_TAILOR_BUYER=item.getType();
                AppConstants.ORDER_TYPE=item.getOrderType();
                context.startActivity(new Intent(context,OrderDetailsActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetOrderNotDeliverdResult item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetOrderNotDeliverdResult item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout relativeLayout_main;
        TextView appointment_list_date_txt, appointment_id_txt, appointment_tailor_name_txt, appointment_product_name_txt, tailor_name_text;
        View view;
        ImageView appointment_list_img;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (LinearLayout) itemView;
            appointment_list_img = itemView.findViewById(R.id.appointment_list_img);
            appointment_list_date_txt = itemView.findViewById(R.id.appointment_list_date_txt);
            appointment_id_txt = itemView.findViewById(R.id.appointment_id_txt);
            appointment_tailor_name_txt = itemView.findViewById(R.id.appointment_tailor_name_txt);
            appointment_product_name_txt = itemView.findViewById(R.id.appointment_product_name_txt);
            tailor_name_text = itemView.findViewById(R.id.tailor_name_text);
//            tailor_name_text.setText("Buyer Name");

        }

        public LinearLayout getMainView() {
            return relativeLayout_main;
        }
    }
}