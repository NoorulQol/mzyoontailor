package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetSkillUpdateCustumizationAttributeResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetCustumizationSkillsUpdateAdapter extends
        RecyclerView.Adapter<GetCustumizationSkillsUpdateAdapter.ViewHolder> {

    Context context;

    private List<GetSkillUpdateCustumizationAttributeResult> getCustomizationThreeEntities;
    Dialog dialog;
    private ApiService restService;
    private UserDetailsEntity mUserDetailsEntityRes;
    int incr = 0;

    public GetCustumizationSkillsUpdateAdapter(Context context, Dialog dialog,
                                               ApiService restService,
                                               List<GetSkillUpdateCustumizationAttributeResult> getCustomizationThreeEntities) {
        this.context = context;
        this.dialog = dialog;
        this.restService = restService;
        this.getCustomizationThreeEntities = getCustomizationThreeEntities;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_status_tracking, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final GetSkillUpdateCustumizationAttributeResult item = getCustomizationThreeEntities.get(position);
//        holder.tracking_update_switch.setEnabled(false);
        holder.countryName.setText(item.getAttributeNameInEnglish());


        if (getCustomizationThreeEntities.get(position).getSwitch() == true) {
            holder.itemView.setEnabled(false);
//            holder.tracking_update_switch.setEnabled(false);
            holder.tracking_update_switch.setChecked(true);
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                insertCustomizationSkillsByTailor("9","1","1");
            }


        });
        holder.view.setTag(item);
//        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//            ViewCompat.setLayoutDirection(holder.bgLayout, ViewCompat.LAYOUT_DIRECTION_RTL);
//            holder.countryName.setText(item.getAttributeNameInEnglish());
//            holder.countryName.setSelected(true);
//        } else {
//            ViewCompat.setLayoutDirection(holder.bgLayout, ViewCompat.LAYOUT_DIRECTION_LTR);
////            holder.countryName.setText(item.getAttributeNameInEnglish());
//            holder.countryName.setSelected(true);
//        }
    }

    @Override
    public int getItemCount() {
        return getCustomizationThreeEntities.size();
    }

    public void add(GetSkillUpdateCustumizationAttributeResult item, int position) {
        getCustomizationThreeEntities.add(position, item);
        notifyItemInserted(position);
    }


    public void add(GetSkillUpdateCustumizationAttributeResult item) {
        getCustomizationThreeEntities.add(item);
        notifyItemInserted(getCustomizationThreeEntities.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main, bgLayout;
        TextView countryName;
        View view, view_grey;
        Switch tracking_update_switch;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            countryName = itemView.findViewById(R.id.countryName);
//            tracking_update_switch = itemView.findViewById(R.id.tracking_update_switch);
            view_grey = (View) itemView.findViewById(R.id.view_grey);
            bgLayout = (RelativeLayout) itemView.findViewById(R.id.bgLayout);

        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }




//    UpdateDeliveryStatus

    public void insertCustomizationSkillsByTailor(String tailorId,String dressSubTypeId,String customizationId) {
            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", tailorId);
            map.put("DressSubTypeId", dressSubTypeId);
            map.put("CustomizationId", customizationId);
            restService.InsertCustomizationSkillsByTailor(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
    }





}