package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.CustumizationAttributeDetailsSkillUpdateActivity;
import com.qoltech.mzyoontailor.entity.GetTailorCustomizationSkillUpdate;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;


public class CustumizationListSkillUpdateAdapter extends RecyclerView.Adapter<CustumizationListSkillUpdateAdapter.ViewHolder> {

    List<GetTailorCustomizationSkillUpdate> items;
    private Context context;
    private PositionUpdateListener listener;

    private ArrayList<String> mDressIdList;
    ApiService restService;
    SharedPreferences sharedPreferences;
    private UserDetailsEntity mUserDetailsEntityRes;

    public CustumizationListSkillUpdateAdapter(Context context, List<GetTailorCustomizationSkillUpdate> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_custumization_with_arrow_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
//        mDressIdList = new ArrayList<>();
//        AppConstants.SKILL_DRESS_ID = new ArrayList<>();
        final GetTailorCustomizationSkillUpdate item = items.get(position);
        holder.view.setTag(item);
        holder.text_skill_update.setText(item.getAttributeNameInEnglish());

//        AppConstants.ATTRIBUTE_ID_LIST.add(String.valueOf(item.getId()));
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.text_skill_update.setText(item.getAttributeNameinArabic());

        } else {
            holder.text_skill_update.setText(item.getAttributeNameInEnglish());

        }


//        holder.switch_skill_update.setChecked(item.getSwitch());
//        if (items.get(position).getSwitch() == true) {
//            holder.itemView.setEnabled(false);
//            holder.switch_skill_update.setEnabled(false);
////            holder.tracking_update_switch.setChecked(true);
//        }
//        for (int i = 0; i < items.size(); i++) {
//            mDressIdList.add(i, "0");
//
//            if (items.get(i).getSwitch()) {
//                mDressIdList.add(i, String.valueOf(items.get(i).getId()));
//            }
//
//        }

//        try {
//
//            Glide.with(context).load(GlobalData.SERVER_URL + "images/DressTypes/" + item.getAttributeImage())
//                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
//                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .placeholder(R.drawable.placeholder)).into(holder.gender_image);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(item.getId());
                AppConstants.ATTRIBUTE_ID = String.valueOf(item.getId());
//                AppConstants.ATTRIBUTE_ID_LIST.add(String.valueOf(item.getId()));

//                for (int i = 0; i < items.size(); i++) {

//
//                if (AppConstants.GETCUSTUMIZATION_ID_CLICK.size()==items.size()){
//                    Toast.makeText(context,"Limit Exceeded",Toast.LENGTH_LONG).show();
//                }
//                else {
//                    AppConstants.GETCUSTUMIZATION_ID_CLICK.add(String.valueOf(item.getId()));
//
//                }
//                }

                context.startActivity(new Intent(context, CustumizationAttributeDetailsSkillUpdateActivity.class));


            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetTailorCustomizationSkillUpdate item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetTailorCustomizationSkillUpdate item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView gender_image;
        TextView text_skill_update;
        //        Switch switch_skill_update;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
//            gender_image = itemView.findViewById(R.id.gender_image);
            text_skill_update = itemView.findViewById(R.id.text_skill_update);
        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }

//
}