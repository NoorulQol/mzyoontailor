package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.SubTypeDressSkillUpdate;
import com.qoltech.mzyoontailor.entity.DressTypeEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GetMaterialMappingAdapter extends RecyclerView.Adapter<GetMaterialMappingAdapter.ViewHolder> {

    List<DressTypeEntity> items;
    private Context context;
    private PositionUpdateListener listener;

    private ArrayList<String> mDressIdList;
    ApiService restService;
    SharedPreferences sharedPreferences;

    public GetMaterialMappingAdapter(Context context, List<DressTypeEntity> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_skill_update_dress_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        mDressIdList = new ArrayList<>();
//        AppConstants.SKILL_DRESS_ID = new ArrayList<>();
        final DressTypeEntity item = items.get(position);
        holder.view.setTag(item);
        holder.text_skill_update.setText(item.getDressTypeInEnglish());

//        holder.switch_skill_update.setChecked(item.getSwitch());
//        if (items.get(position).getSwitch() == true) {
//            holder.itemView.setEnabled(false);
//            holder.switch_skill_update.setEnabled(false);
////            holder.tracking_update_switch.setChecked(true);
//        }
//        for (int i = 0; i < items.size(); i++) {
//            mDressIdList.add(i, "0");
//
//            if (items.get(i).getSwitch()) {
//                mDressIdList.add(i, String.valueOf(items.get(i).getId()));
//            }
//
//        }

        try {

            Glide.with(context).load(GlobalData.SERVER_URL + "images/DressTypes/" + item.getImageURL())
                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(holder.gender_image);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (holder.switch_skill_update.isChecked() == true) {
//                    holder.switch_skill_update.setChecked(false);
//                    mDressIdList.remove(holder.getAdapterPosition());
//                } else {
//                    holder.switch_skill_update.setChecked(true);
//                    mDressIdList.add(String.valueOf(items.get(position).getId()));
//
//                }
//                AppConstants.SKILL_DRESS_ID = new ArrayList<>();
//                AppConstants.SKILL_DRESS_ID = mDressIdList;


//                insertDressTypeSkillsByTailor(sharedPreferences.getString("TailorId", ""),
//                        String.valueOf(items.get(position).getId()));
//                holder.switch_skill_update.setChecked(true);
//                holder.itemView.setEnabled(false);

                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(item.getId());

                context.startActivity(new Intent(context, SubTypeDressSkillUpdate.class));


            }
        });

//        holder.switch_skill_update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.switch_skill_update.isChecked() == true) {
//                    mDressIdList.remove(holder.getAdapterPosition());
//                } else {
//
//                    mDressIdList.add(String.valueOf(items.get(position).getId()));
//
//                }
//                AppConstants.SKILL_DRESS_ID = new ArrayList<>();
//                AppConstants.SKILL_DRESS_ID = mDressIdList;
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(DressTypeEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(DressTypeEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView gender_image;
        TextView text_skill_update;
        //        Switch switch_skill_update;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            gender_image = itemView.findViewById(R.id.gender_image);
            text_skill_update = itemView.findViewById(R.id.text_skill_update);
//            switch_skill_update = itemView.findViewById(R.id.switch_skill_update);
        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }

    public void insertDressTypeSkillsByTailor(String tailorId, String dressTypeId) {
            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", tailorId);
            map.put("DressTypeId", dressTypeId);
            restService.InsertDressTypeSkillsByTailor(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


//                    updateGenderSwitch(response.body().getResult(), "1");
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
    }
//
}