package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.entity.ManuallyEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.AddReferenceScreen;
import com.qoltech.mzyoontailor.ui.MeasurementOneScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ManuallyAdapter extends RecyclerView.Adapter<ManuallyAdapter.Holder> {

    private Context mContext;
    private ArrayList<ManuallyEntity> mManualList;
    private Dialog mDialog;

    public ManuallyAdapter(Context activity, ArrayList<ManuallyEntity> manualList, Dialog dialog) {
        mContext = activity;
        mManualList = manualList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public ManuallyAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new ManuallyAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ManuallyAdapter.Holder holder, final int position) {
        final ManuallyEntity manualEntity = mManualList.get(position);

        holder.mManualImg.setVisibility(View.GONE);

        holder.mManualTxt.setText(manualEntity.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                AppConstants.MEASUREMENT_MANUALLY = mManualList.get(position).getName();

                String[] parts = mManualList.get(position).getName().split("-");
                String part1 = parts[0]; // 004
                String part2 = parts[1];
                String lastOne = parts[parts.length - 1];
                AppConstants.MEASUREMENT_ID = part1;
                AppConstants.MEASUREMENT_EXISTING="EXISTING";
//                ((BaseActivity)mContext).nextScreen(MeasurementTwoScreen.class,true);



                if (AppConstants.TAILORMEASUREMENT.equalsIgnoreCase("TAILORMEASUREMENT")) {
                    Toast.makeText(mContext,"Success",Toast.LENGTH_LONG).show();
                    mContext.startActivity(new Intent(mContext, HomeActivity.class));
                    AppConstants.TAILORMEASUREMENT="";
                    ((MeasurementOneScreen)mContext).updateMeasurementByTailor(AppConstants.ORDER_ID, AppConstants.MEASUREMENT_ID.equalsIgnoreCase("") ? "-1" : AppConstants.MEASUREMENT_ID);
                } else {
                    ((BaseActivity)mContext).nextScreen(AddReferenceScreen.class,true);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mManualList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mManualTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mManualImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

