package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.SubTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.OrderTypeScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubTypeAdapter extends RecyclerView.Adapter<SubTypeAdapter.Holder> {

    private Context mContext;
    private ArrayList<SubTypeEntity> mSubTypeList;
    private UserDetailsEntity mUserDetailsEntityRes;

    public SubTypeAdapter(Context activity, ArrayList<SubTypeEntity> subTypeList) {
        mContext = activity;
        mSubTypeList = subTypeList;
    }

    @NonNull
    @Override
    public SubTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gridview, parent, false);
        return new SubTypeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SubTypeAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final SubTypeEntity subTypeEntity = mSubTypeList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

            holder.mGridViewTxt.setText(subTypeEntity.getNameInArabic());
        }else {
            holder.mGridViewTxt.setText(subTypeEntity.getNameInEnglish());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(mSubTypeList.get(position).getId());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    AppConstants.DRESS_SUB_TYPE_NAME = mSubTypeList.get(position).getNameInArabic();

                }else {
                    AppConstants.DRESS_SUB_TYPE_NAME = mSubTypeList.get(position).getNameInEnglish();

                }
                ((BaseActivity) mContext).nextScreen(OrderTypeScreen.class, true);
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/DressSubType/"+subTypeEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
            Log.e(AppConstants.TAG, ex.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return mSubTypeList.size();
    }

    public void filterList(ArrayList<SubTypeEntity> filterdNames) {
        mSubTypeList = filterdNames;
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_view_txt)
        TextView mGridViewTxt;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

