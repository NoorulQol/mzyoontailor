package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.DressTypeSkillUpdate;
import com.qoltech.mzyoontailor.entity.GenderEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.DressTypeScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderAdapter extends RecyclerView.Adapter<GenderAdapter.Holder> {

    private Context mContext;
    private ArrayList<GenderEntity> mGenderList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public GenderAdapter(Context activity, ArrayList<GenderEntity> genderList) {
        mContext = activity;
        mGenderList = genderList;
    }

    @NonNull
    @Override
    public GenderAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gender_list, parent, false);
        return new GenderAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GenderAdapter.Holder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final GenderEntity genderEntity = mGenderList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.mGridLeftViewTxt.setText(genderEntity.getGenderInArabic());
            holder.mGridRightViewTxt.setText(genderEntity.getGenderInArabic());
        } else {
            holder.mGridLeftViewTxt.setText(genderEntity.getGender());
            holder.mGridRightViewTxt.setText(genderEntity.getGender());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppConstants.GENDER_SCREEN_SKILLUPDATE.equals("SKILL_UPDATE_GENDER")) {


                    AppConstants.GENDER_ID = String.valueOf(genderEntity.getId());
                    mContext.startActivity(new Intent(mContext, DressTypeSkillUpdate.class));
//                    AppConstants.GENDER_SCREEN_SKILLUPDATE = "";
                } else {

                    AppConstants.GENDER_ID = String.valueOf(genderEntity.getId());
                    AppConstants.GENDER_NAME = genderEntity.getGender();
                    AppConstants.OLD_FILTER_GENDER = genderEntity.getGender();
                    AppConstants.NEW_FILTER_GENDER = "";
                    AppConstants.OLD_FILTER_OCCASION = "NONE";
                    AppConstants.NEW_FILTER_OCCASION = "";
                    AppConstants.OLD_FILTER_REGION = "NONE";
                    AppConstants.NEW_FILTER_REGION = "";
                    AppConstants.OCCASTION_LIST_BOOL = "OLD";
                    AppConstants.REGION_LIST_BOOL = "OLD";
                    ((BaseActivity) mContext).nextScreen(DressTypeScreen.class, true);
                }
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL + "images/" + genderEntity.getImageURL())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        if (position % 2 == 1) {
            holder.mGridLeftViewTxt.setVisibility(View.VISIBLE);
            holder.mGridRightViewTxt.setVisibility(View.GONE);
        } else {
            holder.mGridLeftViewTxt.setVisibility(View.GONE);
            holder.mGridRightViewTxt.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return mGenderList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.adapter_gender_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.adapter_left_gender_txt)
        TextView mGridLeftViewTxt;

        @BindView(R.id.adapter_right_gender_txt)
        TextView mGridRightViewTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
