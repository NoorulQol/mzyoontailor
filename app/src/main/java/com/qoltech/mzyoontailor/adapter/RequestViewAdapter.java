package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.fragments.Cusumization_request_fragment;
import com.qoltech.mzyoontailor.fragments.Measurement_request_fragment;

public class RequestViewAdapter extends FragmentPagerAdapter {

    private Context mContext;

    public RequestViewAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new Cusumization_request_fragment();
        } else {
            return new Measurement_request_fragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.customization);
            case 1:
                return mContext.getString(R.string.measurement);
            default:
                return null;
        }
    }

}
