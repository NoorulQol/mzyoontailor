package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.ViewMaterialImages;
import com.qoltech.mzyoontailor.entity.MaterialImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.List;

public class SkillUpdateGetMaterialImageAdapter extends RecyclerView.Adapter<SkillUpdateGetMaterialImageAdapter.ViewHolder> {

    List<MaterialImagesSkillUpdateGetEntity> items;
    private Context context;
    ApiService restService;
    SharedPreferences sharedPreferences;

    public SkillUpdateGetMaterialImageAdapter(Context context, List<MaterialImagesSkillUpdateGetEntity> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public SkillUpdateGetMaterialImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_only_img_grid, parent, false);
        SkillUpdateGetMaterialImageAdapter.ViewHolder viewHolder = new SkillUpdateGetMaterialImageAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SkillUpdateGetMaterialImageAdapter.ViewHolder holder, final int position) {
        final MaterialImagesSkillUpdateGetEntity item = items.get(position);

        try {

            Glide.with(context).load(GlobalData.SERVER_URL + "images/MaterialImages/" + item.getImage())
                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(holder.gender_image);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, ViewMaterialImages.class));
                AppConstants.SET_MATERIAL_IMAGE=item.getImage();

            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(MaterialImagesSkillUpdateGetEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(MaterialImagesSkillUpdateGetEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        ImageView gender_image,grid_view_only_img_close_img;
        TextView text_skill_update;
        //        Switch switch_skill_update;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout = (LinearLayout) itemView;
            gender_image = itemView.findViewById(R.id.grid_view_only_img);

            grid_view_only_img_close_img=itemView.findViewById(R.id.grid_view_only_img_close_img);

            grid_view_only_img_close_img.setVisibility(View.GONE);
        }

        public LinearLayout getMainView() {
            return linearLayout;
        }
    }

//
}
