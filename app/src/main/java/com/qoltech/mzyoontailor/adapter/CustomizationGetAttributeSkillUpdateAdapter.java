package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.CustumizationAttributeDetailsSkillUpdateActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.CustumizationSkillUpdateGetAttributeResult;

import java.util.ArrayList;
import java.util.List;


public class CustomizationGetAttributeSkillUpdateAdapter extends
        RecyclerView.Adapter<CustomizationGetAttributeSkillUpdateAdapter.ViewHolder> {

    List<CustumizationSkillUpdateGetAttributeResult> items;
    private Context context;
    private PositionUpdateListener listener;
    ApiService restService;
    SharedPreferences sharedPreferences;
    ArrayList<String> mSubId = new ArrayList<>();
    private UserDetailsEntity mUserDetailsEntityRes;
    public static List<CustumizationSkillUpdateGetAttributeResult> storeCustumizationSkillUpdateGetAttributeResults;

    CustumizationAttributeDetailsSkillUpdateActivity custumizationAttributeDetailsSkillUpdateActivity
            = new CustumizationAttributeDetailsSkillUpdateActivity();

    public CustomizationGetAttributeSkillUpdateAdapter(Context context, List<CustumizationSkillUpdateGetAttributeResult> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_skill_update_custumizatio_check_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final CustumizationSkillUpdateGetAttributeResult item = items.get(position);
        holder.view.setTag(item);


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.text_skill_update.setText(item.getAttributeNameinArabic());
        } else {
            holder.text_skill_update.setText(item.getAttributeNameInEnglish());

        }
        for (int i=0; i<items.size(); i++)
        {
            items.get(i).setSwitch(false);
        }
        for (int i = 0; i <  items.size(); i++) {
            for(int j=0; j<AppConstants.CUSTOM_ID.size(); j++){
                if (AppConstants.CUSTOM_ID.get(j).equalsIgnoreCase(String.valueOf(items.get(i).getId()))){
                    items.get(i).setSwitch(true);

                }
            }
        }

        try {

            Glide.with(context).load(GlobalData.SERVER_URL + "images/Customization3/" + item.getAttributeImage())
                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(holder.gender_image);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.check_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.check_box.isChecked()) {
                    AppConstants.CUSTOM_ID.add(String.valueOf(item.getId()));

                    AppConstants.ATTRIBUTE_ID_LIST.add(String.valueOf(item.getCustomizationAttributeId()));

                } else {

                    AppConstants.CUSTOM_ID.remove(String.valueOf(item.getId()));

                    AppConstants.ATTRIBUTE_ID_LIST.remove(String.valueOf(item.getCustomizationAttributeId()));

                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.check_box.setChecked(items.get(position).getSwitch());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(CustumizationSkillUpdateGetAttributeResult item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(CustumizationSkillUpdateGetAttributeResult item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView gender_image;
        TextView text_skill_update;
        View view;
        AppCompatCheckBox check_box;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            gender_image = itemView.findViewById(R.id.gender_image);
            text_skill_update = itemView.findViewById(R.id.text_skill_update);
            check_box = itemView.findViewById(R.id.check_box);
        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }


}