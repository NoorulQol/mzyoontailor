package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.TailorListEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderApprovalTailorAdapter extends RecyclerView.Adapter<OrderApprovalTailorAdapter.Holder> {

    private Context mContext;
    private ArrayList<TailorListEntity> mOrderApprovalTailorList;

    public OrderApprovalTailorAdapter(Context activity, ArrayList<TailorListEntity> OrderApprovalTailorList) {
        mContext = activity;
        mOrderApprovalTailorList = OrderApprovalTailorList;
    }

    @NonNull
    @Override
    public OrderApprovalTailorAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_approval_fetch_list, parent, false);
        return new OrderApprovalTailorAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderApprovalTailorAdapter.Holder holder, final int position) {
        final TailorListEntity orderApprovalTailorEntity = mOrderApprovalTailorList.get(position);


        holder.mOrderApprovalHeadingTxt.setText("Tailor"+String.valueOf(position+1));
        holder.mOrderApprovalTxt.setText(orderApprovalTailorEntity.getShopNameInEnglish());

            holder.mAdapterOrderApprovalImg.setImageResource(R.drawable.summary_tailor);

    }

    @Override
    public int getItemCount() {
        return mOrderApprovalTailorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_order_approval_img)
        ImageView mAdapterOrderApprovalImg;

        @BindView(R.id.order_approval_heading_txt)
        TextView mOrderApprovalHeadingTxt;

        @BindView(R.id.adapter_order_approval_txt)
        TextView mOrderApprovalTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
