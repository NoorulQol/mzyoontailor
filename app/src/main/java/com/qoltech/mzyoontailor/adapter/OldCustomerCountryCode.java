package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.CustomerTypeActivity;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.Country;

import java.util.List;

public class OldCustomerCountryCode extends RecyclerView.Adapter<OldCustomerCountryCode.ViewHolder> {

    Context context;

    private List<Country> countries;
    Country COUNTRY_ID;
    private PositionUpdateListener listener;
    Dialog dialog;
    CustomerTypeActivity customerTypeActivity;

    public OldCustomerCountryCode(Context context, List<Country> countries, Country id, Dialog dialog) {
        this.countries = countries;
        this.context = context;
        this.COUNTRY_ID = id;
        this.dialog = dialog;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Country item = countries.get(position);

        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstants.COUNTRY_FLAG = countries.get(position).getFlag();
                Country country = (Country) v.getTag();
                if (listener != null) {
                    customerTypeActivity.onUserTappedCountry(countries.get(position));
                }
                customerTypeActivity.textCountrySelection.setText(country.getPhoneCode());
                Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + country.getFlag())
                        .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.placeholder)).into(customerTypeActivity.flagsImg);
                if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    dialog.dismiss();

                }
            }
        });

        holder.view.setTag(item);
        holder.countryName.setText(item.getCountryName());
        holder.countryName.setSelected(true);
        Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + item.getFlag())
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)).into(holder.flagsImg);
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public void add(Country item, int position) {
        countries.add(position, item);
        notifyItemInserted(position);
    }


    public void add(Country item) {
        countries.add(item);
        notifyItemInserted(countries.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView flagsImg;
        TextView countryName;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            flagsImg = itemView.findViewById(R.id.flagsImg);
            countryName = itemView.findViewById(R.id.countryName);

        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }
}