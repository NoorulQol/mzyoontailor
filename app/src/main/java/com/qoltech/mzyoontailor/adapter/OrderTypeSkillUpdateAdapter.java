package com.qoltech.mzyoontailor.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.MaterialMappingSkillUpdateActivity;
import com.qoltech.mzyoontailor.entity.InsertOrderTypeSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.OrderTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.modal.InsertOrderTypeSkillUpdateModal;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OrderTypeSkillUpdateAdapter extends RecyclerView.Adapter<OrderTypeSkillUpdateAdapter.ViewHolder> {

    List<OrderTypeEntity> items;
    private Context context;
    private PositionUpdateListener listener;
    ApiService restService;
    SharedPreferences sharedPreferences;

    ArrayList<String> mOrderIdList = new ArrayList<>();
    UserDetailsEntity mUserDetailsEntityRes;

    public OrderTypeSkillUpdateAdapter(Context context, List<OrderTypeEntity> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_check_list_one, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

//        AppConstants.SKILL_ORDER_ID = new ArrayList<>();


        final OrderTypeEntity item = items.get(position);
        holder.view.setTag(item);

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.check_text.setText(items.get(position).getHeaderInArabic());
        } else {
            holder.check_text.setText(items.get(position).getHeaderInEnglish());

        }


        if (item.get_switch() == true) {
            holder.check_box.setChecked(true);
            mOrderIdList.add(String.valueOf(items.get(position).getId()));
            AppConstants.SKILL_ORDER_ID = mOrderIdList;


        }
        if (AppConstants.MATERIAL_MAP_SIZE == 0) {

            if (position == 2) {
                holder.check_box.setEnabled(false);
//                holder.check_box.setEnabled(false);
//
//
//                holder.check_box.setChecked(false);


            }


            if (mOrderIdList.contains("3")) {
                mOrderIdList.remove(String.valueOf(items.get(2).getId()));
                insert();
            }
        }


        else if (AppConstants.MATERIAL_MAP_SIZE >0){
            insert();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppConstants.MATERIAL_MAP_SIZE == 0 && position == 2) {
                    context.startActivity(new Intent(context, MaterialMappingSkillUpdateActivity.class));
                    ((Activity)context).finish();
                }
            }
        });

        holder.check_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.check_box.isChecked() == true) {
                    mOrderIdList.add(String.valueOf(items.get(position).getId()));

//                    if (AppConstants.MATERIAL_MAP_SIZE == 0) {
//                        holder.check_box.setEnabled(false);
//                    }

                } else {
                    mOrderIdList.remove(String.valueOf(items.get(position).getId()));

//                    if (AppConstants.MATERIAL_MAP_SIZE == 0) {
//                        holder.check_box.setEnabled(false);
//                        mOrderIdList.remove(String.valueOf(items.get(2).getId()));
//                    }

                }
                AppConstants.SKILL_ORDER_ID = new ArrayList<>();
                AppConstants.SKILL_ORDER_ID = mOrderIdList;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(OrderTypeEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(OrderTypeEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main, circle_image_layout;
        ImageView gender_image;
        TextView check_text;
        AppCompatCheckBox check_box;
        View view;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout = (LinearLayout) itemView;
//            gender_image = itemView.findViewById(R.id.images);
            check_text = itemView.findViewById(R.id.check_text);
            check_box = itemView.findViewById(R.id.check_box);

//            final int sdk = android.os.Build.VERSION.SDK_INT;
//            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                circle_image_layout.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.circle_orange_border));
//            } else {
//                circle_image_layout.setBackground(ContextCompat.getDrawable(context, R.drawable.circle_orange_border));
//            }
        }

        public LinearLayout getMainView() {
            return linearLayout;
        }
    }

    public void insert() {



        ArrayList<InsertOrderTypeSkillUpdateEntity> insertOrderTypeSkillUpdateEntities = new ArrayList<>();

        for (int i = 0; i < AppConstants.SKILL_ORDER_ID.size(); i++) {
            InsertOrderTypeSkillUpdateEntity insertOrderTypeSkillUpdateEntity = new InsertOrderTypeSkillUpdateEntity();
            insertOrderTypeSkillUpdateEntity.setId(AppConstants.SKILL_ORDER_ID.get(i));
            insertOrderTypeSkillUpdateEntities.add(insertOrderTypeSkillUpdateEntity);
        }


        InsertOrderTypeSkillUpdateModal insertOrderTypeSkillUpdateApiModal = new InsertOrderTypeSkillUpdateModal();
        insertOrderTypeSkillUpdateApiModal.setTailorId(sharedPreferences.getString("TailorId", ""));
        insertOrderTypeSkillUpdateApiModal.setOrderTypeId(insertOrderTypeSkillUpdateEntities);

        restService.insertOrderTypeSkillUpdate(insertOrderTypeSkillUpdateApiModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


//                        dialogManager.hideProgress();
//                        Toast.makeText(getApplicationContext(), R.string.order_type_updated_sucessfully, Toast.LENGTH_LONG).show();
//                        finish();
                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });

    }

}