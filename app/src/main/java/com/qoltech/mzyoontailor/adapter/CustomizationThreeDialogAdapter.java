package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.CustomizationThreeScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationThreeDialogAdapter extends  RecyclerView.Adapter<CustomizationThreeDialogAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationAttributesEntity> mCustomizationList;
    private Dialog mDialog;

    private UserDetailsEntity mUserDetailsEntityRes;

    public CustomizationThreeDialogAdapter(Context activity, ArrayList<CustomizationAttributesEntity> getCustomizationList, Dialog dialog) {
        mContext = activity;
        mCustomizationList = getCustomizationList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public CustomizationThreeDialogAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_customization_three_list, parent, false);
        return new CustomizationThreeDialogAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationThreeDialogAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationAttributesEntity getCustomizationEntity = mCustomizationList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mCustomizationThreeTxtViewTxt.setText(getCustomizationEntity.getAttributeNameinArabic());

        }else {
            holder.mCustomizationThreeTxtViewTxt.setText(getCustomizationEntity.getAttributeNameInEnglish());

        }

        for (int i=0; i<AppConstants.CUSTOMIZATION_CLICKED_ARRAY.size(); i++){
            for (int j=0; j<mCustomizationList.size(); j++){
                if (AppConstants.CUSTOMIZATION_CLICKED_ARRAY.get(i).equalsIgnoreCase(String.valueOf(mCustomizationList.get(j).getId()))){
                    mCustomizationList.get(j).setChecked(true);
                }
            }

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CUSTOMIZATION_DRESS_TYPE_ID = String.valueOf(getCustomizationEntity.getId());
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    ((CustomizationThreeScreen)mContext).mCustomizationThreeSpinnerTxt.setText(getCustomizationEntity.getAttributeNameinArabic());

                }else {
                    ((CustomizationThreeScreen)mContext).mCustomizationThreeSpinnerTxt.setText(getCustomizationEntity.getAttributeNameInEnglish());

                }
                AppConstants.CUSTOMIZATION_NAME = getCustomizationEntity.getAttributeNameInEnglish();

                ((CustomizationThreeScreen)mContext).getCustomizationThreeNewFlowApiCall();






                mDialog.dismiss();
            }
        });

        holder.mCustimzationThreegImg.setVisibility(mCustomizationList.get(position).getChecked() ? View.VISIBLE : View.GONE);

//        holder.mCustomizationThreeTxtViewTxt.setTextColor(mCustomizationList.get(position).getChecked() ? mContext.getResources().getColor(R.color.app_color) : mContext.getResources().getColor(R.color.orange_color));

    }

    @Override
    public int getItemCount() {
        return mCustomizationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.customization_three_txt)
        TextView mCustomizationThreeTxtViewTxt;

        @BindView(R.id.customization_three_img)
        ImageView mCustimzationThreegImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
