package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetAreaEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;


public class AreaGetSkillUpdateAdapter extends RecyclerView.Adapter<AreaGetSkillUpdateAdapter.ViewHolder> {

    List<GetAreaEntity> items;
    private Context context;
    private PositionUpdateListener listener;
    ApiService restService;
    SharedPreferences sharedPreferences;
    ArrayList<String> mSubId = new ArrayList<>();
    private UserDetailsEntity mUserDetailsEntityRes;
    public static boolean isSelectedAll;

    public AreaGetSkillUpdateAdapter(Context context, List<GetAreaEntity> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_check_list_one, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
//        mSubId = new ArrayList<>();
//        AppConstants.SKILL_SUB_DRESS_ID = new ArrayList<>();
        GetAreaEntity item = items.get(position);
        holder.view.setTag(item);


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.check_text.setText(item.getArea());
        } else {
            holder.check_text.setText(item.getArea());

        }


//        if (item.get_switch() == true) {
//            holder.check_box.setChecked(true);
//            mSubId.add(String.valueOf(items.get(position).getId()));
//            AppConstants.SKILL_LOCATION_ID = mSubId;
//
//
//        }

        for (int i = 0; i < items.size(); i++) {
            items.get(i).set_switch(false);
        }
        for (int i = 0; i < items.size(); i++) {
            for (int j = 0; j < AppConstants.SKILL_LOCATION_ID.size(); j++) {
                if (AppConstants.SKILL_LOCATION_ID.get(j).equalsIgnoreCase(String.valueOf(items.get(i).getId()))) {
                    items.get(i).set_switch(true);

                }
            }
        }


        holder.check_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (holder.check_box.isChecked() == true) {
                    AppConstants.SKILL_LOCATION_ID.add(String.valueOf(items.get(position).getId()));

                } else {
                    AppConstants.SKILL_LOCATION_ID.remove(String.valueOf(items.get(position).getId()));
                }
//                AppConstants.SKILL_LOCATION_ID = new ArrayList<>();
//                AppConstants.SKILL_LOCATION_ID = mSubId;

            }
        });

        holder.check_box.setChecked(items.get(position).get_switch());





    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetAreaEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetAreaEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        TextView check_text;
        //        Switch switch_skill_update;
        View view;
        AppCompatCheckBox check_box;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout = (LinearLayout) itemView;
            check_text = itemView.findViewById(R.id.check_text);
            check_box = itemView.findViewById(R.id.check_box);
//            switch_skill_update = itemView.findViewById(R.id.switch_skill_update);
        }

        public LinearLayout getMainView() {
            return linearLayout;
        }
    }
    public void selectAll(){
        Log.e("onClickSelectAll","yes");
        isSelectedAll=true;
        notifyDataSetChanged();
    }

    public void unselectAll(){
        Log.e("onClickSelectAll","yes");
        isSelectedAll=false;
        notifyDataSetChanged();
    }
}