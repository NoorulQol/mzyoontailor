package com.qoltech.mzyoontailor.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ViewSliderAdapter extends PagerAdapter {

	ArrayList<View> productImgs = new ArrayList<View>();

	public ViewSliderAdapter(ArrayList<View> views) {
		this.productImgs = views;
	}

	@Override
	public int getItemPosition(Object object) {
		int index = productImgs.indexOf(object);
		if (index == -1)
			return POSITION_NONE;
		else
			return index;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View v = productImgs.get(position);
		container.addView(v);
		return v;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView(productImgs.get(position));
	}

	@Override
	public int getCount() {
		return productImgs.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}

	public int addView(View v) {
		return addView(v, productImgs.size());
	}

	public int addView(View v, int position) {
		productImgs.add(position, v);
		return position;
	}

	public int removeView(ViewPager pager, View v) {
		return removeView(pager, productImgs.indexOf(v));
	}

	public int removeView(ViewPager pager, int position) {
		pager.setAdapter(null);
		productImgs.remove(position);
		pager.setAdapter(this);

		return position;
	}

	public View getView(int position) {
		return productImgs.get(position);
	}

}