package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.CustomizationDetailsSkillUpdate;
import com.qoltech.mzyoontailor.activity.OrderConfirmationActivity;
import com.qoltech.mzyoontailor.fragments.MaterialUsedFragment;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AdapterOneToNine extends RecyclerView.Adapter<AdapterOneToNine.ViewHolder> {

    Context context;
    MaterialUsedFragment materialUsedFragment = new MaterialUsedFragment();
    private ArrayList<String> mDataset;
    CustomizationDetailsSkillUpdate customizationDetailsSkillUpdate = new CustomizationDetailsSkillUpdate();
    OrderConfirmationActivity orderConfirmationActivity = new OrderConfirmationActivity();

    public AdapterOneToNine(Context context, ArrayList<String> mDataset) {
        this.mDataset = mDataset;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.material_used_recyclerview_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.no_of_adapter.setText(mDataset.get(position));
        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppConstants.CLASS_NAME.equalsIgnoreCase("Skill_Update")) {


                    customizationDetailsSkillUpdate.days_required.setText(String.valueOf(mDataset.get(position)));
                    customizationDetailsSkillUpdate.mOrderConfirmDialog.dismiss();
                } else if (AppConstants.CLASS_NAME.equalsIgnoreCase("Material_Used")) {

//                    materialUsedFragment.mOrderConfirmDialog.dismiss();
//                    materialUsedFragment.quantity.setText(String.valueOf(mDataset.get(position)));
                    orderConfirmationActivity.mOrderConfirmDialog.dismiss();

                    orderConfirmationActivity.text_material_required.setText(String.valueOf(mDataset.get(position)));

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");// HH:mm:ss");
                    String reg_date = df.format(c.getTime());
//                showtoast("Currrent Date Time : " + reg_date);

//                Toast.makeText(context, "Currrent Date Time : " + reg_date, Toast.LENGTH_LONG).show();
//                    c.add(Calendar.DATE, position);  // number of days to add
//                    String end_date = df.format(c.getTime());
//                    materialUsedFragment.calender_text.setText(end_date);
//                showtoast("end Time : " + end_date);

//                Toast.makeText(context, "end Time : " + end_date, Toast.LENGTH_LONG).show();

                }
                else if (AppConstants.CLASS_NAME.equalsIgnoreCase("Order_Confirmation")){

                    orderConfirmationActivity.text_days_required.setText(String.valueOf(mDataset.get(position)));
                    orderConfirmationActivity.mOrderConfirmDialog.dismiss();
//                    materialUsedFragment.quantity.setText(String.valueOf(mDataset.get(position)));
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");// HH:mm:ss");
                    String reg_date = df.format(c.getTime());


                    c.add(Calendar.DATE, position);  // number of days to add
                    String end_date = df.format(c.getTime());
                    orderConfirmationActivity.calender_text.setText(end_date);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout_main;
        TextView no_of_adapter;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout_main = (LinearLayout) itemView;
            no_of_adapter = itemView.findViewById(R.id.no_of_adapter);

        }

        public LinearLayout getMainView() {
            return linearLayout_main;
        }
    }
}