package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.AddMaterialScreen;
import com.qoltech.mzyoontailor.ui.AddReferenceScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMaterialAdapter extends RecyclerView.Adapter<AddMaterialAdapter.Holder> {

    private Context mContext;
    //    private ArrayList<GenderEntity> mGenderList;
    private ArrayList<String> mImageList;
    private UserDetailsEntity mUserDetailsEntityRes;
//    private CitySearchFragment mCitySearchFragment;

    public AddMaterialAdapter(Context activity, ArrayList<String> imageList) {
        mContext = activity;
        mImageList = imageList;
    }

    @NonNull
    @Override
    public AddMaterialAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_only_img_grid, parent, false);
        return new AddMaterialAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddMaterialAdapter.Holder holder, final int position) {
//        final SubTypeEntity subTypeEntity = mSubTypeList.get(position);
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
                    try {
                        Glide.with(mContext)
                                .load(mImageList.get(position))
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(((AddMaterialScreen)mContext).mAddMaterialImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
                    try {
                        Glide.with(mContext)
                                .load(mImageList.get(position))
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(((AddReferenceScreen)mContext).mAddReferenceImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }

            }
        });

        if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
            try {
                Glide.with(mContext)
                        .load(mImageList.get(position))
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(((AddMaterialScreen)mContext).mAddMaterialImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
            try {
                Glide.with(mContext)
                        .load(mImageList.get(position))
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(((AddReferenceScreen)mContext).mAddReferenceImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }

        try {
            Glide.with(mContext)
                    .load(mImageList.get(position))
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
            Log.e(AppConstants.TAG, ex.getMessage());
        }


        holder.mGridViewOnlyDeleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManager.getInstance().showOptionPopup(mContext,
                        mContext.getResources().getString(R.string.delete_img),
                        mContext.getResources().getString(R.string.yes),
                        mContext.getResources().getString(R.string.no),
                        mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                            @Override
                            public void onNegativeClick() {

                            }

                            @Override
                            public void onPositiveClick() {
                                mImageList.remove(mImageList.get(position));
                                notifyDataSetChanged();
                                if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
                                    if (mImageList.size()>0){
                                        ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.GONE);

                                    }else {
                                        ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.VISIBLE);

                                    }
                                    try {
                                        Glide.with(mContext)
                                                .load(R.drawable.grey_bg)
                                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                                .into(((AddMaterialScreen)mContext).mAddMaterialImg);

                                    } catch (Exception ex) {
                                        Log.e(AppConstants.TAG, ex.getMessage());
                                    }
                                }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
                                    if (mImageList.size()>0){
                                        ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.GONE);

                                    }else {
                                        ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.VISIBLE);

                                    }

                                    try {
                                        Glide.with(mContext)
                                                .load(R.drawable.grey_bg)
                                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                                .into(((AddReferenceScreen)mContext).mAddReferenceImg);

                                    } catch (Exception ex) {
                                        Log.e(AppConstants.TAG, ex.getMessage());
                                    }
                                }


                                else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_SHOP_PHOTOS")){
                                    try {
                                        Glide.with(mContext)
                                                .load(R.drawable.grey_bg)
                                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                                .into(((AddReferenceScreen)mContext).mAddReferenceImg);

                                    } catch (Exception ex) {
                                        Log.e(AppConstants.TAG, ex.getMessage());
                                    }
                                }
                            }
                        });

            }
        });

//        holder.mGridViewOnlyDeleteImg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mImageList.remove(mImageList.get(position));
//                notifyDataSetChanged();
//
//                if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
//                    if (mImageList.size()>0){
//                        ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.GONE);
//
//                    }else {
//                        ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.VISIBLE);
//
//                    }
//                    try {
//                        Glide.with(mContext)
//                                .load(R.drawable.grey_bg)
//                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                                .into(((AddMaterialScreen)mContext).mAddMaterialImg);
//
//                    } catch (Exception ex) {
//                        Log.e(AppConstants.TAG, ex.getMessage());
//                    }
//                }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")){
//                    if (mImageList.size()>0){
//                        ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.GONE);
//
//                    }else {
//                        ((AddReferenceScreen)mContext).mNoImagesTxt.setVisibility(View.VISIBLE);
//
//                    }
//                    try {
//                        Glide.with(mContext)
//                                .load(R.drawable.grey_bg)
//                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                                .into(((AddReferenceScreen)mContext).mAddReferenceImg);
//
//                    } catch (Exception ex) {
//                        Log.e(AppConstants.TAG, ex.getMessage());
//                    }
//                }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_SHOP_PHOTOS")){
//                    try {
//                        Glide.with(mContext)
//                                .load(R.drawable.grey_bg)
//                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                                .into(((AddReferenceScreen)mContext).mAddReferenceImg);
//
//                    } catch (Exception ex) {
//                        Log.e(AppConstants.TAG, ex.getMessage());
//                    }
//                }
//        }
//        });

        if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_MATERIAL")){
            if (mImageList.size()>0){
                ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.GONE);

            }else {
                ((AddMaterialScreen)mContext).mNoImagesShown.setVisibility(View.VISIBLE);

            }
        }else if (AppConstants.ADD_MATERIAL.equalsIgnoreCase("ADD_REFERENCE")) {
            if (mImageList.size() > 0) {
                ((AddReferenceScreen) mContext).mNoImagesTxt.setVisibility(View.GONE);

            } else {
                ((AddReferenceScreen) mContext).mNoImagesTxt.setVisibility(View.VISIBLE);

            }
        }


        holder.mGridViewOnlyDeleteImg.setVisibility(View.VISIBLE);
//
    }

    @Override
    public int getItemCount() {
        return mImageList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_only_img_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_view_only_img)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_view_only_img_close_img)
        ImageView mGridViewOnlyDeleteImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

