package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.fragments.QuotationPendingListFragment;
import com.qoltech.mzyoontailor.fragments.QuotationRejectListFragment;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.lang.reflect.Field;

public class QuotationListAdapter extends FragmentPagerAdapter {

    private Context mContext;
    public UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    public String json;
    public QuotationListAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        gson = new Gson();
        json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new QuotationPendingListFragment();
        } else {
            return new QuotationRejectListFragment();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
//        switch (position) {
//            case 0:
//                return mContext.getString(R.string.buyer_title);
//            case 1:
//                return mContext.getString(R.string.supplier_title);
//            default:
//                return null;
//        }


        switch (position) {
            case 0:
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    return "مشتر";
                } else {
                    return "PENDING";

                }


            case 1:
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    return "المورد";
                } else {
                    return "REJECTED";

                }
            default:
                return null;
        }
    }



    public void setIndicator (TabLayout tabs, int leftDip, int rightDip){
        Class<?> tabLayout = tabs.getClass();
        Field tabStrip = null;
        try {
            tabStrip = tabLayout.getDeclaredField("mTabStrip");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        tabStrip.setAccessible(true);
        LinearLayout llTab = null;
        try {
            llTab = (LinearLayout) tabStrip.get(tabs);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        int left = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, leftDip,
                Resources.getSystem().getDisplayMetrics());
        int right = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, rightDip,
                Resources.getSystem().getDisplayMetrics());

        for (int i = 0; i < llTab.getChildCount(); i++) {
            View child = llTab.getChildAt(i);
            child.setPadding(0, 0, 0, 0);
            LinearLayout.LayoutParams params =
                    new LinearLayout.LayoutParams(0,
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            1);
            params.leftMargin = left;
            params.rightMargin = right;
            child.setLayoutParams(params);
            child.invalidate();
        }


    }

}
