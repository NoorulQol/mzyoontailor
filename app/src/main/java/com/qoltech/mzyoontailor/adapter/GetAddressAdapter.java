package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetAddressEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.ui.AddAddressScreen;
import com.qoltech.mzyoontailor.ui.AddressScreen;
import com.qoltech.mzyoontailor.ui.ServiceTypeScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetAddressAdapter extends RecyclerView.Adapter<GetAddressAdapter.Holder> {

private Context mContext;
private ArrayList<GetAddressEntity> mAddressList;
        AddressScreen mAddressScreen;
        String mUserId;
private UserDetailsEntity mUserDetailsEntityRes;

public GetAddressAdapter(Context activity, ArrayList<GetAddressEntity> addressList, AddressScreen addressScreen, String userId) {
        mContext = activity;
        mAddressList = addressList;
        mAddressScreen = addressScreen;
        mUserId = userId;
        }

@NonNull
@Override
public GetAddressAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_get_address, parent, false);
        return new GetAddressAdapter.Holder(view);
        }

@Override
public void onBindViewHolder(@NonNull final GetAddressAdapter.Holder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

final GetAddressEntity getAddressEntity = mAddressList.get(position);

        holder.mGetAddressUserNameTxt.setText(getAddressEntity.getFirstName());

        holder.mGetAddressTxt.setText(getAddressEntity.getArea()+", "+getAddressEntity.getStateName()+", "+getAddressEntity.getName()+".");

        holder.mGetAddressPhoneNumTxt.setText(getAddressEntity.getPhoneNo());
        holder.mGetAddressMakeAsDefaultTxt.setVisibility(getAddressEntity.isDefault() == true ? View.VISIBLE : View.GONE);
        holder.mGetAddressLocationTxt.setText(getAddressEntity.getLocationType());

        holder.mAddressAdapParLay.setBackgroundResource(getAddressEntity.isDefault() == true ? R.drawable.orange_border : R.color.white);

        holder.mEditLay.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        AppConstants.EDIT_ADDRESS = "EDIT_ADDRESS";
        AppConstants.CHECK_ADD_ADDRESS = "ADD_ADDRESS_EDIT";
        AppConstants.GET_ADDRESS_ID = String.valueOf(getAddressEntity.getId());
        ((BaseActivity)mContext).nextScreen(AddAddressScreen.class,true);
        }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        AppConstants.GET_ADDRESS_ID = String.valueOf(getAddressEntity.getId());
        if (AppConstants.SERVICE_TYPE.equalsIgnoreCase("SERVICE_TYPE")){
        AppConstants.CHECK_ADD_ADDRESS = "";
        AppConstants.PENDING_DELIVERY_CLICK = "";
        AppConstants.COUNTRY_AREA_ID = String.valueOf(getAddressEntity.getAreaId());

        ((BaseActivity) mContext).nextScreen(ServiceTypeScreen.class, true);

        }

        }
        });

        holder.mDeleteLay.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        DialogManager.getInstance().showOptionPopup(mContext, "Are you sure you want to delete this address?", "Yes", "No",mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
@Override
public void onNegativeClick() {

        }

@Override
public void onPositiveClick() {
        if (NetworkUtil.isNetworkAvailable(mContext)){
        deleteAddressApi(mUserId,String.valueOf(getAddressEntity.getId()));

        }else {
        DialogManager.getInstance().showNetworkErrorPopup(mContext, new InterfaceBtnCallBack() {
@Override
public void onPositiveClick() {
        deleteAddressApi(mUserId,String.valueOf(getAddressEntity.getId()));
        }
        });
        }
        }
        });

        }
        });

        }

public void deleteAddressApi(String buyerId, String Id){
        APIRequestHandler.getInstance().deleteAddressApiCall(AppConstants.DIRECT_USERS_ID,Id,mAddressScreen);
        }
@Override
public int getItemCount() {
        return mAddressList.size();
        }

public class Holder extends RecyclerView.ViewHolder {

    @BindView(R.id.get_address_edit_lay)
    LinearLayout mEditLay;

    @BindView(R.id.get_address_delete_lay)
    LinearLayout mDeleteLay;

    @BindView(R.id.get_address_user_name_txt)
    TextView mGetAddressUserNameTxt;

    @BindView(R.id.get_address_txt)
    TextView mGetAddressTxt;

    @BindView(R.id.get_address_phone_num)
    TextView mGetAddressPhoneNumTxt;

    @BindView(R.id.address_default_address_lay)
    RelativeLayout mGetAddressMakeAsDefaultTxt;

    @BindView(R.id.get_address_location_txt)
    TextView mGetAddressLocationTxt;

    @BindView(R.id.address_adap_par_lay)
    LinearLayout mAddressAdapParLay;

    public Holder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
}


