package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.LocationSkillUpdateGetAreaActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.State;

import java.util.ArrayList;
import java.util.List;


public class StateGetSkillUpdateAdapter extends RecyclerView.Adapter<StateGetSkillUpdateAdapter.ViewHolder> {

    List<State> items;
    private Context context;
    private PositionUpdateListener listener;
    ApiService restService;
    SharedPreferences sharedPreferences;
    ArrayList<String> mSubId;
private  UserDetailsEntity mUserDetailsEntityRes;
    public StateGetSkillUpdateAdapter(Context context, List<State> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_custumization_with_arrow_list_one, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
//        mSubId = new ArrayList<>();
//        AppConstants.SKILL_SUB_DRESS_ID = new ArrayList<>();
        final State item = items.get(position);
        holder.view.setTag(item);
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.text_skill_update.setText(item.getStateName());
        } else {
            holder.text_skill_update.setText(item.getStateName());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        AppConstants.STATE_ID =String.valueOf(item.getId());

                context.startActivity(new Intent(context, LocationSkillUpdateGetAreaActivity.class));

            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(State item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(State item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        TextView text_skill_update;
        //        Switch switch_skill_update;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            text_skill_update = itemView.findViewById(R.id.text_skill_update);
//            switch_skill_update = itemView.findViewById(R.id.switch_skill_update);
        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }


}