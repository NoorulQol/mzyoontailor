package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetMeasurementImageEntity;
import com.qoltech.mzyoontailor.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

//import com.qoltech.mzyoonbuyer.R;
//import com.qoltech.mzyoonbuyer.entity.GetMeasurementImageEntity;
//import com.qoltech.mzyoonbuyer.main.BaseActivity;
//import com.qoltech.mzyoonbuyer.ui.MeasurementScalingSampleScreen;
//import com.qoltech.mzyoonbuyer.utils.AppConstants;

public class SliderImgaeAdapter extends PagerAdapter {


    private ArrayList<GetMeasurementImageEntity> IMAGES;
    private ArrayList<GetMeasurementPartEntity> mParts;
    private LayoutInflater inflater;
    private Context context;
    private Dialog mSlider;
    double num = 0.0;
    ArrayList<String> Values = new ArrayList<>();
    ArrayList<String> Inches = new ArrayList<>();
    private MeasurementScaleAdapter mMeasurementAdapter;

    ArrayList<String> mArrayList;
    HashMap<String,String> parts = new HashMap<>();
    String mCheckToAdd = "";

    private UserDetailsEntity mUserDetailsEntityRes;

    public SliderImgaeAdapter(Context context, ArrayList<GetMeasurementImageEntity> IMAGES ,ArrayList<GetMeasurementPartEntity> part) {
        this.context = context;
        this.IMAGES = IMAGES;
        mParts = part;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        updateResources(context,"en");


        View imageLayout = inflater.inflate(R.layout.adapter_measurement_front_one, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);


        final LinearLayout mMenFrontOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_men_one_required);
        final LinearLayout mMenFrontTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_man_two_requried);
        final LinearLayout mMenBackOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_man_three_requirement);
        final LinearLayout mMenBackTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_men_four_required);
        final LinearLayout mWomenFrontOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_women_one_required);
        final LinearLayout mWomenFrontTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_women_two_required);
        final LinearLayout mWomenFrontThree = (LinearLayout) imageLayout.findViewById(R.id.measurement_women_three_required);
        final LinearLayout mWomenBackOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_women_four_required);
        final LinearLayout mWomenBackTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_women_five_required);
        final LinearLayout mBoyFroneOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_boy_one_required);
        final LinearLayout mBoyFrontTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_boy_two_required);
        final LinearLayout mBoyBackOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_boy_three_required);
        final LinearLayout mBoyBackTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_boy_four_required);
        final LinearLayout mGirlFrontOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_girl_one_required);
        final LinearLayout mGirlFrontTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_girl_two_required);
        final LinearLayout mGirlFrontThree = (LinearLayout) imageLayout.findViewById(R.id.measurement_girl_three_required);
        final LinearLayout mGirlBackOne = (LinearLayout) imageLayout.findViewById(R.id.measurement_girl_four_required);
        final LinearLayout mGirlBackTwo = (LinearLayout) imageLayout.findViewById(R.id.measurement_girl_five_required);

        final RelativeLayout mHeadLay = (RelativeLayout) imageLayout.findViewById(R.id.head_click);
        final RelativeLayout mNeckLay = (RelativeLayout) imageLayout.findViewById(R.id.neck_click);
        final RelativeLayout mChestLay = (RelativeLayout) imageLayout.findViewById(R.id.chest_click);
        final RelativeLayout mWaistLay = (RelativeLayout) imageLayout.findViewById(R.id.waist_click);
        final RelativeLayout mThighLay = (RelativeLayout) imageLayout.findViewById(R.id.thigh_click);
        final RelativeLayout mKneeLay = (RelativeLayout) imageLayout.findViewById(R.id.knee_click);
        final RelativeLayout mAnkleLay = (RelativeLayout) imageLayout.findViewById(R.id.ankle_click);

        final RelativeLayout mOverAllHeightLay = (RelativeLayout) imageLayout.findViewById(R.id.over_all_height_click);
        final RelativeLayout mShortsLay = (RelativeLayout) imageLayout.findViewById(R.id.short_click);
        final RelativeLayout mOutseamLay = (RelativeLayout) imageLayout.findViewById(R.id.outseam_click);
        final RelativeLayout mInseamLay = (RelativeLayout) imageLayout.findViewById(R.id.inseam_click);

        final RelativeLayout mShoulderLay = (RelativeLayout) imageLayout.findViewById(R.id.shoulder_click);
        final RelativeLayout mHalfSleevetLay = (RelativeLayout) imageLayout.findViewById(R.id.half_sleeve_click);
        final RelativeLayout mBicepLay = (RelativeLayout) imageLayout.findViewById(R.id.bicep_click);
        final RelativeLayout mHipLay = (RelativeLayout) imageLayout.findViewById(R.id.hip_click);
        final RelativeLayout mBottomLay = (RelativeLayout) imageLayout.findViewById(R.id.bottom_click);

        final RelativeLayout mHeightLay = (RelativeLayout) imageLayout.findViewById(R.id.height_click);
        final RelativeLayout mSleeveLay = (RelativeLayout) imageLayout.findViewById(R.id.sleeve_click);
        final RelativeLayout mWristLay = (RelativeLayout) imageLayout.findViewById(R.id.wrist_click);

        final RelativeLayout mWomenOverBustLay=(RelativeLayout) imageLayout.findViewById(R.id.women_over_burst_click);
        final RelativeLayout mWomenUnderBustLay = (RelativeLayout) imageLayout.findViewById(R.id.women_under_burst_click);
        final RelativeLayout mWomenHipBoneLay = (RelativeLayout) imageLayout.findViewById(R.id.women_hip_bone_click);
        final RelativeLayout mWomenThighLay = (RelativeLayout) imageLayout.findViewById(R.id.women_thigh_click);
        final RelativeLayout mWomenKneeLay = (RelativeLayout) imageLayout.findViewById(R.id.women_knee_click);
        final RelativeLayout mWomenCalfLay = (RelativeLayout) imageLayout.findViewById(R.id.women_calf_click);
        final RelativeLayout mWomenAnkleLay = (RelativeLayout) imageLayout.findViewById(R.id.women_ankle_click);

        final RelativeLayout mWomenHeadLay = (RelativeLayout) imageLayout.findViewById(R.id.women_head_click);
        final RelativeLayout mWomenNeckLay = (RelativeLayout) imageLayout.findViewById(R.id.women_neck_click);
        final RelativeLayout mWomenBustLay = (RelativeLayout) imageLayout.findViewById(R.id.women_bust_click);
        final RelativeLayout mWomenWaistLay = (RelativeLayout) imageLayout.findViewById(R.id.women_waist_click);
        final RelativeLayout mWomenFullHipLay = (RelativeLayout) imageLayout.findViewById(R.id.women_full_hip_click);

        final RelativeLayout mWomenHeightLay = (RelativeLayout) imageLayout.findViewById(R.id.women_height_click);
        final RelativeLayout mWomenStwLay = (RelativeLayout) imageLayout.findViewById(R.id.women_stw_click);
        final RelativeLayout mWomenNltcLay = (RelativeLayout) imageLayout.findViewById(R.id.women_nltc_click);
        final RelativeLayout mWomenNltbLay = (RelativeLayout) imageLayout.findViewById(R.id.women_nltb_click);
        final RelativeLayout mWomenSthbLay = (RelativeLayout) imageLayout.findViewById(R.id.women_sthb_click);
        final RelativeLayout mWomenWthbLay = (RelativeLayout) imageLayout.findViewById(R.id.women_wthb_click);
        final RelativeLayout mWomenHthLay = (RelativeLayout) imageLayout.findViewById(R.id.women_hth_click);
        final RelativeLayout mWomenInseamLay = (RelativeLayout) imageLayout.findViewById(R.id.women_inseam_click);
        final RelativeLayout mWomenOutseamLay = (RelativeLayout) imageLayout.findViewById(R.id.women_outseam_click);

        final RelativeLayout mWomenShoulderLay = (RelativeLayout) imageLayout.findViewById(R.id.women_shoulder_click);
        final RelativeLayout mWomenBicepLay = (RelativeLayout) imageLayout.findViewById(R.id.women_bicep_click);
        final RelativeLayout mWomenWristLay = (RelativeLayout) imageLayout.findViewById(R.id.women_wrist_click);

        final RelativeLayout mWomenSleeveLay = (RelativeLayout) imageLayout.findViewById(R.id.women_sleeve_click);

        final RelativeLayout mBoyHeadLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_head_click);
        final RelativeLayout mBoyNeckLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_neck_click);
        final RelativeLayout mBoyChestLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_chest_click);
        final RelativeLayout mBoyWaistLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_waist_click);
        final RelativeLayout mBoyThighLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_thigh_click);
        final RelativeLayout mBoyKneeLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_knee_click);
        final RelativeLayout mBoyAnkleLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_ankle_click);

        final RelativeLayout mBoyOverAllHeightLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_over_all_height_click);
        final RelativeLayout mBoyShortsLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_shorts_click);
        final RelativeLayout mBoyOutseamLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_outseam_click);
        final RelativeLayout mBoyInseamLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_inseam_click);

        final RelativeLayout mBoyShoulderLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_shoulder_click);
        final RelativeLayout mBoyHalfSleeveLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_half_sleeve_click);
        final RelativeLayout mBoyBicepLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_bicep_click);
        final RelativeLayout mBoyHipLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_hip_click);
        final RelativeLayout mBoyBottomLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_bottom_click);

        final RelativeLayout mBoyHeightLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_height_click);
        final RelativeLayout mBoySleeveLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_sleeve_click);
        final RelativeLayout mBoyWristLay = (RelativeLayout) imageLayout.findViewById(R.id.boy_wrist_click);

        final RelativeLayout mGirlOverBurstLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_over_burst_click);
        final RelativeLayout mGirlUnderBurstLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_under_burst_click);
        final RelativeLayout mGirlHipBoneLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_hip_bone_click);
        final RelativeLayout mGirlThighLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_thigh_click);
        final RelativeLayout mGirlKneeLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_knee_click);
        final RelativeLayout mGirlCalfLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_calf_click);
        final RelativeLayout mGirlAnkleLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_ankle_click);

        final RelativeLayout mGirlHeadLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_head_click);
        final RelativeLayout mGirlNeckLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_neck_click);
        final RelativeLayout mGirlBustLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_bust_click);
        final RelativeLayout mGirlWaistLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_waist_click);
        final RelativeLayout mGirlFullHipLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_full_hip_click);

        final RelativeLayout mGirlHeightLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_height_click);
        final RelativeLayout mGirlStwLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_stw_click);
        final RelativeLayout mGirlNltcLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_nltc_click);
        final RelativeLayout mGirlNltbLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_nltb_click);
        final RelativeLayout mGirlSthbLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_sthb_click);
        final RelativeLayout mGirlWthbLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_wthb_click);
        final RelativeLayout mGirlHthLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_hth_click);
        final RelativeLayout mGirlInseamLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_inseam_click);
        final RelativeLayout mGirlOutseamLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_outseam_click);

        final RelativeLayout mGirlShoulderLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_shoulder_click);
        final RelativeLayout mGirlBicepLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_bicep_click);
        final RelativeLayout mGirlWristLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_wrist_click);

        final RelativeLayout mGirlSleeveLay = (RelativeLayout) imageLayout.findViewById(R.id.girl_sleeve_click);

        final TextView mHeadTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_head_txt);
        final TextView mNeckTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_neck_txt);
        final TextView mChestTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_chest_txt);
        final TextView mWaistTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_waist_txt);
        final TextView mThighTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_thigh_txt);
        final TextView mKneeTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_knee_txt);
        final TextView mAnkleTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_ankle_txt);

        final TextView mOverAllHeightTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_over_all_height_txt);
        final TextView mShortsTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_short_txt);
        final TextView mOutseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_outseam_txt);
        final TextView mInseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_inseam_txt);

        final TextView mShoulderTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_shoulder_txt);
        final TextView mHalfSleeveTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_half_sleeve_txt);
        final TextView mBicepTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_bicep_txt);
        final TextView mHipTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_hip_txt);
        final TextView mBottomTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_bottom_txt);

        final TextView mHeightTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_height_txt);
        final TextView mSleeveTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_sleeve_txt);
        final TextView mWristTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_wrist_txt);

        final TextView mWomenOverBustTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_over_burst_txt);
        final TextView mWomenUnderBustTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_under_burst_txt);
        final TextView mWomenHipBoneTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_hip_bone_txt);
        final TextView mWomenThighTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_thigh_txt);
        final TextView mWomenKneeTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_knee_txt);
        final TextView mWomenCalfTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_calf_txt);
        final TextView mWomenAnkleTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_ankle_txt);

        final TextView mWomenHeadTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_head_txt);
        final TextView mWomenNeckTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_neck_txt);
        final TextView mWomenBustTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_bust_txt);
        final TextView mWomenWaistTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_waist_txt);
        final TextView mWomenFullHipTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_full_hip_txt);

        final TextView mWomenHeightTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_height_txt);
        final TextView mWomenStwTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_stw_txt);
        final TextView mWomenNltcTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_nltc_txt);
        final TextView mWomenNltbTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_nltb_txt);
        final TextView mWomenSthbTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_sthb_txt);
        final TextView mWomenWthbTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_wthb_txt);
        final TextView mWomenHthTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_hth_txt);
        final TextView mWomenInseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_inseam_txt);
        final TextView mWomenOutseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_outseam_txt);

        final TextView mWomenShoulderTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_shoulder_txt);
        final TextView mWomenBicepTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_bicep_txt);
        final TextView mWomenWristTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_wrist_txt);

        final TextView mWomenSleeveTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_women_sleeve_txt);

        final TextView mBoyHeadTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_head_txt);
        final TextView mBoyNeckTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_neck_txt);
        final TextView mBoyChestTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_chest_txt);
        final TextView mBoyWaistTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_waist_txt);
        final TextView mBoyThighTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_thigh_txt);
        final TextView mBoyKneeTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_knee_txt);
        final TextView mBoyAnkleTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_ankle_txt);

        final TextView mBoyOverAllHeightTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_over_all_height_txt);
        final TextView mBoyShortsTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_shorts_txt);
        final TextView mBoyOutseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_outseam_txt);
        final TextView mBoyInseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_inseam_txt);

        final TextView mBoyShoulderTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_shoulder_txt);
        final TextView mBoyHalfSleeveTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_half_sleeve_txt);
        final TextView mBoyBicepTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_bicep_txt);
        final TextView mBoyHipTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_hip_txt);
        final TextView mBoyBottomTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_bottom_txt);

        final TextView mBoyHeightTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_height_txt);
        final TextView mBoySleeveTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_sleeve_txt);
        final TextView mBoyWristTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_boy_wrist_txt);

        final TextView mGirlOverBurstTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_over_burst_txt);
        final TextView mGirlUnderBurstTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_under_burst_txt);
        final TextView mGirlHipBoneTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_hip_bone_txt);
        final TextView mGirlThighTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_thigh_txt);
        final TextView mGirlKneeTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_knee_txt);
        final TextView mGirlCalfTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_calf_txt);
        final TextView mGirlAnkleTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_ankle_txt);

        final TextView mGirlHeadTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_head_txt);
        final TextView mGirlNeckTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_neck_txt);
        final TextView mGirlBustTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_bust_txt);
        final TextView mGirlWaistTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_waist_txt);
        final TextView mGirlFullHipTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_full_hip_txt);

        final TextView mGirlHeightTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_height_txt);
        final TextView mGirlStwTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_stw_txt);
        final TextView mGirlNltcTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_nltc_txt);
        final TextView mGirlNltbTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_nltb_txt);
        final TextView mGirlSthbTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_sthb_txt);
        final TextView mGirlWthbTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_wthb_txt);
        final TextView mGirlHthTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_hth_txt);
        final TextView mGirlInseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_inseam_txt);
        final TextView mGirlOutseamTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_outseam_txt);

        final TextView mGirlShoulderTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_shoulder_txt);
        final TextView mGirlBicepTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_bicep_txt);
        final TextView mGirlWristTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_wrist_txt);

        final TextView mGirlSleeveTxt = (TextView) imageLayout.findViewById(R.id.measurement_num_girl_sleeve_txt);


        final TextView mHeadTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_head_txt);
        final TextView mNeckTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_neck_txt);
        final TextView mChestTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_chest_txt);
        final TextView mWaistTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_waist_txt);
        final TextView mThighTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_thigh_txt);
        final TextView mKneeTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_knee_txt);
        final TextView mAnkleTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_ankle_txt);

        final TextView mOverAllHeightTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_over_all_height_txt);
        final TextView mShortsTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_short_txt);
        final TextView mOutseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_outseam_txt);
        final TextView mInseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_inseam_txt);

        final TextView mShoulderTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_shoulder_txt);
        final TextView mHalfSleeveTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_half_sleeve_txt);
        final TextView mBicepTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_bicep_txt);
        final TextView mHipTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_hip_txt);
        final TextView mBottomTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_bottom_txt);

        final TextView mHeightTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_height_txt);
        final TextView mSleeveTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_sleeve_txt);
        final TextView mWristTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_wrist_txt);

        final TextView mWomenOverBustTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_over_burst_txt);
        final TextView mWomenUnderBustTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_under_burst_txt);
        final TextView mWomenHipBoneTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_hip_bone_txt);
        final TextView mWomenThighTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_thigh_txt);
        final TextView mWomenKneeTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_knee_txt);
        final TextView mWomenCalfTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_calf_txt);
        final TextView mWomenAnkleTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_ankle_txt);

        final TextView mWomenHeadTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_head_txt);
        final TextView mWomenNeckTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_neck_txt);
        final TextView mWomenBustTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_bust_txt);
        final TextView mWomenWaistTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_waist_txt);
        final TextView mWomenFullHipTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_full_hip_txt);

        final TextView mWomenHeightTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_height_txt);
        final TextView mWomenStwTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_stw_txt);
        final TextView mWomenNltcTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_nltc_txt);
        final TextView mWomenNltbTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_nltb_txt);
        final TextView mWomenSthbTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_sthb_txt);
        final TextView mWomenWthbTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_wthb_txt);
        final TextView mWomenHthTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_hth_txt);
        final TextView mWomenInseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_inseam_txt);
        final TextView mWomenOutseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_outseam_txt);

        final TextView mWomenShoulderTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_shoulder_txt);
        final TextView mWomenBicepTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_bicep_txt);
        final TextView mWomenWristTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_wrist_txt);

        final TextView mWomenSleeveTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_women_sleeve_txt);

        final TextView mBoyHeadTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_head_txt);
        final TextView mBoyNeckTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_neck_txt);
        final TextView mBoyChestTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_chest_txt);
        final TextView mBoyWaistTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_waist_txt);
        final TextView mBoyThighTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_thigh_txt);
        final TextView mBoyKneeTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_knee_txt);
        final TextView mBoyAnkleTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_ankle_txt);

        final TextView mBoyOverAllHeightTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_over_all_height_txt);
        final TextView mBoyShortsTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_shorts_txt);
        final TextView mBoyOutseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_outseam_txt);
        final TextView mBoyInseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_inseam_txt);

        final TextView mBoyShoulderTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_shoulder_txt);
        final TextView mBoyHalfSleeveTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_half_sleeve_txt);
        final TextView mBoyBicepTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_bicep_txt);
        final TextView mBoyHipTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_hip_txt);
        final TextView mBoyBottomTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_bottom_txt);

        final TextView mBoyHeightTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_height_txt);
        final TextView mBoySleeveTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_sleeve_txt);
        final TextView mBoyWristTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_boy_wrist_txt);

        final TextView mGirlOverBurstTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_over_burst_txt);
        final TextView mGirlUnderBurstTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_under_burst_txt);
        final TextView mGirlHipBoneTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_hip_bone_txt);
        final TextView mGirlThighTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_thigh_txt);
        final TextView mGirlKneeTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_knee_txt);
        final TextView mGirlCalfTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_calf_txt);
        final TextView mGirlAnkleTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_ankle_txt);

        final TextView mGirlHeadTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_head_txt);
        final TextView mGirlNeckTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_neck_txt);
        final TextView mGirlBustTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_bust_txt);
        final TextView mGirlWaistTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_waist_txt);
        final TextView mGirlFullHipTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_full_hip_txt);

        final TextView mGirlHeightTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_height_txt);
        final TextView mGirlStwTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_stw_txt);
        final TextView mGirlNltcTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_nltc_txt);
        final TextView mGirlNltbTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_nltb_txt);
        final TextView mGirlSthbTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_sthb_txt);
        final TextView mGirlWthbTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_wthb_txt);
        final TextView mGirlHthTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_hth_txt);
        final TextView mGirlInseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_inseam_txt);
        final TextView mGirlOutseamTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_outseam_txt);

        final TextView mGirlShoulderTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_shoulder_txt);
        final TextView mGirlBicepTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_bicep_txt);
        final TextView mGirlWristTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_wrist_txt);

        final TextView mGirlSleeveTitleTxt = (TextView) imageLayout.findViewById(R.id.measurement_girl_sleeve_txt);


        for (int i=0; i<mParts.size(); i++){

            if (mParts.get(i).getId() == 1 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mHeadLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mHeadTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mHeadTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 2&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mNeckLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mNeckTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mNeckTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 3&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mChestLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mChestTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mChestTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 4&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mWaistLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWaistTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWaistTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 5&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mThighLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mThighTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mThighTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 6&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mKneeLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mKneeTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mKneeTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId()  == 7&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mAnkleLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mAnkleTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mAnkleTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 8&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mOverAllHeightLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOverAllHeightTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mOverAllHeightTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 9&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mShortsLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mShortsTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mShortsTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 10&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mOutseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mOutseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mOutseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 11&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mInseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mInseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mInseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 12&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mShoulderLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mShoulderTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mShoulderTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 13&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mHalfSleevetLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mHalfSleeveTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mHalfSleeveTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 14&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mBicepLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBicepTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBicepTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 15&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mHipLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mHipTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mHipTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 16&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mBottomLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBottomTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBottomTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 17&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mHeightLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mHeightTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mHeightTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 18&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mSleeveLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mSleeveTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mSleeveTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 19&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mWristLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWristTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWristTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 21&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenOverBustLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenOverBustTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenOverBustTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 22&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenUnderBustLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenUnderBustTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenUnderBustTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 23&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenHipBoneLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenHipBoneTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenHipBoneTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 24&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenThighLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenThighTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenThighTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 25&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenKneeLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenKneeTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenKneeTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 26&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenCalfLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenCalfTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenCalfTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 27&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenAnkleLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenAnkleTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenAnkleTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 20&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenHeadLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenHeadTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenHeadTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 28&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenNeckLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenNeckTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenNeckTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 29&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenBustLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenBustTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenBustTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 30&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenWaistLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenWaistTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenWaistTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 31&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenFullHipLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenFullHipTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenFullHipTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 32&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenHeightLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenHeightTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenHeightTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 33&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenStwLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenStwTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenStwTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 34&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenNltcLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenNltcTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenNltcTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 35&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenNltbLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenNltbTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenNltbTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 36&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenSthbLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenSthbTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenSthbTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 37&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenWthbLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenWthbTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenWthbTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 38&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenHthLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenHthTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenHthTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 39&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenInseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenInseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenInseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 40&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenOutseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenOutseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenOutseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 41 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenShoulderLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenShoulderTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenShoulderTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 42&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenBicepLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenBicepTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenBicepTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 43&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenWristLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenWristTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenWristTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 44&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenSleeveLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mWomenSleeveTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenSleeveTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 45&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyHeadLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyHeadTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyHeadTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 46&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyNeckLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyNeckTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyNeckTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 47&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyChestLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyChestTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyChestTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 48&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyWaistLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyWaistTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyWaistTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 49&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyThighLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyThighTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyThighTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 50&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyKneeLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyKneeTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyKneeTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 51&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyAnkleLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyAnkleTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyAnkleTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 52&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyOverAllHeightLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyOverAllHeightTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyOverAllHeightTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 53&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyShortsLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyShortsTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyShortsTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 54&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyOutseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyOutseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyOutseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 55&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyInseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyInseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyInseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 56&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyShoulderLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyShoulderTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyShoulderTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 57&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyHalfSleeveLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyHalfSleeveTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyHalfSleeveTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 58&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyBicepLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyBicepTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyBicepTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 59&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyHipLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyHipTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyHipTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 60&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyBottomLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyBottomTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyBottomTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 61&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyHeightLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyHeightTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyHeightTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 62&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoySleeveLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoySleeveTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoySleeveTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 63&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mBoyWristLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mBoyWristTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mBoyWristTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 65&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlOverBurstLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlOverBurstTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlOverBurstTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 66&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlUnderBurstLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlUnderBurstTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlUnderBurstTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 67&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlHipBoneLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlHipBoneTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlHipBoneTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 68&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlThighLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlThighTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlThighTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 69&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlKneeLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlKneeTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlKneeTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 70&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlCalfLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlCalfTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlCalfTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 71&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlAnkleLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlAnkleTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlAnkleTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 64&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlHeadLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlHeadTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlHeadTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 72&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlNeckLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlNeckTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlNeckTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 73&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlBustLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlBustTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlBustTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 74&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlWaistLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlWaistTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlWaistTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 75&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlHipBoneLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlHipBoneTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlHipBoneTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 76&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlHeightLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlHeadTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mWomenNeckTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 77&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlStwLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlStwTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlStwTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 78&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlNltcLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlNltcTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlNltcTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 79&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlNltbLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlNltbTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlNltbTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 80&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlSthbLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlSthbTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlSthbTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 81&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlWthbLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlWthbTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlWthbTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 82&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlHthLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlHthTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlHthTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 83&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlInseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlInseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlInseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 84&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlOutseamLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlOutseamTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlOutseamTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 85&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlShoulderLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlShoulderTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlShoulderTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 86&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlBicepLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlBicepTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlBicepTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 87&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlWristLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlWristTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlWristTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
            else if (mParts.get(i).getId() == 88&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mGirlSleeveLay.setVisibility(View.VISIBLE);
                if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    mGirlSleeveTitleTxt.setText(mParts.get(i).getTextInArabic());
                }else {
                    mGirlSleeveTitleTxt.setText(mParts.get(i).getTextInEnglish());
                }
            }
        }

        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mHeadTxt.setText(AppConstants.HEAD);
                mNeckTxt.setText(AppConstants.NECK);
                mChestTxt.setText(AppConstants.CHEST);
                mWaistTxt.setText(AppConstants.WAIST);
                mThighTxt.setText(AppConstants.THIGH);
                mKneeTxt.setText(AppConstants.KNEE);
                mAnkleTxt.setText(AppConstants.ANKLE);

                mOverAllHeightTxt.setText(AppConstants.OVER_ALL_HEIGHT);
                mShortsTxt.setText(AppConstants.SHORTS);
                mOutseamTxt.setText(AppConstants.OUTSEAM);
                mInseamTxt.setText(AppConstants.INSEAM);

                mShoulderTxt.setText(AppConstants.SHOULDER);
                mHalfSleeveTxt.setText(AppConstants.HALF_SLEEVE);
                mBicepTxt.setText(AppConstants.BICEP);
                mHipTxt.setText(AppConstants.HIP);
                mBottomTxt.setText(AppConstants.BOTTOM);

                mHeightTxt.setText(AppConstants.HEIGHT);
                mSleeveTxt.setText(AppConstants.SLEEVE);
                mWristTxt.setText(AppConstants.WRIST);
            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenOverBustTxt.setText(AppConstants.WOMEN_OVER_BUST);
                mWomenUnderBustTxt.setText(AppConstants.WOMEN_UNDER_BUST);
                mWomenHipBoneTxt.setText(AppConstants.WOMEN_HIP_BONE);
                mWomenThighTxt.setText(AppConstants.WOMEN_THIGH);
                mWomenKneeTxt.setText(AppConstants.WOMEN_KNEE);
                mWomenCalfTxt.setText(AppConstants.WOMEN_CALF);
                mWomenAnkleTxt.setText(AppConstants.WOMEN_ANKLE);

                mWomenHeadTxt.setText(AppConstants.WOMEN_HEAD);
                mWomenNeckTxt.setText(AppConstants.WOMEN_NECK);
                mWomenBustTxt.setText(AppConstants.WOMEN_BUST);
                mWomenWaistTxt.setText(AppConstants.WOMEN_WAIST);
                mWomenFullHipTxt.setText(AppConstants.WOMEN_FULL_HIP);

                mWomenHeadTxt.setText(AppConstants.WOMEN_HEIGHT);
                mWomenStwTxt.setText(AppConstants.WOMEN_STW);
                mWomenNltcTxt.setText(AppConstants.WOMEN_NLTC);
                mWomenNltbTxt.setText(AppConstants.WOMENT_NLTB);
                mWomenSthbTxt.setText(AppConstants.WOMEN_STHB);
                mWomenWthbTxt.setText(AppConstants.WOMEN_WTHB);
                mWomenHthTxt.setText(AppConstants.WOMEN_HTH);
                mWomenInseamTxt.setText(AppConstants.WOMEN_INSEAM);
                mWomenOutseamTxt.setText(AppConstants.WOMEN_OUTSEAM);

                mWomenShoulderTxt.setText(AppConstants.WOMEN_SHOULDER);
                mWomenBicepTxt.setText(AppConstants.WOMEN_BICEP);
                mWomenWristTxt.setText(AppConstants.WOMEN_WRIST);

                mWomenSleeveTxt.setText(AppConstants.WOMEN_SLEEVE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")) {
                mBoyHeadTxt.setText(AppConstants.BOY_HEAD);
                mBoyNeckTxt.setText(AppConstants.BOY_NECK);
                mBoyChestTxt.setText(AppConstants.BOY_CHEST);
                mBoyWaistTxt.setText(AppConstants.BOY_WAIST);
                mBoyThighTxt.setText(AppConstants.BOY_THIGH);
                mBoyKneeTxt.setText(AppConstants.BOY_KNEE);
                mBoyAnkleTxt.setText(AppConstants.BOY_ANKLE);

                mBoyOverAllHeightTxt.setText(AppConstants.BOY_OVER_ALL_HEIGHT);
                mBoyShortsTxt.setText(AppConstants.BOY_SHORTS);
                mBoyOutseamTxt.setText(AppConstants.BOY_OUTSEAM);
                mBoyInseamTxt.setText(AppConstants.BOY_INSEAM);

                mBoyShoulderTxt.setText(AppConstants.BOY_SHOULDER);
                mBoyHalfSleeveTxt.setText(AppConstants.BOY_HALFSLEEVE);
                mBoyBicepTxt.setText(AppConstants.BOY_BICEP);
                mBoyHipTxt.setText(AppConstants.BOY_HIP);
                mBoyBottomTxt.setText(AppConstants.BOY_BOTTOM);

                mBoyHeightTxt.setText(AppConstants.BOY_HEIGHT);
                mBoySleeveTxt.setText(AppConstants.BOY_SLEEVE);
                mBoyWristTxt.setText(AppConstants.BOY_WRIST);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")) {
                mGirlOverBurstTxt.setText(AppConstants.GIRL_OVER_BURST);
                mGirlUnderBurstTxt.setText(AppConstants.GIRL_UNDER_BURST);
                mGirlHipBoneTxt.setText(AppConstants.GIRL_HIP_BONE);
                mGirlThighTxt.setText(AppConstants.GIRL_THIGH);
                mGirlKneeTxt.setText(AppConstants.GIRL_KNEE);
                mGirlCalfTxt.setText(AppConstants.GIRL_CALF);
                mGirlAnkleTxt.setText(AppConstants.GIRL_ANKLE);

                mGirlHeadTxt.setText(AppConstants.GIRL_HEAD);
                mGirlNeckTxt.setText(AppConstants.GIRL_NECK);
                mGirlBustTxt.setText(AppConstants.GIRL_BUST);
                mGirlWaistTxt.setText(AppConstants.GIRL_WAIST);
                mGirlFullHipTxt.setText(AppConstants.GIRL_FULL_HIP);

                mGirlHeightTxt.setText(AppConstants.GIRL_HEIGHT);
                mGirlStwTxt.setText(AppConstants.GIRL_STW);
                mGirlNltcTxt.setText(AppConstants.GIRL_NLTC);
                mGirlNltbTxt.setText(AppConstants.GIRL_NLTB);
                mGirlSthbTxt.setText(AppConstants.GIRL_STHB);
                mGirlWthbTxt.setText(AppConstants.GIRL_WTHB);
                mGirlHthTxt.setText(AppConstants.GIRL_HTH);
                mGirlInseamTxt.setText(AppConstants.GIRL_INSEAM);
                mGirlOutseamTxt.setText(AppConstants.GIRL_OUTSEAM);

                mGirlShoulderTxt.setText(AppConstants.GIRL_SHOULDER);
                mGirlBicepTxt.setText(AppConstants.GIRL_BICEP);
                mGirlWristTxt.setText(AppConstants.GIRL_WRIST);

                mGirlSleeveTxt.setText(AppConstants.GIRL_SLEEVE);
            }
        }else if(AppConstants.UNITS.equalsIgnoreCase("IN")) {
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")) {
                mHeadTxt.setText(AppConstants.HEAD_INCHES);
                mNeckTxt.setText(AppConstants.NECK_INCHES);
                mChestTxt.setText(AppConstants.CHEST_INCHES);
                mWaistTxt.setText(AppConstants.WAIST_INCHES);
                mThighTxt.setText(AppConstants.THIGH_INCHES);
                mKneeTxt.setText(AppConstants.KNEE_INCHES);
                mAnkleTxt.setText(AppConstants.ANKLE_INCHES);

                mOverAllHeightTxt.setText(AppConstants.OVER_ALL_HEIGHT_INCHES);
                mShortsTxt.setText(AppConstants.SHORTS_INCHES);
                mOutseamTxt.setText(AppConstants.OUTSEAM_INCHES);
                mInseamTxt.setText(AppConstants.INSEAM_INCHES);

                mShoulderTxt.setText(AppConstants.SHOULDER_INCHES);
                mHalfSleeveTxt.setText(AppConstants.HALF_SLEEVE_INCHES);
                mBicepTxt.setText(AppConstants.BICEP_INCHES);
                mHipTxt.setText(AppConstants.HIP_INCHES);
                mBottomTxt.setText(AppConstants.BOTTOM_INCHES);

                mHeightTxt.setText(AppConstants.HEIGHT_INCHES);
                mSleeveTxt.setText(AppConstants.SLEEVE_INCHES);
                mWristTxt.setText(AppConstants.WRIST_INCHES);
            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mWomenOverBustTxt.setText(AppConstants.WOMEN_OVER_BUST_INCHES);
                mWomenUnderBustTxt.setText(AppConstants.WOMEN_UNDER_BUST_INCHES);
                mWomenHipBoneTxt.setText(AppConstants.WOMEN_HIP_BONE_INCHES);
                mWomenThighTxt.setText(AppConstants.WOMEN_THIGH_INCHES);
                mWomenKneeTxt.setText(AppConstants.WOMEN_KNEE_INCHES);
                mWomenCalfTxt.setText(AppConstants.WOMEN_CALF_INCHES);
                mWomenAnkleTxt.setText(AppConstants.WOMEN_ANKLE_INCHES);

                mWomenHeadTxt.setText(AppConstants.WOMEN_HEAD_INCHES);
                mWomenNeckTxt.setText(AppConstants.WOMEN_NECK_INCHES);
                mWomenBustTxt.setText(AppConstants.WOMEN_BUST_INCHES);
                mWomenWaistTxt.setText(AppConstants.WOMEN_WAIST_INCHES);
                mWomenFullHipTxt.setText(AppConstants.WOMEN_FULL_HIP_INCHES);

                mWomenHeadTxt.setText(AppConstants.WOMEN_HEIGHT_INCHES);
                mWomenStwTxt.setText(AppConstants.WOMEN_STW_INCHES);
                mWomenNltcTxt.setText(AppConstants.WOMEN_NLTC_INCHES);
                mWomenNltbTxt.setText(AppConstants.WOMENT_NLTB_INCHES);
                mWomenSthbTxt.setText(AppConstants.WOMEN_STHB_INCHES);
                mWomenWthbTxt.setText(AppConstants.WOMEN_WTHB_INCHES);
                mWomenHthTxt.setText(AppConstants.WOMEN_HTH_INCHES);
                mWomenInseamTxt.setText(AppConstants.WOMEN_INSEAM_INCHES);
                mWomenOutseamTxt.setText(AppConstants.WOMEN_OUTSEAM_INCHES);

                mWomenShoulderTxt.setText(AppConstants.WOMEN_SHOULDER_INCHES);
                mWomenBicepTxt.setText(AppConstants.WOMEN_BICEP_INCHES);
                mWomenWristTxt.setText(AppConstants.WOMEN_WRIST_INCHES);

                mWomenSleeveTxt.setText(AppConstants.WOMEN_SLEEVE_INCHES);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")) {
                mBoyHeadTxt.setText(AppConstants.BOY_HEAD_INCHES);
                mBoyNeckTxt.setText(AppConstants.BOY_NECK_INCHES);
                mBoyChestTxt.setText(AppConstants.BOY_CHEST_INCHES);
                mBoyWaistTxt.setText(AppConstants.BOY_WAIST_INCHES);
                mBoyThighTxt.setText(AppConstants.BOY_THIGH_INCHES);
                mBoyKneeTxt.setText(AppConstants.BOY_KNEE_INCHES);
                mBoyAnkleTxt.setText(AppConstants.BOY_ANKLE_INCHES);

                mBoyOverAllHeightTxt.setText(AppConstants.BOY_OVER_ALL_HEIGHT_INCHES);
                mBoyShortsTxt.setText(AppConstants.BOY_SHORTS_INCHES);
                mBoyOutseamTxt.setText(AppConstants.BOY_OUTSEAM_INCHES);
                mBoyInseamTxt.setText(AppConstants.BOY_INSEAM_INCHES);

                mBoyShoulderTxt.setText(AppConstants.BOY_SHOULDER_INCHES);
                mBoyHalfSleeveTxt.setText(AppConstants.BOY_HALFSLEEVE_INCHES);
                mBoyBicepTxt.setText(AppConstants.BOY_BICEP_INCHES);
                mBoyHipTxt.setText(AppConstants.BOY_HIP_INCHES);
                mBoyBottomTxt.setText(AppConstants.BOY_BOTTOM_INCHES );

                mBoyHeightTxt.setText(AppConstants.BOY_HEIGHT_INCHES);
                mBoySleeveTxt.setText(AppConstants.BOY_SLEEVE_INCHES);
                mBoyWristTxt.setText(AppConstants.BOY_WRIST_INCHES);
            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")) {
                mGirlOverBurstTxt.setText(AppConstants.GIRL_OVER_BURST_INCHES);
                mGirlUnderBurstTxt.setText(AppConstants.GIRL_UNDER_BURST_INCHES);
                mGirlHipBoneTxt.setText(AppConstants.GIRL_HIP_BONE_INCHES);
                mGirlThighTxt.setText(AppConstants.GIRL_THIGH_INCHES);
                mGirlKneeTxt.setText(AppConstants.GIRL_KNEE_INCHES);
                mGirlCalfTxt.setText(AppConstants.GIRL_CALF_INCHES);
                mGirlAnkleTxt.setText(AppConstants.GIRL_ANKLE_INCHES);

                mGirlHeadTxt.setText(AppConstants.GIRL_HEAD_INCHES);
                mGirlNeckTxt.setText(AppConstants.GIRL_NECK_INCHES);
                mGirlBustTxt.setText(AppConstants.GIRL_BUST_INCHES);
                mGirlWaistTxt.setText(AppConstants.GIRL_WAIST_INCHES);
                mGirlFullHipTxt.setText(AppConstants.GIRL_FULL_HIP_INCHES);

                mGirlHeightTxt.setText(AppConstants.GIRL_HEIGHT_INCHES);
                mGirlStwTxt.setText(AppConstants.GIRL_STW_INCHES);
                mGirlNltcTxt.setText(AppConstants.GIRL_NLTC_INCHES);
                mGirlNltbTxt.setText(AppConstants.GIRL_NLTB_INCHES);
                mGirlSthbTxt.setText(AppConstants.GIRL_STHB_INCHES);
                mGirlWthbTxt.setText(AppConstants.GIRL_WTHB_INCHES);
                mGirlHthTxt.setText(AppConstants.GIRL_HTH_INCHES);
                mGirlInseamTxt.setText(AppConstants.GIRL_INSEAM_INCHES);
                mGirlOutseamTxt.setText(AppConstants.GIRL_OUTSEAM_INCHES);

                mGirlShoulderTxt.setText(AppConstants.GIRL_SHOULDER_INCHES);
                mGirlBicepTxt.setText(AppConstants.GIRL_BICEP_INCHES);
                mGirlWristTxt.setText(AppConstants.GIRL_WRIST_INCHES);

                mGirlSleeveTxt.setText(AppConstants.GIRL_SLEEVE_INCHES);

            }
        }


        mHeadLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "1";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 1){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mHeadTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }

            }
        });

        mNeckLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "2";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 2){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mNeckTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }

            }
        });
        mChestLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "3";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 3){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mChestTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWaistLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "4";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 4){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWaistTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mThighLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "5";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 5){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mThighTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mKneeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "6";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 6){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mKneeTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mAnkleLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "7";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 7){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mAnkleTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });

        mOverAllHeightLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "8";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 8){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mOverAllHeightTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mShortsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "9";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 9){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mShortsTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mOutseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "10";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 10){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mOutseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mInseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "11";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 11){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mInseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mShoulderLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "12";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 12){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mShoulderTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mHalfSleevetLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "13";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 13){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mHalfSleeveTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBicepLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "14";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 14){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBicepTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mHipLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "15";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 15){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mHipTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBottomLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "16";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 16){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBottomTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mHeightLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "17";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 17){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mHeightTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mSleeveLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "18";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 18){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mSleeveTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWristLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "19";
                for (int i=0; i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 19){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWaistTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenOverBustLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "21";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 21){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenOverBustTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenUnderBustLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "22";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 22){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenUnderBustTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenHipBoneLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "23";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 23){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenHipBoneTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenThighLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "24";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 24){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenThighTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenKneeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "25";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 25){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenKneeTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenCalfLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "26";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 26){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenCalfTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenAnkleLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "27";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 27){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenAnkleTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenHeadLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "20";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 20){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenHeadTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenNeckLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "28";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 28){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenNeckTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenBustLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "29";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 29){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenBustTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenWaistLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "30";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 30){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenWaistTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenFullHipLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "31";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 31){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenFullHipTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenHeightLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "32";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 32){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenHeightTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenStwLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "33";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 33){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenStwTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenNltcLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "34";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 34){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenNltcTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenNltbLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "35";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 35){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenNltbTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenSthbLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "36";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 36){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenSthbTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenWthbLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "37";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 37){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenWthbTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenHthLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "38";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 38){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenHthTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenInseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "39";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 39){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenInseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenOutseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "40";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 40){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenOutseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenShoulderLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "41";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 41){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenShoulderTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenBicepLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "42";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 42){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenBicepTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenWristLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "43";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 43){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenWristTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mWomenSleeveLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "44";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 44){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mWomenSleeveTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyHeadLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "45";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 45){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyHeadTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyNeckLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "46";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 46){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyNeckTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyChestLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "47";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 47){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyChestTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyWaistLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "48";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 48){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyWaistTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyThighLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "49";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 49){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyThighTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyKneeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "50";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 50){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyKneeTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyAnkleLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "51";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 51){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyAnkleTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyOverAllHeightLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "52";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 52){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyOverAllHeightTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyShortsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "53";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 53){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyShortsTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyOutseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "54";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 54){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyOutseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyInseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "55";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 55){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyInseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyShoulderLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "56";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 56){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyShoulderTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyHalfSleeveLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "57";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 57){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyHalfSleeveTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyBicepLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "58";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 58){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyBicepTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyHipLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "59";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 59){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyHipTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyBottomLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "60";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 60){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyBottomTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyHeightLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "61";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 61){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyHeightTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoySleeveLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "62";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 62){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoySleeveTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mBoyWristLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "63";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 63){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mBoyWristTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlOverBurstLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "65";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 65){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlOverBurstTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlUnderBurstLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "66";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 66){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlUnderBurstTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlHipBoneLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "67";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 67){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlHipBoneTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlThighLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "68";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 68){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlThighTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlKneeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "69";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 69){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlKneeTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlCalfLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "70";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 70){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlCalfTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlAnkleLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "71";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 71){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlAnkleTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlHeadLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "64";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 64){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlHeadTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlNeckLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "72";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 72){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlNeckTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlBustLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "73";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 73){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlBustTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlWaistLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "74";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 74){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlWaistTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlFullHipLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "75";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 75){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlFullHipTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlHeightLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "76";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 76){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlHeightTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlStwLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "77";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 77){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlStwTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlNltcLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "78";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 78){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlNltcTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlNltbLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "79";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 79){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlNltbTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlSthbLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "80";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 80){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlSthbTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlWthbLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "81";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 81){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlWthbTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlHthLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "82";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 82){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlHthTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlInseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "83";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 83){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlInseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlOutseamLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "84";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 84){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlOutseamTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlShoulderLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "85";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 85){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlShoulderTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlBicepLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "86";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 86){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlBicepTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlWristLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "87";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 87){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlWristTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        mGirlSleeveLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_SCALE = "88";
                for (int i=0;i<mParts.size(); i++){
                    if (mParts.get(i).getId() == 88){
                        String mTitleTxt = "";
                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                            mTitleTxt = mParts.get(i).getTextInArabic();
                        }else {
                            mTitleTxt = mParts.get(i).getTextInEnglish();
                        }
                        setAdapter(mGirlSleeveTxt,mParts.get(i).getImage(),mTitleTxt);
                    }
                }
            }
        });
        if (position == 0) {
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mMenFrontOne.setVisibility(View.VISIBLE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.VISIBLE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.VISIBLE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
        }
        if (position == 1) {
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")) {
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.VISIBLE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.VISIBLE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.VISIBLE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
        }
        if (position == 2) {
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.VISIBLE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.VISIBLE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.VISIBLE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.VISIBLE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
        }
        if (position == 3) {
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.VISIBLE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.VISIBLE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.VISIBLE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.VISIBLE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
        }
        if (position == 4){
            if (AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.VISIBLE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.VISIBLE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.GONE);

            }
            else if (AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                mMenFrontOne.setVisibility(View.GONE);
                mMenFrontTwo.setVisibility(View.GONE);
                mMenBackOne.setVisibility(View.GONE);
                mMenBackTwo.setVisibility(View.GONE);
                mWomenFrontOne.setVisibility(View.GONE);
                mWomenFrontTwo.setVisibility(View.GONE);
                mWomenFrontThree.setVisibility(View.GONE);
                mWomenBackOne.setVisibility(View.GONE);
                mWomenBackTwo.setVisibility(View.GONE);
                mBoyFroneOne.setVisibility(View.GONE);
                mBoyFrontTwo.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackOne.setVisibility(View.GONE);
                mBoyBackTwo.setVisibility(View.GONE);
                mGirlFrontOne.setVisibility(View.GONE);
                mGirlFrontTwo.setVisibility(View.GONE);
                mGirlFrontThree.setVisibility(View.GONE);
                mGirlBackOne.setVisibility(View.GONE);
                mGirlBackTwo.setVisibility(View.VISIBLE);

            }
        }

        try {
            Glide.with(context)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+IMAGES.get(position).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                    .into(imageView);
        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }


    public void getArraylist() {
        num = 0.0;

        mArrayList = new ArrayList<>();
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("");
        mArrayList.add("0.0");

        for (int i = 0; i < 35; i++) {
            num = num + 0.1;
            DecimalFormat dtime = new DecimalFormat("###.#");
            mArrayList.add(String.valueOf(Double.valueOf(dtime.format(num))));
        }

    }

    public void setAdapter(final TextView mText,String partImage,String titleTxtStr) {
        getArraylist();
        alertDismiss(mSlider);
        mSlider = getDialog(context, R.layout.pop_up_measurement_scale);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mSlider.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(mSlider.findViewById(R.id.pop_up_measurement_scale_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(mSlider.findViewById(R.id.pop_up_measurement_scale_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        final TextView scaleTxt,titleTxt;
        Button mOkBtn,mCancelBtn;
        ImageView partsImageView;

        RecyclerView recyclerView;
        final LinearLayoutManager layoutManager;
        /*Init view*/
        mOkBtn = mSlider.findViewById(R.id.scale_measurement_ok_btn);
        mCancelBtn = mSlider.findViewById(R.id.scale_measurement_cancel_btn);
        scaleTxt = mSlider.findViewById(R.id.scale_cm_txt);
        partsImageView = mSlider.findViewById(R.id.measurement_scale_body_img);
        titleTxt = mSlider.findViewById(R.id.measurement_scale_body_txt);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            mCancelBtn.setText("إلغاء");
            mOkBtn.setText("حسنا");
        }

        titleTxt.setText(titleTxtStr);

        try {
            Glide.with(context)
                    .load(AppConstants.IMAGE_BASE_URL + "images/Measurement2/" + partImage)
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                    .into(partsImageView);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        recyclerView = mSlider.findViewById(R.id.measurmentScallingSampleRecyclerview);
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        mMeasurementAdapter = new MeasurementScaleAdapter(context, mArrayList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mMeasurementAdapter);

        DialogManager.getInstance().hideProgress();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstPos = layoutManager.findFirstVisibleItemPosition();
                int lastPos = layoutManager.findLastVisibleItemPosition();
                int middle = Math.abs(lastPos - firstPos) / 2 + firstPos;

                if (mCheckToAdd.equalsIgnoreCase("")) {

                    for (int i = 0; i < mArrayList.size(); i++) {
                        if (mArrayList.get(i).equalsIgnoreCase("99.0")) {
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");
                            mArrayList.add("");

                            mMeasurementAdapter.notifyDataSetChanged();

                            mCheckToAdd = "Added";
                        } else {
                            if (String.valueOf(lastPos).equalsIgnoreCase(String.valueOf(mArrayList.size() - 1))) {

                                for (int j = 0; j < 10; j++) {
                                    num = num + 0.1;
                                    DecimalFormat dtime = new DecimalFormat("######.#");
                                    mArrayList.add(String.valueOf(Double.valueOf(dtime.format(num))));

                                }
                                mMeasurementAdapter.notifyDataSetChanged();

                            }


                        }

                    }
                }

                for (int k = 0; k < mMeasurementAdapter.getItemCount(); k++) {
                    if (k == middle) {
                        scaleTxt.setText(mArrayList.get(k).equalsIgnoreCase("") ? "0.0CM" : mArrayList.get(k));
                        AppConstants.MEASUREMENT_LENGTH = mArrayList.get(k).equalsIgnoreCase("") ? "0.0" : mArrayList.get(k);

                    }
                }
            }
        });
        /*Set data*/
        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DecimalFormat dtime = new DecimalFormat("######.#");
                Float values = Float.parseFloat(AppConstants.MEASUREMENT_LENGTH);
                Float multiplyValue = Float.parseFloat("2.5");

                if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("1")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){

                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.HEAD = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.HEAD = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.HEAD_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;

                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("2")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){

                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.NECK = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.NECK = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.NECK_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("3")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.CHEST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.CHEST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.CHEST = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.CHEST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("4")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WAIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WAIST = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WAIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("5")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.THIGH = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.THIGH = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.THIGH_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }

                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("6")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.KNEE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.KNEE = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.KNEE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("7")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.ANKLE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.ANKLE = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.ANKLE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("8")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.OVER_ALL_HEIGHT = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.OVER_ALL_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.OVER_ALL_HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.OVER_ALL_HEIGHT_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("9")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.SHORTS = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.SHORTS_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.SHORTS = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.SHORTS_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("10")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.OUTSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.OUTSEAM = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.OUTSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("11")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.INSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.INSEAM = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.INSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("12")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.SHOULDER = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.SHOULDER = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.SHOULDER_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("13")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.HALF_SLEEVE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.HALF_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.HALF_SLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.HALF_SLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("14")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BICEP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BICEP = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BICEP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("15")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.HIP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.HIP = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.HIP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("16")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOTTOM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOTTOM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOTTOM = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOTTOM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("17")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.HEIGHT = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.HEIGHT = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.HEIGHT_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("18")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.SLEEVE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.SLEEVE = String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.SLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("19")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WRIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WRIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WRIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("21")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_OVER_BUST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_OVER_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_OVER_BUST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_OVER_BUST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("22")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_UNDER_BUST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_UNDER_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_UNDER_BUST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_UNDER_BUST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("23")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_HIP_BONE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_HIP_BONE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_HIP_BONE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_HIP_BONE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("24")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_THIGH = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_THIGH= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_THIGH_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("25")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_KNEE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_KNEE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_KNEE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("26")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_CALF = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_CALF_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_CALF= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_CALF_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("27")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_ANKLE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_ANKLE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_ANKLE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("20")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_HEAD = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_HEAD= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_HEAD_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("28")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_NECK = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_NECK= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_NECK_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("29")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_BUST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_BUST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_BUST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("30")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_WAIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_WAIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_WAIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("31")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_FULL_HIP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_FULL_HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_FULL_HIP= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_FULL_HIP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("32")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_HEIGHT = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_HEIGHT= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_HEIGHT_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("33")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_STW = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_STW_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_STW= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_STW_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("34")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_NLTC = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_NLTC_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_NLTC= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_NLTC_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("35")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMENT_NLTB = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMENT_NLTB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMENT_NLTB= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMENT_NLTB_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("36")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_STHB = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_STHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_STHB= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_STHB_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("37")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_WTHB = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_WTHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_WTHB= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_WTHB_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("38")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_HTH = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_HTH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_HTH= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_HTH_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("39")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_INSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_INSEAM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_INSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("40")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_OUTSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_OUTSEAM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_OUTSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("41")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_SHOULDER = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_SHOULDER= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_SHOULDER_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("42")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_BICEP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_BICEP= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_BICEP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("43")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_WRIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_WRIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_WRIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("44")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.WOMEN_SLEEVE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.WOMEN_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.WOMEN_SLEEVE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.WOMEN_SLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("45")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_HEAD = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_HEAD= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_HEAD_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("46")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_NECK = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_NECK= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_NECK_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("47")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_CHEST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_CHEST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_CHEST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_CHEST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("48")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_WAIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_WAIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_WAIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("49")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_THIGH = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_THIGH= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_THIGH_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("50")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_KNEE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_KNEE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_KNEE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("51")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_ANKLE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_ANKLE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_ANKLE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("52")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_OVER_ALL_HEIGHT = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_OVER_ALL_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_OVER_ALL_HEIGHT= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_OVER_ALL_HEIGHT_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("53")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_SHORTS = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_SHORTS_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_SHORTS= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_SHORTS_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("54")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_OUTSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_OUTSEAM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_OUTSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("55")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_INSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_INSEAM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_INSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("56")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_SHOULDER = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_SHOULDER= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_SHOULDER_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("57")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_HALFSLEEVE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_HALFSLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_HALFSLEEVE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_HALFSLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("58")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_BICEP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_BICEP= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_BICEP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("59")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_HIP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_HIP= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_HIP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("60")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_BOTTOM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_BOTTOM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_BOTTOM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_BOTTOM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("61")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_HEIGHT = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_HEIGHT= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_HEIGHT_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("62")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_SLEEVE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_SLEEVE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_SLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("63")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.BOY_WRIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.BOY_WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.BOY_WRIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.BOY_WRIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("65")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_OVER_BURST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_OVER_BURST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_OVER_BURST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_OVER_BURST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("66")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_UNDER_BURST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_UNDER_BURST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_UNDER_BURST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_UNDER_BURST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("67")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_HIP_BONE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_HIP_BONE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_HIP_BONE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_HIP_BONE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("68")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_THIGH = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_THIGH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_THIGH= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_THIGH_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("69")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_KNEE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_KNEE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_KNEE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_KNEE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("70")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_CALF = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_CALF_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_CALF= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_CALF_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("71")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_ANKLE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_ANKLE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_ANKLE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_ANKLE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }

                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("64")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_HEAD = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_HEAD_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_HEAD= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_HEAD_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("72")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_NECK = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_NECK_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_NECK= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_NECK_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("73")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_BUST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_BUST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_BUST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_BUST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("74")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_WAIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_WAIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_WAIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_WAIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("75")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_FULL_HIP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_FULL_HIP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_FULL_HIP= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_FULL_HIP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("76")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_HEIGHT = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_HEIGHT_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_HEIGHT= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_HEIGHT_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("77")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_STW = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_STW_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_STW= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_STW_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("78")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_NLTC = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_NLTC_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_NLTC= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_NLTC_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("79")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_NLTB = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_NLTB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_NLTB= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_NLTB_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("80")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_STHB = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_STHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_STHB= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_STHB_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("81")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_WTHB = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_WTHB_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_WTHB= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_WTHB_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("82")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_HTH = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_HTH_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_HTH= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_HTH_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("83")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_INSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_INSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_INSEAM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_INSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("84")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_OUTSEAM = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_OUTSEAM_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_OUTSEAM= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_OUTSEAM_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("85")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_SHOULDER = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_SHOULDER_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_SHOULDER= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_SHOULDER_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("86")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_BICEP = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_BICEP_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_BICEP= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_BICEP_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("87")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_WRIST = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_WRIST_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_WRIST= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_WRIST_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }
                else if (AppConstants.MEASUREMENT_SCALE.equalsIgnoreCase("88")&&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                    if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                        AppConstants.GIRL_SLEEVE = AppConstants.MEASUREMENT_LENGTH;
                        AppConstants.GIRL_SLEEVE_INCHES = String.valueOf(dtime.format(values / multiplyValue));
                    }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                        AppConstants.GIRL_SLEEVE= String.valueOf(dtime.format(values * multiplyValue));
                        AppConstants.GIRL_SLEEVE_INCHES = AppConstants.MEASUREMENT_LENGTH;
                    }
                    parts.put(String.valueOf(AppConstants.MEASUREMENT_SCALE),String.valueOf(AppConstants.MEASUREMENT_LENGTH));
                    AppConstants.MEASUREMENT_MAP = parts;
                }

                mText.setText(AppConstants.MEASUREMENT_LENGTH);
                mSlider.dismiss();
                reloadArrayList();
            }
        });
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlider.dismiss();

//                DialogManager.getInstance().showOptionPopup(context, context.getResources().getString(R.string.sure_want_to_cancel), context.getResources().getString(R.string.yes), context.getResources().getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
//                    @Override
//                    public void onNegativeClick() {
//
//                    }
//
//                    @Override
//                    public void onPositiveClick() {
//                        mSlider.dismiss();
//                    }
//                });
            }
        });

        alertShowing(mSlider);

    }

    public void reloadArrayList(){
        for (int i=0; i<mParts.size(); i++){

            if (mParts.get(i).getId() == 1 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HEAD);
                Inches.add(i,AppConstants.HEAD_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 2 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.NECK);
                Inches.add(i,AppConstants.NECK_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 3 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.CHEST);
                Inches.add(i,AppConstants.CHEST_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 4 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.WAIST);
                Inches.add(i,AppConstants.WAIST_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 5 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.THIGH);
                Inches.add(i,AppConstants.THIGH_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 7 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.ANKLE);
                Inches.add(i,AppConstants.ANKLE_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 6 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.KNEE);
                Inches.add(i,AppConstants.KNEE_INCHES);

                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 8 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.OVER_ALL_HEIGHT);
                Inches.add(i,AppConstants.OVER_ALL_HEIGHT_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 9 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.SHORTS);
                Inches.add(i,AppConstants.SHORTS_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 10 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.OUTSEAM);
                Inches.add(i,AppConstants.OUTSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 11 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.INSEAM);
                Inches.add(i,AppConstants.INSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 12 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.SHOULDER);
                Inches.add(i,AppConstants.SHOULDER_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 13 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HALF_SLEEVE);
                Inches.add(i,AppConstants.HALF_SLEEVE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 14 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.BICEP);
                Inches.add(i,AppConstants.BICEP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 15 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HIP);
                Inches.add(i,AppConstants.HIP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 16 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.BOTTOM);
                Inches.add(i,AppConstants.BOTTOM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 17 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HEIGHT);
                Inches.add(i,AppConstants.HEIGHT_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 18 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.SLEEVE);
                Inches.add(i,AppConstants.SLEEVE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 19 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.WRIST);
                Inches.add(i,AppConstants.WRIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 21 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_OVER_BUST);
                Inches.add(i,AppConstants.WOMEN_OVER_BUST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 22 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_UNDER_BUST);
                Inches.add(i,AppConstants.WOMEN_UNDER_BUST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 23 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HIP_BONE);
                Inches.add(i,AppConstants.WOMEN_HIP_BONE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 24 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_THIGH);
                Inches.add(i,AppConstants.WOMEN_THIGH_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 25 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_KNEE);
                Inches.add(i,AppConstants.WOMEN_KNEE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 26 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_CALF);
                Inches.add(i,AppConstants.WOMEN_CALF_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 27 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_ANKLE);
                Inches.add(i,AppConstants.WOMEN_ANKLE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 20 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HEAD);
                Inches.add(i,AppConstants.WOMEN_HEAD_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 28 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_NECK);
                Inches.add(i,AppConstants.WOMEN_NECK_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 29 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_BUST);
                Inches.add(i,AppConstants.WOMEN_BUST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 30 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_WAIST);
                Inches.add(i,AppConstants.WOMEN_WAIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 31 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_FULL_HIP);
                Inches.add(i,AppConstants.WOMEN_FULL_HIP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 32 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HEIGHT);
                Inches.add(i,AppConstants.WOMEN_HEIGHT_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 33 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_STW);
                Inches.add(i,AppConstants.WOMEN_STW_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 34 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_NLTC);
                Inches.add(i,AppConstants.WOMEN_NLTC_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 35 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMENT_NLTB);
                Inches.add(i,AppConstants.WOMENT_NLTB_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 36 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_STHB);
                Inches.add(i,AppConstants.WOMEN_STHB_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 37 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_WTHB);
                Inches.add(i,AppConstants.WOMEN_WTHB_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 38 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HTH);
                Inches.add(i,AppConstants.WOMEN_HTH_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 39 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_INSEAM);
                Inches.add(i,AppConstants.WOMEN_INSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 40 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_OUTSEAM);
                Inches.add(i,AppConstants.WOMEN_OUTSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 41 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_SHOULDER);
                Inches.add(i,AppConstants.WOMEN_SHOULDER_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 42 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_BICEP);
                Inches.add(i,AppConstants.WOMEN_BICEP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 43 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_WRIST);
                Inches.add(i,AppConstants.WOMEN_WRIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 44 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_SLEEVE);
                Inches.add(i,AppConstants.WOMEN_SLEEVE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 45 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HEAD);
                Inches.add(i,AppConstants.BOY_HEAD_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 46 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_NECK);
                Inches.add(i,AppConstants.BOY_NECK_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 47 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_CHEST);
                Inches.add(i,AppConstants.BOY_CHEST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 48 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_WAIST);
                Inches.add(i,AppConstants.BOY_WAIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 49 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_THIGH);
                Inches.add(i,AppConstants.BOY_THIGH_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 50 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_KNEE);
                Inches.add(i,AppConstants.BOY_KNEE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 51 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_ANKLE);
                Inches.add(i,AppConstants.BOY_ANKLE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 52 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_OVER_ALL_HEIGHT);
                Inches.add(i,AppConstants.BOY_OVER_ALL_HEIGHT_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 53 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_SHORTS);
                Inches.add(i,AppConstants.BOY_SHORTS_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 54 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_OUTSEAM);
                Inches.add(i,AppConstants.BOY_OUTSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 55 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_INSEAM);
                Inches.add(i,AppConstants.BOY_INSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 56 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_SHOULDER);
                Inches.add(i,AppConstants.BOY_SHOULDER_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 57 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HALFSLEEVE);
                Inches.add(i,AppConstants.BOY_HALFSLEEVE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 58 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_BICEP);
                Inches.add(i,AppConstants.BOY_BICEP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 59 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HIP);
                Inches.add(i,AppConstants.BOY_HIP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 60 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_BOTTOM);
                Inches.add(i,AppConstants.BOY_BOTTOM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 61 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HEIGHT);
                Inches.add(i,AppConstants.BOY_HEIGHT_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 62 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_SLEEVE);
                Inches.add(i,AppConstants.BOY_SLEEVE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 63 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_WRIST);
                Inches.add(i,AppConstants.BOY_WRIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 65 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_OVER_BURST);
                Inches.add(i,AppConstants.GIRL_OVER_BURST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 66 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_UNDER_BURST);
                Inches.add(i,AppConstants.GIRL_UNDER_BURST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 67 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HIP_BONE);
                Inches.add(i,AppConstants.GIRL_HIP_BONE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 68 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_THIGH);
                Inches.add(i,AppConstants.GIRL_THIGH_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 69 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_KNEE);
                Inches.add(i,AppConstants.GIRL_KNEE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 70 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_CALF);
                Inches.add(i,AppConstants.GIRL_CALF_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 71 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_ANKLE);
                Inches.add(i,AppConstants.GIRL_ANKLE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 64 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HEAD);
                Inches.add(i,AppConstants.GIRL_HEAD_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 72 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_NECK);
                Inches.add(i,AppConstants.GIRL_NECK_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 73 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_BUST);
                Inches.add(i,AppConstants.GIRL_BUST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 74 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_WAIST);
                Inches.add(i,AppConstants.GIRL_WAIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 75 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_FULL_HIP);
                Inches.add(i,AppConstants.GIRL_FULL_HIP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 76 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HEIGHT);
                Inches.add(i,AppConstants.GIRL_HEIGHT_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 77 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_STW);
                Inches.add(i,AppConstants.GIRL_STW_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 78 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_NLTC);
                Inches.add(i,AppConstants.GIRL_NLTC_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 79 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_NLTB);
                Inches.add(i,AppConstants.GIRL_NLTB_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 80 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_STHB);
                Inches.add(i,AppConstants.GIRL_STHB_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 81 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_WTHB);
                Inches.add(i,AppConstants.GIRL_WTHB_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 82 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HTH);
                Inches.add(i,AppConstants.GIRL_HTH_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 83 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_INSEAM);
                Inches.add(i,AppConstants.GIRL_INSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 84 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_OUTSEAM);
                Inches.add(i,AppConstants.GIRL_OUTSEAM_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 85 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_SHOULDER);
                Inches.add(i,AppConstants.GIRL_SHOULDER_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 86 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_BICEP);
                Inches.add(i,AppConstants.GIRL_BICEP_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 87 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_WRIST);
                Inches.add(i,AppConstants.GIRL_WRIST_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mParts.get(i).getId() == 88 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_SLEEVE);
                Inches.add(i,AppConstants.GIRL_SLEEVE_INCHES);


                parts.put(String.valueOf(mParts.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
        }
    }
    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
}