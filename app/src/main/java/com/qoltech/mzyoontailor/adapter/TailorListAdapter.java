package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.TailorListEntity;
import com.qoltech.mzyoontailor.ui.TailorListScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TailorListAdapter extends RecyclerView.Adapter<TailorListAdapter.Holder> {

    private Context mContext;
    private ArrayList<TailorListEntity> mTailorList;
    private int count = 0;

    public TailorListAdapter(Context activity, ArrayList<TailorListEntity> tailorList) {
        mContext = activity;
        mTailorList = tailorList;
    }

    @NonNull
    @Override
    public TailorListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_tailor_list, parent, false);
        return new TailorListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final TailorListAdapter.Holder holder, final int position) {
        final TailorListEntity tailorEntity = mTailorList.get(position);

        for (int i = 0; i<mTailorList.size(); i++){
            mTailorList.get(i).setCount(0);
        }

        holder.mTailorListRatingBar.setRating(tailorEntity.getRating());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTailorList.get(position).getChecked()){
                    mTailorList.get(position).setChecked(false);
                    int sub = --count;
                    ((TailorListScreen)mContext).mTailorSelectedTxt.setText(String.valueOf(sub));
                    for (int i = 0; i<AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.size(); i++){
                        if (mTailorList.get(position).ShopNameInEnglish.equalsIgnoreCase(AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.get(i).ShopNameInEnglish)){
                            AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.remove(i);
                        }
                    }

                }else {
                    mTailorList.get(position).setChecked(true);
                    int add = ++count;
                    ((TailorListScreen)mContext).mTailorSelectedTxt.setText(String.valueOf(String.valueOf(add)));
                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST.add(mTailorList.get(position));
                }
                notifyDataSetChanged();
            }

        });

        holder.mTailorNameTxt.setText(tailorEntity.getTailorNameInEnglish().equalsIgnoreCase("") ? "Abdullah" : tailorEntity.getTailorNameInEnglish());

        holder.mTailorActShopTxt.setText(tailorEntity.getShopNameInEnglish());

        holder.mTailorActOrderNoTxt.setText(String.valueOf(tailorEntity.getOrderCount()));
        try {
            Glide.with(mContext)
                    .load(AppConstants.BASE_URL+"Images/TailorImages/"+tailorEntity.getShopOwnerImageURL())
                    .apply(new RequestOptions().placeholder(R.color.grey_clr).error(R.color.grey_clr))
                    .into(holder.mTailorUserImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.mAdapterTailorParLay.setBackgroundResource(tailorEntity.getChecked() ? R.color.grey_clr : R.color.white);
    }

    @Override
    public int getItemCount() {
        return mTailorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_tailor_par_lay)
        RelativeLayout mAdapterTailorParLay;

        @BindView(R.id.tailor_user_name_txt)
        TextView mTailorNameTxt;

        @BindView(R.id.tailor_act_shop_name_txt)
        TextView mTailorActShopTxt;

        @BindView(R.id.tailor_order_count_txt)
        TextView mTailorActOrderNoTxt;

        @BindView(R.id.tailor_user_img)
        ImageView mTailorUserImg;

        @BindView(R.id.tailor_list_rating_bar)
        RatingBar mTailorListRatingBar;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

