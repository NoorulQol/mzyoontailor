package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsModal;

import java.util.List;

public class GetMeasurementDetailsAdapter extends RecyclerView.Adapter<GetMeasurementDetailsAdapter.ViewHolder> {

    List<GetRequestMeasurementPartsModal> items;
    private Context context;
    private String DRESS_ID = "";
    private PositionUpdateListener listener;
    private UserDetailsEntity mUserDetailsEntityRes;

    public GetMeasurementDetailsAdapter(Context context, List<GetRequestMeasurementPartsModal> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public GetMeasurementDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner2, parent, false);
        GetMeasurementDetailsAdapter.ViewHolder viewHolder = new GetMeasurementDetailsAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GetMeasurementDetailsAdapter.ViewHolder holder, int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        GetRequestMeasurementPartsModal item = items.get(position);
        holder.view.setTag(item);
        Glide.with(context).load(GlobalData.SERVER_URL + "images/Measurement2/" + item.getImage())
                .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)).into(holder.grid_view_img_lay);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            holder.grid_view_txt.setText(item.getMeasurementPartsInArabic().replace("000",""));
        } else {
            holder.grid_view_txt.setText(item.getMeasurementParts().replace("000",""));
        }
//        holder.getMainView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GetBuyerRequestCustomization gender = (GetBuyerRequestCustomization) v.getTag();
//                if (listener != null) {
//                    listener.onPositionUpdate(items.indexOf(gender));
//                }
////                context.startActivity(new Intent(context, DressTypeActivity.class));
//            }
//        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetRequestMeasurementPartsModal item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetRequestMeasurementPartsModal item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout_main;
        ImageView grid_view_img_lay;
        TextView grid_view_txt;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout_main = (LinearLayout) itemView;
            grid_view_img_lay = itemView.findViewById(R.id.grid_custimize_view_img_lay);
            grid_view_txt = itemView.findViewById(R.id.grid_custimize_view_txt);
        }

        public LinearLayout getMainView() {
            return linearLayout_main;
        }
    }
}
