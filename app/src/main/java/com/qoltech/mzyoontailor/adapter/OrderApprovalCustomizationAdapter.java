package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationThreeClickEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderApprovalCustomizationAdapter  extends  RecyclerView.Adapter<OrderApprovalCustomizationAdapter.Holder> {

    private Context mContext;
    ArrayList<CustomizationThreeClickEntity> mOrderApprovalList;
    ArrayList<String> mPosition = new ArrayList<>();

    public OrderApprovalCustomizationAdapter(Context activity, ArrayList<CustomizationThreeClickEntity> OrderApprovalList) {
        mContext = activity;
        mOrderApprovalList = OrderApprovalList;
    }

    @NonNull
    @Override
    public OrderApprovalCustomizationAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_approval_fetch_list, parent, false);
        return new OrderApprovalCustomizationAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderApprovalCustomizationAdapter.Holder holder, final int position) {
        final CustomizationThreeClickEntity orderApprovalEntity = mOrderApprovalList.get(position);
        holder.mOrderApprovalHeadingTxt.setText(orderApprovalEntity.getCustomizationLabelName());
        holder.mOrderApprovalTxt.setText(orderApprovalEntity.getCustomizationSelectedName());

        for(int i=0; i<AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST.size(); i++){
            for (int j=0; j<mOrderApprovalList.size(); j++ ){
                if (AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST.get(i).getAttributeNameInEnglish().equalsIgnoreCase(mOrderApprovalList.get(j).getCustomizationLabelName())){
                    mPosition.add(AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST.get(j).getAttributeImage());
                }
            }

        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+mPosition.get(position))
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mAdapterOrderApprovalImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mOrderApprovalList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_order_approval_img)
        ImageView mAdapterOrderApprovalImg;

        @BindView(R.id.order_approval_heading_txt)
        TextView mOrderApprovalHeadingTxt;

        @BindView(R.id.adapter_order_approval_txt)
        TextView mOrderApprovalTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}