package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.ui.AppointmentDetails;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetAppointmentTimeSlotAdapter extends RecyclerView.Adapter<GetAppointmentTimeSlotAdapter.Holder> {

private Context mContext;
private ArrayList<String> mTimeSlotList;
private Dialog mDialog;
private String mType;

public GetAppointmentTimeSlotAdapter(Context activity, ArrayList<String> getTimeSlotList, Dialog dialog, String type) {
        mContext = activity;
    mTimeSlotList = getTimeSlotList;
        mDialog = dialog;
        mType = type;
        }

@NonNull
@Override
public GetAppointmentTimeSlotAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new GetAppointmentTimeSlotAdapter.Holder(view);
        }

@Override
public void onBindViewHolder(@NonNull final GetAppointmentTimeSlotAdapter.Holder holder, final int position) {

        holder.mGetCountryTxtViewTxt.setText(mTimeSlotList.get(position));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
                  if (mType.equalsIgnoreCase("ORDER_TYPE")){
                      ((AppointmentDetails)mContext).mMaterialTypeTimeSlotTxt.setText(mTimeSlotList.get(position));
                  }else if (mType.equalsIgnoreCase("MEASUREMENT_TYPE")){
                      ((AppointmentDetails)mContext).mMeasurementTypeTimeSlotTxt.setText(mTimeSlotList.get(position));
                  }
        mDialog.dismiss();

        }
        });

        holder.mCountryFlagImg.setVisibility(View.GONE);

        }

@Override
public int getItemCount() {
        return mTimeSlotList.size();
        }

public class Holder extends RecyclerView.ViewHolder {

    @BindView(R.id.get_country_recycler_view_txt)
    TextView mGetCountryTxtViewTxt;

    @BindView(R.id.get_country_flag_img)
    ImageView mCountryFlagImg;


    public Holder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
}




