package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.OrderDetailsActivity;
import com.qoltech.mzyoontailor.activity.SignUpActivity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetTrackingByOrderModal;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackingStatusCheckListAdapter extends RecyclerView.Adapter<TrackingStatusCheckListAdapter.ViewHolder> {

    Context context;

    private List<GetTrackingByOrderModal> getTrackingByOrderModals;
    GetTrackingByOrderModal trackingByOrderModal;
    private PositionUpdateListener listener;
    Dialog dialog;
    SignUpActivity newAccountActivity;
    String[] parts;
    OrderDetailsActivity orderDetailsActivity = new OrderDetailsActivity();
    public static int pos = 0;
    private ApiService restService;
    private UserDetailsEntity mUserDetailsEntityRes;
    int incr = 0;

    public TrackingStatusCheckListAdapter(Context context, Dialog dialog,
                                          ApiService restService, List<GetTrackingByOrderModal> getTrackingByOrderModals,
                                          GetTrackingByOrderModal trackingByOrderModal) {
        this.context = context;
        this.dialog = dialog;
        this.restService = restService;
        this.getTrackingByOrderModals = getTrackingByOrderModals;
        this.trackingByOrderModal = trackingByOrderModal;


    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_status_tracking, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final GetTrackingByOrderModal item = getTrackingByOrderModals.get(position);
        holder.check_box.setEnabled(false);
//        final GetTrackingByOrderModal item1 = getTrackingByOrderModals.get(position);
//        if (getTrackingByOrderModals.get(position).getSwitch() == false) {
//            holder.itemView.setEnabled(true);
//            holder.check_box.setEnabled(false);
//            holder.check_box.setChecked(false);
//        }\


//        if (getTrackingByOrderModals.get(position).getSwitch() == true) {
//            holder.itemView.setEnabled(false);
//            holder.check_box.setEnabled(false);
//            holder.check_box.setChecked(true);
//        }
//


//1
        if (getTrackingByOrderModals.get(0).getSwitch() == true &&
                getTrackingByOrderModals.get(0).getSwitchStatus() == 0) {

            if (position == 1) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 1) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }

//        2

        if (getTrackingByOrderModals.get(1).getSwitch() == true &&
                getTrackingByOrderModals.get(1).getSwitchStatus() == 0) {

            if (position == 2) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 2) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }
//        3
        if (getTrackingByOrderModals.get(2).getSwitch() == true &&
                getTrackingByOrderModals.get(2).getSwitchStatus() == 0) {

            if (position == 3) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 3) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }


//        4
        if (getTrackingByOrderModals.get(3).getSwitch() == true &&
                getTrackingByOrderModals.get(3).getSwitchStatus() == 0) {

            if (position == 4) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);


            }

        } else {
            if (position == 4) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }
//        5
        if (getTrackingByOrderModals.get(4).getSwitch() == true &&
                getTrackingByOrderModals.get(4).getSwitchStatus() == 0) {

            if (position == 5) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 5) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }

//        6
        if (getTrackingByOrderModals.get(5).getSwitch() == true &&
                getTrackingByOrderModals.get(5).getSwitchStatus() == 0) {

            if (position == 6) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 6) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }

//        7
        if (getTrackingByOrderModals.get(6).getSwitch() == true &&
                getTrackingByOrderModals.get(6).getSwitchStatus() == 0) {

            if (position == 7) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 7) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }

//        8
        if (getTrackingByOrderModals.get(7).getSwitch() == true &&
                getTrackingByOrderModals.get(7).getSwitchStatus() == 0) {

            if (position == 8) {
                holder.itemView.setEnabled(true);
                holder.check_box.setEnabled(true);
                holder.check_box.setChecked(false);
            }

        } else {
            if (position == 8) {
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                holder.check_box.setChecked(true);
            }
        }


        if (position == 7) {
            holder.view_grey.setVisibility(View.GONE);
        }


        for (int i = 0; i < getTrackingByOrderModals.size(); i++) {
            if (getTrackingByOrderModals.get(i).getSwitchStatus() == 0) {
                incr = getTrackingByOrderModals.get(i).getId();

                break;

            }


////            switch ()
//            break;


//            switch (getTrackingByOrderModals.get(item.getId()).getSwitchStatus()) {
//                case 0:
//                    incr = getTrackingByOrderModals.get(item.getId()).getId();
//
//                    break;

//            }
        }
//        if (item.getSwitch() == true) {
//            holder.itemView.setVisibility(View.GONE);
//        }
        holder.check_box.setChecked(item.getSwitch());

        holder.check_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.check_box.setChecked(true);
//                Toast.makeText(context, "" + holder.getPosition() + item.getId(), Toast.LENGTH_LONG).show();
                holder.itemView.setEnabled(false);
                holder.check_box.setEnabled(false);
                pos = item.getId();
//                incr = pos - 1;


//                orderDetailsActivity.updateTrackingStatus();
//                notifyDataSetChanged();

                update(String.valueOf(pos));

                getTrackingStatus(AppConstants.ORDER_ID);

//                if (item.getSwitchStatus() == 0) {
//                    incr = item.getId();
//                }
                holder.check_box.setChecked(true);
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    Toast.makeText(context, item.getStatusInArabic(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, item.getStatus(), Toast.LENGTH_LONG).show();
                }


//                if (position == 7) {
//                    UpdateDeliveryStatus(AppConstants.ORDER_ID);
//                }


                if (position==getTrackingByOrderModals.size()-1) {
                    UpdateDeliveryStatus(AppConstants.ORDER_ID);
                }
            }
        });
        holder.view.setTag(item);
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(holder.bgLayout, ViewCompat.LAYOUT_DIRECTION_RTL);
            holder.check_text.setText(item.getStatusInArabic());
            holder.check_text.setSelected(true);
        } else {
            ViewCompat.setLayoutDirection(holder.bgLayout, ViewCompat.LAYOUT_DIRECTION_LTR);
            holder.check_text.setText(item.getStatus());
            holder.check_text.setSelected(true);
        }
    }

    @Override
    public int getItemCount() {
        return getTrackingByOrderModals.size();
    }

    public void add(GetTrackingByOrderModal item, int position) {
        getTrackingByOrderModals.add(position, item);
        notifyItemInserted(position);
    }


    public void add(GetTrackingByOrderModal item) {
        getTrackingByOrderModals.add(item);
        notifyItemInserted(getTrackingByOrderModals.size());
    }


//    class ViewHolder extends RecyclerView.ViewHolder {
//        RelativeLayout relativeLayout_main;
//        TextView check_text;
//        //        Switch switch_skill_update;
//        View view;
//        AppCompatCheckBox check_box;
//        LinearLayout linearLayout;
//
//        private ViewHolder(View itemView) {
//            super(itemView);
//            view = itemView;
//            linearLayout = (LinearLayout) itemView;
//            check_text = itemView.findViewById(R.id.check_text);
//            check_box = itemView.findViewById(R.id.check_box);
////            switch_skill_update = itemView.findViewById(R.id.switch_skill_update);
//        }
//
//        public LinearLayout getMainView() {
//            return linearLayout;
//        }
//    }


    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main, bgLayout;
        TextView check_text;
        View view, view_grey;
        AppCompatCheckBox check_box;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            check_text = itemView.findViewById(R.id.check_text);
            check_box = itemView.findViewById(R.id.check_box);
            view_grey = (View) itemView.findViewById(R.id.view_grey);
            bgLayout = (RelativeLayout) itemView.findViewById(R.id.bgLayout);

        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }


    public void update(String trackingId) {
        if (new UtilService().isNetworkAvailable(context)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("OrderId", AppConstants.ORDER_ID);
            map.put("TrackingStatusId", trackingId);
            restService.updateTracking(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


                    if (response.body().getResult().equalsIgnoreCase("Approved Pending or Appoinment Not Approved")) {
                        Toast.makeText(context, response.body().getResult(), Toast.LENGTH_LONG).show();
                    } else {
                        updateTrackingRem(String.valueOf(incr));
                    }
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } else {
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void getTrackingStatus(String orderId) {


        restService.getTrackingDetailsResponse(orderId).enqueue(new Callback<GetTrackingDetailsResponse>() {
            @Override
            public void onResponse(Call<GetTrackingDetailsResponse> call, Response<GetTrackingDetailsResponse> response) {


                try {

//                    getTrackingDetailsModals = response.body().getResult().getTrackingDetails();
                    getTrackingByOrderModals = response.body().getResult().getGetTrackingByOrder();
//                    setCountrySelectionList();
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<GetTrackingDetailsResponse> call, Throwable t) {

            }
        });
    }
//    UpdateDeliveryStatus

    public void UpdateDeliveryStatus(String orderId) {
        if (new UtilService().isNetworkAvailable(context)) {

            boolean iscompleted = true;
            restService.UpdateDeliveryStatus(orderId, iscompleted).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } else {
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateTrackingRem(String trackingId) {
        if (new UtilService().isNetworkAvailable(context)) {
            HashMap<String, String> map = new HashMap<>();
            map.put("OrderId", AppConstants.ORDER_ID);
            map.put("TrackingStatusId", trackingId);
            restService.updateTrackingRemember(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } else {
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}