package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

public class AdapterOneToHundred extends RecyclerView.Adapter<AdapterOneToHundred.ViewHolder> {

    Context context;
    MaterialUsedAdapter materialUsedAdapter;
    private ArrayList<String> mDataset;

    public AdapterOneToHundred(Context context, ArrayList<String> mDataset) {
        this.mDataset = mDataset;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.material_used_recyclerview_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.no_of_adapter.setText(mDataset.get(position));
        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstants.CHECK_ADD_ADDRESS=mDataset.get(position);
//                holder.getMainView().findViewById()
                materialUsedAdapter.mOrderConfirmDialog.dismiss();
//                materialUsedFragment.quantity.setText(String.valueOf(mDataset.get(position)));
//                Country country = (Country) v.getTag();
//                if (listener != null) {
//                    newAccountActivity.onUserTappedCountry(countries.get(position));
//                }
//                newAccountActivity.textCountrySelection.setText(country.getPhoneCode());
//                Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + country.getFlag())
//                        .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                .placeholder(R.drawable.placeholder)).into(newAccountActivity.flagsImg);
//                if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
//                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                    dialog.dismiss();
//
//                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


   public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout_main;
        TextView no_of_adapter;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout_main = (LinearLayout) itemView;
            no_of_adapter = itemView.findViewById(R.id.no_of_adapter);

        }

        public LinearLayout getMainView() {
            return linearLayout_main;
        }
    }
}