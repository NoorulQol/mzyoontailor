package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.MeasurementOneEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;


public class MeasurementSkillUpdateAdapter extends RecyclerView.Adapter<MeasurementSkillUpdateAdapter.ViewHolder> {

    List<MeasurementOneEntity> items;
    private Context context;
    private PositionUpdateListener listener;
    ApiService restService;
    SharedPreferences sharedPreferences;
    ArrayList<String> mMeasurementId = new ArrayList<>();
    private UserDetailsEntity mUserDetailsEntityRes;

    public MeasurementSkillUpdateAdapter(Context context, List<MeasurementOneEntity> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_check_list_one, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
//        mMeasurementId = new ArrayList<>();

        MeasurementOneEntity item = items.get(position);
        holder.view.setTag(item);
        AppConstants.SKILL_MEASUREMENT_ID = new ArrayList<>();


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.check_text.setText(item.getMeasurementInArabic());
        } else {
            holder.check_text.setText(item.getMeasurementInEnglish());

        }


        if (item.get_switch() == true) {
            holder.check_box.setChecked(true);
            mMeasurementId.add(String.valueOf(items.get(position).getId()));
            AppConstants.SKILL_MEASUREMENT_ID = mMeasurementId;


        }

//        holder.switch_skill_update.setChecked(item.get_switch());

//        for (int i = 0; i < items.size(); i++) {
//            mMeasurementId.add(i, "0");
//            if (items.get(i).get_switch()) {
//                mMeasurementId.add(i, String.valueOf(items.get(i).getId()));
//            }
//
//        }
//
//
//        try {
//
//            Glide.with(context).load(GlobalData.SERVER_URL + "Images/Measurement1/" + item.getBodyImage())
//                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
//                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .placeholder(R.drawable.placeholder)).into(holder.gender_image);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.switch_skill_update.isChecked() == true) {
//                    holder.switch_skill_update.setChecked(false);
//                    mMeasurementId.remove(holder.getAdapterPosition());
//                } else {
//                    holder.switch_skill_update.setChecked(true);
//                    mMeasurementId.add(String.valueOf(items.get(position).getId()));
//
//                }
//                AppConstants.SKILL_MEASUREMENT_ID = new ArrayList<>();
//                AppConstants.SKILL_MEASUREMENT_ID = mMeasurementId;

//                InsertMeasurementSkillsByTailor(sharedPreferences.getString("TailorId", ""),
//                        String.valueOf(items.get(position).getId()));
//                holder.switch_skill_update.setChecked(true);
//                holder.itemView.setEnabled(false);

//            }
//        });

        holder.check_box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (holder.check_box.isChecked() == true) {
//                    mMeasurementId.remove(getAdapterPosition);

//                } else {


//                }


                if (holder.check_box.isChecked() == true) {
                    mMeasurementId.add(String.valueOf(items.get(position).getId()));

                } else {
                    mMeasurementId.remove(String.valueOf(items.get(position).getId()));
                }
                AppConstants.SKILL_MEASUREMENT_ID = new ArrayList<>();
                AppConstants.SKILL_MEASUREMENT_ID = mMeasurementId;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(MeasurementOneEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(MeasurementOneEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main, text_layout, adapter_lay;
        ImageView gender_image;
        TextView check_text;
        AppCompatCheckBox check_box;
        View view;
        LinearLayout linearLayout;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout = (LinearLayout) itemView;
//            gender_image = itemView.findViewById(R.id.images);
            check_text = itemView.findViewById(R.id.check_text);
            check_box = itemView.findViewById(R.id.check_box);
        }

        public LinearLayout getMainView() {
            return linearLayout;
        }
    }

//    public void InsertMeasurementSkillsByTailor(String tailorId, String genderId) {
//        if (new UtilService().isNetworkAvailable(context)) {
//            HashMap<String, String> map = new HashMap<>();
//            map.put("TailorId", tailorId);
//            map.put("MeasurementTypeId", genderId);
//            restService.InsertMeasurementSkillsByTailor(map).enqueue(new Callback<SignUpResponse>() {
//                @Override
//                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//
//
//                }
//
//                @Override
//                public void onFailure(Call<SignUpResponse> call, Throwable t) {
//
//                }
//            });
//        } else {
//            try {
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

}