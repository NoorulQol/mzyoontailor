package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.SkillUpdateGetLocationEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LocationSkillUpdateAdapter extends RecyclerView.Adapter<LocationSkillUpdateAdapter.ViewHolder> {

    List<SkillUpdateGetLocationEntity> items;
    private Context context;
    private PositionUpdateListener listener;
    public int pos;
    ApiService restService;
    SharedPreferences sharedPreferences;

    ArrayList<String> mLocationId;
    public LocationSkillUpdateAdapter(Context context, List<SkillUpdateGetLocationEntity> items,ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService=restService;
        sharedPreferences=context.getSharedPreferences("TailorId",Context.MODE_PRIVATE);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gender_skill_update, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        mLocationId = new ArrayList<>();
        AppConstants.SKILL_LOCATION_ID = new ArrayList<>();

        final SkillUpdateGetLocationEntity item = items.get(position);
        holder.view.setTag(item);
        holder.text_skill_update.setText(""+item.getArea());

        holder.switch_skill_update.setChecked(item.get_switch());
        if (items.get(position).get_switch()==true){
            holder.itemView.setEnabled(false);
            holder.switch_skill_update.setEnabled(false);
//            holder.tracking_update_switch.setChecked(true);
        }
        for(int i=0; i<items.size(); i++){
            mLocationId.add(i,"0");
            if (items.get(i).get_switch()){
                mLocationId.add(i,String.valueOf(items.get(i).getId()));
            }

        }

//        try {
//
//            Glide.with(context).load(GlobalData.SERVER_URL + "Images/" + item.getImageURL())
//                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
//                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .placeholder(R.drawable.placeholder)).into(holder.gender_image);
//
//        } catch (Exception ex) {
//            Log.e(AppConstants.TAG, ex.getMessage());
//        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (holder.switch_skill_update.isChecked() == true) {
//                    holder.switch_skill_update.setChecked(false);
//                    mLocationId.remove(holder.getAdapterPosition());
//                } else {
//                    holder.switch_skill_update.setChecked(true);
//                    mLocationId.add(String.valueOf(items.get(position).getId()));
//
//                }
//                AppConstants.SKILL_LOCATION_ID = new ArrayList<>();
//                AppConstants.SKILL_LOCATION_ID = mLocationId;

                insertServiceSkillsByTailor(sharedPreferences.getString("TailorId", ""),
                        String.valueOf(items.get(position).getId()));
                holder.switch_skill_update.setChecked(true);
                holder.itemView.setEnabled(false);


            }
        });
//
//        holder.switch_skill_update.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (holder.switch_skill_update.isChecked() == true) {
//                    mLocationId.remove(holder.getAdapterPosition());
//                } else {
//
//                    mLocationId.add(String.valueOf(items.get(position).getId()));
//
//                }
//                AppConstants.SKILL_LOCATION_ID = new ArrayList<>();
//                AppConstants.SKILL_LOCATION_ID = mLocationId;
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(SkillUpdateGetLocationEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(SkillUpdateGetLocationEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView gender_image;
        TextView text_skill_update;
        Switch switch_skill_update;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            gender_image = itemView.findViewById(R.id.images);
            text_skill_update = itemView.findViewById(R.id.text_skill_update);
            switch_skill_update = itemView.findViewById(R.id.switch_skill_update);
            gender_image.setVisibility(View.GONE);
        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }

    public void insertServiceSkillsByTailor(String tailorId, String areaId) {
            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", tailorId);
            map.put("AreaId", areaId);
            restService.InsertServiceSkillsByTailor(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


//                    updateGenderSwitch(response.body().getResult(), "1");
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
    }

}