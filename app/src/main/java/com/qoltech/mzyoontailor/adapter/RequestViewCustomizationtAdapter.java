package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GenderEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.DressTypeScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestViewCustomizationtAdapter extends RecyclerView.Adapter<RequestViewCustomizationtAdapter.Holder> {

    private Context mContext;
    private ArrayList<GenderEntity> mGenderList;

//    private CitySearchFragment mCitySearchFragment;

    public RequestViewCustomizationtAdapter(Context activity, ArrayList<GenderEntity> genderList) {
        mContext = activity;
        mGenderList = genderList;
    }

    @NonNull
    @Override
    public RequestViewCustomizationtAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gridview, parent, false);
        return new RequestViewCustomizationtAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RequestViewCustomizationtAdapter.Holder holder, int position) {

        final GenderEntity genderEntity = mGenderList.get(position);

        holder.mGridViewTxt.setText(genderEntity.getGender());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.GENDER_ID =String.valueOf(genderEntity.getId());
                ((BaseActivity) mContext).nextScreen(DressTypeScreen.class, true);
            }
        });

        try {
            Glide.with(mContext)
                    .load(AppConstants.BASE_URL+"images/"+genderEntity.getImageURL())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
            Log.e(AppConstants.TAG, ex.getMessage());
        }
//                try {
//                    holder.mCitiesImgBg.setVisibility(View.VISIBLE);
//
//                    Glide.with(mContext)
//                            .load(withEntity.getPhoto())
//                            .apply(new RequestOptions().placeholder(R.color.blue).error(R.color.blue))
//                            .into(holder.mCitiesImg);
//
//                } catch (Exception ex) {
//                    holder.mCitiesImg.setImageResource(R.drawable.demo_img);
//                    Log.e(AppConstants.TAG, ex.getMessage());
//                }





    }

    @Override
    public int getItemCount() {
        return mGenderList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_view_txt)
        TextView mGridViewTxt;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

