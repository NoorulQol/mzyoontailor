package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetCountryEntity;
import com.qoltech.mzyoontailor.ui.AddAddressScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryPickerAdapter extends RecyclerView.Adapter<CountryPickerAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetCountryEntity> mCountryList;
    private Dialog mDialog;

    public CountryPickerAdapter(Context activity, ArrayList<GetCountryEntity> getCountryList, Dialog dialog) {
        mContext = activity;
        mCountryList = getCountryList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public CountryPickerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new CountryPickerAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CountryPickerAdapter.Holder holder, final int position) {
        final GetCountryEntity getCountryEntity = mCountryList.get(position);

        String string = getCountryEntity.getCountryName();
        String[] parts = string.split("\\(");
        String part1 = parts[0];


        holder.mGetCountryTxtViewTxt.setText(part1);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String strings = mCountryList.get(position).getCountryName();
                String[] part = strings.split("\\(");
                String parts1 = part[0];

                AppConstants.COUNTRY_CODE_ID = String.valueOf(getCountryEntity.getId());


                ((AddAddressScreen)mContext).mAddAddressCountyrNameEdtTxt.setText(parts1);
                ((AddAddressScreen)mContext).mAddAddressStateEdtTxt.setText("");
                ((AddAddressScreen)mContext).mAddressAreaEdtTxt.setText("");

                ((AddAddressScreen)mContext).getStateApiCall(String.valueOf(getCountryEntity.getId()));
                ((AddAddressScreen)mContext).getAreaApiCall("");

                mDialog.dismiss();

            }
        });
        holder.mCountryFlagImg.setVisibility(View.GONE);


    }

    @Override
    public int getItemCount() {
        return mCountryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


