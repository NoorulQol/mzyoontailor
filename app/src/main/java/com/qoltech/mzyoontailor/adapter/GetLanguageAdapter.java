package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.SignUpActivity;
import com.qoltech.mzyoontailor.entity.GetLanguageEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetLanguageAdapter extends RecyclerView.Adapter<GetLanguageAdapter.Holder> {

    private Context mContext;
    private Dialog mDialog;
    private ArrayList<GetLanguageEntity> mLanguageCountryList;

    public GetLanguageAdapter(Context activity, ArrayList<GetLanguageEntity> languageCountryList , Dialog dialog) {
        mContext = activity;
        mLanguageCountryList = languageCountryList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public GetLanguageAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new GetLanguageAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetLanguageAdapter.Holder holder, final int position) {

        holder.mGetCountryTxtViewTxt.setText(mLanguageCountryList.get(position).getLanguage());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mLanguageCountryList.get(position).getLanguage().equalsIgnoreCase("English")){
                    try {
                        Glide.with(mContext)
                                .load(mContext.getResources().getDrawable(R.drawable.america_flag))
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(((SignUpActivity)mContext).mLangFlagImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }
                if (mLanguageCountryList.get(position).getLanguage().equalsIgnoreCase("Arabic")){
                    try {
                        Glide.with(mContext)
                                .load(mContext.getResources().getDrawable(R.drawable.uae_national_flag))
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(((SignUpActivity)mContext).mLangFlagImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }
                }


                ((SignUpActivity)mContext).mChooseLanguageTxt.setText(mLanguageCountryList.get(position).getLanguage());
                ((SignUpActivity)mContext).getLanguage();
                mDialog.dismiss();

            }
        });
        if (mLanguageCountryList.get(position).getLanguage().equalsIgnoreCase("English")){
            try {
                Glide.with(mContext)
                        .load(mContext.getResources().getDrawable(R.drawable.america_flag))
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(holder.mCountryFlagImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }
        if (mLanguageCountryList.get(position).getLanguage().equalsIgnoreCase("Arabic")){
            try {
                Glide.with(mContext)
                        .load(mContext.getResources().getDrawable(R.drawable.uae_national_flag))
                        .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                        .into(holder.mCountryFlagImg);

            } catch (Exception ex) {
                Log.e(AppConstants.TAG, ex.getMessage());
            }
        }


    }

    @Override
    public int getItemCount() {
        return mLanguageCountryList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


