package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.fragments.MaterialUsedFragment;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetAttributeDetailModal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MaterialUsedAdapter extends RecyclerView.Adapter<MaterialUsedAdapter.ViewHolder> {

    List<GetAttributeDetailModal> items;
    private Context context;
    private PositionUpdateListener listener;
    MaterialUsedFragment materialUsedFragment = new MaterialUsedFragment();
    public static Dialog mOrderConfirmDialog;
    public static AdapterOneToHundred mConfirmAdapter;
    public static ArrayList<String> mArrayList;
    public static HashMap<String,String> qtyHashMap = new HashMap<>();
private UserDetailsEntity mUserDetailsEntityRes;
    int positionInt = -1;

    public MaterialUsedAdapter(Context context, List<GetAttributeDetailModal> items) {
        this.items = items;
        this.context = context;

    }

    @Override
    public MaterialUsedAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.material_used_adapter1, parent, false);
        MaterialUsedAdapter.ViewHolder viewHolder = new MaterialUsedAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MaterialUsedAdapter.ViewHolder holder, final int position) {


        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        GetAttributeDetailModal item = items.get(position);
        holder.view.setTag(item);


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.attribute_names.setText(item.getAttributeNameinArabic());
            holder.attribute_types.setText(item.getAttributeTypeNameInArabic());


        }else {
            holder.attribute_names.setText(item.getAttributeNameInEnglish());
            holder.attribute_types.setText(item.getAttributeTypeNameInEnglish());

        }



        holder.quantity.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               qtyHashMap.put(String.valueOf(holder.getAdapterPosition()),s.toString());
               AppConstants.MATERIAL_MAP = qtyHashMap;
           }

           @Override
           public void afterTextChanged(Editable s) {

           }
       });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetAttributeDetailModal item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetAttributeDetailModal item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayoutmain;
        TextView attribute_names, attribute_types;
        ImageView image_dropdown;
        EditText quantity;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayoutmain = (RelativeLayout) itemView;
            attribute_names = itemView.findViewById(R.id.attribute_names);
            attribute_types = itemView.findViewById(R.id.attribute_types);
            quantity = itemView.findViewById(R.id.quantity);
//            image_dropdown = itemView.findViewById(R.id.image_dropdown);
        }

        public RelativeLayout getMainView() {
            return relativeLayoutmain;
        }
    }

    public static Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }


    public void setAdapter(ArrayList<String> mArrayList) {


//        alertDismiss(mOrderConfirmDialog);
        mOrderConfirmDialog = getDialog(context, R.layout.adapter_one_to_nine);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mOrderConfirmDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        RecyclerView recyclerView;

        /*Init view*/

        recyclerView = mOrderConfirmDialog.findViewById(R.id.countryList);
        mConfirmAdapter = new AdapterOneToHundred(context, mArrayList);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mConfirmAdapter);

        alertShowing(mOrderConfirmDialog);

    }


    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

}
