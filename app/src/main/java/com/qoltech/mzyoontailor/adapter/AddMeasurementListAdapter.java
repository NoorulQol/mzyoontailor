package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.ViewMeasurementActivity;
import com.qoltech.mzyoontailor.entity.AddMeasurementEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddMeasurementListAdapter extends RecyclerView.Adapter<AddMeasurementListAdapter.Holder> {

    private Context mContext;
    private ArrayList<AddMeasurementEntity> mMeasurementList;

    private UserDetailsEntity mUserDetailsEntityRes;

    public AddMeasurementListAdapter(Context activity, ArrayList<AddMeasurementEntity> measurementList) {
        mContext = activity;
        mMeasurementList = measurementList;
    }

    @NonNull
    @Override
    public AddMeasurementListAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_add_measurement_list, parent, false);
        return new AddMeasurementListAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final AddMeasurementListAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final AddMeasurementEntity mList = mMeasurementList.get(position);

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL + "Images/DressSubType/" + mList.getImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mMeasurementImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }
        holder.mAddMeasurementNameTxt.setText(mList.getName());
        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.mAddMeasuremenDressTypeTxt.setText(mList.getGender() + " - " + mList.getNameInEnglish());
        } else {
            holder.mAddMeasuremenDressTypeTxt.setText(mList.getNameInEnglish() + " - " + mList.getGender());

        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.MEASUREMENT_MANUALLY = mMeasurementList.get(position).getName();
                AppConstants.MEASUREMENT_ID = String.valueOf(mMeasurementList.get(position).getId());
                AppConstants.MEASUREMENT_EXISTING = "EXISTING";
                AppConstants.EXISTING_MEASUREMENT_ID = String.valueOf(mList.getId());
                mContext.startActivity(new Intent(mContext, ViewMeasurementActivity.class));

            }
        });

    }

    @Override
    public int getItemCount() {
        return mMeasurementList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.profile_image)
        de.hdodenhof.circleimageview.CircleImageView mMeasurementImg;

        @BindView(R.id.add_measurement_list_measurement_name_txt)
        TextView mAddMeasurementNameTxt;

        @BindView(R.id.add_measurement_list_dress_type_txt)
        TextView mAddMeasuremenDressTypeTxt;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


