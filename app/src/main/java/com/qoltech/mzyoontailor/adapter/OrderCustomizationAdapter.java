package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationThreeClickEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderCustomizationAdapter extends RecyclerView.Adapter<OrderCustomizationAdapter.Holder> {

    private Context mContext;
    ArrayList<CustomizationThreeClickEntity> mOrderApprovalList;
    ArrayList<String> mPosition = new ArrayList<>();

    private UserDetailsEntity mUserDetailsEntityRes;
    public OrderCustomizationAdapter(Context activity, ArrayList<CustomizationThreeClickEntity> ordeApprovalList) {
        mContext = activity;
        mOrderApprovalList = ordeApprovalList;
    }

    @NonNull
    @Override
    public OrderCustomizationAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
        return new OrderCustomizationAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderCustomizationAdapter.Holder holder, final int position) {
        final CustomizationThreeClickEntity orderApprovalEntity = mOrderApprovalList.get(position);

        holder.mGridViewTxt.setText(orderApprovalEntity.getCustomizationLabelName()+"\n"+ " - " +"\n"  + orderApprovalEntity.getCustomizationSelectedName());

        for(int i=0; i<AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST.size(); i++){
            for (int j=0; j<mOrderApprovalList.size(); j++ ){
                if (AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST.get(i).getAttributeNameInEnglish().equalsIgnoreCase(mOrderApprovalList.get(j).getCustomizationLabelName())){
                    mPosition.add(AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST.get(j).getAttributeImage());
                }
            }

        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Customazation3/"+orderApprovalEntity.getImages())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return mOrderApprovalList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
