package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.NewCustomerDetails;
import com.qoltech.mzyoontailor.activity.SignUpActivity;
import com.qoltech.mzyoontailor.fragments.ShopDetailsFrag;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.Country;
import com.qoltech.mzyoontailor.wrappers.StateResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountryCodeAdapter extends RecyclerView.Adapter<CountryCodeAdapter.ViewHolder> {

    Context context;

    private List<Country> countries;
    Country COUNTRY_ID;
    private PositionUpdateListener listener;
    Dialog dialog;
    SignUpActivity newAccountActivity;
    String[] parts;
    private ApiService restService;

    public CountryCodeAdapter(Context context, List<Country> countries, Country id, Dialog dialog) {
        this.countries = countries;
        this.context = context;
        this.COUNTRY_ID = id;
        this.dialog = dialog;
    }

    public CountryCodeAdapter(Context context, List<Country> countries, Country id, Dialog dialog, ApiService restService) {
        this.countries = countries;
        this.context = context;
        this.COUNTRY_ID = id;
        this.dialog = dialog;
        this.restService = restService;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Country item = countries.get(position);

        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Country country = (Country) v.getTag();
                if (listener != null) {
                    newAccountActivity.onUserTappedCountry(countries.get(position));

                }
                if (AppConstants.COUNRTY_CODE_ADAPTER == "SIGN_UP") {
                    try {
                        Glide.with(context)
                                .load(GlobalData.SERVER_URL + "images/flags/" + country.getFlag())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(newAccountActivity.flagsImg);

                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                    }

                    newAccountActivity.textCountrySelection.setText(country.getPhoneCode());
//                    Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + country.getFlag())
//                            .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                    .placeholder(R.drawable.placeholder)).into(newAccountActivity.flagsImg);
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();

                    }
                } else if (AppConstants.COUNRTY_CODE_ADAPTER == "SHOP_DETAILS") {
//                    for (int i = 0; i < countries.size(); i++) {
                    String string = countries.get(position).getCountryName();
                    parts = string.split("\\(");
                    String part1 = parts[0]; // 004
                    String part2 = parts[1];


//                    }


                    ShopDetailsFrag.countries_text.setText(parts[0]);

                    AppConstants.COUNTRY_ID = String.valueOf(countries.get(position).getId());
                    ShopDetailsFrag.dialogManager.showProgress(context);
//                    Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + country.getFlag())
//                            .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                    .placeholder(R.drawable.placeholder)).into(newAccountActivity.flagsImg);
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();


//                        ShopDetailsFrag.getStateList();

                    }
                } else if (AppConstants.COUNRTY_CODE_ADAPTER == "NEW_CUSTOMER") {
//                    for (int i = 0; i < countries.size(); i++) {
                    String string = countries.get(position).getCountryName();
                    parts = string.split("\\(");
                    String part1 = parts[0]; // 004
                    String part2 = parts[1];


//                    }


                    NewCustomerDetails.add_address_country_edt_txt.setText(parts[0]);

                    AppConstants.COUNTRY_ID = String.valueOf(countries.get(position).getId());
//                    getStateList(AppConstants.COUNTRY_ID);
//                    ShopDetailsFrag.progressDialog.show();
//                    Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + country.getFlag())
//                            .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                    .placeholder(R.drawable.placeholder)).into(newAccountActivity.flagsImg);
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();


//                        ShopDetailsFrag.getStateList();
                        NewCustomerDetails.  add_address_state_edt_txt.setText("");
                        NewCustomerDetails.  add_address_area_edt_txt.setText("");

                    }
                }

            }
        });

        holder.view.setTag(item);
        holder.countryName.setText(item.getCountryName());
        holder.countryName.setSelected(true);
        Glide.with(context).load(GlobalData.SERVER_URL + "images/flags/" + item.getFlag())
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)).into(holder.flagsImg);
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public void add(Country item, int position) {
        countries.add(position, item);
        notifyItemInserted(position);
    }


    public void add(Country item) {
        countries.add(item);
        notifyItemInserted(countries.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView flagsImg;
        TextView countryName;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            flagsImg = itemView.findViewById(R.id.flagsImg);
            countryName = itemView.findViewById(R.id.countryName);

        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }

    public void getStateList(String country_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("Id",country_id);

        {
            restService.stateList(map).enqueue(new Callback<StateResponse>() {
                @Override
                public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {

                }

                @Override
                public void onFailure(Call<StateResponse> call, Throwable t) {

                }
            });
        }
    }


}