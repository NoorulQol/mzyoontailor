package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.NewCustomerDetails;
import com.qoltech.mzyoontailor.fragments.ShopDetailsFrag;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.State;

import java.util.List;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.ViewHolder> {

    Context context;

    private List<State> countries;
    State COUNTRY_ID;
    private PositionUpdateListener listener;
    Dialog dialog;
    ShopDetailsFrag newAccountActivity;
    String[] parts;

    public StateAdapter(Context context, List<State> countries, State id, Dialog dialog) {
        this.countries = countries;
        this.context = context;
        this.COUNTRY_ID = id;
        this.dialog = dialog;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final State item = countries.get(position);

        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                State state = (State) v.getTag();
                if (listener != null) {
                    newAccountActivity.onUserTappedState(countries.get(position));
                }

                if (AppConstants.COUNRTY_CODE_ADAPTER == "SHOP_DETAILS") {
                    ShopDetailsFrag.state_text.setText(state.getStateName());
                    AppConstants.STATE_ID_SHOP=String.valueOf(state.getId());
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();

                    }
                } else if (AppConstants.COUNRTY_CODE_ADAPTER == "NEW_CUSTOMER") {
                    NewCustomerDetails.add_address_state_edt_txt.setText(state.getStateName());
                    AppConstants.STATE_ID = String.valueOf(state.getId());
                    NewCustomerDetails.add_address_area_edt_txt.setText("");
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();


                    }
                }
            }


        });

        holder.view.setTag(item);
        holder.countryName.setText(item.getStateName());
        holder.countryName.setSelected(true);
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public void add(State item, int position) {
        countries.add(position, item);
        notifyItemInserted(position);
    }


    public void add(State item) {
        countries.add(item);
        notifyItemInserted(countries.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView flagsImg;
        TextView countryName;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            flagsImg = itemView.findViewById(R.id.flagsImg);
            countryName = itemView.findViewById(R.id.countryName);
            flagsImg.setVisibility(View.INVISIBLE);

        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }
}