package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationPatternsEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.ViewDetailsScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoPatternAdapter extends  RecyclerView.Adapter<CustomizationTwoPatternAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationPatternsEntity> mCustomizePatternList;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationTwoPatternAdapter(Context activity, ArrayList<CustomizationPatternsEntity> customizePatternList) {
        mContext = activity;
        mCustomizePatternList = customizePatternList;
    }

    @NonNull
    @Override
    public CustomizationTwoPatternAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
        return new CustomizationTwoPatternAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationTwoPatternAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationPatternsEntity PatternEntity = mCustomizePatternList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(PatternEntity.getPatternInArabic());

        }else {
            holder.mGridViewTxt.setText(PatternEntity.getPatternInEnglish());

        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Pattern/"+PatternEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < mCustomizePatternList.size(); i++) {
                    mCustomizePatternList.get(i).setChecked(false);
                }
                mCustomizePatternList.get(position).setChecked(true);
                AppConstants.PATTERN_ID = String.valueOf(mCustomizePatternList.get(position).getId());
                AppConstants.PATTERN_NAME = mCustomizePatternList.get(position).getPatternInEnglish();

                AppConstants.SEASONAL_NAME = AppConstants.SEASONAL_NAME.equalsIgnoreCase("") ? "None" : AppConstants.SEASONAL_NAME;
                AppConstants.PLACE_OF_INDUSTRY_NAME = AppConstants.PLACE_OF_INDUSTRY_NAME.equalsIgnoreCase("") ? "None" : AppConstants.PLACE_OF_INDUSTRY_NAME;
                AppConstants.BRANDS_NAME = AppConstants.BRANDS_NAME.equalsIgnoreCase("") ? "None" : AppConstants.BRANDS_NAME;
                AppConstants.MATERIAL_TYPE_NAME = AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("") ? "None" : AppConstants.MATERIAL_TYPE_NAME;
                AppConstants.COLOUR_NAME = AppConstants.COLOUR_NAME.equalsIgnoreCase("") ? "None" : AppConstants.COLOUR_NAME;

                ((BaseActivity)mContext).nextScreen(ViewDetailsScreen.class,true);

                notifyDataSetChanged();
            }
        });

        holder.mGridCustimizeImg.setVisibility(PatternEntity.getChecked() ? View.VISIBLE : View.GONE);

    }

    @Override
    public int getItemCount() {
        return mCustomizePatternList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
