package com.qoltech.mzyoontailor.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.NewCustomerDetails;
import com.qoltech.mzyoontailor.entity.GetAreaEntity;
import com.qoltech.mzyoontailor.fragments.ShopDetailsFrag;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.List;

public class AreaAdapter extends RecyclerView.Adapter<AreaAdapter.ViewHolder> {

    Context context;

    private List<GetAreaEntity> countries;
    GetAreaEntity COUNTRY_ID;
    private PositionUpdateListener listener;
    Dialog dialog;
    NewCustomerDetails newAccountActivity;
    String[] parts;

    public AreaAdapter(Context context, List<GetAreaEntity> countries, GetAreaEntity id, Dialog dialog) {
        this.countries = countries;
        this.context = context;
        this.COUNTRY_ID = id;
        this.dialog = dialog;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final GetAreaEntity item = countries.get(position);

        holder.getMainView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetAreaEntity state = (GetAreaEntity) v.getTag();
                if (listener != null) {
//                    newAccountActivity.onUserTappedState(countries.get(position));
                }

                if (AppConstants.COUNRTY_CODE_ADAPTER == "SHOP_DETAILS") {
                    ShopDetailsFrag.state_text.setText(state.getArea());
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();

                    }
                } else if (AppConstants.COUNRTY_CODE_ADAPTER == "NEW_CUSTOMER") {
                    AppConstants.AREA_ID=String.valueOf(state.getId());
                    NewCustomerDetails.add_address_area_edt_txt.setText(state.getArea());
                    if (v != null && countries != null && countries.size() > position && countries.get(position) != null) {
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        dialog.dismiss();

                    }
                }
            }


        });

        holder.view.setTag(item);
        holder.countryName.setText(item.getArea());
        holder.countryName.setSelected(true);
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    public void add(GetAreaEntity item, int position) {
        countries.add(position, item);
        notifyItemInserted(position);
    }


    public void add(GetAreaEntity item) {
        countries.add(item);
        notifyItemInserted(countries.size());
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeLayout_main;
        ImageView flagsImg;
        TextView countryName;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (RelativeLayout) itemView;
            flagsImg = itemView.findViewById(R.id.flagsImg);
            countryName = itemView.findViewById(R.id.countryName);
            flagsImg.setVisibility(View.INVISIBLE);

        }

        public RelativeLayout getMainView() {
            return relativeLayout_main;
        }
    }
}