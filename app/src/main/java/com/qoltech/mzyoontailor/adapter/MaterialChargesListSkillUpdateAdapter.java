package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetMaterialForUpdatingSkillUpdateChargesEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class MaterialChargesListSkillUpdateAdapter extends RecyclerView.Adapter<MaterialChargesListSkillUpdateAdapter.ViewHolder> {

    List<GetMaterialForUpdatingSkillUpdateChargesEntity> items;
    private Context context;
    private PositionUpdateListener listener;

    private ArrayList<String> mDressIdList;
    ApiService restService;
    SharedPreferences sharedPreferences;
    private UserDetailsEntity mUserDetailsEntityRes;
    public static HashMap<String, String> qtyHashMap = new HashMap<>();

    int i = 0;

    public MaterialChargesListSkillUpdateAdapter(Context context, List<GetMaterialForUpdatingSkillUpdateChargesEntity> items, ApiService restService) {
        this.items = items;
        this.context = context;
        this.restService = restService;
        sharedPreferences = context.getSharedPreferences("TailorId", Context.MODE_PRIVATE);

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_material_charges_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final GetMaterialForUpdatingSkillUpdateChargesEntity item = items.get(position);
        holder.view.setTag(item);


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            holder.text_stitching_label.setText(item.getPatternInArabic());
            holder.amount.setText(String.valueOf(item.getMaterialCharges()));
        } else {
            holder.text_stitching_label.setText(item.getPatternInEnglish());
            holder.amount.setText(String.valueOf(item.getMaterialCharges()));


        }
        for (int i = 0; i <= items.size(); i++) {
//        qtyHashMap.put(String.valueOf(item.getId()),"")

//        qtyHashMap.put(String.valueOf(item.getId()), String.valueOf(item.getMaterialCharges()));
//        AppConstants.MATERIAL_MAP = qtyHashMap;

            AppConstants.MATERIAL_MAP.put(String.valueOf(items.get(position).getId()), String.valueOf(item.getMaterialCharges()));

        AppConstants.MATERIAL_MAP_NEW.put(items.get(position).getId(),String.valueOf(item.getMaterialCharges()));
        }


        Set keys = AppConstants.MATERIAL_MAP_NEW.keySet();
        Iterator itr = keys.iterator();

        int key;
        String value;
        while (itr.hasNext()) {

            key = (int) itr.next();
            value = (String) AppConstants.MATERIAL_MAP_NEW.get(key);
            System.out.println(key + " - " + value);

//            materialList.add(String.valueOf(value));
//            materialListId.add(String.valueOf(key));
////            Collections.reverse(materialList);
        }



//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                AppConstants.PATTERN_ID = String.valueOf(item.getId());
//
//
//
//            }
//        });

        holder.amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

//                qtyHashMap.replace(String.valueOf(holder.getAdapterPosition()), s.toString());
//                AppConstants.MATERIAL_MAP = qtyHashMap;


//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                AppConstants.MATERIAL_MAP_NEW.put(items.get(position).getId(),s.toString());

                    AppConstants.MATERIAL_MAP.put(String.valueOf(String.valueOf(items.get(position).getId())), s.toString());
//                }
//                    AppConstants.MATERIAL_MAP.replace(String.valueOf(String.valueOf(items.get(position).getId())), s.toString(),"100");


//                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//                    AppConstants.MATERIAL_MAP.put(String.valueOf(String.valueOf(items.get(position).getId())), s.toString());
//
//                }
//                Object value = AppConstants.MATERIAL_MAP.get(String.valueOf(String.valueOf(items.get(position).getId())));
//                if (value != null && value.equals(String.valueOf(item.getMaterialCharges()))) {
//                    AppConstants.MATERIAL_MAP.put(String.valueOf(String.valueOf(items.get(position).getId())), s.toString());
//                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetMaterialForUpdatingSkillUpdateChargesEntity item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetMaterialForUpdatingSkillUpdateChargesEntity item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        ImageView gender_image;
        TextView text_stitching_label;
        View view;
        EditText amount;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout = (LinearLayout) itemView;
            text_stitching_label = itemView.findViewById(R.id.text_stitching_label);
            amount = itemView.findViewById(R.id.amount);
        }

        public LinearLayout getMainView() {
            return linearLayout;
        }
    }


}