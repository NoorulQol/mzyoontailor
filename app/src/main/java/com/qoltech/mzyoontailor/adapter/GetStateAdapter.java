package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetStateEntity;
import com.qoltech.mzyoontailor.ui.AddAddressScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GetStateAdapter extends  RecyclerView.Adapter<GetStateAdapter.Holder> {

    private Context mContext;
    private ArrayList<GetStateEntity> mStateList;
    private Dialog mDialog;

    public GetStateAdapter(Context activity, ArrayList<GetStateEntity> getStateList, Dialog dialog) {
        mContext = activity;
        mStateList = getStateList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public GetStateAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new GetStateAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final GetStateAdapter.Holder holder, final int position) {
        final GetStateEntity getStateEntity = mStateList.get(position);

        holder.mGetCountryTxtViewTxt.setText(getStateEntity.getStateName());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.COUNTRY_STATE_ID = String.valueOf(mStateList.get(position).getId());
                ((AddAddressScreen) mContext).getAreaApiCall(String.valueOf(mStateList.get(position).getId()));
                ((AddAddressScreen) mContext).mAddAddressStateEdtTxt.setText(mStateList.get(position).getStateName());
                ((AddAddressScreen) mContext).mAddressAreaEdtTxt.setText("");
                mDialog.dismiss();

            }
        });

        holder.mCountryFlagImg.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return mStateList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}


