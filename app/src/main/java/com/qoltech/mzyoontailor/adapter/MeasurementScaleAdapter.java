package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.rd.utils.DensityUtils.dpToPx;

public class MeasurementScaleAdapter extends RecyclerView.Adapter<MeasurementScaleAdapter.Holder> {

    private Context mContext;
    private ArrayList<String> mMeasurmentList;

    public MeasurementScaleAdapter(Context activity, ArrayList<String> measurementList) {
        mContext = activity;
        mMeasurmentList = measurementList;
    }

    @NonNull
    @Override
    public MeasurementScaleAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_text_list, parent, false);
        return new MeasurementScaleAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MeasurementScaleAdapter.Holder holder, int position) {
        if (position%5 == 0) {
            holder.mMeasurementTxt.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)holder.mMeasurementView.getLayoutParams();
            layoutParams.width = dpToPx(50);
            holder.mMeasurementView.setLayoutParams(layoutParams);
        }
        else {
            holder.mMeasurementTxt.setVisibility(View.INVISIBLE);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)holder.mMeasurementView.getLayoutParams();
            layoutParams.width = dpToPx(25);
            holder.mMeasurementView.setLayoutParams(layoutParams);
        }
        holder.mMeasurementTxt.setText(mMeasurmentList.get(position));

    }


    @Override
    public int getItemCount() {
        return mMeasurmentList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.measurement_txt)
        TextView mMeasurementTxt;

        @BindView(R.id.measurement_view)
        View mMeasurementView;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

