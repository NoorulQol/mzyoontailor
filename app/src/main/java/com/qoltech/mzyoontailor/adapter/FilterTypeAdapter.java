package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.FilterTypeEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterTypeAdapter extends  RecyclerView.Adapter<FilterTypeAdapter.Holder> {

    private Context mContext;
    private ArrayList<FilterTypeEntity> mFilterList;

    public FilterTypeAdapter(Context activity, ArrayList<FilterTypeEntity> filterList) {
        mContext = activity;
        mFilterList = filterList;
    }

    @NonNull
    @Override
    public FilterTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filter_type_recycler_view, parent, false);
        return new FilterTypeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterTypeAdapter.Holder holder, final int position) {
//        final GenderEntity genderEntity = mGenderList.get(position);

        holder.mFilterViewTxt.setText(mFilterList.get(position).getfilterTxt());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFilterList.get(position).getfilterTxt().equalsIgnoreCase("Name - Z to A")){
                    AppConstants.FILTER_FLAG = "ZTOA";
                }
                if (mFilterList.get(position).getfilterTxt().equalsIgnoreCase("Name - A to Z")){
                    AppConstants.FILTER_FLAG = "ATOZ";

                }
                if (AppConstants.FILTER_TYPE.equalsIgnoreCase("GENDER")){
                    for (int i = 0; i < mFilterList.size(); i++) {
                        mFilterList.get(i).setChecked(false);
                    }
                    mFilterList.get(position).setChecked(true);
                    AppConstants.OLD_FILTER_GENDER = AppConstants.OLD_FILTER_GENDER.equalsIgnoreCase("") ? mFilterList.get(position).getfilterTxt() : AppConstants.OLD_FILTER_GENDER;
                    AppConstants.NEW_FILTER_GENDER = !AppConstants.OLD_FILTER_GENDER.equalsIgnoreCase("") ? mFilterList.get(position).getfilterTxt() : "";

                }else if ( AppConstants.FILTER_TYPE.equalsIgnoreCase("SORT") ){
                    for (int i = 0; i < mFilterList.size(); i++) {
                        mFilterList.get(i).setChecked(false);
                    }
                    mFilterList.get(position).setChecked(true);
                }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION")){
                    if (mFilterList.get(position).getChecked()){
                        mFilterList.get(position).setChecked(false);

                    }else {
                        mFilterList.get(position).setChecked(true);
                    }
                }else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION")){
                    if (mFilterList.get(position).getChecked()){
                        mFilterList.get(position).setChecked(false);

                    }else {
                        mFilterList.get(position).setChecked(true);
                    }
                }
                else {
                    if (mFilterList.get(position).getChecked()){
                        mFilterList.get(position).setChecked(false);

                    }else {
                        mFilterList.get(position).setChecked(true);

                    }
                }
                notifyDataSetChanged();
            }
        });

        holder.mFilterViewImgLay.setImageResource(mFilterList.get(position).getChecked() ? R.drawable.checked_icon :R.drawable.unchecked_icon);

        if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION") && AppConstants.OCCASTION_LIST_BOOL.equalsIgnoreCase("OLD")) {
            AppConstants.OLD_FILTER_OCCASION_LIST = mFilterList;
        }
        else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("OCCASION") && AppConstants.OCCASTION_LIST_BOOL.equalsIgnoreCase("NEW")) {
            AppConstants.NEW_FILTER_OCCASION_LIST = mFilterList;
        }

        if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION") && AppConstants.REGION_LIST_BOOL.equalsIgnoreCase("OLD")) {
            AppConstants.OLD_FILTER_REGION_LIST = mFilterList;
        }
        else if (AppConstants.FILTER_TYPE.equalsIgnoreCase("REGION") && AppConstants.REGION_LIST_BOOL.equalsIgnoreCase("NEW")) {
            AppConstants.NEW_FILTER_REGION_LIST = mFilterList;
        }
    }

    @Override
    public int getItemCount() {
        return mFilterList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.filter_type_recycler_view_img)
        ImageView mFilterViewImgLay;

        @BindView(R.id.filter_type_recycler_view_txt)
        TextView mFilterViewTxt;



        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

