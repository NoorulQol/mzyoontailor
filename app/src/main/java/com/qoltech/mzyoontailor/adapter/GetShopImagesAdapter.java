package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.fragments.ShopDetailsFrag;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.wrappers.GetShopImagesModal;

import java.util.List;

public class GetShopImagesAdapter extends RecyclerView.Adapter<GetShopImagesAdapter.ViewHolder> {

    List<GetShopImagesModal> items;
    private Context context;
    private String DRESS_ID = "";
    private PositionUpdateListener listener;
    ApiService restService;
    ShopDetailsFrag mshopDetailsFrag;
    boolean isSelectedAll;

    public GetShopImagesAdapter(Context context, List<GetShopImagesModal> items, ShopDetailsFrag shopDetailsFrag) {
        this.items = items;
        this.context = context;
        mshopDetailsFrag = shopDetailsFrag;
    }

    @Override
    public GetShopImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_only_img_grid, parent, false);
        GetShopImagesAdapter.ViewHolder viewHolder = new GetShopImagesAdapter.ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GetShopImagesAdapter.ViewHolder holder, final int position) {
        GetShopImagesModal item = items.get(position);
        holder.view.setTag(item);
//        holder.grid_view_txt.setText(item.getImage());
//        if (item.getShopProfileId() == 0) {
//            Glide.with(context).load(item.getImage())
//                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
//                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                            .placeholder(R.drawable.placeholder)).into(holder.grid_view_only_img);
//
//        }
//        else {
        if (String.valueOf(items.get(position).getId()).equalsIgnoreCase("0")) {
            Glide.with(context).load(item.getImage())
                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(holder.grid_view_only_img);
        } else {
            Glide.with(context).load(GlobalData.SERVER_URL + "images/ShopImages/" + item.getImage())
                    .thumbnail(Glide.with(context).load(R.drawable.gif_loader))
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(holder.grid_view_only_img);
        }

//        }

//        holder.getMainView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                GetBuyerRequestCustomization gender = (GetBuyerRequestCustomization) v.getTag();
//                if (listener != null) {
//                    listener.onPositionUpdate(items.indexOf(gender));
//                }
////                context.startActivity(new Intent(context, DressTypeActivity.class));
//            }
//        });

        if (!isSelectedAll) {
            holder.grid_view_only_img_close_img.setEnabled(false);

            holder.grid_view_only_img_close_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogManager.getInstance().
                            showOptionPopup(context, context.getResources().getString(R.string.are_you_sure_want_delete),
                                    context.getResources().getString(R.string.yes),
                                    context.getResources().getString(R.string.no),
                                    "english", new InterfaceTwoBtnCallBack() {
                                        @Override
                                        public void onNegativeClick() {

                                        }

                                        @Override
                                        public void onPositiveClick() {
                                            if (items.get(position).getId() != 0) {
                                                deletImageProfile(String.valueOf(items.get(position).getId()));

                                            }
                                            items.remove(items.get(position));
                                            notifyDataSetChanged();
                                        }
                                    });

                }
            });
        } else {
            holder.grid_view_only_img_close_img.setEnabled(true);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
    @Override
    public boolean onLongClick(View v) {


        DialogManager.getInstance().
                showOptionPopup(context, context.getResources().getString(R.string.are_you_sure_want_delete),
                        context.getResources().getString(R.string.yes),
                        context.getResources().getString(R.string.no),
                        "english", new InterfaceTwoBtnCallBack() {
                            @Override
                            public void onNegativeClick() {

                            }

                            @Override
                            public void onPositiveClick() {
                                if (items.get(position).getId() != 0) {
                                    deletImageProfile(String.valueOf(items.get(position).getId()));
                                }
                                items.remove(items.get(position));
                                notifyDataSetChanged();
                            }
                        });

        return false;
    }
});

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(GetShopImagesModal item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(GetShopImagesModal item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout_main;
        ImageView grid_view_only_img, grid_view_only_img_close_img;
        TextView grid_view_txt;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            linearLayout_main = (LinearLayout) itemView;
            grid_view_only_img = itemView.findViewById(R.id.grid_view_only_img);
            grid_view_only_img_close_img = itemView.findViewById(R.id.grid_view_only_img_close_img);
//            grid_view_txt = itemView.findViewById(R.id.grid_custimize_view_txt);
//            grid_view_only_img_close_img.setVisibility(View.GONE);
        }

        public LinearLayout getMainView() {
            return linearLayout_main;
        }
    }

    public void deletImageProfile(String Id) {
        APIRequestHandler.getInstance().deletShopImgApi(Id, mshopDetailsFrag);

    }

    public void selectAll() {
        isSelectedAll = true;
        notifyDataSetChanged();

    }

    public void unselectall() {
        isSelectedAll = false;
        notifyDataSetChanged();
    }
}
