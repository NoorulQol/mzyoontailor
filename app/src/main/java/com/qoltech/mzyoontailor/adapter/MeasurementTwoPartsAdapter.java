package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.GetMeasurementPartEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.MeasurementEditTextScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeasurementTwoPartsAdapter extends RecyclerView.Adapter<MeasurementTwoPartsAdapter.Holder> {

    private Context mContext;
    ArrayList<GetMeasurementPartEntity> mMeasuremtnPartsList;
    private UserDetailsEntity mUserDetailsEntityRes;
    ArrayList<String> Values = new ArrayList<>();
    ArrayList<String> Inches = new ArrayList<>();
    HashMap<String,String> parts = new HashMap<>();

    public MeasurementTwoPartsAdapter(Context activity, ArrayList<GetMeasurementPartEntity> measurementPartsList) {
        mContext = activity;
        mMeasuremtnPartsList = measurementPartsList;
    }

    @NonNull
    @Override
    public MeasurementTwoPartsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_measurement_two_part_list, parent, false);
        return new MeasurementTwoPartsAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MeasurementTwoPartsAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        for (int i=0; i<mMeasuremtnPartsList.size(); i++){
            parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(""));
        }


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mMeasurementPartsTxt.setText(mMeasuremtnPartsList.get(position).getTextInArabic());

        }else {
            holder.mMeasurementPartsTxt.setText(mMeasuremtnPartsList.get(position).getTextInEnglish());

        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"images/Measurement2/"+mMeasuremtnPartsList.get(position).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                    .into(holder.mMeasurementPartsImg);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        for (int i=0; i<mMeasuremtnPartsList.size(); i++){

            if (mMeasuremtnPartsList.get(i).getId() == 1 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HEAD);
                Inches.add(i,AppConstants.HEAD_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 2 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.NECK);
                Inches.add(i,AppConstants.NECK_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 3 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.CHEST);
                Inches.add(i,AppConstants.CHEST_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 4 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.WAIST);
                Inches.add(i,AppConstants.WAIST_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 5 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.THIGH);
                Inches.add(i,AppConstants.THIGH_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 7 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.ANKLE);
                Inches.add(i,AppConstants.ANKLE_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));
                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 6 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.KNEE);
                Inches.add(i,AppConstants.KNEE_INCHES);
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));
                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 8 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.OVER_ALL_HEIGHT);
                Inches.add(i,AppConstants.OVER_ALL_HEIGHT_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 9 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.SHORTS);
                Inches.add(i,AppConstants.SHORTS_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 10 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.OUTSEAM);
                Inches.add(i,AppConstants.OUTSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 11 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.INSEAM);
                Inches.add(i,AppConstants.INSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 12 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.SHOULDER);
                Inches.add(i,AppConstants.SHOULDER_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 13 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HALF_SLEEVE);
                Inches.add(i,AppConstants.HALF_SLEEVE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 14 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.BICEP);
                Inches.add(i,AppConstants.BICEP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 15 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HIP);
                Inches.add(i,AppConstants.HIP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 16 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.BOTTOM);
                Inches.add(i,AppConstants.BOTTOM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 17 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.HEIGHT);
                Inches.add(i,AppConstants.HEIGHT_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 18 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.SLEEVE);
                Inches.add(i,AppConstants.SLEEVE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 19 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Male")){
                Values.add(i,AppConstants.WRIST);
                Inches.add(i,AppConstants.WRIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 21 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_OVER_BUST);
                Inches.add(i,AppConstants.WOMEN_OVER_BUST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 22 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_UNDER_BUST);
                Inches.add(i,AppConstants.WOMEN_UNDER_BUST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 23 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HIP_BONE);
                Inches.add(i,AppConstants.WOMEN_HIP_BONE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 24 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_THIGH);
                Inches.add(i,AppConstants.WOMEN_THIGH_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 25 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_KNEE);
                Inches.add(i,AppConstants.WOMEN_KNEE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 26 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_CALF);
                Inches.add(i,AppConstants.WOMEN_CALF_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 27 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_ANKLE);
                Inches.add(i,AppConstants.WOMEN_ANKLE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 20 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HEAD);
                Inches.add(i,AppConstants.WOMEN_HEAD_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 28 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_NECK);
                Inches.add(i,AppConstants.WOMEN_NECK_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 29 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_BUST);
                Inches.add(i,AppConstants.WOMEN_BUST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 30 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_WAIST);
                Inches.add(i,AppConstants.WOMEN_WAIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 31 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_FULL_HIP);
                Inches.add(i,AppConstants.WOMEN_FULL_HIP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 32 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HEIGHT);
                Inches.add(i,AppConstants.WOMEN_HEIGHT_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 33 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_STW);
                Inches.add(i,AppConstants.WOMEN_STW_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 34 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_NLTC);
                Inches.add(i,AppConstants.WOMEN_NLTC_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 35 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMENT_NLTB);
                Inches.add(i,AppConstants.WOMENT_NLTB_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 36 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_STHB);
                Inches.add(i,AppConstants.WOMEN_STHB_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 37 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_WTHB);
                Inches.add(i,AppConstants.WOMEN_WTHB_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 38 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_HTH);
                Inches.add(i,AppConstants.WOMEN_HTH_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 39 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_INSEAM);
                Inches.add(i,AppConstants.WOMEN_INSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 40 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_OUTSEAM);
                Inches.add(i,AppConstants.WOMEN_OUTSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 41 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_SHOULDER);
                Inches.add(i,AppConstants.WOMEN_SHOULDER_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 42 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_BICEP);
                Inches.add(i,AppConstants.WOMEN_BICEP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 43 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_WRIST);
                Inches.add(i,AppConstants.WOMEN_WRIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 44 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Female")){
                Values.add(i,AppConstants.WOMEN_SLEEVE);
                Inches.add(i,AppConstants.WOMEN_SLEEVE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 45 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HEAD);
                Inches.add(i,AppConstants.BOY_HEAD_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 46 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_NECK);
                Inches.add(i,AppConstants.BOY_NECK_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 47 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_CHEST);
                Inches.add(i,AppConstants.BOY_CHEST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 48 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_WAIST);
                Inches.add(i,AppConstants.BOY_WAIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 49 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_THIGH);
                Inches.add(i,AppConstants.BOY_THIGH_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 50 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_KNEE);
                Inches.add(i,AppConstants.BOY_KNEE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 51 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_ANKLE);
                Inches.add(i,AppConstants.BOY_ANKLE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 52 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_OVER_ALL_HEIGHT);
                Inches.add(i,AppConstants.BOY_OVER_ALL_HEIGHT_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 53 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_SHORTS);
                Inches.add(i,AppConstants.BOY_SHORTS_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 54 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_OUTSEAM);
                Inches.add(i,AppConstants.BOY_OUTSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 55 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_INSEAM);
                Inches.add(i,AppConstants.BOY_INSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 56 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_SHOULDER);
                Inches.add(i,AppConstants.BOY_SHOULDER_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 57 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HALFSLEEVE);
                Inches.add(i,AppConstants.BOY_HALFSLEEVE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 58 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_BICEP);
                Inches.add(i,AppConstants.BOY_BICEP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 59 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HIP);
                Inches.add(i,AppConstants.BOY_HIP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 60 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_BOTTOM);
                Inches.add(i,AppConstants.BOY_BOTTOM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 61 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_HEIGHT);
                Inches.add(i,AppConstants.BOY_HEIGHT_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 62 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_SLEEVE);
                Inches.add(i,AppConstants.BOY_SLEEVE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 63 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Boy")){
                Values.add(i,AppConstants.BOY_WRIST);
                Inches.add(i,AppConstants.BOY_WRIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 65 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_OVER_BURST);
                Inches.add(i,AppConstants.GIRL_OVER_BURST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 66 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_UNDER_BURST);
                Inches.add(i,AppConstants.GIRL_UNDER_BURST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 67 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HIP_BONE);
                Inches.add(i,AppConstants.GIRL_HIP_BONE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 68 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_THIGH);
                Inches.add(i,AppConstants.GIRL_THIGH_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 69 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_KNEE);
                Inches.add(i,AppConstants.GIRL_KNEE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 70 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_CALF);
                Inches.add(i,AppConstants.GIRL_CALF_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 71 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_ANKLE);
                Inches.add(i,AppConstants.GIRL_ANKLE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 64 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HEAD);
                Inches.add(i,AppConstants.GIRL_HEAD_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 72 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_NECK);
                Inches.add(i,AppConstants.GIRL_NECK_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 73 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_BUST);
                Inches.add(i,AppConstants.GIRL_BUST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 74 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_WAIST);
                Inches.add(i,AppConstants.GIRL_WAIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 75 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_FULL_HIP);
                Inches.add(i,AppConstants.GIRL_FULL_HIP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 76 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HEIGHT);
                Inches.add(i,AppConstants.GIRL_HEIGHT_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 77 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_STW);
                Inches.add(i,AppConstants.GIRL_STW_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 78 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_NLTC);
                Inches.add(i,AppConstants.GIRL_NLTC_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 79 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_NLTB);
                Inches.add(i,AppConstants.GIRL_NLTB_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 80 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_STHB);
                Inches.add(i,AppConstants.GIRL_STHB_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 81 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_WTHB);
                Inches.add(i,AppConstants.GIRL_WTHB_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 82 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_HTH);
                Inches.add(i,AppConstants.GIRL_HTH_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 83 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_INSEAM);
                Inches.add(i,AppConstants.GIRL_INSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 84 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_OUTSEAM);
                Inches.add(i,AppConstants.GIRL_OUTSEAM_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 85 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_SHOULDER);
                Inches.add(i,AppConstants.GIRL_SHOULDER_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 86 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_BICEP);
                Inches.add(i,AppConstants.GIRL_BICEP_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 87 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_WRIST);
                Inches.add(i,AppConstants.GIRL_WRIST_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
            else if (mMeasuremtnPartsList.get(i).getId() == 88 &&AppConstants.GENDER_NAME.equalsIgnoreCase("Girl")){
                Values.add(i,AppConstants.GIRL_SLEEVE);
                Inches.add(i,AppConstants.GIRL_SLEEVE_INCHES);

                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    holder.mMeasuremntPartsCmTxt.setText(Values.get(i));

                }else if (AppConstants.UNITS.equalsIgnoreCase("IN")){
                    holder.mMeasuremntPartsCmTxt.setText(Inches.get(i));

                }
                parts.put(String.valueOf(mMeasuremtnPartsList.get(i).getId()),String.valueOf(Values.get(i)));
                AppConstants.MEASUREMENT_MAP = parts;
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.SelectedPartsId = String.valueOf(mMeasuremtnPartsList.get(position).getId());
                AppConstants.SelectedPartsImage = mMeasuremtnPartsList.get(position).getImage();
                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                    AppConstants.SelectedPartsName = mMeasuremtnPartsList.get(position).getTextInArabic();

                }else {
                    AppConstants.SelectedPartsName = mMeasuremtnPartsList.get(position).getTextInEnglish();

                }
                if (AppConstants.UNITS.equalsIgnoreCase("CM")){
                    AppConstants.SelectedPartsEditValue = Values.get(position);

                }else {
                    AppConstants.SelectedPartsEditValue = Inches.get(position);

                }
                ((BaseActivity)mContext).nextScreen(MeasurementEditTextScreen.class,true);
            }
        });
        if (AppConstants.UNITS.equalsIgnoreCase("CM")){
            holder.mMeasuremntPartsCmTxt.setText(Values.get(position));

        }else {
            holder.mMeasuremntPartsCmTxt.setText(Inches.get(position));

        }



    }

    @Override
    public int getItemCount() {
        return mMeasuremtnPartsList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.adapter_parts_img)
        ImageView mMeasurementPartsImg;

        @BindView(R.id.adapter_parts_txt)
        TextView mMeasurementPartsTxt;

        @BindView(R.id.adpater_parts_cm_txt)
        TextView mMeasuremntPartsCmTxt;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

