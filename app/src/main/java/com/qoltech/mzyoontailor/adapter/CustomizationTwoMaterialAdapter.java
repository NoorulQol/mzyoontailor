package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationMaterialsEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.CustomizationTwoScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoMaterialAdapter   extends RecyclerView.Adapter<CustomizationTwoMaterialAdapter.Holder> {

        private Context mContext;
        private ArrayList<CustomizationMaterialsEntity> mCustomizeMaterialList;

        private UserDetailsEntity mUserDetailsEntityRes;
        public CustomizationTwoMaterialAdapter(Context activity, ArrayList<CustomizationMaterialsEntity> customizeMaterialList) {
                mContext = activity;
                mCustomizeMaterialList = customizeMaterialList;
        }

        @NonNull
        @Override
        public CustomizationTwoMaterialAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
                return new CustomizationTwoMaterialAdapter.Holder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final CustomizationTwoMaterialAdapter.Holder holder, final int position) {
                mUserDetailsEntityRes = new UserDetailsEntity();

                Gson gson = new Gson();
                String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
                mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

                final CustomizationMaterialsEntity MaterialEntity = mCustomizeMaterialList.get(position);

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
                        holder.mGridViewTxt.setText(MaterialEntity.getMaterialInArabic());

                }else {
                        holder.mGridViewTxt.setText(MaterialEntity.getMaterialInEnglish());

                }

                try {
                        Glide.with(mContext)
                                .load(AppConstants.IMAGE_BASE_URL+"Images/Material/"+MaterialEntity.getImage())
                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
                                .into(holder.mGridViewImgLay);

                } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                if (mCustomizeMaterialList.get(position).getMaterialInEnglish().equalsIgnoreCase("all material type")){
                                        if (mCustomizeMaterialList.get(position).getChecked()){
                                                for (int i=0; i<mCustomizeMaterialList.size(); i++) {
                                                        mCustomizeMaterialList.get(i).setChecked(false);
                                                }
                                                AppConstants.MATERIAL_ID = "";
                                                AppConstants.MATERIAL_TYPE_NAME = "";
                                        }else {
                                                AppConstants.MATERIAL_ID = "";
                                                AppConstants.MATERIAL_TYPE_NAME = "";
                                                for (int i=0; i<mCustomizeMaterialList.size(); i++) {
                                                        mCustomizeMaterialList.get(i).setChecked(true);
                                                        AppConstants.MATERIAL_ID = String.valueOf(mCustomizeMaterialList.get(position).getId()) ;
                                                        AppConstants.MATERIAL_TYPE_NAME = String.valueOf(mCustomizeMaterialList.get(position).getMaterialInEnglish()) ;
                                                }
                                        }
                                        ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();
                                        notifyDataSetChanged();

                                }else {
                                        if (mCustomizeMaterialList.get(position).getChecked()){
                                                mCustomizeMaterialList.get(position).setChecked(false);
                                                for (int i=0; i<mCustomizeMaterialList.size() ; i++){
                                                        if (mCustomizeMaterialList.get(i).getMaterialInEnglish().equalsIgnoreCase("all material type")){
                                                                mCustomizeMaterialList.get(i).setChecked(false);
                                                        }
                                                }
                                        }else {
                                                mCustomizeMaterialList.get(position).setChecked(true);
                                        }
                                        for (int i=0; i<mCustomizeMaterialList.size(); i++){
                                                if (mCustomizeMaterialList.get(i).getChecked()){
                                                        AppConstants.MATERIAL_ID = AppConstants.MATERIAL_ID.equalsIgnoreCase("") ? mCustomizeMaterialList.get(i).getId()+"" : AppConstants.MATERIAL_ID + ","+ mCustomizeMaterialList.get(i).getId();

                                                        AppConstants.MATERIAL_TYPE_NAME =  AppConstants.MATERIAL_TYPE_NAME.equalsIgnoreCase("") ? mCustomizeMaterialList.get(i).getMaterialInEnglish() : AppConstants.MATERIAL_TYPE_NAME + ","+ mCustomizeMaterialList.get(i).getMaterialInEnglish();
                                                }
                                        }
                                        ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();
                                        notifyDataSetChanged();

                                }


                        }
                });

                holder.mGridCustimizeImg.setVisibility(MaterialEntity.getChecked() ?  View.VISIBLE : View.GONE);


        }

        @Override
        public int getItemCount() {
                return mCustomizeMaterialList.size();
        }

        public class Holder extends RecyclerView.ViewHolder {

                @BindView(R.id.grid_custimize_view_lay)
                LinearLayout mGridViewLay;

                @BindView(R.id.grid_custimize_view_img_lay)
                ImageView mGridViewImgLay;

                @BindView(R.id.grid_custimize_view_txt)
                TextView mGridViewTxt;

                @BindView(R.id.grid_custimize_img)
                ImageView mGridCustimizeImg;


                public Holder(View itemView) {
                        super(itemView);
                        ButterKnife.bind(this, itemView);
                }
        }
}


