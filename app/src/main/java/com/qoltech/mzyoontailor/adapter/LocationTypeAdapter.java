package com.qoltech.mzyoontailor.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.ui.AddAddressScreen;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationTypeAdapter extends RecyclerView.Adapter<LocationTypeAdapter.Holder> {

    private Context mContext;
    private ArrayList<String> mLocationList;
    private Dialog mDialog;

    public LocationTypeAdapter(Context activity, ArrayList<String> locationList, Dialog dialog) {
        mContext = activity;
        mLocationList = locationList;
        mDialog = dialog;
    }

    @NonNull
    @Override
    public LocationTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_country_code_list, parent, false);
        return new LocationTypeAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LocationTypeAdapter.Holder holder, final int position) {

        holder.mGetCountryTxtViewTxt.setText(mLocationList.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((AddAddressScreen)mContext).mAddressLocationTypeEdtTxt.setText(mLocationList.get(position));
                mDialog.dismiss();
            }
        });

        holder.mCountryFlagImg.setVisibility(View.GONE);

    }

    @Override
    public int getItemCount() {
        return mLocationList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.get_country_recycler_view_txt)
        TextView mGetCountryTxtViewTxt;

        @BindView(R.id.get_country_flag_img)
        ImageView mCountryFlagImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

