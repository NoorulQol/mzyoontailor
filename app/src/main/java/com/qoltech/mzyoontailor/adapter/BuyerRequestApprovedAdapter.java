package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.PositionUpdateListener;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.BuyerRequestApprovedModal;

import java.util.List;


public class BuyerRequestApprovedAdapter extends RecyclerView.Adapter<BuyerRequestApprovedAdapter.ViewHolder> {

    List<BuyerRequestApprovedModal> items;
    private Context context;
    private PositionUpdateListener listener;
    private UserDetailsEntity mUserDetailsEntityRes;
    public BuyerRequestApprovedAdapter(Context context, List<BuyerRequestApprovedModal> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_buyer_pending_approved_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(context, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        final BuyerRequestApprovedModal item = items.get(position);
        holder.view.setTag(item);

        if(mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

            holder.request_id.setText(String.valueOf(item.getOrderId()));
            holder.buyer_name.setText(item.getName());
            holder.product_name.setText(item.getNameInArabic());
            holder.location_name.setText(String.valueOf(item.getArea()));
            holder.service_name.setText(item.getDeliveryTypeInArabic());
            holder.request_date.setText(item.getOrderDate());

        }else {
            holder.request_id.setText(String.valueOf(item.getOrderId()));
            holder.buyer_name.setText(item.getName());
            holder.product_name.setText(item.getNameInEnglish());
            holder.location_name.setText(String.valueOf(item.getArea()));
            holder.service_name.setText(item.getDeliveryTypeInEnglish());
            holder.request_date.setText(item.getOrderDate());

        }

//        holder.request_id.setText(String.valueOf(item.getOrderId()));
//        holder.buyer_name.setText(item.getName());
//        holder.product_name.setText(item.getNameInEnglish());
//        holder.location_name.setText(String.valueOf(item.getAreaId()));
//        holder.service_name.setText(item.getDeliveryTypeInEnglish());
//        holder.request_date.setText(item.getName());

//        holder.type_of_order.setText(item.getNameInEnglish());
//        holder.buyer_location.setText(String.valueOf(item.getAreaId()));
//        holder.service_type.setText(item.getDeliveryTypeInEnglish());

        Glide.with(context).load(GlobalData.SERVER_URL + "images/BuyerImages/" + item.getProfilePicture())
                .thumbnail(Glide.with(context).load(R.drawable.placeholder))
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)).into(holder.buyer_profile_image);
//        holder.getMainView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BuyerRequestPendingModal gender = (BuyerRequestPendingModal) v.getTag();
//                if (listener != null) {
//                    listener.onPositionUpdate(items.indexOf(gender));
//                }
//                AppConstants.CHECK_BOOK_APPOINTMENT = "RequestList";
//                AppConstants.ORDER_ID = String.valueOf(item.getOrderId());
//                AppConstants.REQUEST_LIST_ID = String.valueOf(item.getOrderId());
//                context.startActivity(new Intent(context, RequestViewActivity.class));
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(BuyerRequestApprovedModal item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void add(BuyerRequestApprovedModal item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void remove(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
//        LinearLayout relativeLayout_main;
//        ImageView gender_image;
//        TextView buyer_name, buyer_location, service_type, type_of_order;
//        View view;

        LinearLayout relativeLayout_main;
        ImageView buyer_profile_image;
        TextView request_id, buyer_name, product_name, location_name, service_name, request_date;
        View view;

        private ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            relativeLayout_main = (LinearLayout) itemView;
            buyer_profile_image = itemView.findViewById(R.id.buyer_profile_image);
            request_id = itemView.findViewById(R.id.request_id);
            buyer_name = itemView.findViewById(R.id.buyer_name);
            product_name = itemView.findViewById(R.id.product_name);
            location_name = itemView.findViewById(R.id.location_name);
            service_name = itemView.findViewById(R.id.service_name);
            request_date = itemView.findViewById(R.id.request_date);
        }

        public LinearLayout getMainView() {
            return relativeLayout_main;
        }
    }
}