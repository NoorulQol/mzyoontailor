package com.qoltech.mzyoontailor.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.CustomizationColorsEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.CustomizationTwoScreen;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomizationTwoColorAdapter extends RecyclerView.Adapter<CustomizationTwoColorAdapter.Holder> {

    private Context mContext;
    private ArrayList<CustomizationColorsEntity> mCustomizeColorList;
    int mTotalInt = 1;

    private UserDetailsEntity mUserDetailsEntityRes;
    public CustomizationTwoColorAdapter(Context activity, ArrayList<CustomizationColorsEntity> customizeColorList) {
        mContext = activity;
        mCustomizeColorList = customizeColorList;
    }

    @NonNull
    @Override
    public CustomizationTwoColorAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_grid_cutomize_corner, parent, false);
        return new CustomizationTwoColorAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomizationTwoColorAdapter.Holder holder, final int position) {
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(mContext, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        final CustomizationColorsEntity colorEntity = mCustomizeColorList.get(position);

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            holder.mGridViewTxt.setText(colorEntity.getColorInArabic());

        }else {
            holder.mGridViewTxt.setText(colorEntity.getColorInEnglish());

        }

        try {
            Glide.with(mContext)
                    .load(AppConstants.IMAGE_BASE_URL+"Images/Color/"+colorEntity.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder))
                    .into(holder.mGridViewImgLay);

        } catch (Exception ex) {
            Log.e(AppConstants.TAG, ex.getMessage());
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCustomizeColorList.get(position).getColorInEnglish().equalsIgnoreCase("All-Colors")){
                    if (mCustomizeColorList.get(position).getChecked()){
                        for (int i=0; i<mCustomizeColorList.size(); i++) {
                            mCustomizeColorList.get(i).setChecked(false);
                        }
                        AppConstants.COLOR_ID = "";
                        AppConstants.COLOUR_NAME = "";

                    }else {
                        AppConstants.COLOR_ID = "";
                        AppConstants.COLOUR_NAME = "";
                        for (int i=0; i<mCustomizeColorList.size(); i++) {
                            mCustomizeColorList.get(i).setChecked(true);
                            AppConstants.COLOR_ID =String.valueOf(mCustomizeColorList.get(position).getId());
                            AppConstants.COLOUR_NAME =String.valueOf(mCustomizeColorList.get(position).getColorInEnglish());

                        }
                    }
                    ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();


//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//
//                    }else {
//                        ((CustomizationTwoScreen)mContext).getCustomizationTwoApiCall();
//
//                    }

                    notifyDataSetChanged();

                }else {
                    if (mCustomizeColorList.get(position).getChecked()){
                        mCustomizeColorList.get(position).setChecked(false);
                        for (int i=0; i<mCustomizeColorList.size() ; i++)
                        {
                            if (mCustomizeColorList.get(i).getColorInEnglish().equalsIgnoreCase("All-Colors"))
                            {
                                mCustomizeColorList.get(i).setChecked(false);

                            }
                        }
                    }else {
                        mTotalInt = 1;
                        for (int i=0; i<mCustomizeColorList.size();i++){
                            if (mCustomizeColorList.get(i).getChecked()){
                                mTotalInt = mTotalInt + 1;
                            }
                            if (String.valueOf(mTotalInt).equalsIgnoreCase(String.valueOf(mCustomizeColorList.size()-1))){
                                for (int j=0; j<mCustomizeColorList.size(); j++){
                                    if (mCustomizeColorList.get(j).getColorInEnglish().equalsIgnoreCase("All-Colors")){
                                        mCustomizeColorList.get(j).setChecked(true);
                                    }
                                }

                            }
                        }
                        mCustomizeColorList.get(position).setChecked(true);
                    }
                    AppConstants.COLOR_ID = "";
                    for (int i=0; i<mCustomizeColorList.size(); i++){
                        if (mCustomizeColorList.get(i).getChecked()){
                            AppConstants.COLOR_ID = AppConstants.COLOR_ID.equalsIgnoreCase("") ? mCustomizeColorList.get(i).getId()+"" : AppConstants.COLOR_ID + ","+ mCustomizeColorList.get(i).getId();

                            AppConstants.COLOUR_NAME =  AppConstants.COLOUR_NAME.equalsIgnoreCase("") ? mCustomizeColorList.get(i).getColorInEnglish() : AppConstants.COLOUR_NAME + ","+ mCustomizeColorList.get(i).getColorInEnglish();
                        }
                    }
                    ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();

//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();
//
//                    }else {
//                        ((CustomizationTwoScreen)mContext).getCustomizationTwoApiCall();
//
//                    }
                    notifyDataSetChanged();
                }

//                if (mCustomizeColorList.get(position).getColorInEnglish().equalsIgnoreCase("All-Colors")){
//                        if (mCustomizeColorList.get(position).getChecked()){
//                            for (int i=0; i<mCustomizeColorList.size(); i++) {
//                                mCustomizeColorList.get(i).setChecked(false);
//                            }
//                            AppConstants.COLOR_ID = "";
//                            AppConstants.COLOUR_NAME = "";
//                        }else {
//                            AppConstants.COLOR_ID = "";
//                            AppConstants.COLOUR_NAME = "";
//                            for (int i=0; i<mCustomizeColorList.size(); i++) {
//                                mCustomizeColorList.get(i).setChecked(true);
//                                AppConstants.COLOR_ID = String.valueOf(mCustomizeColorList.get(position).getId()) ;
//                                AppConstants.COLOUR_NAME =String.valueOf(mCustomizeColorList.get(position).getColorInEnglish());
//
//                            }
//                        }
//
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();
//
//                    }else {
//                        ((CustomizationTwoScreen)mContext).getCustomizationTwoApiCall();
//
//                    }
//                    notifyDataSetChanged();
//                }else {
//                    if (mCustomizeColorList.get(position).getChecked()){
//                        mCustomizeColorList.get(position).setChecked(false);
//
//                        for (int i=0; i<mCustomizeColorList.size() ; i++){
//                            if (mCustomizeColorList.get(i).getColorInEnglish().equalsIgnoreCase("All-Colors")){
//                                mCustomizeColorList.get(i).setChecked(false);
//                            }
//                        }
//                    }else {
//                        mTotalInt = 1;
//                        for (int i=0; i<mCustomizeColorList.size();i++){
//                            if (mCustomizeColorList.get(i).getChecked()){
//                                mTotalInt = mTotalInt + 1;
//                            }
//                            if (String.valueOf(mTotalInt).equalsIgnoreCase(String.valueOf(mCustomizeColorList.size()-1))){
//                                for (int j=0; j<mCustomizeColorList.size(); j++){
//                                    if (mCustomizeColorList.get(j).getColorInEnglish().equalsIgnoreCase("All-Colors")){
//                                        mCustomizeColorList.get(j).setChecked(true);
//                                    }
//                                }
//
//                            }
//                        }
//
//                        mCustomizeColorList.get(position).setChecked(true);
//                    }
//                    for (int i=0; i<mCustomizeColorList.size(); i++){
//                        if (mCustomizeColorList.get(i).getChecked()){
//                            AppConstants.COLOR_ID = AppConstants.COLOR_ID.equalsIgnoreCase("") ? mCustomizeColorList.get(i).getId()+"" :  AppConstants.COLOR_ID + ","+ mCustomizeColorList.get(i).getId();
//
//                            AppConstants.COLOUR_NAME =  AppConstants.COLOUR_NAME.equalsIgnoreCase("") ? mCustomizeColorList.get(i).getColorInEnglish() : AppConstants.COLOUR_NAME + ","+ mCustomizeColorList.get(i).getColorInEnglish();
//                        }
//                    }
//                    if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")) {
//                        ((CustomizationTwoScreen)mContext).getNewFlowCustomizationTwoApiCall();
//
//                    }else {
//                        ((CustomizationTwoScreen)mContext).getCustomizationTwoApiCall();
//
//                    }
//
//                    notifyDataSetChanged();
//
//                }

            }

        });

        holder.mGridCustimizeImg.setVisibility(colorEntity.getChecked() ? View.VISIBLE : View.GONE);


    }

    @Override
    public int getItemCount() {
        return mCustomizeColorList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.grid_custimize_view_lay)
        LinearLayout mGridViewLay;

        @BindView(R.id.grid_custimize_view_img_lay)
        ImageView mGridViewImgLay;

        @BindView(R.id.grid_custimize_view_txt)
        TextView mGridViewTxt;

        @BindView(R.id.grid_custimize_img)
        ImageView mGridCustimizeImg;


        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

