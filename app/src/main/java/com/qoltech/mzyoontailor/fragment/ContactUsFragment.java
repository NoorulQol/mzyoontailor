package com.qoltech.mzyoontailor.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.main.BaseFragment;

import butterknife.ButterKnife;

public class ContactUsFragment extends BaseFragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.frag_contact_us_screen, container, false);

        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }


    private void initView() {
//        AppConstants.HOME_SCREEN = this;

//        setupUI(mParLay);


    }
}
