package com.qoltech.mzyoontailor.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.main.BaseFragment;
import com.qoltech.mzyoontailor.ui.GenderScreen;
import com.qoltech.mzyoontailor.ui.MeasurementScalingSampleScreen;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeScreenFragment extends BaseFragment {


    @BindView(R.id.home_screen_frag_par_lay)
    LinearLayout mHomeScreenParLay;

    @BindView(R.id.profile_image_circle_img)
    de.hdodenhof.circleimageview.CircleImageView mHomeCircleImg;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;


    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = layoutInflater.inflate(R.layout.frag_home_screen, container, false);

        ButterKnife.bind(this, rootView);

        initView();

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.new_order_inner_lay,R.id.book_an_appointment_inner_lay})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_order_inner_lay:
                nextScreenWithOutFinish(GenderScreen.class,false);

                break;
            case R.id.book_an_appointment_inner_lay:
                nextScreenWithOutFinish(MeasurementScalingSampleScreen.class,false);

                break;
        }

    }

    private void initView() {
//        AppConstants.HOME_SCREEN = this;

        setupUI(mHomeScreenParLay);

        mSetHeader();

    }

    private void mSetHeader(){
        mHomeCircleImg.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText("Home");
    }
}
