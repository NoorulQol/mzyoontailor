package com.qoltech.mzyoontailor.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AdditionalMaterialImageAdapter;
import com.qoltech.mzyoontailor.adapter.SkillUpdateGetMaterialImageAdapter;
import com.qoltech.mzyoontailor.adapter.SkillUpdateGetReferenceImageAdapter;
import com.qoltech.mzyoontailor.adapter.TrackingStatusCheckListAdapter;
import com.qoltech.mzyoontailor.entity.AdditionalMaterialImageEntity;
import com.qoltech.mzyoontailor.entity.Balances;
import com.qoltech.mzyoontailor.entity.BuyerAddress;
import com.qoltech.mzyoontailor.entity.GetAppoinmentLeftMaterialEntity;
import com.qoltech.mzyoontailor.entity.GetAppoinmentLeftMeasurementEntity;
import com.qoltech.mzyoontailor.entity.GetOrderDetailScheduleTypeEntity;
import com.qoltech.mzyoontailor.entity.MaterialChargesEntity;
import com.qoltech.mzyoontailor.entity.MaterialImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.entity.MeasurementChargesEntity;
import com.qoltech.mzyoontailor.entity.OrderDetail;
import com.qoltech.mzyoontailor.entity.ProductPrice;
import com.qoltech.mzyoontailor.entity.ReferenceImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.entity.ShippingCharge;
import com.qoltech.mzyoontailor.entity.Status;
import com.qoltech.mzyoontailor.entity.StitchingChargesEntity;
import com.qoltech.mzyoontailor.entity.UrgentStichingChargeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.BuyerOrderDetailResponse;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.ui.MaterialDetailsScreenOrderDetails;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsModal;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsResponse;
import com.qoltech.mzyoontailor.wrappers.GetTrackingByOrderModal;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsModal;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderQuotationActivity extends BaseActivity {


    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;
//    List<GetBuyerAddressModal> getBuyerAddressModals;
//    List<GetOrderDetailsModal> getOrderDetailsModals;
//    List<GetOrderProductPriceModal> getOrderProductPriceModals;
//    List<GetShippingCharges> getShippingChargesModals;
//    List<GetOrderStatusModal> getOrderStatusModals;


//    List<BuyerAddressBuyer> buyerAddressBuyers;
//    List<GetOrderDetailsModal> getOrderDetailsModals;
//    List<GetOrderProductPriceModal> getOrderProductPriceModals;
//    List<GetShippingCharges> getShippingChargesModals;
//    List<GetOrderStatusModal> getOrderStatusModals;


    List<BuyerAddress> buyerAddresses;
    List<OrderDetail> orderDetails;
    List<ProductPrice> productPrices;
    List<ShippingCharge> shippingCharges;
    List<Status> statuses;
    List<ReferenceImagesSkillUpdateGetEntity> referenceImagesSkillUpdateGetEntities;
    List<MaterialImagesSkillUpdateGetEntity> materialImagesSkillUpdateGetEntities;
    List<Balances> balances;

    List<GetAppoinmentLeftMaterialEntity> getAppoinmentLeftMaterialEntities;
    List<GetAppoinmentLeftMeasurementEntity> getAppoinmentLeftMeasurementEntities;
    List<GetOrderDetailScheduleTypeEntity> orderDetailScheduleTypeEntities;

    TextView order_id, product_name, price, sub_total, shipping_handling, tax, appointment_charges,
            order_type, name, address, number, order_date, material_details_text;

    public static TextView tracking_current_status;
    ImageView order_details_image;
    RelativeLayout tracking_status_click;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    TrackingStatusCheckListAdapter trackingStatusAdapter;
    List<GetTrackingDetailsResult> getTrackingDetailsResults;
    GetTrackingDetailsModal getTrackingDetailsModal;
    GetTrackingByOrderModal getTrackingByOrderModal;
    SharedPreferences sharedPreferences;
    //    List<GetTrackingDetailsModal> getTrackingDetailsModals;
    List<GetTrackingByOrderModal> getTrackingByOrderModals;
    AlertDialog dialog;
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    LinearLayout bgLayout, order_layout;
    TextView order_id_txt, qty;
    Button measurement, customization;
    String genderId;
    RecyclerView material_images, reference_images;
    SkillUpdateGetMaterialImageAdapter skillUpdateGetMaterialImageAdapter;
    AdditionalMaterialImageAdapter additionalMaterialImageAdapter;
    SkillUpdateGetReferenceImageAdapter skillUpdateGetReferenceImageAdapter;
    LinearLayout add_refernce_lable_layout, add_material_lable_layout, add_reference_img_layout, add_material_image_layout;
    List<GetRequestMeasurementPartsModal> getMeasurementDetailsModals;

    TextView stitching_charges, measurement_charges,
            urgent_charges, material_delivery_charges,
            delivery_charges, total, payment_status, balance, amount_paid;
    TextView material_type, dress_type, aprox_delivery_date, no_of_days, material_required;
    Float ftotal, bal, remaining;
    EditText pay_remaining;
    ImageView update_amount;
    LinearLayout paid_layout, balance_layout, pay_balance_layout,
            urgent_charges_layout,
            stitching_charges_layout, measurement_charges_layout,
            material_charges_layout,
            schedule_appoinment_label_layout,
            schedule_appointment_layout;


//        if (urgentStichingChargeEntities.get(0).getUrgentStichingCharges() == 0.0) {
//
//        urgent_charges_layout.setVisibility(View.GONE);
//
//    }
//
//                    if (stitchingChargesEntities.get(0).getStichingAndMaterialCharge() == 0.0) {
//        stitching_charges_layout.setVisibility(View.GONE);
//    }
//                    if (measurementChargEntities.get(0).getMeasurementCharges() == 0.0) {
//        measurement_charges_layout.setVisibility(View.GONE);
//    }
//                    if (materialChargesEntities.get(0).getMaterialCharges() == 0.0) {
//        material_charges_layout.setVisibility(View.GONE);
//    }


    TextView material_appointment_type,
            material_appointment_date, material_appointment_left,
            measurement_appointment_type, measurement_appointment_date,
            measurement_appointment_left, view_details_tracking, service_type, view_details,
            quotation_status, quotation_date;

    List<MaterialChargesEntity> materialChargesEntities;
    List<MeasurementChargesEntity> measurementChargEntities;
    List<StitchingChargesEntity> stitchingChargesEntities;
    List<UrgentStichingChargeEntity> urgentStichingChargeEntities;
    List<AdditionalMaterialImageEntity> additionalMaterialImageEntities;
    RelativeLayout view_details_layout;
    DialogManager dialogManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_quotation_activity);

        initView();
        restService = ((MainApplication) getApplication()).getClient();
        order_id = (TextView) findViewById(R.id.order_id);
        product_name = (TextView) findViewById(R.id.product_name);
        price = (TextView) findViewById(R.id.price);
        order_details_image = (ImageView) findViewById(R.id.order_details_image);
        material_appointment_type = (TextView) findViewById(R.id.material_appointment_type);
        material_appointment_date = (TextView) findViewById(R.id.material_appointment_date);
        material_appointment_left = (TextView) findViewById(R.id.material_appointment_left);
        measurement_appointment_type = (TextView) findViewById(R.id.measurement_appointment_type);
        measurement_appointment_date = (TextView) findViewById(R.id.measurement_appointment_date);
        measurement_appointment_left = (TextView) findViewById(R.id.measurement_appointment_left);
        view_details_tracking = (TextView) findViewById(R.id.view_details_tracking);
        total = (TextView) findViewById(R.id.total);
        order_type = (TextView) findViewById(R.id.order_type);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        number = (TextView) findViewById(R.id.number);
        order_date = (TextView) findViewById(R.id.order_date);
        bgLayout = (LinearLayout) findViewById(R.id.bgLayout);
        tracking_status_click = (RelativeLayout) findViewById(R.id.tracking_status_click);
        order_id_txt = (TextView) findViewById(R.id.order_id_txt);
        order_layout = (LinearLayout) findViewById(R.id.order_layout);
        measurement = (Button) findViewById(R.id.measurement);
        customization = (Button) findViewById(R.id.customization);
        tracking_current_status = (TextView) findViewById(R.id.tracking_current_status);
        qty = (TextView) findViewById(R.id.qty_txt);
        material_images = (RecyclerView) findViewById(R.id.material_images);
        reference_images = (RecyclerView) findViewById(R.id.reference_images);
        add_refernce_lable_layout = (LinearLayout) findViewById(R.id.add_refernce_lable_layout);
        add_material_lable_layout = (LinearLayout) findViewById(R.id.add_material_lable_layout);
        add_reference_img_layout = (LinearLayout) findViewById(R.id.add_reference_img_layout);
        add_material_image_layout = (LinearLayout) findViewById(R.id.add_material_image_layout);
        stitching_charges = findViewById(R.id.stitching_charges);
        measurement_charges = findViewById(R.id.measurement_charges);
        urgent_charges = findViewById(R.id.urgent_charges);
        material_delivery_charges = findViewById(R.id.material_delivery_charges);
        delivery_charges = findViewById(R.id.delivery_charges);
        payment_status = findViewById(R.id.payment_status);
        balance = findViewById(R.id.balance);
        amount_paid = findViewById(R.id.amount_paid);
        pay_remaining = findViewById(R.id.pay_remaining);
        update_amount = findViewById(R.id.update_amount);
        paid_layout = findViewById(R.id.paid_layout);
        balance_layout = findViewById(R.id.balance_layout);
        pay_balance_layout = findViewById(R.id.pay_balance_layout);
        material_type = findViewById(R.id.material_type);
        dress_type = findViewById(R.id.dress_type);
        service_type = findViewById(R.id.service_type);
        view_details_layout = findViewById(R.id.view_details_layout);
        quotation_status = findViewById(R.id.quotation_status);
//        material_appointment_layout = findViewById(R.id.material_appointment_layout);
//        measurement_appointment_layout = findViewById(R.id.measurement_appointment_layout);
//        material_appoinment_label_layout = findViewById(R.id.material_appoinment_label_layout);
//        measurement_appoinment_label_layout = findViewById(R.id.measurement_appoinment_label_layout);
        view_details = findViewById(R.id.view_details);
//        material_appointment_status = findViewById(R.id.material_appointment_status);
//        measurement_appointment_status = findViewById(R.id.measurement_appointment_status);
//        material_empty_text_layout = findViewById(R.id.material_empty_text_layout);


//        normal_review_layout,
//                company_material_label_layout,
//                company_review_layout,
//                payment_mzyoon_label_layout, payment_mzyoon_layout;
//        normal_review_label_layout = findViewById(R.id.normal_review_label_layout);
//        normal_review_layout = findViewById(R.id.normal_review_layout);
//        company_material_label_layout = findViewById(R.id.company_material_label_layout);
//        company_review_layout = findViewById(R.id.company_review_layout);
//        payment_mzyoon_label_layout = findViewById(R.id.payment_mzyoon_label_layout);
//        payment_mzyoon_layout = findViewById(R.id.payment_mzyoon_layout);
//        material_appointment_view_button_layout = findViewById(R.id.material_appointment_view_button_layout);
//        material_appointment_view_text = findViewById(R.id.material_appointment_view_text);
//        measurement_appointment_view_button_layout = findViewById(R.id.measurement_appointment_view_button_layout);
//        material_appoinment_left_layout = findViewById(R.id.material_appoinment_left_layout);
//        measurement_appoinment_left_layout = findViewById(R.id.measurement_appoinment_left_layout);
//        measurement_appointment_view_text = findViewById(R.id.measurement_appointment_view_text);


//
//        schedule_appointment_type,schedule_appointment_date,
//                schedule_appointment_left,
//                schedule_appointment_status
//        schedule_appointment_type = findViewById(R.id.schedule_appointment_type);
//        schedule_appointment_date = findViewById(R.id.schedule_appointment_date);
//        schedule_appointment_left = findViewById(R.id.schedule_appointment_left);
//        schedule_appointment_status = findViewById(R.id.schedule_appointment_status);
//        schedule_appoinment_label_layout = findViewById(R.id.schedule_appoinment_label_layout);
//        schedule_appointment_layout = findViewById(R.id.schedule_appointment_layout);
//        schedule_appointment_view_button_layout = findViewById(R.id.schedule_appointment_view_button_layout);
//        schedule_appointment_view_text = findViewById(R.id.schedule_appointment_view_text);
//        material_empty_text = findViewById(R.id.material_empty_text);
//        measurement_empty_text_layout = findViewById(R.id.measurement_empty_text_layout);
//        measurement_empty_text = findViewById(R.id.measurement_empty_text);
//        schedule_empty_text_layout = findViewById(R.id.schedule_empty_text_layout);
//        schedule_empty_text = findViewById(R.id.schedule_empty_text);
        aprox_delivery_date = findViewById(R.id.aprox_delivery_date);
        no_of_days = findViewById(R.id.no_of_days);
        material_required = findViewById(R.id.material_required);
        quotation_date = findViewById(R.id.quotation_date);
        material_details_text = findViewById(R.id.material_details_text);
        urgent_charges_layout = findViewById(R.id.urgent_charges_layout);
        stitching_charges_layout = findViewById(R.id.stitching_charges_layout);
        measurement_charges_layout = findViewById(R.id.measurement_charges_layout);
        material_charges_layout = findViewById(R.id.material_charges_layout);
        dialogManager=new DialogManager();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        dialogManager.showProgress(getApplicationContext());
//        if (AppConstants.ORDER_DETAILS.equalsIgnoreCase("ORDER_DETAILS_PENDING")) {
//
//            normal_review_label_layout.setVisibility(View.GONE);
//            normal_review_layout.setVisibility(View.GONE);
//            company_material_label_layout.setVisibility(View.GONE);
//            company_review_layout.setVisibility(View.GONE);
//            payment_mzyoon_label_layout.setVisibility(View.GONE);
//            payment_mzyoon_layout.setVisibility(View.GONE);
//
//        } else {
//            tracking_status_click.setVisibility(View.GONE);
//        }
        getLanguage();
        getRequestCustomization();

        if (AppConstants.QUOTATION_ADAPTER.equalsIgnoreCase("Pending")) {


            quotation_status.setText(R.string.pending);

        } else {
            quotation_status.setText(R.string.rejected);
        }
        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {

            getOrderDetailsForTailor("Tailor");


        } else {
//            getOrderDetails();
            getOrderDetailsForTailor("Buyer");
        }


//        view_details_tracking.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), OrderTrackingDetailsScreen.class));
//            }
//        });

        view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppConstants.PATTERN_ID.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Choose A Material", Toast.LENGTH_LONG).show();
                } else {
//                    AppConstants.PATTERN_NAME="Order_details";
                    startActivity(new Intent(getApplicationContext(), MaterialDetailsScreenOrderDetails.class));
                }
            }
        });

//        getOrderDetailsForTailor();
        update_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pay_remaining.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter Minimum Amount", Toast.LENGTH_LONG).show();

                } else if (remaining < 0) {

                    Toast.makeText(getApplicationContext(), "Amount Exceeded Than Remaining ", Toast.LENGTH_LONG).show();
                } else if (remaining > 0) {

                    UpdateBalaceAmountByTailor();
                    new AlertDialog.Builder(getApplicationContext())
                            .setMessage("Are You Sure Want To Proceed")
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {


                                            UpdateBalaceAmountByTailor();

//


                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Do something after 100ms
//                                                    finish();
//                                                    startActivity(new Intent(getApplicationContext(), SignUpActivity.class));

                                                    getOrderDetailsForTailor("Tailor");

                                                    Toast.makeText(getApplicationContext(), "Amount Updated Sucessfully", Toast.LENGTH_LONG).show();
//                                                    pay_balance_layout.setVisibility(View.VISIBLE);

                                                }
                                            }, 1000);
                                        }
                                    }).setNegativeButton(R.string.no, null).show();

                } else if (remaining == 0) {


                    new AlertDialog.Builder(getApplicationContext())
                            .setMessage("Are You Sure Want To Proceed")
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {


                                            UpdateBalaceAmountByTailor();
                                            updatePaymentStatus("1");

//


                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Do something after 100ms
//                                                    finish();
//                                                    startActivity(new Intent(getApplicationContext(), SignUpActivity.class));

                                                    getOrderDetailsForTailor("Tailor");
                                                    Toast.makeText(getApplicationContext(), "Amount Updated Sucessfully", Toast.LENGTH_LONG).show();


                                                }
                                            }, 1000);
                                        }
                                    }).setNegativeButton(R.string.no, null).show();


                }
            }
        });

        pay_remaining.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                remaining = Float.parseFloat(AppConstants.BALANCE) - Float.parseFloat(pay_remaining.getText().toString()
                        .equalsIgnoreCase("") ? "0" : pay_remaining.getText().toString());


//                Toast.makeText(getApplicationContext(), "" + remaining, Toast.LENGTH_LONG).show();
//                Integer.parseInt(pay_remaining.getText().toString().
//                        equalsIgnoreCase("") ? "0" : pay_remaining.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


//        tracking_status_click.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getTrackingStatus();
//            }
//        });
        measurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AppConstants.TAILORMEASUREMENT = "TAILORMEASUREMENT";

                if (getMeasurementDetailsModals.size() <= 0) {
//                    if (AppConstants.MEASUREMENT_ID_MANUALLY == 2 ||
//                            AppConstants.MEASUREMENT_ID_MANUALLY == 3
//                    ) {


//                        if (genderId.equalsIgnoreCase("1")) {
//                            AppConstants.GENDER_NAME = "Male";
//                        } else if (genderId.equalsIgnoreCase("2")) {
//                            AppConstants.GENDER_NAME = "Female";
//
//                        } else if (genderId.equalsIgnoreCase("3")) {
//                            AppConstants.GENDER_NAME = "Boy";
//
//                        } else if (genderId.equalsIgnoreCase("4")) {
//                            AppConstants.GENDER_NAME = "Girl";
//
//                        }

//                        if (AppConstants.MEASUREMENT_APPOINTMENT_STATUS.equalsIgnoreCase("Approved") ||
//                                AppConstants.MEASUREMENT_APPOINTMENT_STATUS.equalsIgnoreCase("Buyer Approved")) {
////                        Toast.makeText(getApplicationContext(), "check", Toast.LENGTH_LONG).show();
//                            startActivity(new Intent(getApplicationContext(), MeasurementOneScreen.class));
//                        } else {
//
//                        }


                        if (AppConstants.QUOTATION_ADAPTER.equalsIgnoreCase("Pending")) {



                            Toast.makeText(getApplicationContext(), "Measurement will be added during appointment", Toast.LENGTH_LONG).show();

                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Order Rejected Without Adding Measurement", Toast.LENGTH_LONG).show();

                        }

//                    }
                } else {
                    startActivity(new Intent(getApplicationContext(), ViewMeasurementActivity.class));
//                    Toast.makeText(getApplicationContext(), "sfs", Toast.LENGTH_LONG).show();

                }
            }
        });

        customization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ViewCustomizationActivity.class));
            }
        });


//        material_appointment_view_button_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), AppointmentDetails.class));
//            }
//        });
//
//
//        measurement_appointment_view_button_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), AppointmentDetails.class));
//            }
//        });
//
//
//        schedule_appointment_view_button_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), ScheduletDetailScreen.class));
//            }
//        });
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.order_quotation);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }


    public void getOrderDetailsForTailor(String OrderTypes) {

//        restService.getOrderDetailsResponseForTailor("212").enqueue(new Callback<GetOrderDetailsResponseCheck>() {
        restService.getOrderDetailsForOrderQuotation(AppConstants.ORDER_ID, AppConstants.ORDER_TYPE, OrderTypes).enqueue(new Callback<GetOrderDetailsResponseCheck>() {

            @Override
            public void onResponse(Call<GetOrderDetailsResponseCheck> call, Response<GetOrderDetailsResponseCheck> response) {
                try {

                    orderDetails = response.body().getResult().getOrderDetail();
                    buyerAddresses = response.body().getResult().getBuyerAddress();
                    statuses = response.body().getResult().getStatus();
                    materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();
                    referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();
                    balances = response.body().getResult().getBalances();

                    materialChargesEntities = response.body().getResult().getMaterialChargesEntities();
                    measurementChargEntities = response.body().getResult().getMeasurementChargesEntities();
                    stitchingChargesEntities = response.body().getResult().getStitchingChargesEntities();
                    urgentStichingChargeEntities = response.body().getResult().getUrgentStichingChargeEntities();
                    additionalMaterialImageEntities = response.body().getResult().getAdditionalMaterialImageEntities();



                    if (referenceImagesSkillUpdateGetEntities.size() <= 0) {
                        referenceImagesSkillUpdateGetEntities = new ArrayList<>();
                        add_refernce_lable_layout.setVisibility(View.GONE);
                        add_reference_img_layout.setVisibility(View.GONE);

                    }



                    try {
                        if (orderDetails.get(0).getMaterialType().equalsIgnoreCase("Companies-Material")) {
                            additionalImages();
                            referenceImage();
                        } else {
                            view_details_layout.setVisibility(View.GONE);
                            setImageList();
                            referenceImage();
                        }

                    } catch (Exception e) {

                    }
if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){

    order_id.setText("#" + String.valueOf(orderDetails.get(0).getOrderId()));
    product_name.setText(orderDetails.get(0).getProductName());
    service_type.setText(orderDetails.get(0).getServiceType());

//                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
//                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
//                        shipping_handling.setText("0");


    stitching_charges.setText(String.valueOf(stitchingChargesEntities.get(0).getStichingAndMaterialCharge() * orderDetails.get(0).getQty()));
    urgent_charges.setText(String.valueOf(urgentStichingChargeEntities.get(0).getUrgentStichingCharges()));
    measurement_charges.setText(String.valueOf(measurementChargEntities.get(0).getMeasurementCharges()));
////                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialDeliveryCharges()));
    material_delivery_charges.setText(String.valueOf(materialChargesEntities.get(0).getMaterialCharges()));
    ftotal = stitchingChargesEntities.get(0).getStichingAndMaterialCharge() +
            urgentStichingChargeEntities.get(0).getUrgentStichingCharges() +
            measurementChargEntities.get(0).getMeasurementCharges() +
            materialChargesEntities.get(0).getMaterialCharges();
    total.setText(String.valueOf(ftotal));
    price.setText(String.valueOf(ftotal));
//                    price.setText(String.valueOf(ftotal));
    order_date.setText(orderDetails.get(0).getOrderDt());
    if (urgentStichingChargeEntities.get(0).getUrgentStichingCharges() == 0.0) {

        urgent_charges_layout.setVisibility(View.GONE);

    }

    if (stitchingChargesEntities.get(0).getStichingAndMaterialCharge() == 0.0) {
        stitching_charges_layout.setVisibility(View.GONE);
    }
    if (measurementChargEntities.get(0).getMeasurementCharges() == 0.0) {
        measurement_charges_layout.setVisibility(View.GONE);
    }
    if (materialChargesEntities.get(0).getMaterialCharges() == 0.0) {
        material_charges_layout.setVisibility(View.GONE);
    }


    try {
        Glide.with(getApplicationContext())
                .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                .into(order_details_image);

    } catch (Exception ex) {
        Log.e(AppConstants.TAG, ex.getMessage());
    }
//                    tracking_current_status.setText("Current Status: " + statuses.get(0).getStatus());
    payment_status.setText((orderDetails.get(0).getPaymentStatus()));
    qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));

    order_type.setText(orderDetails.get(0).getServiceType());

    material_type.setText(orderDetails.get(0).getMaterialType());
    name.setText(buyerAddresses.get(0).getFirstName());
    address.setText("  " + buyerAddresses.get(0).getFloor() + "\n" + "  " + buyerAddresses.get(0).getArea() + "\n" +
            "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
    number.setText(buyerAddresses.get(0).getPhoneNo());
    aprox_delivery_date.setText(orderDetails.get(0).getApproximateDeliveryDate());
    no_of_days.setText(orderDetails.get(0).getNoOfDays());

    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
        dress_type.setText(orderDetails.get(0).getDressTypeName());
        qty.setText(String.valueOf("كمية:" + orderDetails.get(0).getQty()));
        material_details_text.setText("تفاصيل المواد" + "(" + orderDetails.get(0).
                getMaterialTypeInArabic() + ")");
        material_required.setText(orderDetails.get(0).getPatternInArabic());
        product_name.setText(orderDetails.get(0).getNameInArabic());
        service_type.setText(orderDetails.get(0).getServiceTypeInArabic());
        material_type.setText(orderDetails.get(0).getMaterialTypeInArabic());


    } else {

        dress_type.setText(orderDetails.get(0).getDressTypeName());
        qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));
        material_details_text.setText("Material Details" + "(" + orderDetails.get(0).
                getMaterialType() + ")");
        material_required.setText(orderDetails.get(0).getPatternInEnglish());
        material_type.setText(orderDetails.get(0).getMaterialType());
        product_name.setText(orderDetails.get(0).getProductName());
        service_type.setText(orderDetails.get(0).getServiceType());

    }
    quotation_date.setText(AppConstants.QUOTATION_DATE);

    AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
    AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
    genderId = String.valueOf(orderDetails.get(0).getGender());
}

else {

    order_id.setText("#" + String.valueOf(orderDetails.get(0).getOrderId()));
    product_name.setText(orderDetails.get(0).getProductName());
    service_type.setText(orderDetails.get(0).getServiceType());

//                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
//                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
//                        shipping_handling.setText("0");


    stitching_charges.setText(String.valueOf(stitchingChargesEntities.get(0).getStichingAndMaterialCharge() * orderDetails.get(0).getQty()));
    urgent_charges.setText(String.valueOf(urgentStichingChargeEntities.get(0).getUrgentStichingCharges()));
    measurement_charges.setText(String.valueOf(measurementChargEntities.get(0).getMeasurementCharges()));
////                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialDeliveryCharges()));
    material_delivery_charges.setText(String.valueOf(materialChargesEntities.get(0).getMaterialCharges()));
    ftotal = stitchingChargesEntities.get(0).getStichingAndMaterialCharge() +
            urgentStichingChargeEntities.get(0).getUrgentStichingCharges() +
            measurementChargEntities.get(0).getMeasurementCharges() +
            materialChargesEntities.get(0).getMaterialCharges();
    total.setText(String.valueOf(ftotal));
    price.setText(String.valueOf(ftotal));
//                    price.setText(String.valueOf(ftotal));
    order_date.setText(orderDetails.get(0).getOrderDt());
    if (urgentStichingChargeEntities.get(0).getUrgentStichingCharges() == 0.0) {

        urgent_charges_layout.setVisibility(View.GONE);

    }

    if (stitchingChargesEntities.get(0).getStichingAndMaterialCharge() == 0.0) {
        stitching_charges_layout.setVisibility(View.GONE);
    }
    if (measurementChargEntities.get(0).getMeasurementCharges() == 0.0) {
        measurement_charges_layout.setVisibility(View.GONE);
    }
    if (materialChargesEntities.get(0).getMaterialCharges() == 0.0) {
        material_charges_layout.setVisibility(View.GONE);
    }


    try {
        Glide.with(getApplicationContext())
                .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                .into(order_details_image);

    } catch (Exception ex) {
        Log.e(AppConstants.TAG, ex.getMessage());
    }
//                    tracking_current_status.setText("Current Status: " + statuses.get(0).getStatus());
    payment_status.setText((orderDetails.get(0).getPaymentStatus()));
    qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));

    order_type.setText(orderDetails.get(0).getServiceType());

    material_type.setText(orderDetails.get(0).getMaterialType());
    name.setText(buyerAddresses.get(0).getFirstName());
    address.setText("  " + buyerAddresses.get(0).getFloor() + "\n" + "  " + buyerAddresses.get(0).getArea() + "\n" +
            "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
    number.setText(buyerAddresses.get(0).getPhoneNo());
    aprox_delivery_date.setText(orderDetails.get(0).getApproximateDeliveryDate());
    no_of_days.setText(orderDetails.get(0).getNoOfDays());

    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
        dress_type.setText(orderDetails.get(0).getDressTypeName());
        qty.setText(String.valueOf("كمية:" + orderDetails.get(0).getQty()));
        material_details_text.setText("تفاصيل المواد" + "(" + orderDetails.get(0).
                getMaterialTypeInArabic() + ")");
        material_required.setText(orderDetails.get(0).getPatternInArabic());
        product_name.setText(orderDetails.get(0).getNameInArabic());
        service_type.setText(orderDetails.get(0).getServiceTypeInArabic());
        material_type.setText(orderDetails.get(0).getMaterialTypeInArabic());


    } else {

        dress_type.setText(orderDetails.get(0).getDressTypeName());
        qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));
        material_details_text.setText("Material Details" + "(" + orderDetails.get(0).
                getMaterialType() + ")");
        material_required.setText(orderDetails.get(0).getPatternInEnglish());
        material_type.setText(orderDetails.get(0).getMaterialType());
        product_name.setText(orderDetails.get(0).getProductName());
        service_type.setText(orderDetails.get(0).getServiceType());

    }
    quotation_date.setText(AppConstants.QUOTATION_DATE);

    AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
    AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
    genderId = String.valueOf(orderDetails.get(0).getGender());
}




                    if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {

                        AppConstants.BALANCE = balances.get(0).getBalance().toString();
                        bal = ftotal - Float.parseFloat(AppConstants.BALANCE);
                        balance.setText(balances.get(0).getBalance().toString());
                        amount_paid.setText(String.valueOf(bal));


                        if (Float.parseFloat(AppConstants.BALANCE) > 0) {

                            paid_layout.setVisibility(View.VISIBLE);
                            balance_layout.setVisibility(View.VISIBLE);
                            pay_balance_layout.setVisibility(View.VISIBLE);

                        }

                        if (Float.parseFloat(AppConstants.BALANCE) == 0) {
                            pay_balance_layout.setVisibility(View.GONE);
                        }

                    }
//                        float bal = Float.parseFloat(AppConstants.BALANCE);
//                        float remaining = ftotal - bal;
//                        tax.setText(productPrices.get(0).getTax());
//
//
//                        appointment_charges.setText(String.valueOf(productPrices.get(0).getAppoinment()));
//                        total.setText(String.valueOf(productPrices.get(0).getTotal()));


                } catch (Exception e) {

                }


            }

            @Override
            public void onFailure(Call<GetOrderDetailsResponseCheck> call, Throwable t) {
                insertError("OrderQuotationActivity", "getOrderDetailsForOrderQuotation()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }


    public void showCoutrySelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
        TextView myMsg = new TextView(this);
        myMsg.setText(R.string.update_order_status);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextColor(Color.BLACK);
        //set custom title
//        builder.setCustomTitle(myMsg);
        dialog = new AlertDialog.Builder(this)
                .setCustomTitle(myMsg)
                .setView(view)
                .create();
        trackingStatusAdapter = new TrackingStatusCheckListAdapter(getApplicationContext(),
                dialog, restService, getTrackingByOrderModals, getTrackingByOrderModal);
        countryList.setAdapter(trackingStatusAdapter);
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
                    getOrderDetailsForTailor("Tailor");
                } else {

//                    getOrderdetailsForBuyer();
                    getOrderDetailsForTailor("Buyer");
                }
            }
        });

    }


    public void getTrackingStatus() {


        restService.getTrackingDetailsResponse(AppConstants.ORDER_ID).enqueue(new Callback<GetTrackingDetailsResponse>() {
            @Override
            public void onResponse(Call<GetTrackingDetailsResponse> call, Response<GetTrackingDetailsResponse> response) {


                try {

//                    getTrackingDetailsModals = response.body().getResult().getTrackingDetails();
                    getTrackingByOrderModals = response.body().getResult().getGetTrackingByOrder();
                    setCountrySelectionList();
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<GetTrackingDetailsResponse> call, Throwable t) {
                insertError("OrderQuotationActivity", "getTrackingDetailsResponse()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }


    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < getTrackingByOrderModals.size(); i++) {
            listSpinner.add(getTrackingByOrderModals.get(i).getStatus());
        }

        showCoutrySelectionList();


        mHeaderLeftBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }


    }


    public void getOrderdetailsForBuyer() {

        restService.getOrderDetailsResponseBuyer(AppConstants.ORDER_ID).enqueue(new Callback<BuyerOrderDetailResponse>() {
            @Override
            public void onResponse(Call<BuyerOrderDetailResponse> call, Response<BuyerOrderDetailResponse> response) {

                if (response.isSuccessful()) {
                    try {
                        orderDetails = response.body().getResult().getOrderDetail();
                        buyerAddresses = response.body().getResult().getBuyerAddress();
                        productPrices = response.body().getResult().getProductPrice();
                        shippingCharges = response.body().getResult().getShippingCharges();
                        statuses = response.body().getResult().getStatus();
                        materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();
                        referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();


                        if (referenceImagesSkillUpdateGetEntities.size() > 0) {
                            add_refernce_lable_layout.setVisibility(View.VISIBLE);
                            add_reference_img_layout.setVisibility(View.VISIBLE);

                        }

                        if (materialImagesSkillUpdateGetEntities.size() > 0) {

                            add_material_lable_layout.setVisibility(View.VISIBLE);
                            add_material_image_layout.setVisibility(View.VISIBLE);

                        }

                        setImageList();

                        order_id.setText(String.valueOf(orderDetails.get(0).getOrderId()));
                        product_name.setText(orderDetails.get(0).getProductName());
                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
                        shipping_handling.setText("0");
                        tracking_current_status.setText("Tracking Current Status: " + statuses.get(0).getStatus());
                        tax.setText(productPrices.get(0).getTax());
                        qty.setText(String.valueOf(orderDetails.get(0).getQty()));

                        appointment_charges.setText(String.valueOf(productPrices.get(0).getAppoinment()));
                        total.setText(String.valueOf(productPrices.get(0).getTotal()));
                        order_type.setText(orderDetails.get(0).getServiceType());


                        name.setText(buyerAddresses.get(0).getFirstName());
                        address.setText("  " + buyerAddresses.get(0).getFloor() + "," +
                                buyerAddresses.get(0).getArea() + "\n" +
                                "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
                        number.setText(buyerAddresses.get(0).getPhoneNo());


                        order_date.setText(orderDetails.get(0).getOrderDt());
                        AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
                        AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
                        genderId = String.valueOf(orderDetails.get(0).getGender());

                        try {
                            Glide.with(getApplicationContext())
                                    .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
                                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                    .into(order_details_image);

                        } catch (Exception ex) {
                            Log.e(AppConstants.TAG, ex.getMessage());
                        }


                    } catch (Exception e) {

                    }

                }

            }

            @Override
            public void onFailure(Call<BuyerOrderDetailResponse> call, Throwable t) {

            }
        });
    }

    private void setImageList() {
        material_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        material_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        skillUpdateGetMaterialImageAdapter = new SkillUpdateGetMaterialImageAdapter(this, materialImagesSkillUpdateGetEntities, restService);
        material_images.setAdapter(skillUpdateGetMaterialImageAdapter);


//


    }


    public void referenceImage() {
        reference_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        reference_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        skillUpdateGetReferenceImageAdapter = new SkillUpdateGetReferenceImageAdapter(this, referenceImagesSkillUpdateGetEntities, restService);
        reference_images.setAdapter(skillUpdateGetReferenceImageAdapter);

    }

    private void additionalImages() {
        material_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        material_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        additionalMaterialImageAdapter = new AdditionalMaterialImageAdapter(this, additionalMaterialImageEntities, restService);
        material_images.setAdapter(additionalMaterialImageAdapter);


//


        reference_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        reference_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        skillUpdateGetReferenceImageAdapter = new SkillUpdateGetReferenceImageAdapter(this, referenceImagesSkillUpdateGetEntities, restService);
        reference_images.setAdapter(skillUpdateGetReferenceImageAdapter);


    }

    public void getRequestCustomization() {

        try {
            restService.getRequestMeasurementParts(AppConstants.ORDER_ID).enqueue(new Callback<GetRequestMeasurementPartsResponse>() {
                @Override
                public void onResponse(Call<GetRequestMeasurementPartsResponse> call, Response<GetRequestMeasurementPartsResponse> response) {
//
                    if (response.isSuccessful()) {

                        try {
                            getMeasurementDetailsModals = response.body().getResult();
                            if (getMeasurementDetailsModals == null) {
                                getMeasurementDetailsModals = new ArrayList<>();
                            }

                            dialogManager.hideProgress();
                        } catch (Exception e) {

                        }

//                            setGenderList(getMeasurementDetailsModals);
                    } else if (response.errorBody() != null) {
                        Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<GetRequestMeasurementPartsResponse> call, Throwable t) {
                    insertError("OrderQuotationActivity", "getRequestMeasurementParts()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
        }
    }


    public void UpdateBalaceAmountByTailor() {

        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("Amount", String.valueOf(remaining));

        restService.UpdateBalaceAmountByTailor(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                getOrderDetailsForTailor("Tailor");
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("OrderQuotationActivity", "UpdateBalaceAmountByTailor()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }

    public void updatePaymentStatus(String paymentStatus) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("PaymentStatus", paymentStatus);
        map.put("Type", "Tailor");

        restService.updatePaymentStatus(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("OrderQuotationActivity", "updatePaymentStatus()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.TAILORMEASUREMENT = "";
        AppConstants.PATTERN_ID = "";
        AppConstants.MEASUREMENT_APPOINTMENT_STATUS = "";

    }
//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}

