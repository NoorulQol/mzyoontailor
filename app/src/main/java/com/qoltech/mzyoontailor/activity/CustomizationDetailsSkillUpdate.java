package com.qoltech.mzyoontailor.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AdapterOneToNine;
import com.qoltech.mzyoontailor.adapter.CustumizationListSkillUpdateAdapter;
import com.qoltech.mzyoontailor.adapter.MaterialChargesListSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.GetAttributesIdToSaveLocalEntity;
import com.qoltech.mzyoontailor.entity.GetMaterialForUpdatingSkillUpdateChargesEntity;
import com.qoltech.mzyoontailor.entity.GetStichingChargeListEntity;
import com.qoltech.mzyoontailor.entity.GetTailorCustomizationSkillUpdate;
import com.qoltech.mzyoontailor.entity.InsertCustumizationSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.InsertMaterialChargesEntitySkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GetAttributeIdToSaveLocalModal;
import com.qoltech.mzyoontailor.modal.GetCustomizationsByTailorIdSkillUpdateResponse;
import com.qoltech.mzyoontailor.modal.InsertCustumizationSkillUpdateModal;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomizationDetailsSkillUpdate extends BaseActivity {
    ApiService restService;
    CustumizationListSkillUpdateAdapter custumizationListSkillUpdateAdapter;
    RecyclerView custumization_list;
    List<GetTailorCustomizationSkillUpdate> getTailorCustomizationSkillUpdates;
    List<GetTailorCustomizationSkillUpdate> getTailorCustomizationSkillUpdatesCheck;

    RecyclerView material_charges_list;

    MaterialChargesListSkillUpdateAdapter materialChargesListSkillUpdateAdapter;
    List<GetMaterialForUpdatingSkillUpdateChargesEntity> getMaterialForUpdatingSkillUpdateChargesEntities;
    AppCompatCheckBox check_box;
    RelativeLayout cutomization_text_label, custumization_list_layout;
    LinearLayout material_charges_layout, stitching_charges_layout;
    Button save;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    SharedPreferences sharedPreferences;
    public ArrayList<String> materialList;
    public ArrayList<String> materialListId;
    public UserDetailsEntity mUserDetailsEntityRes;
    EditText stitching_charges;
    TransparentProgressDialog transparentProgressDialog;
    DialogManager dialogManager;
    List<GetStichingChargeListEntity> getStichingChargeListEntities;

    List<GetAttributesIdToSaveLocalEntity> getAttributesIdToSaveLocalEntities;
    LinearLayout no_of_days_required;
    public static Dialog mOrderConfirmDialog;
    public AdapterOneToNine mConfirmAdapter;
    public ArrayList<String> mArrayList;
    public static TextView days_required;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ui_custumization_details_skill_update);
        restService = ((MainApplication) getApplication()).getClient();
        custumization_list = (RecyclerView) findViewById(R.id.custumization_list);
        material_charges_list = (RecyclerView) findViewById(R.id.material_charges_list);
        check_box = (AppCompatCheckBox) findViewById(R.id.check_box);
        save = (Button) findViewById(R.id.save);
        days_required = (TextView) findViewById(R.id.days_required);
        stitching_charges = (EditText) findViewById(R.id.stitching_charges);
        cutomization_text_label = (RelativeLayout) findViewById(R.id.cutomization_text_label);
        custumization_list_layout = (RelativeLayout) findViewById(R.id.custumization_list_layout);
        stitching_charges_layout = (LinearLayout) findViewById(R.id.stitching_charges_layout);
        material_charges_layout = (LinearLayout) findViewById(R.id.material_charges_layout);
        no_of_days_required = (LinearLayout) findViewById(R.id.no_of_days_required);

        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        dialogManager = new DialogManager();
        mOrderConfirmDialog = new Dialog(getApplicationContext());
        mOrderConfirmDialog.setCancelable(true);
        mArrayList = new ArrayList<>();
        for (int i = 0; i <= 60; i++) {
            mArrayList.add(String.valueOf(i));
        }
        initView();
//        getCustumizationByTailorId(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID);
        getCustumizationByTailorId(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID);

        getAttributesIDForLocalApi(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID);
        check_box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    save.setVisibility(View.VISIBLE);
                    cutomization_text_label.setVisibility(View.VISIBLE);
                    custumization_list_layout.setVisibility(View.VISIBLE);
                    stitching_charges_layout.setVisibility(View.VISIBLE);
                    material_charges_layout.setVisibility(View.VISIBLE);
                    no_of_days_required.setVisibility(View.VISIBLE);
                } else {
                    save.setVisibility(View.GONE);
                    custumization_list_layout.setVisibility(View.GONE);
                    stitching_charges_layout.setVisibility(View.GONE);
                    material_charges_layout.setVisibility(View.GONE);
                    cutomization_text_label.setVisibility(View.GONE);
                    no_of_days_required.setVisibility(View.GONE);

                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (days_required.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Select The Days Required", Toast.LENGTH_LONG).show();
                } else {
                    dialogManager.showProgress(CustomizationDetailsSkillUpdate.this);

                    insertAttributeQuantityApiCall();
                }
//                if (AppConstants.GETCUSTUMIZATION_ID.size() == AppConstants.GETCUSTUMIZATION_ID_CLICK.size()) {
//                    dialogManager.showProgress(CustomizationDetailsSkillUpdate.this);
//                    insertAttributeQuantityApiCall();
//                } else {
//                    Toast.makeText(getApplicationContext(), "Please Click and View All The Items in List Atleat Once", Toast.LENGTH_LONG).show();
//                }
            }

        });

        no_of_days_required.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstants.CLASS_NAME = "Skill_Update";
                setAdapter(mArrayList);
            }
        });

    }

    public void getCustumizationByTailorId(String tailorId, String subTypeID) {


        restService.getCustumizationByTailorId(tailorId, subTypeID)
                .enqueue(new Callback<GetCustomizationsByTailorIdSkillUpdateResponse>() {
                    @Override
                    public void onResponse(Call<GetCustomizationsByTailorIdSkillUpdateResponse> call, Response<GetCustomizationsByTailorIdSkillUpdateResponse> response) {

                        try {

                            getTailorCustomizationSkillUpdates = response.body().getResult().getGetTailorCustomization();
                            getMaterialForUpdatingSkillUpdateChargesEntities = response.body().getResult().getGetMaterial();
                            getStichingChargeListEntities = response.body().getResult().getStichingCharge();
//                            stitching_charges.setText(String.valueOf(getStichingChargeListEntities.get(0).getStichingCharges()));
//                            Toast.makeText(getApplicationContext(), "Noorul" + getTailorCustomizationSkillUpdates.get(0).getAttributeNameInEnglish(), Toast.LENGTH_LONG).show();
                            setGenderList();


                            if (getStichingChargeListEntities.size() > 0) {
//                                setGenderList();

                                check_box.setChecked(true);
                                save.setVisibility(View.VISIBLE);
                                cutomization_text_label.setVisibility(View.VISIBLE);
                                custumization_list_layout.setVisibility(View.VISIBLE);
                                stitching_charges_layout.setVisibility(View.VISIBLE);
                                material_charges_layout.setVisibility(View.VISIBLE);
                                stitching_charges.setText(String.valueOf(getStichingChargeListEntities.get(0).getStichingCharges()));
                                days_required.setText(String.valueOf(getStichingChargeListEntities.get(0).getNoOfDays()));

                            } else {
                                getStichingChargeListEntities = new ArrayList<>();

                            }

//                            AppConstants.ATTRIBUTE_ID_LIST.add(String.valueOf(getTailorCustomizationSkillUpdates.get(getTailorCustomizationSkillUpdates.size()).getId()));

                        } catch (Exception e) {
//                            Toast.makeText(getApplicationContext(), "Noorul" + getTailorCustomizationSkillUpdates.get(0).getAttributeNameInEnglish(), Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<GetCustomizationsByTailorIdSkillUpdateResponse> call, Throwable t) {
                        insertError("CustomizationDetailsSkillUpdate","getCustumizationByTailorId()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });
    }


    private void setGenderList() {
        custumization_list.setHasFixedSize(true);
        custumization_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        custumizationListSkillUpdateAdapter = new CustumizationListSkillUpdateAdapter(this, getTailorCustomizationSkillUpdates, restService);
        custumization_list.setAdapter(custumizationListSkillUpdateAdapter);
        for (int i = 0; i < getTailorCustomizationSkillUpdates.size(); i++) {

            AppConstants.GETCUSTUMIZATION_ID.add(String.valueOf(getTailorCustomizationSkillUpdates.get(i).getId()));
        }

        material_charges_list.setHasFixedSize(true);
        material_charges_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        materialChargesListSkillUpdateAdapter = new MaterialChargesListSkillUpdateAdapter(this,
                getMaterialForUpdatingSkillUpdateChargesEntities, restService);
        material_charges_list.setAdapter(materialChargesListSkillUpdateAdapter);
    }

    //
    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustomizationDetailsSkillUpdate.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();


    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.details);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                AppConstants.CLASS_NAME = "";
                break;
        }

    }


    public void insertAttributeQuantityApiCall() {
        InsertCustumizationSkillUpdateModal insertCustumizationSkillUpdateModal = new InsertCustumizationSkillUpdateModal();

        ArrayList<InsertMaterialChargesEntitySkillUpdateEntity> insertMaterialChargesEntitySkillUpdateEntities = new ArrayList<>();

        ArrayList<InsertCustumizationSkillUpdateEntity> insertCustumizationSkillUpdateEntities = new ArrayList<>();

        for (int i = 0; i < AppConstants.CUSTOM_ID.size(); i++) {
            InsertCustumizationSkillUpdateEntity insertServiceLocationSkillUpdateEntity = new InsertCustumizationSkillUpdateEntity();
            insertServiceLocationSkillUpdateEntity.setAttributeImageId(AppConstants.CUSTOM_ID.get(i));
            insertServiceLocationSkillUpdateEntity.setCustomizationAttributeId(AppConstants.ATTRIBUTE_ID_LIST.get(i));
//            for (int j=0;j<AppConstants.ATTRIBUTE_ID_LIST.size();j++) {
//                insertServiceLocationSkillUpdateEntity.setCustomizationAttributeId(AppConstants.ATTRIBUTE_ID_LIST.get(j));
//            }
            insertCustumizationSkillUpdateEntities.add(insertServiceLocationSkillUpdateEntity);
        }
        materialList = new ArrayList<>();
        materialListId = new ArrayList<>();
        Set keys = AppConstants.MATERIAL_MAP_NEW.keySet();
        Iterator itr = keys.iterator();

        int key;
        String value;
        while (itr.hasNext()) {

            key = (int) itr.next();
            value = (String) AppConstants.MATERIAL_MAP_NEW.get(key);
            System.out.println(key + " - " + value);

            materialList.add(String.valueOf(value));
            materialListId.add(String.valueOf(key));
//            Collections.reverse(materialList);
        }


        for (int i = 0; i < getMaterialForUpdatingSkillUpdateChargesEntities.size(); i++) {
            InsertMaterialChargesEntitySkillUpdateEntity insertMaterialChargesEntitySkillUpdateEntity =
                    new InsertMaterialChargesEntitySkillUpdateEntity();
            insertMaterialChargesEntitySkillUpdateEntity.setMaterialId(materialListId.get(i));
            if (materialList.size() == getMaterialForUpdatingSkillUpdateChargesEntities.size()) {
                insertMaterialChargesEntitySkillUpdateEntity.setMaterialCharges(materialList.get(i));
            } else {
                insertMaterialChargesEntitySkillUpdateEntity.setMaterialCharges("0");
            }
            insertMaterialChargesEntitySkillUpdateEntities.add(insertMaterialChargesEntitySkillUpdateEntity);
        }


//        ArrayList<OrderCustomizationIdValueEntity> orderCustomizationIdValueEntities = new ArrayList<>();


        insertCustumizationSkillUpdateModal.setMaterialCharge(insertMaterialChargesEntitySkillUpdateEntities);
        insertCustumizationSkillUpdateModal.setTailorId(sharedPreferences.getString("TailorId", ""));
        insertCustumizationSkillUpdateModal.setDressSubTypeId(AppConstants.SUB_DRESS_TYPE_ID);
        insertCustumizationSkillUpdateModal.setCustomizationSkill(insertCustumizationSkillUpdateEntities);
        insertCustumizationSkillUpdateModal.setStichingCharges(stitching_charges.getText().toString());
        insertCustumizationSkillUpdateModal.setNoOfDays(days_required.getText().toString());
        restService.insertCustumizationSkillUpdateModal(insertCustumizationSkillUpdateModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                AppConstants.MATERIAL_MAP_NEW = new HashMap<>();
//                AppConstants.CUSTOM_ID = new ArrayList<>();
                AppConstants.GETCUSTUMIZATION_ID_CLICK = new ArrayList<>();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms

                        dialogManager.hideProgress();
                        Toast.makeText(getApplicationContext(), R.string.dress_type_updated_sucessfully, Toast.LENGTH_LONG).show();
                        AppConstants.ATTRIBUTE_ID_LIST = new ArrayList<>();
                        AppConstants.CUSTOM_ID = new ArrayList<>();
                        AppConstants.GETCUSTUMIZATION_ID_CLICK = new ArrayList<>();
                        AppConstants.GETCUSTUMIZATION_ID = new ArrayList<>();
                        AppConstants.CLASS_NAME = "";
                        finish();
                    }
                }, 2000);


            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }


    public void getAttributesIDForLocalApi(String tailorId, String subdressId) {


        try {
            restService.getAttributesIDForLocalApi(tailorId, subdressId).enqueue(new Callback<GetAttributeIdToSaveLocalModal>() {
                @Override
                public void onResponse(Call<GetAttributeIdToSaveLocalModal> call, Response<GetAttributeIdToSaveLocalModal> response) {

                    AppConstants.CUSTOM_ID = new ArrayList<>();

                    AppConstants.ATTRIBUTE_ID_LIST = new ArrayList<>();

                    getAttributesIdToSaveLocalEntities = response.body().getResult();

                    for (int i = 0; i < getAttributesIdToSaveLocalEntities.size(); i++) {

                        AppConstants.CUSTOM_ID.add(String.valueOf(getAttributesIdToSaveLocalEntities.get(i).getCustomizationId()));

                        AppConstants.ATTRIBUTE_ID_LIST.add(String.valueOf(getAttributesIdToSaveLocalEntities.get(i).getCustomizationAttributeId()));

                    }
                }

                @Override
                public void onFailure(Call<GetAttributeIdToSaveLocalModal> call, Throwable t) {
                    insertError("CustomizationDetailsSkillUpdate","getAttributesIDForLocalApi()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

                }
            });
        } catch (Exception e) {

        }
    }

    public void setAdapter(ArrayList<String> mArrayList) {


        alertDismiss(mOrderConfirmDialog);
        mOrderConfirmDialog = getDialog(CustomizationDetailsSkillUpdate.this, R.layout.adapter_one_to_nine);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mOrderConfirmDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        RecyclerView recyclerView;

        /*Init view*/

        recyclerView = mOrderConfirmDialog.findViewById(R.id.countryList);
        mConfirmAdapter = new AdapterOneToNine(CustomizationDetailsSkillUpdate.this, mArrayList);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(CustomizationDetailsSkillUpdate.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mConfirmAdapter);

        alertShowing(mOrderConfirmDialog);

    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }


    public void insertError(String pageName,String methodName,String error,
                            String apiVersion,String deviceId,String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName",pageName);
        map.put("MethodName",methodName);
        map.put("Error", error);
        map.put("ApiVersion",apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type",type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
