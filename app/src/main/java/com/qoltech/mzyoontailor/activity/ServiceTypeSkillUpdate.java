package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ServiceTypesSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.InsertServiceTypeSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.ServiceTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.modal.InsertServiceTypeSkillUpdateModal;
import com.qoltech.mzyoontailor.modal.ServiceTypeResponse;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceTypeSkillUpdate extends AppCompatActivity {
    Button next;
    EditText phone_number;
    String phoneNumber;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    SharedPreferences sharedPreferences;

    ServiceTypesSkillUpdateAdapter serviceTypesSkillUpdateAdapter;
    ApiService restService;
    List<ServiceTypeEntity> serviceTypeEntities;
    RecyclerView genderList;
public  UserDetailsEntity mUserDetailsEntityRes;
Button save;
TransparentProgressDialog transparentProgressDialog;
DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_skill_update);
        genderList = (RecyclerView) findViewById(R.id.genderList);
        restService = ((MainApplication) getApplication()).getClient();
        initView();
        save=(Button)findViewById(R.id.save) ;
        dialogManager=new DialogManager();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        getGender();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManager.showProgress(ServiceTypeSkillUpdate.this);

                insertServiceTypeApi();
            }
        });
    }


    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ServiceTypeSkillUpdate.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.service_type);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getGender() {
        restService.getAppointmentTypeSkillUpdate(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(Call<ServiceTypeResponse> call, Response<ServiceTypeResponse> response) {

                serviceTypeEntities = response.body().getResult();
                setGenderList();
            }

            @Override
            public void onFailure(Call<ServiceTypeResponse> call, Throwable t) {
                insertError("ServiceTypeSkillUpdate","getAppointmentTypeSkillUpdate()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        serviceTypesSkillUpdateAdapter = new ServiceTypesSkillUpdateAdapter(this, serviceTypeEntities, restService);
        genderList.setAdapter(serviceTypesSkillUpdateAdapter);
    }


    public void insertServiceTypeApi() {

        ArrayList<InsertServiceTypeSkillUpdateEntity> insertServiceTypeSkillUpdateEntities = new ArrayList<>();

        for (int i = 0; i < AppConstants.SKILL_APPOINTMENT_ID .size(); i++) {
            InsertServiceTypeSkillUpdateEntity insertServiceTypeSkillUpdateEntity = new InsertServiceTypeSkillUpdateEntity();
            insertServiceTypeSkillUpdateEntity.setId(AppConstants.SKILL_APPOINTMENT_ID.get(i));
            insertServiceTypeSkillUpdateEntities.add(insertServiceTypeSkillUpdateEntity);
        }


        InsertServiceTypeSkillUpdateModal insertServiceTypeSkillUpdateApiModal = new InsertServiceTypeSkillUpdateModal();
        insertServiceTypeSkillUpdateApiModal.setTailorId(sharedPreferences.getString("TailorId", ""));
        insertServiceTypeSkillUpdateApiModal.setAppoinmentTypeId(insertServiceTypeSkillUpdateEntities);


        restService.insertServiceTypeSkillUpdate(insertServiceTypeSkillUpdateApiModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


                        dialogManager.hideProgress();
                        Toast.makeText(getApplicationContext(), R.string.service_type_updated_sucessfully, Toast.LENGTH_LONG).show();
                        finish();

                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                     insertError("ServiceTypeSkillUpdate","insertServiceTypeSkillUpdate()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }
    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    //     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

}
