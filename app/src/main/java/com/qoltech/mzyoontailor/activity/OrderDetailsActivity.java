package com.qoltech.mzyoontailor.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AdditionalMaterialImageAdapter;
import com.qoltech.mzyoontailor.adapter.SkillUpdateGetMaterialImageAdapter;
import com.qoltech.mzyoontailor.adapter.SkillUpdateGetReferenceImageAdapter;
import com.qoltech.mzyoontailor.adapter.TrackingStatusCheckListAdapter;
import com.qoltech.mzyoontailor.entity.AdditionalMaterialImageEntity;
import com.qoltech.mzyoontailor.entity.Balances;
import com.qoltech.mzyoontailor.entity.BuyerAddress;
import com.qoltech.mzyoontailor.entity.CustomerRatingEntity;
import com.qoltech.mzyoontailor.entity.GetAppoinmentLeftMaterialEntity;
import com.qoltech.mzyoontailor.entity.GetAppoinmentLeftMeasurementEntity;
import com.qoltech.mzyoontailor.entity.GetOrderDetailScheduleTypeEntity;
import com.qoltech.mzyoontailor.entity.GetTailorPaymentStatusEntity;
import com.qoltech.mzyoontailor.entity.MaterialChargesEntity;
import com.qoltech.mzyoontailor.entity.MaterialImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.entity.MaterialRatingEntity;
import com.qoltech.mzyoontailor.entity.MaterialReviewEntity;
import com.qoltech.mzyoontailor.entity.MeasurementChargesEntity;
import com.qoltech.mzyoontailor.entity.OrderDetail;
import com.qoltech.mzyoontailor.entity.ProductPrice;
import com.qoltech.mzyoontailor.entity.ReferenceImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.entity.ShippingCharge;
import com.qoltech.mzyoontailor.entity.Status;
import com.qoltech.mzyoontailor.entity.StitchingChargesEntity;
import com.qoltech.mzyoontailor.entity.UrgentStichingChargeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.BuyerOrderDetailResponse;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.ui.AppointmentDetails;
import com.qoltech.mzyoontailor.ui.MaterialDetailsScreenOrderDetails;
import com.qoltech.mzyoontailor.ui.MeasurementOneScreen;
import com.qoltech.mzyoontailor.ui.OrderTrackingDetailsScreen;
import com.qoltech.mzyoontailor.ui.ScheduletDetailScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsModal;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsResponse;
import com.qoltech.mzyoontailor.wrappers.GetTrackingByOrderModal;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsModal;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.GetTrackingDetailsResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;
//    List<GetBuyerAddressModal> getBuyerAddressModals;
//    List<GetOrderDetailsModal> getOrderDetailsModals;
//    List<GetOrderProductPriceModal> getOrderProductPriceModals;
//    List<GetShippingCharges> getShippingChargesModals;
//    List<GetOrderStatusModal> getOrderStatusModals;


//    List<BuyerAddressBuyer> buyerAddressBuyers;
//    List<GetOrderDetailsModal> getOrderDetailsModals;
//    List<GetOrderProductPriceModal> getOrderProductPriceModals;
//    List<GetShippingCharges> getShippingChargesModals;
//    List<GetOrderStatusModal> getOrderStatusModals;


    List<BuyerAddress> buyerAddresses;
    List<OrderDetail> orderDetails;
    List<ProductPrice> productPrices;
    List<ShippingCharge> shippingCharges;
    List<Status> statuses;
    List<ReferenceImagesSkillUpdateGetEntity> referenceImagesSkillUpdateGetEntities;
    List<MaterialImagesSkillUpdateGetEntity> materialImagesSkillUpdateGetEntities;
    List<Balances> balances;

    List<GetAppoinmentLeftMaterialEntity> getAppoinmentLeftMaterialEntities;
    List<GetAppoinmentLeftMeasurementEntity> getAppoinmentLeftMeasurementEntities;
    List<GetOrderDetailScheduleTypeEntity> orderDetailScheduleTypeEntities;
    List<CustomerRatingEntity> customerRatingEntities;
    List<MaterialReviewEntity> materialReviewEntities;

    TextView order_id, product_name, price, sub_total, shipping_handling, tax, appointment_charges,
            order_type, name, address, number, order_date;

    public static TextView tracking_current_status;
    ImageView order_details_image;
    RelativeLayout tracking_status_click;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    TrackingStatusCheckListAdapter trackingStatusAdapter;
    List<GetTrackingDetailsResult> getTrackingDetailsResults;
    GetTrackingDetailsModal getTrackingDetailsModal;
    GetTrackingByOrderModal getTrackingByOrderModal;
    SharedPreferences sharedPreferences;
    //    List<GetTrackingDetailsModal> getTrackingDetailsModals;
    List<GetTrackingByOrderModal> getTrackingByOrderModals;
    AlertDialog dialog;
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    LinearLayout bgLayout, order_layout;
    TextView order_id_txt, qty;
    Button measurement, customization;
    String genderId;
    RecyclerView material_images, reference_images;
    SkillUpdateGetMaterialImageAdapter skillUpdateGetMaterialImageAdapter;
    AdditionalMaterialImageAdapter additionalMaterialImageAdapter;
    SkillUpdateGetReferenceImageAdapter skillUpdateGetReferenceImageAdapter;
    LinearLayout add_refernce_lable_layout, add_material_lable_layout, add_reference_img_layout, add_material_image_layout;
    List<GetRequestMeasurementPartsModal> getMeasurementDetailsModals;

    TextView stitching_charges, measurement_charges,
            urgent_charges, material_delivery_charges,
            delivery_charges, total, payment_status, balance, amount_paid;
    TextView material_type, dress_type;
    Float ftotal, bal, remaining;
    EditText pay_remaining;
    ImageView update_amount;
    LinearLayout paid_layout, balance_layout, pay_balance_layout,
            material_appointment_layout,
            measurement_appointment_layout, material_appoinment_label_layout,
            measurement_appoinment_label_layout,
            schedule_appoinment_label_layout,
            schedule_appointment_layout;


    TextView material_appointment_type,
            material_appointment_date, material_appointment_left,
            measurement_appointment_type, measurement_appointment_date,
            measurement_appointment_left, view_details_tracking, service_type, view_details,
            material_appointment_status, measurement_appointment_status,
            material_appointment_view_text, measurement_appointment_view_text,
            schedule_appointment_type, schedule_appointment_date,
            schedule_appointment_left,
            schedule_appointment_status, schedule_appointment_view_text,
            material_empty_text, measurement_empty_text, schedule_empty_text, material_appointment_time,
            measurement_appointment_time, schedule_appointment_time, material_details_text, order_rating_view_text;


    List<MaterialChargesEntity> materialChargesEntities;
    List<MeasurementChargesEntity> measurementChargEntities;
    List<StitchingChargesEntity> stitchingChargesEntities;
    List<UrgentStichingChargeEntity> urgentStichingChargeEntities;
    List<AdditionalMaterialImageEntity> additionalMaterialImageEntities;
    List<GetTailorPaymentStatusEntity> getTailorPaymentStatusEntities;
    List<MaterialRatingEntity> materialRatingEntities;
    RelativeLayout view_details_layout, material_appointment_view_button_layout,
            measurement_appointment_view_button_layout, schedule_appointment_view_button_layout,
            material_empty_text_layout, measurement_empty_text_layout, schedule_empty_text_layout;
    LinearLayout normal_review_label_layout,
            normal_review_layout,
            company_material_label_layout,
            company_review_layout,
            payment_mzyoon_label_layout,
            payment_mzyoon_layout, material_appoinment_left_layout,
            measurement_appoinment_left_layout,
            schedule_appoinment_left_layout,
            material_appointment_type_layout,
            material_appointment_date_layout,
            material_appointment_time_layout, material_appointment_status_layout,
            measurement_appointment_type_layout,
            measurement_appointment_date_layout,
            measurement_appointment_time_layout, measurement_appointment_status_layout;
    TextView tailor_total_amount, tailor_payment_status, tailor_payment_mode, tailor_payment_date,
            tailor_transaction_number, customer_rating_date, customer_rating_name, customer_rating_text, company_review_rating_date, company_review_rating_name, company_review_rating_text;
    RatingBar customer_rating_rating_bar, company_review_rating_bar;
    de.hdodenhof.circleimageview.CircleImageView customer_rating_profile_image, company_review_profile_image;
    String material_appointment_status_string = "", measurement_appointment_string = "";
    LinearLayout stitching_charges_layout, measurement_charges_layout, material_charges_layout, urgent_charges_layout;
DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details_latest);
        initView();
        restService = ((MainApplication) getApplication()).getClient();
        order_id = (TextView) findViewById(R.id.order_id);
        product_name = (TextView) findViewById(R.id.product_name);
        price = (TextView) findViewById(R.id.price);
        order_details_image = (ImageView) findViewById(R.id.order_details_image);
        material_appointment_type = (TextView) findViewById(R.id.material_appointment_type);
        material_appointment_date = (TextView) findViewById(R.id.material_appointment_date);
        material_appointment_left = (TextView) findViewById(R.id.material_appointment_left);
        measurement_appointment_type = (TextView) findViewById(R.id.measurement_appointment_type);
        measurement_appointment_date = (TextView) findViewById(R.id.measurement_appointment_date);
        measurement_appointment_left = (TextView) findViewById(R.id.measurement_appointment_left);
        view_details_tracking = (TextView) findViewById(R.id.view_details_tracking);
        total = (TextView) findViewById(R.id.total);
        order_type = (TextView) findViewById(R.id.order_type);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        number = (TextView) findViewById(R.id.number);
        order_date = (TextView) findViewById(R.id.order_date);
        bgLayout = (LinearLayout) findViewById(R.id.bgLayout);
        tracking_status_click = (RelativeLayout) findViewById(R.id.tracking_status_click);
        order_id_txt = (TextView) findViewById(R.id.order_id_txt);
        order_layout = (LinearLayout) findViewById(R.id.order_layout);
        measurement = (Button) findViewById(R.id.measurement);
        customization = (Button) findViewById(R.id.customization);
        tracking_current_status = (TextView) findViewById(R.id.tracking_current_status);
        qty = (TextView) findViewById(R.id.qty_txt);
        material_images = (RecyclerView) findViewById(R.id.material_images);
        reference_images = (RecyclerView) findViewById(R.id.reference_images);
        add_refernce_lable_layout = (LinearLayout) findViewById(R.id.add_refernce_lable_layout);
        add_material_lable_layout = (LinearLayout) findViewById(R.id.add_material_lable_layout);
        add_reference_img_layout = (LinearLayout) findViewById(R.id.add_reference_img_layout);
        add_material_image_layout = (LinearLayout) findViewById(R.id.add_material_image_layout);
        stitching_charges = findViewById(R.id.stitching_charges);
        measurement_charges = findViewById(R.id.measurement_charges);
        urgent_charges = findViewById(R.id.urgent_charges);
        material_delivery_charges = findViewById(R.id.material_delivery_charges);
        delivery_charges = findViewById(R.id.delivery_charges);
        payment_status = findViewById(R.id.payment_status);
        balance = findViewById(R.id.balance);
        amount_paid = findViewById(R.id.amount_paid);
        pay_remaining = findViewById(R.id.pay_remaining);
        update_amount = findViewById(R.id.update_amount);
        paid_layout = findViewById(R.id.paid_layout);
        balance_layout = findViewById(R.id.balance_layout);
        pay_balance_layout = findViewById(R.id.pay_balance_layout);
        material_type = findViewById(R.id.material_type);
        dress_type = findViewById(R.id.dress_type);
        service_type = findViewById(R.id.service_type);
        view_details_layout = findViewById(R.id.view_details_layout);
        material_appointment_layout = findViewById(R.id.material_appointment_layout);
        measurement_appointment_layout = findViewById(R.id.measurement_appointment_layout);
        material_appoinment_label_layout = findViewById(R.id.material_appoinment_label_layout);
        measurement_appoinment_label_layout = findViewById(R.id.measurement_appoinment_label_layout);
        view_details = findViewById(R.id.view_details);
        material_appointment_status = findViewById(R.id.material_appointment_status);
        measurement_appointment_status = findViewById(R.id.measurement_appointment_status);
        material_empty_text_layout = findViewById(R.id.material_empty_text_layout);
        schedule_appoinment_left_layout = findViewById(R.id.schedule_appoinment_left_layout);

//        normal_review_layout,
//                company_material_label_layout,
//                company_review_layout,
//                payment_mzyoon_label_layout, payment_mzyoon_layout;
        normal_review_label_layout = findViewById(R.id.normal_review_label_layout);
        normal_review_layout = findViewById(R.id.normal_review_layout);
        company_material_label_layout = findViewById(R.id.company_material_label_layout);
        company_review_layout = findViewById(R.id.company_review_layout);
        payment_mzyoon_label_layout = findViewById(R.id.payment_mzyoon_label_layout);
        payment_mzyoon_layout = findViewById(R.id.payment_mzyoon_layout);
        material_appointment_view_button_layout = findViewById(R.id.material_appointment_view_button_layout);
        material_appointment_view_text = findViewById(R.id.material_appointment_view_text);
        measurement_appointment_view_button_layout = findViewById(R.id.measurement_appointment_view_button_layout);
        material_appoinment_left_layout = findViewById(R.id.material_appoinment_left_layout);
        measurement_appoinment_left_layout = findViewById(R.id.measurement_appoinment_left_layout);
        measurement_appointment_view_text = findViewById(R.id.measurement_appointment_view_text);


//
//        schedule_appointment_type,schedule_appointment_date,
//                schedule_appointment_left,
//                schedule_appointment_status
        schedule_appointment_type = findViewById(R.id.schedule_appointment_type);
        schedule_appointment_date = findViewById(R.id.schedule_appointment_date);
        schedule_appointment_time = findViewById(R.id.schedule_appointment_time);

        schedule_appointment_left = findViewById(R.id.schedule_appointment_left);
        schedule_appointment_status = findViewById(R.id.schedule_appointment_status);
        schedule_appoinment_label_layout = findViewById(R.id.schedule_appoinment_label_layout);
        schedule_appointment_layout = findViewById(R.id.schedule_appointment_layout);
        schedule_appointment_view_button_layout = findViewById(R.id.schedule_appointment_view_button_layout);
        schedule_appointment_view_text = findViewById(R.id.schedule_appointment_view_text);
        material_empty_text = findViewById(R.id.material_empty_text);
        measurement_empty_text_layout = findViewById(R.id.measurement_empty_text_layout);
        measurement_empty_text = findViewById(R.id.measurement_empty_text);
        schedule_empty_text_layout = findViewById(R.id.schedule_empty_text_layout);
        schedule_empty_text = findViewById(R.id.schedule_empty_text);
        material_appointment_time = findViewById(R.id.material_appointment_time);
        measurement_appointment_time = findViewById(R.id.measurement_appointment_time);

        material_appointment_type_layout = findViewById(R.id.material_appointment_type_layout);
        material_appointment_date_layout = findViewById(R.id.material_appointment_date_layout);
        material_appointment_time_layout = findViewById(R.id.material_appointment_time_layout);
        material_appointment_status_layout = findViewById(R.id.material_appointment_status_layout);


        measurement_appointment_type_layout = findViewById(R.id.measurement_appointment_type_layout);
        measurement_appointment_date_layout = findViewById(R.id.measurement_appointment_date_layout);
        measurement_appointment_status_layout = findViewById(R.id.measurement_appointment_status_layout);
        measurement_appointment_time_layout = findViewById(R.id.measurement_appointment_time_layout);
        customer_rating_text = findViewById(R.id.customer_rating_text);
        material_details_text = findViewById(R.id.material_details_text);
        tailor_total_amount = findViewById(R.id.tailor_total_amount);
        tailor_payment_status = findViewById(R.id.tailor_payment_status);
        tailor_payment_mode = findViewById(R.id.tailor_payment_mode);
        tailor_payment_date = findViewById(R.id.tailor_payment_date);
        tailor_transaction_number = findViewById(R.id.tailor_transaction_number);
        customer_rating_date = findViewById(R.id.customer_rating_date);
        customer_rating_profile_image = findViewById(R.id.customer_rating_profile_image);
        customer_rating_rating_bar = findViewById(R.id.customer_rating_rating_bar);
        customer_rating_name = findViewById(R.id.customer_rating_name);
        company_review_rating_date = findViewById(R.id.company_review_rating_date);
        company_review_rating_name = findViewById(R.id.company_review_rating_name);
        company_review_rating_text = findViewById(R.id.company_review_rating_text);
        company_review_rating_bar = findViewById(R.id.company_review_rating_bar);
        company_review_profile_image = findViewById(R.id.company_review_profile_image);

        stitching_charges_layout = findViewById(R.id.stitching_charges_layout);
        measurement_charges_layout = findViewById(R.id.measurement_charges_layout);
        urgent_charges_layout = findViewById(R.id.urgent_charges_layout);
        material_charges_layout = findViewById(R.id.material_charges_layout);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        dialogManager=new DialogManager();
        if (AppConstants.ORDER_DETAILS.equalsIgnoreCase("ORDER_DETAILS_PENDING")) {

            normal_review_label_layout.setVisibility(View.GONE);
            normal_review_layout.setVisibility(View.GONE);
            company_material_label_layout.setVisibility(View.GONE);
            company_review_layout.setVisibility(View.GONE);
            payment_mzyoon_label_layout.setVisibility(View.GONE);
            payment_mzyoon_layout.setVisibility(View.GONE);

        } else {
            customer_rating_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customer_rating_text.setMovementMethod(new ScrollingMovementMethod());

                }
            });
            tracking_status_click.setVisibility(View.GONE);
        }
        getLanguage();
        getRequestCustomization();
        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
            material_appointment_layout.setVisibility(View.GONE);
            measurement_appointment_layout.setVisibility(View.GONE);
            material_appoinment_label_layout.setVisibility(View.GONE);
            measurement_appoinment_label_layout.setVisibility(View.GONE);
            dialogManager.showProgress(getApplicationContext());
            getOrderDetailsForTailor("Tailor");


        } else {
//            getOrderDetails();
            dialogManager.showProgress(getApplicationContext());

            getOrderDetailsForTailor("Buyer");
        }


        view_details_tracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), OrderTrackingDetailsScreen.class));
            }
        });

        view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (AppConstants.PATTERN_ID.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Choose A Material", Toast.LENGTH_LONG).show();
                } else {
//                    AppConstants.PATTERN_NAME="Order_details";
                    startActivity(new Intent(getApplicationContext(), MaterialDetailsScreenOrderDetails.class));
                }
            }
        });

//        getOrderDetailsForTailor();
        update_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pay_remaining.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter Minimum Amount", Toast.LENGTH_LONG).show();

                } else if (remaining < 0) {

                    Toast.makeText(getApplicationContext(), "Amount Exceeded Than Remaining ", Toast.LENGTH_LONG).show();
                } else if (remaining > 0) {

                    UpdateBalaceAmountByTailor();
                    new AlertDialog.Builder(OrderDetailsActivity.this)
                            .setMessage("Are You Sure Want To Proceed")
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {


                                            UpdateBalaceAmountByTailor();

//


                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Do something after 100ms
//                                                    finish();
//                                                    startActivity(new Intent(getApplicationContext(), SignUpActivity.class));

                                                    getOrderDetailsForTailor("Tailor");

                                                    Toast.makeText(getApplicationContext(), "Amount Updated Sucessfully", Toast.LENGTH_LONG).show();
//                                                    pay_balance_layout.setVisibility(View.VISIBLE);

                                                }
                                            }, 1000);
                                        }
                                    }).setNegativeButton(R.string.no, null).show();

                } else if (remaining == 0) {


                    new AlertDialog.Builder(OrderDetailsActivity.this)
                            .setMessage("Are You Sure Want To Proceed")
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {


                                            UpdateBalaceAmountByTailor();
                                            updatePaymentStatus("1");

//


                                            final Handler handler = new Handler();
                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //Do something after 100ms
//                                                    finish();
//                                                    startActivity(new Intent(getApplicationContext(), SignUpActivity.class));

                                                    getOrderDetailsForTailor("Tailor");
                                                    Toast.makeText(getApplicationContext(), "Amount Updated Sucessfully", Toast.LENGTH_LONG).show();


                                                }
                                            }, 1000);
                                        }
                                    }).setNegativeButton(R.string.no, null).show();


                }
            }
        });

        pay_remaining.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                remaining = Float.parseFloat(AppConstants.BALANCE) - Float.parseFloat(pay_remaining.getText().toString()
                        .equalsIgnoreCase("") ? "0" : pay_remaining.getText().toString());


//                Toast.makeText(getApplicationContext(), "" + remaining, Toast.LENGTH_LONG).show();
//                Integer.parseInt(pay_remaining.getText().toString().
//                        equalsIgnoreCase("") ? "0" : pay_remaining.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        tracking_status_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (getMeasurementDetailsModals.size() <= 0) {
                        Toast.makeText(getApplicationContext(), R.string.update_measurement_to_proceed, Toast.LENGTH_LONG).show();
                    } else if (material_appointment_status_string.trim().equalsIgnoreCase("Rejected") ||
                            material_appointment_status_string.trim().equalsIgnoreCase("Waiting for Buyer Approval") ||
                            material_appointment_status_string.equalsIgnoreCase("Waiting to approve")) {
                        Toast.makeText(getApplicationContext(), "Material Appointment Still Pending", Toast.LENGTH_LONG).show();

                    } else {
                        getTrackingStatus();

                    }
                } catch (Exception e) {

                }
            }
        });
        measurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AppConstants.TAILORMEASUREMENT = "TAILORMEASUREMENT";

                if (getMeasurementDetailsModals.size() == 0) {
                    if (AppConstants.MEASUREMENT_ID_MANUALLY == 2 ||
                            AppConstants.MEASUREMENT_ID_MANUALLY == 3
                    ) {


                        if (genderId.equalsIgnoreCase("1")) {
                            AppConstants.GENDER_NAME = "Male";
                        } else if (genderId.equalsIgnoreCase("2")) {
                            AppConstants.GENDER_NAME = "Female";

                        } else if (genderId.equalsIgnoreCase("3")) {
                            AppConstants.GENDER_NAME = "Boy";

                        } else if (genderId.equalsIgnoreCase("4")) {
                            AppConstants.GENDER_NAME = "Girl";

                        }

                        if (AppConstants.MEASUREMENT_APPOINTMENT_STATUS.equalsIgnoreCase("Approved") ||
                                AppConstants.MEASUREMENT_APPOINTMENT_STATUS.equalsIgnoreCase("Buyer Approved")) {
//                        Toast.makeText(getApplicationContext(), "check", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), MeasurementOneScreen.class));
                        } else {

                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                Toast.makeText(getApplicationContext(), "حجز موعد للمضي قدما", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(getApplicationContext(), "Book An Appointment To Proceed", Toast.LENGTH_LONG).show();
                            }
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Measurement Empty", Toast.LENGTH_LONG).show();

                    }
                } else {
                    startActivity(new Intent(getApplicationContext(), ViewMeasurementActivity.class));
//                    Toast.makeText(getApplicationContext(), "sfs", Toast.LENGTH_LONG).show();

                }
            }
        });

        customization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ViewCustomizationActivity.class));
            }
        });


        material_appointment_view_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AppointmentDetails.class));
            }
        });


        measurement_appointment_view_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AppointmentDetails.class));
            }
        });


        schedule_appointment_view_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ScheduletDetailScreen.class));
            }
        });
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.order_details);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }


    public void getOrderDetailsForTailor(String OrderTypes) {

//        restService.getOrderDetailsResponseForTailor("212").enqueue(new Callback<GetOrderDetailsResponseCheck>() {
        restService.getOrderDetailsResponseForTailor(AppConstants.ORDER_ID, AppConstants.ORDER_TYPE, OrderTypes).
                enqueue(new Callback<GetOrderDetailsResponseCheck>() {

                    @Override
                    public void onResponse(Call<GetOrderDetailsResponseCheck> call, Response<GetOrderDetailsResponseCheck> response) {
//                Toast.makeText(OrderDetailsActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        try {

                            orderDetails = response.body().getResult().getOrderDetail();
                            buyerAddresses = response.body().getResult().getBuyerAddress();
//                        productPrices = response.body().getResult().getProductPrice();
//                        shippingCharges = response.body().getResult().getShippingCharges();
                            statuses = response.body().getResult().getStatus();
                            materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();
                            referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();
                            balances = response.body().getResult().getBalances();

                            materialChargesEntities = response.body().getResult().getMaterialChargesEntities();
                            measurementChargEntities = response.body().getResult().getMeasurementChargesEntities();
                            stitchingChargesEntities = response.body().getResult().getStitchingChargesEntities();
                            urgentStichingChargeEntities = response.body().getResult().getUrgentStichingChargeEntities();

                            getAppoinmentLeftMeasurementEntities = response.body().getResult().getGetAppoinmentLeftMeasurementEntities();

                            getAppoinmentLeftMaterialEntities = response.body().getResult().getGetAppoinmentLeftMaterialEntities();
                            additionalMaterialImageEntities = response.body().getResult().getAdditionalMaterialImageEntities();
                            orderDetailScheduleTypeEntities = response.body().getResult().getGetOrderDetailScheduleTypeEntities();
                            getTailorPaymentStatusEntities = response.body().getResult().getGetTailorPaymentStatusEntities();
                            customerRatingEntities = response.body().getResult().getCustomerRatingEntities();
                            materialReviewEntities = response.body().getResult().getMaterialReview();
                            materialRatingEntities = response.body().getResult().getMaterialRating();
//                    Below List and Function For MaterialAppointment

                            try {

                                if (getTailorPaymentStatusEntities.size() > 0) {
                                    tailor_payment_mode.setText(getTailorPaymentStatusEntities.get(0).getTransactionMode());
                                    tailor_transaction_number.setText(String.valueOf(getTailorPaymentStatusEntities.get(0).getReferenceNo()));
                                    tailor_payment_status.setText(getTailorPaymentStatusEntities.get(0).getStatus());
                                } else {

                                }
                            } catch (Exception e) {

                            }


                            try {
                                if (customerRatingEntities.size() > 0) {
                                    customer_rating_date.setText(customerRatingEntities.get(0).getCreatedDt());
                                    customer_rating_name.setText(customerRatingEntities.get(0).getName());
                                    customer_rating_text.setText(customerRatingEntities.get(0).getReview());
                                    customer_rating_rating_bar.setRating(customerRatingEntities.get(0).getRating());


                                    Glide.with(getApplicationContext())
                                            .load(GlobalData.SERVER_URL + "Images/BuyerImages/" + customerRatingEntities.get(0).getProfilePicture())
                                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                            .into(customer_rating_profile_image);
                                } else {
                                    normal_review_label_layout.setVisibility(View.GONE);
                                    normal_review_layout.setVisibility(View.GONE);

                                }
                            } catch (Exception e) {

                            }


                            try {
                                if (materialReviewEntities.size() > 0) {
                                    company_review_rating_date.setText(materialReviewEntities.get(0).getCreatedOn());
                                    company_review_rating_text.setText(materialReviewEntities.get(0).getReview());
                                    company_review_rating_bar.setRating(materialRatingEntities.get(0).getRating());
                                    company_review_rating_name.setText(customerRatingEntities.get(0).getName());

//                                    customer_rating_rating_bar.setRating(customerRatingEntities.get(0).getRating());
                                    Glide.with(getApplicationContext())
                                            .load(GlobalData.SERVER_URL + "Images/BuyerImages/" + customerRatingEntities.get(0).getProfilePicture())
                                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                            .into(company_review_profile_image);
                                } else {
                                    company_material_label_layout.setVisibility(View.GONE);
                                    company_review_layout.setVisibility(View.GONE);
                                }
                            } catch (Exception e) {

                            }

                            if (getAppoinmentLeftMeasurementEntities.size() <= 0) {
                                material_appointment_layout.setVisibility(View.GONE);
                                material_appoinment_label_layout.setVisibility(View.GONE);


                            }


                            if (getAppoinmentLeftMeasurementEntities.size() <= 0 &&
                                    orderDetails.get(0).getMaterialTypeId().equalsIgnoreCase("1")) {
                                material_appointment_layout.setVisibility(View.VISIBLE);
                                material_appoinment_label_layout.setVisibility(View.VISIBLE);


                                material_appointment_date_layout.setVisibility(View.GONE);
                                material_appointment_time_layout.setVisibility(View.GONE);
                                material_appointment_status_layout.setVisibility(View.GONE);
                                material_appoinment_left_layout.setVisibility(View.GONE);
                                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                    material_appointment_type.setText(orderDetails.get(0).getMaterialTypeInArabic());

                                } else {
                                    material_appointment_type.setText(orderDetails.get(0).getMaterialType());

                                }
                                material_empty_text_layout.setVisibility(View.VISIBLE);
                                material_empty_text.setText(R.string.appointment_details_by_yours);
                                material_appointment_view_text.setText(R.string.create_appointment);


                            }

                            //Waiting for Buyer  To Give Appointment
                            if (getAppoinmentLeftMeasurementEntities.size() <= 0 &&
                                    orderDetails.get(0).getMaterialTypeId().equalsIgnoreCase("2")) {
                                material_appointment_layout.setVisibility(View.VISIBLE);
                                material_appoinment_label_layout.setVisibility(View.VISIBLE);
                                material_appointment_view_button_layout.setVisibility(View.GONE);
//                        material_appointment_view_text.setText("Buyer Need To Appointment");
                                material_empty_text_layout.setVisibility(View.VISIBLE);
                                material_empty_text.setText(R.string.appointment_details_by_buyer);

                            }

//                    try {
//                        if (getAppoinmentLeftMeasurementEntities.size() <= 0 &&
//                                orderDetails.get(0).getMaterialTypeId().equalsIgnoreCase("1") &&
//                                getAppoinmentLeftMeasurementEntities.get(0).isMaterialAppoinmentStatus() == true) {
//                            material_appointment_layout.setVisibility(View.VISIBLE);
//                            material_appoinment_label_layout.setVisibility(View.VISIBLE);
//                            material_appointment_view_text.setText("View Appointment");
//
//                        }
//                    } catch (Exception e) {
//
//                    }


//                    Below List and Function For Measurement


                            if (getAppoinmentLeftMaterialEntities.size() <= 0) {
                                measurement_appointment_layout.setVisibility(View.GONE);
                                measurement_appoinment_label_layout.setVisibility(View.GONE);

                            }


                            if (getAppoinmentLeftMaterialEntities.size() <= 0 &&
                                    orderDetails.get(0).getMeasurementType() == 2) {
                                measurement_appointment_layout.setVisibility(View.VISIBLE);
                                measurement_appoinment_label_layout.setVisibility(View.VISIBLE);
//                        measurement_appointment_view_text.setText("Create Appointment");


                                measurement_appointment_date_layout.setVisibility(View.GONE);
                                measurement_appointment_time_layout.setVisibility(View.GONE);
                                measurement_appointment_status_layout.setVisibility(View.GONE);
                                measurement_appoinment_left_layout.setVisibility(View.GONE);
                                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                    measurement_appointment_type.setText(orderDetails.get(0).getMeasurementInArabic());

                                } else {
                                    measurement_appointment_type.setText(orderDetails.get(0).getMeasurementInEnglish());

                                }
                                measurement_empty_text_layout.setVisibility(View.VISIBLE);
                                measurement_empty_text.setText(R.string.appointment_details_by_yours);

                                measurement_appointment_view_text.setText(R.string.create_appointment);


                            }

//                    Below List and Function For MeasurementAppointment

                            if (getAppoinmentLeftMaterialEntities.size() <= 0 &&
                                    orderDetails.get(0).getMeasurementType() == 3) {
                                measurement_appointment_layout.setVisibility(View.VISIBLE);
                                measurement_appoinment_label_layout.setVisibility(View.VISIBLE);
//                        measurement_appointment_view_text.setText("Buyer Need To Appointment");


                                measurement_appointment_view_button_layout.setVisibility(View.GONE);
//                        material_appointment_view_text.setText("Buyer Need To Appointment");
                                measurement_empty_text_layout.setVisibility(View.VISIBLE);
                                measurement_empty_text.setText(R.string.appointment_details_by_buyer);


                            }

//                    try {
//                        if (getAppoinmentLeftMaterialEntities.size() <= 0 &&
//                                orderDetails.get(0).getMeasurementType() == 2 &&
//                                getAppoinmentLeftMaterialEntities.get(0).isMaasuremntAppoinmentStatus() == true) {
//                            measurement_appointment_layout.setVisibility(View.VISIBLE);
//                            measurement_appoinment_label_layout.setVisibility(View.VISIBLE);
//                            measurement_appointment_view_text.setText("View Appointment");
//
//                        }
//                    } catch (Exception e) {
//
//                    }


//                    Below List and Function For Schedule


                            if (orderDetailScheduleTypeEntities.size() <= 0) {
                                schedule_appointment_layout.setVisibility(View.GONE);
                                schedule_appoinment_label_layout.setVisibility(View.GONE);
                            }


                            if (orderDetailScheduleTypeEntities.size() <= 0 &&
                                    orderDetails.get(0).getMaterialTypeId().equalsIgnoreCase("3")
                                    && orderDetails.get(0).getMeasurementType() == 1
                                    && orderDetails.get(0).getServiceType().equalsIgnoreCase("Schedule")) {
//                        schedule_appointment_layout.setVisibility(View.VISIBLE);
//                        schedule_appoinment_label_layout.setVisibility(View.VISIBLE);
//                        schedule_appointment_view_text.setText("Buyer Need To Appointment");

                                schedule_appointment_layout.setVisibility(View.VISIBLE);
                                schedule_appoinment_label_layout.setVisibility(View.VISIBLE);
                                schedule_appointment_view_button_layout.setVisibility(View.GONE);
//                        material_appointment_view_text.setText("Buyer Need To Appointment");
                                schedule_empty_text_layout.setVisibility(View.VISIBLE);
                                schedule_empty_text.setText("Appointment will be visible once the " +
                                        "Buyer updates the appointment details");


                                if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
                                    schedule_appointment_layout.setVisibility(View.GONE);
                                    schedule_appoinment_label_layout.setVisibility(View.GONE);
//                            schedule_appointment_view_text.setText("Buyer Need To  Appointment");

                                }

                            }


                            if (referenceImagesSkillUpdateGetEntities.size() <= 0) {
                                referenceImagesSkillUpdateGetEntities = new ArrayList<>();
                                add_refernce_lable_layout.setVisibility(View.GONE);
                                add_reference_img_layout.setVisibility(View.GONE);

                            }


                            if (orderDetails.get(0).getMaterialType().equalsIgnoreCase("Companies-Material")) {
                                additionalImages();
                                referenceImage();
                            } else {
                                view_details_layout.setVisibility(View.GONE);
                                setImageList();
                                referenceImage();
                            }

                            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                order_id.setText("#" + String.valueOf(orderDetails.get(0).getOrderId()));
                                product_name.setText(orderDetails.get(0).getNameInArabic());
                                service_type.setText(orderDetails.get(0).getServiceTypeInArabic());


                                stitching_charges.setText(String.valueOf(stitchingChargesEntities.get(0).getStichingAndMaterialCharge() * orderDetails.get(0).getQty()));
                                urgent_charges.setText(String.valueOf(urgentStichingChargeEntities.get(0).getUrgentStichingCharges()));
                                measurement_charges.setText(String.valueOf(measurementChargEntities.get(0).getMeasurementCharges()));
////                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialDeliveryCharges()));
                                material_delivery_charges.setText(String.valueOf(materialChargesEntities.get(0).getMaterialCharges()));
                                ftotal = stitchingChargesEntities.get(0).getStichingAndMaterialCharge() +
                                        urgentStichingChargeEntities.get(0).getUrgentStichingCharges() +
                                        measurementChargEntities.get(0).getMeasurementCharges() +
                                        materialChargesEntities.get(0).getMaterialCharges();

                                float add = ftotal;
                                total.setText(String.valueOf(add));
                                price.setText(String.valueOf(ftotal));
                                order_date.setText(orderDetails.get(0).getOrderDt());
                                dress_type.setText(orderDetails.get(0).getDressTypeName());

                                if (urgentStichingChargeEntities.get(0).getUrgentStichingCharges() == 0.0) {

                                    urgent_charges_layout.setVisibility(View.GONE);

                                }

                                if (stitchingChargesEntities.get(0).getStichingAndMaterialCharge() == 0.0) {
                                    stitching_charges_layout.setVisibility(View.GONE);
                                }
                                if (measurementChargEntities.get(0).getMeasurementCharges() == 0.0) {
                                    measurement_charges_layout.setVisibility(View.GONE);
                                }
                                if (materialChargesEntities.get(0).getMaterialCharges() == 0.0) {
                                    material_charges_layout.setVisibility(View.GONE);
                                }

                                try {
                                    Glide.with(getApplicationContext())
                                            .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
                                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                            .into(order_details_image);

                                } catch (Exception ex) {
                                    Log.e(AppConstants.TAG, ex.getMessage());
                                }
                                tracking_current_status.setText("الحالة الحالية:" + statuses.get(0).getStatus());
                                payment_status.setText((orderDetails.get(0).getPaymentStatus()));
                                qty.setText(String.valueOf("كمية:" + orderDetails.get(0).getQty()));

                                order_type.setText(orderDetails.get(0).getServiceTypeInArabic());

                                material_type.setText(orderDetails.get(0).getMaterialTypeInArabic());
                                name.setText("  " + buyerAddresses.get(0).getFirstName());
                                address.setText("  " + buyerAddresses.get(0).getFloor() + "\n" + "  " + buyerAddresses.get(0).getArea() + "\n" +
                                        "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
                                number.setText("  " + buyerAddresses.get(0).getPhoneNo());

                                material_details_text.setText("تفاصيل المواد" + "(" + orderDetails.get(0).
                                        getMaterialTypeInArabic() + ")");
                                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
                                AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
                                AppConstants.PATTERN_ID = String.valueOf(orderDetails.get(0).getPatternId());
                                genderId = String.valueOf(orderDetails.get(0).getGender());
                                dialogManager.hideProgress();
                            } else {
                                order_id.setText("#" + String.valueOf(orderDetails.get(0).getOrderId()));
                                product_name.setText(orderDetails.get(0).getProductName());
                                service_type.setText(orderDetails.get(0).getServiceType());

//                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
//                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
//                        shipping_handling.setText("0");


                                stitching_charges.setText(String.valueOf(stitchingChargesEntities.get(0).getStichingAndMaterialCharge() * orderDetails.get(0).getQty()));
                                urgent_charges.setText(String.valueOf(urgentStichingChargeEntities.get(0).getUrgentStichingCharges()));
                                measurement_charges.setText(String.valueOf(measurementChargEntities.get(0).getMeasurementCharges()));
////                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialDeliveryCharges()));
                                material_delivery_charges.setText(String.valueOf(materialChargesEntities.get(0).getMaterialCharges()));


                                ftotal = stitchingChargesEntities.get(0).getStichingAndMaterialCharge() * orderDetails.get(0).getQty() +
                                        urgentStichingChargeEntities.get(0).getUrgentStichingCharges() +
                                        measurementChargEntities.get(0).getMeasurementCharges() +
                                        materialChargesEntities.get(0).getMaterialCharges();
                                float add = ftotal;

                                total.setText(String.valueOf(add));
                                price.setText(String.valueOf(ftotal));
//                    price.setText(String.valueOf(ftotal));
                                order_date.setText(orderDetails.get(0).getOrderDt());
                                dress_type.setText(orderDetails.get(0).getDressTypeName());
                                if (urgentStichingChargeEntities.get(0).getUrgentStichingCharges() == 0.0) {

                                    urgent_charges_layout.setVisibility(View.GONE);

                                }

                                if (stitchingChargesEntities.get(0).getStichingAndMaterialCharge() == 0.0) {
                                    stitching_charges_layout.setVisibility(View.GONE);
                                }
                                if (measurementChargEntities.get(0).getMeasurementCharges() == 0.0) {
                                    measurement_charges_layout.setVisibility(View.GONE);
                                }
                                if (materialChargesEntities.get(0).getMaterialCharges() == 0.0) {
                                    material_charges_layout.setVisibility(View.GONE);
                                }
                                try {
                                    Glide.with(getApplicationContext())
                                            .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
                                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                            .into(order_details_image);

                                } catch (Exception ex) {
                                    Log.e(AppConstants.TAG, ex.getMessage());
                                }
                                tracking_current_status.setText("Current Status: " + statuses.get(0).getStatus());
                                payment_status.setText((orderDetails.get(0).getPaymentStatus()));
                                qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));

                                order_type.setText(orderDetails.get(0).getServiceType());

                                material_type.setText(orderDetails.get(0).getMaterialType());
                                name.setText("  " + buyerAddresses.get(0).getFirstName());
                                address.setText("  " + buyerAddresses.get(0).getFloor() + "\n" + "  " + buyerAddresses.get(0).getArea() + "\n" +
                                        "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
                                material_details_text.setText("Material Details" + "(" + orderDetails.get(0).
                                        getMaterialType() + ")");


                                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
                                AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
                                AppConstants.PATTERN_ID = String.valueOf(orderDetails.get(0).getPatternId());
                                genderId = String.valueOf(orderDetails.get(0).getGender());
                                dialogManager.hideProgress();

                            }

                            try {
                                AppConstants.CHECK_BOOK_APPOINTMENT = "BookAnAppointment";
                                AppConstants.APPROVED_TAILOR_ID = sharedPreferences.getString("TailorId", "");
                                AppConstants.REQUEST_LIST_ID = String.valueOf(orderDetails.get(0).getOrderId());
                                AppConstants.APPOINTMENT_LIST_ID = String.valueOf(orderDetails.get(0).getOrderId());
                                AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
                            } catch (Exception e) {

                            }


                            if (getAppoinmentLeftMaterialEntities.size() > 0) {


                                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                    measurement_appointment_type.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInArabic());
                                    measurement_appointment_left.setText("" + getAppoinmentLeftMaterialEntities.get(0).
                                            getAppointmentLeftMeterialCount());
                                    measurement_appointment_status.setText(getAppoinmentLeftMaterialEntities.get(0).getStatus());
                                    AppConstants.MEASUREMENT_APPOINTMENT_STATUS = getAppoinmentLeftMaterialEntities.get(0).getStatus();
                                    String[] separated = getAppoinmentLeftMaterialEntities.get(0).getOrderDt().split("To");
//                        measurement_appointment_date.setText(separated[0] + "\n" + separated[1]);
                                    measurement_appointment_date.setText(getAppoinmentLeftMaterialEntities.get(0).getOrderDt());
                                    measurement_appointment_time.setText(getAppoinmentLeftMaterialEntities.get(0).getAppointmentTime());


                                } else {
                                    measurement_appointment_type.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInEnglish());
                                    measurement_appointment_left.setText("" + getAppoinmentLeftMaterialEntities.get(0).
                                            getAppointmentLeftMeterialCount());
                                    measurement_appointment_status.setText(getAppoinmentLeftMaterialEntities.get(0).getStatus());
                                    AppConstants.MEASUREMENT_APPOINTMENT_STATUS = getAppoinmentLeftMaterialEntities.get(0).getStatus();
                                    String[] separated = getAppoinmentLeftMaterialEntities.get(0).getOrderDt().split("To");
//                        measurement_appointment_date.setText(separated[0] + "\n" + separated[1]);
                                    measurement_appointment_date.setText(getAppoinmentLeftMaterialEntities.get(0).getOrderDt());
                                    measurement_appointment_time.setText(getAppoinmentLeftMaterialEntities.get(0).getAppointmentTime());
                                }


                                if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Waiting to approve")) {
                                    measurement_appointment_view_text.setText(R.string.approve_appointment);


                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Waiting for Buyer Approval")) {
                                    measurement_appointment_view_button_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Rejected") &&
                                        getAppoinmentLeftMaterialEntities.get(0).getAppointmentLeftMeterialCount() > 0) {
                                    measurement_appointment_view_button_layout.setVisibility(View.GONE);

                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Rejected") &&
                                        getAppoinmentLeftMaterialEntities.get(0).getAppointmentLeftMeterialCount() == 0) {
                                    measurement_appointment_view_button_layout.setVisibility(View.GONE);
                                    measurement_appoinment_left_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Buyer Rejected") &&
                                        getAppoinmentLeftMaterialEntities.get(0).getAppointmentLeftMeterialCount() > 0) {
                                    measurement_appointment_view_text.setText(R.string.create_appointment);
                                    measurement_appoinment_left_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Buyer Rejected") &&
                                        getAppoinmentLeftMaterialEntities.get(0).getAppointmentLeftMeterialCount() == 0) {
                                    measurement_appointment_view_button_layout.setVisibility(View.GONE);
                                    measurement_appoinment_left_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Approved")) {
                                    measurement_appointment_view_button_layout.setVisibility(View.GONE);
                                    measurement_appoinment_left_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMaterialEntities.get(0).getStatus().equalsIgnoreCase("Buyer Approved")) {
                                    measurement_appointment_view_button_layout.setVisibility(View.GONE);
                                    measurement_appoinment_left_layout.setVisibility(View.GONE);


                                } else {

                                }


                            }

                            if (getAppoinmentLeftMeasurementEntities.size() > 0) {
                                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                    material_appointment_type.setText(getAppoinmentLeftMeasurementEntities.get(0).getHeaderInArabic());
                                    material_appointment_left.setText("" + getAppoinmentLeftMeasurementEntities.get(0).
                                            getAppointmentLeftMeasurementCount());

                                    material_appointment_status.setText(getAppoinmentLeftMeasurementEntities.get(0).getStatus());

                                    String[] separated1 = getAppoinmentLeftMeasurementEntities.get(0).getOrderDt().split("T");
//                        material_appointment_date.setText(separated1[0] + "\n" + separated1[1]);
                                    material_appointment_date.setText(getAppoinmentLeftMeasurementEntities.get(0).getOrderDt());
                                    material_appointment_time.setText(getAppoinmentLeftMeasurementEntities.get(0).getAppointmentTime());


                                } else {
                                    material_appointment_type.setText(getAppoinmentLeftMeasurementEntities.get(0).getHeaderInEnglish());
                                    material_appointment_left.setText("" + getAppoinmentLeftMeasurementEntities.get(0).
                                            getAppointmentLeftMeasurementCount());

                                    material_appointment_status.setText(getAppoinmentLeftMeasurementEntities.get(0).getStatus());

                                    String[] separated1 = getAppoinmentLeftMeasurementEntities.get(0).getOrderDt().split("T");
//                        material_appointment_date.setText(separated1[0] + "\n" + separated1[1]);
                                    material_appointment_date.setText(getAppoinmentLeftMeasurementEntities.get(0).getOrderDt());
                                    material_appointment_time.setText(getAppoinmentLeftMeasurementEntities.get(0).getAppointmentTime());
                                    material_appointment_status_string = getAppoinmentLeftMeasurementEntities.get(0).getStatus();
                                }

                                if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Waiting to approve")) {
                                    material_appointment_view_text.setText(R.string.approve_appointment);


                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Waiting for Buyer Approval")) {
                                    material_appointment_view_button_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Rejected") &&
                                        getAppoinmentLeftMeasurementEntities.get(0).getAppointmentLeftMeasurementCount() > 0) {
                                    material_appointment_view_button_layout.setVisibility(View.GONE);

                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Rejected") &&
                                        getAppoinmentLeftMeasurementEntities.get(0).getAppointmentLeftMeasurementCount() == 0) {
                                    material_appointment_view_button_layout.setVisibility(View.GONE);
                                    material_appoinment_left_layout.setVisibility(View.GONE);


                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Buyer Rejected") &&
                                        getAppoinmentLeftMeasurementEntities.get(0).getAppointmentLeftMeasurementCount() > 0) {
                                    material_appointment_view_text.setText(R.string.create_appointment);

                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Buyer Rejected") &&
                                        getAppoinmentLeftMeasurementEntities.get(0).getAppointmentLeftMeasurementCount() == 0) {
                                    material_appointment_view_button_layout.setVisibility(View.GONE);
                                    material_appoinment_left_layout.setVisibility(View.GONE);

                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Approved")) {
                                    material_appointment_view_button_layout.setVisibility(View.GONE);
                                    material_appoinment_left_layout.setVisibility(View.GONE);

                                } else if (getAppoinmentLeftMeasurementEntities.get(0).getStatus().equalsIgnoreCase("Buyer Approved")) {
                                    material_appointment_view_button_layout.setVisibility(View.GONE);
                                    material_appoinment_left_layout.setVisibility(View.GONE);


                                } else {

                                }


                            }


                            if (orderDetailScheduleTypeEntities.size() > 0) {


                                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                    schedule_appointment_view_button_layout.setVisibility(View.INVISIBLE);

                                    schedule_appointment_type.setText(orderDetailScheduleTypeEntities.get(0).getDeliveryTypeInArabic());
                                    schedule_appointment_left.setText("" + orderDetailScheduleTypeEntities.get(0).
                                            getAppointmentLeftScheduleCount());

                                    schedule_appointment_status.setText(orderDetailScheduleTypeEntities.get(0).getStatus());
                                    schedule_appointment_time.setText(orderDetailScheduleTypeEntities.get(0).getAppointmentTime());
                                    schedule_appointment_date.setText(orderDetailScheduleTypeEntities.get(0).getOrderDt());
                                    AppConstants.MEASUREMENT_APPOINTMENT_STATUS = orderDetailScheduleTypeEntities.get(0).getStatus();

                                } else {
                                    schedule_appointment_view_button_layout.setVisibility(View.INVISIBLE);

                                    schedule_appointment_type.setText(orderDetailScheduleTypeEntities.get(0).getDeliveryTypeInEnglish());
                                    schedule_appointment_left.setText("" + orderDetailScheduleTypeEntities.get(0).
                                            getAppointmentLeftScheduleCount());

                                    schedule_appointment_status.setText(orderDetailScheduleTypeEntities.get(0).getStatus());
                                    schedule_appointment_time.setText(orderDetailScheduleTypeEntities.get(0).getAppointmentTime());

//                                    String[] separated1 = orderDetailScheduleTypeEntities.get(0).getOrderDt().split("T");
                                    schedule_appointment_date.setText(orderDetailScheduleTypeEntities.get(0).getOrderDt());
//                                    schedule_appointment_date.setText(orderDetailScheduleTypeEntities.get(0).getOrderDt());
                                    AppConstants.MEASUREMENT_APPOINTMENT_STATUS = orderDetailScheduleTypeEntities.get(0).getStatus();


                                }

                                try {

                                } catch (Exception e) {

                                }

                            }
                            if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {

                                AppConstants.BALANCE = balances.get(0).getBalance().toString();
                                bal = ftotal - Float.parseFloat(AppConstants.BALANCE);
                                balance.setText(balances.get(0).getBalance().toString());
                                amount_paid.setText(String.valueOf(bal));
//                            Toast.makeText(getApplicationContext(), AppConstants.BALANCE, Toast.LENGTH_LONG).show();


                                if (Float.parseFloat(AppConstants.BALANCE) > 0) {

                                    paid_layout.setVisibility(View.VISIBLE);
                                    balance_layout.setVisibility(View.VISIBLE);
                                    pay_balance_layout.setVisibility(View.VISIBLE);

                                }

                                if (Float.parseFloat(AppConstants.BALANCE) == 0) {
                                    pay_balance_layout.setVisibility(View.GONE);
                                }

                            }
//                        float bal = Float.parseFloat(AppConstants.BALANCE);
//                        float remaining = ftotal - bal;
//                        tax.setText(productPrices.get(0).getTax());
//
//
//                        appointment_charges.setText(String.valueOf(productPrices.get(0).getAppoinment()));
//                        total.setText(String.valueOf(productPrices.get(0).getTotal()));


                        } catch (Exception e) {

                        }


                    }

                    @Override
                    public void onFailure(Call<GetOrderDetailsResponseCheck> call, Throwable t) {
                        insertError("OrderDetailsActivity", "getTrackingDetailsResponse()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

                    }
                });
    }


//    public void getOrderDetails(String OrderTypes) {
//
////        restService.getOrderDetailsResponseForTailor("212").enqueue(new Callback<GetOrderDetailsResponseCheck>() {
//        restService.getOrderDetailsResponse(AppConstants.ORDER_ID, AppConstants.ORDER_TYPE, OrderTypes).enqueue(new Callback<GetOrderDetailsResponseCheckNew>() {
//
//            @Override
//            public void onResponse(Call<GetOrderDetailsResponseCheckNew> call, Response<GetOrderDetailsResponseCheckNew> response) {
//                Toast.makeText(OrderDetailsActivity.this, "Success", Toast.LENGTH_SHORT).show();
////                try {
////
////                    orderDetails = response.body().getResult().getOrderDetail();
////                    buyerAddresses = response.body().getResult().getBuyerAddress();
//////                        productPrices = response.body().getResult().getProductPrice();
//////                        shippingCharges = response.body().getResult().getShippingCharges();
////                    statuses = response.body().getResult().getStatus();
////                    materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();
////                    referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();
////                    balances = response.body().getResult().getBalances();
////                    getAppoinmentLeftMeasurementEntities = response.body().getResult().getGetAppoinmentLeftMeasurementEntities();
////
////                    getAppoinmentLeftMaterialEntities = response.body().getResult().getGetAppoinmentLeftMaterialEntities();
////
//////                        if (orderDetails == null) {
//////                            orderDetails = new ArrayList<>();
//////                        }
//////                        if (buyerAddresses == null) {
//////                            buyerAddresses = new ArrayList<>();
//////                        }
//////                        if (statuses == null) {
//////                            statuses = new ArrayList<>();
//////                        }
//////                        if (materialImagesSkillUpdateGetEntities.size()<=0) {
//////                            materialImagesSkillUpdateGetEntities = new ArrayList<>();
//////
//////                            add_material_lable_layout.setVisibility(View.GONE);
//////                            add_material_image_layout.setVisibility(View.GONE);
//////
//////                        }
//////                        if (referenceImagesSkillUpdateGetEntities.size()<=0) {
//////                            referenceImagesSkillUpdateGetEntities = new ArrayList<>();
//////                            add_refernce_lable_layout.setVisibility(View.GONE);
//////                            add_reference_img_layout.setVisibility(View.GONE);
//////
//////                        }
//////
//////                        if (referenceImagesSkillUpdateGetEntities.size() > 0) {
//////                            add_refernce_lable_layout.setVisibility(View.VISIBLE);
//////                            add_reference_img_layout.setVisibility(View.VISIBLE);
//////
//////                        }
//////
//////                        if (materialImagesSkillUpdateGetEntities.size() > 0) {
//////
//////                            add_material_lable_layout.setVisibility(View.VISIBLE);
//////                            add_material_image_layout.setVisibility(View.VISIBLE);
//////
//////                        }
////
////
////                    setImageList();
////                    order_id.setText("#" + String.valueOf(orderDetails.get(0).getOrderId()));
////                    product_name.setText(orderDetails.get(0).getProductName());
//////                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
//////                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
//////                        shipping_handling.setText("0");
////
////
////                    stitching_charges.setText(String.valueOf(response.body().getResult().getStichingAndMaterialCharge()));
////                    urgent_charges.setText(String.valueOf(response.body().getResult().getUrgentStichingCharges()));
////                    measurement_charges.setText(String.valueOf(response.body().getResult().getMeasurementCharges()));
//////                        material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialDeliveryCharges()));
////                    material_delivery_charges.setText(String.valueOf(response.body().getResult().getMaterialCharges()));
////                    ftotal = response.body().getResult().getStichingAndMaterialCharge() +
////                            response.body().getResult().getUrgentStichingCharges() +
////                            response.body().getResult().getMeasurementCharges() +
////                            response.body().getResult().getMaterialCharges();
////                    total.setText(String.valueOf(ftotal));
////                    price.setText(String.valueOf(ftotal));
////                    order_date.setText(orderDetails.get(0).getOrderDt());
////                    dress_type.setText(orderDetails.get(0).getDressTypeName());
////
////                    try {
////                        Glide.with(getApplicationContext())
////                                .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
////                                .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
////                                .into(order_details_image);
////
////                    } catch (Exception ex) {
////                        Log.e(AppConstants.TAG, ex.getMessage());
////                    }
////                    tracking_current_status.setText("Tracking Current Status: " + statuses.get(0).getStatus());
////                    payment_status.setText((orderDetails.get(0).getPaymentStatus()));
////                    qty.setText(String.valueOf("Quantity:" + orderDetails.get(0).getQty()));
////
////                    order_type.setText(orderDetails.get(0).getServiceType());
////
////                    material_type.setText(orderDetails.get(0).getServiceType());
////                    name.setText(buyerAddresses.get(0).getFirstName());
////                    address.setText("  " + buyerAddresses.get(0).getFloor() + "\n" + "  " + buyerAddresses.get(0).getArea() + "\n" +
////                            "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
////                    number.setText(buyerAddresses.get(0).getPhoneNo());
////
////
////                    AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
////                    AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
////                    genderId = String.valueOf(orderDetails.get(0).getGender());
////
//////                        material_appointment_type = (TextView) findViewById(R.id.material_appointment_type);
//////                        material_appointment_date = (TextView) findViewById(R.id.material_appointment_date);
//////                        material_appointment_left = (TextView) findViewById(R.id.material_appointment_left);
//////                        measurement_appointment_type = (TextView) findViewById(R.id.measurement_appointment_type);
//////                        measurement_appointment_date = (TextView) findViewById(R.id.measurement_appointment_date);
//////                        measurement_appointment_left = (TextView) findViewById(R.id.measurement_appointment_left);
////
////
////                    measurement_appointment_type.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInEnglish());
////                    measurement_appointment_left.setText("" + getAppoinmentLeftMaterialEntities.get(0).
////                            getAppointmentLeftMaterialCount());
////
////
////                    measurement_appointment_date.setText(getAppoinmentLeftMaterialEntities.get(0).getMeasurementInEnglish());
////
////                    material_appointment_type.setText(getAppoinmentLeftMeasurementEntities.get(0).getHeaderInEnglish());
////                    material_appointment_left.setText("" + getAppoinmentLeftMeasurementEntities.get(0).
////                            getAppointmentLeftMeasurementCount());
////
////
////                    material_appointment_date.setText(getAppoinmentLeftMeasurementEntities.get(0).getHeaderInEnglish());
////                    if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
////
////                        AppConstants.BALANCE = balances.get(0).getBalance().toString();
////                        bal = ftotal - Float.parseFloat(AppConstants.BALANCE);
////                        balance.setText(balances.get(0).getBalance().toString());
////                        amount_paid.setText(String.valueOf(bal));
//////                            Toast.makeText(getApplicationContext(), AppConstants.BALANCE, Toast.LENGTH_LONG).show();
////
////
////                        if (Float.parseFloat(AppConstants.BALANCE) > 0) {
////
////                            paid_layout.setVisibility(View.VISIBLE);
////                            balance_layout.setVisibility(View.VISIBLE);
////                            pay_balance_layout.setVisibility(View.VISIBLE);
////
////                        }
////
////                        if (Float.parseFloat(AppConstants.BALANCE) == 0) {
////                            pay_balance_layout.setVisibility(View.GONE);
////                        }
////
////                    }
//////                        float bal = Float.parseFloat(AppConstants.BALANCE);
//////                        float remaining = ftotal - bal;
//////                        tax.setText(productPrices.get(0).getTax());
//////
//////
//////                        appointment_charges.setText(String.valueOf(productPrices.get(0).getAppoinment()));
//////                        total.setText(String.valueOf(productPrices.get(0).getTotal()));
////
////
////                } catch (Exception e) {
//
////                }
//
//
//            }
//
//            @Override
//            public void onFailure(Call<GetOrderDetailsResponseCheckNew> call, Throwable t) {
//
//            }
//        });
//    }


    public void showCoutrySelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
        TextView myMsg = new TextView(this);
        myMsg.setText(R.string.update_order_status);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextColor(Color.BLACK);
        //set custom title
//        builder.setCustomTitle(myMsg);
        dialog = new AlertDialog.Builder(this)
                .setCustomTitle(myMsg)
                .setView(view)
                .create();
        trackingStatusAdapter = new TrackingStatusCheckListAdapter(getApplicationContext(),
                dialog, restService, getTrackingByOrderModals, getTrackingByOrderModal);
        countryList.setAdapter(trackingStatusAdapter);
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
                    getOrderDetailsForTailor("Tailor");
                } else {

//                    getOrderdetailsForBuyer();
                    getOrderDetailsForTailor("Buyer");
                }
            }
        });

    }


    public void getTrackingStatus() {


        restService.getTrackingDetailsResponse(AppConstants.ORDER_ID).enqueue(new Callback<GetTrackingDetailsResponse>() {
            @Override
            public void onResponse(Call<GetTrackingDetailsResponse> call, Response<GetTrackingDetailsResponse> response) {


                try {

//                    getTrackingDetailsModals = response.body().getResult().getTrackingDetails();
                    getTrackingByOrderModals = response.body().getResult().getGetTrackingByOrder();
                    setCountrySelectionList();
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Call<GetTrackingDetailsResponse> call, Throwable t) {
                insertError("OrderDetailsActivity", "getTrackingDetailsResponse()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }


    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < getTrackingByOrderModals.size(); i++) {
            listSpinner.add(getTrackingByOrderModals.get(i).getStatus());
        }

        showCoutrySelectionList();


        mHeaderLeftBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }


    }


    public void getOrderdetailsForBuyer() {

        restService.getOrderDetailsResponseBuyer(AppConstants.ORDER_ID).enqueue(new Callback<BuyerOrderDetailResponse>() {
            @Override
            public void onResponse(Call<BuyerOrderDetailResponse> call, Response<BuyerOrderDetailResponse> response) {

                if (response.isSuccessful()) {
                    try {
                        orderDetails = response.body().getResult().getOrderDetail();
                        buyerAddresses = response.body().getResult().getBuyerAddress();
                        productPrices = response.body().getResult().getProductPrice();
                        shippingCharges = response.body().getResult().getShippingCharges();
                        statuses = response.body().getResult().getStatus();
                        materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();
                        referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();


                        if (referenceImagesSkillUpdateGetEntities.size() > 0) {
                            add_refernce_lable_layout.setVisibility(View.VISIBLE);
                            add_reference_img_layout.setVisibility(View.VISIBLE);

                        }

                        if (materialImagesSkillUpdateGetEntities.size() > 0) {

                            add_material_lable_layout.setVisibility(View.VISIBLE);
                            add_material_image_layout.setVisibility(View.VISIBLE);

                        }

                        setImageList();

                        order_id.setText(String.valueOf(orderDetails.get(0).getOrderId()));
                        product_name.setText(orderDetails.get(0).getProductName());
                        price.setText(String.valueOf(productPrices.get(0).getPrice()));
                        sub_total.setText(String.valueOf(productPrices.get(0).getTotal()));
                        shipping_handling.setText("0");
                        tracking_current_status.setText("Tracking Current Status: " + statuses.get(0).getStatus());
                        tax.setText(productPrices.get(0).getTax());
                        qty.setText(String.valueOf(orderDetails.get(0).getQty()));

                        appointment_charges.setText(String.valueOf(productPrices.get(0).getAppoinment()));


                        total.setText(String.valueOf(productPrices.get(0).getTotal() * orderDetails.get(0).getQty()));
                        order_type.setText(orderDetails.get(0).getServiceType());


                        name.setText(buyerAddresses.get(0).getFirstName());
                        address.setText("  " + buyerAddresses.get(0).getFloor() + "," +
                                buyerAddresses.get(0).getArea() + "\n" +
                                "  " + buyerAddresses.get(0).getStateName() + "\n" + "  " + buyerAddresses.get(0).getCountryName());
                        number.setText(buyerAddresses.get(0).getPhoneNo());


                        order_date.setText(orderDetails.get(0).getOrderDt());
                        AppConstants.SUB_DRESS_TYPE_ID = String.valueOf(orderDetails.get(0).getDressType());
                        AppConstants.DIRECT_USERS_ID = String.valueOf(orderDetails.get(0).getCustomerId());
                        genderId = String.valueOf(orderDetails.get(0).getGender());

                        try {
                            Glide.with(getApplicationContext())
                                    .load(GlobalData.SERVER_URL + "Images/DressSubType/" + orderDetails.get(0).getImage())
                                    .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                    .into(order_details_image);

                        } catch (Exception ex) {
                            Log.e(AppConstants.TAG, ex.getMessage());
                        }


                    } catch (Exception e) {

                    }

                }

            }

            @Override
            public void onFailure(Call<BuyerOrderDetailResponse> call, Throwable t) {

            }
        });
    }

    private void setImageList() {
        material_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        material_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        skillUpdateGetMaterialImageAdapter = new SkillUpdateGetMaterialImageAdapter(this, materialImagesSkillUpdateGetEntities, restService);
        material_images.setAdapter(skillUpdateGetMaterialImageAdapter);


//


    }


    public void referenceImage() {
        reference_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        reference_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        skillUpdateGetReferenceImageAdapter = new SkillUpdateGetReferenceImageAdapter(this, referenceImagesSkillUpdateGetEntities, restService);
        reference_images.setAdapter(skillUpdateGetReferenceImageAdapter);

    }

    private void additionalImages() {
        material_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        material_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        additionalMaterialImageAdapter = new AdditionalMaterialImageAdapter(this, additionalMaterialImageEntities, restService);
        material_images.setAdapter(additionalMaterialImageAdapter);


//


        reference_images.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        reference_images.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        skillUpdateGetReferenceImageAdapter = new SkillUpdateGetReferenceImageAdapter(this, referenceImagesSkillUpdateGetEntities, restService);
        reference_images.setAdapter(skillUpdateGetReferenceImageAdapter);


    }

    public void getRequestCustomization() {

        try {
            restService.getRequestMeasurementParts(AppConstants.ORDER_ID).enqueue(new Callback<GetRequestMeasurementPartsResponse>() {
                @Override
                public void onResponse(Call<GetRequestMeasurementPartsResponse> call, Response<GetRequestMeasurementPartsResponse> response) {
//
                    if (response.isSuccessful()) {

                        try {
                            getMeasurementDetailsModals = response.body().getResult();
                            if (getMeasurementDetailsModals == null) {
                                getMeasurementDetailsModals = new ArrayList<>();
                            }
                        } catch (Exception e) {

                        }

//                            setGenderList(getMeasurementDetailsModals);
                    } else if (response.errorBody() != null) {
                        Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();


                    }


                }

                @Override
                public void onFailure(Call<GetRequestMeasurementPartsResponse> call, Throwable t) {
                    insertError("OrderDetailsActivity", "getRequestMeasurementParts()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
        }
    }


    public void UpdateBalaceAmountByTailor() {

        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("Amount", String.valueOf(remaining));

        restService.UpdateBalaceAmountByTailor(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                getOrderDetailsForTailor("Tailor");
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("OrderDetailsActivity", "UpdateBalaceAmountByTailor()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }

    public void updatePaymentStatus(String paymentStatus) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("PaymentStatus", paymentStatus);
        map.put("Type", "Tailor");

        restService.updatePaymentStatus(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("OrderDetailsActivity", "updatePaymentStatus()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.TAILORMEASUREMENT = "";
        AppConstants.PATTERN_ID = "";
        AppConstants.MEASUREMENT_APPOINTMENT_STATUS = "";

    }
//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
            material_appointment_layout.setVisibility(View.GONE);
            measurement_appointment_layout.setVisibility(View.GONE);
            material_appoinment_label_layout.setVisibility(View.GONE);
            measurement_appoinment_label_layout.setVisibility(View.GONE);

            getOrderDetailsForTailor("Tailor");


        } else {
//            getOrderDetails();
            getOrderDetailsForTailor("Buyer");
        }
    }
}
