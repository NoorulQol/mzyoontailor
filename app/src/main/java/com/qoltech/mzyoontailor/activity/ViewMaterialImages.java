package com.qoltech.mzyoontailor.activity;

import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.OnMatrixChangedListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewMaterialImagesAdapter;
import com.qoltech.mzyoontailor.entity.MaterialImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMaterialImages extends BaseActivity  {

    public static ImageView myImage;
    RecyclerView material_list_recyclerview;
    ApiService restService;
    List<MaterialImagesSkillUpdateGetEntity> materialImagesSkillUpdateGetEntities;
    ViewMaterialImagesAdapter viewMaterialImagesAdapter;
    String url = "";
    int i = 0;
    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;


//    private static final String TAG = "Touch";
//    @SuppressWarnings("unused")
//    private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;
//
//    // These matrices will be used to scale points of the image
//    Matrix matrix = new Matrix();
//    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
//    static final int NONE = 0;
//    static final int DRAG = 1;
//    static final int ZOOM = 2;
//    int mode = NONE;
//
//    // these PointF objects are used to record the point(s) the user is touching
//    PointF start = new PointF();
//    PointF mid = new PointF();
//    float oldDist = 1f;
//    PhotoViewAttacher photoViewAttacher;
    public static PhotoView photo_view;
RectF rectf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_material_images_activity);
//        Fresco.initialize(this);
        myImage = findViewById(R.id.myImage);
        photo_view = findViewById(R.id.photo_view);

        material_list_recyclerview = findViewById(R.id.material_list_recyclerview);
        restService = ((MainApplication) getApplication()).getClient();
        rectf = new RectF(0,0,0,0);
        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
            getMaterialImages("Tailor");
        } else {

            getMaterialImages("Buyer");

        }


//        photoViewAttacher = new PhotoViewAttacher(myImage);
//        photoViewAttacher.update();
//        final Animation zoomAnimation = AnimationUtils.loadAnimation(this, R.anim.zoom);
//        myImage.startAnimation(zoomAnimation);
        url = getIntent().getStringExtra("image_url");


        Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "images/MaterialImages/" + AppConstants.SET_MATERIAL_IMAGE)
                .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.gif_loader))
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL).
                        format(DecodeFormat.PREFER_ARGB_8888).override(Target.SIZE_ORIGINAL)

                        .placeholder(R.drawable.placeholder)).into(photo_view);


//        myImage.setOnTouchListener(this);
//        myImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                i++;
//                if (i % 2 == 0) {
//                    material_list_recyclerview.setVisibility(View.VISIBLE);
//                } else {
//                    material_list_recyclerview.setVisibility(View.GONE);
//
//                }
//
//            }
//        });

        photo_view.setOnMatrixChangeListener(new OnMatrixChangedListener() {
            @Override
            public void onMatrixChanged(RectF rect) {
                if (rect.left==rectf.left) {
                    material_list_recyclerview.setVisibility(View.VISIBLE);
//                    Toast.makeText(getApplicationContext(), "sux" + rect, Toast.LENGTH_LONG).show();
                } else {
                    material_list_recyclerview.setVisibility(View.GONE);
//                    Toast.makeText(getApplicationContext(), "fai" + rect, Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    public void getMaterialImages(String orderTypes) {
        restService.getOrderDetailsResponseForTailor(AppConstants.ORDER_ID, AppConstants.ORDER_TYPE, orderTypes).
                enqueue(new Callback<GetOrderDetailsResponseCheck>() {

                    @Override
                    public void onResponse(Call<GetOrderDetailsResponseCheck> call, Response<GetOrderDetailsResponseCheck> response) {


                        materialImagesSkillUpdateGetEntities = response.body().getResult().getMaterialImage();


                        if (materialImagesSkillUpdateGetEntities.size() > 0) {
                            setImageList();
                        }

                    }

                    @Override
                    public void onFailure(Call<GetOrderDetailsResponseCheck> call, Throwable t) {
                             insertError("ViewMaterialImages","getOrderDetailsResponseForTailor()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });

    }

    private void setImageList() {
        material_list_recyclerview.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        material_list_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        viewMaterialImagesAdapter = new ViewMaterialImagesAdapter(this, materialImagesSkillUpdateGetEntities, restService);
        material_list_recyclerview.setAdapter(viewMaterialImagesAdapter);


//


    }

    //     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
