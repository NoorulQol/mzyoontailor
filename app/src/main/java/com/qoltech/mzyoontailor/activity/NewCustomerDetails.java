package com.qoltech.mzyoontailor.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AreaAdapter;
import com.qoltech.mzyoontailor.adapter.CountryCodeAdapter;
import com.qoltech.mzyoontailor.adapter.StateAdapter;
import com.qoltech.mzyoontailor.entity.GetAreaEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.modal.GetAreaResponse;
import com.qoltech.mzyoontailor.ui.GenderScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.Country;
import com.qoltech.mzyoontailor.wrappers.CountryResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.State;
import com.qoltech.mzyoontailor.wrappers.StateResponse;
import com.qoltech.mzyoontailor.wrappers.WelcomeMessageNewUserResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCustomerDetails extends Activity {
    public static EditText add_address_frt_name_edt_txt, add_address_sec_name_edt_txt, add_address_country_edt_txt,
            add_address_state_edt_txt, add_address_area_edt_txt, add_address_floor_edt_txt, add_address_landmark_edt_txt,
            add_address_loc_type_edt_txt,
            add_address_mobile_num_edt_txt;
    public static ImageView add_address_flag_img, add_address_country_edt_img,
            add_address_state_edt_img, add_address_area_edt_img, add_address_loc_type_edt_img;
    TextView add_address_country_code_txt;
    Button add_address_save_btn;
    ApiService restService;
    List<Country> countries;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    CountryCodeAdapter countryCodeDialogAdapter;
    static boolean rememberLastSelection = false;
    static Context context;
    static String CCP_PREF_FILE = "CCP_PREF_FILE";
    static String selectionMemoryTag = "ccp_last_selection";
    Country country;
    List<State> states;
    StateAdapter stateAdapter;
    State state;
    List<GetAreaEntity> areas;
    GetAreaEntity area;
    AreaAdapter areaAdapter;
    SharedPreferences sharedPreferences;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    LinearLayout appointment_list_par_lay;
    RelativeLayout add_address_par_lay;
    TransparentProgressDialog transparentProgressDialog;
    DialogManager dialogManager;
    public String[] loc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer_details);
        restService = ((MainApplication) getApplication()).getClient();
        initView();
        dialogManager=new DialogManager();
        add_address_frt_name_edt_txt = (EditText) findViewById(R.id.add_address_frt_name_edt_txt);
        add_address_sec_name_edt_txt = (EditText) findViewById(R.id.add_address_sec_name_edt_txt);
        add_address_country_edt_txt = (EditText) findViewById(R.id.add_address_country_edt_txt);
        add_address_state_edt_txt = (EditText) findViewById(R.id.add_address_state_edt_txt);
        add_address_area_edt_txt = (EditText) findViewById(R.id.add_address_area_edt_txt);
        add_address_floor_edt_txt = (EditText) findViewById(R.id.add_address_floor_edt_txt);
        add_address_landmark_edt_txt = (EditText) findViewById(R.id.add_address_landmark_edt_txt);
        add_address_mobile_num_edt_txt = (EditText) findViewById(R.id.add_address_mobile_num_edt_txt);
        add_address_loc_type_edt_txt = findViewById(R.id.add_address_loc_type_edt_txt);
        add_address_flag_img = (ImageView) findViewById(R.id.add_address_flag_img);
        add_address_country_code_txt = (TextView) findViewById(R.id.add_address_country_code_txt);
        add_address_save_btn = (Button) findViewById(R.id.add_address_save_btn);
        add_address_country_edt_img = (ImageView) findViewById(R.id.add_address_country_edt_img);
        add_address_state_edt_img = (ImageView) findViewById(R.id.add_address_state_edt_img);
        add_address_area_edt_img = (ImageView) findViewById(R.id.add_address_area_edt_img);
        add_address_loc_type_edt_img = (ImageView) findViewById(R.id.add_address_loc_type_edt_img);
        add_address_par_lay = (RelativeLayout) findViewById(R.id.add_address_par_lay);
        sharedPreferences = getSharedPreferences("Existing_Customer", MODE_PRIVATE);
        add_address_mobile_num_edt_txt.setText(sharedPreferences.getString("customer_phone_num", ""));
        add_address_country_code_txt.setText(sharedPreferences.getString("country_code", ""));
        if (!AppConstants.COUNTRY_FLAG.equalsIgnoreCase("")) {
            Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "images/flags/" + AppConstants.COUNTRY_FLAG)
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(add_address_flag_img);

        }
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        getCountryList();
        add_address_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (add_address_frt_name_edt_txt.getText().toString().equals("")
//                        && add_address_sec_name_edt_txt.getText().toString().equals("") &&
//                        add_address_floor_edt_txt.getText().toString().equals("") &&
//                        add_address_landmark_edt_txt.getText().toString().equals("") &&
//                        add_address_loc_type_edt_txt.getText().toString().equals("") &&
//                        add_address_mobile_num_edt_txt.getText().toString().equals("")) {
//                    Toast.makeText(getApplicationContext(), "Please Enter All Feilds", Toast.LENGTH_LONG).show();
//                }
//
////                else if (AppConstants.CountryCode.equalsIgnoreCase("")) {}
////                else if (AppConstants.STATE_ID.equalsIgnoreCase("")) {
////                    Toast.makeText(getApplicationContext(), "Select A Country First", Toast.LENGTH_LONG).show();
////                } else if (AppConstants.AREA_ID.equalsIgnoreCase("")) {
////                    Toast.makeText(getApplicationContext(), "Select A State First", Toast.LENGTH_LONG).show();
////                }
//                else {
//
//                }


//                   if (AppConstants.CountryCode.equalsIgnoreCase("")) {
//                       Toast.makeText(getApplicationContext(), "Please Enter All Feilds", Toast.LENGTH_LONG).show();
//                   }
//                if (AppConstants.CountryCode.equalsIgnoreCase("") && AppConstants.STATE_ID.equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Select A Country First", Toast.LENGTH_LONG).show();
//                } else if (AppConstants.AREA_ID.equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Select A State First", Toast.LENGTH_LONG).show();
//                } else {
//
//                }
//if (add_address_frt_name_edt_txt.getText().toString().isEmpty()||
//        add_address_sec_name_edt_txt.getText().toString()||
//        add_address_country_edt_txt.getText().toString()||){
//
//}

                if (add_address_frt_name_edt_txt.getText().toString().isEmpty() ||
                        add_address_sec_name_edt_txt.getText().toString().isEmpty() ||
                        add_address_country_edt_txt.getText().toString().isEmpty() ||
                        add_address_state_edt_txt.getText().toString().isEmpty() ||
                        add_address_area_edt_txt.getText().toString().isEmpty() ||
                        add_address_floor_edt_txt.getText().toString().isEmpty() ||
                        add_address_landmark_edt_txt.getText().toString().isEmpty() ||
                        add_address_loc_type_edt_txt.getText().toString().isEmpty() ||
                        add_address_mobile_num_edt_txt.getText().toString().isEmpty()
                        ) {
                    Toast.makeText(getApplicationContext(), "Enter All Feilds", Toast.LENGTH_LONG).show();
                } else {
                    updateNewUserDetails();
                    sendMsgToNewCustomer();
                    startActivity(new Intent(getApplicationContext(), GenderScreen.class));
                }

            }
        });
        add_address_state_edt_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                textCountrySelection.performClick();
                AppConstants.COUNRTY_CODE_ADAPTER = "NEW_CUSTOMER";

                //Do something after 100ms\
                dialogManager.showProgress(NewCustomerDetails.this);
                getStateList();

//
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
//                        progressDialog.dismiss();
                        showStateSelectionList();
//                        getAreaList();
                       dialogManager.hideProgress();
                    }
                }, 3000);

            }
        });
        add_address_area_edt_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                textCountrySelection.performClick();
                AppConstants.COUNRTY_CODE_ADAPTER = "NEW_CUSTOMER";
                dialogManager.showProgress(NewCustomerDetails.this);
                //Do something after 100ms
                getAreaList(AppConstants.STATE_ID);

//
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        dialogManager.hideProgress();
                        showAreaSelectionList();
//                        getAreaList();
                    }
                }, 3000);

            }
        });

        add_address_loc_type_edt_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                textCountrySelection.performClick();
                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerDetails.this);
                builder.setTitle(R.string.location_type);

// add a list

                if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                    loc = new String[]{"الصفحة الرئيسية\n", "عمل"};
                } else {
                    loc = new String[]{"Home", "Work"};
                }
                builder.setItems(loc, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        add_address_loc_type_edt_txt.setText(loc[which]);

                    }
                });

// create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });


    }

    public void updateNewUserDetails() {


        HashMap<String, String> map = new HashMap<>();
        map.put("CountryCode", NewCustomerActivity.textCountrySelection.getText().toString());
        map.put("PhoneNo", NewCustomerActivity.new_customer_phone_no.getText().toString());
        map.put("Name", NewCustomerActivity.userNameNewCustomer.getText().toString());


        restService.updateNewUserDetails(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                AppConstants.DIRECT_USERS_ID = response.body().getResult();
                updateDirectBuyerAddress(AppConstants.DIRECT_USERS_ID);


            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void updateDirectBuyerAddress(String buyerId) {

        HashMap<String, String> map = new HashMap<>();
        map.put("BuyerId", buyerId);
        map.put("FirstName", add_address_frt_name_edt_txt.getText().toString());
        map.put("LastName", add_address_sec_name_edt_txt.getText().toString());
        map.put("CountryId", AppConstants.COUNTRY_ID);
        map.put("StateId", AppConstants.STATE_ID);
        map.put("AreaId", AppConstants.AREA_ID);
        map.put("Floor", add_address_floor_edt_txt.getText().toString());
        map.put("LandMark", add_address_landmark_edt_txt.getText().toString());
        map.put("LocationType", add_address_loc_type_edt_txt.getText().toString());
        map.put("ShippingNotes", "nothing");
        map.put("IsDefault", "1");
        map.put("CountryCode", add_address_country_code_txt.getText().toString());
        map.put("PhoneNo", add_address_mobile_num_edt_txt.getText().toString());
        map.put("Longitude", "0");
        map.put("Lattitude", "0");


        restService.updateDirectCustomerAddress(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                AppConstants.GET_ADDRESS_ID = response.body().getResult();
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("NewCustomerDetails","updateDirectCustomerAddress()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    public void sendMsgToNewCustomer() {


        HashMap<String, String> map = new HashMap<>();
        map.put("UserName", NewCustomerActivity.userNameNewCustomer.getText().toString());
        map.put("PhoneNumber", NewCustomerActivity.textCountrySelection.getText().toString() + NewCustomerActivity.new_customer_phone_no.getText().toString());

        restService.WelcomeMessageToNewCustomer(map).enqueue(new Callback<WelcomeMessageNewUserResponse>() {
            @Override
            public void onResponse(Call<WelcomeMessageNewUserResponse> call, Response<WelcomeMessageNewUserResponse> response) {


            }

            @Override
            public void onFailure(Call<WelcomeMessageNewUserResponse> call, Throwable t) {
                insertError("NewCustomerDetails","WelcomeMessageToNewCustomer()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    private void showCoutrySelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
        TextView myMsg = new TextView(this);
        myMsg.setText(R.string.select_country);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextColor(Color.BLACK);
        //set custom title
//        builder.setCustomTitle(myMsg);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCustomTitle(myMsg)
                .setView(view)
                .create();
        countryCodeDialogAdapter = new CountryCodeAdapter(getApplicationContext(), countries, country, dialog, restService);
        countryList.setAdapter(countryCodeDialogAdapter);
        dialog.show();

    }

    private void getCountryList() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {

            try {
                restService.getCountry().enqueue(new Callback<CountryResponse>() {
                    @Override
                    public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {

                        if (response.isSuccessful()) {
                            countries = response.body().getResult();
                            if (countries == null) {
                                countries = new ArrayList<>();
                            }
                            setCountrySelectionList();
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CountryResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                        insertError("NewCustomerDetails","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");


                    }
                });

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void onUserTappedCountry(Country country) {
        if (rememberLastSelection) {
            storeSelectedCountryNameCode(country.getPhoneCode());
        }
    }


    static void storeSelectedCountryNameCode(String selectedCountryNameCode) {
        //get the shared pref
        SharedPreferences sharedPref = context.getSharedPreferences(
                CCP_PREF_FILE, Context.MODE_PRIVATE);

        //we want to write in shared pref, so lets get editor for it
        SharedPreferences.Editor editor = sharedPref.edit();

        // add our last selection country name code in pref
        editor.putString(selectionMemoryTag, selectedCountryNameCode);

        //finally save it...
        editor.apply();
    }

    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < countries.size(); i++) {
            listSpinner.add(countries.get(i).getCountryName());
        }
        add_address_country_edt_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                textCountrySelection.performClick();
                AppConstants.COUNRTY_CODE_ADAPTER = "NEW_CUSTOMER";
                dialogManager.showProgress(NewCustomerDetails.this);
                showCoutrySelectionList();


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
//                        getStateList();
                        dialogManager.hideProgress();
                    }
                }, 3000);

            }
        });
    }

    public void getStateList() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {

            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("Id", AppConstants.COUNTRY_ID);
                restService.stateList(map).enqueue(new Callback<StateResponse>() {
                    @Override
                    public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {

                        if (response.isSuccessful()) {
                            states = response.body().getResult();
                            if (states == null) {
                                states = new ArrayList<>();
                            }
                            setStateSelectionList();
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<StateResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();

                        insertError("NewCustomerDetails","stateList()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void setStateSelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < states.size(); i++) {
            listSpinner.add(states.get(i).getStateName());
        }


    }

    private void setAreaSelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < areas.size(); i++) {
            listSpinner.add(areas.get(i).getArea());
        }


    }

    public void showStateSelectionList() {
        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        countryList.setLayoutManager(linearLayoutManager);

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.select_state)
                .setView(view)
                .create();
        stateAdapter = new StateAdapter(this, states, state, dialog);
        countryList.setAdapter(stateAdapter);
        dialog.show();

    }

    private void showAreaSelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
        TextView myMsg = new TextView(this);
        myMsg.setText(R.string.select_area);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextColor(Color.BLACK);
        //set custom title
//        builder.setCustomTitle(myMsg);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCustomTitle(myMsg)
                .setView(view)
                .create();
        areaAdapter = new AreaAdapter(getApplicationContext(), areas, area, dialog);
        countryList.setAdapter(areaAdapter);
        dialog.show();

    }

    public void getAreaList(String area_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("Id", area_id);
        restService.getAreaApi(map).enqueue(new Callback<GetAreaResponse>() {
            @Override
            public void onResponse(Call<GetAreaResponse> call, Response<GetAreaResponse> response) {
                if (response.isSuccessful()) {
                    areas = response.body().getResult();
                    if (states == null) {
                        states = new ArrayList<>();
                    }
                    setAreaSelectionList();
                }
            }

            @Override
            public void onFailure(Call<GetAreaResponse> call, Throwable t) {
     insertError("NewCustomerDetails","getAreaApi()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.new_customer_address);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_address_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.add_address_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }


//    /    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
