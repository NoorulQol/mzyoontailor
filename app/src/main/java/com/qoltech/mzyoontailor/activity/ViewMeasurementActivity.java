package com.qoltech.mzyoontailor.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GetMeasurementDetailsAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.AddReferenceScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResult;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsModal;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMeasurementActivity extends AppCompatActivity {

    GetMeasurementDetailsAdapter getCustomizationRequestAdapter;
    RecyclerView measurement_request_recycler_view;

    ApiService restService;
    GetBuyerRequestResult getBuyerRequestResult = new GetBuyerRequestResult();
    List<GetRequestMeasurementPartsModal> getMeasurementDetailsModals;
    SharedPreferences sharedPreferences, sharedPreferences1;
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout;
    Gson gson;
    Button next;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_measurement_request);

        restService = ((MainApplication) getApplication()).getClient();
        sharedPreferences1 = getSharedPreferences("OrderId", MODE_PRIVATE);
        measurement_request_recycler_view = findViewById(R.id.measurement_request_recycler_view);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        bgLayout = (LinearLayout) findViewById(R.id.bgLayout);
        next = findViewById(R.id.next);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

//        getRequestCustomization();

        if (AppConstants.MEASUREMENT_EXISTING.equalsIgnoreCase("EXISTING")) {
            getExistingMeasurent();

        } else {
            next.setVisibility(View.GONE);
            getRequestCustomization();
        }
        getLanguage();


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.TAILORMEASUREMENT.equalsIgnoreCase("TAILORMEASUREMENT")) {
                    Toast.makeText(getApplicationContext(), "Measurement Updated Sucessfully",
                            Toast.LENGTH_LONG).show();


                    AppConstants.TAILORMEASUREMENT = "";
                    updateMeasurementByTailor(AppConstants.ORDER_ID, AppConstants.MEASUREMENT_ID.equalsIgnoreCase("")
                            ? "-1" : AppConstants.MEASUREMENT_ID);
//                    ((MeasurementOneScreen) getApplicationContext()).updateMeasurementByTailor(AppConstants.ORDER_ID, AppConstants.MEASUREMENT_ID.equalsIgnoreCase("") ? "-1" :
//                            AppConstants.MEASUREMENT_ID);

                    startActivity(new Intent(getApplicationContext(),
                            OrderDetailsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                } else {
//                    ((BaseActivity) getApplicationContext()).nextScreen(AddReferenceScreen.class, true);
                    AppConstants.DIRECT_MEASUREMENT_TYPE = "Manually";

                    startActivity(new Intent(getApplicationContext(), AddReferenceScreen.class));
                }


            }
        });
    }


    public void getRequestCustomization() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {

            try {
                restService.getRequestMeasurementParts(AppConstants.ORDER_ID).enqueue(new Callback<GetRequestMeasurementPartsResponse>() {
                    @Override
                    public void onResponse(Call<GetRequestMeasurementPartsResponse> call, Response<GetRequestMeasurementPartsResponse> response) {
//
                        if (response.isSuccessful()) {

                            try {
                                getMeasurementDetailsModals = response.body().getResult();
                                if (getMeasurementDetailsModals == null) {
                                    getMeasurementDetailsModals = new ArrayList<>();
                                }
                            } catch (Exception e) {

                            }

                            setGenderList(getMeasurementDetailsModals);
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }


                    }

                    @Override
                    public void onFailure(Call<GetRequestMeasurementPartsResponse> call, Throwable t) {
                        insertError("ViewMeasurementActivity", "getRequestMeasurementParts()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

                    }
                });
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void getExistingMeasurent() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {

            try {
                restService.getExistingMeasurement(AppConstants.EXISTING_MEASUREMENT_ID).enqueue(new Callback<GetRequestMeasurementPartsResponse>() {
                    @Override
                    public void onResponse(Call<GetRequestMeasurementPartsResponse> call, Response<GetRequestMeasurementPartsResponse> response) {
//
                        if (response.isSuccessful()) {

                            try {
                                getMeasurementDetailsModals = response.body().getResult();
                                if (getMeasurementDetailsModals.size() <= 0) {
                                    next.setVisibility(View.GONE);
                                    getMeasurementDetailsModals = new ArrayList<>();
                                }
                            } catch (Exception e) {

                            }

                            setGenderList(getMeasurementDetailsModals);
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();


                        }


                    }

                    @Override
                    public void onFailure(Call<GetRequestMeasurementPartsResponse> call, Throwable t) {
                        insertError("ViewMeasurementActivity", "getExistingMeasurement()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

                    }
                });
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void setGenderList(List<GetRequestMeasurementPartsModal> getMeasurementDetailsModals) {

        getCustomizationRequestAdapter = new GetMeasurementDetailsAdapter(getApplicationContext(), getMeasurementDetailsModals);
        measurement_request_recycler_view.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        measurement_request_recycler_view.setAdapter(getCustomizationRequestAdapter);

    }

    public void updateMeasurementByTailor(String orderId, String measurementId) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", orderId);
        map.put("MeasurementId", measurementId);

        restService.updateMeasurementByTailor(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                AppConstants.MEASUREMENT_ID = "";
                AppConstants.MEASUREMENT_EXISTING = "";
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("ViewMeasurementActivity", "updateMeasurementByTailor()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCustomizationRequestAdapter != null) {
            getCustomizationRequestAdapter.notifyDataSetChanged();
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.MEASUREMENT_EXISTING = "";
    }


    //     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}