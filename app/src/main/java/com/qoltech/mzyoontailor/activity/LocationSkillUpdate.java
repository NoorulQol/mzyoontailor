package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.LocationSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.SkillUpdateGetLocationEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.SkillUpdateGetLocationModal;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GridviewDecoration;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSkillUpdate extends BaseActivity {
    Button next;
    EditText phone_number;
    String phoneNumber;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    SharedPreferences sharedPreferences;

    LocationSkillUpdateAdapter genderSkillUpdateAdapter;
    ApiService restService;
    List<SkillUpdateGetLocationEntity> genderEntities;
    RecyclerView genderList;

DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_dress_type_skill_update);
        genderList = (RecyclerView) findViewById(R.id.genderList);
        restService = ((MainApplication) getApplication()).getClient();
        dialogManager=new DialogManager();
        initView();
        sharedPreferences=getSharedPreferences("TailorId",MODE_PRIVATE);
        dialogManager.showProgress(getApplicationContext());
        getGender();
    }


//    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
//        mHeaderTxt.setText("Customer Type");
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getGender() {
        restService.getLocationSkillUpdate(sharedPreferences.getString("TailorId","")).enqueue(new Callback<SkillUpdateGetLocationModal>() {
            @Override
            public void onResponse(Call<SkillUpdateGetLocationModal> call, Response<SkillUpdateGetLocationModal> response) {

                genderEntities = response.body().getResult();
                setGenderList();
                dialogManager.hideProgress();
            }

            @Override
            public void onFailure(Call<SkillUpdateGetLocationModal> call, Throwable t) {
                insertError("LocationSkillUpdate","getGender()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    private void setGenderList() {
        genderList.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        GridviewDecoration itemDecoration = new GridviewDecoration(this, R.dimen.size8);
        genderList.addItemDecoration(itemDecoration);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        genderSkillUpdateAdapter = new LocationSkillUpdateAdapter(this, genderEntities,restService);
        genderList.setAdapter(genderSkillUpdateAdapter);
    }

    public void insertError(String pageName,String methodName,String error,
                            String apiVersion,String deviceId,String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName",pageName);
        map.put("MethodName",methodName);
        map.put("Error", error);
        map.put("ApiVersion",apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type",type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
