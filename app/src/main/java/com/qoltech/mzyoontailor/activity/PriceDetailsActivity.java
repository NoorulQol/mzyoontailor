package com.qoltech.mzyoontailor.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceDetailsActivity extends BaseActivity implements View.OnClickListener {
    Dialog mHintDialog;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    private UserDetailsEntity mUserDetailsEntityRes;
    TextView stitching_charge_rupees, stitching_charge_paisa,
            material_charges_rupees, material_charges_paisa, measurement_charges_rupees,
            measurement_charges_paisa, urgent_charges_rupees, urgent_charges_paisa, delivery_charges_rupees,
            delivery_charges_paisa, total_rupees, total_paisa;
    RelativeLayout aed_layout;
    RadioButton full_payment_advance, full_payment_while_delivery, payment,
            payment_mzoon_app, payment_cash, payment_card;
    LinearLayout linear_lay, stitching_type_charges_layout, material_type_charges_layout,
            measurement_type_charges_layout, service_type_charges_layout;
    Button continue_btn;
    Bundle getBundle;
    ApiService restService;
    SharedPreferences sharedPreferences;
    String stitching_charge_rupees_string, stitching_charge_paisa_string,
            material_charges_rupees_string, material_charges_paisa_string,
            measurement_charges_rupees_string, measurement_charges_paisa_string,
            urgent_charges_rupees_string, urgent_charges_paisa_string;
    OrderConfirmationActivity orderConfirmationActivity = new OrderConfirmationActivity();
DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_details);
        restService = ((MainApplication) getApplication()).getClient();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        stitching_charge_rupees = findViewById(R.id.stitching_charge_rupees);
        stitching_charge_paisa = findViewById(R.id.stitching_charge_paisa);
        material_charges_rupees = findViewById(R.id.material_charges_rupees);
        material_charges_paisa = findViewById(R.id.material_charges_paisa);
        measurement_charges_rupees = findViewById(R.id.measurement_charges_rupees);
        measurement_charges_paisa = findViewById(R.id.measurement_charges_paisa);
        urgent_charges_rupees = findViewById(R.id.urgent_charges_rupees);
        urgent_charges_paisa = findViewById(R.id.urgent_charges_paisa);
        total_rupees = findViewById(R.id.total_rupees);
        total_paisa = findViewById(R.id.total_paisa);

        aed_layout = findViewById(R.id.aed_layout);
        full_payment_advance = findViewById(R.id.full_payment_advance);
        full_payment_while_delivery = findViewById(R.id.full_payment_while_delivery);
        payment_mzoon_app = findViewById(R.id.payment_mzoon_app);
        payment_cash = findViewById(R.id.payment_cash);
        payment_card = findViewById(R.id.payment_card);
        continue_btn = findViewById(R.id.continue_btn);
        linear_lay = findViewById(R.id.linear_lay);
        delivery_charges_rupees = findViewById(R.id.delivery_charges_rupees);
        delivery_charges_paisa = findViewById(R.id.delivery_charges_paisa);
        stitching_type_charges_layout = findViewById(R.id.stitching_type_charges_layout);
        material_type_charges_layout = findViewById(R.id.material_type_charges_layout);
        measurement_type_charges_layout = findViewById(R.id.measurement_type_charges_layout);
        service_type_charges_layout = findViewById(R.id.service_type_charges_layout);
        dialogManager=new DialogManager();
        full_payment_advance.setOnClickListener(this);
        full_payment_while_delivery.setOnClickListener(this);
        payment_mzoon_app.setOnClickListener(this);
        payment_cash.setOnClickListener(this);
        payment_card.setOnClickListener(this);
        continue_btn.setOnClickListener(this);
        initView();
        getBundle = getIntent().getExtras();

//        getPaymentTextDialog();
//        getPaymentEnterDialog();

//        bundle.putString("custumization_stitching_charge_rupees", custumization_stitching_charge_rupees.getText().toString());
//        bundle.putString("custumization_stitching_charge_paisa", custumization_stitching_charge_paisa.getText().toString());
//        bundle.putString("appointment_charges_rupees", appointment_charges_rupees.getText().toString());
//        bundle.putString("appointment_charges_paisa", appointment_charges_paisa.getText().toString());
//        bundle.putString("measurement_charges_rupees", measurement_charges_rupees.getText().toString());
//        bundle.putString("measurement_charges_paisa", measurement_charges_paisa.getText().toString());
//        bundle.putString("urgent_charges_rupees", urgent_charges_rupees.getText().toString());
//        bundle.putString("urgent_charges_paisa", urgent_charges_paisa.getText().toString());
//        bundle.putString("total_rupees", total_rupees.getText().toString());
//        bundle.putString("total_paisa", total_paisa.getText().toString());

        stitching_charge_rupees.setText(getBundle.getString("custumization_stitching_charge_rupees"));
        stitching_charge_paisa.setText(getBundle.getString("custumization_stitching_charge_paisa"));
        material_charges_rupees.setText(getBundle.getString("appointment_charges_rupees"));
        material_charges_paisa.setText(getBundle.getString("appointment_charges_paisa"));
        measurement_charges_rupees.setText(getBundle.getString("measurement_charges_rupees"));
        measurement_charges_paisa.setText(getBundle.getString("measurement_charges_paisa"));
        urgent_charges_rupees.setText(getBundle.getString("urgent_charges_rupees"));
        urgent_charges_paisa.setText(getBundle.getString("urgent_charges_paisa"));
        total_rupees.setText(getBundle.getString("total_rupees"));
        total_paisa.setText(getBundle.getString("total_paisa"));
        delivery_charges_rupees.setText(getBundle.getString("delivery_charges_rupees"));
        delivery_charges_paisa.setText(getBundle.getString("delivery_charges_paisa"));

//        bundle.putString("delivery_charges_rupees", seperated5[0]);
//        bundle.putString("delivery_charges_paisa", seperated5[1]);
        try {
            if (getBundle.getString("custumization_stitching_charge_rupees").equalsIgnoreCase("0") ||
                    getBundle.getString("custumization_stitching_charge_rupees").equalsIgnoreCase("") ||
                    getBundle.getString("custumization_stitching_charge_rupees").isEmpty()) {
                stitching_type_charges_layout.setVisibility(View.GONE);

            }
            if (getBundle.getString("appointment_charges_rupees").equalsIgnoreCase("0") ||
                    getBundle.getString("appointment_charges_rupees").equalsIgnoreCase("") ||
                    getBundle.getString("appointment_charges_rupees").isEmpty()) {
                material_type_charges_layout.setVisibility(View.GONE);


            }
            if (getBundle.getString("measurement_charges_rupees").equalsIgnoreCase("0") ||
                    getBundle.getString("measurement_charges_rupees").equalsIgnoreCase("") ||
                    getBundle.getString("measurement_charges_rupees").isEmpty()) {
                measurement_type_charges_layout.setVisibility(View.GONE);


            }
            if (getBundle.getString("urgent_charges_rupees").equalsIgnoreCase("0") ||
                    getBundle.getString("urgent_charges_rupees").equalsIgnoreCase("") ||
                    getBundle.getString("urgent_charges_rupees").isEmpty()) {
                service_type_charges_layout.setVisibility(View.GONE);

            }
        } catch (Exception e) {

        }


    }

    public void initView() {

        ButterKnife.bind(this);

//        setupUI(mAddReferencePayLay);

        setHeader();
//        mAddImageList = new ArrayList<>();
//
//        mAddImageList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(PriceDetailsActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

//        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
//            getHintDialog();
//
//        }

    }

    public void getPaymentTextDialog() {
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(PriceDetailsActivity.this, R.layout.pop_up_price_details);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.price_details_popup_layout), ViewCompat.LAYOUT_DIRECTION_RTL);


        } else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.price_details_popup_layout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button ok, cancel;
        TextView contetnt_text;
//
        ok = mHintDialog.findViewById(R.id.ok);
        cancel = mHintDialog.findViewById(R.id.cancel);
        contetnt_text = mHintDialog.findViewById(R.id.contetnt_text);

        /*Set data*/
        contetnt_text.setText(getResources().getString(R.string.add_ref_hint));

        if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("1") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("1")) {

            contetnt_text.setText(R.string.payment_through_mzyoon_app_contetnt);
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("1")) {
            contetnt_text.setText(R.string.payment_status_will_be_updated_once_the_product_is_delivered);
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("2")) {
            contetnt_text.setText(R.string.payment_status_will_be_updated_once_the_product_is_delivered);
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("3")) {
            contetnt_text.setText(R.string.payment_status_will_be_updated_once_the_product_is_delivered);
        }
//        mAddMaterialTxt.setText(getResources().getString(R.string.add_ref_hint));
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHintDialog.dismiss();

            }
        });

        alertShowing(mHintDialog);
    }

    public void getPaymentEnterDialog() {
        alertDismiss(mHintDialog);
        mHintDialog = getDialog(PriceDetailsActivity.this, R.layout.pop_up_price_details_card_payment);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mHintDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.price_details_popup_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.payable_amount_rupees), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.payment_amount_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
            mHintDialog.findViewById(R.id.payable_amount_rupees).setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            mHintDialog.findViewById(R.id.payable_amount_paisa).setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

        } else {
            ViewCompat.setLayoutDirection(mHintDialog.findViewById(R.id.price_details_popup_layout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

        Button ok, cancel;
        final EditText payable_amount_rupees, payable_amount_paisa, tr, transaction_number;
        LinearLayout transaction_number_layout;
////
        payable_amount_rupees = mHintDialog.findViewById(R.id.payable_amount_rupees);
        payable_amount_paisa = mHintDialog.findViewById(R.id.payable_amount_paisa);
        transaction_number = mHintDialog.findViewById(R.id.transaction_number);
        transaction_number_layout = mHintDialog.findViewById(R.id.transaction_number_layout);

        ok = mHintDialog.findViewById(R.id.ok);
//        cancel = mHintDialog.findViewById(R.id.cancel);
//        contetnt_text = mHintDialog.findViewById(R.id.contetnt_text);
//
//        /*Set data*/
//        contetnt_text.setText(getResources().getString(R.string.add_ref_hint));
////        mAddMaterialTxt.setText(getResources().getString(R.string.add_ref_hint));
        if (AppConstants.PAYMENT_MODE.equalsIgnoreCase("2")) {
            transaction_number_layout.setVisibility(View.GONE);
            AppConstants.TRANSACTION_NUMBER = "0";
        }

        payable_amount_rupees.setText(getBundle.getString("total_rupees"));

        payable_amount_paisa.setText(getBundle.getString("total_paisa"));

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (transaction_number.getText().toString().isEmpty()) {
                    transaction_number.setError("Enter Transaction Number");
                } else if (payable_amount_rupees.getText().toString().isEmpty()) {
                    payable_amount_rupees.setError("Enter Price");

                } else {
                    AppConstants.TRANSACTION_NUMBER = transaction_number.getText().toString();

                    mHintDialog.dismiss();
                }
            }
        });

//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mHintDialog.dismiss();
//
//            }
//        });

        alertShowing(mHintDialog);
    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.price_details));
        mRightSideImg.setVisibility(View.VISIBLE);

//        if (AppConstants.DIRECT_ORDER.equalsIgnoreCase("TAILOR_FLOW")){
//            mMeasurementWizLay.setVisibility(View.GONE);
//            mNewFlowMeasurementWizLay.setVisibility(View.VISIBLE);
//        }else {
//            mMeasurementWizLay.setVisibility(View.VISIBLE);
//            mNewFlowMeasurementWizLay.setVisibility(View.GONE);
//        }
    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bg_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
            stitching_charge_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            stitching_charge_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

            material_charges_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            material_charges_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

            measurement_charges_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            measurement_charges_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

            urgent_charges_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            urgent_charges_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);
            total_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            total_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

//            aed_layout.setVerticalGravity(View.FOCUS_LEFT);
//            updateResources(AddReferenceScreen.this,"ar");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bg_layout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(AddReferenceScreen.this,"en");

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.full_payment_advance:
                clickedRadioButton(full_payment_advance);
                AppConstants.PAYMENT_TYPE = "1";
                AppConstants.PAYMENT_MODE = "";
                setEnableTrue();
                clickedRadioButtonThree(full_payment_advance);
                break;
            case R.id.full_payment_while_delivery:
                clickedRadioButton(full_payment_while_delivery);
                AppConstants.PAYMENT_TYPE = "2";
                AppConstants.PAYMENT_MODE = "";

                setEnableTrue();
                clickedRadioButtonThree(full_payment_while_delivery);

                break;


            case R.id.payment_mzoon_app:


                clickedRadioButtonThree(payment_mzoon_app);
                AppConstants.PAYMENT_MODE = "1";
                if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2")) {
                    AppConstants.TRANSACTION_NUMBER = "0";

                    getPaymentTextDialog();
                } else {
                    AppConstants.TRANSACTION_NUMBER = "0";

                    getPaymentTextDialog();
                }
                break;
            case R.id.payment_cash:

                clickedRadioButtonThree(payment_cash);
                AppConstants.PAYMENT_MODE = "2";
                if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("1")) {
                    AppConstants.TRANSACTION_NUMBER = "0";

                    getPaymentEnterDialog();

                } else {
                    AppConstants.TRANSACTION_NUMBER = "0";

                    getPaymentTextDialog();
                }
                break;
            case R.id.payment_card:

                clickedRadioButtonThree(payment_card);
                AppConstants.PAYMENT_MODE = "3";
                if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("1")) {
                    getPaymentEnterDialog();

                } else {
                    AppConstants.TRANSACTION_NUMBER = "0";

                    getPaymentTextDialog();

                }
                break;

            case R.id.continue_btn:
                if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Select A Payment Type", Toast.LENGTH_LONG).show();

                } else if (!AppConstants.PAYMENT_TYPE.isEmpty() && AppConstants.PAYMENT_MODE.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Select A Payment Mode", Toast.LENGTH_LONG).show();

                } else {

                    dialogManager.showProgress(getApplicationContext());
                    insertPriceDetails();

                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    AppConstants.TAILOR_DIRECT_ORDER = "";
                    AppConstants.PATTERN_ID = "";
                    AppConstants.SUB_DRESS_TYPE_ID = "";
                    AppConstants.PAYMENT_MODE = "";
                    AppConstants.PAYMENT_TYPE = "";

                    AppConstants.HEAD = "";
                    AppConstants.NECK = "";
                    AppConstants.CHEST = "";
                    AppConstants.WAIST = "";
                    AppConstants.THIGH = "";
                    AppConstants.ANKLE = "";
                    AppConstants.KNEE = "";

                    AppConstants.OVER_ALL_HEIGHT = "";
                    AppConstants.SHORTS = "";
                    AppConstants.OUTSEAM = "";
                    AppConstants.INSEAM = "";

                    AppConstants.SHOULDER = "";
                    AppConstants.HALF_SLEEVE = "";
                    AppConstants.BICEP = "";
                    AppConstants.HIP = "";
                    AppConstants.BOTTOM = "";

                    AppConstants.HEIGHT = "";
                    AppConstants.SLEEVE = "";
                    AppConstants.WRIST = "";

                    AppConstants.WOMEN_OVER_BUST = "";
                    AppConstants.WOMEN_UNDER_BUST = "";
                    AppConstants.WOMEN_HIP_BONE = "";
                    AppConstants.WOMEN_THIGH = "";
                    AppConstants.WOMEN_KNEE = "";
                    AppConstants.WOMEN_CALF = "";
                    AppConstants.WOMEN_ANKLE = "";

                    AppConstants.WOMEN_HEAD = "";
                    AppConstants.WOMEN_NECK = "";
                    AppConstants.WOMEN_BUST = "";
                    AppConstants.WOMEN_WAIST = "";
                    AppConstants.WOMEN_FULL_HIP = "";

                    AppConstants.WOMEN_HEIGHT = "";
                    AppConstants.WOMEN_STW = "";
                    AppConstants.WOMEN_NLTC = "";
                    AppConstants.WOMENT_NLTB = "";
                    AppConstants.WOMEN_STHB = "";
                    AppConstants.WOMEN_WTHB = "";
                    AppConstants.WOMEN_HTH = "";
                    AppConstants.WOMEN_INSEAM = "";
                    AppConstants.WOMEN_OUTSEAM = "";

                    AppConstants.WOMEN_SHOULDER = "";
                    AppConstants.WOMEN_BICEP = "";
                    AppConstants.WOMEN_WRIST = "";

                    AppConstants.WOMEN_SLEEVE = "";

                    AppConstants.BOY_HEAD = "";
                    AppConstants.BOY_NECK = "";
                    AppConstants.BOY_CHEST = "";
                    AppConstants.BOY_WAIST = "";
                    AppConstants.BOY_THIGH = "";
                    AppConstants.BOY_KNEE = "";
                    AppConstants.BOY_ANKLE = "";

                    AppConstants.BOY_OVER_ALL_HEIGHT = "";
                    AppConstants.BOY_SHORTS = "";
                    AppConstants.BOY_OUTSEAM = "";
                    AppConstants.BOY_INSEAM = "";

                    AppConstants.BOY_SHOULDER = "";
                    AppConstants.BOY_HALFSLEEVE = "";
                    AppConstants.BOY_HIP = "";
                    AppConstants.BOY_BICEP = "";
                    AppConstants.BOY_BOTTOM = "";

                    AppConstants.BOY_HEIGHT = "";
                    AppConstants.BOY_SLEEVE = "";
                    AppConstants.BOY_WRIST = "";

                    AppConstants.GIRL_OVER_BURST = "";
                    AppConstants.GIRL_UNDER_BURST = "";
                    AppConstants.GIRL_HIP_BONE = "";
                    AppConstants.GIRL_THIGH = "";
                    AppConstants.GIRL_KNEE = "";
                    AppConstants.GIRL_CALF = "";
                    AppConstants.GIRL_ANKLE = "";
                    AppConstants.GIRL_HEAD = "";
                    AppConstants.GIRL_NECK = "";
                    AppConstants.GIRL_BUST = "";
                    AppConstants.GIRL_WAIST = "";
                    AppConstants.GIRL_FULL_HIP = "";
                    AppConstants.GIRL_HEIGHT = "";
                    AppConstants.GIRL_STW = "";
                    AppConstants.GIRL_NLTC = "";
                    AppConstants.GIRL_NLTB = "";
                    AppConstants.GIRL_STHB = "";
                    AppConstants.GIRL_WTHB = "";
                    AppConstants.GIRL_HTH = "";
                    AppConstants.GIRL_INSEAM = "";
                    AppConstants.GIRL_OUTSEAM = "";
                    AppConstants.GIRL_SHOULDER = "";
                    AppConstants.GIRL_BICEP = "";
                    AppConstants.GIRL_WRIST = "";
                    AppConstants.GIRL_SLEEVE = "";
                    AppConstants.MEASUREMENT_VALUES = new ArrayList<>();
                    AppConstants.MEASUREMENT_ID = "";
                    AppConstants.MEASUREMENT_MANUALLY = "";
                    AppConstants.MEASUREMENT_MAP = new HashMap<>();
//                        AppConstants.SUB_DRESS_TYPE_ID = "";
                    AppConstants.GET_ADDRESS_ID = "";

//                        AppConstants.PATTERN_ID = "";

                    AppConstants.ORDER_TYPE_ID = "";

                    AppConstants.MEASUREMENT_ID = "";

                    AppConstants.MATERIAL_IMAGES = new ArrayList<>();

                    AppConstants.REFERENCE_IMAGES = new ArrayList<>();

                    AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

                    AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                    AppConstants.DELIVERY_TYPE_ID = "";

                    AppConstants.MEASUREMENT_TYPE = "";

                    AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();

                    AppConstants.SEASONAL_ID = "";
                    AppConstants.PLACE_INDUSTRY_ID = "";
                    AppConstants.BRANDS_ID = "";


                    AppConstants.MATERIAL_ID = "";
                    AppConstants.MATERIAL_TYPE_NAME = "";

                    AppConstants.COLOR_ID = "";
                    AppConstants.COLOUR_NAME = "";

                    AppConstants.PATTERN_ID = "";
                    AppConstants.PATTERN_NAME = "";

                    AppConstants.SEASONAL_NAME = "";
                    AppConstants.PLACE_OF_INDUSTRY_NAME = "";
                    AppConstants.BRANDS_NAME = "";


                    AppConstants.MEASUREMENT_TYPE_CHARGES = 0;
                    AppConstants.ORDER_TYPE_CHARGES = 0;
                    AppConstants.SERVICE_TYPE_CHARGES = 0;
                    finish();

                }

                break;
            case R.id.header_left_side_img:
                onBackPressed();
                AppConstants.PAYMENT_TYPE = "";
                AppConstants.PAYMENT_MODE = "";
                break;
        }


    }

    private void clickedRadioButton(RadioButton radioButton) {
        full_payment_advance.setChecked(false);
        full_payment_while_delivery.setChecked(false);
//        mBinding.radioButton3.setChecked(false);
//        mBinding.radioButton4.setChecked(false);

        radioButton.setChecked(true);
    }

    private void clickedRadioButtonThree(RadioButton radioButton) {
        payment_mzoon_app.setChecked(false);
        payment_cash.setChecked(false);
        payment_card.setChecked(false);

//        mBinding.radioButton3.setChecked(false);
//        mBinding.radioButton4.setChecked(false);

        radioButton.setChecked(true);
    }

    private void setEnableTrue() {
        payment_mzoon_app.setEnabled(true);
        payment_cash.setEnabled(true);
        payment_card.setEnabled(true);

//        mBinding.radioButton3.setChecked(false);
//        mBinding.radioButton4.setChecked(false);

    }

    private void setEnableFalse() {
        payment_mzoon_app.setEnabled(false);
        payment_cash.setEnabled(false);
        payment_card.setEnabled(false);

//        mBinding.radioButton3.setChecked(false);
//        mBinding.radioButton4.setChecked(false);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppConstants.PAYMENT_TYPE = "";
        AppConstants.PAYMENT_MODE = "";
    }

    public void insertPriceDetails() {


        uploadOrderQuotationTailorOrder();
        AppConstants.MEASUREMENT_EXISTING = "";

        if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("1") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("1")) {
            updatePaymentStatus("1");
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("1") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("2")) {
            updatePaymentStatus("1");
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("1") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("3")) {
            updatePaymentStatus("1");
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("1")) {
            updatePaymentStatus("2");
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("2")) {
            updatePaymentStatus("2");
        } else if (AppConstants.PAYMENT_TYPE.equalsIgnoreCase("2") && AppConstants.PAYMENT_MODE.equalsIgnoreCase("3")) {
            updatePaymentStatus("2");
        }

    }

    public void uploadOrderQuotationTailorOrder() {
        try {

//
//            stitching_charge_rupees.setText(getBundle.getString("custumization_stitching_charge_rupees"));
//            stitching_charge_paisa.setText(getBundle.getString("custumization_stitching_charge_paisa"));
//            material_charges_rupees.setText(getBundle.getString("appointment_charges_rupees"));
//            material_charges_paisa.setText(getBundle.getString("appointment_charges_paisa"));
//            measurement_charges_rupees.setText(getBundle.getString("measurement_charges_rupees"));
//            measurement_charges_paisa.setText(getBundle.getString("measurement_charges_paisa"));
//            urgent_charges_rupees.setText(getBundle.getString("urgent_charges_rupees"));
//            urgent_charges_paisa.setText(getBundle.getString("urgent_charges_paisa"));
//            total_rupees.setText(getBundle.getString("total_rupees"));
//            total_paisa.setText(getBundle.getString("total_paisa"));
//            delivery_charges_rupees.setText(getBundle.getString("delivery_charges_rupees"));
//            delivery_charges_paisa.setText(getBundle.getString("delivery_charges_paisa"));

            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences.getString("TailorId", ""));
            map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            map.put("StichingTime", orderConfirmationActivity.text_days_required.getText().toString());
            map.put("TailorCharges[0][TailorChargesId]", String.valueOf(1));
            map.put("TailorCharges[0][Amount]", stitching_charge_rupees.getText().toString() + "." + stitching_charge_paisa.getText().toString());
            map.put("TailorCharges[1][TailorChargesId]", String.valueOf(2));
            map.put("TailorCharges[1][Amount]", material_charges_rupees.getText().toString() + "." + material_charges_paisa.getText().toString());
            map.put("TailorCharges[2][TailorChargesId]", String.valueOf(3));
            map.put("TailorCharges[2][Amount]", measurement_charges_rupees.getText().toString() + "." + measurement_charges_paisa.getText().toString());
            map.put("TailorCharges[3][TailorChargesId]", String.valueOf(4));
            map.put("TailorCharges[3][Amount]", urgent_charges_rupees.getText().toString() + "." + urgent_charges_paisa.getText().toString());
            map.put("ApproximateDeliveryTime", orderConfirmationActivity.calender_text.getText().toString());

            map.put("Amount", total_rupees.getText().toString() + "." +
                    total_paisa.getText().toString());
            map.put("Balance", "0");
            map.put("PaymentType", AppConstants.PAYMENT_TYPE);
            map.put("PaymentMode", AppConstants.PAYMENT_MODE);
            map.put("Transactionno", AppConstants.TRANSACTION_NUMBER);
//        map.put("TailorId", sharedPreferences.getString("TailorId",""));
//        map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            restService.orderQuotationTailorOrder(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + rupees_total + paisa_total, Toast.LENGTH_LONG).show();
//                    Toast.makeText(getActivity(), response.body().getResponseMsg() + total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                    isApproveOrder();

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();

        }
    }

    public void isApproveOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("IsApproved", "1");
        map.put("Type", "Tailor");

        restService.approveOrder(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                insertApprovedTailor();
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void insertApprovedTailor() {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("ApprovedTailorId", sharedPreferences.getString("TailorId", ""));

        restService.buyerOrderApproval(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                dialogManager.hideProgress();
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void updatePaymentStatus(String paymentStatus) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("PaymentStatus", paymentStatus);
        map.put("Type", "Tailor");

        restService.updatePaymentStatus(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
