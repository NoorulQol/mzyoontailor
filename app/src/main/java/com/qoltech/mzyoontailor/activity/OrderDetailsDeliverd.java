package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GetOrderDeliverdAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.BuyerRequestApprovedResponse;
import com.qoltech.mzyoontailor.wrappers.GetOrderNotDeliverdResponse;
import com.qoltech.mzyoontailor.wrappers.GetOrderNotDeliverdResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsDeliverd extends BaseActivity {
    TextView title;
    FragmentTransaction ft;
    Toolbar toolbar;
    FrameLayout fl;
    private static final String TAG = "GridViewActivity";
    GetOrderDeliverdAdapter getOrderDeliverdAdapter;
    RecyclerView genderList;
    ApiService restService;
    List<GetOrderNotDeliverdResult> genders;
    public static String GENDER_ID = "";
    HomeActivity homeActivity = new HomeActivity();
    BuyerRequestApprovedResponse buyerRequestApprovedResponse;
    SharedPreferences sharedPreferences;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    LinearLayout appointment_list_par_lay;
    TextView no_result_found;
    DialogManager dialogManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_request_approved);
        initView();
        setupWindowAnimations();
        restService = ((MainApplication) this.getApplication()).getClient();
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
//        title = findViewById(R.id.headerTitle);
//        toolbar = findViewById(R.id.toolbar);
        restService = ((MainApplication) this.getApplication()).getClient();
        no_result_found=findViewById(R.id.no_result_found);
//        setSupportActionBar(toolbar);
//        toolbar.setContentInsetStartWithNavigation(0);
//        title.setText("Pending");
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        genderList = findViewById(R.id.appointment_list_recycler_view);
        dialogManager=new DialogManager();
        dialogManager.showProgress(getApplicationContext());

        getLanguage();
        getGenderList();

    }

    private void getGenderList() {
        if (new UtilService().isNetworkAvailable(this)) {

            try {
                restService.getOrderDelivered(sharedPreferences.getString("TailorId","")).enqueue(new Callback<GetOrderNotDeliverdResponse>() {
                    @Override
                    public void onResponse(Call<GetOrderNotDeliverdResponse> call, Response<GetOrderNotDeliverdResponse> response) {

                        if (response.isSuccessful()) {
                            genders = response.body().getResult();

                            try {


                                if (genders.size() > 0) {


                                    setGenderList();
                                    dialogManager.hideProgress();

                                } else {
                                    no_result_found.setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {

                            }

                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<GetOrderNotDeliverdResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();

                        insertError("OrderDetailsDeliverd","getOrderDelivered()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");


                    }
                });

            } catch (Exception e) {
                Toast.makeText(this, "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
//        GridviewDecoration itemDecoration = new GridviewDecoration(this, R.dimen.size8);
//        genderList.addItemDecoration(itemDecoration);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        getOrderDeliverdAdapter = new GetOrderDeliverdAdapter(this, genders);
        genderList.setAdapter(getOrderDeliverdAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getOrderDeliverdAdapter != null) {
            getOrderDeliverdAdapter.notifyDataSetChanged();
        }
    }

    private void setupWindowAnimations() {
        this.overridePendingTransition(R.anim.animation_enter,
                R.anim.animation_leave);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }


    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.order_completed);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }
    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_list_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.appointment_list_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }

//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
