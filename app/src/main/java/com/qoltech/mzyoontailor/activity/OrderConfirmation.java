package com.qoltech.mzyoontailor.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.OrderConfirmationTabAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderConfirmation extends AppCompatActivity {
    TextView title;
    public static ViewPager viewPager;
    public static ImageView get_dress_sub_type_image;
    public static TextView get_dress_sub_type_name_txt, quantity;
    public static EditText qty;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    LinearLayout bgLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        bgLayout=(LinearLayout) findViewById(R.id.bgLayout);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        initView();
        title = findViewById(R.id.headerTitle);
        get_dress_sub_type_name_txt = findViewById(R.id.get_dress_sub_type_name_txt);
        quantity = findViewById(R.id.quantity);
        qty=findViewById(R.id.qty);
        get_dress_sub_type_image = findViewById(R.id.get_dress_sub_type_image);


//        setupWindowAnimations();
//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        toolbar.setContentInsetStartWithNavigation(0);
//        title.setText("Order Quotation");
//        getSupportActionBar().setTitle("");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager_order_confirmation);
//
        // Create an adapter that knows which fragment should be shown on each page
        OrderConfirmationTabAdapter adapter = new OrderConfirmationTabAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs_order_confirmation);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0c2c75"));
//        tabLayout.setSelectedTabIndicatorHeight((int) (50 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#000000"), Color.parseColor("#ffffff"));
        tabLayout.setupWithViewPager(viewPager);


    }

    private void setupWindowAnimations() {
        this.overridePendingTransition(R.anim.animation_enter,
                R.anim.animation_leave);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.order_quotation);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomerLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            country_code_lay_old.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
//            old_customer_phone_no.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }

////    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");
//
//    public void insertError(String pageName, String methodName, String error,
//                            String apiVersion, String deviceId, String type) {
//
//        HashMap<String, String> map = new HashMap<>();
//
//        map.put("PageName", pageName);
//        map.put("MethodName", methodName);
//        map.put("Error", error);
//        map.put("ApiVersion", apiVersion);
//        map.put("DeviceId", deviceId);
//        map.put("Type", type);
//        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
//            @Override
//            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//
//            }
//
//            @Override
//            public void onFailure(Call<SignUpResponse> call, Throwable t) {
//
//            }
//        });
//    }
}
