package com.qoltech.mzyoontailor.activity;

import android.graphics.RectF;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.OnMatrixChangedListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewReferenceImagesAdapter;
import com.qoltech.mzyoontailor.entity.ReferenceImagesSkillUpdateGetEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GetOrderDetailsResponseCheck;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewReferenceImagesActivity extends BaseActivity {

    public static ImageView myImage;
    RecyclerView material_list_recyclerview;
    ApiService restService;
    List<ReferenceImagesSkillUpdateGetEntity> referenceImagesSkillUpdateGetEntities;
    ViewReferenceImagesAdapter viewReferenceImagesAdapter;
    String url = "";
    int i = 0;
    public static PhotoView photo_view;
    RectF rectf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_material_images_activity);
        myImage = findViewById(R.id.myImage);
        photo_view = findViewById(R.id.photo_view);
        material_list_recyclerview = findViewById(R.id.material_list_recyclerview);
        restService = ((MainApplication) getApplication()).getClient();

        myImage = findViewById(R.id.myImage);
        rectf = new RectF(0, 0, 0, 0);
        if (AppConstants.TYPE_TAILOR_BUYER.equalsIgnoreCase("Tailor")) {
            getMaterialImages("Tailor");
        } else {

            getMaterialImages("Buyer");

        }


        Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "images/ReferenceImages/" +
                AppConstants.SET_REFERENCE_IMAGE)
                .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.gif_loader))
                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.placeholder)).into(photo_view);


        photo_view.setOnMatrixChangeListener(new OnMatrixChangedListener() {
            @Override
            public void onMatrixChanged(RectF rect) {
                if (rect.left == rectf.left) {
                    material_list_recyclerview.setVisibility(View.VISIBLE);
//                    Toast.makeText(getApplicationContext(), "sux" + rect, Toast.LENGTH_LONG).show();
                } else {
                    material_list_recyclerview.setVisibility(View.GONE);
//                    Toast.makeText(getApplicationContext(), "fai" + rect, Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    public void getMaterialImages(String orderTypes) {
        restService.getOrderDetailsResponseForTailor(AppConstants.ORDER_ID, AppConstants.ORDER_TYPE, orderTypes).
                enqueue(new Callback<GetOrderDetailsResponseCheck>() {

                    @Override
                    public void onResponse(Call<GetOrderDetailsResponseCheck> call, Response<GetOrderDetailsResponseCheck> response) {


                        referenceImagesSkillUpdateGetEntities = response.body().getResult().getReferenceImage();


                        if (referenceImagesSkillUpdateGetEntities.size() > 0) {
                            setImageList();
                        }

                    }

                    @Override
                    public void onFailure(Call<GetOrderDetailsResponseCheck> call, Throwable t) {
                             insertError("ViewReferenceImagesActivity","getOrderDetailsResponseForTailor()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });

    }

    private void setImageList() {
        material_list_recyclerview.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        material_list_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        viewReferenceImagesAdapter = new ViewReferenceImagesAdapter(this, referenceImagesSkillUpdateGetEntities, restService);
        material_list_recyclerview.setAdapter(viewReferenceImagesAdapter);


//


    }

    //     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
