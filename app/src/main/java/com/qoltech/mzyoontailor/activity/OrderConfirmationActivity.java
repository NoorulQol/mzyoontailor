package com.qoltech.mzyoontailor.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AdapterOneToNine;
import com.qoltech.mzyoontailor.entity.GetTailorDefaultChargesEntity;
import com.qoltech.mzyoontailor.entity.InsertAttributeApiCallEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.AppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.GetMaterialChargesByTailorResponse;
import com.qoltech.mzyoontailor.modal.GetTailorDefaultChargesResponse;
import com.qoltech.mzyoontailor.modal.InsertAttributeQuantityApicallModal;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.ui.AppointmentDetails;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetAttributeDetailModal;
import com.qoltech.mzyoontailor.wrappers.GetDressSubtypeModal;
import com.qoltech.mzyoontailor.wrappers.GetMeasurementDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.GetTailorChargesResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderConfirmationActivity extends BaseActivity {

    public static TextView text_material_required,
            text_days_required,
            calender_text, get_dress_sub_type_name_txt;
    ImageView image_dropdown_material_reauired, image_dropdown_days_reauired, get_dress_sub_type_image;
    public static ArrayList<String> mArrayList;
    public static Dialog mOrderConfirmDialog;
    public static AdapterOneToNine mConfirmAdapter;
    List<GetDressSubtypeModal> getDressSubtypeModals;

    public static EditText measurement_charges_rupees, measurement_charges_paisa, custumization_stitching_charge_rupees,
            custumization_stitching_charge_paisa, appointment_charges_rupees, appointment_charges_paisa,
            urgent_charges_rupees, urgent_charges_paisa, total_paisa,

    tax_rupees, tax_paisa, total_rupees, amount_paid;
    ApiService restService;
    List<GetTailorDefaultChargesEntity> getTailorDefaultChargesEntities;
    String r1, r2, r3, r4, p1, p2, p3, p4;
    int i, j = 0;
    SharedPreferences sharedPreferences;
    LinearLayout measurement_charges_layout, delivery_charges_layout,
            service_type_charges, amount_paid_lay, material_charges_layout;
    int rupees_total, paisa_total;

    int firstDigit, lastDigit;
    int add;
    Button continue_btn;
    private String mMeasurementTypeBoolStr = "",
            mOrderTypeBoolStr = "", mOrderTypeNameStr = "", mMeasurementTypeNameStr = "";
    ArrayList<String> materialList;
    List<GetAttributeDetailModal> getAttributeDetailModals;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    private UserDetailsEntity mUserDetailsEntityRes;
    Bundle bundle;
    String deliveryChargesRupeesString="0",deliveryChargesPaisaString="0";
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_quotation_new_activity);
        restService = ((MainApplication) getApplication()).getClient();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        text_material_required = findViewById(R.id.text_material_required);
        text_days_required = findViewById(R.id.text_days_required);
        calender_text = findViewById(R.id.calender_text);
        image_dropdown_material_reauired = findViewById(R.id.image_dropdown_material_reauired);
        image_dropdown_days_reauired = findViewById(R.id.image_dropdown_days_reauired);
        get_dress_sub_type_name_txt = findViewById(R.id.get_dress_sub_type_name_txt);
        get_dress_sub_type_image = findViewById(R.id.get_dress_sub_type_image);
        measurement_charges_rupees = (EditText) findViewById(R.id.measurement_charges_rupees);
        measurement_charges_paisa = (EditText) findViewById(R.id.measurement_charges_paisa);
        custumization_stitching_charge_rupees = (EditText) findViewById(R.id.custumization_stitching_charge_rupees);
        custumization_stitching_charge_paisa = (EditText) findViewById(R.id.custumization_stitching_charge_paisa);
        appointment_charges_rupees = (EditText) findViewById(R.id.appointment_charges_rupees);
        appointment_charges_paisa = (EditText) findViewById(R.id.appointment_charges_paisa);
        total_rupees = (EditText) findViewById(R.id.total_rupees);
//        material_delivery_charges_paisa = (EditText) findViewById(R.id.material_delivery_charges_paisa);
        urgent_charges_rupees = (EditText) findViewById(R.id.urgent_charges_rupees);
        urgent_charges_paisa = (EditText) findViewById(R.id.urgent_charges_paisa);
        total_paisa = findViewById(R.id.total_paisa);
        material_charges_layout = findViewById(R.id.material_charges_layout);
        measurement_charges_layout = findViewById(R.id.measurement_charges_layout);
        delivery_charges_layout = findViewById(R.id.delivery_charges_layout);
        service_type_charges = findViewById(R.id.service_type_charges);
        amount_paid_lay = findViewById(R.id.amount_paid_lay);
        continue_btn = findViewById(R.id.continue_btn);
        dialogManager=new DialogManager();
        dialogManager.showProgress(getApplicationContext());
        bundle = new Bundle();
        mOrderConfirmDialog = new Dialog(getApplicationContext());
        mOrderConfirmDialog.setCancelable(true);
//        getMeasurementDetails();
        initView();
        getDressTypes();
        AppConstants.CLASS_NAME = "Material_Used";
        if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {
            getTailorChargesStatus();

        } else {
            getAppointmentMaterialApiCall();
            getAppointmentMeasurementApiCall();

        }
        getDefaultChargesCharges();


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
//
//                getStichingChargesbyTailor(sharedPreferences.getString("TailorId", ""), "1");
//                getMaterialChargesByTailor(sharedPreferences.getString("TailorId", ""), "1",
//                       "1");
                try {


                    getStichingChargesbyTailor(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID);
//                AppConstants.TAILOR_DIRECT_ORDER = "TAILOR_DIRECT_ORDER"
                    if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {
                        getMaterialChargesByTailor(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID,
                                AppConstants.PATTERN_ID);

                    } else {
                        getMaterialChargesByTailor(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID,
                                AppConstants.MATERIAL_ID);

                    }
                } catch (Exception e) {
                    {
                    }
                }

            }
        }, 1000);


//        if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1 &&
//                AppConstants.ORDER_TYPE_CHARGES == 3) {
//            measurement_charges_layout.setVisibility(View.GONE);
//            material_charges_layout.setVisibility(View.VISIBLE);
//
//        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1) {
//            measurement_charges_layout.setVisibility(View.GONE);
//
//        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 3) {
//            measurement_charges_layout.setVisibility(View.VISIBLE);
//            j = 1;
//
//        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 2) {
//            measurement_charges_layout.setVisibility(View.VISIBLE);
//            j = 1;
//
//        } else if (AppConstants.ORDER_TYPE_CHARGES == 3) {
//            measurement_charges_layout.setVisibility(View.GONE);
//
//
//        } else {
//            measurement_charges_layout.setVisibility(View.VISIBLE);
//
//            j = 1;
//
//
//        }
//
//
//        if (AppConstants.ORDER_TYPE_CHARGES == 3) {
//
//            material_charges_layout.setVisibility(View.VISIBLE);
//        } else {
//
//            material_charges_layout.setVisibility(View.GONE);
//
//        }
//
//        if (AppConstants.SERVICE_TYPE_CHARGES == 1) {
//
//            service_type_charges.setVisibility(View.VISIBLE);
//            i = 1;
//        } else {
//
//            service_type_charges.setVisibility(View.GONE);
//
//        }

//        End


        if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1 &&
                AppConstants.ORDER_TYPE_CHARGES == 3) {
            measurement_charges_layout.setVisibility(View.GONE);
            material_charges_layout.setVisibility(View.VISIBLE);

        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1) {
            measurement_charges_layout.setVisibility(View.GONE);

        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 3) {
            measurement_charges_layout.setVisibility(View.VISIBLE);
            j = 1;

        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 2) {
            measurement_charges_layout.setVisibility(View.VISIBLE);
            j = 1;

        } else if (AppConstants.ORDER_TYPE_CHARGES == 3) {
            measurement_charges_layout.setVisibility(View.GONE);


        } else {
            measurement_charges_layout.setVisibility(View.VISIBLE);

            j = 1;


        }


        if (AppConstants.ORDER_TYPE_CHARGES == 3) {

            material_charges_layout.setVisibility(View.VISIBLE);
        } else {

            material_charges_layout.setVisibility(View.VISIBLE);

        }

        if (AppConstants.SERVICE_TYPE_CHARGES == 1) {

            service_type_charges.setVisibility(View.VISIBLE);
            i = 1;
        } else {

            service_type_charges.setVisibility(View.GONE);

        }


        mArrayList = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            mArrayList.add(String.valueOf(i));
        }

        image_dropdown_material_reauired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.CLASS_NAME = "Material_Used";
                setAdapter(mArrayList);


            }
        });
        image_dropdown_days_reauired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppConstants.CLASS_NAME = "Order_Confirmation";
                setAdapter(mArrayList);


            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {

                    if (AppConstants.TOTAL_RUPEES.equalsIgnoreCase("")) {

                        Toast.makeText(getApplicationContext(), "Enter Minimum Charges", Toast.LENGTH_LONG).show();
//                            ViewPager viewPager = (ViewPager) getApplicationContext().findViewById(
//                                    R.id.viewpager_order_confirmation);
//                            viewPager.setCurrentItem(0);

                    } else {


//                        uploadOrderQuotationTailorOrder();
//                        insertAttributeQuantityApiCall();

                        if (calender_text.getText().toString().isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Select the Delivery Date", Toast.LENGTH_LONG).show();

                        } else {

                            bundle.putString("custumization_stitching_charge_rupees", custumization_stitching_charge_rupees.getText().toString());
                            bundle.putString("custumization_stitching_charge_paisa", custumization_stitching_charge_paisa.getText().toString());
                            bundle.putString("appointment_charges_rupees", appointment_charges_rupees.getText().toString());
                            bundle.putString("appointment_charges_paisa", appointment_charges_paisa.getText().toString());
                            bundle.putString("measurement_charges_rupees", measurement_charges_rupees.getText().toString());
                            bundle.putString("measurement_charges_paisa", measurement_charges_paisa.getText().toString());
                            bundle.putString("urgent_charges_rupees", urgent_charges_rupees.getText().toString());
                            bundle.putString("urgent_charges_paisa", urgent_charges_paisa.getText().toString());
//                            bundle.putString("total_rupees", total_rupees.getText().toString());
//                            bundle.putString("total_paisa", total_paisa.getText().toString());
                            if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")){


                                Handler handler1=new Handler();
                                handler1.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {


                                        int i=Integer.parseInt(total_rupees.getText().toString())+Integer.parseInt(deliveryChargesRupeesString);
                                        int j=Integer.parseInt(total_paisa.getText().toString())+Integer.parseInt(deliveryChargesPaisaString);

                                        if (String.valueOf(j).length()>2){


                                            String kk=String.valueOf(j).substring(0, 1);
                                            String jj=String.valueOf(j).substring(1, 3);
                                            int k=i+Integer.parseInt(kk);

                                            bundle.putString("total_rupees", String.valueOf(k));
                                            bundle.putString("total_paisa", String.valueOf(jj));


                                        }

                                        else {
//                                            int i=Integer.parseInt(total_rupees.getText().toString())+Integer.parseInt(deliveryChargesRupeesString);
//                                            int j=Integer.parseInt(total_paisa.getText().toString())+Integer.parseInt(deliveryChargesPaisaString);

                                            bundle.putString("total_rupees", String.valueOf(i));
                                            bundle.putString("total_paisa", String.valueOf(j));

                                        }

                                        Intent intent = new Intent(getApplicationContext(), PriceDetailsActivity.class);
                                        intent.putExtras(bundle);
//                            AppConstants.TAILOR_DIRECT_ORDER = "";

                                        startActivity(intent);

                                    }
                                },300);


                            }

                            else {
                                bundle.putString("total_rupees", total_rupees.getText().toString());
                                bundle.putString("total_paisa", total_paisa.getText().toString());
                                Intent intent = new Intent(getApplicationContext(), PriceDetailsActivity.class);
                                intent.putExtras(bundle);
//                            AppConstants.TAILOR_DIRECT_ORDER = "";

                                startActivity(intent);
                            }

                        }

                    }
                } else {

                    if (calender_text.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please Select the Delivery Date", Toast.LENGTH_LONG).show();
                    } else {


//                    if (AppConstants.INITIATED_BY.equalsIgnoreCase("Tailor")) {
//
//                        if (AppConstants.TOTAL_RUPEES.equalsIgnoreCase("")) {
//                            Toast.makeText(getApplicationContext(), "Enter Minimum Charges", Toast.LENGTH_LONG).show();
//                            ViewPager viewPager = (ViewPager) getApplicationContext().findViewById(
//                                    R.id.viewpager_order_confirmation);
//                            viewPager.setCurrentItem(0);
//
//                        } else if (total_rupees.getText().toString().isEmpty() ||
//                                total_rupees.getText().toString().equalsIgnoreCase("0.0")) {
//                            Toast.makeText(getApplicationContext(), "Enter Minimum Charge", Toast.LENGTH_LONG).show();
//                            ViewPager viewPager = (ViewPager) getActivity().findViewById(
//                                    R.id.viewpager_order_confirmation);
//                            viewPager.setCurrentItem(0);
//
//                        } else if (Float.parseFloat(total_rupees.getText().toString()) <
//                                Float.parseFloat(amount_paid.getText().toString())) {
//                            Toast.makeText(getApplicationContext(), "Amount Paid Exceeded Total Amount", Toast.LENGTH_LONG).show();
//                        } else {
//
//                            uploadOrderQuotationForTailorOrder();
//                            insertAttributeQuantityApiCall();
////                            insertDirectCustomerAmout(AppConstants.ORDER_ID);
//
//                            if (amount_paid.getText().toString().equalsIgnoreCase("0")) {
//                                updatePaymentStatus("1");
//                            } else {
//                                updatePaymentStatus("2");
//                            }
//                            insertApprovedTailor();
//                            startActivity(new Intent(getActivity(), HomeActivity.class));
//                            AppConstants.TOTAL_RUPEES = "";
//                            AppConstants.MATERIAL_ID = "";
//                            AppConstants.SUB_DRESS_TYPE_ID = "";
//                        }
//
//
//                    } else {

                        if (AppConstants.TOTAL_RUPEES.equalsIgnoreCase("")) {

                            Toast.makeText(getApplicationContext(), "Enter Minimum Charges", Toast.LENGTH_LONG).show();
//                            ViewPager viewPager = (ViewPager) getApplicationContext().findViewById(
//                                    R.id.viewpager_order_confirmation);
//                            viewPager.setCurrentItem(0);

                        } else {


                            uploadOrderQuotation();
                            insertAttributeQuantityApiCall();

//                            if (mOrderTypeNameStr.equalsIgnoreCase("Companies-Material") &&
//                                    mMeasurementTypeNameStr.equalsIgnoreCase("Manually")){
//                                    Toast.makeText(getActivity(), "Appointment Created", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(getActivity(), HomeActivity.class));
//                                AppConstants.TOTAL_RUPEES = "";
//                            }else {
                            if (mOrderTypeBoolStr.equalsIgnoreCase("true") || mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                                if (AppConstants.INITIATED_BY.equalsIgnoreCase("Tailor")) {
                                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                    AppConstants.TOTAL_RUPEES = "";
                                } else {
                                    startActivity(new Intent(getApplicationContext(), AppointmentDetails.class));
                                    AppConstants.TOTAL_RUPEES = "";
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Appointment Created", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                                AppConstants.TOTAL_RUPEES = "";
                            }
//                            }


                        }

//                    }

                    }

                }
            }
        });

        measurement_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                rupees_total =
                        Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +


                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        measurement_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +


                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        custumization_stitching_charge_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                rupees_total =
                        Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +


                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        custumization_stitching_charge_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +


                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        appointment_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                rupees_total =
                        Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +


                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        appointment_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +


                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        urgent_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                rupees_total =
                        Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +


                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        urgent_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +


                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
//                    total_rupees.setText(add + "." + lastDigit);
                    total_rupees.setText(String.valueOf(add));

                    total_paisa.setText(String.valueOf(lastDigit));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(String.valueOf(rupees_total));
                    total_paisa.setText(String.valueOf(paisa_total));

                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    public void setAdapter(ArrayList<String> mArrayList) {


        alertDismiss(mOrderConfirmDialog);
        mOrderConfirmDialog = getDialog(OrderConfirmationActivity.this, R.layout.adapter_one_to_nine);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mOrderConfirmDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        RecyclerView recyclerView;

        /*Init view*/

        recyclerView = mOrderConfirmDialog.findViewById(R.id.countryList);
        mConfirmAdapter = new AdapterOneToNine(OrderConfirmationActivity.this, mArrayList);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(OrderConfirmationActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mConfirmAdapter);

        alertShowing(mOrderConfirmDialog);

    }

    public void getDressTypes() {

        restService.getMeasurementDetails(AppConstants.ORDER_ID).enqueue(new Callback<GetMeasurementDetailsResponse>() {
            @Override
            public void onResponse(Call<GetMeasurementDetailsResponse> call, Response<GetMeasurementDetailsResponse> response) {
                try {

//                    getAttributeDetailModals = new ArrayList<>();
                    getDressSubtypeModals = new ArrayList<>();
//                    getMaterialTypes = new ArrayList<>();
                    getAttributeDetailModals = response.body().getResult().getGetAttributeDetails();
                    getDressSubtypeModals = response.body().getResult().getGetDressSubtypes();
//                    getMaterialTypes = response.body().getResult().getGetMaterialType();
                    try {
//                        Glide.with(getActivity())
//                                .load(GlobalData.SERVER_URL + "Images/DressSubType/" + getDressSubtypeModals.get(0).getImage())
//                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                                .into(orderConfirmation.get_dress_sub_type_image);

                        Glide.with(getApplicationContext())
                                .load(GlobalData.SERVER_URL + "Images/DressSubType/" +
                                        getDressSubtypeModals.get(0).getImage())
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true).placeholder(R.drawable.placeholder))
                                .into(get_dress_sub_type_image);

//                        apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))

                        dialogManager.hideProgress();
                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());


                    }

//                    attribute_types.setText(getMaterialTypes.get(0).getMaterialInArabic());
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

                        get_dress_sub_type_name_txt.setText(getDressSubtypeModals.get(0).getNameInArabic());

                    } else {
                        get_dress_sub_type_name_txt.setText(getDressSubtypeModals.get(0).getNameInEnglish());


                    }


//                    setGenderList(getAttributeDetailModals);


                } catch (Exception e) {
                    getDressSubtypeModals = new ArrayList<>();
//                    getMaterialTypes = new ArrayList<>();

                }


            }

            @Override
            public void onFailure(Call<GetMeasurementDetailsResponse> call, Throwable t) {

            }
        });

    }

    public void getDefaultChargesCharges() {


        restService.getDefaultCharges().enqueue(new Callback<GetTailorDefaultChargesResponse>() {
            @Override
            public void onResponse(Call<GetTailorDefaultChargesResponse> call, Response<GetTailorDefaultChargesResponse> response) {
                getTailorDefaultChargesEntities = response.body().getResult();


//                custumization_stitching_charge_rupees.setText(String.valueOf(getTailorDefaultChargesEntities.get(0).getAmount()));

//                custumization_stitching_charge_rupees.setText(separated[0]);
//                custumization_stitching_charge_paisa.setText(separated[1]);

                String s1 = String.valueOf(response.body().getResult().get(2).getAmount());
                String s2 = String.valueOf(response.body().getResult().get(3).getAmount());
                String s3 = String.valueOf(response.body().getResult().get(0).getAmount());
                String s4 = String.valueOf(response.body().getResult().get(1).getAmount());
                String s5 = String.valueOf(response.body().getResult().get(5).getAmount());

                String[] separated1 = s1.split("\\.");
                String[] separated2 = s2.split("\\.");
                String[] separated3 = s3.split("\\.");
                String[] separated4 = s4.split("\\.");
                String[] seperated5 = s5.split("\\.");
//              Stitching Charges
                r1 = separated1[0];
                p1 = separated1[1];
//              material Charges
                r2 = separated2[0];
                p2 = separated2[1];
//             Measurement Charges
                r3 = separated3[0];
                p3 = separated3[1];
//            Urgent Charges
                r4 = separated4[0];
                p4 = separated4[1];


                if (i == 1) {
                    urgent_charges_rupees.setText(r4);
                    urgent_charges_paisa.setText(p4);
                }

                if (j == 1) {
                    measurement_charges_rupees.setText(r3);
                    measurement_charges_paisa.setText(p3);
                }
                deliveryChargesRupeesString=seperated5[0];
                deliveryChargesPaisaString=seperated5[1];

                bundle.putString("delivery_charges_rupees", seperated5[0]);
                bundle.putString("delivery_charges_paisa", seperated5[1]);

            }

            @Override
            public void onFailure(Call<GetTailorDefaultChargesResponse> call, Throwable t) {

            }
        });
    }


    public void getMaterialChargesByTailor(String tailorId, String subTypeId, String materialId) {


        restService.getMaterialChargesByTailor(tailorId, subTypeId, materialId).enqueue(new Callback<GetMaterialChargesByTailorResponse>() {
            @Override
            public void onResponse(Call<GetMaterialChargesByTailorResponse> call, Response<GetMaterialChargesByTailorResponse> response) {
                try {
//                    if (response.body().getResponseMsg().equalsIgnoreCase("Success")) {


                    if (AppConstants.ORDER_TYPE_CHARGES == 3) {


                        if (response.body().getResult() == 0.0) {
//                    getDefaultChargesForMaterialtCharges();

                            appointment_charges_rupees.setText(r2);
                            appointment_charges_paisa.setText(p2);

                        } else {
                            String s1 = String.valueOf(response.body().getResult());
                            String[] separated1 = s1.split("\\.");

                            appointment_charges_rupees.setText(separated1[0]);
                            appointment_charges_paisa.setText(separated1[1]);
                        }

//                        if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {
//                            appointment_charges_rupees.setText("0");
//                            appointment_charges_paisa.setText("0");
//                            material_charges_layout.setVisibility(View.GONE);
//                        }


                    } else {
                        material_charges_layout.setVisibility(View.GONE);
                        appointment_charges_rupees.setText("0");
                        appointment_charges_paisa.setText("0");

                    }


//                    } else if (response.body().getResponseMsg().equalsIgnoreCase("Failure")) {
//                        appointment_charges_rupees.setText("0");
//                        appointment_charges_paisa.setText("0");
//
//                    }
                } catch (Exception e) {
                    appointment_charges_rupees.setText("0");
                    appointment_charges_paisa.setText("0");
                }

            }

            @Override
            public void onFailure(Call<GetMaterialChargesByTailorResponse> call, Throwable t) {
                appointment_charges_rupees.setText("0");
                appointment_charges_paisa.setText("0");

            }
        });
    }


    public void getStichingChargesbyTailor(String tailorId, String subTypeId) {


        restService.getStichingChargesbyTailor(tailorId, subTypeId).enqueue(new Callback<GetMaterialChargesByTailorResponse>() {
            @Override
            public void onResponse(Call<GetMaterialChargesByTailorResponse> call, Response<GetMaterialChargesByTailorResponse> response) {
                if (response.body().getResult() == 0.0) {
//                    getDefaultChargesForStitchingCharges();


                    custumization_stitching_charge_rupees.setText(r1);
                    custumization_stitching_charge_paisa.setText(p1);
                } else {
//                    custumization_stitching_charge_rupees.setText(String.valueOf(response.body().getResult()));

                    String s1 = String.valueOf(response.body().getResult());
                    String[] separated1 = s1.split("\\.");

                    custumization_stitching_charge_rupees.setText(separated1[0]);
                    custumization_stitching_charge_paisa.setText(separated1[1]);

                }

            }

            @Override
            public void onFailure(Call<GetMaterialChargesByTailorResponse> call, Throwable t) {

            }
        });
    }

    public void getAppointmentMaterialApiCall() {
        if (NetworkUtil.isNetworkAvailable(getApplicationContext())) {

            APIRequestHandler.getInstance().getAppointmentMaterialActApi(AppConstants.REQUEST_LIST_ID,
                    OrderConfirmationActivity.this);


        } else {
            DialogManager.getInstance().showNetworkErrorPopup(getApplicationContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMaterialApiCall();
                }
            });
        }
    }

    public void getAppointmentMeasurementApiCall() {
        if (NetworkUtil.isNetworkAvailable(getApplicationContext())) {

            APIRequestHandler.getInstance().getAppointmentMeasurementActApi(AppConstants.REQUEST_LIST_ID,
                    OrderConfirmationActivity.this);


        } else {
            DialogManager.getInstance().showNetworkErrorPopup(getApplicationContext(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMeasurementApiCall();
                }
            });
        }
    }


    public void uploadOrderQuotation() {
        try {

            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences.getString("TailorId", ""));
            map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            map.put("StichingTime", text_days_required.getText().toString());
            map.put("TailorCharges[0][TailorChargesId]", String.valueOf(1));
            map.put("TailorCharges[0][Amount]", custumization_stitching_charge_rupees.getText().toString() + "." + custumization_stitching_charge_paisa.getText().toString());
            map.put("TailorCharges[1][TailorChargesId]", String.valueOf(2));
            map.put("TailorCharges[1][Amount]", appointment_charges_rupees.getText().toString() + "." + appointment_charges_paisa.getText().toString());
            map.put("TailorCharges[2][TailorChargesId]", String.valueOf(3));
            map.put("TailorCharges[2][Amount]", measurement_charges_rupees.getText().toString() + "." + measurement_charges_paisa.getText().toString());
            map.put("TailorCharges[3][TailorChargesId]", String.valueOf(4));
            map.put("TailorCharges[3][Amount]", urgent_charges_rupees.getText().toString() + "." + urgent_charges_paisa.getText().toString());
//            map.put("TailorCharges[4][TailorChargesId]", String.valueOf(5));
//            map.put("TailorCharges[4][Amount]", urgent_charges_rupees.getText().toString() + "." + urgent_charges_paisa.getText().toString());
//            map.put("TailorCharges[5][TailorChargesId]", String.valueOf(6));
//            map.put("TailorCharges[5][Amount]", delivery_charges_rupees.getText().toString() + "." + delivery_charges_paisa.getText().toString());
//            map.put("TailorCharges[6][TailorChargesId]", String.valueOf(7));
//            map.put("TailorCharges[6][Amount]", service_charge_rupees.getText().toString() + "." + service_charge_paisa.getText().toString());
//            map.put("TailorCharges[7][TailorChargesId]", String.valueOf(8));
//            map.put("TailorCharges[7][Amount]", tax_rupees.getText().toString() + "." + tax_paisa.getText().toString());
            map.put("ApproximateDeliveryTime", calender_text.getText().toString());
//        map.put("TailorId", sharedPreferences.getString("TailorId",""));
//        map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            restService.orderQuotation(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + rupees_total + paisa_total, Toast.LENGTH_LONG).show();
//                    Toast.makeText(getActivity(), response.body().getResponseMsg() + total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                    isApproveOrder();

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();

        }
    }

    public void uploadOrderQuotationTailorOrder() {
        try {

            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences.getString("TailorId", ""));
            map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            map.put("StichingTime", text_days_required.getText().toString());
            map.put("TailorCharges[0][TailorChargesId]", String.valueOf(1));
            map.put("TailorCharges[0][Amount]", custumization_stitching_charge_rupees.getText().toString() + "." + custumization_stitching_charge_paisa.getText().toString());
            map.put("TailorCharges[1][TailorChargesId]", String.valueOf(2));
            map.put("TailorCharges[1][Amount]", appointment_charges_rupees.getText().toString() + "." + appointment_charges_paisa.getText().toString());
            map.put("TailorCharges[2][TailorChargesId]", String.valueOf(3));
            map.put("TailorCharges[2][Amount]", measurement_charges_rupees.getText().toString() + "." + measurement_charges_paisa.getText().toString());
            map.put("TailorCharges[3][TailorChargesId]", String.valueOf(4));
            map.put("TailorCharges[3][Amount]", urgent_charges_rupees.getText().toString() + "." + urgent_charges_paisa.getText().toString());
            map.put("ApproximateDeliveryTime", calender_text.getText().toString());

            map.put("Amount", calender_text.getText().toString());
            map.put("Balance", calender_text.getText().toString());
            map.put("PaymentType", AppConstants.PAYMENT_TYPE);
            map.put("PaymentMode", AppConstants.PAYMENT_MODE);
//        map.put("TailorId", sharedPreferences.getString("TailorId",""));
//        map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            restService.orderQuotationTailorOrder(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + rupees_total + paisa_total, Toast.LENGTH_LONG).show();
//                    Toast.makeText(getActivity(), response.body().getResponseMsg() + total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                    isApproveOrder();

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "" + e, Toast.LENGTH_LONG).show();

        }
    }


    public void isApproveOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("IsApproved", "1");
        map.put("Type", "Tailor");

        restService.approveOrder(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void insertAttributeQuantityApiCall() {
        InsertAttributeQuantityApicallModal insertAttributeQuantityApicallModal = new InsertAttributeQuantityApicallModal();

        ArrayList<InsertAttributeApiCallEntity> insertAttributeApiCallEntities = new ArrayList<>();

        insertAttributeQuantityApicallModal.setMaterialQty(text_material_required.getText().toString());
        insertAttributeQuantityApicallModal.setOrderId(AppConstants.ORDER_ID);

        materialList = new ArrayList<>();


        Set keys = AppConstants.MATERIAL_MAP.keySet();
        Iterator itr = keys.iterator();

        String key;
        String value;
        while (itr.hasNext()) {

            key = (String) itr.next();
            value = (String) AppConstants.MATERIAL_MAP.get(key);
            System.out.println(key + " - " + value);
            materialList.add(value);

        }


        for (int i = 0; i < getAttributeDetailModals.size(); i++) {
            InsertAttributeApiCallEntity insertAttributeApiCallEntity = new InsertAttributeApiCallEntity();
            insertAttributeApiCallEntity.setAttributeId(String.valueOf(getAttributeDetailModals.get(i).getCustomizationAttributeId()));
            insertAttributeApiCallEntity.setAttributeTypeId(String.valueOf(getAttributeDetailModals.get(i).getAttributeImageId()));
            if (materialList.size() == getAttributeDetailModals.size()) {
                insertAttributeApiCallEntity.setQuantity(materialList.get(i));
            } else {


                insertAttributeApiCallEntity.setQuantity("0");

            }


            insertAttributeApiCallEntities.add(insertAttributeApiCallEntity);
        }

        insertAttributeQuantityApicallModal.setAttributeQuantity(insertAttributeApiCallEntities);


        restService.insertAttributeQuantity(insertAttributeQuantityApicallModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + orderPricingFragment.rupees_total + orderPricingFragment.paisa_total, Toast.LENGTH_LONG).show();
//                Toast.makeText(getActivity(), response.body().getResponseMsg() + orderPricingFragment.total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                AppConstants.MATERIAL_MAP = new HashMap<>();
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AppointmentMaterialResponse) {
            AppointmentMaterialResponse mResponse = (AppointmentMaterialResponse) resObj;
            if (mResponse.getResult().size() > 0) {
                mOrderTypeNameStr = mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish();
                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")) {

                    mOrderTypeBoolStr = "false";


                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")) {

                    mOrderTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")) {

                    mOrderTypeBoolStr = "true";
                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Tailor Come To Your Place")) {

                    mOrderTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Manually")) {
                    mOrderTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Go to Tailor Shop")) {

                    mOrderTypeBoolStr = "true";

                }

            }

        }
        if (resObj instanceof AppointmentMeasurementResponse) {
            AppointmentMeasurementResponse mResponse = (AppointmentMeasurementResponse) resObj;
            if (mResponse.getResult().size() > 0) {
                mMeasurementTypeNameStr = mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish();
                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")) {

                    mMeasurementTypeBoolStr = "false";
                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Manually")) {
                    mMeasurementTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")) {

                    mMeasurementTypeBoolStr = "true";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Own Material-Courier the Material")) {

                    mMeasurementTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Companies-Material")) {
                    mMeasurementTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")) {

                    mMeasurementTypeBoolStr = "true";
                }

            }
        }
    }

    public void initView() {

        ButterKnife.bind(this);

//        setupUI(mAddReferencePayLay);

        setHeader();
//        mAddImageList = new ArrayList<>();
//
//        mAddImageList = new ArrayList<>();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderConfirmationActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

//        if (!mUserDetailsEntityRes.getHINT_ON_OFF().equalsIgnoreCase("OFF")) {
//            getHintDialog();
//
//        }

    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(getResources().getString(R.string.order_quotation));
        mRightSideImg.setVisibility(View.VISIBLE);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("TAILOR_DIRECT_ORDER")) {


//


                    DialogManager.getInstance().showOptionPopup(OrderConfirmationActivity.this,
                            "Order Will Be Cancelled" + "\n" + "Are You Sure Want To Go back",
                            getResources().getString(R.string.yes), getResources().getString(R.string.no),
                            mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                                @Override
                                public void onNegativeClick() {


                                }

                                @Override
                                public void onPositiveClick() {
//                                    mBackHandleString = "true";
//
//                                    getLanguage();
//                                    onBackPressed();


                                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    AppConstants.TAILOR_DIRECT_ORDER = "";

                                    AppConstants.HEAD = "";
                                    AppConstants.NECK = "";
                                    AppConstants.CHEST = "";
                                    AppConstants.WAIST = "";
                                    AppConstants.THIGH = "";
                                    AppConstants.ANKLE = "";
                                    AppConstants.KNEE = "";

                                    AppConstants.OVER_ALL_HEIGHT = "";
                                    AppConstants.SHORTS = "";
                                    AppConstants.OUTSEAM = "";
                                    AppConstants.INSEAM = "";

                                    AppConstants.SHOULDER = "";
                                    AppConstants.HALF_SLEEVE = "";
                                    AppConstants.BICEP = "";
                                    AppConstants.HIP = "";
                                    AppConstants.BOTTOM = "";

                                    AppConstants.HEIGHT = "";
                                    AppConstants.SLEEVE = "";
                                    AppConstants.WRIST = "";

                                    AppConstants.WOMEN_OVER_BUST = "";
                                    AppConstants.WOMEN_UNDER_BUST = "";
                                    AppConstants.WOMEN_HIP_BONE = "";
                                    AppConstants.WOMEN_THIGH = "";
                                    AppConstants.WOMEN_KNEE = "";
                                    AppConstants.WOMEN_CALF = "";
                                    AppConstants.WOMEN_ANKLE = "";

                                    AppConstants.WOMEN_HEAD = "";
                                    AppConstants.WOMEN_NECK = "";
                                    AppConstants.WOMEN_BUST = "";
                                    AppConstants.WOMEN_WAIST = "";
                                    AppConstants.WOMEN_FULL_HIP = "";

                                    AppConstants.WOMEN_HEIGHT = "";
                                    AppConstants.WOMEN_STW = "";
                                    AppConstants.WOMEN_NLTC = "";
                                    AppConstants.WOMENT_NLTB = "";
                                    AppConstants.WOMEN_STHB = "";
                                    AppConstants.WOMEN_WTHB = "";
                                    AppConstants.WOMEN_HTH = "";
                                    AppConstants.WOMEN_INSEAM = "";
                                    AppConstants.WOMEN_OUTSEAM = "";

                                    AppConstants.WOMEN_SHOULDER = "";
                                    AppConstants.WOMEN_BICEP = "";
                                    AppConstants.WOMEN_WRIST = "";

                                    AppConstants.WOMEN_SLEEVE = "";

                                    AppConstants.BOY_HEAD = "";
                                    AppConstants.BOY_NECK = "";
                                    AppConstants.BOY_CHEST = "";
                                    AppConstants.BOY_WAIST = "";
                                    AppConstants.BOY_THIGH = "";
                                    AppConstants.BOY_KNEE = "";
                                    AppConstants.BOY_ANKLE = "";

                                    AppConstants.BOY_OVER_ALL_HEIGHT = "";
                                    AppConstants.BOY_SHORTS = "";
                                    AppConstants.BOY_OUTSEAM = "";
                                    AppConstants.BOY_INSEAM = "";

                                    AppConstants.BOY_SHOULDER = "";
                                    AppConstants.BOY_HALFSLEEVE = "";
                                    AppConstants.BOY_HIP = "";
                                    AppConstants.BOY_BICEP = "";
                                    AppConstants.BOY_BOTTOM = "";

                                    AppConstants.BOY_HEIGHT = "";
                                    AppConstants.BOY_SLEEVE = "";
                                    AppConstants.BOY_WRIST = "";

                                    AppConstants.GIRL_OVER_BURST = "";
                                    AppConstants.GIRL_UNDER_BURST = "";
                                    AppConstants.GIRL_HIP_BONE = "";
                                    AppConstants.GIRL_THIGH = "";
                                    AppConstants.GIRL_KNEE = "";
                                    AppConstants.GIRL_CALF = "";
                                    AppConstants.GIRL_ANKLE = "";
                                    AppConstants.GIRL_HEAD = "";
                                    AppConstants.GIRL_NECK = "";
                                    AppConstants.GIRL_BUST = "";
                                    AppConstants.GIRL_WAIST = "";
                                    AppConstants.GIRL_FULL_HIP = "";
                                    AppConstants.GIRL_HEIGHT = "";
                                    AppConstants.GIRL_STW = "";
                                    AppConstants.GIRL_NLTC = "";
                                    AppConstants.GIRL_NLTB = "";
                                    AppConstants.GIRL_STHB = "";
                                    AppConstants.GIRL_WTHB = "";
                                    AppConstants.GIRL_HTH = "";
                                    AppConstants.GIRL_INSEAM = "";
                                    AppConstants.GIRL_OUTSEAM = "";
                                    AppConstants.GIRL_SHOULDER = "";
                                    AppConstants.GIRL_BICEP = "";
                                    AppConstants.GIRL_WRIST = "";
                                    AppConstants.GIRL_SLEEVE = "";
                                    AppConstants.MEASUREMENT_VALUES = new ArrayList<>();
                                    AppConstants.MEASUREMENT_ID = "";
                                    AppConstants.MEASUREMENT_MANUALLY = "";
                                    AppConstants.MEASUREMENT_MAP = new HashMap<>();
//                        AppConstants.SUB_DRESS_TYPE_ID = "";
                                    AppConstants.GET_ADDRESS_ID = "";

//                        AppConstants.PATTERN_ID = "";

                                    AppConstants.ORDER_TYPE_ID = "";

                                    AppConstants.MEASUREMENT_ID = "";

                                    AppConstants.MATERIAL_IMAGES = new ArrayList<>();

                                    AppConstants.REFERENCE_IMAGES = new ArrayList<>();

                                    AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

                                    AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

                                    AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

                                    AppConstants.DELIVERY_TYPE_ID = "";

                                    AppConstants.MEASUREMENT_TYPE = "";

                                    AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();
                                    startActivity(intent);
                                }
                            });


                } else {
                    onBackPressed();

                }
                break;
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bg_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
            custumization_stitching_charge_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            custumization_stitching_charge_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

            appointment_charges_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            appointment_charges_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

            measurement_charges_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            measurement_charges_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

            urgent_charges_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            urgent_charges_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);
            total_rupees.setBackgroundResource(R.drawable.edit_text_round_corner_right_grey_border);
            total_paisa.setBackgroundResource(R.drawable.edit_text_round_corner_grey_border);

//            aed_layout.setVerticalGravity(View.FOCUS_LEFT);
//            updateResources(AddReferenceScreen.this,"ar");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bg_layout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            updateResources(AddReferenceScreen.this,"en");

        }
    }

    @Override
    public void onBackPressed() {


        if (AppConstants.TAILOR_DIRECT_ORDER.equalsIgnoreCase("")) {
            super.onBackPressed();
        } else {
            DialogManager.getInstance().showOptionPopup(OrderConfirmationActivity.this,
                    "Order Will Be Cancelled" + "\n" + "Are You Sure Want To Go back",
                    getResources().getString(R.string.yes), getResources().getString(R.string.no),
                    mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                        @Override
                        public void onNegativeClick() {


                        }

                        @Override
                        public void onPositiveClick() {
//                                    mBackHandleString = "true";
//
//                                    getLanguage();
//                                    onBackPressed();


                            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            AppConstants.TAILOR_DIRECT_ORDER = "";
                            startActivity(intent);
                        }
                    });


        }
    }

    public void getTailorChargesStatus() {
        restService.getTailorChargesStatus(AppConstants.ORDER_ID).enqueue(new Callback<GetTailorChargesResponse>() {
            @Override
            public void onResponse(Call<GetTailorChargesResponse> call, Response<GetTailorChargesResponse> response) {
                AppConstants.MEASUREMENT_TYPE_CHARGES = response.body().getResult().get(0).getMeasurementType();
                AppConstants.ORDER_TYPE_CHARGES = response.body().getResult().get(0).getOrderType();
                AppConstants.SERVICE_TYPE_CHARGES = response.body().getResult().get(0).getServiceType();


            }

            @Override
            public void onFailure(Call<GetTailorChargesResponse> call, Throwable t) {
//                insertError("RequestViewActivity","getTailorChargesStatus()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });

    }
}
