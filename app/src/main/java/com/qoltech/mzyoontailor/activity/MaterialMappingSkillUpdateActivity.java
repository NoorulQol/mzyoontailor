package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GetMaterialMappingSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.MaterialMappingGetResposne;
import com.qoltech.mzyoontailor.wrappers.MaterialMappingGetResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialMappingSkillUpdateActivity extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;
    List<MaterialMappingGetResult> dressTypeEntities;
    RecyclerView genderList;
    GetMaterialMappingSkillUpdateAdapter genderSkillUpdateAdapter;
    SharedPreferences sharedPreferences;
    UserDetailsEntity mUserDetailsEntityRes;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initView();
        setContentView(R.layout.activity_dress_type_skill_update);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        restService = ((MainApplication) getApplication()).getClient();
        genderList = (RecyclerView) findViewById(R.id.genderList);
        dialogManager=new DialogManager();
        dialogManager.showProgress(getApplicationContext());
        initView();
        getDressType();


    }

    //
    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MaterialMappingSkillUpdateActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.material_mapping);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    public void getDressType() {
        restService.getMaterialMappingList().enqueue(new Callback<MaterialMappingGetResposne>() {
            @Override
            public void onResponse(Call<MaterialMappingGetResposne> call, Response<MaterialMappingGetResposne> response) {
                dressTypeEntities = response.body().getResult();
                setGenderList();
                dialogManager.hideProgress();
            }

            @Override
            public void onFailure(Call<MaterialMappingGetResposne> call, Throwable t) {


    insertError("MaterialMappingSkillUpdateActivity","getDressType()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        genderSkillUpdateAdapter = new GetMaterialMappingSkillUpdateAdapter(this, dressTypeEntities, restService);
        genderList.setAdapter(genderSkillUpdateAdapter);
    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }


//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
