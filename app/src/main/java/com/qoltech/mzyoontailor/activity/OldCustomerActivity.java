package com.qoltech.mzyoontailor.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.GenderScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.OldCustomerPojo;
import com.qoltech.mzyoontailor.wrappers.OldCustomerRespone;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OldCustomerActivity extends BaseActivity {
    Button old_customer_next;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;

    EditText userNameOldCustomer, phone_number, city;
    TextView textCountryCode, text_country;
    ImageView flagsImg, flagsImg_state;
    ApiService restService;
    SharedPreferences sharedPreferences;

    List<OldCustomerPojo> oldCustomerList;
    private UserDetailsEntity mUserDetailsEntityRes;
    FrameLayout country_code_lay_old;
    LinearLayout name_layout, userNameOldCustomerLayout;
    Gson gson;
    EditText old_customer_phone_no;
DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_customer);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        old_customer_next = (Button) findViewById(R.id.old_customer_next);
//        userNameOldCustomer = (EditText) findViewById(R.id.userNameOldCustomer);
        phone_number = (EditText) findViewById(R.id.old_customer_phone_no);
        city = (EditText) findViewById(R.id.city);
        textCountryCode = (TextView) findViewById(R.id.textCountryCode);
        text_country = (TextView) findViewById(R.id.text_country);
        flagsImg = (ImageView) findViewById(R.id.flagsImg);
        flagsImg_state = (ImageView) findViewById(R.id.flagsImg_state);
        old_customer_phone_no = (EditText) findViewById(R.id.old_customer_phone_no);
        userNameOldCustomer = (EditText) findViewById(R.id.userNameOldCustomer);
        country_code_lay_old = (FrameLayout) findViewById(R.id.country_code_lay_old);
        userNameOldCustomerLayout = (LinearLayout) findViewById(R.id.userNameOldCustomerLayout);
        sharedPreferences = getSharedPreferences("Existing_Customer", MODE_PRIVATE);
        sharedPreferences.getString("customer_phone_num", "");
        dialogManager=new DialogManager();
        getLanguage();
        initView();
        dialogManager.showProgress(getApplicationContext());
        getOldCustomers();
        old_customer_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), GenderScreen.class));
            }
        });

    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);
        restService = ((MainApplication) getApplication()).getClient();
        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.old_customer);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }


    public void getOldCustomers() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {
            restService.getOldCustomer(sharedPreferences.getString("country_code", ""),sharedPreferences.getString("customer_phone_num", "")).enqueue(new Callback<OldCustomerRespone>() {
                @Override
                public void onResponse(Call<OldCustomerRespone> call, Response<OldCustomerRespone> response) {
                    oldCustomerList = response.body().getResult();

                    try {
                        if (oldCustomerList == null) {

                            oldCustomerList = new ArrayList<>();
                        } else {
                            phone_number.setText(oldCustomerList.get(0).getPhoneNumber());
                            city.setText(oldCustomerList.get(0).getStateName());
                            textCountryCode.setText(oldCustomerList.get(0).getCountryCode());
                            text_country.setText(oldCustomerList.get(0).getCountryName());
                            userNameOldCustomer.setText("" + oldCustomerList.get(0).getUserName());

                            try {

                                Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "Images/Flags/" + oldCustomerList.get(0).getFlag())
                                        .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.gif_loader))
                                        .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .placeholder(R.drawable.placeholder)).into(flagsImg);

                            } catch (Exception ex) {
                                Log.e(AppConstants.TAG, ex.getMessage());
                            }

                            try {

                                Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "Images/Flags/" + oldCustomerList.get(0).getFlag())
                                        .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.gif_loader))
                                        .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                                .placeholder(R.drawable.placeholder)).into(flagsImg_state);

                            } catch (Exception ex) {
                                Log.e(AppConstants.TAG, ex.getMessage());
                            }

                            dialogManager.hideProgress();

                        }
                    } catch (Exception e) {

                    }


                }

                @Override
                public void onFailure(Call<OldCustomerRespone> call, Throwable t) {
                    insertError("OldCustomerActivity","getOldCustomer()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

                }
            });
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomerLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
            country_code_lay_old.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
            old_customer_phone_no.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }
//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
