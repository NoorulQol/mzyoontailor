package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.MeasurementSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.InsertMeasurementSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.MeasurementOneEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.InsertMeasurementSkillUpdateApiModal;
import com.qoltech.mzyoontailor.modal.MeasurementOneResponse;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeasurementSkillUpdate extends BaseActivity {
    Button next;
    EditText phone_number;
    String phoneNumber;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    SharedPreferences sharedPreferences;

    MeasurementSkillUpdateAdapter measurementSkillUpdateAdapter;
    ApiService restService;
    List<MeasurementOneEntity> measurementOneEntities;
    RecyclerView genderList;
    InsertMeasurementSkillUpdateApiModal insertMeasurementSkillUpdateApiModal;
    Button save;
    public UserDetailsEntity mUserDetailsEntityRes;
    TransparentProgressDialog transparentProgressDialog;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_skill_update);
        genderList = (RecyclerView) findViewById(R.id.genderList);
        restService = ((MainApplication) getApplication()).getClient();
//        initView();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        save = (Button) findViewById(R.id.save);
        dialogManager=new DialogManager();
        dialogManager.showProgress(getApplicationContext());
        initView();
        getGender();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogManager.showProgress(MeasurementSkillUpdate.this);
                insertMeasurementApi();
            }
        });
    }


    //    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(MeasurementSkillUpdate.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.measurement);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getGender() {
        restService.getMeasurementTypeSkillUpdate(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<MeasurementOneResponse>() {
            @Override
            public void onResponse(Call<MeasurementOneResponse> call, Response<MeasurementOneResponse> response) {

                measurementOneEntities = response.body().getResult();
                setGenderList();
                dialogManager.hideProgress();
            }

            @Override
            public void onFailure(Call<MeasurementOneResponse> call, Throwable t) {
                insertError("LocationSkillUpdateGetCountryActivity", "getMeasurementTypeSkillUpdate()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }

    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        measurementSkillUpdateAdapter = new MeasurementSkillUpdateAdapter(this, measurementOneEntities, restService);
        genderList.setAdapter(measurementSkillUpdateAdapter);
    }

    public void insertMeasurementApi() {

        ArrayList<InsertMeasurementSkillUpdateEntity> insertMeasurementSkillUpdateEntities = new ArrayList<>();

        for (int i = 0; i < AppConstants.SKILL_MEASUREMENT_ID.size(); i++) {
            InsertMeasurementSkillUpdateEntity insertMeasurementSkillUpdateEntity = new InsertMeasurementSkillUpdateEntity();
            insertMeasurementSkillUpdateEntity.setId(AppConstants.SKILL_MEASUREMENT_ID.get(i));
            insertMeasurementSkillUpdateEntities.add(insertMeasurementSkillUpdateEntity);
        }


        InsertMeasurementSkillUpdateApiModal insertMeasurementSkillUpdateApiModal = new InsertMeasurementSkillUpdateApiModal();
        insertMeasurementSkillUpdateApiModal.setTailorId(sharedPreferences.getString("TailorId", ""));
        insertMeasurementSkillUpdateApiModal.setMeasurementTypeId(insertMeasurementSkillUpdateEntities);


        restService.insertMeasurementSkillUpdate(insertMeasurementSkillUpdateApiModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


                        dialogManager.hideProgress();
                        Toast.makeText(getApplicationContext(), R.string.measurement_type_updated_sucessfully, Toast.LENGTH_LONG).show();
                        finish();
                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {


                insertError("LocationSkillUpdateGetCountryActivity", "insertMeasurementApi()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

}

