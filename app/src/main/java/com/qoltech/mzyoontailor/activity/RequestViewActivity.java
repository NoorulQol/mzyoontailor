package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.RequestViewAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetTailorChargesResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestViewActivity extends AppCompatActivity {
    TextView check;
    TextView title;
    FragmentTransaction ft;
    Toolbar toolbar;
    SharedPreferences sharedPreferences1;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    public UserDetailsEntity mUserDetailsEntityRes;
    public Gson gson;
    public String json;
    LinearLayout bgLayout;
    ApiService restService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_view);
        bgLayout = (LinearLayout) findViewById(R.id.bgLayout);
        restService = ((MainApplication) getApplication()).getClient();

        gson = new Gson();
        json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        getTailorChargesStatus();
        initView();


        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_req);
//
        // Create an adapter that knows which fragment should be shown on each page
        RequestViewAdapter adapter = new RequestViewAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs_req);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0c2c75"));
//        tabLayout.setSelectedTabIndicatorHeight((int) (50 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#000000"), Color.parseColor("#ffffff"));
        tabLayout.setupWithViewPager(viewPager);

    }

    private void setupWindowAnimations() {
        this.overridePendingTransition(R.anim.animation_enter,
                R.anim.animation_leave);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            this.overridePendingTransition(R.anim.animation_f_enter,
                    R.anim.animation_f_leave);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.request_parts);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomerLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            country_code_lay_old.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
//            old_customer_phone_no.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }

    public void getTailorChargesStatus() {
        restService.getTailorChargesStatus(AppConstants.ORDER_ID).enqueue(new Callback<GetTailorChargesResponse>() {
            @Override
            public void onResponse(Call<GetTailorChargesResponse> call, Response<GetTailorChargesResponse> response) {
                AppConstants.MEASUREMENT_TYPE_CHARGES = response.body().getResult().get(0).getMeasurementType();
                AppConstants.ORDER_TYPE_CHARGES = response.body().getResult().get(0).getOrderType();
                AppConstants.SERVICE_TYPE_CHARGES = response.body().getResult().get(0).getServiceType();


            }

            @Override
            public void onFailure(Call<GetTailorChargesResponse> call, Throwable t) {
     insertError("RequestViewActivity","getTailorChargesStatus()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });

    }

//     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


}