package com.qoltech.mzyoontailor.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.StateGetSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.State;
import com.qoltech.mzyoontailor.wrappers.StateResponse;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSkillUpdateGetStateActivity extends BaseActivity {
    StateGetSkillUpdateAdapter stateGetSkillUpdateAdapter;
    ApiService restService;
    List<State> getCountries;
    RecyclerView genderList;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    public UserDetailsEntity mUserDetailsEntityRes;
    TextView no_result_found;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_dress_type_skill_update);

        restService = ((MainApplication) getApplication()).getClient();
        genderList = (RecyclerView) findViewById(R.id.genderList);
        no_result_found = (TextView) findViewById(R.id.no_result_found);
        dialogManager = new DialogManager();
        dialogManager.showProgress(getApplicationContext());
        getCountry(AppConstants.COUNTRY_AREA_ID);
        initView();
    }


    public void getCountry(String countryId) {
        HashMap<String, String> map = new HashMap<>();
        map.put("Id", countryId);
        restService.stateList(map).enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {

                getCountries = response.body().getResult();

                if (getCountries.size() <= 0) {
                    no_result_found.setVisibility(View.VISIBLE);
                }
                setGenderList();


                dialogManager.hideProgress();
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {
                insertError("LocationSkillUpdateGetStateActivity", "getCountry()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

            }
        });
    }


    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        stateGetSkillUpdateAdapter = new StateGetSkillUpdateAdapter(this, getCountries, restService);
        genderList.setAdapter(stateGetSkillUpdateAdapter);
    }


    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(LocationSkillUpdateGetStateActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.location);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_sub_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_sub_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }


//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
