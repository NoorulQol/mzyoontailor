package com.qoltech.mzyoontailor.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.OldCustomerCountryCode;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.Country;
import com.qoltech.mzyoontailor.wrappers.CountryResponse;
import com.qoltech.mzyoontailor.wrappers.CustomerTypeResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerTypeActivity extends BaseActivity {
    Button next;
    EditText phone_number;
    String phoneNumber;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;


    ApiService restService;
    List<Country> countries;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    OldCustomerCountryCode countryCodeDialogAdapter;
    Country country;
    static boolean rememberLastSelection = false;
    static Context context;
    static String CCP_PREF_FILE = "CCP_PREF_FILE";
    static String selectionMemoryTag = "ccp_last_selection";
    public static TextView textCountrySelection;
    public static ImageView countrySpinner, flagsImg;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    NewCustomerActivity newCustomerActivity = new NewCustomerActivity();
    private UserDetailsEntity mUserDetailsEntityRes;
    Gson gson;
    FrameLayout country_code_lay;
    LinearLayout total_edittext_layout, phone_number_layout;
    TextView mobile_num_txt;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_type);
        restService = ((MainApplication) getApplication()).getClient();
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        sharedPreferences = getSharedPreferences("Existing_Customer", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        initView();


        textCountrySelection = findViewById(R.id.textCountry);
        flagsImg = findViewById(R.id.flagsImg);
        countrySpinner = findViewById(R.id.imgSpinner);


        next = (Button) findViewById(R.id.next);
        phone_number = (EditText) findViewById(R.id.phone_number);
        country_code_lay = (FrameLayout) findViewById(R.id.country_code_lay);
        total_edittext_layout = (LinearLayout) findViewById(R.id.total_edittext_layout);
        mobile_num_txt = (TextView) findViewById(R.id.mobile_num_txt);
//        phone_number_layout = (LinearLayout) findViewById(R.id.phone_number_layout);
        phoneNumber = phone_number.getText().toString();
        dialogManager = new DialogManager();

        getCountryList();
        getLanguage();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(), NewCustomerActivity.class));

                if (phone_number.getText().toString().isEmpty()) {

                    Toast.makeText(getApplicationContext(), R.string.enter_the_mobile_num, Toast.LENGTH_SHORT).show();

                } else {
                    AppConstants.COUNTRY_FLAG_CLICK = "COUNTRY_FLAG_CLICK";

                    getCustomerType();
                }

            }
        });
    }


    public void getCustomerType() {
//        startActivity(new Intent(getApplicationContext(), NewCustomerActivity.class));

        restService.getCustomerType(textCountrySelection.getText().toString(), phone_number.getText().toString()).enqueue(new Callback<CustomerTypeResponse>() {
            @Override
            public void onResponse(Call<CustomerTypeResponse> call, Response<CustomerTypeResponse> response) {

                try {
                    dialogManager.showProgress(CustomerTypeActivity.this);

//                    if (response.body().getResult().equalsIgnoreCase("New User"))

                    if (response.body().getResult().length() == 8) {

//                        Toast.makeText(getApplicationContext(), "New User", Toast.LENGTH_SHORT).show();
                        editor.putString("customer_phone_num", phone_number.getText().toString().trim());
                        editor.putString("country_code", textCountrySelection.getText().toString().trim());
                        AppConstants.CountryCode = textCountrySelection.getText().toString();
                        editor.putString("User_Type", response.body().getResult());
                        editor.commit();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                dialogManager.hideProgress();
                                startActivity(new Intent(getApplicationContext(), NewCustomerActivity.class));

                            }
                        }, 2000);

                    } else if (response.body().getResult().length() > 8) {
//                       String currentString = "Fruit: they taste good";
                        String currentString = response.body().getResult();
                        String[] separated = currentString.split(",");
                        AppConstants.DIRECT_USERS_ID = separated[1];
//                        AppConstants.GET_ADDRESS_ID = separated[1];
                        editor.putString("customer_phone_num", phone_number.getText().toString());
                        editor.putString("country_code", textCountrySelection.getText().toString().trim());
                        editor.putString("User_Type", response.body().getResult());
                        editor.commit();
//                        Toast.makeText(getApplicationContext(), "Existing User", Toast.LENGTH_SHORT).show();


                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                dialogManager.hideProgress();
                                startActivity(new Intent(getApplicationContext(), OldCustomerActivity.class));
                            }
                        }, 2000);


                    } else {
                        Toast.makeText(getApplicationContext(), "Connection Failure,Try Again Later", Toast.LENGTH_SHORT).show();
                        dialogManager.hideProgress();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Connection Failure,Try Again Later", Toast.LENGTH_SHORT).show();
                    dialogManager.hideProgress();
                }

            }

            @Override
            public void onFailure(Call<CustomerTypeResponse> call, Throwable t) {

            }
        });
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.customer_type);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                AppConstants.COUNTRY_FLAG="UnitedArabEmirates.png";

                AppConstants.TAILOR_DIRECT_ORDER="";
                break;
        }

    }

    private void showCoutrySelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
//        TextView myMsg = new TextView(this);
//        myMsg.setText("SELECT COUNTRY");
//        myMsg.setTextSize(20);
//        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
//        myMsg.setTextColor(Color.BLACK);

        TextView dialogTitle = new TextView(getApplicationContext());
        dialogTitle.setText(R.string.select_country);
        dialogTitle.setTextColor(getResources().getColor(R.color.black));
        dialogTitle.setGravity(Gravity.CENTER_HORIZONTAL);


        DisplayMetrics metrics;
        metrics = getApplicationContext().getResources().getDisplayMetrics();
        float Textsize = dialogTitle.getTextSize() / metrics.density;
        dialogTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, (getResources().getDimension(R.dimen.text9) * 1.2f));


        //set custom title
//        builder.setCustomTitle(myMsg);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCustomTitle(dialogTitle)
                .setView(view)
                .create();
        countryCodeDialogAdapter = new OldCustomerCountryCode(getApplicationContext(), countries, country, dialog);
        countryList.setAdapter(countryCodeDialogAdapter);
        dialog.show();

    }

    private void getCountryList() {

            try {
                restService.getCountry().enqueue(new Callback<CountryResponse>() {
                    @Override
                    public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {

                        if (response.isSuccessful()) {
                            countries = response.body().getResult();
                            if (countries == null) {
                                countries = new ArrayList<>();
                            }
                            setCountrySelectionList();
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CountryResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();

                            insertError("CustomerTypeActivity","getCountryList()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
    }

    public void onUserTappedCountry(Country country) {
        if (rememberLastSelection) {
            storeSelectedCountryNameCode(country.getPhoneCode());
        }
    }


    static void storeSelectedCountryNameCode(String selectedCountryNameCode) {
        //get the shared pref
        SharedPreferences sharedPref = context.getSharedPreferences(
                CCP_PREF_FILE, Context.MODE_PRIVATE);

        //we want to write in shared pref, so lets get editor for it
        SharedPreferences.Editor editor = sharedPref.edit();

        // add our last selection country name code in pref
        editor.putString(selectionMemoryTag, selectedCountryNameCode);

        //finally save it...
        editor.apply();
    }

    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < countries.size(); i++) {
            listSpinner.add(countries.get(i).getCountryName());
        }
        textCountrySelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCoutrySelectionList();
            }
        });
        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textCountrySelection.performClick();
            }
        });
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.total_edittext_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
            country_code_lay.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
            phone_number.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);
            updateResources(getApplicationContext(), "ar");
//            mobile_num_txt.setHint(getResources().getString(R.string.mobile_num));
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);
            ViewCompat.setLayoutDirection(findViewById(R.id.total_edittext_layout), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(getApplicationContext(), "en");


        }


//        submitButton.setText(getResources().getString(R.string.contin));
//        login_choose_language_txt.setText(getResources().getString(R.string.choose_location));
    }


    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
//    insertError("HomeActivity","getGenderList()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName,String methodName,String error,
                            String apiVersion,String deviceId,String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName",pageName);
        map.put("MethodName",methodName);
        map.put("Error", error);
        map.put("ApiVersion",apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type",type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        AppConstants.COUNTRY_FLAG="UnitedArabEmirates.png";

        super.onBackPressed();
    }
}
