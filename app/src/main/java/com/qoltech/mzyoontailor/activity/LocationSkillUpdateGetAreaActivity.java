package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.AreaGetSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.GetAreaEntity;
import com.qoltech.mzyoontailor.entity.InsertServiceLocationSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.GetAreaResponse;
import com.qoltech.mzyoontailor.modal.InsertServiceLocationSkillUpdateModal;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSkillUpdateGetAreaActivity extends BaseActivity {
    AreaGetSkillUpdateAdapter areaGetSkillUpdateAdapter;
    ApiService restService;
    List<GetAreaEntity> getCountries;
    RecyclerView genderList;
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    Button save;
    public UserDetailsEntity mUserDetailsEntityRes;
    SharedPreferences sharedPreferences;
    TransparentProgressDialog transparentProgressDialog;
    DialogManager dialogManager;
    AppCompatCheckBox select_all;
    ArrayList<String> strings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_list_skill_update);

        restService = ((MainApplication) getApplication()).getClient();
        genderList = (RecyclerView) findViewById(R.id.genderList);
        dialogManager = new DialogManager();
        initView();
        save = (Button) findViewById(R.id.save);
//        select_all = (AppCompatCheckBox) findViewById(R.id.select_all);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        getArea(sharedPreferences.getString("TailorId", ""), AppConstants.STATE_ID);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialogManager.showProgress(LocationSkillUpdateGetAreaActivity.this);

                insertServiceLocationTypeApi();
            }
        });


//        select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                } else {
//
//                }
//            }
//        });


    }


    public void getArea(String tailorId, String stateId) {
//        HashMap<String, String> map = new HashMap<>();
//        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
//        map.put("StateId", stateId);
        restService.getAreaApiSkillUpdate(tailorId, stateId).enqueue(new Callback<GetAreaResponse>() {
            @Override
            public void onResponse(Call<GetAreaResponse> call, Response<GetAreaResponse> response) {

                getCountries = response.body().getResult();
                setGenderList();

                for (int i = 0; i < getCountries.size(); i++) {
                    if (getCountries.get(i).get_switch() == true) {
                        AppConstants.SKILL_LOCATION_ID.add(String.valueOf(getCountries.get(i).getId()));
                    }
                }
            }

            @Override
            public void onFailure(Call<GetAreaResponse> call, Throwable t) {

            }
        });
    }


    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        genderList.setItemViewCacheSize(20);
        genderList.setDrawingCacheEnabled(true);
        genderList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        areaGetSkillUpdateAdapter = new AreaGetSkillUpdateAdapter(this, getCountries, restService);

        genderList.setAdapter(areaGetSkillUpdateAdapter);
    }


    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(LocationSkillUpdateGetAreaActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.location);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                AppConstants.SKILL_LOCATION_ID=new ArrayList<>();
                onBackPressed();
                break;
        }

    }


    public void insertServiceLocationTypeApi() {

        ArrayList<InsertServiceLocationSkillUpdateEntity> insertServiceLocationSkillUpdateEntities = new ArrayList<>();

        for (int i = 0; i < AppConstants.SKILL_LOCATION_ID.size(); i++) {
            InsertServiceLocationSkillUpdateEntity insertServiceLocationSkillUpdateEntity = new InsertServiceLocationSkillUpdateEntity();
            insertServiceLocationSkillUpdateEntity.setId(AppConstants.SKILL_LOCATION_ID.get(i));
            insertServiceLocationSkillUpdateEntities.add(insertServiceLocationSkillUpdateEntity);
        }


        InsertServiceLocationSkillUpdateModal insertServiceLocationSkillUpdateModal = new InsertServiceLocationSkillUpdateModal();
        insertServiceLocationSkillUpdateModal.setTailorId(sharedPreferences.getString("TailorId", ""));
        insertServiceLocationSkillUpdateModal.setStateId(AppConstants.STATE_ID);
        insertServiceLocationSkillUpdateModal.setAreaId(insertServiceLocationSkillUpdateEntities);


        restService.insertServiceLocationSkillUpdate(insertServiceLocationSkillUpdateModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


                        dialogManager.hideProgress();
                        Toast.makeText(getApplicationContext(), R.string.location_type_updated_sucessfully, Toast.LENGTH_LONG).show();
                        finish();
                        AppConstants.SKILL_LOCATION_ID = new ArrayList<>();
                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.measurement_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    public void onBackPressed() {

        AppConstants.SKILL_LOCATION_ID=new ArrayList<>();

        super.onBackPressed();
    }
}
