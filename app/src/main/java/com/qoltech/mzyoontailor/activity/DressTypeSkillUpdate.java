package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.DressTypesSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.DressTypeEntity;
import com.qoltech.mzyoontailor.entity.GenderEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.DressTypeResponse;
import com.qoltech.mzyoontailor.modal.SkillUpdateGetLocationModal;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DressTypeSkillUpdate extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;
    List<DressTypeEntity> dressTypeEntities;
    RecyclerView genderList;
    DressTypesSkillUpdateAdapter genderSkillUpdateAdapter;
    SharedPreferences sharedPreferences;
    Button reset;
    LinearLayout genderFilterLayout;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    List<GenderEntity> genderEntitiesModal;
    AlertDialog dialog;
    private UserDetailsEntity mUserDetailsEntityRes;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initView();
        setContentView(R.layout.activity_dress_type_skill_update);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        restService = ((MainApplication) getApplication()).getClient();
        reset = (Button) findViewById(R.id.reset);
        genderFilterLayout = findViewById(R.id.genderFilterLayout);
        genderList = (RecyclerView) findViewById(R.id.genderList);
        dialogManager=new DialogManager();
        initView();
        AppConstants.GETCUSTUMIZATION_ID_CLICK=new ArrayList<>();
        dialogManager.showProgress(getApplicationContext());
        getDressType();

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDressTypeSkills();
            }
        });

    }

    //
    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(DressTypeSkillUpdate.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.dress_type);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    public void getDressType() {
        restService.getDressTypeByGenderId(AppConstants.GENDER_ID).enqueue(new Callback<DressTypeResponse>() {
            @Override
            public void onResponse(Call<DressTypeResponse> call, Response<DressTypeResponse> response) {
                dressTypeEntities = response.body().getResult();
                setGenderList();
                dialogManager.hideProgress();
            }

            @Override
            public void onFailure(Call<DressTypeResponse> call, Throwable t) {
                insertError("DressTypeSkillUpdate","getDressType()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        genderSkillUpdateAdapter = new DressTypesSkillUpdateAdapter(this, dressTypeEntities, restService);
        genderList.setAdapter(genderSkillUpdateAdapter);
    }

    public void deleteDressTypeSkills() {


        restService.deleteSkillsdressType(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<SkillUpdateGetLocationModal>() {
            @Override
            public void onResponse(Call<SkillUpdateGetLocationModal> call, Response<SkillUpdateGetLocationModal> response) {


            }

            @Override
            public void onFailure(Call<SkillUpdateGetLocationModal> call, Throwable t) {
                insertError("DressTypeSkillUpdate","deleteDressTypeSkills()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }



    public void getLanguage(){

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        }else {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppConstants.GETCUSTUMIZATION_ID_CLICK=new ArrayList<>();
    }



    public void insertError(String pageName,String methodName,String error,
                            String apiVersion,String deviceId,String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName",pageName);
        map.put("MethodName",methodName);
        map.put("Error", error);
        map.put("ApiVersion",apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type",type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
