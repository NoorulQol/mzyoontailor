package com.qoltech.mzyoontailor.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.fragments.AppointmentFragment;
import com.qoltech.mzyoontailor.fragments.FaqFragments;
import com.qoltech.mzyoontailor.fragments.HomeFragment;
import com.qoltech.mzyoontailor.fragments.LanguageSettingFragment;
import com.qoltech.mzyoontailor.fragments.MyAccountFragment;
import com.qoltech.mzyoontailor.fragments.ReferFriendsFragment;
import com.qoltech.mzyoontailor.fragments.RewardsFragment;
import com.qoltech.mzyoontailor.fragments.SettingsFriendsFragment;
import com.qoltech.mzyoontailor.fragments.SkillsUpdateFragment;
import com.qoltech.mzyoontailor.fragments.TermsFragment;
import com.qoltech.mzyoontailor.fragments.TransactionFragment;
import com.qoltech.mzyoontailor.main.MyFirebaseInstanceIdService;
import com.qoltech.mzyoontailor.ui.AppointmentListScreen;
import com.qoltech.mzyoontailor.ui.SettingScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.TailorProfileGet;
import com.qoltech.mzyoontailor.wrappers.TailorProfileGetResponse;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public DrawerLayout drawer;
    private ImageView navigationIcon;
    Toolbar toolbar;
    int i = 0;
    BottomNavigationView bottomNavigationBar;
    private FragmentTransaction ft;
    FrameLayout frameLayout;
    NavigationView navigationView;
    public static Handler.Callback homeFragmentCallback;
    Context context;
    public TextView headerTitle, userName, userNameNavigation;
    SharedPreferences sharedPreferences, sharedPreferences1, sharedPreferencesTailorName;
    TransparentProgressDialog progressDialog;
    ApiService restService;
    CircularImageView userImage, userNavigationImage;
    SignUpResponse signUp = new SignUpResponse();
    List<TailorProfileGet> tailorProfileGetList;
    SharedPreferences.Editor editor, editor1, editor2;
    String navName = "sdsfvfdv";
    NavigationView nav_view;
    View hView;
    String tailorNameInEnglish = "";
    public UserDetailsEntity mUserDetailsEntityRes;
    public Gson gson;
    public String json;
    RelativeLayout bgLayout;
    LinearLayout home_action_bar;
    DialogManager dialogManager;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        bgLayout = (RelativeLayout) findViewById(R.id.bgLayout);
//        ViewCompat.setLayoutDirection(findViewById(R.id.container), ViewCompat.LAYOUT_DIRECTION_RTL);
//        if (isFirstTime()) {
//            // What you do when the Application is Opened First time Goes here
//
//            Toast.makeText(getApplicationContext(), "Welcome To Mzyoon", Toast.LENGTH_SHORT).show();
////            new ShowcaseView.Builder(this)
////                    .setTarget(new ActionViewTarget(this, ActionViewTarget.Type.HOME))
////                    .setContentTitle("Welcome To Mzyoon")
////                    .setContentText("This is highlighting the Home button")
////                    .hideOnTouchOutside()
////                    .build();
//        }


        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.menu_home));

        headerTitle = findViewById(R.id.headerTitle);
        userName = findViewById(R.id.userName);
        userNameNavigation = findViewById(R.id.userNameNavigation);
        drawer = findViewById(R.id.drawer_layout);
        navigationIcon = findViewById(R.id.navigationIcon);
        userImage = (CircularImageView) findViewById(R.id.userImage);
        userNavigationImage = (CircularImageView) findViewById(R.id.userNavigationImage);
        frameLayout = findViewById(R.id.content_frame);
        restService = ((MainApplication) getApplication()).getClient();
        bottomNavigationBar = findViewById(R.id.bottom_navigation_bar);
        nav_view = findViewById(R.id.nav_view);
        dialogManager = new DialogManager();
        home_action_bar = (LinearLayout) findViewById(R.id.home_action_bar);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        sharedPreferences1 = getSharedPreferences("ShopId", MODE_PRIVATE);
        sharedPreferencesTailorName = getSharedPreferences("TailorName", MODE_PRIVATE);
        initView();

        getProfile();
        getMenuText();
        deviceDetails();

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//            }
//        }, 2000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            disableShiftMode(bottomNavigationBar);
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.syncState();


        navigationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    //drawer is open
                    drawer.closeDrawer(Gravity.START);
//                    navigationIcon.setScaleX(1);

                } else {
                    drawer.openDrawer(Gravity.START);
//                    navigationIcon.setScaleX(-1);
                }
//

            }
        });

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                navigationIcon.setTranslationX(drawerView.getX() + drawerView.getWidth());

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //add this line to display menu1 when the activity is loaded
        //displaySelectedScreen(R.id.my_account);
        initBottomBar();

    }

    public void getProfile() {
        tailorProfileGetList = new ArrayList<>();
        restService.getTailorProfile(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<TailorProfileGetResponse>() {
            @Override
            public void onResponse(Call<TailorProfileGetResponse> call, Response<TailorProfileGetResponse> response) {

                if (response.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();

                    dialogManager.hideProgress();

                    try {
                        if (tailorProfileGetList == null) {

                            tailorProfileGetList = new ArrayList<>();
                        } else {
                            tailorProfileGetList = response.body().getResult();

                            sharedPreferencesTailorName.edit().putString("TailorName", tailorProfileGetList.get(0).getTailorNameInEnglish()).commit();
                            try {

                                navigationView = (NavigationView) findViewById(R.id.nav_view);
                                hView = navigationView.getHeaderView(0);
                                userNameNavigation = (TextView) hView.findViewById(R.id.userNameNavigation);
                                userNavigationImage = (CircularImageView) hView.findViewById(R.id.userNavigationImage);


                                if (tailorProfileGetList.get(0).getShopOwnerImageURL().equals("null")) {

                                } else {


                                    Glide.with(HomeActivity.this)
                                            .load(GlobalData.SERVER_URL + "Images/TailorImages/" + tailorProfileGetList.get(0).getShopOwnerImageURL())
                                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                            .into(userImage);

//                                userNameNavigation.setText(tailorProfileGetList.get(0).getTailorNameInEnglish());

//                                    navName = tailorProfileGetList.get(0).getTailorNameInEnglish();


                                    Glide.with(HomeActivity.this)
                                            .load(GlobalData.SERVER_URL + "Images/TailorImages/" + tailorProfileGetList.get(0).getShopOwnerImageURL())
                                            .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                            .into(userNavigationImage);


                                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
                                        userNameNavigation.setText(tailorProfileGetList.get(0).getTailorNameInArabic());

                                    } else {
                                        userNameNavigation.setText(tailorProfileGetList.get(0).getTailorNameInEnglish());

                                    }

                                }


                            } catch (Exception ex) {
//                    holder.mCountryFlagImg.setImageResource(R.drawable.demo_img);
                                Log.e(AppConstants.TAG, ex.getMessage());
                            }


                        }
                    } catch (Exception e) {
                        System.out.println("Something went wrong.");
                    }

                }
            }

            @Override
            public void onFailure(Call<TailorProfileGetResponse> call, Throwable t) {


//          PageName,MethodName,Error,ApiVersion,DeviceId,Type
                insertError("HomeActivity", "getProfile()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");
            }
        });
    }

    private void initBottomBar() {
        try {
            bottomNavigationBar.equals(true);

            bottomNavigationBar.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            Fragment fragCategory = null;
                            ft = getSupportFragmentManager().beginTransaction();

                            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                            getSupportActionBar().setHomeButtonEnabled(false);
                            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                            switch (item.getItemId()) {

                                case R.id.menu_home:
                                    fragCategory = new HomeFragment();
                                    headerTitle.setText(R.string.menu_home);


//                                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                                        headerTitle.setText("الصفحة الرئيسية");
//
//                                    } else {
//                                        headerTitle.setText("Home");
//
//                                    }
                                    break;
//                                case R.id.menu_profile:
//                                    headerTitle.setText(R.string.menu_profile);
//                                    break;
                                case R.id.menu_order:
                                    fragCategory = new CartFragment();
                                    headerTitle.setText(R.string.menu_orders);
                                    break;
                                case R.id.menu_cart:
                                    fragCategory = new CartFragment();
                                    headerTitle.setText(R.string.menu_cart);
                                    break;
                                case R.id.menu_contact:
                                    fragCategory = new SkillsUpdateFragment();
                                    headerTitle.setText(R.string.menu_contact);
                                    break;
                            }

                            ActionBar actionBar = getSupportActionBar();
                            if (actionBar != null) {
                                actionBar.setTitle(item.getTitle());
                            }
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.content_frame, fragCategory);
                            transaction.commit();

                            return true;
                        }
                    });
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, new HomeFragment());
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        homeFragmentCallback = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        navigationView.getMenu().getItem(1).setChecked(true);
                        new Handler(MyAccountFragment.callback).sendEmptyMessage(0);

                        navigationIcon.setScaleX(1);
                        break;
                    case 1:
                        navigationView.getMenu().getItem(2).setChecked(true);
                        new Handler(AppointmentFragment.callback).sendEmptyMessage(0);
                        drawer.closeDrawer(Gravity.START);

                        break;
                    case 2:
                        navigationView.getMenu().getItem(3).setChecked(true);
                        new Handler(TransactionFragment.callback).sendEmptyMessage(0);
                        break;
                    case 3:
                        navigationView.getMenu().getItem(4).setChecked(true);
                        new Handler(LanguageSettingFragment.callback).sendEmptyMessage(0);
                        break;

                    case 4:
                        navigationView.getMenu().getItem(5).setChecked(true);
                        new Handler(RewardsFragment.callback).sendEmptyMessage(0);
                        break;
                    case 5:
                        navigationView.getMenu().getItem(6).setChecked(true);
                        new Handler(ReferFriendsFragment.callback).sendEmptyMessage(0);
                        break;
                    case 6:
                        navigationView.getMenu().getItem(7).setChecked(true);
                        new Handler(FaqFragments.callback).sendEmptyMessage(0);
                        break;
                    case 7:
                        navigationView.getMenu().getItem(8).setChecked(true);
                        new Handler(TermsFragment.callback).sendEmptyMessage(0);
                        break;
                    case 8:
                        navigationView.getMenu().getItem(9).setChecked(true);
                        new Handler(SettingsFriendsFragment.callback).sendEmptyMessage(0);
                        break;

                    default:
                        break;
                }


                return false;
            }
        };
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
//            super.onBackPressed();

            exitFromApp();

        }
    }


    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.my_account:
//                fragment = new MyAccountFragment();
                startActivity(new Intent(getApplicationContext(), MyProfileActivity.class));
                navigationIcon.setImageResource(R.mipmap.side_menu_icon);
//                headerTitle.setText(R.string.my_account);
                break;
//            case R.id.my_shop:
//
//                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
//                } else {
//                    fragment = new MyShopFragment();
////                    headerTitle.setText(R.string.my_shop);
//                }
//                break;
            case R.id.appointment:
//                fragment = new AppointmentFragment();

                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
                } else {

                    startActivity(new Intent(getApplicationContext(), AppointmentListScreen.class));
//                    headerTitle.setText(R.string.book_an_appointment);
                }

                break;

            case R.id.order_quotation:
//                fragment = new AppointmentFragment();

                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
                } else {

                    startActivity(new Intent(getApplicationContext(), OrderQuotationListActivity.class));
//                    headerTitle.setText(R.string.book_an_appointment);
                }

                break;


//            case R.id.transaction:
//
//                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
//                } else {
//                    fragment = new TransactionFragment();
////                    headerTitle.setText(R.string.transaction);
//                }
//
//                break;
//            case R.id.laungage_setting:
//                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
//                } else {
////                    fragment = new LanguageSettingFragment();
//                    startActivity(new Intent(getApplicationContext(), LanguageSettingScreen.class));
////                    headerTitle.setText(R.string.laungage_settings);
//                }
//                break;
//            case R.id.rewards:
//                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
//                } else {
//                    fragment = new RewardsFragment();
////                    headerTitle.setText(R.string.rewards);
//                }
//                break;
//            case R.id.refer_friends:
//
//
//                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
//                } else {
//                    fragment = new ReferFriendsFragment();
////                    headerTitle.setText(R.string.refer_friends);
//                }
//                break;
//            case R.id.faq:
//                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
//                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
//                } else {
//                    fragment = new FaqFragments();
////                    headerTitle.setText(R.string.faq);
//                }
//                break;
            case R.id.terms:

                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
                } else {

//                    headerTitle.setText(R.string.terms_and_condition);

                    startActivity(new Intent(HomeActivity.this, TermsAndConditionActivity.class));
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse("http://mzyoon.com/Terms%20and%20Conditions.html"));
//                    startActivity(intent);
                }
                break;
            case R.id.settings:
                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
                } else {
//                    fragment = new LanguageSettingFragment();
                    startActivity(new Intent(getApplicationContext(), SettingScreen.class));
//                    headerTitle.setText(R.string.laungage_settings);
                }
                break;


            case R.id.Store:

                if (sharedPreferences1.getString("ShopId", "").equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Create a Shop Profile To Continue", Toast.LENGTH_LONG).show();
                } else {

//                    headerTitle.setText(R.string.terms_and_condition);

                    startActivity(new Intent(HomeActivity.this, Store.class));
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse("http://mzyoon.com/Terms%20and%20Conditions.html"));
//                    startActivity(intent);
                }

                break;
            case R.id.logOut:
                new AlertDialog.Builder(HomeActivity.this)
                        .setMessage(R.string.are_you_sure_want_to_lo)
                        .setPositiveButton(R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {


                                        editor = sharedPreferences.edit();
                                        editor1 = sharedPreferences1.edit();
                                        editor.putString("TailorId", "");

                                        editor.remove("TailorId");
                                        editor.clear();
                                        editor1.remove("ShopId");
                                        editor1.commit();
                                        editor.commit();


//


                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                //Do something after 100ms
                                                finish();
                                                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                                            }
                                        }, 2000);
                                    }
                                }).setNegativeButton(R.string.no, null).show();
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //calling the method displayselectedscreen and passing the id of selected menu
        displaySelectedScreen(item.getItemId());
        //make this method blank
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);

        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (@SuppressLint("NewApi") NoSuchFieldException | IllegalAccessException e) {
        }
    }

    public void deviceDetails() {
        MyFirebaseInstanceIdService mMyFirebaseInstanceIdService = new MyFirebaseInstanceIdService();
        mMyFirebaseInstanceIdService.onTokenRefresh();

        dialogManager.showProgress(HomeActivity.this);
        try {
            String deviceId = Settings.Secure.getString(getApplicationContext()
                    .getContentResolver(), Settings.Secure.ANDROID_ID);
//                    String countryCode = textCountrySelection.getText().toString().trim();
            HashMap<String, String> map = new HashMap<>();
            map.put("DeviceId", deviceId);
            map.put("Os", Build.VERSION.RELEASE);
            map.put("Manufacturer", Build.MANUFACTURER);
            map.put("CountryCode", sharedPreferences.getString("CountryCode", ""));
            map.put("PhoneNumber", sharedPreferences.getString("PhoneNumber", ""));
            map.put("Model", Build.MODEL);
            map.put("AppVersion", Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName());
            map.put("Type", "Tailor");
            map.put("Fcm", AppConstants.TOKEN);
            restService.deviceDetails(map).enqueue(new Callback<SignUpResponse>() {

                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                    signUp = response.body();
//                    Toast.makeText(getBaseContext(), AppConstants.TOKEN, Toast.LENGTH_SHORT).show();
////                    PageName,MethodName,Error,ApiVersion,DeviceId,Type
//                    insertError("HomeActivity","deviceDetails()","ajbck","ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                    Toast.makeText(getBaseContext(), "Connection Failure, try again!" + t.toString(), Toast.LENGTH_SHORT).show();


//          PageName,MethodName,Error,ApiVersion,DeviceId,Type
                    insertError("HomeActivity", "deviceDetails()", "" + t, "ApiVersion", AppConstants.DEVICE_ID_NEW, "Tailor");

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        userNameNavigation.setText(navName);
        getLanguage();

        getProfile();
        deviceDetails();



        AppConstants.TAILOR_DIRECT_ORDER = "";
        AppConstants.PATTERN_ID = "";
        AppConstants.SUB_DRESS_TYPE_ID = "";
        AppConstants.PAYMENT_MODE = "";
        AppConstants.PAYMENT_TYPE = "";

        AppConstants.HEAD = "";
        AppConstants.NECK = "";
        AppConstants.CHEST = "";
        AppConstants.WAIST = "";
        AppConstants.THIGH = "";
        AppConstants.ANKLE = "";
        AppConstants.KNEE = "";

        AppConstants.OVER_ALL_HEIGHT = "";
        AppConstants.SHORTS = "";
        AppConstants.OUTSEAM = "";
        AppConstants.INSEAM = "";

        AppConstants.SHOULDER = "";
        AppConstants.HALF_SLEEVE = "";
        AppConstants.BICEP = "";
        AppConstants.HIP = "";
        AppConstants.BOTTOM = "";

        AppConstants.HEIGHT = "";
        AppConstants.SLEEVE = "";
        AppConstants.WRIST = "";

        AppConstants.WOMEN_OVER_BUST = "";
        AppConstants.WOMEN_UNDER_BUST = "";
        AppConstants.WOMEN_HIP_BONE = "";
        AppConstants.WOMEN_THIGH = "";
        AppConstants.WOMEN_KNEE = "";
        AppConstants.WOMEN_CALF = "";
        AppConstants.WOMEN_ANKLE = "";

        AppConstants.WOMEN_HEAD = "";
        AppConstants.WOMEN_NECK = "";
        AppConstants.WOMEN_BUST = "";
        AppConstants.WOMEN_WAIST = "";
        AppConstants.WOMEN_FULL_HIP = "";

        AppConstants.WOMEN_HEIGHT = "";
        AppConstants.WOMEN_STW = "";
        AppConstants.WOMEN_NLTC = "";
        AppConstants.WOMENT_NLTB = "";
        AppConstants.WOMEN_STHB = "";
        AppConstants.WOMEN_WTHB = "";
        AppConstants.WOMEN_HTH = "";
        AppConstants.WOMEN_INSEAM = "";
        AppConstants.WOMEN_OUTSEAM = "";

        AppConstants.WOMEN_SHOULDER = "";
        AppConstants.WOMEN_BICEP = "";
        AppConstants.WOMEN_WRIST = "";

        AppConstants.WOMEN_SLEEVE = "";

        AppConstants.BOY_HEAD = "";
        AppConstants.BOY_NECK = "";
        AppConstants.BOY_CHEST = "";
        AppConstants.BOY_WAIST = "";
        AppConstants.BOY_THIGH = "";
        AppConstants.BOY_KNEE = "";
        AppConstants.BOY_ANKLE = "";

        AppConstants.BOY_OVER_ALL_HEIGHT = "";
        AppConstants.BOY_SHORTS = "";
        AppConstants.BOY_OUTSEAM = "";
        AppConstants.BOY_INSEAM = "";

        AppConstants.BOY_SHOULDER = "";
        AppConstants.BOY_HALFSLEEVE = "";
        AppConstants.BOY_HIP = "";
        AppConstants.BOY_BICEP = "";
        AppConstants.BOY_BOTTOM = "";

        AppConstants.BOY_HEIGHT = "";
        AppConstants.BOY_SLEEVE = "";
        AppConstants.BOY_WRIST = "";

        AppConstants.GIRL_OVER_BURST = "";
        AppConstants.GIRL_UNDER_BURST = "";
        AppConstants.GIRL_HIP_BONE = "";
        AppConstants.GIRL_THIGH = "";
        AppConstants.GIRL_KNEE = "";
        AppConstants.GIRL_CALF = "";
        AppConstants.GIRL_ANKLE = "";
        AppConstants.GIRL_HEAD = "";
        AppConstants.GIRL_NECK = "";
        AppConstants.GIRL_BUST = "";
        AppConstants.GIRL_WAIST = "";
        AppConstants.GIRL_FULL_HIP = "";
        AppConstants.GIRL_HEIGHT = "";
        AppConstants.GIRL_STW = "";
        AppConstants.GIRL_NLTC = "";
        AppConstants.GIRL_NLTB = "";
        AppConstants.GIRL_STHB = "";
        AppConstants.GIRL_WTHB = "";
        AppConstants.GIRL_HTH = "";
        AppConstants.GIRL_INSEAM = "";
        AppConstants.GIRL_OUTSEAM = "";
        AppConstants.GIRL_SHOULDER = "";
        AppConstants.GIRL_BICEP = "";
        AppConstants.GIRL_WRIST = "";
        AppConstants.GIRL_SLEEVE = "";
        AppConstants.MEASUREMENT_VALUES = new ArrayList<>();
        AppConstants.MEASUREMENT_ID = "";
        AppConstants.MEASUREMENT_MANUALLY = "";
        AppConstants.MEASUREMENT_MAP = new HashMap<>();
//                        AppConstants.SUB_DRESS_TYPE_ID = "";
        AppConstants.GET_ADDRESS_ID = "";

//                        AppConstants.PATTERN_ID = "";

        AppConstants.ORDER_TYPE_ID = "";

        AppConstants.MEASUREMENT_ID = "";

        AppConstants.MATERIAL_IMAGES = new ArrayList<>();

        AppConstants.REFERENCE_IMAGES = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICK_ARRAY = new ArrayList<>();

        AppConstants.CUSTOMIZATION_CLICKED_ARRAY = new ArrayList<>();

        AppConstants.ORDER_APPROVAL_SELECTED_TAILOR_LIST = new ArrayList<>();

        AppConstants.DELIVERY_TYPE_ID = "";

        AppConstants.MEASUREMENT_TYPE = "";

        AppConstants.CUSTOMIZATION_ATTRIBUTE_LIST = new ArrayList<>();

        AppConstants.SEASONAL_ID = "";
        AppConstants.PLACE_INDUSTRY_ID = "";
        AppConstants.BRANDS_ID = "";


        AppConstants.MATERIAL_ID = "";
        AppConstants.MATERIAL_TYPE_NAME = "";

        AppConstants.COLOR_ID = "";
        AppConstants.COLOUR_NAME = "";

        AppConstants.PATTERN_ID = "";
        AppConstants.PATTERN_NAME = "";

        AppConstants.SEASONAL_NAME = "";
        AppConstants.PLACE_OF_INDUSTRY_NAME = "";
        AppConstants.BRANDS_NAME = "";


        AppConstants.MEASUREMENT_TYPE_CHARGES = 0;
        AppConstants.ORDER_TYPE_CHARGES = 0;
        AppConstants.SERVICE_TYPE_CHARGES = 0;

    }

    /*App exit popup*/
    private void exitFromApp() {
        DialogManager.getInstance().showOptionPopup(this, getString(R.string.exit_msg),
                getString(R.string.yes), getString(R.string.no), mUserDetailsEntityRes.getLanguage(), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onPositiveClick() {
                        ActivityCompat.finishAffinity(HomeActivity.this);
                    }

                    @Override
                    public void onNegativeClick() {

                    }
                });

    }

    private boolean isFirstTime() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bottom_navigation_bar), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.home_action_bar), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameOldCustomerLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            country_code_lay_old.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
//            old_customer_phone_no.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);

            updateResources(HomeActivity.this, "ar");

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bottom_navigation_bar), ViewCompat.LAYOUT_DIRECTION_LTR);

            ViewCompat.setLayoutDirection(findViewById(R.id.home_action_bar), ViewCompat.LAYOUT_DIRECTION_LTR);

            updateResources(HomeActivity.this, "en");

        }
    }


    private static void updateResources(Context context,
                                        String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public void initView() {
        ButterKnife.bind(this);

        mUserDetailsEntityRes = new UserDetailsEntity();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(HomeActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
//        MyNotificationManager.getInstance(this).displayNotification("Greetings", "Thanks for using Mzyoon App");
        getLanguage();


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {

            insertLanguage("Arabic");

        } else {

            insertLanguage("English");

        }


        deviceDetails();
    }

    public void getMenuText() {

        Menu menu = nav_view.getMenu();
        Menu menu1 = bottomNavigationBar.getMenu();
        MenuItem my_account = menu.findItem(R.id.my_account);
        MenuItem appointment = menu.findItem(R.id.appointment);
        MenuItem settings = menu.findItem(R.id.settings);
        MenuItem terms = menu.findItem(R.id.terms);
        MenuItem logOut = menu.findItem(R.id.logOut);

        // set new title to the MenuItem
        MenuItem menu_home = menu1.findItem(R.id.menu_home);
        MenuItem menu_order = menu1.findItem(R.id.menu_order);
        MenuItem menu_cart = menu1.findItem(R.id.menu_cart);
        MenuItem menu_contact = menu1.findItem(R.id.menu_contact);


        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            my_account.setTitle("لي الحساب");
            appointment.setTitle("احجز موعد");
            settings.setTitle("الإعدادات");
            terms.setTitle("شروط و شرط");
            logOut.setTitle("الخروج");
            menu_home.setTitle("الصفحة الرئيسية");
            menu_order.setTitle("طلب");
            menu_cart.setTitle("عربة التسوق");
            menu_contact.setTitle("مهارة تحديث");

        } else {
            my_account.setTitle("My Account");
            appointment.setTitle("Book An Appointment");
            settings.setTitle("Settings");
            terms.setTitle("Terms and Condition");
            logOut.setTitle("Logout");
            menu_home.setTitle("Home");
            menu_order.setTitle("Order");
            menu_cart.setTitle("Cart");
            menu_contact.setTitle("Skill Update");

        }
    }

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


    public void insertLanguage(String language) {

        HashMap<String, String> map = new HashMap<>();

        map.put("Id", sharedPreferences.getString("TailorId", ""));
        map.put("language", language);
        map.put("Type", "Tailor");
        restService.insertLanguage(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


}