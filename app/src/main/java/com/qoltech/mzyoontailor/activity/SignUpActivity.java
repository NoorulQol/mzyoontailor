package com.qoltech.mzyoontailor.activity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.CountryCodeAdapter;
import com.qoltech.mzyoontailor.adapter.GetLanguageAdapter;
import com.qoltech.mzyoontailor.entity.GetCountryEntity;
import com.qoltech.mzyoontailor.entity.GetLanguageEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.CountryCodeResponse;
import com.qoltech.mzyoontailor.modal.GetLanguageResponse;
import com.qoltech.mzyoontailor.modal.LoginOTPResponse;
import com.qoltech.mzyoontailor.pinview.PinView;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.ui.OTPScreen;
import com.qoltech.mzyoontailor.ui.ProfileIntroScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.Country;
import com.qoltech.mzyoontailor.wrappers.CountryResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.ValidationResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qoltech.mzyoontailor.main.MZYOONApplication.getContext;

public class SignUpActivity extends BaseActivity {
    EditText phoneNo;
    public static TextView textCountrySelection;
    public static ImageView countrySpinner, flagsImg;
    Button submitButton;
    List<Country> countries;
    Country country;
    public SignUpResponse signUp = new SignUpResponse();
    public ValidationResponse validate = new ValidationResponse();
    ApiService restService;
    public static Handler.Callback callback;
    private AlertDialog.Builder builder;
    TransparentProgressDialog progressDialog;
    PinView pinview;
    AlertDialog.Builder alertDialogBuilder;
    Dialog alertDialog;
    public int counter;
    CountryCodeAdapter countryCodeDialogAdapter;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    static boolean rememberLastSelection = false;
    static Context context;
    static String CCP_PREF_FILE = "CCP_PREF_FILE";
    static String selectionMemoryTag = "ccp_last_selection";
    public static ProgressBar linearTimerView;
    TextView count_time;
    int pStatus = 0;
    private Handler handler = new Handler();
    Toolbar toolbar;
    CountDownTimer countDownTimer;
    Button resentOtpTxt;
    SharedPreferences sharedPreferences, sharedPreferencesShopId;
    SharedPreferences.Editor editor, editor1;
    public static ImageView lang_flagsImg;
    public static TextView choose_language_txt, login_choose_language_txt;
    RelativeLayout login_choose_language_lay;
    private UserDetailsEntity mUserDetailsEntity;
    ArrayList<String> mLanguageCountryList;
    ArrayList<Integer> mLanguageFlatList;
    private Dialog mDialog, mChooseLanguageDialog;
    private GetLanguageAdapter mLanguageAdapter;
    ArrayList<GetLanguageEntity> mLanguageEntity;
    public static ImageView mLangFlagImg;
    public TextView mChooseLanguageTxt;
    private ArrayList<GetCountryEntity> mCountryList;
    LinearLayout popup_country_alert_par_lay,pin_view_layout;

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        setupWindowAnimations();
        setupUI(findViewById(R.id.bgLayout));
        restService = ((MainApplication) getApplication()).getClient();
        AppConstants.DEVICE_ID_NEW = Settings.Secure.getString(getApplicationContext()
                .getContentResolver(), Settings.Secure.ANDROID_ID);
        textCountrySelection = findViewById(R.id.textCountry);
        flagsImg = findViewById(R.id.flagsImg);
        countrySpinner = findViewById(R.id.imgSpinner);
        phoneNo = findViewById(R.id.phoneNo);
        submitButton = findViewById(R.id.submitButton);
        lang_flagsImg = (ImageView) findViewById(R.id.lang_flag_img);
        choose_language_txt = findViewById(R.id.choose_language_txt);
        login_choose_language_txt = findViewById(R.id.login_choose_language_txt);
        login_choose_language_lay = findViewById(R.id.login_choose_language_lay);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        sharedPreferencesShopId = getSharedPreferences("ShopId", MODE_PRIVATE);

        mLanguageCountryList = new ArrayList<>();
//
//        mLanguageCountryList.add("English");
//        mLanguageCountryList.add("Arabic");
//
//        mLanguageFlatList = new ArrayList<>();
//
//        mLanguageFlatList.add(R.drawable.america_flag);
//        mLanguageFlatList.add(R.drawable.uae_national_flag);

        if (sharedPreferences.getString("TailorId", "").equalsIgnoreCase("")) {
            setContentView(R.layout.activity_signup);
            setupWindowAnimations();
            setupUI(findViewById(R.id.bgLayout));
            restService = ((MainApplication) getApplication()).getClient();
            textCountrySelection = findViewById(R.id.textCountry);
            flagsImg = findViewById(R.id.flagsImg);
            countrySpinner = findViewById(R.id.imgSpinner);
            phoneNo = findViewById(R.id.phoneNo);
            submitButton = findViewById(R.id.submitButton);
//            submitButton = findViewById(R.id.submitButton);
            lang_flagsImg = (ImageView) findViewById(R.id.lang_flag_img);
            choose_language_txt = findViewById(R.id.choose_language_txt);
            login_choose_language_txt = findViewById(R.id.login_choose_language_txt);
            login_choose_language_lay = findViewById(R.id.login_choose_language_lay);
            mLangFlagImg = findViewById(R.id.lang_flag_img);
            mChooseLanguageTxt = findViewById(R.id.choose_language_txt);
            sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
//            mLanguageCountryList = new ArrayList<>();
//
//            mLanguageCountryList.add("English");
//            mLanguageCountryList.add("Arabic");
//
//            mLanguageFlatList = new ArrayList<>();
//
//            mLanguageFlatList.add(R.drawable.america_flag);
//            mLanguageFlatList.add(R.drawable.uae_national_flag);

            getCountryList();
            getLanguage();
            mLanguageEntity = new ArrayList<>();
            getLanguageApi();
//            initView();
        } else {

            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }


        findViewById(R.id.submitButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (phoneNo.getText().length() <= 5) {
                    phoneNo.setError("Number should be more than 5 characters in length");

                    phoneNo.requestFocus();
                } else if (phoneNo.getText().length() > 20) {
                    phoneNo.setError("Number Exceeded the Limits");

                } else {


                    sentOtp();
                }


            }
        });


        login_choose_language_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mLanguageEntity.size() > 0) {
                    alertDismiss(mChooseLanguageDialog);
                    mChooseLanguageDialog = getDialog(SignUpActivity.this, R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mChooseLanguageDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
                        ViewCompat.setLayoutDirection(mChooseLanguageDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
                    } else {
                        ViewCompat.setLayoutDirection(mChooseLanguageDialog.findViewById(R.id.popup_country_alert_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mChooseLanguageDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mChooseLanguageDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mChooseLanguageDialog.findViewById(R.id.country_popup_recycler_view);
                    mLanguageAdapter = new GetLanguageAdapter(SignUpActivity.this, mLanguageEntity, mChooseLanguageDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mLanguageAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_language));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mChooseLanguageDialog.dismiss();
                        }
                    });

                    alertShowing(mChooseLanguageDialog);
                } else {
                    Toast.makeText(SignUpActivity.this, getResources().getString(R.string.sorry_no_result_found), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void showCoutrySelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        popup_country_alert_par_lay = view.findViewById(R.id.popup_country_alert_par_lay);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
//        TextView myMsg = new TextView(this);
//        myMsg.setText("SELECT COUNTRY");
//        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
//
//        myMsg.setTextColor(Color.BLACK);
        //set custom title
//        builder.setCustomTitle(myMsg);

        TextView dialogTitle = new TextView(getApplicationContext());
        dialogTitle.setText("SELECT COUNTRY");
        dialogTitle.setTextColor(getResources().getColor(R.color.black));
        dialogTitle.setGravity(Gravity.CENTER_HORIZONTAL);


        DisplayMetrics metrics;
        metrics = getApplicationContext().getResources().getDisplayMetrics();
        float Textsize = dialogTitle.getTextSize() / metrics.density;
        dialogTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, (getResources().getDimension(R.dimen.text9)*1.2f));
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCustomTitle(dialogTitle)
                .setView(view)
                .create();
        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(popup_country_alert_par_lay, ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(popup_country_alert_par_lay, ViewCompat.LAYOUT_DIRECTION_LTR);

        }
        countryCodeDialogAdapter = new CountryCodeAdapter(getApplicationContext(), countries, country, dialog);
        countryList.setAdapter(countryCodeDialogAdapter);

        dialog.show();

    }

    private void getCountryList() {
        if (NetworkUtil.isNetworkAvailable(SignUpActivity.this)) {

            try {
                restService.getCountry().enqueue(new Callback<CountryResponse>() {
                    @Override
                    public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {

                        try {
                            if (response.isSuccessful()) {
//                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();


                                countries = response.body().getResult();
                                if (countries == null) {
                                    countries = new ArrayList<>();
                                }
                                setCountrySelectionList();
                            } else if (response.errorBody() != null) {
                                Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onFailure(Call<CountryResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                        insertError("SignUpActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");


                    }
                });

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

            DialogManager.getInstance().showNetworkErrorPopup(SignUpActivity.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getCountryList();
                }
            });
        }
    }

    public void onUserTappedCountry(Country country) {
        if (rememberLastSelection) {
            storeSelectedCountryNameCode(country.getPhoneCode());
        }
    }


    static void storeSelectedCountryNameCode(String selectedCountryNameCode) {
        //get the shared pref
        SharedPreferences sharedPref = context.getSharedPreferences(
                CCP_PREF_FILE, Context.MODE_PRIVATE);

        //we want to write in shared pref, so lets get editor for it
        SharedPreferences.Editor editor = sharedPref.edit();

        // add our last selection country name code in pref
        editor.putString(selectionMemoryTag, selectedCountryNameCode);

        //finally save it...
        editor.apply();
    }

    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < countries.size(); i++) {
            listSpinner.add(countries.get(i).getCountryName());
        }
        textCountrySelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCoutrySelectionList();
                AppConstants.COUNRTY_CODE_ADAPTER = "SIGN_UP";
            }
        });
        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textCountrySelection.performClick();
            }
        });
    }

    public void sentOtp() {
        String mobNo = phoneNo.getText().toString().trim();
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(mobNo)) {
            Toast.makeText(getBaseContext(), "Please enter the Mobile Number", Toast.LENGTH_SHORT).show();
            focusView = phoneNo;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            if (new UtilService().isNetworkAvailable(SignUpActivity.this)) {

                progressDialog = new TransparentProgressDialog(SignUpActivity.this);
                progressDialog.show();

                try {
                    String deviceId = Settings.Secure.getString(getApplicationContext()
                            .getContentResolver(), Settings.Secure.ANDROID_ID);
                    String countryCode = textCountrySelection.getText().toString().trim();
                    HashMap<String, String> map = new HashMap<>();
                    map.put("DeviceId", deviceId);
                    map.put("CountryCode", countryCode);
                    map.put("PhoneNo", mobNo);

                    if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
                        map.put("Language", "arabic");
                    } else {
                        map.put("Language", "English");
                    }

                    restService.sentOtp(map).enqueue(new Callback<SignUpResponse>() {

                        @Override
                        public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                            signUp = response.body();
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            if (response.isSuccessful()) {
                                if (signUp != null && signUp
                                        .getResponseMsg().trim().equalsIgnoreCase("Success")) {
                                    showOtpPopUp(signUp.getResult());


                                } else if (signUp != null) {
                                    Toast.makeText(getBaseContext(), signUp.getResponseMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<SignUpResponse> call, Throwable t) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }

                            Toast.makeText(getBaseContext(), "Connection Failure, try again!" + t.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getBaseContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void showOtpPopUp(String mobileNo) {

        if (mobileNo == null || mobileNo.trim().isEmpty()) {
            Toast.makeText(getBaseContext(), "Invalid Mobile Number", Toast.LENGTH_SHORT).show();
            return;
        }
        LayoutInflater inflater = (LayoutInflater) SignUpActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.otp_popup, (ViewGroup) findViewById(R.id.layout));
        /*PopupWindow pwindow = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, true);
        pwindow.showAtLocation(view, Gravity.CENTER, 0, 0);*/
        pinview = (PinView) view.findViewById(R.id.pinView);
        pin_view_layout=(LinearLayout)view.findViewById(R.id.pin_view_layout);
        resentOtpTxt = view.findViewById(R.id.btn_resentOtp);
        linearTimerView = view.findViewById(R.id.circularProgressBar);
        resentOtpTxt.setEnabled(false);
        resentOtpTxt.setAlpha(0.5f);
        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.progressbar);
        final ProgressBar mProgress = (ProgressBar) view.findViewById(R.id.circularProgressBar);
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(0); // Secondary Progress
        mProgress.setMax(30); // Maximum Progress
        mProgress.setProgressDrawable(drawable);
        resentOtpTxt.setEnabled(false);
        resentOtpTxt.setAlpha(0.5f);
        final ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress", 0, 30);
        animation.setDuration(mProgress.getProgress());
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")){
            ViewCompat.setLayoutDirection(view.findViewById(R.id.layout), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(view.findViewById(R.id.pinView), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(view.findViewById(R.id.pin_view_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        }else {
            ViewCompat.setLayoutDirection(view.findViewById(R.id.layout), ViewCompat.LAYOUT_DIRECTION_LTR);
        }
//        otpPinView.onKey(otpPinView.getFocusedChild(), KeyEvent.KEYCODE_DEL, new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DEL));

        count_time = view.findViewById(R.id.count_time);
        view.findViewById(R.id.btn_change).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        countDownTimer.cancel();
                        counter = 0;
                    }
                });
        resentOtpTxt.setOnClickListener(
                new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onClick(View v) {

                        countDownTimer.cancel();
                        counter = 0;

                        reSentOtp();


                    }
                });

        pinview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (pinview.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(getBaseContext(), "Please enter Valid OTP", Toast.LENGTH_SHORT).show();
                    return;
                }
                otpValidation(pinview.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        int millsec = 30 * 1000;
        countDownTimer = new CountDownTimer(millsec, 1000) {
            public void onTick(long millisUntilFinished) {

                String formattedTime = String.format("%02d S",

                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
                count_time.setText(formattedTime);
                counter++;
                linearTimerView.setProgress(counter + 1);
            }

            public void onFinish() {
                count_time.setText("00 S");
                resentOtpTxt.setEnabled(true);
                resentOtpTxt.setAlpha(1f);
            }
        }.start();

        alertDialogBuilder = new AlertDialog.Builder(SignUpActivity.this, R.style.DialogAnimation);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setView(view);
        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.FILL_PARENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setAttributes(lp);
        setupUI(alertDialog.getWindow().getDecorView());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(pinview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
    }

    private void otpValidation(String otp) {

        boolean cancel = false;
        View focusView = null;
        if (pinview.getText().toString().length() <= 5) {
//            Toast.makeText(getBaseContext(), "please enter 6 digits", Toast.LENGTH_SHORT).show();
            focusView = pinview;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            if (new UtilService().isNetworkAvailable(SignUpActivity.this)) {
                progressDialog = new TransparentProgressDialog(SignUpActivity.this);
                progressDialog.show();
                try {
                    String deviceId = Settings.Secure.getString(getApplicationContext()
                            .getContentResolver(), Settings.Secure.ANDROID_ID);
                    String countryCode = textCountrySelection.getText().toString().trim();
                    String mobNo = phoneNo.getText().toString().trim();

                    HashMap<String, String> map = new HashMap<>();
                    map.put("DeviceId", deviceId);
                    map.put("CountryCode", countryCode);
                    map.put("PhoneNo", mobNo);
                    map.put("OTP", pinview.getText().toString());
                    map.put("Type", "Tailor");

                    restService.OtpValidation(map).enqueue(new Callback<ValidationResponse>() {
                        @Override
                        public void onResponse(Call<ValidationResponse> call, Response<ValidationResponse> response) {

                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }


                            try {
                                if (validate.getResult().equalsIgnoreCase("-1")) {
                                    Toast.makeText(getApplicationContext(), R.string.invalid_otp, Toast.LENGTH_SHORT).show();

                                } else {

//                                    Toast.makeText(getApplicationContext(), "OTP Successful Verified", Toast.LENGTH_SHORT).show();
                                    editor = sharedPreferences.edit();
                                    editor1 = sharedPreferencesShopId.edit();
                                    editor.putString("TailorId", response.body().getUserId());
                                    editor.putString("PhoneNumber", phoneNo.getText().toString());
                                    editor.putString("CountryCode", textCountrySelection.getText().toString());
//                                    if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {
//                                        Toast.makeText(getApplicationContext(), "Create The Shop Profile" + response.body().getShopProfileId(), Toast.LENGTH_LONG).show();
//                                        editor1.putString("ShopId", "");
//                                        editor1.commit();
//
//                                    } else {
//                                        editor1.putString("ShopId", String.valueOf(response.body().getShopProfileId()));
//                                        editor1.commit();
//                                    }
                                    if (response.body().getShopProfileId() != null) {
                                        editor1.putString("ShopId", String.valueOf(response.body().getShopProfileId()));
                                        editor1.commit();

                                    } else {
//                                        Toast.makeText(getApplicationContext(), "Create The Shop Profile" + response.body().getShopProfileId(), Toast.LENGTH_LONG).show();
                                        editor1.putString("ShopId", "");
                                        editor1.commit();

                                    }

                                    editor.commit();


//
                                    restService.IsprofileCreated(response.body().getUserId()).enqueue(new Callback<SignUpResponse>() {
                                        @Override
                                        public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                                            try {


                                                if (response.isSuccessful()) {
                                                    if (response.body().getResult().equalsIgnoreCase("New Tailor")) {
                                                        Intent i = new Intent(SignUpActivity.this, ProfileIntroScreen.class);
                                                        //i.putExtra("username", validate.getResponseMsg());


                                                        startActivity(i);
                                                        finish();

                                                    }
                                                }
                                                if (response.body().getResult().equalsIgnoreCase("Existing Tailor")) {
                                                    Intent i = new Intent(SignUpActivity.this, HomeActivity.class);
                                                    //i.putExtra("username", validate.getResponseMsg());
                                                    startActivity(i);
                                                    finish();


                                                }
                                            } catch (Exception e) {

                                                Intent i = new Intent(SignUpActivity.this, HomeActivity.class);
                                                //i.putExtra("username", validate.getResponseMsg());
                                                startActivity(i);
                                                finish();

                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<SignUpResponse> call, Throwable t) {
                                            insertError("SignUpActivity","OtpValidation()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

                                        }
                                    });
                                    final Handler handler = new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //Do something after 100ms
                                            finish();
                                        }
                                    }, 2000);

                                }

                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), "Invalid OTP", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<ValidationResponse> call, Throwable t) {
                            if (progressDialog != null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void isProfileCreated() {

    }

    public void reSentOtp() {
        String mobNo = phoneNo.getText().toString().trim();
        if (new UtilService().isNetworkAvailable(SignUpActivity.this)) {
            progressDialog = new TransparentProgressDialog(SignUpActivity.this);
            progressDialog.show();
            try {
                String deviceId = Settings.Secure.getString(getApplicationContext()
                        .getContentResolver(), Settings.Secure.ANDROID_ID);
                String countryCode = textCountrySelection.getText().toString().trim();
                HashMap<String, String> map = new HashMap<>();
                map.put("DeviceId", deviceId);
                map.put("CountryCode", countryCode);
                map.put("PhoneNo", mobNo);

                if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
                    map.put("Language", "arabic");
                } else {
                    map.put("Language", "English");
                }
                restService.sentOtp(map).enqueue(new Callback<SignUpResponse>() {
                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                        signUp = response.body();
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        if (response.isSuccessful()) {
//                            showOtpPopUp(signUp.getResponseMsg());
                            resentOtpTxt.setEnabled(false);
                            resentOtpTxt.setAlpha(0.5f);
                            countDownTimer.start();
                        } else if (signUp != null) {
                            Toast.makeText(getBaseContext(), signUp.getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                        if (progressDialog != null && progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        Toast.makeText(getBaseContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();

                        insertError("SignUpActivity","getLanguage()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getBaseContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.animation_f_enter,
                R.anim.animation_f_leave);
    }

    private void setupWindowAnimations() {
        this.overridePendingTransition(R.anim.animation_enter,
                R.anim.animation_leave);
    }

    public void setupUI(View view) {
        try {
            if (!(view instanceof EditText)) {
                view.setOnTouchListener(new View.OnTouchListener() {
                    @SuppressLint("ClickableViewAccessibility")
                    public boolean onTouch(View v, MotionEvent event) {
                        hideSoftKeyboard(SignUpActivity.this);
                        return false;

                    }
                });
            }

            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    setupUI(innerView);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /* @Override
     public void receivedOTP(String otp) {
         pinview.setText(otp);
     }*/
//    public void getGenderApiCall(){
//
//        if (NetworkUtil.isNetworkAvailable(GenderScreen.this)){
//            APIRequestHandler.getInstance().getGenderAPICall(GenderScreen.this);
//        }else {
//            DialogManager.getInstance().showNetworkErrorPopup(GenderScreen.this, new InterfaceBtnCallBack() {
//                @Override
//                public void onPositiveClick() {
//                    getGenderApiCall();
//                }
//            });
//        }
//
//    }


    @Override
    protected void onResume() {

        getCountryList();
        super.onResume();
    }


    public void getLanguage() {
        mUserDetailsEntity = new UserDetailsEntity();

        mUserDetailsEntity.setLanguage(mChooseLanguageTxt.getText().toString());

        PreferenceUtil.storeUserDetails(SignUpActivity.this, mUserDetailsEntity);

        if (mUserDetailsEntity.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            updateResources(SignUpActivity.this, "ar");


        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
            updateResources(SignUpActivity.this, "en");

        }
        phoneNo.setHint(getResources().getString(R.string.mobile_num));
        submitButton.setText(getResources().getString(R.string.contin));
        login_choose_language_txt.setText(getResources().getString(R.string.choose_location));
    }


    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof CountryCodeResponse) {
            CountryCodeResponse mResponse = (CountryCodeResponse) resObj;
            mCountryList = new ArrayList<>();
            mCountryList.addAll(mResponse.getResult());
        }
        if (resObj instanceof LoginOTPResponse) {
            LoginOTPResponse mResponse = (LoginOTPResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                nextScreen(OTPScreen.class, true);
            }
        }
//        if (resObj instanceof GetLanguageResponse) {
//            GetLanguageResponse mResponse = (GetLanguageResponse) resObj;
//            mLanguageEntity = mResponse.getResult();
//        }

    }

    @Override
    public void onRequestFailure(Throwable t) {
        super.onRequestFailure(t);
    }

    private static void updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }


    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void getLanguageApiCall() {
        if (NetworkUtil.isNetworkAvailable(this)) {
            APIRequestHandler.getInstance().getLanguageApiCall(SignUpActivity.this);
        } else {
            DialogManager.getInstance().showNetworkErrorPopup(SignUpActivity.this, new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getLanguageApiCall();
                }
            });
        }
    }


    public void getLanguageApi() {
        restService.getLanguage().enqueue(new Callback<GetLanguageResponse>() {
            @Override
            public void onResponse(Call<GetLanguageResponse> call, Response<GetLanguageResponse> response) {
                mLanguageEntity = response.body().getResult();
            }

            @Override
            public void onFailure(Call<GetLanguageResponse> call, Throwable t) {
                     insertError("SignUpActivity","getLanguage()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }


    //     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}