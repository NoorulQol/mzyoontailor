package com.qoltech.mzyoontailor.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.NewCustomerCountryCodeAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.ui.GenderScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.Country;
import com.qoltech.mzyoontailor.wrappers.CountryResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.WelcomeMessageNewUserResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCustomerActivity extends BaseActivity {
    Button new_customer_next;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    public static TextView textCountrySelection;
    public static ImageView countrySpinner, flagsImg;

    ApiService restService;
    List<Country> countries;
    RecyclerView countryList;
    LinearLayoutManager linearLayoutManager;
    NewCustomerCountryCodeAdapter countryCodeDialogAdapter;
    Country country;
    static boolean rememberLastSelection = false;
    static Context context;
    static String CCP_PREF_FILE = "CCP_PREF_FILE";
    static String selectionMemoryTag = "ccp_last_selection";
    SharedPreferences sharedPreferences;
    public static EditText new_customer_phone_no, userNameNewCustomer;
    private UserDetailsEntity mUserDetailsEntityRes;
    FrameLayout country_code_lay;
    LinearLayout name_layout;
    Gson gson;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        sharedPreferences = getSharedPreferences("Existing_Customer", MODE_PRIVATE);
        restService = ((MainApplication) getApplication()).getClient();
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        dialogManager=new DialogManager();
        initView();


        textCountrySelection = findViewById(R.id.textCountry);
        flagsImg = findViewById(R.id.flagsImg);
        countrySpinner = findViewById(R.id.imgSpinner);
        new_customer_phone_no = findViewById(R.id.new_customer_phone_no);
        userNameNewCustomer = findViewById(R.id.userNameNewCustomer);
        country_code_lay = findViewById(R.id.country_code_lay);
        name_layout = findViewById(R.id.name_layout);
        if (!AppConstants.COUNTRY_FLAG.equalsIgnoreCase("")) {
            Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "images/flags/" + AppConstants.COUNTRY_FLAG)
                    .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.placeholder)).into(flagsImg);

        }


        new_customer_next = (Button) findViewById(R.id.new_customer_next);
        sharedPreferences.getString("User_Type", "");
        new_customer_phone_no.setText(sharedPreferences.getString("customer_phone_num", "0"));
        textCountrySelection.setText(sharedPreferences.getString("country_code", ""));
        getCountryList();
        getLanguage();

        new_customer_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (new_customer_phone_no.getText().toString().isEmpty()) {

                    Toast.makeText(getApplicationContext(), "Please Enter All Fields", Toast.LENGTH_LONG).show();
                }


                if (userNameNewCustomer.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please Enter All Fields", Toast.LENGTH_LONG).show();

                } else {
                    dialogManager.showProgress(getApplicationContext());
                    updateNewUserDetails();
                    startActivity(new Intent(getApplicationContext(), GenderScreen.class));
                    sendMsgToNewCustomer();

                    AppConstants.NEW_CUSTOMER_ACTIVITY="NEW_CUSTOMER_ACTIVITY";
                }

            }
        });
    }


    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();


    }

    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.new_customer);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    private void showCoutrySelectionList() {
        View view = LayoutInflater.from(this).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getBaseContext());
        countryList.setLayoutManager(linearLayoutManager);
        TextView myMsg = new TextView(this);
        myMsg.setText("SELECT COUNTRY");
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextColor(Color.BLACK);
        //set custom title
//        builder.setCustomTitle(myMsg);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCustomTitle(myMsg)
                .setView(view)
                .create();
        countryCodeDialogAdapter = new NewCustomerCountryCodeAdapter(getApplicationContext(), countries, country, dialog);
        countryList.setAdapter(countryCodeDialogAdapter);
        dialog.show();

    }

    private void getCountryList() {
        if (new UtilService().isNetworkAvailable(getApplicationContext())) {

            try {
                restService.getCountry().enqueue(new Callback<CountryResponse>() {
                    @Override
                    public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {

                        if (response.isSuccessful()) {
                            countries = response.body().getResult();
                            if (countries == null) {
                                countries = new ArrayList<>();
                            }
                            setCountrySelectionList();
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getApplicationContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CountryResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();

                        insertError("NewCustomerActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");


                    }
                });

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void onUserTappedCountry(Country country) {
        if (rememberLastSelection) {
            storeSelectedCountryNameCode(country.getPhoneCode());
        }
    }


    static void storeSelectedCountryNameCode(String selectedCountryNameCode) {
        //get the shared pref
        SharedPreferences sharedPref = context.getSharedPreferences(
                CCP_PREF_FILE, Context.MODE_PRIVATE);

        //we want to write in shared pref, so lets get editor for it
        SharedPreferences.Editor editor = sharedPref.edit();

        // add our last selection country name code in pref
        editor.putString(selectionMemoryTag, selectedCountryNameCode);

        //finally save it...
        editor.apply();
    }

    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < countries.size(); i++) {
            listSpinner.add(countries.get(i).getCountryName());
        }
        textCountrySelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCoutrySelectionList();
            }
        });
        countrySpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textCountrySelection.performClick();
            }
        });
    }


    public void sendMsgToNewCustomer() {


        HashMap<String, String> map = new HashMap<>();
        map.put("UserName", userNameNewCustomer.getText().toString());
        map.put("PhoneNumber", textCountrySelection.getText().toString() + new_customer_phone_no.getText().toString());

        restService.WelcomeMessageToNewCustomer(map).enqueue(new Callback<WelcomeMessageNewUserResponse>() {
            @Override
            public void onResponse(Call<WelcomeMessageNewUserResponse> call, Response<WelcomeMessageNewUserResponse> response) {


            }

            @Override
            public void onFailure(Call<WelcomeMessageNewUserResponse> call, Throwable t) {
                insertError("NewCustomerActivity","WelcomeMessageToNewCustomer()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }


    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }


    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.total_edittext_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
            country_code_lay.setBackgroundResource(R.drawable.edit_text_round_corner_leftside);
            new_customer_phone_no.setBackgroundResource(R.drawable.edit_text_round_corner_rightside);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.total_edittext_layout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }


    public void updateNewUserDetails() {


        HashMap<String, String> map = new HashMap<>();
        map.put("CountryCode", textCountrySelection.getText().toString());
        map.put("PhoneNo", new_customer_phone_no.getText().toString());
        map.put("Name", userNameNewCustomer.getText().toString());


        restService.updateNewUserDetails(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                AppConstants.DIRECT_USERS_ID = response.body().getResult();
//                updateDirectBuyerAddress( AppConstants.DIRECT_USERS_ID );
                dialogManager.hideProgress();

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("NewCustomerActivity","updateNewUserDetails()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }


    public void updateDirectBuyerAddress(String buyerId) {

        HashMap<String, String> map = new HashMap<>();
        map.put("BuyerId", buyerId);
        map.put("FirstName", new_customer_phone_no.getText().toString());
        map.put("LastName", userNameNewCustomer.getText().toString());
        map.put("CountryId", "1");
        map.put("StateId", "2");
        map.put("AreaId", "3");
        map.put("Floor", textCountrySelection.getText().toString());
        map.put("LandMark", new_customer_phone_no.getText().toString());
        map.put("LocationType", userNameNewCustomer.getText().toString());
        map.put("ShippingNotes", textCountrySelection.getText().toString());
        map.put("IsDefault", new_customer_phone_no.getText().toString());
        map.put("CountryCode", userNameNewCustomer.getText().toString());
        map.put("PhoneNo", userNameNewCustomer.getText().toString());
        map.put("Longitude", "0");
        map.put("Lattitude", "0");



        restService.updateDirectCustomerAddress(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                AppConstants.DIRECT_USERS_ID = response.body().getResult();

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
    insertError("NewCustomerActivity","updateDirectCustomerAddress()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

//    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


}
