package com.qoltech.mzyoontailor.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.OnMatrixChangedListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewCustomizationRequestAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestCustomization;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResponse;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCustomizationActivity extends AppCompatActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ImageView get_buyer_request_season_image, get_buyer_request_industry_image,
            get_buyer_request_brand_image,
            get_buyer_request_materialtype_image,
            get_buyer_request_color_image, get_buyer_request_pattern_image;
    TextView get_buyer_request_season_text, get_buyer_request_industry_text,
            get_buyer_request_brand_text,
            get_buyer_request_materialtype_text, material_details_txt;
    Button approve_request, reject_request;
    ViewCustomizationRequestAdapter viewCustomizationRequestAdapter;
    RecyclerView get_buyer_request_customization_recyclerView;

    ApiService restService;
    GetBuyerRequestResult getBuyerRequestResult = new GetBuyerRequestResult();
    List<GetBuyerRequestCustomization> getBuyerRequestCustomizations;
    SharedPreferences sharedPreferences, sharedPreferences1;
    FrameLayout material_details_layout;
    Dialog mAppointmentApprove;
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout;
    Gson gson;
    public static PhotoView photo_view;


    public static ImageView myImage;
    RecyclerView customization_list_recyclerview;
    Handler handler = null;
    Matrix matrix;
    RectF rectf;
Rect rect;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_custumization_three);

        restService = ((MainApplication) getApplication()).getClient();
//        get_buyer_request_industry_image = (ImageView) findViewById(R.id.get_buyer_request_industry_image);
//        get_buyer_request_brand_image = (ImageView) findViewById(R.id.get_buyer_request_brand_image);
//        get_buyer_request_materialtype_image = (ImageView) findViewById(R.id.get_buyer_request_materialtype_image);
//        get_buyer_request_industry_text = (TextView) findViewById(R.id.get_buyer_request_industry_text);
//        get_buyer_request_brand_text = (TextView) findViewById(R.id.get_buyer_request_brand_text);
//        get_buyer_request_materialtype_text = (TextView) findViewById(R.id.get_buyer_request_materialtype_text);
//        get_buyer_request_customization_recyclerView = findViewById(R.id.get_buyer_request_customization_recyclerView);
//        approve_request = findViewById(R.id.approve_request);
//        material_details_layout = (FrameLayout) findViewById(R.id.material_details_layout);
//
        sharedPreferences = getApplicationContext().getSharedPreferences("TailorId", Context.MODE_PRIVATE);
//        sharedPreferences1 = getApplicationContext().getSharedPreferences("OrderId", MODE_PRIVATE);
//        material_details_txt = (TextView) findViewById(R.id.material_details_txt);
//        reject_request = (Button) findViewById(R.id.reject_request);

        bgLayout = findViewById(R.id.bgLayout);
        myImage = (ImageView) findViewById(R.id.myImage);
        customization_list_recyclerview = (RecyclerView) findViewById(R.id.customization_list_recyclerview);
        photo_view = findViewById(R.id.photo_view);
        initView();
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getApplicationContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        getBuyerRequestCustomizations = new ArrayList<>();
        getRequestCustomization();
//        rectf = new RectF(photo_view.getDisplayRect().left, photo_view.getDisplayRect().top, photo_view.getDisplayRect().
//                right, photo_view.getDisplayRect().bottom);

        rectf = new RectF(0,0,0,0);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "images/Customazation3/" +
                        AppConstants.SET_MATERIAL_IMAGE)
                        .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.gif_loader))
                        .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL).
                                format(DecodeFormat.PREFER_ARGB_8888).override(Target.SIZE_ORIGINAL)

                                .placeholder(R.drawable.placeholder)).into(photo_view);

            }
        }, 400);
        matrix = photo_view.getImageMatrix();

        photo_view.setOnMatrixChangeListener(new OnMatrixChangedListener() {
            @Override
            public void onMatrixChanged(RectF rect) {
//                if (rect.left==rectf.left) {
//                    customization_list_recyclerview.setVisibility(View.VISIBLE);
////                    Toast.makeText(getApplicationContext(), "sux" + rect, Toast.LENGTH_LONG).show();
//                } else {
//                    customization_list_recyclerview.setVisibility(View.GONE);
////                    Toast.makeText(getApplicationContext(), "fai" + rect, Toast.LENGTH_LONG).show();
//
//                }
            }
        });

    }


    public void getRequestCustomization() {

        restService.getCustomizationType(AppConstants.ORDER_ID).enqueue(new Callback<GetBuyerRequestResponse>() {
            @Override
            public void onResponse(Call<GetBuyerRequestResponse> call, Response<GetBuyerRequestResponse> response) {


                try {
                    getBuyerRequestCustomizations = response.body().getResult().getCustomization();

                    setGenderList(getBuyerRequestCustomizations);

                    AppConstants.SET_MATERIAL_IMAGE = getBuyerRequestCustomizations.get(0).getImages();
//                    if (getBuyerRequestCustomizations != null) {
//
//                        Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "Images/PlaceOfIndustry/" + response.body().getResult().getPlaceofIndustry().get(0).getImage())
//                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_industry_image);
//                        Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "/Images/Brands/" + response.body().getResult().getMaterialBrand().get(0).getImage())
//                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_brand_image);
//                        Glide.with(getApplicationContext()).load(GlobalData.SERVER_URL + "/Images/Material/" + response.body().getResult().getMaterialType().get(0).getImage())
//                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_materialtype_image);
//
//                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//
//
//                            get_buyer_request_industry_text.setText(response.body().getResult().getPlaceofIndustry().get(0).getPlaceInArabic());
//                            get_buyer_request_brand_text.setText(response.body().getResult().getMaterialBrand().get(0).getBrandInArabic());
//                            get_buyer_request_materialtype_text.setText(response.body().getResult().getMaterialType().get(0).getMaterialInArabic());
//
//                        } else {
////                            holder.mGridLeftViewTxt.setText(genderEntity.getGender());
////                            holder.mGridRightViewTxt.setText(genderEntity.getGender());
//
//                            get_buyer_request_industry_text.setText(response.body().getResult().getPlaceofIndustry().get(0).getPlaceInEnglish());
//                            get_buyer_request_brand_text.setText(response.body().getResult().getMaterialBrand().get(0).getBrandInEnglish());
//                            get_buyer_request_materialtype_text.setText(response.body().getResult().getMaterialType().get(0).getMaterialInEnglish());
//
//
//                        }
//
//                    } else {
//
//
//                    }


                } catch (Exception e) {
//                    material_details_layout.setVisibility(View.GONE);
//                    material_details_txt.setVisibility(View.GONE);
                }


            }

            @Override
            public void onFailure(Call<GetBuyerRequestResponse> call, Throwable t) {
                     insertError("ViewCustomizationActivity","getCustomizationType()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });

    }


    private void setGenderList(List<GetBuyerRequestCustomization> getBuyerRequestCustomizations) {

        viewCustomizationRequestAdapter = new ViewCustomizationRequestAdapter(getApplicationContext(), getBuyerRequestCustomizations);
        customization_list_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        customization_list_recyclerview.setAdapter(viewCustomizationRequestAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (viewCustomizationRequestAdapter != null) {
            viewCustomizationRequestAdapter.notifyDataSetChanged();
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }


    //     insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(ViewCustomizationActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.customization_details);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }}
} 
