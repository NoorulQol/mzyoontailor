package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.OrderTypeSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.InsertOrderTypeSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.OrderTypeEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.modal.InsertOrderTypeSkillUpdateModal;
import com.qoltech.mzyoontailor.modal.OrderTypeResponse;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderTypeSkillUpdate extends BaseActivity {
    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    ApiService restService;
    List<OrderTypeEntity> orderTypeEntities;
    RecyclerView genderList;
    OrderTypeSkillUpdateAdapter orderTypeSkillUpdateAdapter;
    SharedPreferences sharedPreferences;
    Button save;
    private UserDetailsEntity mUserDetailsEntityRes;
    TransparentProgressDialog transparentProgressDialog;
    DialogManager dialogManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initView();
        setContentView(R.layout.activity_order_type_skill_update);
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);

        restService = ((MainApplication) getApplication()).getClient();
        genderList = (RecyclerView) findViewById(R.id.genderList);
        save = (Button) findViewById(R.id.save);
        dialogManager = new DialogManager();
        initView();
        getMaterialMappingListSize();

        getSubDressType();
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertOrderTypeApi();
                dialogManager.showProgress(OrderTypeSkillUpdate.this);

            }
        });


    }

    private void getSubDressType() {

        restService.getOrderTypeSkillUpdate(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<OrderTypeResponse>() {
            @Override
            public void onResponse(Call<OrderTypeResponse> call, Response<OrderTypeResponse> response) {
                orderTypeEntities = response.body().getResult();
                setGenderList();
            }

            @Override
            public void onFailure(Call<OrderTypeResponse> call, Throwable t) {
                insertError("OrderTypeSkillUpdate","getOrderTypeSkillUpdate()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    //
    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();

        mUserDetailsEntityRes = new UserDetailsEntity();

        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(OrderTypeSkillUpdate.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();
    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.order_type);
        mRightSideImg.setVisibility(View.VISIBLE);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

//    public void getSubDressType() {
//        restService.getOrderType().enqueue(new Callback<OrderTypeEntity>() {
//            @Override
//            public void onResponse(Call<OrderTypeEntity> call, Response<OrderTypeEntity> response) {
//                orderTypeEntities = response.body().getResult();
//                setGenderList();
//            }
//
//            @Override
//            public void onFailure(Call<OrderTypeEntity> call, Throwable t) {
//
//            }
//        });
//    }


    private void setGenderList() {
        genderList.setHasFixedSize(true);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        orderTypeSkillUpdateAdapter = new OrderTypeSkillUpdateAdapter(this, orderTypeEntities, restService);
        genderList.setAdapter(orderTypeSkillUpdateAdapter);
    }

    public void insertOrderTypeApi() {

        ArrayList<InsertOrderTypeSkillUpdateEntity> insertOrderTypeSkillUpdateEntities = new ArrayList<>();

        for (int i = 0; i < AppConstants.SKILL_ORDER_ID.size(); i++) {
            InsertOrderTypeSkillUpdateEntity insertOrderTypeSkillUpdateEntity = new InsertOrderTypeSkillUpdateEntity();
            insertOrderTypeSkillUpdateEntity.setId(AppConstants.SKILL_ORDER_ID.get(i));
            insertOrderTypeSkillUpdateEntities.add(insertOrderTypeSkillUpdateEntity);
        }


        InsertOrderTypeSkillUpdateModal insertOrderTypeSkillUpdateApiModal = new InsertOrderTypeSkillUpdateModal();
        insertOrderTypeSkillUpdateApiModal.setTailorId(sharedPreferences.getString("TailorId", ""));
        insertOrderTypeSkillUpdateApiModal.setOrderTypeId(insertOrderTypeSkillUpdateEntities);


        restService.insertOrderTypeSkillUpdate(insertOrderTypeSkillUpdateApiModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {


                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms


                        dialogManager.hideProgress();
                        Toast.makeText(getApplicationContext(), R.string.order_type_updated_sucessfully, Toast.LENGTH_LONG).show();
                        finish();
                    }
                }, 2000);

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                insertError("OrderTypeSkillUpdate","insertOrderTypeSkillUpdate()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.order_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }


    public void getMaterialMappingListSize() {


        restService.getMaterialCount(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                AppConstants.MATERIAL_MAP_SIZE = Integer.parseInt(response.body().getResult());
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
    insertError("OrderTypeSkillUpdate","getMaterialCount()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

            }
        });
    }

//    /    /    insertError("LocationSkillUpdateGetCountryActivity","getCountry()",""+t,"ApiVersion", AppConstants.DEVICE_ID_NEW,"Tailor");

    public void insertError(String pageName, String methodName, String error,
                            String apiVersion, String deviceId, String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName", pageName);
        map.put("MethodName", methodName);
        map.put("Error", error);
        map.put("ApiVersion", apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type", type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
