package com.qoltech.mzyoontailor.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.CustomizationGetAttributeSkillUpdateAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GridviewDecoration;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.CustumizationSkillUpdateGetAttributeResponse;
import com.qoltech.mzyoontailor.wrappers.CustumizationSkillUpdateGetAttributeResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustumizationAttributeDetailsSkillUpdateActivity extends BaseActivity {

    ApiService restService;
    CustomizationGetAttributeSkillUpdateAdapter customizationGetAttributeSkillUpdateAdapter;
    List<CustumizationSkillUpdateGetAttributeResult> custumizationSkillUpdateGetAttributeResults;
    public static   List<CustumizationSkillUpdateGetAttributeResult> storeCustumizationSkillUpdateGetAttributeResults;
    RecyclerView genderList;

    @BindView(R.id.header_txt)
    TextView mHeaderTxt;

    @BindView(R.id.header_right_side_img)
    ImageView mRightSideImg;

    @BindView(R.id.header_left_side_img)
    ImageView mHeaderLeftBackBtn;
    SharedPreferences sharedPreferences;
    ArrayList<String> materialList;
    private UserDetailsEntity mUserDetailsEntityRes;
    DialogManager dialogManager;

    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_dress_type_skill_update);
        genderList = (RecyclerView) findViewById(R.id.genderList);
        restService = ((MainApplication) getApplication()).getClient();
        sharedPreferences = getSharedPreferences("TailorId", MODE_PRIVATE);
        dialogManager=new DialogManager();
        dialogManager.showProgress(getApplicationContext());

        getCustumizationAttribute(AppConstants.ATTRIBUTE_ID, sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID);

        initView();

    }

    public void getCustumizationAttribute(String attributeId, String tailorID, String dressTypeId) {


        restService.getSkillUpdateCustumizationAttribute(attributeId, tailorID, dressTypeId).
                enqueue(new Callback<CustumizationSkillUpdateGetAttributeResponse>() {
                    @Override
                    public void onResponse(Call<CustumizationSkillUpdateGetAttributeResponse> call,
                                           Response<CustumizationSkillUpdateGetAttributeResponse> response) {

                        custumizationSkillUpdateGetAttributeResults = response.body().getResult();
                        dialogManager.hideProgress();
                        storeCustumizationSkillUpdateGetAttributeResults=custumizationSkillUpdateGetAttributeResults;





                        setGenderList();

                    }

                    @Override
                    public void onFailure(Call<CustumizationSkillUpdateGetAttributeResponse> call, Throwable t) {
                        insertError("CustumizationAttributeDetailsSkillUpdateActivity","getCustumizationAttribute()",""+t,"ApiVersion",AppConstants.DEVICE_ID_NEW,"Tailor");

                    }
                });
    }


    private void setGenderList() {
        genderList.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        GridviewDecoration itemDecoration = new GridviewDecoration(this, R.dimen.size8);
        genderList.addItemDecoration(itemDecoration);
//        genderList.setLayoutManager(gridLayoutManager);
        genderList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        customizationGetAttributeSkillUpdateAdapter = new CustomizationGetAttributeSkillUpdateAdapter(this, storeCustumizationSkillUpdateGetAttributeResults, restService);
        genderList.setAdapter(customizationGetAttributeSkillUpdateAdapter);
    }


    /*InitViews*/
    private void initView() {

        ButterKnife.bind(this);

        setHeader();
        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(CustumizationAttributeDetailsSkillUpdateActivity.this, AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

        getLanguage();

    }


    public void setHeader() {
        mHeaderLeftBackBtn.setVisibility(View.VISIBLE);
        mHeaderTxt.setVisibility(View.VISIBLE);
        mHeaderTxt.setText(R.string.customization_details);
        mRightSideImg.setVisibility(View.VISIBLE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.header_left_side_img})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.header_left_side_img:
                onBackPressed();
                break;
        }

    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_sub_type_par_lay), ViewCompat.LAYOUT_DIRECTION_RTL);

        } else {
            ViewCompat.setLayoutDirection(findViewById(R.id.dress_sub_type_par_lay), ViewCompat.LAYOUT_DIRECTION_LTR);

        }
    }

    public void insertError(String pageName,String methodName,String error,
                            String apiVersion,String deviceId,String type) {

        HashMap<String, String> map = new HashMap<>();

        map.put("PageName",pageName);
        map.put("MethodName",methodName);
        map.put("Error", error);
        map.put("ApiVersion",apiVersion);
        map.put("DeviceId", deviceId);
        map.put("Type",type);
        restService.insertError(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}
