package com.qoltech.mzyoontailor.fragments;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.GetMeasurementDetailsAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResult;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsModal;
import com.qoltech.mzyoontailor.wrappers.GetRequestMeasurementPartsResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class Measurement_request_fragment extends Fragment {

    public static Handler.Callback callback;

    GetMeasurementDetailsAdapter getCustomizationRequestAdapter;
    RecyclerView measurement_request_recycler_view;

    ApiService restService;
    GetBuyerRequestResult getBuyerRequestResult = new GetBuyerRequestResult();
    List<GetRequestMeasurementPartsModal> getMeasurementDetailsModals;
    SharedPreferences sharedPreferences, sharedPreferences1;
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout;
    Gson gson;
    ImageView measurement_type_image;
    TextView measurement_type_text;
    Button next;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        rootView = inflater.inflate(R.layout.fragment_measurement_request, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
            sharedPreferences1 = getActivity().getSharedPreferences("OrderId", MODE_PRIVATE);
        }
        measurement_request_recycler_view = rootView.findViewById(R.id.measurement_request_recycler_view);


        measurement_type_text = rootView.findViewById(R.id.measurement_type_text);
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);
        bgLayout = (LinearLayout) rootView.findViewById(R.id.bgLayout);
        measurement_type_image = (ImageView) rootView.findViewById(R.id.measurement_type_image);
        next = rootView.findViewById(R.id.next);
        next.setVisibility(View.GONE);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getActivity(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getRequestCustomization();
        measurement_type_image.setVisibility(View.GONE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms

                if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1) {
                    measurement_type_image.setVisibility(View.GONE);

                } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 2) {
                    measurement_type_image.setVisibility(View.VISIBLE);
                    measurement_type_text.setVisibility(View.VISIBLE);
                    measurement_type_image.setImageResource(R.drawable.go_to_tailor);

                    measurement_type_text.setText("Go To Tailor Shop");
//                    measurement_type_text.setText("");

                } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 3) {
                    measurement_type_image.setVisibility(View.VISIBLE);
                    measurement_type_text.setVisibility(View.VISIBLE);

                    measurement_type_image.setImageResource(R.drawable.tailor_come);
                    measurement_type_text.setText("Tailor Come To Place");


                }
            }
        }, 1000);

        getLanguage();
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
    }


    public void getRequestCustomization() {

        try {
            restService.getRequestMeasurementParts(AppConstants.ORDER_ID).enqueue(new Callback<GetRequestMeasurementPartsResponse>() {
                @Override
                public void onResponse(Call<GetRequestMeasurementPartsResponse> call, Response<GetRequestMeasurementPartsResponse> response) {
//
                    if (response.isSuccessful()) {

                        try {
                            getMeasurementDetailsModals = response.body().getResult();
                            if (getMeasurementDetailsModals == null) {
                                getMeasurementDetailsModals = new ArrayList<>();
                            }
                        } catch (Exception e) {

                        }

                        setGenderList(getMeasurementDetailsModals);
                    } else if (response.errorBody() != null) {
                        Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(Call<GetRequestMeasurementPartsResponse> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
        }
    }


    private void setGenderList(List<GetRequestMeasurementPartsModal> getMeasurementDetailsModals) {

        getCustomizationRequestAdapter = new GetMeasurementDetailsAdapter(getActivity(), getMeasurementDetailsModals);
        measurement_request_recycler_view.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        measurement_request_recycler_view.setAdapter(getCustomizationRequestAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCustomizationRequestAdapter != null) {
            getCustomizationRequestAdapter.notifyDataSetChanged();
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }
}
