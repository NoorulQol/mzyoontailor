package com.qoltech.mzyoontailor.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.ViewProfileAdapter;

public class MyAccountFragment extends Fragment {
    public static Handler.Callback callback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
//        return inflater.inflate(R.layout.activity_my_account, container, false);

        View rootView = inflater.inflate(R.layout.activity_my_account, container, false);
        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager2);
//
        // Create an adapter that knows which fragment should be shown on each page
        ViewProfileAdapter adapter = new ViewProfileAdapter(getActivity(), getChildFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.sliding_tabs2);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0c2c75"));
//        tabLayout.setSelectedTabIndicatorHeight((int) (50 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#000000"), Color.parseColor("#ffffff"));
        tabLayout.setupWithViewPager(viewPager);
        return rootView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
    }
}
