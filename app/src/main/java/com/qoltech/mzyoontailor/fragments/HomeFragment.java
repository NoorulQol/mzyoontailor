package com.qoltech.mzyoontailor.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.TabAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {
    Context context;
    private UserDetailsEntity mUserDetailsEntityRes;

    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

         rootView = inflater.inflate(R.layout.activity_home, container, false);
        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
//
        // Create an adapter that knows which fragment should be shown on each page
        TabAdapter adapter = new TabAdapter(getActivity(), getChildFragmentManager());

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.sliding_tabs);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#0c2c75"));
//        tabLayout.setSelectedTabIndicatorHeight((int) (50 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#000000"), Color.parseColor("#ffffff"));
        tabLayout.setupWithViewPager(viewPager);
//        Fragment fragment = null;
//
//        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//        ft.replace(R.id.frame_home, fragment);
//        ft.commit();
        initView();
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles


    }


    private boolean isFirstTime() {
        SharedPreferences preferences = getActivity().getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
        if (!ranBefore) {
            // first time
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("RanBefore", true);
            editor.commit();
        }
        return !ranBefore;
    }
    private void initView() {
//        AppConstants.HOME_SCREEN = this;

//        setupUI(mHomeScreenParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

//        mSetHeader();

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
        }
        else {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

    }
}

