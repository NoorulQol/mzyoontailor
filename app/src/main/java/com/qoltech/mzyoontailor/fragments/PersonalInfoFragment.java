package com.qoltech.mzyoontailor.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.adapter.ProfileGenderAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.SelectDateFragment;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.ImageResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.TailorProfileGet;
import com.qoltech.mzyoontailor.wrappers.TailorProfileGetResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class PersonalInfoFragment extends Fragment {
    public static Handler.Callback callback;
    EditText tailor_name, tailor_name_arabic, mobile_number, email_id;
    public static EditText dob;
    public static CircularImageView userImage;
    ImageView camera, calender;
    CheckBox male, female;
    Button save, cancel;
    ApiService restService;
    boolean m, f;
    private List<String> mAppPermissionsStrArrList;

    SharedPreferences sharedPreferences;
    TransparentProgressDialog progressDialog;
    DialogManager dialogManager;
    TailorProfileGetResponse tailorProfileGetResponse;
    List<TailorProfileGet> tailorProfileGetList;
    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;
    private Uri mPictureFileUri;
    Uri selectedImagePath;
    private String mUserProfileImagePath = "";
    private File mUserImageFile = null;
    String IMAGE_PATH = "";
    private static final String IMAGE_DIRECTORY = "/MZYOON";
    SharedPreferences sharedPreferencesImageName, sharedPreferencesShopId;
    String shopOwnerImageUrlFromApi = "";
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout_lay;
    Gson gson;
    RelativeLayout profile_gender_lay;
    Dialog mGenderDialog;
    ArrayList<String> mGenderList;
    ProfileGenderAdapter mGenderAdapter;
    public static EditText profile_gender_edt_txt;
    Button edit_profile;
    ImageView cameraButton;

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        rootView = inflater.inflate(R.layout.fragment_personal_info, container, false);

        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        tailor_name = (EditText) rootView.findViewById(R.id.tailor_name);
        tailor_name_arabic = (EditText) rootView.findViewById(R.id.tailor_name_arabic);
        mobile_number = (EditText) rootView.findViewById(R.id.mobile_number);
        email_id = (EditText) rootView.findViewById(R.id.email_id);
        dob = (EditText) rootView.findViewById(R.id.dob);

//        male = (CheckBox) rootView.findViewById(R.id.male);
//        female = (CheckBox) rootView.findViewById(R.id.female);
        save = (Button) rootView.findViewById(R.id.save);
        cancel = (Button) rootView.findViewById(R.id.cancel);
        edit_profile = (Button) rootView.findViewById(R.id.edit_profile);
        userImage = (CircularImageView) rootView.findViewById(R.id.userImage);
        calender = (ImageView) rootView.findViewById(R.id.calender);
        profile_gender_edt_txt = (EditText) rootView.findViewById(R.id.profile_gender_edt_txt);
        profile_gender_lay = (RelativeLayout) rootView.findViewById(R.id.profile_gender_lay);
        setupUI(rootView.findViewById(R.id.bgLayout));
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);
        sharedPreferencesImageName = getActivity().getSharedPreferences("ImageName", MODE_PRIVATE);
        bgLayout_lay = (LinearLayout) rootView.findViewById(R.id.bgLayout_lay);
        cameraButton = rootView.findViewById(R.id.cameraButton);
//        cameraButton.setVisibility(View.VISIBLE);
//        cameraButton.setAlpha(0);
        gson = new Gson();
        dialogManager = new DialogManager();
        String json = PreferenceUtil.getStringValue(getActivity(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        sharedPreferencesShopId = getActivity().getSharedPreferences("ShopId", MODE_PRIVATE);
        getLanguage();
        mGenderList = new ArrayList<>();

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {


            mGenderList.add("الذكر");
            mGenderList.add("إناثا");
        } else {

            mGenderList.add("Male");
            mGenderList.add("Female");
        }
        getProfile();


        calender.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
                AppConstants.CALENDER = "personal_info";

            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEnableFalse();
                edit_profile.setVisibility(View.VISIBLE);
                cameraButton.setEnabled(false);
//                cameraButton.setAlpha(127);
                cameraButton.setVisibility(View.INVISIBLE);

            }
        });


        profile_gender_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGenderList.size() > 0) {
                    alertDismiss(mGenderDialog);
                    mGenderDialog = getDialog(getActivity(), R.layout.popup_country_alert);

                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                    Window window = mGenderDialog.getWindow();

                    if (window != null) {
                        LayoutParams.copyFrom(window.getAttributes());
                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(LayoutParams);
                        window.setGravity(Gravity.CENTER);
                    }

                    TextView cancelTxt, headerTxt;

                    RecyclerView countryRecyclerView;

                    /*Init view*/
                    cancelTxt = mGenderDialog.findViewById(R.id.country_text_cancel);
                    headerTxt = mGenderDialog.findViewById(R.id.country_header_text_cancel);

                    countryRecyclerView = mGenderDialog.findViewById(R.id.country_popup_recycler_view);
                    mGenderAdapter = new ProfileGenderAdapter(getActivity(), mGenderList, mGenderDialog);

                    countryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    countryRecyclerView.setAdapter(mGenderAdapter);

                    /*Set data*/
                    headerTxt.setText(getResources().getString(R.string.choose_your_gender));
                    cancelTxt.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mGenderDialog.dismiss();
                        }
                    });

                    alertShowing(mGenderDialog);
                } else {
                    Toast.makeText(getActivity(),
                            getResources().getString(R.string.sorry_no_result_found),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tailor_name.getText().toString().isEmpty() || tailor_name_arabic.getText().toString().isEmpty()
                        || mobile_number.getText().toString().isEmpty() ||
                        email_id.getText().toString().isEmpty() || dob.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Enter All Feilds", Toast.LENGTH_LONG).show();
                } else if (!isEmailValid(email_id.getText().toString().trim())) {
                    email_id.setError("Please give valid email address");
                } else if (profile_gender_edt_txt.getText().toString().isEmpty()) {

                    Toast.makeText(getActivity(), "Select A Gender", Toast.LENGTH_LONG).show();


                } else if (!profile_gender_edt_txt.equals("Male") || !profile_gender_edt_txt.equals("Female")) {


//                    uploadPersonalDetails();
//                    updateProfile1();
//                    setEnableFalse();
                    if (IMAGE_PATH.equalsIgnoreCase("") && sharedPreferencesImageName.getString("ImageName", "") != null) {
                        updateProfileWithoutImage();

                    } else {
                        updateProfile1();
                        setEnableFalse();
                    }


                } else {
                    Toast.makeText(getActivity(), "Select A Gender", Toast.LENGTH_LONG).show();


                }
            }
        });


//        male.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    female.setChecked(false);
//                }
//
//            }
//        });
//
//
//        female.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if (isChecked) {
//                    male.setChecked(false);
//                }
//            }
//        });


        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEnableTrue();
                edit_profile.setVisibility(View.GONE);
                cameraButton.setVisibility(View.VISIBLE);

                cameraButton.setEnabled(true);
//                cameraButton.setAlpha(250);


            }
        });
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//
//                showPictureDialog();
//                profile_photo = 1;
//                shop_photo = "";
//                profile_photo = "Profile";
                uploadImages();
            }
        });

        return rootView;
    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

//    public final static boolean isValidEmail(CharSequence target) {
//        if (TextUtils.isEmpty(target)) {
//            return false;
//        } else {
//            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//        }
//    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void setupUI(View view) {
        try {
            if (!(view instanceof EditText)) {
                view.setOnTouchListener(new View.OnTouchListener() {
                    @SuppressLint("ClickableViewAccessibility")
                    public boolean onTouch(View v, MotionEvent event) {
                        hideSoftKeyboard(getActivity());
                        return false;

                    }
                });
            }

            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    setupUI(innerView);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getProfile() {
        tailorProfileGetList = new ArrayList<>();
        restService.getTailorProfile(sharedPreferences.getString("TailorId", "100")).enqueue(new Callback<TailorProfileGetResponse>() {
            @Override
            public void onResponse(Call<TailorProfileGetResponse> call, Response<TailorProfileGetResponse> response) {

                if (response.isSuccessful()) {
//                    Toast.makeText(getActivity(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();

                    tailorProfileGetList = response.body().getResult();

                    try {
                        if (tailorProfileGetList == null) {

                            tailorProfileGetList = new ArrayList<>();

                        } else {
                            cameraButton.setVisibility(View.INVISIBLE);

                            cameraButton.setEnabled(false);
//                            cameraButton.setAlpha(127);
                            try {


                                Glide.with(getActivity())
                                        .load(GlobalData.SERVER_URL + "Images/TailorImages/" + tailorProfileGetList.get(0).getShopOwnerImageURL())
                                        .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                        .into(userImage);
                                Glide.with(getActivity())
                                        .load(GlobalData.SERVER_URL + "Images/TailorImages/" + tailorProfileGetList.get(0).getShopOwnerImageURL())
                                        .apply(new RequestOptions().placeholder(R.drawable.placeholder).error(R.color.app_border))
                                        .into(ShopDetailsFrag.userImage);
                                mobile_number.setText(tailorProfileGetList.get(0).getPhoneNumber());

                                shopOwnerImageUrlFromApi = tailorProfileGetList.get(0).getShopOwnerImageURL().toString();
                                tailor_name.setText(tailorProfileGetList.get(0).getTailorNameInEnglish());
                                tailor_name_arabic.setText(tailorProfileGetList.get(0).getTailorNameInArabic());
                                email_id.setText(tailorProfileGetList.get(0).getEmailId());
                                profile_gender_edt_txt.setText(tailorProfileGetList.get(0).getGender());
                                dob.setText(tailorProfileGetList.get(0).getDob());

//                            if (!tailorProfileGetList.get(0).getGender().equalsIgnoreCase("Female") ||
//                                    !tailorProfileGetList.get(0).getGender().equalsIgnoreCase("إناثا")) {
////                                male.setChecked(false);
////                                female.setChecked(true);
//                            } else if (!tailorProfileGetList.get(0).getGender().equalsIgnoreCase("Male") ||
//                                    !tailorProfileGetList.get(0).getGender().equalsIgnoreCase("الذكر")) {
////
////                                male.setChecked(true);
////                                female.setChecked(false);
//                            }
                                setEnableFalse();

                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Something went wrong.");
                    }

                }
            }

            @Override
            public void onFailure(Call<TailorProfileGetResponse> call, Throwable t) {

            }
        });
    }


    public void setEnableTrue() {
        tailor_name.setEnabled(true);
        tailor_name_arabic.setEnabled(true);
        tailor_name.setEnabled(true);
//        mobile_number.setEnabled(true);
        email_id.setEnabled(true);
//        dob.setEnabled(true);
        save.setEnabled(true);
        calender.setEnabled(true);
        profile_gender_lay.setEnabled(true);
        userImage.setEnabled(true);
    }


    public void setEnableFalse() {
        tailor_name.setEnabled(false);
        tailor_name_arabic.setEnabled(false);
        tailor_name.setEnabled(false);
        mobile_number.setEnabled(false);
        email_id.setEnabled(false);
        dob.setEnabled(false);
        save.setEnabled(false);
        calender.setEnabled(false);
        profile_gender_lay.setEnabled(false);
        userImage.setEnabled(false);
    }


    private void uploadImages() {
//        AppConstants.ADD_MATERIAL = "ADD_MATERIAL";
        DialogManager.getInstance().showImageUploadPopup(getActivity(), getActivity().
                        getString(R.string.select_phot_type),
                getActivity().getString(R.string.take_camera),
                getActivity().getString(R.string.open_gallery), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {
                        captureImage();
                    }

                    @Override
                    public void onPositiveClick() {
                        galleryImage();
                    }
                });
    }

    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

                    Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                    String path = saveImage(thumbnail);
                    IMAGE_PATH = compressImage(String.valueOf(getImageUri(getContext(), thumbnail)));
//                    IMAGE_PATH = path;
//                    saveImage(thumbnail);

                    Glide.with(this)
                            .load(IMAGE_PATH)
                            .into(userImage);

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }


                }

                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    // successfully captured the image
                    // display it in image view
                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
//                        String path = saveImage(bitmap);
                        IMAGE_PATH = compressImage(String.valueOf(contentURI));

                        Glide.with(this)
                                .load(IMAGE_PATH)
                                .into(userImage);

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;

        }
    }

//    public String saveImage(Bitmap myBitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//        File wallpaperDirectory = new File(
//                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
//        // have the object build the directory structure, if needed.
//        if (!wallpaperDirectory.exists()) {
////            if (is() == true) {
//            wallpaperDirectory.mkdirs();
//
////            }
//        }
//
//        try {
//            File f = new File(wallpaperDirectory, Calendar.getInstance()
//                    .getTimeInMillis() + ".jpg");
//            f.createNewFile();
//            FileOutputStream fo = new FileOutputStream(f);
//            fo.write(bytes.toByteArray());
//            MediaScannerConnection.scanFile(getContext(),
//                    new String[]{f.getPath()},
//                    new String[]{"image/jpeg"}, null);
//            fo.close();
//            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());
//
//            return f.getAbsolutePath();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
//        return "";
//    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;

        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public void updateProfile1() {
        if (new UtilService().isNetworkAvailable(getActivity())) {


            try {

                mUserImageFile = new File(IMAGE_PATH);

                // create RequestBody instance from file
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);

                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("TailorImage", mUserImageFile.getName(), requestFile);
                restService.imageUpload2(body).enqueue(new Callback<ImageResponse>() {


                    @Override
                    public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                        if (response.isSuccessful()) {
                            String[] parts = response.body().getResult().get(0).split("\\\\");
                            String part1 = parts[0]; // 004
                            String part2 = parts[1];
                            String lastOne = parts[parts.length - 1];

                            try {
                                dialogManager.showProgress(getActivity());
                                HashMap<String, String> map = new HashMap<>();
                                map.put("TailorId", sharedPreferences.getString("TailorId", "100"));
                                map.put("TailorNameInEnglish", tailor_name.getText().toString());
                                map.put("TailorNameInArabic", tailor_name_arabic.getText().toString());
                                if (mUserImageFile == null) {
                                    map.put("ShopOwnerImageURL", "");
                                } else {
                                    map.put("ShopOwnerImageURL", lastOne.toString());

                                }
                                map.put("EmailId", email_id.getText().toString());
                                map.put("Dob", dob.getText().toString());
                                map.put("Gender", profile_gender_edt_txt.getText().toString());
//                                if (male.isChecked()) {
//                                    map.put("Gender", "male");
//
//                                } else if (female.isChecked()) {
//                                    map.put("Gender", "female");
//
//                                }

                                map.put("ModifiedBy", tailor_name.getText().toString());


                                restService.tailorProfileUpload(map).enqueue(new Callback<SignUpResponse>() {
                                    @Override
                                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                                        if (response.isSuccessful()) {

                                            if (response.body().getResponseMsg().equalsIgnoreCase("Success")) {
                                                AppConstants.CALENDER = "";


                                                final Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        // Do something after 5s = 5000ms
                                                        Toast.makeText(getActivity(), R.string.profile_created_sucessfully, Toast.LENGTH_LONG).show();


                                                        dialogManager.hideProgress();

                                                        if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {

                                                            ViewPager viewPager = (ViewPager) getActivity().findViewById(
                                                                    R.id.viewpager2);
                                                            viewPager.setCurrentItem(1);
                                                            Toast.makeText(getActivity(), "Create A Shop Profile To Complete The Registration Process", Toast.LENGTH_LONG).show();


                                                        } else {

                                                            startActivity(new Intent(getActivity(), HomeActivity.class));

                                                        }
                                                    }
                                                }, 5000);
                                            } else if (response.body().getResponseMsg().equalsIgnoreCase("Failure")) {
                                                Toast.makeText(getActivity(), "Profile Not Created", Toast.LENGTH_LONG).show();
                                                dialogManager.hideProgress();
                                                final Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        // Do something after 5s = 5000ms
                                                        Toast.makeText(getActivity(), "Profile Not Created", Toast.LENGTH_LONG).show();


                                                        dialogManager.hideProgress();
                                                    }
                                                }, 5000);

                                            }

                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                                        Toast.makeText(getActivity(), "No Profile Is Created", Toast.LENGTH_LONG).show();


                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }


                    }

                    @Override
                    public void onFailure(Call<ImageResponse> call, Throwable t) {


                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void updateProfileWithoutImage() {


        try {
            dialogManager.showProgress(getActivity());
            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences.getString("TailorId", "100"));
            map.put("TailorNameInEnglish", tailor_name.getText().toString());
            map.put("TailorNameInArabic", tailor_name_arabic.getText().toString());
//            map.put("ShopOwnerImageURL", shopOwnerImageUrlFromApi);
            if (sharedPreferencesImageName.getString("ImageName", "") != null) {
                map.put("ShopOwnerImageURL", sharedPreferencesImageName.getString("ImageName", ""));


            }
            if (sharedPreferencesImageName.getString("ImageName", "") == null) {

                map.put("ShopOwnerImageURL", shopOwnerImageUrlFromApi);
            } else {
                map.put("ShopOwnerImageURL", shopOwnerImageUrlFromApi);

            }

            map.put("EmailId", email_id.getText().toString());
            map.put("Dob", dob.getText().toString());
            map.put("Gender", profile_gender_edt_txt.getText().toString());
            map.put("ModifiedBy", tailor_name.getText().toString());
//            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                map.put("Gender", profile_gender_edt_txt.getText().toString());
//            } else {
//                map.put("Gender", "male");
//            }


//            if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                map.put("Gender", "إناثا");
//            } else {
//                map.put("Gender", "female");
//            }


            restService.tailorProfileUpload(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getResponseMsg().equalsIgnoreCase("Success")) {
                            AppConstants.CALENDER = "";


                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 5s = 5000ms
                                    Toast.makeText(getActivity(), R.string.profile_created_sucessfully, Toast.LENGTH_LONG).show();


                                    dialogManager.hideProgress();

                                    if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {

                                        ViewPager viewPager = (ViewPager) getActivity().findViewById(
                                                R.id.viewpager2);
                                        viewPager.setCurrentItem(1);
                                        Toast.makeText(getActivity(), "Create A Shop Profile To Complete The Registration Process", Toast.LENGTH_LONG).show();


                                    } else {

                                        startActivity(new Intent(getActivity(), HomeActivity.class));

                                    }
                                }
                            }, 5000);
                        } else if (response.body().getResponseMsg().equalsIgnoreCase("Failure")) {
                            Toast.makeText(getActivity(), "Profile Not Created", Toast.LENGTH_LONG).show();
                            dialogManager.hideProgress();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 5s = 5000ms
                                    Toast.makeText(getActivity(), "Profile Not Created", Toast.LENGTH_LONG).show();


                                    dialogManager.hideProgress();
                                }
                            }, 5000);

                        }

                    }

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), "No Profile Is Created", Toast.LENGTH_LONG).show();


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }


    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }


}


