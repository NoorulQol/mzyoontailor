package com.qoltech.mzyoontailor.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.wrappers.DashBoardHomeResponse;
import com.qoltech.mzyoontailor.wrappers.DashboardPojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class SupplierFragment extends Fragment {
    List<DashboardPojo> dashresp;
    TextView text_deliveredS, text_pendingS, text_approvedS, text_waitingS;
    ApiService restService;
    Context context;
    SharedPreferences sharedPreferences;

    public SupplierFragment() {
        // Required empty public constructor
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.supplier_layout, container, false);
        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        text_deliveredS = (TextView) rootView.findViewById(R.id.text_deliveredS);
        text_pendingS = (TextView) rootView.findViewById(R.id.text_pendingS);
        text_approvedS = (TextView) rootView.findViewById(R.id.text_approvedS);
        text_waitingS = (TextView) rootView.findViewById(R.id.text_waitingS);
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);
        getDetails();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getDetails() {
        if (new UtilService().isNetworkAvailable(getContext())) {

            try {
                restService.getDashboardData(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<DashBoardHomeResponse>() {
                    @Override
                    public void onResponse(Call<DashBoardHomeResponse> call, Response<DashBoardHomeResponse> response) {

                        if (response.isSuccessful()) {
                            dashresp = response.body().getResult();
                            if (dashresp == null) {
                                dashresp = new ArrayList<>();
                            }
                            text_deliveredS.setText("" + dashresp.get(0).getStockDelivered());
                            text_pendingS.setText("" + dashresp.get(0).getStockPending());
                            text_approvedS.setText("" + dashresp.get(0).getStockApproved());
                            text_waitingS.setText("" + dashresp.get(0).getStockWaiting());

                            Log.d("getArrayResponse", Arrays.toString(new List[]{dashresp}));
//                            Toast.makeText(getActivity(), "Sucess" + dashresp.get(0).getCompletedOrder(), Toast.LENGTH_SHORT).show();

                        } else if (response.errorBody() != null) {

                        }
                    }

                    @Override
                    public void onFailure(Call<DashBoardHomeResponse> call, Throwable t) {
                        Toast.makeText(getActivity(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                Toast.makeText(getActivity(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


}

