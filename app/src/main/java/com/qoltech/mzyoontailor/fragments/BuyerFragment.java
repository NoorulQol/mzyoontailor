package com.qoltech.mzyoontailor.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.BuyerRequestApproved;
import com.qoltech.mzyoontailor.activity.BuyerRequestPending;
import com.qoltech.mzyoontailor.activity.CustomerTypeActivity;
import com.qoltech.mzyoontailor.activity.MyProfileActivity;
import com.qoltech.mzyoontailor.activity.OrderDetailsDeliverd;
import com.qoltech.mzyoontailor.activity.OrderDetailsPending;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.DashBoardHomeResponse;
import com.qoltech.mzyoontailor.wrappers.DashboardPojo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class BuyerFragment extends Fragment {
    List<DashboardPojo> dashresp;
    TextView text_completed, text_pending, text_approved, text_pendingBuyerRequest, text_received, text_waiting;
    ApiService restService;
    Context context;
    LinearLayout new_order;
    RelativeLayout order_details, buyer_request, payment;
    Fragment fragment = null;
    SharedPreferences sharedPreferences;
    LinearLayout buyer_req_approved, buyer_req_pending, order_details_pending, order_details_completed;
    SwipeRefreshLayout swipeLayout;
    SharedPreferences sharedPreferencesShopId;

    public BuyerFragment() {
        // Required empty public constructor
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.buyer_layout, container, false);
        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        text_completed = (TextView) rootView.findViewById(R.id.text_completed);
        text_pending = (TextView) rootView.findViewById(R.id.text_pending);
        text_approved = (TextView) rootView.findViewById(R.id.text_approved);
        text_pendingBuyerRequest = (TextView) rootView.findViewById(R.id.text_pendingBuyerRequest);
        text_received = (TextView) rootView.findViewById(R.id.text_received);
        text_waiting = (TextView) rootView.findViewById(R.id.text_waiting);
        new_order = (LinearLayout) rootView.findViewById(R.id.new_order);
        order_details = (RelativeLayout) rootView.findViewById(R.id.order_details);
        buyer_request = (RelativeLayout) rootView.findViewById(R.id.buyer_request);
        payment = (RelativeLayout) rootView.findViewById(R.id.payment);
        buyer_req_approved = (LinearLayout) rootView.findViewById(R.id.buyer_req_approved);
        buyer_req_pending = (LinearLayout) rootView.findViewById(R.id.buyer_req_pending);
        order_details_pending = (LinearLayout) rootView.findViewById(R.id.order_details_pending);
        order_details_completed = (LinearLayout) rootView.findViewById(R.id.order_details_completed);
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);
        swipeLayout = rootView.findViewById(R.id.swipe_container);
        sharedPreferencesShopId = getActivity().getSharedPreferences("ShopId", MODE_PRIVATE);
        getDetails();

        new_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {
                    Toast.makeText(getActivity(), "Create A Shop Profile", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getActivity(), MyProfileActivity.class));

                } else {

                    AppConstants.TAILOR_DIRECT_ORDER = "TAILOR_DIRECT_ORDER";

                    startActivity(new Intent(getActivity(), CustomerTypeActivity.class));

                }
            }
        });

        buyer_req_approved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BuyerRequestApproved.class));
            }
        });

        buyer_req_pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.TAILOR_DIRECT_ORDER = "";

                startActivity(new Intent(getActivity(), BuyerRequestPending.class));
            }
        });

        order_details_pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_DETAILS = "ORDER_DETAILS_PENDING";

                startActivity(new Intent(getActivity(), OrderDetailsPending.class));
            }
        });
        order_details_completed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.ORDER_DETAILS = "ORDER_DETAILS_COMPLETED";
                startActivity(new Intent(getActivity(), OrderDetailsDeliverd.class));
            }
        });


        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code here
                // To keep animation for 4 seconds
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Stop animation (This will be after 3 seconds)
                        getDetails();
                        swipeLayout.setRefreshing(false);
                    }
                }, 4000); // Delay in millis
            }
        });

        // Scheme colors for animation
        swipeLayout.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_green_light),
                getResources().getColor(android.R.color.holo_orange_light),
                getResources().getColor(android.R.color.holo_red_light)
        );
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getDetails() {

        try {
            restService.getDashboardData(sharedPreferences.getString("TailorId", "10")).enqueue(new Callback<DashBoardHomeResponse>() {
                @Override
                public void onResponse(Call<DashBoardHomeResponse> call, Response<DashBoardHomeResponse> response) {

                    if (response.isSuccessful()) {
                        dashresp = response.body().getResult();
                        if (dashresp == null) {
                            dashresp = new ArrayList<>();
                        }
                        text_completed.setText("" + dashresp.get(0).getCompletedOrder());
                        text_pending.setText("" + dashresp.get(0).getPendingOrder());
                        text_approved.setText("" + dashresp.get(0).getApprovedRequest());
                        text_pendingBuyerRequest.setText("" + dashresp.get(0).getPendingRequest());
                        text_received.setText("" + dashresp.get(0).getReceivedAmount());
                        text_waiting.setText("" + dashresp.get(0).getPendingAmount());

                        Log.d("getArrayResponse", Arrays.toString(new List[]{dashresp}));
//                            Toast.makeText(getActivity(), "Sucess" + dashresp.get(0).getCompletedOrder(), Toast.LENGTH_SHORT).show();

                    } else if (response.errorBody() != null) {

                    }
                }

                @Override
                public void onFailure(Call<DashBoardHomeResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                getDetails();
            }
        }, 1000);

    }

    @Override
    public void onStart() {
        super.onStart();
        getDetails();
    }
}

