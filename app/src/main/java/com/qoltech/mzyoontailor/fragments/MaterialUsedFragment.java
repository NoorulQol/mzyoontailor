package com.qoltech.mzyoontailor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.activity.OrderConfirmation;
import com.qoltech.mzyoontailor.adapter.AdapterOneToNine;
import com.qoltech.mzyoontailor.adapter.MaterialUsedAdapter;
import com.qoltech.mzyoontailor.entity.InsertAttributeApiCallEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseFragment;
import com.qoltech.mzyoontailor.modal.AppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.InsertAttributeQuantityApicallModal;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.ui.AppointmentDetails;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.SelectDateFragment;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetAttributeDetailModal;
import com.qoltech.mzyoontailor.wrappers.GetDressSubtypeModal;
import com.qoltech.mzyoontailor.wrappers.GetMaterialType;
import com.qoltech.mzyoontailor.wrappers.GetMeasurementDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MaterialUsedFragment extends BaseFragment {
    public static Handler.Callback callback;
    ApiService restService;
    ImageView image_dropdown;
    OrderPricingFragment orderPricingFragment = new OrderPricingFragment();
    OrderConfirmation orderConfirmation = new OrderConfirmation();
    public static Dialog mOrderConfirmDialog;
    public static AdapterOneToNine mConfirmAdapter;
    public static ArrayList<String> mArrayList;
    MaterialUsedAdapter materialUsedAdapter;
    public static TextView quantity;
    RecyclerView material_used_recyclerview;
    List<GetAttributeDetailModal> getAttributeDetailModals;
    List<GetDressSubtypeModal> getDressSubtypeModals;
    List<GetMaterialType> getMaterialTypes;
    Button continue_btn;
    public static TextView calender_text, attribute_types;
    ImageView calender;
    EditText quantity1;
    SharedPreferences sharedPreferences;
    Dialog mAppointmentApprove;
    ArrayList<String> materialList;
    private String mMeasurementTypeBoolStr = "", mOrderTypeBoolStr = "",mOrderTypeNameStr = "",mMeasurementTypeNameStr = "";
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout;
    Gson gson;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View rootView = inflater.inflate(R.layout.fragment_material_used, container, false);
        image_dropdown = (ImageView) rootView.findViewById(R.id.image_dropdown);
        quantity = (TextView) rootView.findViewById(R.id.quantity);
        calender_text = (TextView) rootView.findViewById(R.id.calender_text);
        attribute_types = (TextView) rootView.findViewById(R.id.attribute_types);
        calender = (ImageView) rootView.findViewById(R.id.calender);
        continue_btn = (Button) rootView.findViewById(R.id.continue_btn);
        quantity1 = (EditText) rootView.findViewById(R.id.quantity1);
        sharedPreferences = getActivity().getSharedPreferences("TailorId", Context.MODE_PRIVATE);
        material_used_recyclerview = (RecyclerView) rootView.findViewById(R.id.material_used_recyclerview);
        mOrderConfirmDialog = new Dialog(getActivity());
        mOrderConfirmDialog.setCancelable(true);
        bgLayout = rootView.findViewById(R.id.bgLayout);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getActivity(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
//        getLanguage();
        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();

        getMeasurementDetails();
        getAppointmentMaterialApiCall();
        getAppointmentMeasurementApiCall();
   AppConstants.CLASS_NAME="Material_Used";
        mArrayList = new ArrayList<>();
        for (int i = 0; i <= 100; i++) {
            mArrayList.add(String.valueOf(i));
        }

        image_dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdapter(mArrayList);
            }
        });

        calender.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");

                AppConstants.CALENDER = "material_used";

            }
        });
        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (calender_text.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Select the Delivery Date", Toast.LENGTH_LONG).show();
                } else {

//                    alertDismiss(mAppointmentApprove);
//                    mAppointmentApprove = getDialog(getActivity(), R.layout.pop_up_cutomization_request);
//                    WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
//                    Window window = mAppointmentApprove.getWindow();
//                    if (window != null) {
//                        LayoutParams.copyFrom(window.getAttributes());
//                        LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
//                        LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                        window.setAttributes(LayoutParams);
//                        window.setGravity(Gravity.CENTER);
//                    }
//                    RelativeLayout okLay, recheduleLay;
//                    okLay = mAppointmentApprove.findViewById(R.id.appointment_reschedule_ok_lay);
//                    recheduleLay = mAppointmentApprove.findViewById(R.id.appointment_reschedule_check_lay);
//
//                    okLay.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            mAppointmentApprove.dismiss();
//
//                            if (AppConstants.INITIATED_BY.equalsIgnoreCase("Tailor")) {
//                                uploadOrderQuotation();
//                                insertAttributeQuantityApiCall();
//                                insertDirectCustomerAmout(AppConstants.ORDER_ID);
//                                updatePaymentStatus();
//                                insertApprovedTailor();
//                                startActivity(new Intent(getActivity(), HomeActivity.class));
//                            } else {
//                                uploadOrderQuotation();
//                                insertAttributeQuantityApiCall();
//                                if (mOrderTypeBoolStr.equalsIgnoreCase("true") || mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
//                                    startActivity(new Intent(getActivity(), AppointmentDetails.class));
//                                } else {
//                                    startActivity(new Intent(getActivity(), HomeActivity.class));
//                                    Toast.makeText(getActivity(), "Appointment Created", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//
//                        }
//                    });
//
//                    recheduleLay.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Toast.makeText(getActivity(), "We will reschedule the date and get you soon!", Toast.LENGTH_SHORT).show();
//                            mAppointmentApprove.dismiss();
//                        }
//                    });
//
//                    alertShowing(mAppointmentApprove);


                    if (AppConstants.INITIATED_BY.equalsIgnoreCase("Tailor")) {

                        if (AppConstants.TOTAL_RUPEES.equalsIgnoreCase("")) {
                            Toast.makeText(getActivity(), "Enter Minimum Charges", Toast.LENGTH_LONG).show();
                            ViewPager viewPager = (ViewPager) getActivity().findViewById(
                                    R.id.viewpager_order_confirmation);
                            viewPager.setCurrentItem(0);

                        } else if (orderPricingFragment.total_rupees.getText().toString().isEmpty() ||
                                orderPricingFragment.total_rupees.getText().toString().equalsIgnoreCase("0.0")) {
                            Toast.makeText(getActivity(), "Enter Minimum Charge", Toast.LENGTH_LONG).show();
                            ViewPager viewPager = (ViewPager) getActivity().findViewById(
                                    R.id.viewpager_order_confirmation);
                            viewPager.setCurrentItem(0);

                        } else if (Float.parseFloat(orderPricingFragment.total_rupees.getText().toString()) <
                                Float.parseFloat(orderPricingFragment.amount_paid.getText().toString())) {
                            Toast.makeText(getActivity(), "Amount Paid Exceeded Total Amount", Toast.LENGTH_LONG).show();
                        } else {

                            uploadOrderQuotationForTailorOrder();
                            insertAttributeQuantityApiCall();
//                            insertDirectCustomerAmout(AppConstants.ORDER_ID);

                            if (orderPricingFragment.amount_paid.getText().toString().equalsIgnoreCase("0")) {
                                updatePaymentStatus("1");
                            } else {
                                updatePaymentStatus("2");
                            }
                            insertApprovedTailor();
                            startActivity(new Intent(getActivity(), HomeActivity.class));
                            AppConstants.TOTAL_RUPEES = "";
                            AppConstants.MATERIAL_ID = "";
                            AppConstants.SUB_DRESS_TYPE_ID = "";
                        }


                    } else {

                        if (AppConstants.TOTAL_RUPEES.equalsIgnoreCase("")) {

                            Toast.makeText(getActivity(), "Enter Minimum Charges", Toast.LENGTH_LONG).show();
                            ViewPager viewPager = (ViewPager) getActivity().findViewById(
                                    R.id.viewpager_order_confirmation);
                            viewPager.setCurrentItem(0);

                        } else {


                            uploadOrderQuotation();
                            insertAttributeQuantityApiCall();

//                            if (mOrderTypeNameStr.equalsIgnoreCase("Companies-Material") &&
//                                    mMeasurementTypeNameStr.equalsIgnoreCase("Manually")){
//                                    Toast.makeText(getActivity(), "Appointment Created", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(getActivity(), HomeActivity.class));
//                                AppConstants.TOTAL_RUPEES = "";
//                            }else {
                                if (mOrderTypeBoolStr.equalsIgnoreCase("true") ||
                                        mMeasurementTypeBoolStr.equalsIgnoreCase("true")) {
                                    if (AppConstants.INITIATED_BY.equalsIgnoreCase("Tailor")) {
                                        startActivity(new Intent(getActivity(), HomeActivity.class));
                                        AppConstants.TOTAL_RUPEES = "";
                                    }
                                    else {
                                        startActivity(new Intent(getActivity(), AppointmentDetails.class));
                                        AppConstants.TOTAL_RUPEES = "";
                                    }
                                }
                                else {
                                    Toast.makeText(getActivity(), "Appointment Created", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getActivity(), HomeActivity.class));
                                    AppConstants.TOTAL_RUPEES = "";
                                }
//                            }


                        }

                    }

                }

            }
        });
        return rootView;
    }

    public void getAppointmentMaterialApiCall() {
        if (NetworkUtil.isNetworkAvailable(getActivity())) {

            APIRequestHandler.getInstance().getAppointmentMaterialFragApi(AppConstants.REQUEST_LIST_ID, MaterialUsedFragment.this);


        } else {
            DialogManager.getInstance().showNetworkErrorPopup(getActivity(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMaterialApiCall();
                }
            });
        }
    }

    public void getAppointmentMeasurementApiCall() {
        if (NetworkUtil.isNetworkAvailable(getActivity())) {

            APIRequestHandler.getInstance().getAppointmentMeasurementFragApi(AppConstants.REQUEST_LIST_ID,
                    MaterialUsedFragment.this);


        } else {
            DialogManager.getInstance().showNetworkErrorPopup(getActivity(), new InterfaceBtnCallBack() {
                @Override
                public void onPositiveClick() {
                    getAppointmentMeasurementApiCall();
                }
            });
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
    }


    public void uploadOrderQuotation() {
        try {

            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences.getString("TailorId", ""));
            map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            map.put("StichingTime", quantity.getText().toString());
            map.put("TailorCharges[0][TailorChargesId]", String.valueOf(1));
            map.put("TailorCharges[0][Amount]", orderPricingFragment.custumization_stitching_charge_rupees.getText().toString() + "." + orderPricingFragment.custumization_stitching_charge_paisa.getText().toString());
            map.put("TailorCharges[1][TailorChargesId]", String.valueOf(2));
            map.put("TailorCharges[1][Amount]", orderPricingFragment.appointment_charges_rupees.getText().toString() + "." + orderPricingFragment.appointment_charges_paisa.getText().toString());
            map.put("TailorCharges[2][TailorChargesId]", String.valueOf(3));
            map.put("TailorCharges[2][Amount]", orderPricingFragment.measurement_charges_rupees.getText().toString() + "." + orderPricingFragment.measurement_charges_paisa.getText().toString());
            map.put("TailorCharges[3][TailorChargesId]", String.valueOf(4));
            map.put("TailorCharges[3][Amount]", orderPricingFragment.urgent_charges_rupees.getText().toString() + "." + orderPricingFragment.urgent_charges_paisa.getText().toString());
//            map.put("TailorCharges[4][TailorChargesId]", String.valueOf(5));
//            map.put("TailorCharges[4][Amount]", orderPricingFragment.urgent_charges_rupees.getText().toString() + "." + orderPricingFragment.urgent_charges_paisa.getText().toString());
//            map.put("TailorCharges[5][TailorChargesId]", String.valueOf(6));
//            map.put("TailorCharges[5][Amount]", orderPricingFragment.delivery_charges_rupees.getText().toString() + "." + orderPricingFragment.delivery_charges_paisa.getText().toString());
//            map.put("TailorCharges[6][TailorChargesId]", String.valueOf(7));
//            map.put("TailorCharges[6][Amount]", orderPricingFragment.service_charge_rupees.getText().toString() + "." + orderPricingFragment.service_charge_paisa.getText().toString());
//            map.put("TailorCharges[7][TailorChargesId]", String.valueOf(8));
//            map.put("TailorCharges[7][Amount]", orderPricingFragment.tax_rupees.getText().toString() + "." + orderPricingFragment.tax_paisa.getText().toString());
            map.put("ApproximateDeliveryTime", calender_text.getText().toString());
//        map.put("TailorId", sharedPreferences.getString("TailorId",""));
//        map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            restService.orderQuotation(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + orderPricingFragment.rupees_total + orderPricingFragment.paisa_total, Toast.LENGTH_LONG).show();
//                    Toast.makeText(getActivity(), response.body().getResponseMsg() + orderPricingFragment.total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                    isApproveOrder();

                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getActivity(), "" + e, Toast.LENGTH_LONG).show();

        }
    }


    public void uploadOrderQuotationForTailorOrder() {
        try {

            HashMap<String, String> map = new HashMap<>();
            map.put("TailorId", sharedPreferences.getString("TailorId", ""));
            map.put("OrderId", String.valueOf(AppConstants.ORDER_ID));
            map.put("StichingTime", quantity.getText().toString());
            map.put("TailorCharges[0][TailorChargesId]", String.valueOf(1));
            map.put("TailorCharges[0][Amount]", orderPricingFragment.custumization_stitching_charge_rupees.getText().toString() + "." + orderPricingFragment.custumization_stitching_charge_paisa.getText().toString());
            map.put("TailorCharges[1][TailorChargesId]", String.valueOf(2));
            map.put("TailorCharges[1][Amount]", orderPricingFragment.appointment_charges_rupees.getText().toString() + "." + orderPricingFragment.appointment_charges_paisa.getText().toString());
            map.put("TailorCharges[2][TailorChargesId]", String.valueOf(3));
            map.put("TailorCharges[2][Amount]", orderPricingFragment.measurement_charges_rupees.getText().toString() + "." + orderPricingFragment.measurement_charges_paisa.getText().toString());
            map.put("TailorCharges[3][TailorChargesId]", String.valueOf(4));
            map.put("TailorCharges[3][Amount]", orderPricingFragment.urgent_charges_rupees.getText().toString() + "." + orderPricingFragment.urgent_charges_paisa.getText().toString());
//            map.put("TailorCharges[4][TailorChargesId]", String.valueOf(5));
//            map.put("TailorCharges[4][Amount]", orderPricingFragment.urgent_charges_rupees.getText().toString() + "." + orderPricingFragment.urgent_charges_paisa.getText().toString());
//            map.put("TailorCharges[5][TailorChargesId]", String.valueOf(6));
//            map.put("TailorCharges[5][Amount]", orderPricingFragment.delivery_charges_rupees.getText().toString() + "." + orderPricingFragment.delivery_charges_paisa.getText().toString());
//            map.put("TailorCharges[6][TailorChargesId]", String.valueOf(7));
//            map.put("TailorCharges[6][Amount]", orderPricingFragment.service_charge_rupees.getText().toString() + "." + orderPricingFragment.service_charge_paisa.getText().toString());
//            map.put("TailorCharges[7][TailorChargesId]", String.valueOf(8));
//            map.put("TailorCharges[7][Amount]", orderPricingFragment.tax_rupees.getText().toString() + "." + orderPricingFragment.tax_paisa.getText().toString());
            map.put("ApproximateDeliveryTime", calender_text.getText().toString());
            map.put("Amount", orderPricingFragment.total_rupees.getText().toString());
//            map.put("Balance", orderPricingFragment.amount_paid.getText().toString()); balance
            map.put("Balance", String.valueOf(orderPricingFragment.balance));
            restService.insertTailorChargesForTailorOrder(map).enqueue(new Callback<SignUpResponse>() {
                @Override
                public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + orderPricingFragment.rupees_total + orderPricingFragment.paisa_total, Toast.LENGTH_LONG).show();
//                    Toast.makeText(getActivity(), response.body().getResponseMsg() + orderPricingFragment.total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                    isApproveOrder();
                    updateQuantity();
                }

                @Override
                public void onFailure(Call<SignUpResponse> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            Toast.makeText(getActivity(), "" + e, Toast.LENGTH_LONG).show();

        }
    }


    public void insertAttributeQuantityApiCall() {
        InsertAttributeQuantityApicallModal insertAttributeQuantityApicallModal = new InsertAttributeQuantityApicallModal();

        ArrayList<InsertAttributeApiCallEntity> insertAttributeApiCallEntities = new ArrayList<>();

        insertAttributeQuantityApicallModal.setMaterialQty(quantity1.getText().toString());
        insertAttributeQuantityApicallModal.setOrderId(AppConstants.ORDER_ID);

        materialList = new ArrayList<>();


        Set keys = AppConstants.MATERIAL_MAP.keySet();
        Iterator itr = keys.iterator();

        String key;
        String value;
        while (itr.hasNext()) {

            key = (String) itr.next();
            value = (String) AppConstants.MATERIAL_MAP.get(key);
            System.out.println(key + " - " + value);
            materialList.add(value);

        }


        for (int i = 0; i < getAttributeDetailModals.size(); i++) {
            InsertAttributeApiCallEntity insertAttributeApiCallEntity = new InsertAttributeApiCallEntity();
            insertAttributeApiCallEntity.setAttributeId(String.valueOf(getAttributeDetailModals.get(i).getCustomizationAttributeId()));
            insertAttributeApiCallEntity.setAttributeTypeId(String.valueOf(getAttributeDetailModals.get(i).getAttributeImageId()));
            if (materialList.size() == getAttributeDetailModals.size()) {
                insertAttributeApiCallEntity.setQuantity(materialList.get(i));
            } else {


                insertAttributeApiCallEntity.setQuantity("0");

            }


            insertAttributeApiCallEntities.add(insertAttributeApiCallEntity);
        }

        insertAttributeQuantityApicallModal.setAttributeQuantity(insertAttributeApiCallEntities);


        restService.insertAttributeQuantity(insertAttributeQuantityApicallModal).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
//                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + orderPricingFragment.rupees_total + orderPricingFragment.paisa_total, Toast.LENGTH_LONG).show();
//                Toast.makeText(getActivity(), response.body().getResponseMsg() + orderPricingFragment.total_rupees.getText().toString(), Toast.LENGTH_LONG).show();

                AppConstants.MATERIAL_MAP = new HashMap<>();
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });

    }

    public void setAdapter(ArrayList<String> mArrayList) {


        alertDismiss(mOrderConfirmDialog);
        mOrderConfirmDialog = getDialog(getActivity(), R.layout.adapter_one_to_nine);

        WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
        Window window = mOrderConfirmDialog.getWindow();

        if (window != null) {
            LayoutParams.copyFrom(window.getAttributes());
            LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
            LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(LayoutParams);
            window.setGravity(Gravity.CENTER);
        }

        RecyclerView recyclerView;

        /*Init view*/

        recyclerView = mOrderConfirmDialog.findViewById(R.id.countryList);
        mConfirmAdapter = new AdapterOneToNine(getActivity(), mArrayList);
        final LinearLayoutManager layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mConfirmAdapter);

        alertShowing(mOrderConfirmDialog);

    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    /*Default dialog init method*/
    public static Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public void getMeasurementDetails() {

        restService.getMeasurementDetails(AppConstants.ORDER_ID).enqueue(new Callback<GetMeasurementDetailsResponse>() {
            @Override
            public void onResponse(Call<GetMeasurementDetailsResponse> call, Response<GetMeasurementDetailsResponse> response) {
                try {

                    getAttributeDetailModals = new ArrayList<>();
                    getDressSubtypeModals = new ArrayList<>();
                    getMaterialTypes = new ArrayList<>();
                    getAttributeDetailModals = response.body().getResult().getGetAttributeDetails();
                    getDressSubtypeModals = response.body().getResult().getGetDressSubtypes();
                    getMaterialTypes = response.body().getResult().getGetMaterialType();
                    try {
//                        Glide.with(getActivity())
//                                .load(GlobalData.SERVER_URL + "Images/DressSubType/" + getDressSubtypeModals.get(0).getImage())
//                                .apply(new RequestOptions().placeholder(R.color.app_border).error(R.color.app_border))
//                                .into(orderConfirmation.get_dress_sub_type_image);

                        Glide.with(getActivity())
                                .load(GlobalData.SERVER_URL + "Images/DressSubType/" + getDressSubtypeModals.get(0).getImage())
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true))
                                .into(orderConfirmation.get_dress_sub_type_image);
                    } catch (Exception ex) {
                        Log.e(AppConstants.TAG, ex.getMessage());


                    }
                    if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//                        ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);

                        orderConfirmation.get_dress_sub_type_name_txt.setText(getDressSubtypeModals.get(0).getNameInArabic());
                        attribute_types.setText(getMaterialTypes.get(0).getMaterialInArabic());

                    } else {
                        orderConfirmation.get_dress_sub_type_name_txt.setText(getDressSubtypeModals.get(0).getNameInEnglish());
                        attribute_types.setText(getMaterialTypes.get(0).getMaterialInEnglish());


                    }


                    setGenderList(getAttributeDetailModals);


                } catch (Exception e) {
                    getAttributeDetailModals = new ArrayList<>();
                    getDressSubtypeModals = new ArrayList<>();
                    getMaterialTypes = new ArrayList<>();

                }


            }

            @Override
            public void onFailure(Call<GetMeasurementDetailsResponse> call, Throwable t) {

            }
        });

    }


    private void setGenderList(List<GetAttributeDetailModal> getMeasurementDetailsResults) {

        materialUsedAdapter = new MaterialUsedAdapter(getActivity(), getMeasurementDetailsResults);
        material_used_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        material_used_recyclerview.setAdapter(materialUsedAdapter);


    }

    public void isApproveOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("IsApproved", "1");

        restService.approveOrder(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void updateQuantity() {

        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("Qty", "1");

        restService.updateQuantity(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof AppointmentMaterialResponse) {
            AppointmentMaterialResponse mResponse = (AppointmentMaterialResponse) resObj;
            if (mResponse.getResult().size() > 0) {
                mOrderTypeNameStr = mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish();
                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Courier the Material")) {

                    mOrderTypeBoolStr = "false";


                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Companies-Material")) {

                    mOrderTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")) {

                    mOrderTypeBoolStr = "true";
                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Tailor Come To Your Place")) {

                    mOrderTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Manually")) {
                    mOrderTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getHeaderInEnglish().equalsIgnoreCase("Go to Tailor Shop")) {

                    mOrderTypeBoolStr = "true";

                }

            }

        }
        if (resObj instanceof AppointmentMeasurementResponse) {
            AppointmentMeasurementResponse mResponse = (AppointmentMeasurementResponse) resObj;
            if (mResponse.getResult().size() > 0) {
                mMeasurementTypeNameStr = mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish();
                if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Tailor Come To Your Place")) {

                    mMeasurementTypeBoolStr = "false";
                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Manually")) {
                    mMeasurementTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Go to Tailor Shop")) {

                    mMeasurementTypeBoolStr = "true";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Own Material-Courier the Material")) {

                    mMeasurementTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Companies-Material")) {
                    mMeasurementTypeBoolStr = "false";

                } else if (mResponse.getResult().get(mResponse.getResult().size() - 1).getMeasurementInEnglish().equalsIgnoreCase("Own Material-Direct Delivery")) {

                    mMeasurementTypeBoolStr = "true";
                }

            }
        }
    }

//    public void getLanguage() {
//
//        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
//            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
////            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
////            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
//        } else {
//            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
////            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
////            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);
//
//
//        }
//    }


    public void insertDirectCustomerAmout(String order_id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", order_id);
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("UserId", AppConstants.DIRECT_USERS_ID);
        map.put("Amount", orderPricingFragment.total_rupees.getText().toString());
        map.put("Balance", orderPricingFragment.amount_paid.getText().toString());

        restService.insertDirectCustomerAmout(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


    public void updatePaymentStatus(String paymentStatus) {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("PaymentStatus", paymentStatus);

        restService.updatePaymentStatus(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void insertApprovedTailor() {
        HashMap<String, String> map = new HashMap<>();
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("ApprovedTailorId", sharedPreferences.getString("TailorId", ""));

        restService.buyerOrderApproval(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }
}