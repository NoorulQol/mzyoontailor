package com.qoltech.mzyoontailor.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.LocationSkillUpdateGetCountryActivity;
import com.qoltech.mzyoontailor.activity.MaterialMappingSkillUpdateActivity;
import com.qoltech.mzyoontailor.activity.MeasurementSkillUpdate;
import com.qoltech.mzyoontailor.activity.OrderTypeSkillUpdate;
import com.qoltech.mzyoontailor.activity.ServiceTypeSkillUpdate;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.ui.GenderScreen;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class SkillsUpdateFragment extends Fragment {
    LinearLayout gender_skill_update_click, dress_skill_update_click,
            materila_mapping_click, order_type_skill_update_click,
            customization_skill_update_click, measurement_skill_update_click,
            appointment_skill_update_click, location_skill_update_click;
    public static Handler.Callback callback;
    SharedPreferences sharedPreferences;

    Button fab;
    ApiService restService;
    TransparentProgressDialog transparentProgressDialog;
    private UserDetailsEntity mUserDetailsEntityRes;
    View rootView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        rootView = inflater.inflate(R.layout.fragment_skills_update, container, false);
//        gender_skill_update_click = (LinearLayout) rootView.findViewById(R.id.gender_skill_update_click);
        dress_skill_update_click = (LinearLayout) rootView.findViewById(R.id.dress_skill_update_click);
        materila_mapping_click = (LinearLayout) rootView.findViewById(R.id.materila_mapping_click);
        order_type_skill_update_click = (LinearLayout) rootView.findViewById(R.id.order_type_skill_update_click);
//        customization_skill_update_click = (LinearLayout) rootView.findViewById(R.id.customization_skill_update_click);
        measurement_skill_update_click = (LinearLayout) rootView.findViewById(R.id.measurement_skill_update_click);
        appointment_skill_update_click = (LinearLayout) rootView.findViewById(R.id.appointment_skill_update_click);
        location_skill_update_click = (LinearLayout) rootView.findViewById(R.id.location_skill_update_click);
//        fab = (Button) rootView.findViewById(R.id.fab);
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);


        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        initView();

        dress_skill_update_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.GENDER_SCREEN_SKILLUPDATE="SKILL_UPDATE_GENDER";
                startActivity(new Intent(getActivity(), GenderScreen.class));
            }
        });
        materila_mapping_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MaterialMappingSkillUpdateActivity.class));
            }
        });
        order_type_skill_update_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), OrderTypeSkillUpdate.class));
            }
        });
        measurement_skill_update_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MeasurementSkillUpdate.class));
            }
        });
        appointment_skill_update_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ServiceTypeSkillUpdate.class));
            }
        });
        location_skill_update_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LocationSkillUpdateGetCountryActivity.class));
            }
        });
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                transparentProgressDialog = new TransparentProgressDialog(getActivity());
//                transparentProgressDialog.show();
//                ArrayList<SkillIdEntity> mGenderEntity = new ArrayList<>();
//                ArrayList<SkillIdEntity> mDressEntity = new ArrayList<>();
//                ArrayList<SkillIdEntity> mOrderEntity = new ArrayList<>();
//                ArrayList<SkillIdEntity> mMeasurementEntity = new ArrayList<>();
//                ArrayList<SkillIdEntity> mAppointmentEntity = new ArrayList<>();
//                ArrayList<SkillIdEntity> mLocationEntity = new ArrayList<>();
//                ArrayList<DressSubIdEntity> mDressSubIdEntity = new ArrayList<>();
//
//                for (int i = 0; i < AppConstants.SKILL_GENDER_ID.size(); i++) {
//                    SkillIdEntity skillIdEntity = new SkillIdEntity();
//
//                    skillIdEntity.setId(AppConstants.SKILL_GENDER_ID.get(i));
//                    mGenderEntity.add(skillIdEntity);
//
//                }
//
//                for (int i = 0; i < AppConstants.SKILL_DRESS_ID.size(); i++) {
//                    SkillIdEntity skillIdEntity = new SkillIdEntity();
//
//                    skillIdEntity.setId(AppConstants.SKILL_DRESS_ID.get(i));
//                    mDressEntity.add(skillIdEntity);
//
//                }
//
//                for (int i = 0; i < AppConstants.SKILL_ORDER_ID.size(); i++) {
//                    SkillIdEntity skillIdEntity = new SkillIdEntity();
//
//                    skillIdEntity.setId(AppConstants.SKILL_ORDER_ID.get(i));
//                    mOrderEntity.add(skillIdEntity);
//
//                }
//
//                for (int i = 0; i < AppConstants.SKILL_MEASUREMENT_ID.size(); i++) {
//                    SkillIdEntity skillIdEntity = new SkillIdEntity();
//
//                    skillIdEntity.setId(AppConstants.SKILL_MEASUREMENT_ID.get(i));
//                    mMeasurementEntity.add(skillIdEntity);
//
//                }
//
//                for (int i = 0; i < AppConstants.SKILL_APPOINTMENT_ID.size(); i++) {
//                    SkillIdEntity skillIdEntity = new SkillIdEntity();
//
//                    skillIdEntity.setId(AppConstants.SKILL_APPOINTMENT_ID.get(i));
//                    mAppointmentEntity.add(skillIdEntity);
//
//                }
//
//                for (int i = 0; i < AppConstants.SKILL_LOCATION_ID.size(); i++) {
//                    SkillIdEntity skillIdEntity = new SkillIdEntity();
//
//                    skillIdEntity.setId(AppConstants.SKILL_LOCATION_ID.get(i));
//                    mLocationEntity.add(skillIdEntity);
//
//                }
//
//
//                for (int i = 0; i < AppConstants.SKILL_CUSTOMIZATION_ID.size(); i++) {
//                    DressSubIdEntity dressSubIdEntity = new DressSubIdEntity();
//
//                    dressSubIdEntity.setCustomizationId(AppConstants.SKILL_CUSTOMIZATION_ID.get(i));
////                    mDressSubIdEntity.add(dressSubIdEntity);
//
//                    for (int j = 0; j < AppConstants.SKILL_SUB_DRESS_ID.size(); i++) {
//
//                        dressSubIdEntity.setDressSubTypeId(AppConstants.SKILL_SUB_DRESS_ID.get(j));
//
//
//                    }
//                    mDressSubIdEntity.add(dressSubIdEntity);
//                }
//
//
//                SkillUpdateResponse mResponse = new SkillUpdateResponse();
//                mResponse.setTailorId(sharedPreferences.getString("TailorId", ""));
//                mResponse.setGenderId(mGenderEntity);
//                mResponse.setDressType(mDressEntity);
//                mResponse.setOrderType(mOrderEntity);
//                mResponse.setMeasurementId(mMeasurementEntity);
//                mResponse.setAppoinmentId(mAppointmentEntity);
//                mResponse.setAreaId(mLocationEntity);
//                mResponse.setDressSubtypeCustomazationMapping(mDressSubIdEntity);
//
//                restService.insertSkillUpdate(mResponse).enqueue(new Callback<OrderSummaryResponse>() {
//                    @Override
//                    public void onResponse(Call<OrderSummaryResponse> call, Response<OrderSummaryResponse> response) {
//
//                        final Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                // Do something after 5s = 5000ms
//                                Toast.makeText(getActivity(), "Skill Updated Successfully", Toast.LENGTH_LONG).show();
//
//
//                                transparentProgressDialog.dismiss();
//                            }
//                        }, 5000);
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<OrderSummaryResponse> call, Throwable t) {
//
//                    }
//                });
//            }
//        });
        return rootView;


    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
    }


    private void initView() {
//        AppConstants.HOME_SCREEN = this;

//        setupUI(mHomeScreenParLay);

        mUserDetailsEntityRes = new UserDetailsEntity();


        Gson gson = new Gson();
        String json = PreferenceUtil.getStringValue(getContext(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);

//        mSetHeader();

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);

        }

    }

}

