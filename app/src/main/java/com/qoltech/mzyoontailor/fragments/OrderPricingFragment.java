package com.qoltech.mzyoontailor.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.OrderConfirmationTabAdapter;
import com.qoltech.mzyoontailor.entity.GetTailorDefaultChargesEntity;
import com.qoltech.mzyoontailor.modal.GetMaterialChargesByTailorResponse;
import com.qoltech.mzyoontailor.modal.GetTailorDefaultChargesResponse;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class OrderPricingFragment extends Fragment {
    public static Handler.Callback callback;

    public static EditText measurement_charges_rupees, measurement_charges_paisa, custumization_stitching_charge_rupees,
            custumization_stitching_charge_paisa, appointment_charges_rupees, appointment_charges_paisa,
            material_delivery_charges_rupees,
            material_delivery_charges_paisa, urgent_charges_rupees, urgent_charges_paisa,
            delivery_charges_rupees, delivery_charges_paisa, service_charge_rupees, service_charge_paisa,
            tax_rupees, tax_paisa, total_rupees, amount_paid;
    //    Button continue_btn;
    ApiService restService;
    int rupees_total, paisa_total;
    int firstDigit, lastDigit;
    int add, increment = 0;
    public static float balance;
    OrderConfirmationTabAdapter orderConfirmationTabAdapter;
    LinearLayout measurement_charges_layout, delivery_charges_layout, service_type_charges, amount_paid_lay, material_charges_layout;
    List<GetTailorDefaultChargesEntity> getTailorDefaultChargesEntities;
    String r1, r2, r3, r4, p1, p2, p3, p4;
    SharedPreferences sharedPreferences;
    Context context;
    int i, j = 0;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View rootView = inflater.inflate(R.layout.fragment_order_pricing, container, false);
        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        measurement_charges_rupees = (EditText) rootView.findViewById(R.id.measurement_charges_rupees);
        measurement_charges_paisa = (EditText) rootView.findViewById(R.id.measurement_charges_paisa);
        custumization_stitching_charge_rupees = (EditText) rootView.findViewById(R.id.custumization_stitching_charge_rupees);
        custumization_stitching_charge_paisa = (EditText) rootView.findViewById(R.id.custumization_stitching_charge_paisa);
        appointment_charges_rupees = (EditText) rootView.findViewById(R.id.appointment_charges_rupees);
        appointment_charges_paisa = (EditText) rootView.findViewById(R.id.appointment_charges_paisa);
        material_delivery_charges_rupees = (EditText) rootView.findViewById(R.id.material_delivery_charges_rupees);
        material_delivery_charges_paisa = (EditText) rootView.findViewById(R.id.material_delivery_charges_paisa);
        urgent_charges_rupees = (EditText) rootView.findViewById(R.id.urgent_charges_rupees);
        urgent_charges_paisa = (EditText) rootView.findViewById(R.id.urgent_charges_paisa);
        delivery_charges_rupees = (EditText) rootView.findViewById(R.id.delivery_charges_rupees);
        delivery_charges_paisa = (EditText) rootView.findViewById(R.id.delivery_charges_paisa);
        service_charge_rupees = (EditText) rootView.findViewById(R.id.service_charge_rupees);
        service_charge_paisa = (EditText) rootView.findViewById(R.id.service_charge_paisa);
        tax_rupees = (EditText) rootView.findViewById(R.id.tax_rupees);
        tax_paisa = (EditText) rootView.findViewById(R.id.tax_paisa);
        total_rupees = (EditText) rootView.findViewById(R.id.total_rupees);

        amount_paid = (EditText) rootView.findViewById(R.id.amount_paid);
//        continue_btn = (Button) rootView.findViewById(R.id.continue_btn);
        measurement_charges_layout = (LinearLayout) rootView.findViewById(R.id.measurement_charges_layout);
        delivery_charges_layout = (LinearLayout) rootView.findViewById(R.id.delivery_charges_layout);
        service_type_charges = (LinearLayout) rootView.findViewById(R.id.service_type_charges);
        amount_paid_lay = (LinearLayout) rootView.findViewById(R.id.amount_paid_lay);
        material_charges_layout = (LinearLayout) rootView.findViewById(R.id.material_charges_layout);
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);
        getDefaultChargesCharges();


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
//                finish();
//                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
//                AppConstants.MATERIAL_ID = String.valueOf(item.getMaterialId());
//                AppConstants.SUB_DRESS_TYPE_ID
                getStichingChargesbyTailor(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID);
                getMaterialChargesByTailor(sharedPreferences.getString("TailorId", ""), AppConstants.SUB_DRESS_TYPE_ID,
                        AppConstants.MATERIAL_ID);
            }
        }, 1000);


//        paisa_total =
//                Integer.parseInt(measurement_charges_paisa.getText().toString()) +
//                        Integer.parseInt(custumization_stitching_charge_paisa.getText().toString()) +
//                        Integer.parseInt(appointment_charges_paisa.getText().toString()) +
//                        Integer.parseInt(material_delivery_charges_paisa.getText().toString()) +
//                        Integer.parseInt(urgent_charges_paisa.getText().toString()) +
//                        Integer.parseInt(delivery_charges_paisa.getText().toString()) +
//                        Integer.parseInt(service_charge_paisa.getText().toString()) +
//                        Integer.parseInt(tax_paisa.getText().toString());
//
//        rupees_total =
//                Integer.parseInt(measurement_charges_rupees.getText().toString()) +
//                        Integer.parseInt(custumization_stitching_charge_rupees.getText().toString()) +
//                        Integer.parseInt(appointment_charges_rupees.getText().toString()) +
//                        Integer.parseInt(material_delivery_charges_rupees.getText().toString()) +
//                        Integer.parseInt(urgent_charges_rupees.getText().toString()) +
//                        Integer.parseInt(delivery_charges_rupees.getText().toString()) +
//                        Integer.parseInt(service_charge_rupees.getText().toString()) +
//                        Integer.parseInt(tax_rupees.getText().toString());


        if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1 &&
                AppConstants.ORDER_TYPE_CHARGES == 3) {
            measurement_charges_layout.setVisibility(View.GONE);
            material_charges_layout.setVisibility(View.VISIBLE);

        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 1) {
            measurement_charges_layout.setVisibility(View.GONE);

        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 3) {
            measurement_charges_layout.setVisibility(View.VISIBLE);
            j = 1;

        } else if (AppConstants.MEASUREMENT_TYPE_CHARGES == 2) {
            measurement_charges_layout.setVisibility(View.VISIBLE);
            j = 1;

        } else if (AppConstants.ORDER_TYPE_CHARGES == 3) {
            measurement_charges_layout.setVisibility(View.GONE);


        } else {
            measurement_charges_layout.setVisibility(View.VISIBLE);

            j = 1;


        }


        if (AppConstants.ORDER_TYPE_CHARGES == 3) {

            material_charges_layout.setVisibility(View.VISIBLE);
        } else {

            material_charges_layout.setVisibility(View.GONE);

        }

        if (AppConstants.SERVICE_TYPE_CHARGES == 1) {

            service_type_charges.setVisibility(View.VISIBLE);
            i = 1;
        } else {

            service_type_charges.setVisibility(View.GONE);

        }


//        amount_paid.setText("0");

        amount_paid.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                balance = Float.parseFloat(AppConstants.TOTAL_RUPEES) -
                        Float.parseFloat(amount_paid.getText().toString().equalsIgnoreCase("") ? "0" :
                                amount_paid.getText().toString());

//                Toast.makeText(getActivity(), "" + balance, Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        if (AppConstants.INITIATED_BY.equalsIgnoreCase("Tailor")) {

            amount_paid_lay.setVisibility(View.VISIBLE);
        } else {
            amount_paid_lay.setVisibility(View.GONE);

        }


        measurement_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +

                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +

                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        measurement_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +

                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +

                                Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString());
                ;


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        custumization_stitching_charge_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +
                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +

                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +

                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        custumization_stitching_charge_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +
                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +

                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +

                                Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        appointment_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +
                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +
                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +
                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        appointment_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +
                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +
                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +
                                Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        material_delivery_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +

                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +

                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +

                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +

                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        material_delivery_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +
                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +
                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +
                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +
                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +
                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        urgent_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +
                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +
                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        urgent_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +

                                Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        delivery_charges_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +
                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +
                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +
                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +
                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        delivery_charges_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +
                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +
                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +
                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +
                                Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        service_charge_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +

                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +

                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +

                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString()) +

                                Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString());


                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        service_charge_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +

                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +

                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +

                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +

                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString()) +

                                Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tax_rupees.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rupees_total =
                        Integer.parseInt(tax_rupees.getText().toString().equalsIgnoreCase("") ? "0" : tax_rupees.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_rupees.getText().toString()) +
                                Integer.parseInt(appointment_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_rupees.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(urgent_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_rupees.getText().toString()) +
                                Integer.parseInt(delivery_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_rupees.getText().toString()) +
                                Integer.parseInt(service_charge_rupees.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_rupees.getText().toString()) +
                                Integer.parseInt(measurement_charges_rupees.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_rupees.getText().toString());

                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tax_paisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                paisa_total =
                        Integer.parseInt(tax_paisa.getText().toString().equalsIgnoreCase("") ? "0" : tax_paisa.getText().toString()) +
                                Integer.parseInt(custumization_stitching_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : custumization_stitching_charge_paisa.getText().toString()) +
                                Integer.parseInt(appointment_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : appointment_charges_paisa.getText().toString()) +
                                Integer.parseInt(material_delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : material_delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(urgent_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : urgent_charges_paisa.getText().toString()) +
                                Integer.parseInt(delivery_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : delivery_charges_paisa.getText().toString()) +
                                Integer.parseInt(service_charge_paisa.getText().toString().equalsIgnoreCase("") ? "0" : service_charge_paisa.getText().toString()) +
                                Integer.parseInt(measurement_charges_paisa.getText().toString().equalsIgnoreCase("") ? "0" : measurement_charges_paisa.getText().toString());
                if (Integer.toString(paisa_total).length() > 2) {
                    firstDigit = Integer.parseInt(Integer.toString(paisa_total).substring(0, 1));
                    lastDigit = Integer.parseInt(Integer.toString(paisa_total).substring(1, 3));
                    add = rupees_total + firstDigit;
                    total_rupees.setText(add + "." + lastDigit);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();


                } else {

                    total_rupees.setText(rupees_total + "." + paisa_total);
                    AppConstants.TOTAL_RUPEES = total_rupees.getText().toString();

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        continue_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                uploadOrderQuotation();
//
//                if (!measurement_charges_rupees.getText().toString().isEmpty() &&
//                        !custumization_stitching_charge_rupees.getText().toString().isEmpty() &&
//                        !appointment_charges_rupees.getText().toString().isEmpty() &&
//                        !material_delivery_charges_rupees.getText().toString().isEmpty() &&
//                        !urgent_charges_rupees.getText().toString().isEmpty() &&
//                        !delivery_charges_rupees.getText().toString().isEmpty() &&
//                        !service_charge_rupees.getText().toString().isEmpty() &&
//                        !tax_rupees.getText().toString().isEmpty() && !measurement_charges_paisa.getText().toString().isEmpty() &&
//                        !custumization_stitching_charge_paisa.getText().toString().isEmpty() &&
//                        !appointment_charges_paisa.getText().toString().isEmpty() &&
//                        !material_delivery_charges_paisa.getText().toString().isEmpty() &&
//                        !urgent_charges_paisa.getText().toString().isEmpty() &&
//                        !delivery_charges_paisa.getText().toString().isEmpty() &&
//                        !service_charge_paisa.getText().toString().isEmpty() &&
//                        !tax_paisa.getText().toString().isEmpty()
//
//                        ) {
//                    ViewPager viewPager = (ViewPager) getActivity().findViewById(
//                            R.id.viewpager_order_confirmation);
//                    viewPager.setCurrentItem(1);
//
//                } else {
//
//                    increment++;
//                    if (increment > 3) {
//                        Toast.makeText(getActivity(), "Please Fill Some Data For Empty Values \n             " +
//                                "To Proceed Next", Toast.LENGTH_LONG).show();
//
//                    } else {
//                        Toast.makeText(getActivity(), "Please Fill All The Feild", Toast.LENGTH_LONG).show();
//
//                    }
//                }
//
//            }
//        });
        return rootView;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
    }


    public void uploadOrderQuotation() {

        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", String.valueOf(1));
        map.put("StichingTime", "21 days");
        map.put("OrderId", String.valueOf(72));
        map.put("TailorCharges[0][TailorChargesId]", String.valueOf(9));
        map.put("TailorCharges[0][Amount]", measurement_charges_rupees.getText().toString() + "." + measurement_charges_paisa.getText().toString());
        map.put("TailorCharges[1][TailorChargesId]", String.valueOf(11));
        map.put("TailorCharges[1][Amount]", custumization_stitching_charge_rupees.getText().toString() + "." + custumization_stitching_charge_paisa.getText().toString());
        map.put("TailorCharges[2][TailorChargesId]", String.valueOf(12));
        map.put("TailorCharges[2][Amount]", appointment_charges_rupees.getText().toString() + "." + appointment_charges_paisa.getText().toString());
        map.put("TailorCharges[3][TailorChargesId]", String.valueOf(14));
        map.put("TailorCharges[3][Amount]", material_delivery_charges_rupees.getText().toString() + "." + material_delivery_charges_paisa.getText().toString());
        map.put("TailorCharges[4][TailorChargesId]", String.valueOf(1));
        map.put("TailorCharges[4][Amount]", urgent_charges_rupees.getText().toString() + "." + urgent_charges_paisa.getText().toString());
        map.put("TailorCharges[5][TailorChargesId]", String.valueOf(1));
        map.put("TailorCharges[5][Amount]", delivery_charges_rupees.getText().toString() + "." + delivery_charges_paisa.getText().toString());
        map.put("TailorCharges[6][TailorChargesId]", String.valueOf(1));
        map.put("TailorCharges[6][Amount]", service_charge_rupees.getText().toString() + "." + service_charge_paisa.getText().toString());
        map.put("TailorCharges[7][TailorChargesId]", String.valueOf(1));
        map.put("TailorCharges[7][Amount]", String.valueOf(rupees_total) + "." + String.valueOf(paisa_total));
        map.put("ApproximateDeliveryDate", "02/07/2020");

        restService.orderQuotation(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                Toast.makeText(getActivity(), response.body().getResponseMsg().toString() + rupees_total + paisa_total, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }


    public void getDefaultChargesCharges() {


        restService.getDefaultCharges().enqueue(new Callback<GetTailorDefaultChargesResponse>() {
            @Override
            public void onResponse(Call<GetTailorDefaultChargesResponse> call, Response<GetTailorDefaultChargesResponse> response) {
                getTailorDefaultChargesEntities = response.body().getResult();


//                custumization_stitching_charge_rupees.setText(String.valueOf(getTailorDefaultChargesEntities.get(0).getAmount()));

//                custumization_stitching_charge_rupees.setText(separated[0]);
//                custumization_stitching_charge_paisa.setText(separated[1]);

                String s1 = String.valueOf(response.body().getResult().get(2).getAmount());
                String s2 = String.valueOf(response.body().getResult().get(3).getAmount());
                String s3 = String.valueOf(response.body().getResult().get(0).getAmount());
                String s4 = String.valueOf(response.body().getResult().get(1).getAmount());
                String[] separated1 = s1.split("\\.");
                String[] separated2 = s2.split("\\.");
                String[] separated3 = s3.split("\\.");
                String[] separated4 = s4.split("\\.");

//              Stitching Charges
                r1 = separated1[0];
                p1 = separated1[1];
//              material Charges
                r2 = separated2[0];
                p2 = separated2[1];
//             Measurement Charges
                r3 = separated3[0];
                p3 = separated3[1];
//            Urgent Charges
                r4 = separated4[0];
                p4 = separated4[1];


                if (i == 1) {
                    urgent_charges_rupees.setText(r4);
                    urgent_charges_paisa.setText(p4);
                }

                if (j == 1) {
                    measurement_charges_rupees.setText(r3);
                    measurement_charges_paisa.setText(p3);
                }

            }

            @Override
            public void onFailure(Call<GetTailorDefaultChargesResponse> call, Throwable t) {

            }
        });
    }


    public void getMaterialChargesByTailor(String tailorId, String subTypeId, String materialId) {


        restService.getMaterialChargesByTailor(tailorId, subTypeId, materialId).enqueue(new Callback<GetMaterialChargesByTailorResponse>() {
            @Override
            public void onResponse(Call<GetMaterialChargesByTailorResponse> call, Response<GetMaterialChargesByTailorResponse> response) {
                try {
                    if (response.body().getResponseMsg().equalsIgnoreCase("Success")) {


                        if (AppConstants.ORDER_TYPE_CHARGES == 3) {


                            if (response.body().getResult() == 0.0) {
//                    getDefaultChargesForMaterialtCharges();

                                appointment_charges_rupees.setText(r2);
                                appointment_charges_paisa.setText(p2);

                            } else {
                                String s1 = String.valueOf(response.body().getResult());
                                String[] separated1 = s1.split("\\.");

                                appointment_charges_rupees.setText(separated1[0]);
                                appointment_charges_paisa.setText(separated1[1]);
                            }

                        } else {
                            appointment_charges_rupees.setText("0");
                            appointment_charges_paisa.setText("0");

                        }

                    } else if (response.body().getResponseMsg().equalsIgnoreCase("Failure")) {
                        appointment_charges_rupees.setText("0");
                        appointment_charges_paisa.setText("0");

                    }
                } catch (Exception e) {
                    appointment_charges_rupees.setText("0");
                    appointment_charges_paisa.setText("0");
                }

            }

            @Override
            public void onFailure(Call<GetMaterialChargesByTailorResponse> call, Throwable t) {
                appointment_charges_rupees.setText("0");
                appointment_charges_paisa.setText("0");

            }
        });
    }


    public void getStichingChargesbyTailor(String tailorId, String subTypeId) {


        restService.getStichingChargesbyTailor(tailorId, subTypeId).enqueue(new Callback<GetMaterialChargesByTailorResponse>() {
            @Override
            public void onResponse(Call<GetMaterialChargesByTailorResponse> call, Response<GetMaterialChargesByTailorResponse> response) {
                if (response.body().getResult() == 0.0) {
//                    getDefaultChargesForStitchingCharges();


                    custumization_stitching_charge_rupees.setText(r1);
                    custumization_stitching_charge_paisa.setText(p1);
                } else {
//                    custumization_stitching_charge_rupees.setText(String.valueOf(response.body().getResult()));

                    String s1 = String.valueOf(response.body().getResult());
                    String[] separated1 = s1.split("\\.");

                    custumization_stitching_charge_rupees.setText(separated1[0]);
                    custumization_stitching_charge_paisa.setText(separated1[1]);

                }

            }

            @Override
            public void onFailure(Call<GetMaterialChargesByTailorResponse> call, Throwable t) {

            }
        });
    }


}
