package com.qoltech.mzyoontailor.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.CommenInterface.InterfaceTwoBtnCallBack;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.activity.MapsActivity;
import com.qoltech.mzyoontailor.adapter.AddMaterialAdapter;
import com.qoltech.mzyoontailor.adapter.CountryCodeAdapter;
import com.qoltech.mzyoontailor.adapter.GetShopImagesAdapter;
import com.qoltech.mzyoontailor.adapter.StateAdapter;
import com.qoltech.mzyoontailor.entity.ShopImagesEntity;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.main.BaseFragment;
import com.qoltech.mzyoontailor.modal.DeleteShopImageResponse;
import com.qoltech.mzyoontailor.modal.FileUploadResponse;
import com.qoltech.mzyoontailor.modal.InsertUpdateShopResponse;
import com.qoltech.mzyoontailor.service.APIRequestHandler;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.util.TransparentProgressDialog;
import com.qoltech.mzyoontailor.util.UtilService;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.utils.NetworkUtil;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.Country;
import com.qoltech.mzyoontailor.wrappers.CountryResponse;
import com.qoltech.mzyoontailor.wrappers.GetShopImagesModal;
import com.qoltech.mzyoontailor.wrappers.GetShopProfileModal;
import com.qoltech.mzyoontailor.wrappers.GetShopProfileResponse;
import com.qoltech.mzyoontailor.wrappers.ImageResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;
import com.qoltech.mzyoontailor.wrappers.State;
import com.qoltech.mzyoontailor.wrappers.StateResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.google.android.gms.plus.PlusOneDummyView.TAG;

public class ShopDetailsFrag extends BaseFragment {
    public static TextView textCountrySelection = null;
    public static ImageView flagsImg = null;
    public static TextView countries_text, state_text;
    ImageView country_imgSpinner, state_imgSpinner;
    List<Country> countries;
    List<GetShopProfileModal> getShopProfileModals;
    List<GetShopImagesModal> getShopImagesModals;
    List<State> states;
    RecyclerView countryList;
    ApiService restService;
    static ApiService mrestService;
    LinearLayoutManager linearLayoutManager;
    CountryCodeAdapter countryCodeDialogAdapter;
    StateAdapter stateAdapter;
    Country country;
    State state;
    static boolean rememberLastSelection = false;
    static Context context;
    static String CCP_PREF_FILE = "CCP_PREF_FILE";
    static String selectionMemoryTag = "ccp_last_selection";
    int i = 0;
    public static TransparentProgressDialog progressDialog;
    public static DialogManager dialogManager;
    Button add_more_photos;
    private String profile_photo, shop_photo;
    private Uri mPictureFileUri;
    public final int REQUEST_CAMERA = 999;
    public final int REQUEST_GALLERY = 888;
    private static final String IMAGE_DIRECTORY = "/MZYOON";
    private String mUserProfileImagePath = "";
    private File mUserImageFile = null;
    private ArrayList<String> mAddImageList;
    private AddMaterialAdapter mAddMaterialAdapter;
    private GetShopImagesAdapter getShopImagesAdapter;
    public static CircularImageView userImage;
    RecyclerView add_shop_images_recyclerview;
    SharedPreferences sharedPreferences, sharedPreferencesTailorId, sharedPreferencesShopId;
    EditText shop_name_english, shop_name_arabic, shop_address_english, shop_address_arabic;
    public static TextView set_from_map;
    public static TextView address_from_map;
    public static LinearLayout location;
    Button edit_location, save_shop_detials_btn;
    public SignUpResponse signUp = new SignUpResponse();
    MapsActivity mapsActivity;
    SharedPreferences.Editor editor, editorLoc;
    LinearLayout set_from_map_layout;
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout_lay;
    Gson gson;
    RelativeLayout location_layout;
    String IMAGE_PATH = "";
    Button edit, edit_profile;
    int condition = 0;

    int buttonState=0;

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_shop_details_one, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        }
        sharedPreferencesTailorId = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);
        sharedPreferencesShopId = getActivity().getSharedPreferences("ShopId", MODE_PRIVATE);
        editor = sharedPreferencesShopId.edit();
        sharedPreferences = getActivity().getSharedPreferences("Location", MODE_PRIVATE);
        location = (LinearLayout) rootView.findViewById(R.id.location);
        set_from_map = (TextView) rootView.findViewById(R.id.set_from_map);
        address_from_map = (TextView) rootView.findViewById(R.id.address_from_map);
        shop_name_english = (EditText) rootView.findViewById(R.id.shop_name_english);
        shop_name_arabic = (EditText) rootView.findViewById(R.id.shop_name_arabic);
        shop_address_english = (EditText) rootView.findViewById(R.id.shop_address_english);
        shop_address_arabic = (EditText) rootView.findViewById(R.id.shop_address_arabic);
        edit_location = (Button) rootView.findViewById(R.id.edit_location);
        save_shop_detials_btn = (Button) rootView.findViewById(R.id.save_shop_detials_btn);
        countries_text = (TextView) rootView.findViewById(R.id.countries_text);
        add_more_photos = (Button) rootView.findViewById(R.id.add_more_photos);
        state_text = (TextView) rootView.findViewById(R.id.state_text);
        flagsImg = (ImageView) rootView.findViewById(R.id.flagsImg);
        country_imgSpinner = (ImageView) rootView.findViewById(R.id.country_imgSpinner);
        state_imgSpinner = (ImageView) rootView.findViewById(R.id.state_imgSpinner);
        userImage = (CircularImageView) rootView.findViewById(R.id.userImage);
        add_shop_images_recyclerview = (RecyclerView) rootView.findViewById(R.id.add_shop_images_recyclerview);
        set_from_map_layout = (LinearLayout) rootView.findViewById(R.id.set_from_map_layout);
        edit_profile = (Button) rootView.findViewById(R.id.edit_profile);
        setupUI(rootView.findViewById(R.id.bgLayout));
        progressDialog = new TransparentProgressDialog(getActivity());
        mapsActivity = new MapsActivity();
        mAddImageList = new ArrayList<>();
        location_layout = (RelativeLayout) rootView.findViewById(R.id.location_layout);
        bgLayout_lay = (LinearLayout) rootView.findViewById(R.id.bgLayout_lay);
        dialogManager = new DialogManager();
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getActivity(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        edit = rootView.findViewById(R.id.edit);
        editorLoc = sharedPreferences.edit();

        getLanguage();
        getCountryList();
        if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {
            edit_profile.setVisibility(View.GONE);
//            getShopImagesAdapter.selectAll();
            Toast.makeText(getActivity(), "Create Shop Details", Toast.LENGTH_LONG).show();
            getShopImagesModals = new ArrayList<>();
            setGenderList(getShopImagesModals);

        } else {
            getShopProfile();
            setEnableFalse();
        }


        add_more_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                shop_photo = 2;
//                shop_photo = "Shop";
//                profile_photo = "";
                uploadImages();
                condition = 1;


            }
        });
        set_from_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        });
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEnableTrue();
                edit_profile.setVisibility(View.GONE);
                getShopImagesAdapter.selectAll();
                buttonState=1;



            }
        });
        add_shop_images_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (buttonState==1){
                    dialogManager.showProgress(getActivity());

                    setEnableTrue();
                    edit_profile.setVisibility(View.GONE);
                    getShopImagesAdapter.selectAll();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setEnableTrue();
                            edit_profile.setVisibility(View.GONE);
                            getShopImagesAdapter.selectAll();
                            dialogManager.hideProgress();
                        }
                    },1000);
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        add_shop_images_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                dialogManager.showProgress(getActivity());

                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEnableFalse();
                edit_profile.setVisibility(View.VISIBLE);
                getShopImagesAdapter.unselectall();

                buttonState=0;
            }
        });

        edit_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!address_from_map.getText().toString().equalsIgnoreCase("")) {
                    AppConstants.CHECK_ADD_ADDRESS = "ADD_ADDRESS_EDIT";
                }
                startActivity(new Intent(getActivity(), MapsActivity.class));
            }
        });


        save_shop_detials_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (shop_name_english.getText().toString().isEmpty() || shop_name_arabic.getText().toString().isEmpty() ||
                        shop_address_english.getText().toString().isEmpty() || shop_address_arabic.getText().toString().isEmpty()
                        || countries_text.getText().toString().isEmpty()
                        || state_text.getText().toString().isEmpty() || address_from_map.getText().toString().isEmpty()) {


                    Toast.makeText(getActivity(), R.string.enter_all_feilds, Toast.LENGTH_LONG).show();
                } else if (condition == 0) {
//                    Toast.makeText(getActivity(), R.string.add_shop_image, Toast.LENGTH_LONG).show();
                    updateShopProfile(mAddImageList);
                } else {

                    uploadProfileImageApiCall();
                }

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        dialogManager.hideProgress();
                    }
                }, 3000);


            }
        });


//        save_shop_detials_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (shop_name_english.getText().toString().isEmpty() || shop_name_arabic.getText().toString().isEmpty() ||
//                        shop_address_english.getText().toString().isEmpty() || shop_address_arabic.getText().toString().isEmpty()
//                        || countries_text.getText().toString().isEmpty()
//                        || state_text.getText().toString().isEmpty() || address_from_map.getText().toString().isEmpty()) {
//                    Toast.makeText(getActivity(), "Enter All The Feilds", Toast.LENGTH_LONG).show();
//                } else if (mAddImageList.size() <= 0) {
//                    Toast.makeText(getActivity(), "Add Shop Image", Toast.LENGTH_LONG).show();
//                } else {
//
//                    uploadProfileImageApiCall();
//                }
//
//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        //Do something after 100ms
//                        progressDialog.dismiss();
//                    }
//                }, 3000);
//
//
//            }
//        });

        return rootView;
    }

    public void updateShopProfile(ArrayList<String> mImageResultArray) {
        if (new UtilService().isNetworkAvailable(getContext())) {

            dialogManager.showProgress(getActivity());
            try {


                ArrayList<ShopImagesEntity> shopImagesEntity = new ArrayList<>();
                for (int i = 0; i < mImageResultArray.size(); i++) {
                    ShopImagesEntity ShopImagesEntites = new ShopImagesEntity();

                    ShopImagesEntites.setShopProfileId(String.valueOf(i));
                    ShopImagesEntites.setImage(mImageResultArray.get(i));
                    shopImagesEntity.add(ShopImagesEntites);
                }


//                HashMap<String, String> map = new HashMap<>();
//                map.put("TailorId", sharedPreferencesTailorId.getString("TailorId", ""));
//                if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {
//                    map.put("ShopProfileId", "");
//                } else {
//                    map.put("ShopProfileId", sharedPreferencesShopId.getString("ShopId", ""));
//                }
//
//                map.put("ShopNameInEnglish", shop_name_english.getText().toString());
//                map.put("ShopNameInArabic", shop_name_english.getText().toString());
//                map.put("CountryId", "1");
//                map.put("CityId", "2");
//                map.put("Latitude", sharedPreferences.getString("latitude", ""));
//                map.put("Longitude", sharedPreferences.getString("longitude", ""));
//                map.put("AddressInEnglish", shop_address_english.getText().toString());
//                map.put("AddressinArabic", shop_address_arabic.getText().toString());
//                for (int i=0;i<mImageResultArray.size(); i++){
//                    String name = "ShopImages["+i+"][ShopProfileId]";
//                    map.put(name, String.valueOf(i));
//                    String image = "ShopImages["+i+"][Image]";
//                    map.put(image, mImageResultArray.get(i));
//                }
                InsertUpdateShopResponse mInsertUpdateShopResponse = new InsertUpdateShopResponse();
                mInsertUpdateShopResponse.setTailorId(sharedPreferencesTailorId.getString("TailorId", ""));
                if (sharedPreferencesShopId.getString("ShopId", "").isEmpty()) {
                    mInsertUpdateShopResponse.setShopProfileId("");
                } else {
                    mInsertUpdateShopResponse.setShopProfileId(sharedPreferencesShopId.getString("ShopId", ""));
                }
                mInsertUpdateShopResponse.setShopNameInEnglish(shop_name_english.getText().toString());
                mInsertUpdateShopResponse.setShopNameInArabic(shop_name_arabic.getText().toString());
                mInsertUpdateShopResponse.setCountryId(AppConstants.COUNTRY_ID);
                mInsertUpdateShopResponse.setCityId(AppConstants.STATE_ID_SHOP);
                mInsertUpdateShopResponse.setLatitude(Double.parseDouble(sharedPreferences.getString("latitude", "")));
                mInsertUpdateShopResponse.setLongitude(Double.parseDouble(sharedPreferences.getString("longitude", "")));
                mInsertUpdateShopResponse.setAddressInEnglish(shop_address_english.getText().toString());
                mInsertUpdateShopResponse.setAddressinArabic(shop_address_arabic.getText().toString());
                mInsertUpdateShopResponse.setShopImages(shopImagesEntity);

                restService.shopdetailsUpload1(mInsertUpdateShopResponse).enqueue(new Callback<SignUpResponse>() {

                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                        signUp = response.body();
                        if (response.isSuccessful()) {

                            if (signUp != null && signUp
                                    .getResponseMsg().trim().equalsIgnoreCase("Success")) {

                                Toast.makeText(getContext(), R.string.profile_created_sucessfully, Toast.LENGTH_SHORT).show();

                                editor.putString("ShopId", response.body().getResult());
                                editor.commit();

                                startActivity(new Intent(getActivity(), HomeActivity.class));
                            } else if (signUp != null) {
                                Toast.makeText(getContext(), signUp.getResponseMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {

                        Toast.makeText(getContext(), "Connection Failure, try again!" + t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadImages() {
        AppConstants.ADD_MATERIAL = "ADD_MATERIAL";
        DialogManager.getInstance().showImageUploadPopup(getActivity(), getActivity().getString(R.string.select_phot_type),
                getActivity().getString(R.string.take_camera),
                getActivity().getString(R.string.open_gallery), new InterfaceTwoBtnCallBack() {
                    @Override
                    public void onNegativeClick() {
                        captureImage();
                    }

                    @Override
                    public void onPositiveClick() {
                        galleryImage();
                    }
                });
    }


    /*open camera Image*/
    private void captureImage() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    /*open gallery Image*/
    private void galleryImage() {

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);


    }

    @Override
    public void onRequestSuccess(Object resObj) {
        super.onRequestSuccess(resObj);
        if (resObj instanceof FileUploadResponse) {
            FileUploadResponse mResponse = (FileUploadResponse) resObj;
            if (mResponse.getResponseMsg().equalsIgnoreCase("Success")) {
                ArrayList<String> mResponseArrayList = new ArrayList<>();
                for (int i = 0; i < mResponse.getResult().size(); i++) {
                    String[] parts = mResponse.getResult().get(i).split("\\\\");
                    String part1 = parts[0]; // 004
                    String part2 = parts[1];
                    String lastOne = parts[parts.length - 1];

                    mResponseArrayList.add(lastOne);

                }

                updateShopProfile(mResponseArrayList);
//                condition = 1;
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            /*Open Camera Request Check*/
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {

                    try {

                        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                        String path = saveImage(thumbnail);
                        IMAGE_PATH = path;
                        IMAGE_PATH = path;
                        saveImage(thumbnail);

                        mAddImageList.add(IMAGE_PATH);
                        GetShopImagesModal mlist = new GetShopImagesModal(0, String.valueOf(IMAGE_PATH));
                        getShopImagesModals.add(mlist);
                        setGenderList(getShopImagesModals);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                getShopImagesAdapter.selectAll();
                            }
                        }, 1000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/
                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }
                break;
            /*Open Photo Gallery Request Check*/
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    // successfully captured the image
                    // display it in image view

                    Uri contentURI = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                        String path = saveImage(bitmap);
                        IMAGE_PATH = path;

                        mAddImageList.add(IMAGE_PATH);

                        setAdapter(mAddImageList);

                        GetShopImagesModal mlist = new GetShopImagesModal(0, IMAGE_PATH);
                        getShopImagesModals.add(mlist);
                        setGenderList(getShopImagesModals);
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                getShopImagesAdapter.selectAll();
                            }
                        }, 1000);

//                        Toast.makeText(getActivity(), "camera", Toast.LENGTH_LONG).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "I Can't find the location this file!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    if (resultCode == RESULT_CANCELED) {
                        /*Cancelling the image capture process by the user*/

                    } else {
                        /*image capture getting failed due to certail technical issues*/
                    }
                }


                break;


        }
    }

    public static void deleteShopImgApi(String Id) {
        if (new UtilService().isNetworkAvailable(context)) {

            try {
                mrestService.deleteShopProfileImg(Id).enqueue(new Callback<DeleteShopImageResponse>() {
                    @Override
                    public void onResponse(Call<DeleteShopImageResponse> call, Response<DeleteShopImageResponse> response) {

                        if (response.isSuccessful()) {

                            try {

                            } catch (Exception e) {

                            }
                        } else if (response.errorBody() != null) {
                            Toast.makeText(context, response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteShopImageResponse> call, Throwable t) {
                        Toast.makeText(context, "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                Toast.makeText(context, "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            if (isStoragePermissionGranted() == true) {
                wallpaperDirectory.mkdirs();

            }
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getActivity(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    private boolean checkPermission() {

        boolean addPermission = true;
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
            int readStoragePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
            int storagePermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            if (storagePermission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }


        return addPermission;

    }

    /*Set Adapter for the Recycler view*/
    public void setAdapter(ArrayList<String> addMaterialList) {

//        if (mAddMaterialAdapter == null) {

        mAddMaterialAdapter = new AddMaterialAdapter(getActivity(), addMaterialList);
        add_shop_images_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        add_shop_images_recyclerview.setAdapter(mAddMaterialAdapter);


//        }
//        else {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    mAddMaterialAdapter.notifyDataSetChanged();
//                }
//            });
//        }

    }

    private void setCountrySelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < countries.size(); i++) {
            listSpinner.add(countries.get(i).getCountryName());
        }


//        textCountrySelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showCoutrySelectionList();
//            }
//        });
        country_imgSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                textCountrySelection.performClick();
                AppConstants.COUNRTY_CODE_ADAPTER = "SHOP_DETAILS";

                showCoutrySelectionList();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 100ms
                        getStateList();
                        dialogManager.hideProgress();
                        state_text.setText("");
                    }
                }, 3000);

            }
        });


//        state_imgSpinner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                textCountrySelection.performClick();
//                AppConstants.COUNRTY_CODE_ADAPTER = "SHOP_DETAILS";
//                getStateList();
//                showStateSelectionList();
//            }
//        });
    }


    private void setStateSelectionList() {
        List<String> listSpinner = new ArrayList<String>();
        for (int i = 0; i < states.size(); i++) {
            listSpinner.add(states.get(i).getStateName());
        }


//        textCountrySelection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showCoutrySelectionList();
//            }
//        });
//        country_imgSpinner.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                textCountrySelection.performClick();
//                AppConstants.COUNRTY_CODE_ADAPTER = "SHOP_DETAILS";
//
//                showCoutrySelectionList();
//            }
//        });


        state_imgSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                textCountrySelection.performClick();
                AppConstants.COUNRTY_CODE_ADAPTER = "SHOP_DETAILS";

                //Do something after 100ms
                showStateSelectionList();


            }
        });
    }

    public void getShopProfile() {
        if (new UtilService().isNetworkAvailable(getContext())) {

            try {
                restService.getShopProfile(sharedPreferencesTailorId.getString("TailorId", "0")).enqueue(new Callback<GetShopProfileResponse>() {
                    @Override
                    public void onResponse(Call<GetShopProfileResponse> call, Response<GetShopProfileResponse> response) {

                        if (response.isSuccessful()) {

                            try {
                                if (getShopProfileModals == null) {
                                    getShopProfileModals = new ArrayList<>();
                                }
                                if (getShopImagesModals == null) {
                                    getShopImagesModals = new ArrayList<>();

                                } else {
                                    getShopImagesModals = response.body().getResult().getShopImages();

                                    for (int i = 0; i < getShopImagesModals.size(); i++) {
                                        mAddImageList.add(getShopImagesModals.get(i).getImage());
                                    }
                                }

                                getShopImagesModals = response.body().getResult().getShopImages();
                                setGenderList(getShopImagesModals);
//                            setAdapter(getShopImagesModals.get(0).getShopProfileId());
                                getShopProfileModals = response.body().getResult().getGetShopProfiles();
                                shop_name_english.setText(getShopProfileModals.get(0).getShopNameInEnglish());
                                shop_name_arabic.setText(getShopProfileModals.get(0).getShopNameInArabic());
                                shop_address_english.setText(getShopProfileModals.get(0).getAddressInEnglish());
                                shop_address_arabic.setText(getShopProfileModals.get(0).getAddressinArabic());
                                countries_text.setText(getShopProfileModals.get(0).getCountryName());
                                state_text.setText(getShopProfileModals.get(0).getStateName());
                                AppConstants.COUNTRY_ID = getShopProfileModals.get(0).getCountryId();
                                AppConstants.STATE_ID_SHOP = getShopProfileModals.get(0).getCityId();
                                getShopProfileModals.get(0).getLatitude();
                                getShopProfileModals.get(0).getLongitude();
                                editorLoc.putString("latitude", String.valueOf(getShopProfileModals.get(0).getLatitude()));
                                editorLoc.putString("longitude", String.valueOf(getShopProfileModals.get(0).getLongitude()));
                                editorLoc.commit();

                                getAddress();

                                AppConstants.SELECTED_LAT_STR = String.valueOf(getShopProfileModals.get(0).getLatitude());
                                AppConstants.SELECTED_LONG_STR = String.valueOf(getShopProfileModals.get(0).getLongitude());

                                AppConstants.LAT_STR = String.valueOf(getShopProfileModals.get(0).getLatitude());
                                AppConstants.LONG_STR = String.valueOf(getShopProfileModals.get(0).getLongitude());

                                mapsActivity.getLoc();


                            } catch (Exception e) {

                            }
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<GetShopProfileResponse> call, Throwable t) {
                        Toast.makeText(getContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                Toast.makeText(getContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadProfileImageApiCall() {

        if (NetworkUtil.isNetworkAvailable(getContext())) {
            if (mAddImageList.size() > 0) {

                APIRequestHandler.getInstance().updateShopImg(mAddImageList, ShopDetailsFrag.this, "ShopImages");

            }
        } else {
            uploadProfileImageApiCall();
        }
    }

    private void uploadMultiFile() {
        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[mAddImageList.size()];

        for (int index = 0; index < mAddImageList.size(); index++) {

            mUserImageFile = new File(mAddImageList.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);
            multipartTypedOutput[index] = MultipartBody.Part.createFormData("ShopImages" + "[" + index + "]", mUserImageFile.getName(), surveyBody);

        }
        restService.upImageManys(multipartTypedOutput).enqueue(new Callback<ImageResponse>() {

            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getActivity(), response.body().getResult().get(0), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {

                Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG).show();

            }
        });

    }

    private void setGenderList(List<GetShopImagesModal> getMeasurementDetailsModals) {

        getShopImagesAdapter = new GetShopImagesAdapter(getActivity(), getMeasurementDetailsModals, ShopDetailsFrag.this);
        add_shop_images_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        add_shop_images_recyclerview.setAdapter(getShopImagesAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getShopImagesAdapter != null) {
            getShopImagesAdapter.notifyDataSetChanged();
        }
    }

    private void getCountryList() {
        if (new UtilService().isNetworkAvailable(getContext())) {

            try {
                restService.getCountry().enqueue(new Callback<CountryResponse>() {
                    @Override
                    public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {

                        if (response.isSuccessful()) {
                            countries = response.body().getResult();
                            if (countries == null) {
                                countries = new ArrayList<>();
                            }
                            setCountrySelectionList();
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CountryResponse> call, Throwable t) {
                        Toast.makeText(getContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                Toast.makeText(getContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    public void getStateList() {
        if (new UtilService().isNetworkAvailable(getContext())) {

            try {
                HashMap<String, String> map = new HashMap<>();
                map.put("Id", AppConstants.COUNTRY_ID);
                restService.stateList(map).enqueue(new Callback<StateResponse>() {
                    @Override
                    public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {

                        if (response.isSuccessful()) {
                            states = response.body().getResult();
                            dialogManager.hideProgress();
                            if (states == null) {
                                states = new ArrayList<>();
                            }
                            setStateSelectionList();
                        } else if (response.errorBody() != null) {
                            Toast.makeText(getContext(), response.body().getResponseMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<StateResponse> call, Throwable t) {
                        Toast.makeText(getContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
                    }
                });

            } catch (Exception e) {
                Toast.makeText(getContext(), "Connection Failure, try again!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void showCoutrySelectionList() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getContext());
        countryList.setLayoutManager(linearLayoutManager);

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle("SELECT COUNTRY")
                .setView(view)
                .create();
        countryCodeDialogAdapter = new CountryCodeAdapter(getContext(), countries, country, dialog);
        countryList.setAdapter(countryCodeDialogAdapter);
        dialog.show();

    }


    public void showStateSelectionList() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.country_list, null);
        countryList = view.findViewById(R.id.countryList);
        countryList.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getContext());
        countryList.setLayoutManager(linearLayoutManager);

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle("SELECT STATE")
                .setView(view)
                .create();
        stateAdapter = new StateAdapter(getContext(), states, state, dialog);
        countryList.setAdapter(stateAdapter);
        dialog.show();

    }

    public void onUserTappedCountry(Country country) {
        if (rememberLastSelection) {
            storeSelectedCountryNameCode(country.getPhoneCode());
        }
    }

    static void storeSelectedCountryNameCode(String selectedCountryNameCode) {
        //get the shared pref
        SharedPreferences sharedPref = context.getSharedPreferences(CCP_PREF_FILE, Context.MODE_PRIVATE);

        //we want to write in shared pref, so lets get editor for it
        SharedPreferences.Editor editor = sharedPref.edit();

        // add our last selection country name code in pref
        editor.putString(selectionMemoryTag, selectedCountryNameCode);

        //finally save it...
        editor.apply();
    }


    public void onUserTappedState(State country) {
        if (rememberLastSelection) {
            storeSelectedStateNameCode(country.getStateName());
        }
    }

    static void storeSelectedStateNameCode(String selectedCountryNameCode) {
        //get the shared pref
        SharedPreferences sharedPref = context.getSharedPreferences(CCP_PREF_FILE, Context.MODE_PRIVATE);

        //we want to write in shared pref, so lets get editor for it
        SharedPreferences.Editor editor = sharedPref.edit();

        // add our last selection country name code in pref
        editor.putString(selectionMemoryTag, selectedCountryNameCode);

        //finally save it...
        editor.apply();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setupUI(View view) {
        try {
            if (!(view instanceof EditText)) {
                view.setOnTouchListener(new View.OnTouchListener() {
                    @SuppressLint("ClickableViewAccessibility")
                    public boolean onTouch(View v, MotionEvent event) {
                        hideSoftKeyboard(getActivity());
                        return false;

                    }
                });
            }

            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    setupUI(innerView);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getAddress() throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        addresses = geocoder.getFromLocation(Double.valueOf(getShopProfileModals.get(0).getLatitude()), Double.valueOf(getShopProfileModals.get(0).getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
//        Toast.makeText(getActivity(), city, Toast.LENGTH_LONG).show();
        address_from_map.setText(address);
    }


    public void setEnableTrue() {

        location.setEnabled(true);
        set_from_map.setEnabled(true);
        address_from_map.setEnabled(true);
        shop_name_english.setEnabled(true);
        shop_name_arabic.setEnabled(true);
        edit_location.setEnabled(true);
        save_shop_detials_btn.setEnabled(true);
        add_more_photos.setEnabled(true);
//        flagsImg.setEnabled(true);
        country_imgSpinner.setEnabled(true);
        state_imgSpinner.setEnabled(true);
        userImage.setEnabled(true);
        set_from_map_layout.setEnabled(true);
        country_imgSpinner.setEnabled(true);
        shop_address_english.setEnabled(true);
        shop_address_arabic.setEnabled(true);
    }


    public void setEnableFalse() {
        location.setEnabled(false);
        set_from_map.setEnabled(false);
        address_from_map.setEnabled(false);
        shop_name_english.setEnabled(false);
        shop_name_arabic.setEnabled(false);
        edit_location.setEnabled(false);
        save_shop_detials_btn.setEnabled(false);
        add_more_photos.setEnabled(false);
//        flagsImg.setEnabled(false);
        country_imgSpinner.setEnabled(false);
        state_imgSpinner.setEnabled(false);
        userImage.setEnabled(false);
        set_from_map_layout.setEnabled(false);
        country_imgSpinner.setEnabled(false);
        shop_address_english.setEnabled(false);
        shop_address_arabic.setEnabled(false);
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout_lay), ViewCompat.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.location_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
//  ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }
}
