package com.qoltech.mzyoontailor.fragments;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.adapter.BuyerQuotationPendingAdapter;
import com.qoltech.mzyoontailor.entity.GetQuotaionPendingEntity;
import com.qoltech.mzyoontailor.modal.GetOrderQuotationPendingModal;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.MainApplication;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class QuotationPendingListFragment extends Fragment {
//? use this for adapter
//    order_quotation_buyer_approved_reject_list.xml

    RecyclerView appointment_list_recycler_view;
    TextView no_result_found;
    ApiService restService;
    SharedPreferences sharedPreferences;
    List<GetQuotaionPendingEntity> getQuotaionPendingEntities;
    BuyerQuotationPendingAdapter buyerQuotationPendingAdapter;

    public QuotationPendingListFragment() {
        // Required empty public constructor
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_pending, container, false);
        appointment_list_recycler_view = rootView.findViewById(R.id.appointment_list_recycler_view);
        no_result_found = rootView.findViewById(R.id.no_result_found);
        restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        sharedPreferences = getActivity().getSharedPreferences("TailorId", MODE_PRIVATE);

        getPendingOrders();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
            }
        }, 1000);

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    public void getPendingOrders() {
        restService.getOrderQuotationPending(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<GetOrderQuotationPendingModal>() {
            @Override
            public void onResponse(Call<GetOrderQuotationPendingModal> call, Response<GetOrderQuotationPendingModal> response) {


                getQuotaionPendingEntities = response.body().getResult();

                if (getQuotaionPendingEntities.size() > 0) {
                    setList();
                }
            }

            @Override
            public void onFailure(Call<GetOrderQuotationPendingModal> call, Throwable t) {

            }
        });
    }
    public void getRejectOrders() {
        restService.getOrderQuotationPending(sharedPreferences.getString("TailorId", "")).enqueue(new Callback<GetOrderQuotationPendingModal>() {
            @Override
            public void onResponse(Call<GetOrderQuotationPendingModal> call, Response<GetOrderQuotationPendingModal> response) {


                getQuotaionPendingEntities = response.body().getResult();

                if (getQuotaionPendingEntities.size() > 0) {
                    setList();
                }
            }

            @Override
            public void onFailure(Call<GetOrderQuotationPendingModal> call, Throwable t) {

            }
        });
    }


    private void setList() {
        appointment_list_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        buyerQuotationPendingAdapter = new BuyerQuotationPendingAdapter(getActivity(), getQuotaionPendingEntities, restService);
        appointment_list_recycler_view.setAdapter(buyerQuotationPendingAdapter);
    }
}

