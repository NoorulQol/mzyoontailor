package com.qoltech.mzyoontailor.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.qoltech.mzyoontailor.R;
import com.qoltech.mzyoontailor.activity.HomeActivity;
import com.qoltech.mzyoontailor.activity.OrderConfirmationActivity;
import com.qoltech.mzyoontailor.adapter.GetCustomizationRequestAdapter;
import com.qoltech.mzyoontailor.entity.UserDetailsEntity;
import com.qoltech.mzyoontailor.util.ApiService;
import com.qoltech.mzyoontailor.util.GlobalData;
import com.qoltech.mzyoontailor.util.MainApplication;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.PreferenceUtil;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestCustomization;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResponse;
import com.qoltech.mzyoontailor.wrappers.GetBuyerRequestResult;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class Cusumization_request_fragment extends Fragment {

    public static Handler.Callback callback;
    ImageView get_buyer_request_season_image, get_buyer_request_industry_image,
            get_buyer_request_brand_image,
            get_buyer_request_materialtype_image,
            get_buyer_request_color_image, get_buyer_request_pattern_image;
    TextView get_buyer_request_season_text, get_buyer_request_industry_text,
            get_buyer_request_brand_text,
            get_buyer_request_materialtype_text, material_details_txt;
    Button approve_request, reject_request;
    GetCustomizationRequestAdapter getCustomizationRequestAdapter;
    RecyclerView get_buyer_request_customization_recyclerView;

    ApiService restService;
    GetBuyerRequestResult getBuyerRequestResult = new GetBuyerRequestResult();
    List<GetBuyerRequestCustomization> getBuyerRequestCustomizations;
    SharedPreferences sharedPreferences, sharedPreferences1;
    FrameLayout material_details_layout;
    Dialog mAppointmentApprove;
    View rootView;
    private UserDetailsEntity mUserDetailsEntityRes;
    LinearLayout bgLayout;
    Gson gson;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        rootView = inflater.inflate(R.layout.fragment_custumization_request, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            restService = ((MainApplication) Objects.requireNonNull(getActivity()).getApplication()).getClient();
        }
//        get_buyer_request_season_image = (ImageView) rootView.findViewById(R.id.get_buyer_request_season_image);
        get_buyer_request_industry_image = (ImageView) rootView.findViewById(R.id.get_buyer_request_industry_image);
        get_buyer_request_brand_image = (ImageView) rootView.findViewById(R.id.get_buyer_request_brand_image);
        get_buyer_request_materialtype_image = (ImageView) rootView.findViewById(R.id.get_buyer_request_materialtype_image);
//        get_buyer_request_color_image = (ImageView) rootView.findViewById(R.id.get_buyer_request_color_image);
//        get_buyer_request_pattern_image = (ImageView) rootView.findViewById(R.id.get_buyer_request_pattern_image);

//        get_buyer_request_season_text = (TextView) rootView.findViewById(R.id.get_buyer_request_season_text);
        get_buyer_request_industry_text = (TextView) rootView.findViewById(R.id.get_buyer_request_industry_text);
        get_buyer_request_brand_text = (TextView) rootView.findViewById(R.id.get_buyer_request_brand_text);
        get_buyer_request_materialtype_text = (TextView) rootView.findViewById(R.id.get_buyer_request_materialtype_text);
//        get_buyer_request_color_text = (TextView) rootView.findViewById(R.id.get_buyer_request_color_text);
//        get_buyer_request_pattern_text = (TextView) rootView.findViewById(R.id.get_buyer_request_pattern_text);
        get_buyer_request_customization_recyclerView = rootView.findViewById(R.id.get_buyer_request_customization_recyclerView);
        approve_request = rootView.findViewById(R.id.approve_request);
        material_details_layout = (FrameLayout) rootView.findViewById(R.id.material_details_layout);
//
        sharedPreferences = getActivity().getSharedPreferences("TailorId", Context.MODE_PRIVATE);
        sharedPreferences1 = getActivity().getSharedPreferences("OrderId", MODE_PRIVATE);
        material_details_txt = (TextView) rootView.findViewById(R.id.material_details_txt);
        reject_request = (Button) rootView.findViewById(R.id.reject_request);
        bgLayout = rootView.findViewById(R.id.bgLayout);
        gson = new Gson();
        String json = PreferenceUtil.getStringValue(getActivity(), AppConstants.USER_DETAILS);
        mUserDetailsEntityRes = gson.fromJson(json, UserDetailsEntity.class);
        getLanguage();
        approve_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDismiss(mAppointmentApprove);
                mAppointmentApprove = getDialog(getActivity(), R.layout.pop_up_cutomization_request);
                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                Window window = mAppointmentApprove.getWindow();
                if (window != null) {
                    LayoutParams.copyFrom(window.getAttributes());
                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(LayoutParams);
                    window.setGravity(Gravity.CENTER);
                }
                RelativeLayout okLay, recheduleLay;
                okLay = mAppointmentApprove.findViewById(R.id.appointment_reschedule_ok_lay);
                recheduleLay = mAppointmentApprove.findViewById(R.id.appointment_reschedule_check_lay);

                okLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAppointmentApprove.dismiss();
//                        startActivity(new Intent(getActivity(), OrderConfirmation.class));

                        startActivity(new Intent(getActivity(), OrderConfirmationActivity.class));

                    }
                });

                recheduleLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(getActivity(), "We will reschedule the date and get you soon!", Toast.LENGTH_SHORT).show();
                        mAppointmentApprove.dismiss();
                    }
                });

                alertShowing(mAppointmentApprove);


            }
        });


        reject_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDismiss(mAppointmentApprove);
                mAppointmentApprove = getDialog(getActivity(), R.layout.pop_up_cutomization_reject);
                WindowManager.LayoutParams LayoutParams = new WindowManager.LayoutParams();
                Window window = mAppointmentApprove.getWindow();
                if (window != null) {
                    LayoutParams.copyFrom(window.getAttributes());
                    LayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                    LayoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(LayoutParams);
                    window.setGravity(Gravity.CENTER);
                }
                RelativeLayout okLay, recheduleLay;
                okLay = mAppointmentApprove.findViewById(R.id.appointment_reschedule_ok_lay);
                recheduleLay = mAppointmentApprove.findViewById(R.id.appointment_reschedule_check_lay);

                okLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isApproveOrder();


                        mAppointmentApprove.dismiss();

                    }
                });

                recheduleLay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mAppointmentApprove.dismiss();
                    }
                });

                alertShowing(mAppointmentApprove);


            }
        });
        getBuyerRequestCustomizations = new ArrayList<>();
        getRequestCustomization();
        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Menu 1");
    }


    public void getRequestCustomization() {

        restService.getCustomizationType(AppConstants.ORDER_ID).enqueue(new Callback<GetBuyerRequestResponse>() {
            @Override
            public void onResponse(Call<GetBuyerRequestResponse> call, Response<GetBuyerRequestResponse> response) {


                try {
                    getBuyerRequestCustomizations = response.body().getResult().getCustomization();

                    setGenderList(getBuyerRequestCustomizations);
                    if (getBuyerRequestCustomizations != null) {

                        Glide.with(getActivity()).load(GlobalData.SERVER_URL + "Images/PlaceOfIndustry/" + response.body().getResult().getPlaceofIndustry().get(0).getImage())
                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_industry_image);
                        Glide.with(getActivity()).load(GlobalData.SERVER_URL + "/Images/Brands/" + response.body().getResult().getMaterialBrand().get(0).getImage())
                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_brand_image);
                        Glide.with(getActivity()).load(GlobalData.SERVER_URL + "/Images/Material/" + response.body().getResult().getMaterialType().get(0).getImage())
                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_materialtype_image);

                        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {


                            get_buyer_request_industry_text.setText(response.body().getResult().getPlaceofIndustry().get(0).getPlaceInArabic());
                            get_buyer_request_brand_text.setText(response.body().getResult().getMaterialBrand().get(0).getBrandInArabic());
                            get_buyer_request_materialtype_text.setText(response.body().getResult().getMaterialType().get(0).getMaterialInArabic());

                        } else {
//                            holder.mGridLeftViewTxt.setText(genderEntity.getGender());
//                            holder.mGridRightViewTxt.setText(genderEntity.getGender());

                            get_buyer_request_industry_text.setText(response.body().getResult().getPlaceofIndustry().get(0).getPlaceInEnglish());
                            get_buyer_request_brand_text.setText(response.body().getResult().getMaterialBrand().get(0).getBrandInEnglish());
                            get_buyer_request_materialtype_text.setText(response.body().getResult().getMaterialType().get(0).getMaterialInEnglish());


                        }

//                        get_buyer_request_industry_text.setText(response.body().getResult().getPlaceofIndustry().get(0).getPlaceInEnglish());
//                        Glide.with(getActivity()).load(GlobalData.SERVER_URL + "Images/PlaceOfIndustry/" + response.body().getResult().getPlaceofIndustry().get(0).getImage())
//                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_industry_image);
//                        get_buyer_request_brand_text.setText(response.body().getResult().getMaterialBrand().get(0).getBrandInEnglish());
//                        Glide.with(getActivity()).load(GlobalData.SERVER_URL + "/Images/Brands/" + response.body().getResult().getMaterialBrand().get(0).getImage())
//                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_brand_image);
//                        get_buyer_request_materialtype_text.setText(response.body().getResult().getMaterialType().get(0).getMaterialInEnglish());
//                        Glide.with(getActivity()).load(GlobalData.SERVER_URL + "/Images/Material/" + response.body().getResult().getMaterialType().get(0).getImage())
//                                .apply(new RequestOptions().fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL)
//                                        .placeholder(R.drawable.placeholder)).into(get_buyer_request_materialtype_image);
                    } else {


                    }


                } catch (Exception e) {
                    material_details_layout.setVisibility(View.GONE);
                    material_details_txt.setVisibility(View.GONE);
                }


            }

            @Override
            public void onFailure(Call<GetBuyerRequestResponse> call, Throwable t) {

            }
        });

    }


    private void setGenderList(List<GetBuyerRequestCustomization> getBuyerRequestCustomizations) {

        getCustomizationRequestAdapter = new GetCustomizationRequestAdapter(getActivity(), getBuyerRequestCustomizations);
        get_buyer_request_customization_recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        get_buyer_request_customization_recyclerView.setAdapter(getCustomizationRequestAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getCustomizationRequestAdapter != null) {
            getCustomizationRequestAdapter.notifyDataSetChanged();
        }
    }

    public void alertShowing(Dialog dialog) {
        /*To check if the dialog is null or not. if it'border_with_transparent_bg not a null, the dialog will be shown orelse it will not get appeared*/
        if (dialog != null) {
            try {
                dialog.show();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }
    }

    public void alertDismiss(Dialog dialog) {
        /*To check if the dialog is shown, if the dialog is shown it will be cancelled */
        if (dialog != null && dialog.isShowing()) {
            try {
                dialog.dismiss();
            } catch (Exception e) {
                Log.e(AppConstants.TAG, e.getMessage());
            }
        }

    }

    /*Default dialog init method*/
    public Dialog getDialog(Context context, int layout) {

        Dialog mCommonDialog;
        mCommonDialog = new Dialog(context);
        mCommonDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (mCommonDialog.getWindow() != null) {
            mCommonDialog.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            mCommonDialog.setContentView(layout);
            mCommonDialog.getWindow().setGravity(Gravity.CENTER);
            mCommonDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
        }
        mCommonDialog.setCancelable(false);
        mCommonDialog.setCanceledOnTouchOutside(false);

        return mCommonDialog;
    }

    public void isApproveOrder() {

        HashMap<String, String> map = new HashMap<>();
        map.put("TailorId", sharedPreferences.getString("TailorId", ""));
        map.put("OrderId", AppConstants.ORDER_ID);
        map.put("IsApproved", "2");

        restService.approveOrder(map).enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                Toast.makeText(getActivity(), R.string.order_is_rejected, Toast.LENGTH_SHORT).show();

                getActivity().finish();
                startActivity(new Intent(getActivity(), HomeActivity.class));
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {

            }
        });
    }

    public void getLanguage() {

        if (mUserDetailsEntityRes.getLanguage().equalsIgnoreCase("Arabic")) {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.userNameNewCustomer), ViewCompat.LAYOUT_DIRECTION_RTL);
//            ViewCompat.setLayoutDirection(findViewById(R.id.name_layout), ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(rootView.findViewById(R.id.bgLayout), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.country_code_lay), ViewCompat.LAYOUT_DIRECTION_LTR);
//            ViewCompat.setLayoutDirection(findViewById(R.id.phone_number), ViewCompat.LAYOUT_DIRECTION_LTR);


        }
    }
}
