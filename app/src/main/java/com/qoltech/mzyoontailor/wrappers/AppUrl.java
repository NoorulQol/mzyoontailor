package com.qoltech.mzyoontailor.wrappers;

/**
 * Created by apple on 23/03/18.
 */

public class AppUrl {
    private String android;
    private String ios;

    public String getAndroid() {
        return android;
    }

    public void setAndroid(String android) {
        this.android = android;
    }

    public String getIos() {
        return ios;
    }

    public void setIos(String ios) {
        this.ios = ios;
    }
}
