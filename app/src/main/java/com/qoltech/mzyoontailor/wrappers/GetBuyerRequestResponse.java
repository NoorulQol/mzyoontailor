package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBuyerRequestResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private GetBuyerRequestResult result;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public GetBuyerRequestResult getResult() {
        return result == null ? new GetBuyerRequestResult() : result;
    }

    public void setResult(GetBuyerRequestResult result) {
        this.result = result;
    }

}