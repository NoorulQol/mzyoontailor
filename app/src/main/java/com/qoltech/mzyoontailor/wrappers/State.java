package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class State {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CountryId")
    @Expose
    private Integer countryId;
    @SerializedName("StateName")
    @Expose
    private String stateName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getStateName() {
        return stateName==null?"":stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

}
