package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetRequestMeasurementPartsModal {
    @SerializedName("MeasurementParts")
    @Expose
    private String measurementParts;
    @SerializedName("MeasurementPartsInArabic")
    @Expose
    private String measurementPartsInArabic;
    @SerializedName("Image")
    @Expose
    private String image;

    public String getMeasurementParts() {
        return measurementParts;
    }

    public void setMeasurementParts(String measurementParts) {
        this.measurementParts = measurementParts;
    }

    public String getMeasurementPartsInArabic() {
        return measurementPartsInArabic;
    }

    public void setMeasurementPartsInArabic(String measurementPartsInArabic) {
        this.measurementPartsInArabic = measurementPartsInArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}