package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOrderDetailsResult {
    @SerializedName("Status")
    @Expose
    private List<GetOrderStatusModal> status = null;

    @SerializedName("Shipping_Charges")
    @Expose
    private List<GetShippingCharges> shippingCharges = null;
    @SerializedName("OrderDetail")
    @Expose
    private List<GetOrderDetailsModal> orderDetail = null;
    @SerializedName("ProductPrice")
    @Expose
    private List<GetOrderProductPriceModal> productPrice = null;
    @SerializedName("BuyerAddress")
    @Expose
    private List<GetBuyerAddressModal> buyerAddress = null;

    public List<GetOrderDetailsModal> getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(List<GetOrderDetailsModal> orderDetail) {
        this.orderDetail = orderDetail;
    }

    public List<GetOrderProductPriceModal> getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(List<GetOrderProductPriceModal> productPrice) {
        this.productPrice = productPrice;
    }

    public List<GetBuyerAddressModal> getBuyerAddress() {
        return buyerAddress;
    }

    public void setBuyerAddress(List<GetBuyerAddressModal> buyerAddress) {
        this.buyerAddress = buyerAddress;
    }

    public List<GetOrderStatusModal> getStatus() {
        return status;
    }

    public void setStatus(List<GetOrderStatusModal> status) {
        this.status = status;
    }

    public List<GetShippingCharges> getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(List<GetShippingCharges> shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    @Expose
    private Float stichingAndMaterialCharge;
    @SerializedName("MeasurementCharges")
    @Expose
    private Float measurementCharges;
    @SerializedName("UrgentStichingCharges")
    @Expose
    private Float urgentStichingCharges;
    @SerializedName("DeliveryCharge")
    @Expose
    private Float deliveryCharge;
    @SerializedName("MaterialDeliveryCharges")
    @Expose
    private Float materialDeliveryCharges;
}
