package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyerRequestApprovedModal {

    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("profilePicture")
    @Expose
    private String profilePicture;
    @SerializedName("Area")
    @Expose
    private Object area;
    @SerializedName("StateName")
    @Expose
    private String stateName;
    @SerializedName("DeliveryTypeInEnglish")
    @Expose
    private String deliveryTypeInEnglish;
    @SerializedName("DeliveryTypeInArabic")
    @Expose
    private String deliveryTypeInArabic;
    @SerializedName("NameInEnglish")
    @Expose
    private String nameInEnglish;
    @SerializedName("NameInArabic")
    @Expose
    private String nameInArabic;
    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Object getArea() {
        return area;
    }

    public void setArea(Object area) {
        this.area = area;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getDeliveryTypeInEnglish() {
        return deliveryTypeInEnglish;
    }

    public void setDeliveryTypeInEnglish(String deliveryTypeInEnglish) {
        this.deliveryTypeInEnglish = deliveryTypeInEnglish;
    }

    public String getDeliveryTypeInArabic() {
        return deliveryTypeInArabic;
    }

    public void setDeliveryTypeInArabic(String deliveryTypeInArabic) {
        this.deliveryTypeInArabic = deliveryTypeInArabic;
    }

    public String getNameInEnglish() {
        return nameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        this.nameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return nameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        this.nameInArabic = nameInArabic;
    }

}