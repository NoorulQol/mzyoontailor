package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetMeasurementDetailsResult {

    @SerializedName("GetDressSubtypes")
    @Expose
    private List<GetDressSubtypeModal> getDressSubtypes = null;

    @SerializedName("GetMaterialType")
    @Expose
    private List<GetMaterialType> getMaterialType  = null;

    @SerializedName("GetAttributeDetails")
    @Expose
    private List<GetAttributeDetailModal> getAttributeDetails = null;


    public List<GetDressSubtypeModal> getGetDressSubtypes() {
        return getDressSubtypes==null?new ArrayList<GetDressSubtypeModal>():getDressSubtypes;
    }

    public void setGetDressSubtypes(List<GetDressSubtypeModal> getDressSubtypes) {
        this.getDressSubtypes = getDressSubtypes;
    }

    public List<GetAttributeDetailModal> getGetAttributeDetails() {
        return getAttributeDetails==null?new ArrayList<GetAttributeDetailModal>():getAttributeDetails;
    }

    public void setGetAttributeDetails(List<GetAttributeDetailModal> getAttributeDetails) {
        this.getAttributeDetails = getAttributeDetails;
    }


    public List<GetMaterialType> getGetMaterialType() {
        return getMaterialType==null?new ArrayList<GetMaterialType>():getMaterialType;
    }

    public void setGetMaterialType(List<GetMaterialType> getMaterialType) {
        this.getMaterialType = getMaterialType;
    }




}
