package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BuyerRequestPendingResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<BuyerRequestPendingModal> result = null;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<BuyerRequestPendingModal> getResult() {
        return result==null?new ArrayList<BuyerRequestPendingModal>():result;
    }

    public void setResult(List<BuyerRequestPendingModal> result) {
        this.result = result;
    }

}
