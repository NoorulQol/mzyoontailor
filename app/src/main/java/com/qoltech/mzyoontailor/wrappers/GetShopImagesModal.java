package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetShopImagesModal {

    @SerializedName("ShopProfileId")
    @Expose
    private Integer shopProfileId;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("Id")
    @Expose
    private int Id;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }


    public GetShopImagesModal(Integer shopProfileId, String image) {
        this.shopProfileId = shopProfileId;
        this.image = image;
    }

    public Integer getShopProfileId() {
        return shopProfileId;
    }

    public void setShopProfileId(Integer shopProfileId) {
        this.shopProfileId = shopProfileId;
    }

    public String getImage() {
        return image==null?"":image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
