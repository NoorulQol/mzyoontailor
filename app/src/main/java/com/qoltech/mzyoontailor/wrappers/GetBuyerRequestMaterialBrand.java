package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBuyerRequestMaterialBrand {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("BrandInEnglish")
    @Expose
    private String brandInEnglish;
    @SerializedName("BrandInArabic")
    @Expose
    private String brandInArabic;
    @SerializedName("Image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrandInEnglish() {
        return brandInEnglish;
    }

    public void setBrandInEnglish(String brandInEnglish) {
        this.brandInEnglish = brandInEnglish;
    }

    public String getBrandInArabic() {
        return brandInArabic;
    }

    public void setBrandInArabic(String brandInArabic) {
        this.brandInArabic = brandInArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
