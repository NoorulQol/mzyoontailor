package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBuyerRequestCustomization {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CustomizationNameInEnglish")
    @Expose
    private String customizationNameInEnglish;
    @SerializedName("CustomizationNameInArabic")
    @Expose
    private String customizationNameInArabic;
    @SerializedName("Images")
    @Expose
    private String images;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomizationNameInEnglish() {
        return customizationNameInEnglish == null ? "" : customizationNameInEnglish;
    }

    public void setCustomizationNameInEnglish(String customizationNameInEnglish) {
        this.customizationNameInEnglish = customizationNameInEnglish;
    }

    public String getCustomizationNameInArabic() {
        return customizationNameInArabic== null ? "" : customizationNameInArabic;
    }

    public void setCustomizationNameInArabic(String customizationNameInArabic) {
        this.customizationNameInArabic = customizationNameInArabic;
    }

    public String getImages() {
        return images==null?"":images;
    }

    public void setImages(String images) {
        this.images = images;
    }

}