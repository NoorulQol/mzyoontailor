package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAttributeDetailModal {


    @SerializedName("CustomizationAttributeId")
    @Expose
    private Integer customizationAttributeId;
    @SerializedName("AttributeImageId")
    @Expose
    private Integer attributeImageId;
    @SerializedName("AttributeNameInEnglish")
    @Expose
    private String attributeNameInEnglish;
    @SerializedName("AttributeNameinArabic")
    @Expose
    private String attributeNameinArabic;
    @SerializedName("AttributeTypeNameInEnglish")
    @Expose
    private String attributeTypeNameInEnglish;
    @SerializedName("AttributeTypeNameInArabic")
    @Expose
    private String attributeTypeNameInArabic;
    @SerializedName("qty")
    @Expose
    private String qty;

    public String getQty() {
        return qty == null  ? "" : qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public Integer getCustomizationAttributeId() {
        return customizationAttributeId;
    }

    public void setCustomizationAttributeId(Integer customizationAttributeId) {
        this.customizationAttributeId = customizationAttributeId;
    }

    public Integer getAttributeImageId() {
        return attributeImageId;
    }

    public void setAttributeImageId(Integer attributeImageId) {
        this.attributeImageId = attributeImageId;
    }

    public String getAttributeNameInEnglish() {
        return attributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        this.attributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameinArabic() {
        return attributeNameinArabic;
    }

    public void setAttributeNameinArabic(String attributeNameinArabic) {
        this.attributeNameinArabic = attributeNameinArabic;
    }

    public String getAttributeTypeNameInEnglish() {
        return attributeTypeNameInEnglish;
    }

    public void setAttributeTypeNameInEnglish(String attributeTypeNameInEnglish) {
        this.attributeTypeNameInEnglish = attributeTypeNameInEnglish;
    }

    public String getAttributeTypeNameInArabic() {
        return attributeTypeNameInArabic;
    }

    public void setAttributeTypeNameInArabic(String attributeTypeNameInArabic) {
        this.attributeTypeNameInArabic = attributeTypeNameInArabic;
    }

}