package com.qoltech.mzyoontailor.wrappers;

import java.util.ArrayList;
import java.util.List;

public class CountryResponse {
    private String ResponseMsg;
    private List<Country> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public List<Country> getResult() {
        return Result==null?new ArrayList<Country>():Result;
    }

    public void setResult(List<Country> result) {
        Result = result;
    }
}
