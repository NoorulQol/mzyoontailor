package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMaterialType {
    @SerializedName("MaterialInEnglish")
    @Expose
    private String materialInEnglish;
    @SerializedName("MaterialInArabic")
    @Expose
    private String materialInArabic;

    public String getMaterialInEnglish() {
        return materialInEnglish==null?"":materialInEnglish;
    }

    public void setMaterialInEnglish(String materialInEnglish) {
        this.materialInEnglish = materialInEnglish;
    }

    public String getMaterialInArabic() {
        return materialInArabic==null?"":materialInArabic;
    }

    public void setMaterialInArabic(String materialInArabic) {
        this.materialInArabic = materialInArabic;
    }

}
