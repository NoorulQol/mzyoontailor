package com.qoltech.mzyoontailor.wrappers;

import java.util.ArrayList;
import java.util.List;

public class DashBoardHomeResponse {
    private String response;
    private String message;
    private String response_text;
    private int status;
    private List<DashboardPojo> Result;


    public List<DashboardPojo> getResult() {
        return Result==null?new ArrayList<DashboardPojo>():Result;
    }

    public void setResult(List<DashboardPojo> result) {
        Result = result;
    }

    public String getResponse() {
        return response==null?"":response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message==null?"":message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse_text() {
        return response_text==null?"":response_text;
    }

    public void setResponse_text(String response_text) {
        this.response_text = response_text;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}

