package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetBuyerRequestResult {


    @SerializedName("MaterialBrand")
    @Expose
    private List<GetBuyerRequestMaterialBrand> materialBrand = null;
    @SerializedName("MaterialType")
    @Expose
    private List<GetBuyerRequestMaterialType> materialType = null;
    @SerializedName("PlaceofIndustry")
    @Expose
    private List<GetBuyerRequestPlaceOfIndustry> placeofIndustry = null;
    @SerializedName("Customization")
    @Expose
    private List<GetBuyerRequestCustomization> customization = null;

    public List<GetBuyerRequestMaterialBrand> getMaterialBrand() {
        return materialBrand == null ? new ArrayList<GetBuyerRequestMaterialBrand>() : materialBrand;
    }

    public void setMaterialBrand(List<GetBuyerRequestMaterialBrand> materialBrand) {
        this.materialBrand = materialBrand;
    }

    public List<GetBuyerRequestMaterialType> getMaterialType() {
        return materialType == null ? new ArrayList<GetBuyerRequestMaterialType>() : materialType;
    }

    public void setMaterialType(List<GetBuyerRequestMaterialType> materialType) {
        this.materialType = materialType;
    }

    public List<GetBuyerRequestPlaceOfIndustry> getPlaceofIndustry() {
        return placeofIndustry == null ? new ArrayList<GetBuyerRequestPlaceOfIndustry>() : placeofIndustry;
    }

    public void setPlaceofIndustry(List<GetBuyerRequestPlaceOfIndustry> placeofIndustry) {
        this.placeofIndustry = placeofIndustry;
    }

    public List<GetBuyerRequestCustomization> getCustomization() {
        return customization;
    }

    public void setCustomization(List<GetBuyerRequestCustomization> customization) {
        this.customization = customization;
    }

}