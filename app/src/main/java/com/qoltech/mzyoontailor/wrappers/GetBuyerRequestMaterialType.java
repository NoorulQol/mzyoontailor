package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBuyerRequestMaterialType {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("MaterialInEnglish")
    @Expose
    private String materialInEnglish;
    @SerializedName("MaterialInArabic")
    @Expose
    private String materialInArabic;
    @SerializedName("Image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMaterialInEnglish() {
        return materialInEnglish==null?"":materialInEnglish;
    }

    public void setMaterialInEnglish(String materialInEnglish) {
        this.materialInEnglish = materialInEnglish;
    }

    public String getMaterialInArabic() {
        return materialInArabic==null?"":materialInArabic;
    }

    public void setMaterialInArabic(String materialInArabic) {
        this.materialInArabic = materialInArabic;
    }

    public String getImage() {
        return image==null?"":image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}