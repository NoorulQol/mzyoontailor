package com.qoltech.mzyoontailor.wrappers;

public class Country {

    private Integer Id;
    private String CountryName;
    private String PhoneCode;
    private String Flag;

    public String getFlag() {
        return Flag == null ? "" : Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getCountryName() {
        return CountryName==null?"":CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getPhoneCode() {
        return PhoneCode==null?"":PhoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        PhoneCode = phoneCode;
    }
}
