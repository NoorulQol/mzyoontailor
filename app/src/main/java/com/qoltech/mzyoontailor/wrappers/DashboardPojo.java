package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashboardPojo {

    @SerializedName("CompletedOrder")
    @Expose
    private Integer completedOrder;
    @SerializedName("PendingOrder")
    @Expose
    private Integer pendingOrder;
    @SerializedName("ApprovedRequest")
    @Expose
    private Integer approvedRequest;
    @SerializedName("PendingRequest")
    @Expose
    private Integer pendingRequest;
    @SerializedName("ReceivedAmount")
    @Expose
    private Float receivedAmount;
    @SerializedName("PendingAmount")
    @Expose
    private Integer pendingAmount;
    @SerializedName("StockDelivered")
    @Expose
    private Integer stockDelivered;
    @SerializedName("StockPending")
    @Expose
    private Integer stockPending;
    @SerializedName("StockApproved")
    @Expose
    private Integer stockApproved;
    @SerializedName("StockWaiting")
    @Expose
    private Integer stockWaiting;

    public Integer getCompletedOrder() {
        return completedOrder;
    }

    public void setCompletedOrder(Integer completedOrder) {
        this.completedOrder = completedOrder;
    }

    public Integer getPendingOrder() {
        return pendingOrder;
    }

    public void setPendingOrder(Integer pendingOrder) {
        this.pendingOrder = pendingOrder;
    }

    public Integer getApprovedRequest() {
        return approvedRequest;
    }

    public void setApprovedRequest(Integer approvedRequest) {
        this.approvedRequest = approvedRequest;
    }

    public Integer getPendingRequest() {
        return pendingRequest;
    }

    public void setPendingRequest(Integer pendingRequest) {
        this.pendingRequest = pendingRequest;
    }

    public Float getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Float receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public Integer getPendingAmount() {
        return pendingAmount;
    }

    public void setPendingAmount(Integer pendingAmount) {
        this.pendingAmount = pendingAmount;
    }

    public Integer getStockDelivered() {
        return stockDelivered;
    }

    public void setStockDelivered(Integer stockDelivered) {
        this.stockDelivered = stockDelivered;
    }

    public Integer getStockPending() {
        return stockPending;
    }

    public void setStockPending(Integer stockPending) {
        this.stockPending = stockPending;
    }

    public Integer getStockApproved() {
        return stockApproved;
    }

    public void setStockApproved(Integer stockApproved) {
        this.stockApproved = stockApproved;
    }

    public Integer getStockWaiting() {
        return stockWaiting;
    }

    public void setStockWaiting(Integer stockWaiting) {
        this.stockWaiting = stockWaiting;
    }

}