package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustumizationSkillUpdateGetAttributeResult {

    @SerializedName("Id")
    @Expose
    private Integer id;

    public Integer getCustomizationAttributeId() {
        return CustomizationAttributeId;
    }

    public void setCustomizationAttributeId(Integer customizationAttributeId) {
        CustomizationAttributeId = customizationAttributeId;
    }

    @SerializedName("CustomizationAttributeId")
    @Expose
    private Integer CustomizationAttributeId;

    @SerializedName("AttributeNameInEnglish")
    @Expose
    private String attributeNameInEnglish;
    @SerializedName("AttributeNameinArabic")
    @Expose
    private String attributeNameinArabic;
    @SerializedName("AttributeImage")
    @Expose
    private String attributeImage;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttributeNameInEnglish() {
        return attributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        this.attributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameinArabic() {
        return attributeNameinArabic;
    }

    public void setAttributeNameinArabic(String attributeNameinArabic) {
        this.attributeNameinArabic = attributeNameinArabic;
    }

    public String getAttributeImage() {
        return attributeImage;
    }

    public void setAttributeImage(String attributeImage) {
        this.attributeImage = attributeImage;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

}
