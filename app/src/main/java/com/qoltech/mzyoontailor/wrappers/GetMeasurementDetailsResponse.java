package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMeasurementDetailsResponse {


    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private GetMeasurementDetailsResult result;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public GetMeasurementDetailsResult getResult() {
        return result==null?new GetMeasurementDetailsResult():result;
    }

    public void setResult(GetMeasurementDetailsResult result) {
        this.result = result;
    }

}