package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetShopProfileResult {
    @SerializedName("GetShopProfiles")
    @Expose
    private List<GetShopProfileModal> getShopProfiles = null;
    @SerializedName("ShopImages")
    @Expose
    private List<GetShopImagesModal> shopImages = null;

    public List<GetShopProfileModal> getGetShopProfiles() {
        return getShopProfiles==null?new ArrayList<GetShopProfileModal>():getShopProfiles;
    }

    public void setGetShopProfiles(List<GetShopProfileModal> getShopProfiles) {
        this.getShopProfiles = getShopProfiles;
    }

    public List<GetShopImagesModal> getShopImages() {
        return shopImages==null?new ArrayList<GetShopImagesModal>():shopImages;
    }

    public void setShopImages(List<GetShopImagesModal> shopImages) {
        this.shopImages = shopImages;
    }

}
