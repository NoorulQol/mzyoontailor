package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StateResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<State> result = null;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<State> getResult() {
        return result==null?new ArrayList<State>():result;
    }

    public void setResult(List<State> result) {
        this.result = result;
    }

}