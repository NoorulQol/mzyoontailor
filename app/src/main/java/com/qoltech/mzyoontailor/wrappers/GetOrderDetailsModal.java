package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderDetailsModal {



    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("OrderDt")
    @Expose
    private String orderDt;
    @SerializedName("Product_Name")
    @Expose
    private String productName;
    @SerializedName("NameInArabic")
    @Expose
    private String nameInArabic;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("Amount")
    @Expose
    private Float amount;
    @SerializedName("ServiceType")
    @Expose
    private String serviceType;
    @SerializedName("MeasurementType")
    @Expose
    private Integer measurementType;
    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("DressType")
    @Expose
    private Integer dressType;

    public Integer getGender() {
        return Gender;
    }

    public void setGender(Integer gender) {
        Gender = gender;
    }

    @SerializedName("Gender")
    @Expose
    private Integer Gender;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderDt() {
        return orderDt;
    }

    public void setOrderDt(String orderDt) {
        this.orderDt = orderDt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getNameInArabic() {
        return nameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        this.nameInArabic = nameInArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Integer getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(Integer measurementType) {
        this.measurementType = measurementType;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getDressType() {
        return dressType;
    }

    public void setDressType(Integer dressType) {
        this.dressType = dressType;
    }

}