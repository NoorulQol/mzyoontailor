package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValidationResponse {
    private String ResponseMsg;
    private String Result;
    private  String UserId;
    @SerializedName("ShopProfileId")
    @Expose
    private Integer shopProfileId;
    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId==null?"":userId;
    }

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg==null?"":responseMsg;

    }

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result==null?"":result;
    }

    public Integer getShopProfileId() {
        return shopProfileId;
    }

    public void setShopProfileId(Integer shopProfileId) {
        this.shopProfileId = shopProfileId;
    }
}
