package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTrackingDetailsResult {
    @SerializedName("GetTrackingByOrder")
    @Expose
    private List<GetTrackingByOrderModal> getTrackingByOrder = null;
    @SerializedName("TrackingDetails")
    @Expose
    private List<GetTrackingDetailsModal> trackingDetails = null;

    public List<GetTrackingByOrderModal> getGetTrackingByOrder() {
        return getTrackingByOrder;
    }

    public void setGetTrackingByOrder(List<GetTrackingByOrderModal> getTrackingByOrder) {
        this.getTrackingByOrder = getTrackingByOrder;
    }

    public List<GetTrackingDetailsModal> getTrackingDetails() {
        return trackingDetails;
    }

    public void setTrackingDetails(List<GetTrackingDetailsModal> trackingDetails) {
        this.trackingDetails = trackingDetails;
    }

}