package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTailorChargesResult {


    @SerializedName("MeasurementType")
    @Expose
    private Integer measurementType;
    @SerializedName("OrderType")
    @Expose
    private Integer orderType;
    @SerializedName("ServiceType")
    @Expose
    private Integer serviceType;

    public Integer getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(Integer measurementType) {
        this.measurementType = measurementType;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

}
