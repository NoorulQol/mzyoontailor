package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetRequestMeasurementPartsResponse {
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetRequestMeasurementPartsModal> result = null;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetRequestMeasurementPartsModal> getResult() {
        return result==null?new ArrayList<GetRequestMeasurementPartsModal>():result;
    }

    public void setResult(List<GetRequestMeasurementPartsModal> result) {
        this.result = result;
    }

}
