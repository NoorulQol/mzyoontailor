package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderProductPriceModal {

    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("Appoinment")
    @Expose
    private Integer appoinment;
    @SerializedName("Tax")
    @Expose
    private Integer tax;
    @SerializedName("Total")
    @Expose
    private Integer total;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAppoinment() {
        return appoinment;
    }

    public void setAppoinment(Integer appoinment) {
        this.appoinment = appoinment;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
