package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TailorProfileGet {

    @SerializedName("TailorNameInEnglish")
    @Expose
    private String tailorNameInEnglish;
    @SerializedName("TailorNameInArabic")
    @Expose
    private String tailorNameInArabic;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("EmailId")
    @Expose
    private String emailId;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("ShopOwnerImageURL")
    @Expose
    private Object shopOwnerImageURL;
    @SerializedName("Dob")
    @Expose
    private String dob;

    public String getTailorNameInEnglish() {
        return tailorNameInEnglish;
    }

    public void setTailorNameInEnglish(String tailorNameInEnglish) {
        this.tailorNameInEnglish = tailorNameInEnglish;
    }

    public String getTailorNameInArabic() {
        return tailorNameInArabic;
    }

    public void setTailorNameInArabic(String tailorNameInArabic) {
        this.tailorNameInArabic = tailorNameInArabic;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getShopOwnerImageURL() {
        return shopOwnerImageURL;
    }

    public void setShopOwnerImageURL(Object shopOwnerImageURL) {
        this.shopOwnerImageURL = shopOwnerImageURL;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

}