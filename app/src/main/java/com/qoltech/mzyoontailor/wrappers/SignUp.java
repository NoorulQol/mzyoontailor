package com.qoltech.mzyoontailor.wrappers;

public class SignUp {
    private String DeviceId;
    private String CountryCode;
    private String PhoneNo;

    public String getDeviceId() {
        return DeviceId==null?"":DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public String getCountryCode() {
        return CountryCode==null?"":CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getPhoneNo() {
        return PhoneNo==null?"":PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

}
