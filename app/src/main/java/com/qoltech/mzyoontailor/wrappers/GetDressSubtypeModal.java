package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDressSubtypeModal {
    @SerializedName("NameInEnglish")
    @Expose
    private String nameInEnglish;
    @SerializedName("NameInArabic")
    @Expose
    private String nameInArabic;
    @SerializedName("Image")
    @Expose
    private String image;

    public String getNameInEnglish() {
        return nameInEnglish==null?"":nameInEnglish;
    }

    public void setNameInEnglish(String nameInEnglish) {
        this.nameInEnglish = nameInEnglish;
    }

    public String getNameInArabic() {
        return nameInArabic==null?"":nameInArabic;
    }

    public void setNameInArabic(String nameInArabic) {
        this.nameInArabic = nameInArabic;
    }

    public String getImage() {
        return image==null?"":image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
