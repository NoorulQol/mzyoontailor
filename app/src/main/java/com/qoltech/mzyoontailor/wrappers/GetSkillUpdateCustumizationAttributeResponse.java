package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSkillUpdateCustumizationAttributeResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetSkillUpdateCustumizationAttributeResult> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetSkillUpdateCustumizationAttributeResult> getResult() {
        return result;
    }

    public void setResult(List<GetSkillUpdateCustumizationAttributeResult> result) {
        this.result = result;
    }

}
