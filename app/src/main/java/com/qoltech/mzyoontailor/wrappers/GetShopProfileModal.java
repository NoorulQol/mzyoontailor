package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetShopProfileModal {
    @SerializedName("TailorId")
    @Expose
    private Integer tailorId;
    @SerializedName("ShopNameInEnglish")
    @Expose
    private String shopNameInEnglish;
    @SerializedName("ShopNameInArabic")
    @Expose
    private String shopNameInArabic;
    @SerializedName("CountryName")
    @Expose
    private String countryName;
    @SerializedName("StateName")
    @Expose
    private String stateName;
    @SerializedName("Latitude")
    @Expose
    private Float latitude;
    @SerializedName("Longitude")
    @Expose
    private Float longitude;
    @SerializedName("AddressInEnglish")
    @Expose
    private String addressInEnglish;
    @SerializedName("AddressinArabic")
    @Expose
    private String addressinArabic;
    @SerializedName("ShopOwnerImageURL")
    @Expose
    private String shopOwnerImageURL;

    @SerializedName("CountryId")
    @Expose
    private String CountryId;



    @SerializedName("CityId")
    @Expose
    private String CityId;

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public Integer getTailorId() {
        return tailorId;
    }

    public void setTailorId(Integer tailorId) {
        this.tailorId = tailorId;
    }

    public String getShopNameInEnglish() {
        return shopNameInEnglish==null?"":shopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        this.shopNameInEnglish = shopNameInEnglish;
    }

    public String getShopNameInArabic() {
        return shopNameInArabic==null?"":shopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        this.shopNameInArabic = shopNameInArabic;
    }

    public String getCountryName() {
        return countryName==null?"":countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getStateName() {
        return stateName==null?"":stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public String getAddressInEnglish() {
        return addressInEnglish==null?"":addressInEnglish;
    }

    public void setAddressInEnglish(String addressInEnglish) {
        this.addressInEnglish = addressInEnglish;
    }

    public String getAddressinArabic() {
        return addressinArabic==null?"":addressinArabic;
    }

    public void setAddressinArabic(String addressinArabic) {
        this.addressinArabic = addressinArabic;
    }

    public String getShopOwnerImageURL() {
        return shopOwnerImageURL==null?"":shopOwnerImageURL;
    }

    public void setShopOwnerImageURL(String shopOwnerImageURL) {
        this.shopOwnerImageURL = shopOwnerImageURL;
    }

}
