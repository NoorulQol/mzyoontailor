package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BuyerRequestApprovedResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<BuyerRequestApprovedModal> result = null;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<BuyerRequestApprovedModal> getResult() {
        return result==null?new ArrayList<BuyerRequestApprovedModal>():result;
    }

    public void setResult(List<BuyerRequestApprovedModal> result) {
        this.result = result;
    }

}
