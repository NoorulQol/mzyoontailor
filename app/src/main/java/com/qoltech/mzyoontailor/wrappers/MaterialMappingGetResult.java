package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaterialMappingGetResult {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("PatternInEnglish")
    @Expose
    private String patternInEnglish;
    @SerializedName("PatternInArabic")
    @Expose
    private String patternInArabic;
    @SerializedName("Image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPatternInEnglish() {
        return patternInEnglish;
    }

    public void setPatternInEnglish(String patternInEnglish) {
        this.patternInEnglish = patternInEnglish;
    }

    public String getPatternInArabic() {
        return patternInArabic;
    }

    public void setPatternInArabic(String patternInArabic) {
        this.patternInArabic = patternInArabic;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}