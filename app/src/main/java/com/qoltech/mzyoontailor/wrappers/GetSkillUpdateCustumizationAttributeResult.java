package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSkillUpdateCustumizationAttributeResult {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("AttributeNameInEnglish")
    @Expose
    private String attributeNameInEnglish;
    @SerializedName("AttributeNameinArabic")
    @Expose
    private String attributeNameinArabic;
    @SerializedName("AttributeImage")
    @Expose
    private Object attributeImage;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAttributeNameInEnglish() {
        return attributeNameInEnglish;
    }

    public void setAttributeNameInEnglish(String attributeNameInEnglish) {
        this.attributeNameInEnglish = attributeNameInEnglish;
    }

    public String getAttributeNameinArabic() {
        return attributeNameinArabic;
    }

    public void setAttributeNameinArabic(String attributeNameinArabic) {
        this.attributeNameinArabic = attributeNameinArabic;
    }

    public Object getAttributeImage() {
        return attributeImage;
    }

    public void setAttributeImage(Object attributeImage) {
        this.attributeImage = attributeImage;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

}
