package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OldCustomerRespone {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<OldCustomerPojo> result = null;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<OldCustomerPojo> getResult() {
        return result==null?new ArrayList<OldCustomerPojo>():result;
    }

    public void setResult(List<OldCustomerPojo> result) {
        this.result = result;
    }

}
