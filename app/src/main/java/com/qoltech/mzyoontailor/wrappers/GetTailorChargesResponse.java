package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTailorChargesResponse {
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetTailorChargesResult> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetTailorChargesResult> getResult() {
        return result;
    }

    public void setResult(List<GetTailorChargesResult> result) {
        this.result = result;
    }

}
