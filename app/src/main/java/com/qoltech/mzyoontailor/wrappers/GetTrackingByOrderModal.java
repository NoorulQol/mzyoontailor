package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTrackingByOrderModal {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Switch")
    @Expose
    private Boolean _switch;
    @SerializedName("SwitchStatus")
    @Expose
    private Integer switchStatus;

    @SerializedName("statusInArabic")
    @Expose
    private String statusInArabic;

    public String getStatusInArabic() {
        return statusInArabic;
    }

    public void setStatusInArabic(String statusInArabic) {
        this.statusInArabic = statusInArabic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

    public Integer getSwitchStatus() {
        return switchStatus;
    }

    public void setSwitchStatus(Integer switchStatus) {
        this.switchStatus = switchStatus;
    }

}