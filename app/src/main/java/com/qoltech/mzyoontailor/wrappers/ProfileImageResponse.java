package com.qoltech.mzyoontailor.wrappers;

public class ProfileImageResponse {



    private String responseMsg;
    private String result;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getResult() {
        return result==null?"":result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}