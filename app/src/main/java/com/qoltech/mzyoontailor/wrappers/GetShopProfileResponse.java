package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetShopProfileResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private GetShopProfileResult result;

    public String getResponseMsg() {
        return responseMsg==null?"":responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public GetShopProfileResult getResult() {
        return result==null?new GetShopProfileResult():result;
    }

    public void setResult(GetShopProfileResult result) {
        this.result = result;
    }

}