package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderDetailsResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private GetOrderDetailsResult result;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public GetOrderDetailsResult getResult() {
        return result;
    }

    public void setResult(GetOrderDetailsResult result) {
        this.result = result;
    }

}
