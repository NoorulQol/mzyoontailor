package com.qoltech.mzyoontailor.wrappers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBuyerRequestPlaceOfIndustry {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("PlaceInEnglish")
    @Expose
    private String placeInEnglish;
    @SerializedName("PlaceInArabic")
    @Expose
    private String placeInArabic;
    @SerializedName("Image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlaceInEnglish() {
        return placeInEnglish==null?"":placeInEnglish;
    }

    public void setPlaceInEnglish(String placeInEnglish) {
        this.placeInEnglish = placeInEnglish;
    }

    public String getPlaceInArabic() {
        return placeInArabic==null?"":placeInArabic;
    }

    public void setPlaceInArabic(String placeInArabic) {
        this.placeInArabic = placeInArabic;
    }

    public String getImage() {
        return image==null?"":image;
    }

    public void setImage(String image) {
        this.image = image;
    }



}