package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.GetNoOfAppointmentDateForScheduleEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetNoOfAppointmentDateForScheduleResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetNoOfAppointmentDateForScheduleEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetNoOfAppointmentDateForScheduleEntity> getResult() {
        return Result ;
    }

    public void setResult(ArrayList<GetNoOfAppointmentDateForScheduleEntity> result) {
        Result = result;
    }

}
