package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.ApicallidEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomizationOneApiCallModal implements Serializable {
    public ArrayList<ApicallidEntity> placeofOrginId;
    public ArrayList<ApicallidEntity> seasonId;

    public ArrayList<ApicallidEntity> getPlaceofOrginId() {
        return placeofOrginId == null ? new ArrayList<ApicallidEntity>() : placeofOrginId;
    }

    public void setPlaceofOrginId(ArrayList<ApicallidEntity> placeofOrginId) {
        this.placeofOrginId = placeofOrginId;
    }

    public ArrayList<ApicallidEntity> getSeasonId() {
        return seasonId == null ? new ArrayList<ApicallidEntity>() : seasonId;
    }

    public void setSeasonId(ArrayList<ApicallidEntity> seasonId) {
        this.seasonId = seasonId;
    }


}
