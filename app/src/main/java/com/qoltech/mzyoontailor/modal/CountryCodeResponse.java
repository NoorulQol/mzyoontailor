package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GetCountryEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CountryCodeResponse implements Serializable {

    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetCountryEntity> getResult() {
        return Result == null ? new ArrayList<GetCountryEntity>() : Result;
    }

    public void setResult(ArrayList<GetCountryEntity> result) {
        Result = result;
    }

    public ArrayList<GetCountryEntity> Result;
}
