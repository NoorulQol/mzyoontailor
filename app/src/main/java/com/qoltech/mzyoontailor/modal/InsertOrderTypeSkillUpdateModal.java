package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.InsertOrderTypeSkillUpdateEntity;

import java.util.ArrayList;

public class InsertOrderTypeSkillUpdateModal {

    ArrayList<InsertOrderTypeSkillUpdateEntity> OrderTypeId;
    String TailorId;

    public String getTailorId() {
        return TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public ArrayList<InsertOrderTypeSkillUpdateEntity> getOrderTypeId() {
        return OrderTypeId;
    }

    public void setOrderTypeId(ArrayList<InsertOrderTypeSkillUpdateEntity> orderTypeId) {
        OrderTypeId = orderTypeId;
    }
}
