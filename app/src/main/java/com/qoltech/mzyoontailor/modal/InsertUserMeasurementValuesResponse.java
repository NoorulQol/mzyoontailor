package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.UserMeasurementValueEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertUserMeasurementValuesResponse implements Serializable {
    public String UserId;
    public String DressTypeId;
    public String Units;
    public String MeasurementBy;
    public String CreatedBy;
    public String Name;
    public ArrayList<UserMeasurementValueEntity> userMeasurements;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDressTypeId() {
        return DressTypeId;
    }

    public void setDressTypeId(String dressTypeId) {
        DressTypeId = dressTypeId;
    }

    public String getUnits() {
        return Units;
    }

    public void setUnits(String units) {
        Units = units;
    }

    public String getMeasurementBy() {
        return MeasurementBy;
    }

    public void setMeasurementBy(String measurementBy) {
        MeasurementBy = measurementBy;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ArrayList<UserMeasurementValueEntity> getUserMeasurements() {
        return userMeasurements;
    }

    public void setUserMeasurements(ArrayList<UserMeasurementValueEntity> userMeasurements) {
        userMeasurements = userMeasurements;
    }


}
