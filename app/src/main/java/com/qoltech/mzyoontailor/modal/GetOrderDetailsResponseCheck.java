package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.CheckGetOrderDetailsResult;

import java.io.Serializable;

public class GetOrderDetailsResponseCheck implements Serializable {

   public String ResponseMsg;
    public CheckGetOrderDetailsResult Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public CheckGetOrderDetailsResult getResult() {
        return Result == null ? new CheckGetOrderDetailsResult() : Result;
    }

    public void setResult(CheckGetOrderDetailsResult result) {
        Result = result;
    }




}
