package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;

public class AppointmentMaterialEntity implements Serializable {
    public String status;
    public int OrderId;
    public int BookAppointId;
    public String HeaderInEnglish;
    public String HeaderInArabic;
    public String HeaderImage;
    public String BodyImage;
    public String Payment;
    public int AppointmentId;

    public int getAppointmentId() {
        return AppointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        AppointmentId = appointmentId;
    }


    public void setPayment(String payment) {
        Payment = payment;
    }

    public String getPayment() {
        return Payment == null ? "" : Payment;
    }

    public String getStatus() {
        return status == null ? "" : status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getOrderId() {
        return OrderId;
    }

    public void setOrderId(int orderId) {
        OrderId = orderId;
    }

    public int getBookAppointId() {
        return BookAppointId;
    }

    public void setBookAppointId(int bookAppointId) {
        BookAppointId = bookAppointId;
    }

    public String getHeaderInEnglish() {
        return HeaderInEnglish == null ? "" : HeaderInEnglish;
    }

    public void setHeaderInEnglish(String headerInEnglish) {
        HeaderInEnglish = headerInEnglish;
    }

    public String getHeaderInArabic() {
        return HeaderInArabic == null ? "" : HeaderInArabic;
    }

    public void setHeaderInArabic(String headerInArabic) {
        HeaderInArabic = headerInArabic;
    }

    public String getHeaderImage() {
        return HeaderImage == null ? "" : HeaderImage;
    }

    public void setHeaderImage(String headerImage) {
        HeaderImage = headerImage;
    }

    public String getBodyImage() {
        return BodyImage == null ? "" : BodyImage;
    }

    public void setBodyImage(String bodyImage) {
        BodyImage = bodyImage;
    }

}
