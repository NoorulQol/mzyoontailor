package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.GetPatternByMaterialIdResult;

public class MaterialByIdGetRespone {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private GetPatternByMaterialIdResult result;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public GetPatternByMaterialIdResult getResult() {
        return result;
    }

    public void setResult(GetPatternByMaterialIdResult result) {
        this.result = result;
    }

}
