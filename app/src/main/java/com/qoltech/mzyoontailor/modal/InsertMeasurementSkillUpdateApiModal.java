package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.InsertMeasurementSkillUpdateEntity;

import java.util.ArrayList;

public class InsertMeasurementSkillUpdateApiModal {


    public ArrayList<InsertMeasurementSkillUpdateEntity> getMeasurementTypeId() {
        return MeasurementTypeId;
    }

    public void setMeasurementTypeId(ArrayList<InsertMeasurementSkillUpdateEntity> measurementTypeId) {
        MeasurementTypeId = measurementTypeId;
    }

    public String getTailorId() {
        return TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public ArrayList<InsertMeasurementSkillUpdateEntity> MeasurementTypeId;

    public String TailorId;

}
