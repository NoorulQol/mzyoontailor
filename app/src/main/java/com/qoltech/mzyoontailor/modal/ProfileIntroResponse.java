package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;

public class ProfileIntroResponse implements Serializable {
    private String ResponseMsg;
    private String Result;


    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }

}
