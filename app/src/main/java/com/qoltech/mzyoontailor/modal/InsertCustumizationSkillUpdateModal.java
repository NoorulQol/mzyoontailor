package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.InsertCustumizationSkillUpdateEntity;
import com.qoltech.mzyoontailor.entity.InsertMaterialChargesEntitySkillUpdateEntity;

import java.util.ArrayList;

public class InsertCustumizationSkillUpdateModal {
    String TailorId;
    String DressSubTypeId;
    String StichingCharges;
    String NoOfDays;

    public String getNoOfDays() {
        return NoOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        NoOfDays = noOfDays;
    }
//    public String getCustomizationAttributeId() {
//        return CustomizationAttributeId;
//    }
//
//    public void setCustomizationAttributeId(String customizationAttributeId) {
//        CustomizationAttributeId = customizationAttributeId;
//    }

    ArrayList<InsertCustumizationSkillUpdateEntity> OrderCustomization;
    ArrayList<InsertMaterialChargesEntitySkillUpdateEntity> Materials;

    public String getTailorId() {
        return TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public String getDressSubTypeId() {
        return DressSubTypeId;
    }

    public void setDressSubTypeId(String dressSubTypeId) {
        DressSubTypeId = dressSubTypeId;
    }

    public String getStichingCharges() {
        return StichingCharges;
    }

    public void setStichingCharges(String stichingCharges) {
        StichingCharges = stichingCharges;
    }

    public ArrayList<InsertCustumizationSkillUpdateEntity> getCustomizationSkill() {
        return OrderCustomization;
    }

    public void setCustomizationSkill(ArrayList<InsertCustumizationSkillUpdateEntity> customizationSkill) {
        OrderCustomization = customizationSkill;
    }

    public ArrayList<InsertMaterialChargesEntitySkillUpdateEntity> getMaterialCharge() {
        return Materials;
    }

    public void setMaterialCharge(ArrayList<InsertMaterialChargesEntitySkillUpdateEntity> materialCharge) {
        Materials = materialCharge;
    }
}
