package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.CustomizationAttributesEntity;
import com.qoltech.mzyoontailor.entity.CustomizationImagesEntity;
import com.qoltech.mzyoontailor.entity.GetCustomizationThreeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomizationThreeGetModal implements Serializable {

    public ArrayList<CustomizationImagesEntity> CustomizationImages;
    public ArrayList<CustomizationAttributesEntity> CustomizationAttributes;
    public ArrayList<GetCustomizationThreeEntity> AttributeImages;

    public ArrayList<GetCustomizationThreeEntity> getAttributeImages() {
        return AttributeImages == null ? new ArrayList<GetCustomizationThreeEntity>() : AttributeImages;
    }

    public void setAttributeImages(ArrayList<GetCustomizationThreeEntity> attributeImages) {
        AttributeImages = attributeImages;
    }



    public ArrayList<CustomizationImagesEntity> getCustomizationImages() {
        return CustomizationImages == null ? new ArrayList<CustomizationImagesEntity>() : CustomizationImages;
    }

    public void setCustomizationImages(ArrayList<CustomizationImagesEntity> customizationImages) {
        CustomizationImages = customizationImages;
    }

    public ArrayList<CustomizationAttributesEntity> getCustomizationAttributes() {
        return CustomizationAttributes == null ? new ArrayList<CustomizationAttributesEntity>() : CustomizationAttributes;
    }

    public void setCustomizationAttributes(ArrayList<CustomizationAttributesEntity> customizationAttributes) {
        CustomizationAttributes = customizationAttributes;
    }


}


