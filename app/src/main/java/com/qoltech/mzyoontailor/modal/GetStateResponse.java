package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.GetStateEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetStateResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetStateEntity> getResult() {
        return Result == null ? new ArrayList<GetStateEntity>() : Result;
    }

    public void setResult(ArrayList<GetStateEntity> result) {
        Result = result;
    }

    public ArrayList<GetStateEntity> Result;
}
