package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;

public class CustomizationThreeGetResponse implements Serializable {

    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg == null ? "" : ResponseMsg;
    }

    public CustomizationThreeGetModal getResult() {
        return Result == null ? new CustomizationThreeGetModal() : Result;
    }

    public void setResult(CustomizationThreeGetModal result) {
        Result = result;
    }

    public CustomizationThreeGetModal Result;
}
