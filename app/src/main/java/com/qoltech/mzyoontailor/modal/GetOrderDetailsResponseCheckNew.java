package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.ResultGetOrderDetails;

import java.io.Serializable;

public class GetOrderDetailsResponseCheckNew implements Serializable {

    public String ResponseMsg;
    public ResultGetOrderDetails Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ResultGetOrderDetails getResult() {
        return Result == null ? new ResultGetOrderDetails() : Result;
    }

    public void setResult(ResultGetOrderDetails result) {
        Result = result;
    }
}