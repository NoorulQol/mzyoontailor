package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GetMeasurementTwoEntity;

import java.io.Serializable;

public class GetMeasurementTwoResponse implements Serializable {

    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetMeasurementTwoEntity getResult() {
        return Result == null ? new GetMeasurementTwoEntity() : Result;
    }

    public void setResult(GetMeasurementTwoEntity result) {
        Result = result;
    }

    public GetMeasurementTwoEntity Result;
}
