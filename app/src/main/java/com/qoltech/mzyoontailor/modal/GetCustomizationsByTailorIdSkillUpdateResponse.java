package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.GetCustomizationsByTailorIdResult;

public class GetCustomizationsByTailorIdSkillUpdateResponse {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private GetCustomizationsByTailorIdResult result;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public GetCustomizationsByTailorIdResult getResult() {
        return result;
    }

    public void setResult(GetCustomizationsByTailorIdResult result) {
        this.result = result;
    }

}
