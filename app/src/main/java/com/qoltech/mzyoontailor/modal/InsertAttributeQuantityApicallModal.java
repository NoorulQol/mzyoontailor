package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.InsertAttributeApiCallEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertAttributeQuantityApicallModal implements Serializable {
    public ArrayList<InsertAttributeApiCallEntity> AttributeQuantity;
    public String MaterialQty;
    public String OrderId;
    public String getMaterialQty() {
        return MaterialQty;
    }

    public void setMaterialQty(String materialQty) {
        MaterialQty = materialQty;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }



    public ArrayList<InsertAttributeApiCallEntity> getAttributeQuantity() {
        return AttributeQuantity == null ? new ArrayList<InsertAttributeApiCallEntity>() : AttributeQuantity;
    }

    public void setAttributeQuantity(ArrayList<InsertAttributeApiCallEntity> attributeQuantity) {
        AttributeQuantity = attributeQuantity;
    }

}
