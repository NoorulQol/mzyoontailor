package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class FileUploadResponse implements Serializable {
    public ArrayList<String> Result;
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<String> getResult() {
        return Result == null ? new ArrayList<String>() : Result;
    }

    public void setResult(ArrayList<String> result) {
        Result = result;
    }

}
