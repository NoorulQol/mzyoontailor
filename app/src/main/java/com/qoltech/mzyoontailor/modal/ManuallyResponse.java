package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.ManuallyEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class ManuallyResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<ManuallyEntity> getResult() {
        return Result == null ? new ArrayList<ManuallyEntity>() : Result;
    }

    public void setResult(ArrayList<ManuallyEntity> result) {
        Result = result;
    }

    public ArrayList<ManuallyEntity> Result;
}
