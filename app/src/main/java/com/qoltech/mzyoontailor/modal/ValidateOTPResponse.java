package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;

public class ValidateOTPResponse implements Serializable {
    public String ResponseMsg;
    public String Result;


    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }

}
