package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.GetTailorDefaultChargesEntity;

import java.util.List;

public class GetTailorDefaultChargesResponse {
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetTailorDefaultChargesEntity> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetTailorDefaultChargesEntity> getResult() {
        return result;
    }

    public void setResult(List<GetTailorDefaultChargesEntity> result) {
        this.result = result;
    }

}
