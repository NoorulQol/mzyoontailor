package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.InsertServiceTypeSkillUpdateEntity;

import java.util.ArrayList;

public class InsertServiceTypeSkillUpdateModal {
    ArrayList<InsertServiceTypeSkillUpdateEntity> AppoinmentTypeId;
    String TailorId;

    public ArrayList<InsertServiceTypeSkillUpdateEntity> getAppoinmentTypeId() {
        return AppoinmentTypeId;
    }

    public void setAppoinmentTypeId(ArrayList<InsertServiceTypeSkillUpdateEntity> appoinmentTypeId) {
        AppoinmentTypeId = appoinmentTypeId;
    }

    public String getTailorId() {
        return TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }
}
