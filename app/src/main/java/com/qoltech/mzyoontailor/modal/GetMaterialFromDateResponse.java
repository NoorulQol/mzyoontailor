package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.GetMaterialFromDateEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMaterialFromDateResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetMaterialFromDateEntity> getResult() {
        return Result==null?new ArrayList<GetMaterialFromDateEntity>():Result;
    }

    public void setResult(ArrayList<GetMaterialFromDateEntity> result) {
        Result = result;
    }

    public ArrayList<GetMaterialFromDateEntity> Result;
}
