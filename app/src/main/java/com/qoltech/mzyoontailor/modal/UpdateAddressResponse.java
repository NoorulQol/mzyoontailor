package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;

public class UpdateAddressResponse implements Serializable {
    public String ResponseMsg;
    public String Result;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg == null ? "" : ResponseMsg;
    }

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }


}
