package com.qoltech.mzyoontailor.modal;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.GetAreaEntity;

import java.util.List;

public class GetAreaResponse  {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetAreaEntity> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetAreaEntity> getResult() {
        return result;
    }

    public void setResult(List<GetAreaEntity> result) {
        this.result = result;
    }

}