package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;
import java.util.ArrayList;

public class AppointmentMaterialResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<AppointmentMaterialEntity> getResult() {
        return Result == null ? new ArrayList<AppointmentMaterialEntity>() : Result;
    }

    public void setResult(ArrayList<AppointmentMaterialEntity> result) {
        Result = result;
    }

    public ArrayList<AppointmentMaterialEntity> Result;

}
