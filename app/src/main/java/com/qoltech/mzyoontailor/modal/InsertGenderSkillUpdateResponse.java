package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GenderIds;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertGenderSkillUpdateResponse implements Serializable {

    public ArrayList<GenderIds> getIds() {
        return Ids == null ? new ArrayList<GenderIds>() : Ids;
    }

    public void setIds(ArrayList<GenderIds> ids) {
        Ids = ids;
    }

    public ArrayList<GenderIds> Ids;

}
