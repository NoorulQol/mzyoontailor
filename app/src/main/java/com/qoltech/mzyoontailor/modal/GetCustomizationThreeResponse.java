package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GetCustomizationThreeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetCustomizationThreeResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<GetCustomizationThreeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetCustomizationThreeEntity> getResult() {
        return Result == null ? new ArrayList<GetCustomizationThreeEntity>() : Result;
    }

    public void setResult(ArrayList<GetCustomizationThreeEntity> result) {
        Result = result;
    }
}
