package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.ViewDetailsEntity;

import java.io.Serializable;

public class ViewDetailsResponse implements Serializable {
    public String ResponseMsg;

    public ViewDetailsEntity getResult() {
        return Result == null ? new ViewDetailsEntity() : Result;
    }

    public void setResult(ViewDetailsEntity result) {
        Result = result;
    }

    public ViewDetailsEntity Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }



}
