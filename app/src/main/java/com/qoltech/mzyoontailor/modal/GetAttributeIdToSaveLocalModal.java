package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.GetAttributesIdToSaveLocalEntity;

import java.util.List;

public class GetAttributeIdToSaveLocalModal {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetAttributesIdToSaveLocalEntity> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetAttributesIdToSaveLocalEntity> getResult() {
        return result;
    }

    public void setResult(List<GetAttributesIdToSaveLocalEntity> result) {
        this.result = result;
    }

}
