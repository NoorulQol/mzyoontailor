package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.BuyerOrderDetailsResult;

public class BuyerOrderDetailResponse {


    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private BuyerOrderDetailsResult result;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public BuyerOrderDetailsResult getResult() {
        return result;
    }

    public void setResult(BuyerOrderDetailsResult result) {
        this.result = result;
    }

}
