package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.ApicallidEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class CustomizationTwoApiCallModal implements Serializable {
    public ArrayList<ApicallidEntity> BrandId;
    public ArrayList<ApicallidEntity> MaterialTypeId;
    public ArrayList<ApicallidEntity> ColorId;

    public ArrayList<ApicallidEntity> getColorId() {
        return ColorId == null ? new ArrayList<ApicallidEntity>() : ColorId;
    }

    public void setColorId(ArrayList<ApicallidEntity> colorId) {
        ColorId = colorId;
    }

    public ArrayList<ApicallidEntity> getBrandId() {
        return BrandId == null ? new ArrayList<ApicallidEntity>() : BrandId;
    }

    public void setBrandId(ArrayList<ApicallidEntity> brandId) {
        BrandId = brandId;
    }

    public ArrayList<ApicallidEntity> getMaterialTypeId() {
        return MaterialTypeId == null ? new ArrayList<ApicallidEntity>() : MaterialTypeId;
    }

    public void setMaterialTypeId(ArrayList<ApicallidEntity> materialTypeId) {
        MaterialTypeId = materialTypeId;
    }

}
