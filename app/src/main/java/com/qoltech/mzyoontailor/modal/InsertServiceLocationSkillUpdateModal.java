package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.InsertServiceLocationSkillUpdateEntity;

import java.util.ArrayList;

public class InsertServiceLocationSkillUpdateModal {

    String TailorId;
    String StateId;

    ArrayList<InsertServiceLocationSkillUpdateEntity> AreaId;

    public String getTailorId() {
        return TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }

    public ArrayList<InsertServiceLocationSkillUpdateEntity> getAreaId() {
        return AreaId;
    }

    public void setAreaId(ArrayList<InsertServiceLocationSkillUpdateEntity> areaId) {
        AreaId = areaId;
    }
}
