package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.InsertMeasurmentValueEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertMeasurementValueModal implements Serializable {
    public String UserId;
    public String DressTypeId;
    public ArrayList<InsertMeasurmentValueEntity> MeasurementValue;
    public String Units;
    public String MeasurementBy;
    public String CreatedBy;
    public String Name;

    public String getUserId() {
        return UserId == null ? "" : UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDressTypeId() {
        return DressTypeId == null ? "" : DressTypeId;
    }

    public void setDressTypeId(String dressTypeId) {
        DressTypeId = dressTypeId;
    }

    public ArrayList<InsertMeasurmentValueEntity> getMeasurementValue() {
        return MeasurementValue == null ? new ArrayList<InsertMeasurmentValueEntity>() : MeasurementValue;
    }

    public void setMeasurementValue(ArrayList<InsertMeasurmentValueEntity> measurementValue) {
        MeasurementValue = measurementValue;
    }

    public String getUnits() {
        return Units == null ? "" : Units;
    }

    public void setUnits(String units) {
        Units = units;
    }

    public String getMeasurementBy() {
        return MeasurementBy == null ? "" : MeasurementBy;
    }

    public void setMeasurementBy(String measurementBy) {
        MeasurementBy = measurementBy;
    }

    public String getCreatedBy() {
        return CreatedBy == null ? "" : CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getName() {
        return Name == null ? "" : Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
