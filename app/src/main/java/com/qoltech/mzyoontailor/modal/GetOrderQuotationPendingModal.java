package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.GetQuotaionPendingEntity;

import java.util.List;

public class GetOrderQuotationPendingModal {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<GetQuotaionPendingEntity> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<GetQuotaionPendingEntity> getResult() {
        return result;
    }

    public void setResult(List<GetQuotaionPendingEntity> result) {
        this.result = result;
    }

}
