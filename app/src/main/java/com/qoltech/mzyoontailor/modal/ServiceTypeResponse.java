package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.ServiceTypeEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class ServiceTypeResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<ServiceTypeEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<ServiceTypeEntity> getResult() {
        return Result == null ? new ArrayList<ServiceTypeEntity>() : Result;
    }

    public void setResult(ArrayList<ServiceTypeEntity> result) {
        Result = result;
    }

}
