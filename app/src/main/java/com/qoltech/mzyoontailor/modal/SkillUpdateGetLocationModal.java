package com.qoltech.mzyoontailor.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qoltech.mzyoontailor.entity.SkillUpdateGetLocationEntity;

import java.util.List;

public class SkillUpdateGetLocationModal {

    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("Result")
    @Expose
    private List<SkillUpdateGetLocationEntity> result = null;

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<SkillUpdateGetLocationEntity> getResult() {
        return result;
    }

    public void setResult(List<SkillUpdateGetLocationEntity> result) {
        this.result = result;
    }

}
