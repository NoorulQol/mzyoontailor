package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GenderEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GenderResponse implements Serializable {

    public String ResponseMsg;
    public ArrayList<GenderEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GenderEntity> getResult() {
        return Result == null ? new ArrayList<GenderEntity>() : Result;
    }

    public void setResult(ArrayList<GenderEntity> result) {
        Result = result;
    }

}

