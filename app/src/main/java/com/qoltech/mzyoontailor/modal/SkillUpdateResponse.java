package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.DressSubIdEntity;
import com.qoltech.mzyoontailor.entity.SkillIdEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class SkillUpdateResponse implements Serializable {
    public String TailorId;
    public ArrayList<SkillIdEntity> GenderId;
    public ArrayList<SkillIdEntity> DressType;
    public ArrayList<DressSubIdEntity> DressSubtypeCustomazationMapping;
    public ArrayList<SkillIdEntity> OrderType;
    public ArrayList<SkillIdEntity> MeasurementId;
    public ArrayList<SkillIdEntity> AppoinmentId;

    public String getTailorId() {
        return TailorId == null ? "" : TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public ArrayList<SkillIdEntity> getGenderId() {
        return GenderId == null ? new ArrayList<SkillIdEntity>() : GenderId;
    }

    public void setGenderId(ArrayList<SkillIdEntity> genderId) {
        GenderId = genderId;
    }

    public ArrayList<SkillIdEntity> getDressType() {
        return DressType == null ? new ArrayList<SkillIdEntity>() : DressType;
    }

    public void setDressType(ArrayList<SkillIdEntity> dressType) {
        DressType = dressType;
    }

    public ArrayList<DressSubIdEntity> getDressSubtypeCustomazationMapping() {
        return DressSubtypeCustomazationMapping == null ? new ArrayList<DressSubIdEntity>() :DressSubtypeCustomazationMapping ;
    }

    public void setDressSubtypeCustomazationMapping(ArrayList<DressSubIdEntity> dressSubtypeCustomazationMapping) {
        DressSubtypeCustomazationMapping = dressSubtypeCustomazationMapping;
    }

    public ArrayList<SkillIdEntity> getOrderType() {
        return OrderType == null ? new ArrayList<SkillIdEntity>() : OrderType;
    }

    public void setOrderType(ArrayList<SkillIdEntity> orderType) {
        OrderType = orderType;
    }

    public ArrayList<SkillIdEntity> getMeasurementId() {
        return MeasurementId == null ? new ArrayList<SkillIdEntity>() : MeasurementId;
    }

    public void setMeasurementId(ArrayList<SkillIdEntity> measurementId) {
        MeasurementId = measurementId;
    }

    public ArrayList<SkillIdEntity> getAppoinmentId() {
        return AppoinmentId == null ? new ArrayList<SkillIdEntity>() : AppoinmentId;
    }

    public void setAppoinmentId(ArrayList<SkillIdEntity> appoinmentId) {
        AppoinmentId = appoinmentId;
    }

    public ArrayList<SkillIdEntity> getAreaId() {
        return AreaId == null ? new ArrayList<SkillIdEntity>() : AreaId;
    }

    public void setAreaId(ArrayList<SkillIdEntity> areaId) {
        AreaId = areaId;
    }

    public ArrayList<SkillIdEntity> AreaId;

}
