package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GetCustomizationTwoEntity;

import java.io.Serializable;

public class GetCustomizationTwoResponse implements Serializable {

    public String ResponseMsg;
    public GetCustomizationTwoEntity Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetCustomizationTwoEntity getResult() {
        return Result == null ? new GetCustomizationTwoEntity() : Result;
    }

    public void setResult(GetCustomizationTwoEntity result) {
        Result = result;
    }
}
