package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.GetCustomizationOneEntity;

import java.io.Serializable;

public class GetCustomizationOneResponse implements Serializable {
    public String ResponseMsg;
    public GetCustomizationOneEntity Result;

    public String getResponseMsg() {
        return ResponseMsg ==  null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public GetCustomizationOneEntity getResult() {
        return Result == null ? new GetCustomizationOneEntity() : Result;
    }

    public void setResult(GetCustomizationOneEntity result) {
        Result = result;
    }
}
