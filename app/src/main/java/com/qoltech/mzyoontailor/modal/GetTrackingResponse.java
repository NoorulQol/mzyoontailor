package com.qoltech.mzyoontailor.modal;


import com.qoltech.mzyoontailor.entity.GetTrackingEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class GetTrackingResponse implements Serializable {
    public String ResponseMsg;
    public ArrayList<GetTrackingEntity> Result;

    public String getResponseMsg() {
        return ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<GetTrackingEntity> getResult() {
        return Result;
    }

    public void setResult(ArrayList<GetTrackingEntity> result) {
        Result = result;
    }

}
