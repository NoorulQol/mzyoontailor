package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.TailorListEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class TailorListResponse implements Serializable {
    public String ResponseMsg;

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public ArrayList<TailorListEntity> getResult() {
        return Result == null ? new ArrayList<TailorListEntity>() :Result;
    }

    public void setResult(ArrayList<TailorListEntity> result) {
        Result = result;
    }

    public ArrayList<TailorListEntity> Result;


}
