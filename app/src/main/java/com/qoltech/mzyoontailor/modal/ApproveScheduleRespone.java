package com.qoltech.mzyoontailor.modal;

import java.io.Serializable;

public class ApproveScheduleRespone implements Serializable {
    public String Result;

    public String getResult() {
        return Result == null ? "" : Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getResponseMsg() {
        return ResponseMsg == null ? "" : ResponseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        ResponseMsg = responseMsg;
    }

    public String ResponseMsg;
}
