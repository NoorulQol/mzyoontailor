package com.qoltech.mzyoontailor.modal;

import com.qoltech.mzyoontailor.entity.ShopImagesEntity;

import java.io.Serializable;
import java.util.ArrayList;

public class InsertUpdateShopResponse implements Serializable {
    public String TailorId;
    public String ShopNameInEnglish;
    public String ShopNameInArabic;
    public String CountryId;
    public String CityId;
   public Double Latitude;
   public Double Longitude;
   public String AddressInEnglish;
   public String AddressinArabic;
   public String ShopProfileId;

    public String getTailorId() {
        return TailorId;
    }

    public void setTailorId(String tailorId) {
        TailorId = tailorId;
    }

    public String getShopNameInEnglish() {
        return ShopNameInEnglish;
    }

    public void setShopNameInEnglish(String shopNameInEnglish) {
        ShopNameInEnglish = shopNameInEnglish;
    }

    public String getShopNameInArabic() {
        return ShopNameInArabic;
    }

    public void setShopNameInArabic(String shopNameInArabic) {
        ShopNameInArabic = shopNameInArabic;
    }

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }

    public String getAddressInEnglish() {
        return AddressInEnglish;
    }

    public void setAddressInEnglish(String addressInEnglish) {
        AddressInEnglish = addressInEnglish;
    }

    public String getAddressinArabic() {
        return AddressinArabic;
    }

    public void setAddressinArabic(String addressinArabic) {
        AddressinArabic = addressinArabic;
    }

    public String getShopProfileId() {
        return ShopProfileId;
    }

    public void setShopProfileId(String shopProfileId) {
        ShopProfileId = shopProfileId;
    }

    public ArrayList<ShopImagesEntity> getShopImages() {
        return ShopImages;
    }

    public void setShopImages(ArrayList<ShopImagesEntity> shopImages) {
        ShopImages = shopImages;
    }

    public ArrayList<ShopImagesEntity> ShopImages;
}
