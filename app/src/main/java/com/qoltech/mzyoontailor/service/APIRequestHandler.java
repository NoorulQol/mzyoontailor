package com.qoltech.mzyoontailor.service;

import android.os.Build;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.qoltech.mzyoontailor.main.BaseActivity;
import com.qoltech.mzyoontailor.main.BaseFragment;
import com.qoltech.mzyoontailor.modal.AddMeasurementResponse;
import com.qoltech.mzyoontailor.modal.AppointmentListResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.ApproveMaterialResponse;
import com.qoltech.mzyoontailor.modal.ApproveMeasurementResponse;
import com.qoltech.mzyoontailor.modal.ApproveScheduleRespone;
import com.qoltech.mzyoontailor.modal.CountryCodeResponse;
import com.qoltech.mzyoontailor.modal.CustomizationOneApiCallModal;
import com.qoltech.mzyoontailor.modal.CustomizationThreeGetResponse;
import com.qoltech.mzyoontailor.modal.CustomizationTwoApiCallModal;
import com.qoltech.mzyoontailor.modal.DeleteAddressResponse;
import com.qoltech.mzyoontailor.modal.DeleteShopImageResponse;
import com.qoltech.mzyoontailor.modal.DressTypeResponse;
import com.qoltech.mzyoontailor.modal.FileUploadResponse;
import com.qoltech.mzyoontailor.modal.GenderResponse;
import com.qoltech.mzyoontailor.modal.GetAddressResponse;
import com.qoltech.mzyoontailor.modal.GetAppointmentDateForScheduleResponse;
import com.qoltech.mzyoontailor.modal.GetAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoontailor.modal.GetAreaResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationOneResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationThreeResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationTwoResponse;
import com.qoltech.mzyoontailor.modal.GetLanguageResponse;
import com.qoltech.mzyoontailor.modal.GetMaterialFromDateResponse;
import com.qoltech.mzyoontailor.modal.GetMeasurementFromDateResponse;
import com.qoltech.mzyoontailor.modal.GetMeasurementTwoResponse;
import com.qoltech.mzyoontailor.modal.GetNoOfAppointmentDateForScheduleResponse;
import com.qoltech.mzyoontailor.modal.GetStateResponse;
import com.qoltech.mzyoontailor.modal.GetTrackingResponse;
import com.qoltech.mzyoontailor.modal.InsertAddressResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.InsertLanguageModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueResponse;
import com.qoltech.mzyoontailor.modal.LoginOTPResponse;
import com.qoltech.mzyoontailor.modal.ManuallyResponse;
import com.qoltech.mzyoontailor.modal.MeasurementOneResponse;
import com.qoltech.mzyoontailor.modal.NewFlowCustomizationOneApiCall;
import com.qoltech.mzyoontailor.modal.NewFlowCustomizationTwoApiCall;
import com.qoltech.mzyoontailor.modal.OrderSummaryApiCallModal;
import com.qoltech.mzyoontailor.modal.OrderSummaryResponse;
import com.qoltech.mzyoontailor.modal.OrderTypeResponse;
import com.qoltech.mzyoontailor.modal.ProfileIntroResponse;
import com.qoltech.mzyoontailor.modal.ServiceTypeResponse;
import com.qoltech.mzyoontailor.modal.SubTypeResponse;
import com.qoltech.mzyoontailor.modal.TailorListResponse;
import com.qoltech.mzyoontailor.modal.UpdateAddressResponse;
import com.qoltech.mzyoontailor.modal.ValidateOTPResponse;
import com.qoltech.mzyoontailor.modal.ViewDetailsNewFlowResponse;
import com.qoltech.mzyoontailor.modal.ViewDetailsResponse;
import com.qoltech.mzyoontailor.utils.AppConstants;
import com.qoltech.mzyoontailor.utils.DialogManager;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIRequestHandler {

    private static APIRequestHandler sInstance = new APIRequestHandler();
    private static Retrofit mRetrofitDialog;

    /*Init retrofit for API call*/
    private Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    /*Init retrofit for API call*/
    private OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(1000, TimeUnit.SECONDS)
            .writeTimeout(1000, TimeUnit.SECONDS)
            .build();

    private Retrofit retrofit = new Retrofit.Builder().baseUrl(AppConstants.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();

    private APICommonInterface mServiceInterface = retrofit.create(APICommonInterface.class);

    public static APIRequestHandler getInstance() {
        return sInstance;
    }

    public static Retrofit getClient() {
        if (mRetrofitDialog == null) {
            mRetrofitDialog = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetrofitDialog;
    }

    /*GetCountryAPICall*/
    public void getAllCountryAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAllCountryAPI("api/Login/GetAllCountries").enqueue(new Callback<CountryCodeResponse>() {
            @Override
            public void onResponse(@NonNull Call<CountryCodeResponse> call, @NonNull Response<CountryCodeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountryCodeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }


    /*GetLoginOTPAPICall*/
    public void loginOTPGenerateAPICall(String CountryCode, String PhoneNum, String DeviceId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.loginOTPGenerateAPI(CountryCode, PhoneNum, DeviceId).enqueue(new Callback<LoginOTPResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginOTPResponse> call, @NonNull Response<LoginOTPResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginOTPResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*GetResendOTPAPICall*/
    public void resendOTPGenerateAPICall(String CountryCode, String PhoneNum, String DeviceId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.resendOTPGenerateAPI(CountryCode, PhoneNum, DeviceId).enqueue(new Callback<LoginOTPResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginOTPResponse> call, @NonNull Response<LoginOTPResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginOTPResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*GetValidateOTPAPICall*/
    public void validateOTPAPICall(String CountryCode, String PhoneNum, String DeviceId, int otp, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.validateOTPAPI(CountryCode, PhoneNum, DeviceId, otp, "Customer").enqueue(new Callback<ValidateOTPResponse>() {
            @Override
            public void onResponse(@NonNull Call<ValidateOTPResponse> call, @NonNull Response<ValidateOTPResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ValidateOTPResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","validateOTPAPICall",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*ProfileIntroAPICall*/
    public void profileIntroAPICall(int id, String userName, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.profileIntroAPI(id, userName).enqueue(new Callback<ProfileIntroResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProfileIntroResponse> call, @NonNull Response<ProfileIntroResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ProfileIntroResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","profileIntroAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetGenderAPICall*/
    public void getGenderAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getGenderAPI("api/Order/GetGenders").enqueue(new Callback<GenderResponse>() {
            @Override
            public void onResponse(@NonNull Call<GenderResponse> call, @NonNull Response<GenderResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GenderResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getGenderAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetDressTypeAPICall*/
    public void getDressTypeAPICall(final BaseActivity baseActivity, String genderId) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getDressTypeAPI("api/Order/GetDressTypeByGender?genderId=" + genderId).enqueue(new Callback<DressTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<DressTypeResponse> call, @NonNull Response<DressTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<DressTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getDressTypeAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetOrderTypeAPICall*/
    public void getOrderTypeAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getOrderTypeAPI("api/Order/DisplayOrderType").enqueue(new Callback<OrderTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderTypeResponse> call, @NonNull Response<OrderTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getOrderTypeAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetMeasurementOneAPICall*/
    public void getMeasurementOneAPICall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementOneAPI("api/Order/DisplayMeasurement1").enqueue(new Callback<MeasurementOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<MeasurementOneResponse> call, @NonNull Response<MeasurementOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<MeasurementOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementOneAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetCustomizationThreeAPICall*/
    public void getCustomizationThreeAPICall(String id, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationThreeAPI("api/Order/GetAttributesByAttributeId?AttributeId=" + id).enqueue(new Callback<GetCustomizationThreeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Response<GetCustomizationThreeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationThreeAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*Customization One Api*/
    public void getCustomizationOneApi(CustomizationOneApiCallModal customizationApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationOneAPI(customizationApiCallModal).enqueue(new Callback<GetCustomizationOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Response<GetCustomizationOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);


                insertErrorApiCall("APIRequestHandler","getCustomizationOneApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });
    }


    /*Customization Two Api*/
    public void getCustomizationTwoApi(CustomizationTwoApiCallModal customizationTwoApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationTwoAPI(customizationTwoApiCallModal).enqueue(new Callback<GetCustomizationTwoResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Response<GetCustomizationTwoResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationTwoAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });
    }

    /*GetCountryAPICall*/
    public void getSubTypeCall(String dressSubTypeId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getSubTypeAPI("api/order/DisplayDressSubType?Id=" + dressSubTypeId).enqueue(new Callback<SubTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<SubTypeResponse> call, @NonNull Response<SubTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getSubTypeAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*CustomizationThreeAPICall*/
    public void customizationThreeApiCall(String DressTypeId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.customizationThreeAPICall(DressTypeId).enqueue(new Callback<CustomizationThreeGetResponse>() {
            @Override
            public void onResponse(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Response<CustomizationThreeGetResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","customizationThreeAPICall",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetMeasurementTwoAPICall*/
    public void getMeasurementTwoApiCall(String measurementId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementTwoAPI("api/Order/GetMeasurement2?Id=" + measurementId).enqueue(new Callback<GetMeasurementTwoResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMeasurementTwoResponse> call, @NonNull Response<GetMeasurementTwoResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMeasurementTwoResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementTwoAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*ManuallyApiCall*/
    public void manuallyApiCall(String dressId, String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.manuallyAPI("Api/Order/GetExistingUserMeasurement?DressTypeId=" + dressId + "&&UserId=" + userId).enqueue(new Callback<ManuallyResponse>() {
            @Override
            public void onResponse(@NonNull Call<ManuallyResponse> call, @NonNull Response<ManuallyResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ManuallyResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","manuallyAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*GetTailorListAPICall*/
    public void getTailorListApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getTailorListAPI("api/Order/GetTailorlist").enqueue(new Callback<TailorListResponse>() {
            @Override
            public void onResponse(@NonNull Call<TailorListResponse> call, @NonNull Response<TailorListResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<TailorListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

                insertErrorApiCall("APIRequestHandler","getTailorListAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*OrderSummaryApiCall*/
    public void OrderSummaryApi(int dressId, int CustomerId, int AddressId, int PatternId, String Ordertype, String MeasurementId, String MaterialImage, String ReferenceImage, String CustomizationAttributeId, String AttributeImageId, String UserMeasurementId, String UserMeasurementValue, String MeasurementBy, String CreatedBy, String MeasurementName, String TailorId, String DeliveryTypeId, String MeasurmentType, String Units, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.orderSummaryAPI(dressId, CustomerId, AddressId, PatternId, Ordertype, MeasurementId, MaterialImage, ReferenceImage, CustomizationAttributeId, AttributeImageId, UserMeasurementId, UserMeasurementValue, MeasurementBy, CreatedBy, MeasurementName, TailorId, DeliveryTypeId, MeasurmentType, Units).enqueue(new Callback<OrderSummaryResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderSummaryResponse> call, @NonNull Response<OrderSummaryResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderSummaryResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","orderSummaryAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetServiceTypeApiCall*/
    public void getServiceTypeApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getServiceTypeApi("api/Shop/GetServiceType").enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceTypeResponse> call, @NonNull Response<ServiceTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getServiceTypeApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*InsertAppointmentMaterialApiCall*/
    public void insertAppointmentMaterialApi(String OrderId, String AppointmentType, String AppointmentTime, String From, String To, String TypeId,String Type, String CreatedBy, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAppointmentMaterialAPI(OrderId, AppointmentType, AppointmentTime, From, To, TypeId,Type, CreatedBy).enqueue(new Callback<InsertAppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAppointmentMaterialResponse> call, @NonNull Response<InsertAppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertAppointmentMeasurementAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*InsertAppointmentMeasurementApiCall*/
    public void insertAppointmentMeasurementApi(String OrderId, String AppointmentType, String AppointmentTime, String From, String To, String TypeId,String Type, String CreatedBy, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAppointmentMeasurementAPI(OrderId, AppointmentType, AppointmentTime, From, To, TypeId,Type, CreatedBy).enqueue(new Callback<InsertAppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAppointmentMeasurementResponse> call, @NonNull Response<InsertAppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertAppointmentMeasurementAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetAppointmentMaterialApiCall*/
    public void getAppointmentMaterialApi(String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentMaterialApi("api/Order/GetCustomerInAppoinmentMeaterial?OrderId=" + userId).enqueue(new Callback<AppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Response<AppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMaterialApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*GetAppointmentMeasurementApiCall*/
    public void getAppointmentMeasurementApi(String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentMeasurementApi("api/Order/GetCustomerInAppoinmentMeasurement?OrderId=" + userId).enqueue(new Callback<AppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Response<AppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

                insertErrorApiCall("APIRequestHandler","getAppointmentMeasurementApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetAppointmentMaterialApiCall*/
    public void getAppointmentMaterialFragApi(String userId, final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getAppointmentMaterialApi("api/Order/GetCustomerInAppoinmentMeaterial?OrderId=" + userId).enqueue(new Callback<AppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Response<AppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMaterialApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*GetAppointmentMeasurementApiCall*/
    public void getAppointmentMeasurementFragApi(String userId, final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getAppointmentMeasurementApi("api/Order/GetCustomerInAppoinmentMeasurement?OrderId=" + userId).enqueue(new Callback<AppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Response<AppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMeasurementApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }




    /*GetAppointmentMaterialApiCall*/
    public void getAppointmentMaterialActApi(String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentMaterialApi("api/Order/GetCustomerInAppoinmentMeaterial?OrderId=" + userId).enqueue(new Callback<AppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Response<AppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMaterialApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*GetAppointmentMeasurementApiCall*/
    public void getAppointmentMeasurementActApi(String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentMeasurementApi("api/Order/GetCustomerInAppoinmentMeasurement?OrderId=" + userId).enqueue(new Callback<AppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Response<AppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMeasurementApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }



    public void uploadMultiFile(ArrayList<String> mAddImageList, final BaseActivity baseActivity, String fileName) {
        DialogManager.getInstance().showProgress(baseActivity);
        File mUserImageFile;

        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[mAddImageList.size()];

        for (int index = 0; index < mAddImageList.size(); index++) {

            mUserImageFile = new File(mAddImageList.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);
            multipartTypedOutput[index] = MultipartBody.Part.createFormData(fileName + "[" + index + "]", mUserImageFile.getName(), surveyBody);

        }

        mServiceInterface.multiFileUpload(multipartTypedOutput).enqueue(new Callback<FileUploadResponse>() {

            @Override
            public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","multiFileUpload",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });


    }

    public void updateShopImg(ArrayList<String> mAddImageList, final BaseFragment baseActivity, String fileName) {
        DialogManager.getInstance().showProgress(baseActivity.getContext());
        File mUserImageFile;

        MultipartBody.Part[] multipartTypedOutput = new MultipartBody.Part[mAddImageList.size()];

        for (int index = 0; index < mAddImageList.size(); index++) {

            mUserImageFile = new File(mAddImageList.get(index));
            RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), mUserImageFile);
            multipartTypedOutput[index] = MultipartBody.Part.createFormData(fileName + "[" + index + "]", mUserImageFile.getName(), surveyBody);

        }

        mServiceInterface.multiFileUpload(multipartTypedOutput).enqueue(new Callback<FileUploadResponse>() {

            @Override
            public void onResponse(Call<FileUploadResponse> call, Response<FileUploadResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    DialogManager.getInstance().hideProgress();
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(Call<FileUploadResponse> call, Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","multiFileUpload",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });


    }

    /*GetLanguageApiCall*/
    public void getLanguageApiCall(final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getLanguageApi("api/Login/GetAllLanguages").enqueue(new Callback<GetLanguageResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetLanguageResponse> call, @NonNull Response<GetLanguageResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetLanguageResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getLanguageApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*ApproveMaterialApiCall*/
    public void approveMaterialApiCall(String AppointmentId, String IsApproved, String Reason, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.approveMaterialAppointmentAPI(AppointmentId, IsApproved, Reason).enqueue(new Callback<ApproveMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApproveMaterialResponse> call, @NonNull Response<ApproveMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApproveMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","approveMaterialAppointmentAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*ApproveMeasurementApiCall*/
    public void approveMeasurementApiCall(String AppointmentId, String IsApproved, String Reason, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.approveMeasurementAppointmentAPI(AppointmentId, IsApproved, Reason).enqueue(new Callback<ApproveMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApproveMeasurementResponse> call, @NonNull Response<ApproveMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApproveMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","approveMeasurementAppointmentAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetAppointmentListApiCall*/
    public void getAppointmentListApi(String userId, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentListApi("api/Shop/AppoinmentListTailor?Id=" + userId).enqueue(new Callback<AppointmentListResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentListResponse> call, @NonNull Response<AppointmentListResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentListResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentListApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*GetMaterialDateApiCall*/
    public void getMaterialDateApiCall(String OrderId,String TypeId, String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMaterialDateApi("api/Order/GetAppointmentDateForMaterail?OrderId=" + OrderId+"&TypeId="+TypeId+"&Type="+Type).enqueue(new Callback<GetMaterialFromDateResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMaterialFromDateResponse> call, @NonNull Response<GetMaterialFromDateResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMaterialFromDateResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMaterialDateApiCall",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*GetMeasurementDateApiCall*/
    public void getMeasurementDateApiCall(String OrderId,String TypeId, String Type, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getMeasurementDateApi("api/Order/GetAppointmentDateForMeasurement?OrderId=" + OrderId+"&TypeId="+TypeId+"&Type="+Type).enqueue(new Callback<GetMeasurementFromDateResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMeasurementFromDateResponse> call, @NonNull Response<GetMeasurementFromDateResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetMeasurementFromDateResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getMeasurementDateApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*OrderSummaryApiCall*/
    public void orderSummary(OrderSummaryApiCallModal orderSummaryApiCallModal, final BaseActivity baseActivity) {

        mServiceInterface.orderSummary(orderSummaryApiCallModal).enqueue(new Callback<OrderSummaryResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderSummaryResponse> call, @NonNull Response<OrderSummaryResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderSummaryResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","orderSummary",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    public void insertMeasurementValue(InsertMeasurementValueModal measurementValueModal, final BaseActivity baseActivity) {

        mServiceInterface.insertMeasurementValue(measurementValueModal).enqueue(new Callback<InsertMeasurementValueResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertMeasurementValueResponse> call, @NonNull Response<InsertMeasurementValueResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertMeasurementValueResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","insertMeasurementValue",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }
    /*GetAppointmentListApiCall*/
    public void deletShopImgApi(String Id, final BaseFragment baseActivity ) {
        DialogManager.getInstance().showProgress(baseActivity.getContext());
        mServiceInterface.deleteShopImgApi("Api/Shop/DeleteShopImages?Id=" + Id).enqueue(new Callback<DeleteShopImageResponse>() {
            @Override
            public void onResponse(@NonNull Call<DeleteShopImageResponse> call, @NonNull Response<DeleteShopImageResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteShopImageResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","deleteShopImgApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }



    /*GetViewPatternDetailsApiCall*/
    public void getViewDetailsApiCall(String PatternId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getViewDetailsApi("api/order/ViewMaterialDetails?Id="+PatternId).enqueue(new Callback<ViewDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ViewDetailsResponse> call, @NonNull Response<ViewDetailsResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }
            @Override
            public void onFailure(@NonNull Call<ViewDetailsResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getViewDetailsApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }



    /*DeleteAddressApiCall*/
    public void deleteAddressApiCall(String BuyerId,String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.deldeteAddressAPI("api/Shop/DeleteBuyerAddress?BuyerId="+BuyerId+"&Id="+Id ).enqueue(new Callback<DeleteAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<DeleteAddressResponse> call, @NonNull Response<DeleteAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAddressAPI",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }



    /*GetBuyerAddressAPICall*/
    public void getBuyerAddressApiCall(String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddressAPI("api/Shop/GetBuyerAddressByBuyerId?BuyerId="+Id).enqueue(new Callback<GetAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAddressResponse> call, @NonNull Response<GetAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getAddressAPI",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }



    /*GetAddressByIdAPICall*/
    public void getAddressApiCall(String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddressAPI("api/Shop/GetBuyerAddressById?Id="+Id).enqueue(new Callback<GetAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAddressResponse> call, @NonNull Response<GetAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getAddressAPI",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }

    /*GetStateApiCall*/
    public void getStateApiCall(String Id, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getStateAPI(Id).enqueue(new Callback<GetStateResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetStateResponse> call, @NonNull Response<GetStateResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetStateResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getStateAPI",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }
    /*GetAreaApiCall*/
    public void getAreaApiCall(String Id,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAreaApi(Id).enqueue(new Callback<GetAreaResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAreaResponse> call, @NonNull Response<GetAreaResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAreaResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getAreaApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }


    /*InsertAddressAPICall*/
    public void insertAddressApiCall(int BuyerId,String FirstName
            ,String LastName ,int CountryId,int StateId,int Area,String Floor,
                                     String LandMark,String LocationType,
                                     String ShippingNotes,Boolean IsDefault,String CountryCode,
                                     String PhoneNo,Double Longitude,Double Latitude,
                                     final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAddressAPI(BuyerId,FirstName,LastName,CountryId,StateId,Area,Floor,LandMark,LocationType,ShippingNotes,IsDefault,CountryCode,PhoneNo,Longitude,Latitude).enqueue(new Callback<InsertAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAddressResponse> call, @NonNull Response<InsertAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","insertAddressAPI",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }
    /*GetStateApiCall*/
    public void updateAddressApiCall(String Id , String BuyerId, String FirstName, String LastName, int CountryId, int StateId, int Area, String Floor, String LandMark,String LocationType,String ShippingNotes,Boolean IsDefault,String CountryCode,String PhoneNo,Double Longitude,Double Latitude, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.updateAddressAPI(Id,BuyerId,FirstName,LastName,CountryId,StateId,Area,Floor,LandMark,LocationType,ShippingNotes,IsDefault,CountryCode,PhoneNo,Longitude,Latitude).enqueue(new Callback<UpdateAddressResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateAddressResponse> call, @NonNull Response<UpdateAddressResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<UpdateAddressResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","updateAddressAPI",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }
    /*InsertLangaugae*/
    public void insertLanguageApiCall(String Id,String Language,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertLanguageApi(Id,Language,Type).enqueue(new Callback<InsertLanguageModal>() {
            @Override
            public void onResponse(@NonNull Call<InsertLanguageModal> call, @NonNull Response<InsertLanguageModal> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }
            @Override
            public void onFailure(@NonNull Call<InsertLanguageModal> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","insertLanguageApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }
    /*GetAddMeasurementApiCall*/
//    appsapi.mzyoon.com/Api/Order/GetExistingUserMeasurement?DressTypeId=1&UserId=1148

    public void getAddMeasurementApiCall(String subTypeId,String CustomerId,String tailorId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAddMeasurementApi("api/order/GetExistingUserMeasurement?DressTypeId="+subTypeId+"&UserId="+CustomerId+"&TailorId="+tailorId).enqueue(new Callback<AddMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AddMeasurementResponse> call, @NonNull Response<AddMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }
            @Override
            public void onFailure(@NonNull Call<AddMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getAddMeasurementApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }


    /*GetTrackingDetailsApiCall*/
    public void getTrackingDetailsApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getTrackingDetailsApi("api/Order/GetTrackingDetails?OrderId="+OrderId).enqueue(new Callback<GetTrackingResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetTrackingResponse> call, @NonNull Response<GetTrackingResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetTrackingResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getTrackingDetailsApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }
    /*Get Schedule Type*/
    public void getAppointmentForScheduleTypeApiCall(String OrderId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentForScheduleTypeApi("api/Order/GetCustomerInAppoinment?OrderId="+OrderId).enqueue(new Callback<GetAppointmentForScheduleTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAppointmentForScheduleTypeResponse> call, @NonNull Response<GetAppointmentForScheduleTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAppointmentForScheduleTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getAppointmentForScheduleTypeApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });
    }

    /*Insert Schedule Type*/
    public void insertAppointmentForScheduleTypeApiCall(String OrderId,String AppointmentType,String AppointmentTime,String From,String To,String Type,String TypeId,String CreatedBy, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.insertAppointmentForScheduleTypeApi(OrderId,AppointmentType,AppointmentTime,From,To,Type,TypeId,CreatedBy).enqueue(new Callback<InsertAppointmentForScheduleTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<InsertAppointmentForScheduleTypeResponse> call, @NonNull Response<InsertAppointmentForScheduleTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<InsertAppointmentForScheduleTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","insertAppointmentForScheduleTypeApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });

    }
    /*Get Schedule Type Date*/
    public void getAppointmentDateForScheduleTypeApiCall(String OrderId,String TypeId,String Type,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getAppointmentDateForScheduleTypeApi("api/Order/GetAppointmentDate?OrderId="+OrderId+"&TypeId="+TypeId+"&Type="+Type).enqueue(new Callback<GetAppointmentDateForScheduleResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetAppointmentDateForScheduleResponse> call, @NonNull Response<GetAppointmentDateForScheduleResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetAppointmentDateForScheduleResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getAppointmentDateForScheduleTypeApiCall",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });
    }

    /*Get Schedule Type No Of Date*/
    public void getNoOfAppointmentDateForScheduleTypeApiCall(String TailorId,String DressSubTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNoOfAppointmentDateForScheduleTypeApi("api/Order/GetTailorServiveDays?TailorId="+TailorId+"&DressSubTypeId="+DressSubTypeId).enqueue(new Callback<GetNoOfAppointmentDateForScheduleResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetNoOfAppointmentDateForScheduleResponse> call, @NonNull Response<GetNoOfAppointmentDateForScheduleResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetNoOfAppointmentDateForScheduleResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","getNoOfAppointmentDateForScheduleTypeApi",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);

            }
        });
    }

    /*ApproveScheduleApiCall*/
    public void approveScheduleApiCall(String AppointmentId, String IsApproved, String Reason, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.approveScheduleAppointmentAPI(AppointmentId, IsApproved, Reason).enqueue(new Callback<ApproveScheduleRespone>() {
            @Override
            public void onResponse(@NonNull Call<ApproveScheduleRespone> call, @NonNull Response<ApproveScheduleRespone> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ApproveScheduleRespone> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);

                insertErrorApiCall("baseActivity","approveScheduleAppointmentAPI",t.getMessage(),"OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }

    /*Insert Error*/
    public void insertErrorApiCall(String PageName,String MethodName,String Error,String ApiVersion,String DeviceId, final BaseActivity baseActivity) {
        mServiceInterface.insertErrorApi(PageName,MethodName,Error,ApiVersion,DeviceId,"Customer").enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(@NonNull Call<SignUpResponse> call, @NonNull Response<SignUpResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("baseActivity","insertErrorApiCall",response.raw().message(),"OS","DeviceId",baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpResponse> call, @NonNull Throwable t) {
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","insertErrorApiCall",t.getMessage(),"OS","DeviceId",baseActivity);

            }
        });

    }
    /*Insert Error*/
    public void insertErrorApiCall(String PageName,String MethodName,String Error,String ApiVersion,String DeviceId, final BaseFragment baseActivity) {
        mServiceInterface.insertErrorApi(PageName,MethodName,Error,ApiVersion,DeviceId,"Customer").enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(@NonNull Call<SignUpResponse> call, @NonNull Response<SignUpResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("baseActivity","insertErrorApiCall",response.raw().message(),"OS","DeviceId",baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpResponse> call, @NonNull Throwable t) {
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("baseActivity","insertErrorApiCall",t.getMessage(),"OS","DeviceId",baseActivity);

            }
        });

    }


    /*GetNewFlowTailorListAPICall*/
    public void getNewFlowOrderTypeApiCall( String tailorId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowOrderTypeApi("api/Order/GetOrderTypeByTailor?TailorId=" + tailorId).enqueue(new Callback<OrderTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<OrderTypeResponse> call, @NonNull Response<OrderTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<OrderTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
            }
        });

    }

    /*GetCustomizationThreeAPICall*/
    public void getCustomizationThreeNewFlowAPICall(String AttributeId,String TailorId,String DressSubType ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getCustomizationThreeAPI("api/Order/GetAttributeSelectionByAttributeId?AttributeId="+AttributeId+"&TailorId="+TailorId+"&DressSubTypeId="+DressSubType).enqueue(new Callback<GetCustomizationThreeResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Response<GetCustomizationThreeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getCustomizationThreeNewFlowAPICall",response.raw().message(), Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationThreeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getCustomizationThreeNewFlowAPICall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*CustomizationThreeAPICall*/
    public void customizationNewFlowThreeApiCall(String TailorId,String DressSubTypeId,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.customizationThreeNewFlowAPICall(TailorId,DressSubTypeId).enqueue(new Callback<CustomizationThreeGetResponse>() {
            @Override
            public void onResponse(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Response<CustomizationThreeGetResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","customizationNewFlowThreeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<CustomizationThreeGetResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","customizationNewFlowThreeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });

    }

    /*GetNewFlowServiceAPICall*/
    public void getNewFlowServiceTypeApiCall( String tailorId ,final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowServiceTypeApi("api/Order/GetDeliveryTypeByTailorId?TailorId=" + tailorId).enqueue(new Callback<ServiceTypeResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceTypeResponse> call, @NonNull Response<ServiceTypeResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowServiceTypeApiCall",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ServiceTypeResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowServiceTypeApiCall",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }



    /*Customization One NewFlow Api*/
    public void getNewFlowCustomizationOneApi(NewFlowCustomizationOneApiCall customizationApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowCustomizationOneAPI(customizationApiCallModal).enqueue(new Callback<GetCustomizationOneResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Response<GetCustomizationOneResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationOneApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationOneResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationOneApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }
    /*Customization Two NewFlow Api*/
    public void getNewFlowCustomizationTwoApi(NewFlowCustomizationTwoApiCall customizationTwoApiCallModal, final BaseActivity baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowCustomizationTwoAPI(customizationTwoApiCallModal).enqueue(new Callback<GetCustomizationTwoResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Response<GetCustomizationTwoResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationTwoApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<GetCustomizationTwoResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationTwoApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }


    /*Customization Two NewFlow Api*/
    public void getNewFlowViewDetailsApiCall(String TailorId,String PatternId,final BaseActivity baseActivity)  {
        DialogManager.getInstance().showProgress(baseActivity);
        mServiceInterface.getNewFlowViewDetailsApi("api/shop/GetMaterialDetails?TailorId="+TailorId+"&materialid="+PatternId).enqueue(new Callback<ViewDetailsNewFlowResponse>() {
            @Override
            public void onResponse(@NonNull Call<ViewDetailsNewFlowResponse> call, @NonNull Response<ViewDetailsNewFlowResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                    insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationTwoApi",response.raw().message(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

                }
            }

            @Override
            public void onFailure(@NonNull Call<ViewDetailsNewFlowResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getNewFlowCustomizationTwoApi",t.getMessage(),Build.VERSION.RELEASE,AppConstants.DEVICE_ID,baseActivity);

            }
        });
    }

    /*GetAppointmentMaterialApiCall*/
    public void getAppointmentMaterialActApiFragment(String userId, final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getAppointmentMaterialApi("api/Order/GetCustomerInAppoinmentMeaterial?OrderId=" + userId).enqueue(new Callback<AppointmentMaterialResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Response<AppointmentMaterialResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMaterialResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMaterialApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }


    /*GetAppointmentMeasurementApiCall*/
    public void getAppointmentMeasurementActApiFragment(String userId, final BaseFragment baseActivity) {
        DialogManager.getInstance().showProgress(baseActivity.getActivity());
        mServiceInterface.getAppointmentMeasurementApi("api/Order/GetCustomerInAppoinmentMeasurement?OrderId=" + userId).enqueue(new Callback<AppointmentMeasurementResponse>() {
            @Override
            public void onResponse(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Response<AppointmentMeasurementResponse> response) {
                DialogManager.getInstance().hideProgress();
                if (response.isSuccessful() && response.body() != null) {
                    baseActivity.onRequestSuccess(response.body());
                } else {
                    baseActivity.onRequestFailure(new Throwable(response.raw().message()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AppointmentMeasurementResponse> call, @NonNull Throwable t) {
                DialogManager.getInstance().hideProgress();
                baseActivity.onRequestFailure(t);
                insertErrorApiCall("APIRequestHandler","getAppointmentMeasurementApi",t.getMessage(),
                        "OS",AppConstants.DEVICE_ID_NEW,baseActivity);
            }
        });

    }
}
