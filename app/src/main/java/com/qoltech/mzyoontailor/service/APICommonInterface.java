package com.qoltech.mzyoontailor.service;

import com.qoltech.mzyoontailor.modal.AddMeasurementResponse;
import com.qoltech.mzyoontailor.modal.AppointmentListResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.AppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.ApproveMaterialResponse;
import com.qoltech.mzyoontailor.modal.ApproveMeasurementResponse;
import com.qoltech.mzyoontailor.modal.ApproveScheduleRespone;
import com.qoltech.mzyoontailor.modal.CountryCodeResponse;
import com.qoltech.mzyoontailor.modal.CustomizationOneApiCallModal;
import com.qoltech.mzyoontailor.modal.CustomizationThreeGetResponse;
import com.qoltech.mzyoontailor.modal.CustomizationTwoApiCallModal;
import com.qoltech.mzyoontailor.modal.DeleteAddressResponse;
import com.qoltech.mzyoontailor.modal.DeleteShopImageResponse;
import com.qoltech.mzyoontailor.modal.DressTypeResponse;
import com.qoltech.mzyoontailor.modal.FileUploadResponse;
import com.qoltech.mzyoontailor.modal.GenderResponse;
import com.qoltech.mzyoontailor.modal.GetAddressResponse;
import com.qoltech.mzyoontailor.modal.GetAppointmentDateForScheduleResponse;
import com.qoltech.mzyoontailor.modal.GetAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoontailor.modal.GetAreaResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationOneResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationThreeResponse;
import com.qoltech.mzyoontailor.modal.GetCustomizationTwoResponse;
import com.qoltech.mzyoontailor.modal.GetLanguageResponse;
import com.qoltech.mzyoontailor.modal.GetMaterialFromDateResponse;
import com.qoltech.mzyoontailor.modal.GetMeasurementFromDateResponse;
import com.qoltech.mzyoontailor.modal.GetMeasurementTwoResponse;
import com.qoltech.mzyoontailor.modal.GetNoOfAppointmentDateForScheduleResponse;
import com.qoltech.mzyoontailor.modal.GetStateResponse;
import com.qoltech.mzyoontailor.modal.GetTrackingResponse;
import com.qoltech.mzyoontailor.modal.InsertAddressResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentForScheduleTypeResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentMaterialResponse;
import com.qoltech.mzyoontailor.modal.InsertAppointmentMeasurementResponse;
import com.qoltech.mzyoontailor.modal.InsertLanguageModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueModal;
import com.qoltech.mzyoontailor.modal.InsertMeasurementValueResponse;
import com.qoltech.mzyoontailor.modal.LoginOTPResponse;
import com.qoltech.mzyoontailor.modal.ManuallyResponse;
import com.qoltech.mzyoontailor.modal.MeasurementOneResponse;
import com.qoltech.mzyoontailor.modal.NewFlowCustomizationOneApiCall;
import com.qoltech.mzyoontailor.modal.NewFlowCustomizationTwoApiCall;
import com.qoltech.mzyoontailor.modal.OrderSummaryApiCallModal;
import com.qoltech.mzyoontailor.modal.OrderSummaryResponse;
import com.qoltech.mzyoontailor.modal.OrderTypeResponse;
import com.qoltech.mzyoontailor.modal.ProfileIntroResponse;
import com.qoltech.mzyoontailor.modal.ServiceTypeResponse;
import com.qoltech.mzyoontailor.modal.SubTypeResponse;
import com.qoltech.mzyoontailor.modal.TailorListResponse;
import com.qoltech.mzyoontailor.modal.UpdateAddressResponse;
import com.qoltech.mzyoontailor.modal.ValidateOTPResponse;
import com.qoltech.mzyoontailor.modal.ViewDetailsNewFlowResponse;
import com.qoltech.mzyoontailor.modal.ViewDetailsResponse;
import com.qoltech.mzyoontailor.wrappers.SignUpResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface APICommonInterface {


    /*Get all country api call*/
    @GET
    Call<CountryCodeResponse> getAllCountryAPI(@Url String queryUrl);

    /*Get gender api call*/
    @GET
    Call<GenderResponse> getGenderAPI(@Url String queryUrl);

    /*Get dress type api call*/
    @GET
    Call<DressTypeResponse> getDressTypeAPI(@Url String queryUrl);

    /*Get dress type api call*/
    @GET
    Call<OrderTypeResponse> getOrderTypeAPI(@Url String queryUrl);

    /*Get Measurement api call*/
    @GET
    Call<MeasurementOneResponse> getMeasurementOneAPI(@Url String queryUrl);

    /*Get CustomizationThree api call*/
    @GET
    Call<GetCustomizationThreeResponse> getCustomizationThreeAPI(@Url String queryUrl);

    /*Get SubType api call*/
    @GET
    Call<SubTypeResponse> getSubTypeAPI(@Url String queryUrl);


    /*Manual api call*/
    @GET
    Call<ManuallyResponse> manuallyAPI(@Url String queryUrl);

    /*Manual api call*/
    @GET
    Call<DeleteShopImageResponse> deleteShopImgApi(@Url String queryUrl);

    /*UpdateProfileApiCall*/
    @FormUrlEncoded
    @POST("api/Order/GetCustomization3")
    Call<CustomizationThreeGetResponse> customizationThreeAPICall(@Field("DressTypeId") String DressTypeId);


    /*GenerateOTP*/
    @FormUrlEncoded
    @POST("api/Login/GenerateOTP")
    Call<LoginOTPResponse> loginOTPGenerateAPI(@Field("CountryCode") String CountryCode, @Field("PhoneNo") String PhoneNo, @Field("DeviceId") String DeviceId);

    /*ResendOTP*/
    @FormUrlEncoded
    @POST("api/Login/ResendOTP")
    Call<LoginOTPResponse> resendOTPGenerateAPI(@Field("CountryCode") String CountryCode, @Field("PhoneNo") String PhoneNo, @Field("DeviceId") String DeviceId);

    /*ValidateOTP*/
    @FormUrlEncoded
    @POST("api/Login/ValidateOTP")
    Call<ValidateOTPResponse> validateOTPAPI(@Field("CountryCode") String CountryCode, @Field("PhoneNo") String PhoneNo, @Field("DeviceId") String DeviceId, @Field("OTP") int OTP, @Field("Type") String Type);

    /*ProfileIntro*/
    @FormUrlEncoded
    @POST("api/Shop/InsertBuyerName")
    Call<ProfileIntroResponse> profileIntroAPI(@Field("Id") int id, @Field("Name") String name);


    /*CustomizationOne*/
    @POST("api/Order/GetCustomization1")
    Call<GetCustomizationOneResponse> getCustomizationOneAPI(@Body CustomizationOneApiCallModal customizationOneApiCallModal);

    /*CustomizationTwo*/
    @POST("api/Order/GetCustomization2")
    Call<GetCustomizationTwoResponse> getCustomizationTwoAPI(@Body CustomizationTwoApiCallModal customizationTwoApiCallModal);

    /*Get Measurement two api call*/
    @GET
    Call<GetMeasurementTwoResponse> getMeasurementTwoAPI(@Url String queryUrl);

    /*GetState*/
    @FormUrlEncoded
    @POST("api/Shop/DisplayStatebyCountry")
    Call<GetStateResponse> getStateAPI(@Field("Id") String Id);

    /*Get Tailor List api call*/
    @GET
    Call<TailorListResponse> getTailorListAPI(@Url String queryUrl);

    /*Order Summary*/
    @FormUrlEncoded
    @POST("api/Order/InsertOrder")
    Call<OrderSummaryResponse> orderSummaryAPI(@Field("dressType") int dressType, @Field("CustomerId") int CustomerId, @Field("AddressId") int AddressId, @Field("PatternId") int PatternId, @Field("Ordertype") String Ordertype, @Field("MeasurementId") String MeasurementId, @Field("MaterialImage[0][Image]") String MaterialImage, @Field("ReferenceImage[0][Image]") String ReferenceImage, @Field("OrderCustomization[0][CustomizationAttributeId]") String OrderCustomization, @Field("OrderCustomization[0][AttributeImageId]") String OrderCusomizationImage, @Field("UserMeasurementValue[0][UserMeasurementId]") String UserMeasurementId, @Field("UserMeasurementValue[0][Value]") String UserMeasurementValue, @Field(" MeasurementBy") String MeasurementBy, @Field("CreatedBy") String CreatedBy, @Field("MeasurementName") String MeasurementName, @Field("TailorId[0][Id]") String TailorId, @Field("DeliveryTypeId") String DeliveryTypeId, @Field("MeasurmentType") String MeasurmentType, @Field("Units") String Units);


    /*Get Service Type*/
    @GET
    Call<ServiceTypeResponse> getServiceTypeApi(@Url String queryUrl);


    /*Insert Appointment Material*/
    @FormUrlEncoded
    @POST("api/Order/InsertAppointforMaterial")
    Call<InsertAppointmentMaterialResponse> insertAppointmentMaterialAPI(@Field("OrderId") String OrderId, @Field("AppointmentType") String AppointmentType, @Field("AppointmentTime") String AppointmentTime, @Field("From") String From, @Field("To") String To, @Field("TypeId") String TypeId, @Field("Type") String Type, @Field("CreatedBy") String CreatedBy);

    /*Insert Appointment Measurement*/
    @FormUrlEncoded
    @POST("api/Order/InsertAppointforMeasurement")
    Call<InsertAppointmentMeasurementResponse> insertAppointmentMeasurementAPI(@Field("OrderId") String OrderId, @Field("AppointmentType") String AppointmentType, @Field("AppointmentTime") String AppointmentTime, @Field("From") String From, @Field("To") String To, @Field("TypeId") String TypeId, @Field("Type") String Type, @Field("CreatedBy") String CreatedBy);


    /*Get AppointmentMaterial*/
    @GET
    Call<AppointmentMaterialResponse> getAppointmentMaterialApi(@Url String queryUrl);


    /*Get AppointmentMeasurement*/
    @GET
    Call<AppointmentMeasurementResponse> getAppointmentMeasurementApi(@Url String queryUrl);


    @Multipart
    @POST("api/FileUpload/UploadFile")
    @Headers("Accept: */*")
    Call<FileUploadResponse> multiFileUpload(@Part MultipartBody.Part[] multipartTypedOutput);

    /*Get Order Details*/
    @GET
    Call<GetLanguageResponse> getLanguageApi(@Url String queryUrl);


    /*Arrprove Appointment Material*/
    @FormUrlEncoded
    @POST("api/Order/BuyerOrderApprovalMaterial")
    Call<ApproveMaterialResponse> approveMaterialAppointmentAPI(@Field("AppointmentId") String AppointmentId, @Field("IsApproved") String IsApproved, @Field("Reason") String Reason);

    /*Arrprove Appointment Schedule*/
    @FormUrlEncoded
    @POST("Api/Order/BuyerOrderAppoinmentApproval")
    Call<ApproveScheduleRespone> approveScheduleAppointmentAPI(@Field("AppointmentId") String AppointmentId,
                                                               @Field("IsApproved") String IsApproved,
                                                               @Field("Reason") String Reason);


    /*Arrprove Appointment Measurement*/
    @FormUrlEncoded
    @POST("api/Order/BuyerOrderApprovalMeasurement")
    Call<ApproveMeasurementResponse> approveMeasurementAppointmentAPI(@Field("AppointmentId") String AppointmentId, @Field("IsApproved") String IsApproved, @Field("Reason") String Reason);


    /*Get AppointmentList*/
    @GET
    Call<AppointmentListResponse> getAppointmentListApi(@Url String queryUrl);


    /*Get MaterialDate*/
    @GET
    Call<GetMaterialFromDateResponse> getMaterialDateApi(@Url String queryUrl);


    /*Get MeasurementDate*/
    @GET
    Call<GetMeasurementFromDateResponse> getMeasurementDateApi(@Url String queryUrl);


    /*Insert Order Summary*/
    @POST("api/Order/InsertOrder")
    Call<OrderSummaryResponse> orderSummary(@Body OrderSummaryApiCallModal orderSummaryApiCallModal);

    /*Insert Measurement Value*/
    @POST("api/Order/InsertUserMeasurementValues")
    Call<InsertMeasurementValueResponse> insertMeasurementValue(@Body InsertMeasurementValueModal measurementValueModal);



    /*Get View Pattern Details*/
    @GET
    Call<ViewDetailsResponse> getViewDetailsApi(@Url String queryUrl);


    /*Delete Address*/
    @GET
    Call<DeleteAddressResponse> deldeteAddressAPI(@Url String queryUrl);


    /*Get Buyer Address api call*/
    @GET
    Call<GetAddressResponse> getAddressAPI(@Url String queryUrl);



    /*Get Area*/
    @FormUrlEncoded
    @POST("api/Shop/GetAreaByState")
    Call<GetAreaResponse> getAreaApi(@Field("StateId") String Id);


    /*InsertAddress*/
    @FormUrlEncoded
    @POST("api/Shop/InsertBuyerAddress")
    Call<InsertAddressResponse> insertAddressAPI(@Field("BuyerId") int BuyerId, @Field("FirstName") String FirstName , @Field("LastName") String LastName , @Field("CountryId") int CountryId, @Field("StateId") int StateId, @Field("AreaId") int Area, @Field("Floor") String Floor, @Field("LandMark") String LandMark, @Field("LocationType") String LocationType, @Field("ShippingNotes") String ShippingNotes, @Field("IsDefault") Boolean IsDefault, @Field("CountryCode") String CountryCode, @Field("PhoneNo") String PhoneNo, @Field("Longitude") Double Longitude, @Field("Lattitude") Double Latitude);


    /*Update Buyer Address*/
    @FormUrlEncoded
    @POST("api/Shop/UpdateBuyerAddress")
    Call<UpdateAddressResponse> updateAddressAPI(@Field("Id") String Id , @Field("BuyerId") String BuyerId, @Field("FirstName") String FirstName, @Field("LastName") String LastName, @Field("CountryId") int CountryId, @Field("StateId") int StateId, @Field("AreaId") int Area, @Field("Floor") String Floor, @Field("LandMark") String LandMark, @Field("LocationType") String LocationType, @Field("ShippingNotes") String ShippingNotes, @Field("IsDefault") Boolean IsDefault, @Field("CountryCode") String CountryCode, @Field("PhoneNo") String PhoneNo, @Field("Longitude") Double Longitude, @Field("Lattitude") Double Latitude);


    /*Tailor Approved*/
    @FormUrlEncoded
    @POST("api/Login/InsertLanguage")
    Call<InsertLanguageModal> insertLanguageApi(@Field("Id") String Id, @Field("language") String language,
                                                @Field("Type") String Type);


    /*Get Add Measurement*/
    @GET
    Call<AddMeasurementResponse> getAddMeasurementApi(@Url String queryUrl);

    /*Get Tracking Details*/
    @GET
    Call<GetTrackingResponse> getTrackingDetailsApi(@Url String queryUrl);
    /*Get ScheduleType*/
    @GET
    Call<GetAppointmentForScheduleTypeResponse> getAppointmentForScheduleTypeApi(@Url String queryUrl);

    /*Insert Schedule Type Date*/
    @FormUrlEncoded
    @POST("api/Order/InsertAppointment")
    Call<InsertAppointmentForScheduleTypeResponse> insertAppointmentForScheduleTypeApi(@Field("OrderId") String OrderId, @Field("AppointmentType") String AppointmentType, @Field("AppointmentTime") String AppointmentTime, @Field("From") String From, @Field("To") String To, @Field("Type") String Type, @Field("TypeId") String TypeId, @Field("CreatedBy") String CreatedBy);

/*Get Schedul Type Date*/
    @GET
    Call<GetAppointmentDateForScheduleResponse> getAppointmentDateForScheduleTypeApi(@Url String queryUrl);

    /*Get Schedul Type Date*/
    @GET
    Call<GetNoOfAppointmentDateForScheduleResponse> getNoOfAppointmentDateForScheduleTypeApi(@Url String queryUrl);

    /*Insert Error*/
    @FormUrlEncoded
    @POST("api/Order/InsertAppointment")
    Call<SignUpResponse> insertErrorApi(@Field("PageName") String PageName, @Field("MethodName") String MethodName, @Field("Error") String Error,
                                        @Field("ApiVersion") String ApiVersion, @Field("DeviceId") String DeviceId, @Field("Type") String Type);


    /*Get Order Type*/
    @GET
    Call<OrderTypeResponse> getNewFlowOrderTypeApi(@Url String queryUrl);

    /*GetNewFlowCustomizationThreeApiCall*/
    @FormUrlEncoded
    @POST("api/order/GetMaterialSelection3")
    Call<CustomizationThreeGetResponse> customizationThreeNewFlowAPICall(@Field("TailorId") String TailorId,@Field("DressSubTypeId") String DressSubTypeId);

    /*Get Service Type*/
    @GET
    Call<ServiceTypeResponse> getNewFlowServiceTypeApi(@Url String queryUrl);


    /*CustomizationOne*/
    @POST("api/Order/GetMaterialSelection1")
    Call<GetCustomizationOneResponse> getNewFlowCustomizationOneAPI(@Body NewFlowCustomizationOneApiCall customizationOneApiCallModal);


    /*CustomizationTwo*/
    @POST("api/Order/GetMaterialSelection2")
    Call<GetCustomizationTwoResponse> getNewFlowCustomizationTwoAPI(@Body NewFlowCustomizationTwoApiCall customizationTwoApiCallModal);


    /*Get View Pattern Details*/
    @GET
    Call<ViewDetailsNewFlowResponse> getNewFlowViewDetailsApi(@Url String queryUrl);
}
