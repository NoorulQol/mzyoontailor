package com.qoltech.mzyoontailor.CommenInterface;


public interface InterfaceTwoBtnCallBack extends InterfaceBtnCallBack {

    void onNegativeClick();
}
